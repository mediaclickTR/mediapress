<!doctype html>
<html lang="en">
<head>
    @include('RenderableEditorPanel::editors.header')
</head>
<body>
<div class="container">
    <div class="py-5 text-center">
        <div class="float-left">
            <a href="{{route('admin.editors.index')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Listeye
                Dön</a>
        </div>
        <div class="float-right">
            <a href="javascript:void(0);" onclick="javascript:sendToCloud()" class="btn btn-primary"> <i
                        class="fa fa-cloud-upload-alt"></i> Buluta Gönder</a>
        </div>
        <h4>{!! $class !!}</h4>
    </div>

    <div class="row">
        <div class="col-12">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Kapat</span>
                    </button>
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 order-md-2 mb-4">
            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="elements-tab" data-toggle="tab" href="#elements" role="tab"
                       aria-controls="elements" aria-selected="true">Elements</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="templates-tab" data-toggle="tab" href="#templates" role="tab"
                       aria-controls="templates" aria-selected="false">Templates</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="layouts-tab" data-toggle="tab" href="#layouts" role="tab"
                       aria-controls="layouts" aria-selected="false">Layouts</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade show active" id="elements" role="tabpanel" aria-labelledby="elements-tab">
                    <div class="elements_toolbar">
                        <div class="toolbar_row">
                            <div class="toolbar_cell">
                                <button type="button" data-toggle-target="#filter_tools" data-toggle-state="out"
                                        class="elements_toolbar_toggler btn btn-light" title="Filtreler Aç/Kapa"><i
                                            class="fa fa-filter"></i> <i class="fa fa-chevron-down chevron"></i>
                                </button>
                            </div>
                            <div class="toolbar_cell">
                                {{--    <div class="input-group">
                                        <span class="input-group-addon" id="prefixId">prefix</span>
                                        <input
                                            type="text|password|email|number|submit|date|datetime|datetime-local|month|color|range|search|tel|time|url|week"
                                            name="name" id="name" class="form-control" placeholder=""
                                            aria-describedby="prefixId">
                                    </div>--}}
                                <div class="form-group">
                                    <input
                                            type="text"
                                            class="form-control" name="" id="filterElementsInput"
                                            aria-describedby="helpId" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elements_tools">
                        <div id="filter_tools" style="display:none;">

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="filter-all-tags-check" class="form-check-input" checked>
                                    <h5>Tags</h5>
                                </label>
                            </div>

                            <ul class="list-unstyled" id="filter-objects-by-tags" style="text-indent: 9px;">
                                @foreach($object_tags as $tag_key=>$tag)
                                    <li>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input"
                                                       value="{{$tag_key}}" checked>
                                                {{ $tag }}
                                            </label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <hr>
                        </div>
                    </div>
                    <ul class="list-group renderables-list">@foreach($types as $key=>$value)
                            <li class="list-group-item p-1 pl-3 pr-2 @foreach($value["object_tags"] as $tag)has_{{ $tag }}_tag @endforeach">
                                <a href="javascript:void(0);"
                                   onclick="javascript:addRenderableElement($(this))"
                                   data-type="{!! $key !!}"
                                   title="{!! $value["description"] !!}"
                                ><i class="fa fa-{!! $value["icon_key"] !!}"></i> {!! $value["name"] !!}</a>
                                <button type="button" class="btn btn-info btn-sm d-inline-block float-right p-0 pl-1 pr-1 rendereable-doc-trigger">
                                    <i class="fa fa-question-circle"></i>
                                </button>

                            </li> @endforeach
                    </ul>


                </div>
                <div class="tab-pane fade" id="templates" role="tabpanel" aria-labelledby="templates-tab">
                    <div class="elements_tools">
                        <div id="filter_tools" style="display:none;">

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="filter-all-tags-check" class="form-check-input" checked>
                                    <h5>Tags</h5>
                                </label>
                            </div>

                            <ul class="list-unstyled" id="filter-objects-by-tags" style="text-indent: 9px;">
                                @foreach($object_tags as $tag_key=>$tag)
                                    <li>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input"
                                                       value="{{$tag_key}}" checked>
                                                {{ $tag }}
                                            </label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <hr>
                        </div>
                    </div>
                    <ul class="list-group renderables-list">
                        @foreach($templates as $temp)
                            <li class="list-group-item p-1 pl-3 pr-2 has_basic_html_tag p-2">
                                <a href="javascript:void(0);" onclick="javascript:addTemplate($(this))" data-template='{!! $temp['data'] !!}' title="{!! $temp['name'] !!}"><i class="fa {!! $temp['icon'] !!}"></i> {!! $temp['name'] !!}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-pane fade" id="layouts" role="tabpanel" aria-labelledby="layouts-tab">
                    <img src="https://www.basemodels.co.uk/images/loading.gif.pagespeed.ce.tHQj3U4D_l.gif" alt="">


                </div>

            </div>


        </div>
        <div class="col-md-8 order-md-1">
           <div class="col text-center">
               <button type="button" class="btn btn-lg btn-primary" onclick='javascript:$("#outputForm")[0].submit();'>Kaydet</button>
               <div class="clearfix"></div>
            </div>
            <div class="form nested">
                <div class="dd" id="nestable" style="max-width: inherit">

                </div>
            </div>
            <footer class="my-5 pt-5 text-muted text-center text-small">
                <form id="outputForm" action="{!! route('admin.editors.update') !!}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="filePath" value="{!! $filePath !!}">
                    <textarea id="nestable-output" name="output" class="form-control"
                              style="display: none">{!! $list !!}</textarea>
                    <button type="submit" class="btn btn-lg btn-primary">Kaydet</button>

                </form>
            </footer>
        </div>
    </div>


    @include('RenderableEditorPanel::editors.renderableElement')
    @include('RenderableEditorPanel::editors.sendToCloud')
    @include('RenderableEditorPanel::editors.tagModal')

</div>
<div>
    <h2>input_array</h2>
    <hr>
    <p> Array döndürebilen BuilderRenderable yapıları:</p>
    <ol>
        <li><a href="#input_array_type_stdarray"> Standart Array Notasyonu </a></li>
        <li><a href="#input_array_type_ds"> DataSource </a></li>
        <li><a href="#input_array_type_varkw"> &lt;var&gt; Anahtarı </a></li>
        <li><a href="#input_array_type_fa"> Function Array'leri </a></li>
    </ol>
    <br><br>

    <h4 id="input_array_type_stdarray">1. Standart Array Notasyonu</h4>
    <p>
        Php içinde işlenebilecek şekilde aktarılacak <strong>durağan ve anahtarlı dize</strong>.
        Renderable içine şu örnekteki gibi basılabilecek:
        <code>
            <pre>
                [
                    0 => "Seçim Yok",
                    "0" => "--Seçim Yok--",
                    "aaa" => "İlk Tamga 3 Kere",
                ]</pre>
        </code>
    </p>
    <br><br>

    <h4 id="input_array_type_stdarray">2. DataSource</h4>
    <p>
        Renderable tarafından tanınan DataSource array'i. <strong>Dinamik veri</strong> sağlama amaçlı.
        Renderable içine şu örnekteki gibi basılabilecek:
        <code>
            <pre>
                [
                    "type"=>"ds:<abbr title="Mediapress Modülü Adı">Content</abbr>-><abbr
                        title="Modüle ait DataSource'un adı">DefaultSitemapXmlBlockData</abbr>",
                    "params" => [
                        "sitemap_xml_type" => "default",
                        "sitemap_feature" => "homepage"
                    ]
                ]</pre>
        </code>
    </p>
    <br><br>

    <h4 id="input_array_type_stdarray">3. &lt;var&gt; Anahtarı</h4>
    <p>
        Renderable'ın bir değişkenin değeriyle yer değiştireceği string.
        Renderable içine şu örnekteki gibi basılacak:
        <code>
            <pre>&lt;var&gt;<abbr title="Değeri, bu dizenin yerine geçecek PHP değişkeni adı">degisken_adi</abbr>&lt;/var&gt;</pre>
        </code>
    </p>
    <br><br>

    <h4 id="input_array_type_stdarray">4. Function Array'leri</h4>
    <p>
        <code>
            <pre></pre>
        </code>
    </p>

    <br>
    <br>

    <h4 id="control_doc_mpfile">5. MPFile Elemanı</h4>
    <p>
        MPFile elemanı için örnek ayarlar:
        <code>
            <pre>"options.html.attributes.name"=> "category&lt;print&gt;category->id&lt;/print&gt;"</pre>
        </code>
        <code>
            <pre>"params.files"=> "&lt;var&gt;category->mfiles&lt;/var&gt;"</pre>
        </code>
    </p>


</div>



<script>


    $(document).ready(function () {
        $("button.elements_toolbar_toggler").click(function (e) {
            var button = this;
            var target = $($(button).data("toggle-target"));
            var state = $(button).data("toggle-state");
            if (state == "out") {
                target.slideDown("100", function () {
                    $(button).data("toggle-state", "in").find("i.fa.chevron").toggleClass("fa-chevron-down").toggleClass("fa-chevron-up")
                })
            } else {
                target.slideUp("100", function () {
                    $(button).data("toggle-state", "out").find("i.fa.chevron").toggleClass("fa-chevron-down").toggleClass("fa-chevron-up")
                })
            }
        });

        $("div#filter_tools input#filter-all-tags-check").change(function () {
            $("div#filter_tools ul#filter-objects-by-tags > li input").prop("checked", $(this).prop("checked"));
            filterByTags();
        });

        $("div#filter_tools ul#filter-objects-by-tags > li input").change(function () {
            filterByTags();
        });

        $('div.elements_toolbar #filterElementsInput').keyup(function () {
            var valThis = $(this).val().toLowerCase();
            if (valThis == "") {
                $('.renderables-list > li.list-group-item').show();
            } else {
                $('.renderables-list > li.list-group-item').each(function () {
                    var text = $(this).text().toLowerCase();
                    (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
                });
            }
            ;
        });
    });

    function filterByTags() {
        var selected_tags = [];
        $("div#filter_tools ul#filter-objects-by-tags > li input:checked").each(function (ind, el) {
            selected_tags.push($(el).val());
        });
        var classes_of_selected_tags = [];
        $("ul.renderables-list li").hide();
        if (selected_tags.length) {
            for (var i = 0; i < selected_tags.length; i++) {
                $("ul.renderables-list li.has_" + selected_tags[i] + "_tag").show();
            }
        }
    }


    function addRenderableElement(el) {
        let type = el.data('type');
        let info = {_token: '{!! csrf_token() !!}'};
        $('#renderableElement input[name=id]').val(null);
        $('#renderableElement input[name=type]').val(type);
        $.post('{!! route('admin.editors.ajax.post') !!}/' + type, info, function (response) {
            $('#renderableElement .modal-body').html(response.json);
            $('#renderableElementTitle').html('<i class="fa fa-' + response.json.icon_key + '"></i> ' + response.json.name + '<br><small>' + response.json.description + '</small>');
            $('#renderableElement .modal-body').html(response.view);
            $('#renderableElement').modal('show');


        })

    }

    function editRenderableElement(el) {
        el = el.parents('li');
        let type = el.data('type');
        let id = el.data('id');
        let info = el.data('_info');

        $('#renderableElement input[name=id]').val(id);
        $('#renderableElement input[name=type]').val(type);
        info._token = '{!! csrf_token() !!}';
        $.post('{!! route('admin.editors.ajax.post') !!}/' + type, info, function (response) {
            $('#renderableElement .modal-body').html(response.json);
            $('#renderableElementTitle').html('<i class="fa fa-' + response.json.icon_key + '"></i> ' + response.json.name + '<br><small>' + response.json.description + '</small>');
            $('#renderableElement .modal-body').html(response.view);
            $('#renderableElement').modal('show');

        })

    }


    function syntaxHighlight(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    getLayouts();

    function name(type, options) {

        let list = @json($names);

        if (list[type] !== undefined) {
            let typer = list[type];
            if (options !== undefined && options.title !== undefined) {
                return typer + ' (' + options.title + ')';
            }
            return typer;
        }
        return type;
    }

    function addLayout(el) {

        let confirmation = confirm("Are you sure to change this layout?");

        if (!confirmation) {
            return;
        }
        $('.nested').html('');
        let obj = el.data('data');


        let output = ' <div class="dd" id="nestable" style="max-width: inherit"><ol class="dd-list">';
        $.each(obj, function (index, item) {

            output += buildItem(item, 1);

        });
        output += "</ol> </div>";
        $('.nested').html(output);
        render();


    }

    function addTemplate(el) {

        let obj = el.data('template');

        let output = '';

        $.each(obj, function (index, item) {
            output += buildItem(item, 1);
        });

        $('#nestable > ol').append(output);
    }

    function render() {
        $('#nestable').nestable({
            maxDepth: 100
        }).on('change', updateOutput);
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    }

    var obj = '{!! $list !!}';
    var output = '<ol class="dd-list">';


    $.each(JSON.parse(obj), function (index, item) {


        output += buildItem(item);

    });
    output += "</ol>";
    $('#nestable').html(output);
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };


    render();

    function buildItem(item, generate = 0) {
        let id = '';
        if (generate == 0) {
            id = item.id;
        } else {
            id = generate_token();
        }
        var html = "<li class='dd-item dd3-item' data-id='" + id + "' ";
        if (item.type !== undefined) {
            html += "data-type='" + item.type + "' ";
        }
        if (item.info !== undefined) {
            item._info = item.info;
        }
        if (item._info !== undefined) {
            if (item._info == '[]') {
                item._info = '{}';
            }
            if (generate == 0) {
                html += "data-_info='" + item._info + "' ";
            } else {
                html += "data-_info='" + JSON.stringify(item._info) + "' ";
            }

        }
        html += ">";
        let options = '';
        try{
            if(item._info.indexOf("&quot;") >= 0){
                options = json3(item._info).options;
            }
        }catch (e) {
            options =item._info.options;
        }

        html += "<div class='dd-handle dd3-handle'>Drag</div><div class='dd3-content'><dr>" + name(item.type, options) + "</dr> <span class='float-right'>" +
            "&nbsp;<i class='fa fa-trash' title='Delete Element with subs' onclick='javascript:removeElementAll($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-times' title='Delete Element' onclick='javascript:removeElement($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-copy' title='Clone Element' onclick='javascript:clone($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-cog' title='Options' onclick='javascript:editRenderableElement($(this))'></i>" +
            "</span></div>  ";

        if (item.children) {

            html += "<ol class='dd-list'>";
            $.each(item.children, function (index, sub) {
                html += buildItem(sub, generate);
            });
            html += "</ol>";

        }

        html += "</li>";

        return html;
    }

    function getLayouts() {
        $.get('{!! route('admin.editors.layouts') !!}', function (editors) {
            let html = ' <div class="list-group">';
            $.each(editors, function (index, sub) {

                html += ' <a href="javascript:void(0);" class="list-group-item list-group-item-action" onclick="javascript:addLayout($(this))" data-data="' + sub.structure + '" >'
                html += sub.name + ' &nbsp;<span class="float-right  badge badge-pill badge-primary" title="' + sub.username + '"><i class="fa fa-info"></i></span></a>'

            });
            html += '</div>';

            $('#layouts').html(html)
        });
    }
    function change_name(form){
        let namef = name(form.type,form.info.options)

         $('li[data-id=' + form.id + '] > .dd3-content dr').html(namef);
    }

    function saveRenderableElement() {

        let form = $('#renderableElement form').serializeControls();

        if (form.id) {
            change_name(form);
            $('li[data-id=' + form.id + ']').data('_info', form.info);
        } else {
            let html = buildItem(form, 1);
            $('#nestable > ol').append(html);
        }

        $('#renderableElement').modal('hide');
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    }

    $.fn.serializeControls = function () {
        var data = {};

        function buildInputObject(arr, val) {
            if (arr.length < 1)
                return val;
            var objkey = arr[0];
            if (objkey.slice(-1) == "]") {
                objkey = objkey.slice(0, -1);
            }
            var result = {};
            if (arr.length == 1) {
                result[objkey] = val;
            } else {
                arr.shift();
                var nestedVal = buildInputObject(arr, val);
                result[objkey] = nestedVal;
            }
            return result;
        }

        $.each(this.serializeArray(), function () {
            var val = this.value;
            var c = this.name.split("[");
            var a = buildInputObject(c, val);
            $.extend(true, data, a);
        });

        return data;
    }

    function addElement(el) {
        let token = generate_token();
        let type = el.data('type');
        let options = el.data('options');
        let params = el.data('params');
        var html = "<li class='dd-item dd3-item' data-id='" + token + "' ";
        if (type !== undefined) {
            html += "data-type='" + type + "' ";
        }
        if (options !== undefined) {

            html += "data-options='" + json2(options) + "' ";
        }

        if (params !== undefined) {
            html += "data-params='" + json2(params) + "' ";
        }
        html += ">";
        html += "<div class='dd-handle dd3-handle'>Drag</div><div class='dd3-content'><dr>" + name(type, options) + " </dr><span class='float-right'>" +
            "&nbsp;<i class='fa fa-trash' title='Delete Element with subs' onclick='javascript:removeElementAll($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-times' title='Delete Element' onclick='javascript:removeElement($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-copy' title='Clone Element' onclick='javascript:clone($(this))'></i>" +
            "&nbsp;&nbsp;<i class='fa fa-cog' title='Options' onclick='javascript:editRenderableElement($(this))'></i>" +
            "</span></div>  ";

        html += "</li>";
        $('#nestable > ol').append(html);
        updateNestable();
    }

    function updateNestable() {
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    }

    function sendToCloud() {
        $('.json_output').val($('#nestable-output').val());
        $('#sendCloudModal').modal('show');

    }

    function sendToCloudAjax() {
        let serialize = $('.form-holder-ajax').serializeArray();

        $.post('{!! route('admin.editors.cloud') !!}', serialize, function (response) {

            console.log(response);
        }).done(function () {
            $('#sendCloudModal').modal('hide');
        });
    }


    function addnew() {
        let active = $('#myTabContent.tab-content .active');


        let id = active.attr('id');

        let token = generate_token();

        if (id == 'tags') {

            $('#tagModal').modal('show');
        } else {
            $('#myTabContent #' + id).append('<div class="form-group"><div class="row"><div class="col-md-5"><input type="text" class="form-control" name="' + id + '.keys.' + token + '" value=""></div><div class="col-md-1 text-center">-</div><div class="col-md-5"><input type="text" class="form-control" name="' + id + '.values.' + token + '" value=""></div><a href="javascript:void(0);" onclick="javascript:remove($(this))"><i class="fa fa-trash"></i></a></div></div>');
        }

    }

    function addnewMpfile(id) {
        $('#tagModal input[name=holder]').val(id);
        $('#renderableElement').modal('hide');
        $('#tagModal').modal('show');
    }

    function remove(el) {
        let parent = el.parents('.form-group');
        parent.remove();

    }

    function removeElement(el) {

        let confirmation = confirm("Are you sure to delete this element?");

        if (confirmation) {
            let li = el.parent().parent().parent();

            let children = li.find('> ol > li');
            li.before(children);
            li.remove();
        }
        updateNestable();


    }

    function removeElementAll(el) {

        let confirmation = confirm("Are you sure to delete this element with subs?");

        if (confirmation) {
            let li = el.parent().parent().parent();

            li.remove();
        }
        updateNestable();

    }

    function clone(el) {
        let confirmation = confirm("Are you sure to clone this element?");

        if (confirmation) {
            let li = el.parent().parent().parent();

            li.parents('.dd').find('>ol').append(li.clone());


            let structure = li.parents('.dd');
            $.each(structure.find("li[data-id^='5']"), function (i, val) {
                $(val).attr('data-id', generate_token());

            });

            updateNestable();
        }
    }


    function json(object) {
        return object;
        /*   let string = JSON.stringify(object);
           return string.replace('/\"/g', '&quot;'); */
    }

    function json2(object) {

        let string = JSON.stringify(object);

        return string.replace('/\"/g', '&quot;');
    }

    function json3(object) {

        let string = JSON.stringify(object);
        string = string.replace(/"/g, '');
        return JSON.parse(string.replace(/&quot;/g, '\"'));
    }

    function generate_token() {
        //edit the token allowed characters
        var a = "abcdef1234567890".split("");
        var b = [];
        for (var i = 0; i < 12; i++) {
            var j = (Math.random() * (a.length - 1)).toFixed(0);
            b[i] = a[j];
        }
        return "5" + b.join("");
    }

    function updateTags() {
        let form = $('form.form-holder-ajax-forms');
        let tagOptions = {};

        tagOptions.key = form.find('input[name=key]').val();
        tagOptions.file_type = form.find('input[name=file_type]:checked').val();
        tagOptions.required = form.find('input[name=required]:checked').val();
        tagOptions.title = form.find('input[name=title]').val();
        tagOptions.allow_actions = ["select", "upload"];
        tagOptions.allow_diskkeys = ["azure", "local"];
        tagOptions.extensions = form.find('input[name=extensions]').val();
        tagOptions.min_width = form.find('input[name=min_width]').val();
        tagOptions.max_width = form.find('input[name=max_width]').val();
        tagOptions.min_height = form.find('input[name=min_height]').val();
        tagOptions.max_height = form.find('input[name=max_height]').val();
        tagOptions.width = form.find('input[name=width]').val();
        tagOptions.height = form.find('input[name=height]').val();
        tagOptions.min_filesize = form.find('input[name=min_filesize]').val();
        tagOptions.max_filesize = form.find('input[name=max_filesize]').val();
        tagOptions.max_file_count = form.find('input[name=max_file_count]').val();
        tagOptions.additional_rules = form.find('input[name=additional_rules]').val();

        pluginArray = (JSON.stringify(tagOptions)).replace(/"/g, '&quot;');
        let tagId = form.find('input[name=id]').val();
        let tagHolder = form.find('input[name=holder]').val();

        if (tagId) {

            let token = generate_token();

            $('#' + tagId).parents('.form-group').remove();
            let html = '<div class="form-group row">\n' +
                '                <label class="col-sm-4 col-form-label">' + tagOptions.key + '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' + tagOptions.key + '">\n' +
                '  <i class="fa fa-info"></i>\n' +
                '</button> </label>\n' +
                '                <div class="col-sm-6">\n' +
                '                  <input type="text" readonly name="info[options][tags][' + tagOptions.key + ']" class="form-control" id="' + token + '" value=\'' + pluginArray + '\'>\n' +
                '                </div>\n' +
                '                <div class="col-sm-2">\n' +
                '                <a href="javascript:void(0);" onclick="javascript:editTag(\'' + token + '\')"><i class="fa fa-edit"></i></a> &nbsp;<a href="javascript:void(0);" onclick="javascript:remove($(this))"><i class="fa fa-trash"></i></a>\n' +
                '</div>\n' +
                '              </div>';
            $('#' + tagHolder + ' .tag-content').append(html);

        } else {


            let token = generate_token();

            let html = '<div class="form-group row">\n' +
                '                <label class="col-sm-4 col-form-label">' + tagOptions.key + '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' + tagOptions.key + '">\n' +
                '  <i class="fa fa-info"></i>\n' +
                '</button> </label>\n' +
                '                <div class="col-sm-6">\n' +
                '                  <input type="text" readonly name="info[options][tags][' + tagOptions.key + ']" class="form-control" id="' + token + '" value=\'' + pluginArray + '\'>\n' +
                '                </div>\n' +
                '                <div class="col-sm-2">\n' +
                '                <a href="javascript:void(0);" onclick="javascript:editTag(\'' + token + '\')"><i class="fa fa-edit"></i></a> &nbsp;<a href="javascript:void(0);" onclick="javascript:remove($(this))"><i class="fa fa-trash"></i></a>\n' +
                '</div>\n' +
                '              </div>';
            $('#' + tagHolder + ' .tag-content').append(html);
        }
        $('#tagModal').modal('hide');
    }

    function addnewInput(id) {
        var txt;
        var key = prompt("Write the key ", "");
        if (key == "") {
            alert('Can not be empty');
            addnewInput(id);
        } else if (key !== null) {
            let token = generate_token();
            let name = $('#' + id).data('name') + '[' + key + ']';
            let html = '<div class="form-group row">\n' +
                '                <label class="col-sm-4 col-form-label">' + key + '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' + key + '">\n' +
                '  <i class="fa fa-info"></i>\n' +
                '</button> </label>\n' +
                '                <div class="col-sm-6">\n' +
                '                  <input type="text" name="' + name + '" class="form-control" id="' + token + '" value="">\n' +
                '                </div>\n' +
                '                <div class="col-sm-2">\n' +

                '</div>\n' +
                '              </div>';
            $('#' + id + '> div > div > .tag-content').append(html);
        }

    }


    function addnewTabInput(id) {
        var txt;
        var key = prompt("Write the key ", "");
        if (key == "") {
            alert('Can not be empty');
            addnewInput(id);
        } else if (key !== null) {
            let token = generate_token();
            let name = $('#' + id).data('name') + '[' + key + ']';
            let html = '<div class="form-group row">\n' +
                '                <label class="col-sm-4 col-form-label">' + key + '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' + key + '">\n' +
                '  <i class="fa fa-info"></i>\n' +
                '</button> </label>\n' +
                '                <div class="col-sm-6">\n' +
                '                  <input type="text" name="' + name + '" class="form-control" id="' + token + '" value="">\n' +
                '                </div>\n' +
                '                <div class="col-sm-2">\n' +

                '</div>\n' +
                '              </div>';
            $('#' + id ).append(html);
        }

    }

    function editTag(token) {


        let tagOptions = JSON.parse($('#' + token).val());
        let holder = $('#' + token).parents('.html-group').attr('id');

        let form = $('form.form-holder-ajax-forms');
        form.find('input[name=key]').val(tagOptions.key);
        form.find("input[name=file_type]").prop("checked", false);
        form.find("input[name=file_type]").prop("checked", false);
        form.find("input[name=file_type][value=" + tagOptions.file_type + "]").prop("checked", true);
        form.find("input[name=required]").prop("checked", false);
        form.find("input[name=required]").prop("checked", false);
        form.find("input[name=required][value=" + tagOptions.required + "]").prop("checked", true);
        form.find('input[name=title]').val(tagOptions.title);
        form.find('input[name=extensions]').val(tagOptions.extensions);
        form.find('input[name=min_width]').val(tagOptions.min_width);
        form.find('input[name=max_width]').val(tagOptions.max_width);
        form.find('input[name=min_height]').val(tagOptions.min_height);
        form.find('input[name=max_height]').val(tagOptions.max_height);
        form.find('input[name=width]').val(tagOptions.width);
        form.find('input[name=height]').val(tagOptions.height);
        form.find('input[name=min_filesize]').val(tagOptions.min_filesize);
        form.find('input[name=max_filesize]').val(tagOptions.max_filesize);
        form.find('input[name=max_file_count]').val(tagOptions.max_file_count);
        form.find('input[name=additional_rules]').val(tagOptions.additional_rules);
        form.find('input[name=id]').val(token);
        form.find('input[name=holder]').val(holder);

        if (tagOptions.file_type == 'image' || tagOptions.file_type == 1) {
            $('.imageoptions').show();
        } else {
            $('.imageoptions').hide();
        }
        $('#renderableElement').modal('hide');
        $('#tagModal').modal('show');
    }

    function changeType(type) {
        if (type == 'image' || type == 1) {
            $('.imageoptions').show();
            $('input[name=extensions]').val('JPG,JPEG,PNG,GIF,SVG');
            $('input[name=max_filesize]').val(5120);
        } else {
            $('.imageoptions').hide();
            $('input[name=extensions]').val('PDF,DOC');
            $('input[name=max_filesize]').val(25600);
        }
    }

    $('#tagModal').on('hidden.bs.modal', function () {
        $('#renderableElement').modal('show');
    })



</script>
</body>
</html>
