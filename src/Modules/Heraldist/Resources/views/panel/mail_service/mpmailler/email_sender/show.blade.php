@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.email_detail") !!}</h1>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">{!! trans("MPCorePanel::general.title") !!} : </label>
                            {!! $sended->email_template->mail_title !!} <br/>
                        </div>
                        <div class="col-md-12">
                            <label for="">{!! trans("MPCorePanel::general.subject") !!} : </label>
                            {!! $sended->email_template->mail_subject !!} <br/>
                        </div>
                        <div class="col-md-12">
                            <label for="">{!! trans("MPCorePanel::general.detail") !!} :</label>
                            {!! $mail_body !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="">{!! trans("HeraldistPanel::mail_service.sended_emails") !!}</label>
                    @php $emails = explode(',',$sended->maillist->emails);@endphp
                    @foreach($emails as $email)
                        <p>{!! $email !!}</p>
                    @endforeach
                </div>
            </div>
            <div class="float-right">
                <a href="{!! route('Heraldist.mpmailler.email_senders.delete',$sended->id) !!}" ><button class="btn btn-primary" class="pull-right">{!! trans("MPCorePanel::general.delete") !!}</button></a>
            </div>
    </div>
@endsection
