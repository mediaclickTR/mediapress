<?php

namespace Mediapress\Modules\Heraldist\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\Heraldist\Models\Message;

class UserFormSender extends Mailable
{
    use Queueable, SerializesModels;

    protected $ebulletin;
    protected $sender_email;
    protected $sender_name;
    public $subject;
    public $view;
    public $message;

    public function __construct(Message $message)
    {
        $this->form = $message->form;
        $this->message = $message;

        $this->sender_email = config('mediapress.form_mail.sender_email');
        $this->sender_name = config('mediapress.form_mail.sender_name');

        if (file_exists(resource_path("views/vendor/mail/form_user.blade.php"))) {
            $this->view = "vendor.mail.form_user";
        } else {
            $this->view = 'HeraldistWeb::forms.form_user';
        }
    }

    public function build()
    {
        $data = [
            'sender_email' => $this->sender_email,
            'sender_name' => $this->sender_name,
            'message' => $this->message,
            'view' => $this->view,
            'subject' => langPartAttr('form.user_send.subject', 'Gönderdiğiniz Form Elimize Ulaştı'),
            'title' => langPartAttr('form.user_send.title', "Merhabalar,"),
            'content' => langPartAttr('form.user_send.content', "Az önce gönderdiğiniz mesaj elimize ulaştı. Bize ulaşan mesajın bir kopyasını aşağıda paylaşıyoruz:")
        ];

        $form = $this->form;
        $message = $data['message'];

        return $this
            ->subject($data['subject'])
            ->from($data['sender_email'], $data['sender_name'])
            ->view($data['view'], compact('data', 'message', 'form'));
    }
}
