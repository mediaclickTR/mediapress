<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MailTemplate extends Model
{
    use SoftDeletes;
    protected $table = 'mail_templates';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = [
          "name",
          "mail_title",
          "mail_subject",
          "mail_detail",
          "body"
    ];

    public function websites()
    {
        return $this->belongsToMany(Website::class)->withPivot('mail_template_id');
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
}
