<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class SitemapType extends Model
{
    use SoftDeletes,CacheQueryBuilder;
    public const CONTENT_PANEL_SITEMAP_TYPES = "ContentPanel::sitemap.types.";
    protected $table = 'sitemap_types';
    protected $dates = ['deleted_at'];

    public $timestamps = true;

    protected $primaryKey = 'id';
    protected $fillable = ['name', 'controller', 'external_controller','external_required','sitemap_type_type', 'created_at', 'updated_at'];

    public function translatedName(){
        return trans(self::CONTENT_PANEL_SITEMAP_TYPES . $this->attributes['name']) === self::CONTENT_PANEL_SITEMAP_TYPES . $this->attributes['name'] ? $this->attributes['name'] : trans(self::CONTENT_PANEL_SITEMAP_TYPES . $this->attributes['name']);
    }

}
