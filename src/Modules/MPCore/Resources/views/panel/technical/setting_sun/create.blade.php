@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans('MPCorePanel::setting_sun.create-setting') !!}</h1>
        <div class="col-md-12">
            <form action="{!! route('MPCore.technical.setting_sun.store') !!}" novalidate method="POST">
               @include("MPCorePanel::technical.setting_sun.form")
            </form>
        </div>
    </div>
@endsection