<?php

    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 14.11.2018
     * Time: 17:00
     */

    namespace Mediapress\Modules\MPCore\FormBuilder\Renderables;

    use Mediapress\Facades\DataSourceEngine;
    use Mediapress\AllBuilder\Foundation\BuilderRenderable;

    class ManageCountryGroups extends BuilderRenderable
    {

        public const STYLE = "style";
        public const TYPE = "type";
        public const PARAMS = "params";
        public const CONTENTS = "contents";
        public const BLANK = "blank";
        public const TAG = "tag";
        public const ATTRIBUTES = "attributes";
        public const CLASS1 = "class";
        public const OPTIONS = "options";
        public const IGNORED_IF = "ignored_if";
        public const DATA = "data";
        public const FAICON = "faicon";
        public const ICONNAME = "iconname";
        public const SIZE_X = "size-x";
        public const BUTTON = "button";
        public const MARGIN_LEFT_5_PX = "margin-left:5px;";
        public const VALUE = "value";
        public const INPUT = "input";
        public const HIDDEN = "hidden";

        public function defaultContents()
        {
            extract($this->params);

            $delete_img = asset("vendor/mediapress/".'images/sure-to-delete.gif');

            $owner_types_only = $request_params["ownerTypesOnly"] ?? [];

            $owner_type = $request_params["selectedOwnerType"] ?? null;
            $owner_id = $request_params["selectedOwnerId"] ?? null;

            /*$owner_type = "Mediapress\\Modules\\Content\\Website";
            $owner_id = 1;*/

            $can_change_owner = !(isset($request_params["changesOwner"]) && $request_params["changesOwner"] == "no");


            $selectable_owners = DataSourceEngine::getDataSource("MPCore", "CountryGroupOwnersDS", ["ownerTypesOnly" => $owner_types_only])->getData() ?? [];

            $form_ignored_if = false;
            $form_ignorance_text = [];
            if (!$owner_type && !$can_change_owner) {
                $form_ignored_if = true;
                $form_ignorance_text[] = "<ul>";
                $form_ignorance_text[] = "<li><abbr title=\"Ülke listesinin sahibi olan veri nesnesi\">Sahip nesne</abbr> belirtilmemiş</li>";
                $form_ignorance_text[] = "<li><abbr title=\"Ülke listesinin sahibi olan veri nesnesi\">Sahip nesne</abbr> değiştirme izni yok.</li>";
                if (!count($selectable_owners)) {
                    $form_ignorance_text[] = "<li>Değiştirme izni olsaydı bile seçilebilecek <abbr title=\"Ülke listesinin sahibi olan veri nesnesi\">sahip</abbr> yok.</li>";
                }
                $form_ignorance_text[] = "</ul>";
            } elseif (!count($selectable_owners) && !$owner_type) {
                $form_ignored_if = true;
                $form_ignorance_text[] = "<ul>";
                $form_ignorance_text[] = "<li><abbr title=\"Ülke listesinin sahibi olan veri nesnesi\">Sahip nesne</abbr> belirtilmemiş</li>";
                $form_ignorance_text[] = "<li>Seçilebilecek <abbr title=\"Ülke listesinin sahibi olan veri nesnesi\">sahip nesne</abbr> yok.</li>";
                $form_ignorance_text[] = "</ul>";
            }

            $form_ignorance_text = implode("\n", $form_ignorance_text);

            $ownersGroupsRoute = url(route("country_groups.ownersGroups"));
            $groupsCountriesRoute = url(route("country_groups.groupsCountries"));
            $groupRemoveRoute = url(route("country_groups.remove"));


            $renderables_script = <<< "SCRIPT"
                    $('document').ready(function(){
                        $('.transfer-selector').lwMultiSelect({
                            addAllText: "Tümü",
                            removeAllText: "Hiçbiri",
                            selectedLabel: "Seçilenler",
                        });
                        $('#selectOwner').change(function(e){
                            var data = $(this).val();
                            
                        //alert("-"+data+"-");
                            var pieces = data.split("-");
                            if(pieces.length==2){
                                $("#cg_owner").val(pieces[0]);
                                $("#cg_owner_id").val(pieces[1]);
                            }else{
                                $("#cg_owner").val("");
                                $("#cg_owner_id").val("");
                            }
                            cgOwnerChanged();
                            
                        });
                        $("html").delegate(".lwms-main","mouseenter",function(){
                            $(".lwms-main").closest(".form-group").addClass("focus");
                        });
                        $("html").delegate(".lwms-main","mouseleave",function(){
                            $(".lwms-main").closest(".form-group.focus").removeClass("focus");
                        });
                        $('button#save_button').click(function(){saveCountryGroup()});
                        $('button#add_cg').click(function(){newCountryGroup()});
                        $('button#remove_cg').click(function(){removeCountryGroup()});
                        
                        $("#cancel_button").click(function(){
                            cancelEdit();
                        });
                        
                            cgOwnerChanged();
                    });

SCRIPT;


            $renderables_script .= <<< "SCRIPTFUNCS"
            
                    function ungroupedCountriesOfOwner(){
                        
                        var owner = $("#cg_owner").val();
                        var owner_id = $("#cg_owner_id").val();
                        var csrf = $("meta[name='csrf-token']").attr('content');
                        var data_ = {"owner":owner, "owner_id":owner_id,"token_":csrf}; 
                        
                        /*if(owner == "" || owner_id==""){
                            $("#cg-form-controls").hide();
                        }else{
                            $("#add_cg").show();
                            $("#cg-form-controls").show();
                        }*/
                        var countries_found = 0;
                        
                        $.ajax({
                            type:"GET",
                            url:"$ownersGroupsRoute",
                            data:data_,
                            dataType:"json",
                            beforeSend:function(){},
                            success:function(response){
                                //console.log(response);
                                $.each(response,function(ind,model){
                                    groups_found++;
                                      var li = $('<li onclick="javascript:groupChanged(this);" ' +
                                       'class="list-group-item" ' +
                                       'data-group-name="'+model.title+'" ' +
                                        'data-group-id="'+model.id+'">' +
                                       '<span>'+model.title+'</span>' +
                                        '<i class="fa-chevron-right fa" style="margin-left:5px;"></i>' +
                                         '</li>');
                                      $('#country-groups').append(li);
                                });
                                
                                if(groups_found==0){
                                    $("#country-groups").hide();
                                    $("#cg-placeholder").html("Ülke grubu yok. Yeni bir tane ekleyin.").show();
                                    $("#cg-form-editor").hide();
                                }else{
                                    $("#cg-placeholder").html("").hide();
                                    $("#country-groups").show();
                                }
                                
                                if(select_cg_id){
                                    sel = $('#country-groups li[data-group-id="'+select_cg_id+'"]');
                                    if(sel.length){sel.click();}
                                }
                            },
                            error:function(jqXHR,textStatus,errorThrown ){
                                new swal("Veri Alınamıyor",textStatus.toUpperCase() +": "+errorThrown,'error');
                            },
                            complete:function(){
                                
                            }
                        });
                        
                    }
            
                    function cgOwnerChanged(select_cg_id){
                         clearGroupsList();
                         $("#cg-form-editor").hide();
                        //pull cgroups which belongs to selected owner
                        
                        var owner = $("#cg_owner").val();
                        var owner_id = $("#cg_owner_id").val();
                        var csrf = $("meta[name='csrf-token']").attr('content');
                        var data_ = {"owner":owner, "owner_id":owner_id,"token_":csrf}; 
                        
                        if(owner == "" || owner_id==""){
                            $("#cg-form-controls").hide();
                        }else{
                            $("#add_cg").show();
                            $("#cg-form-controls").show();
                        }
                        
                        var groups_found = 0;
                        
                        $.ajax({
                            type:"GET",
                            url:"$ownersGroupsRoute",
                            data:data_,
                            dataType:"json",
                            beforeSend:function(){},
                            success:function(response){
                                //console.log(response);
                                $.each(response,function(ind,model){
                                    groups_found++;
                                      var li = $('<li onclick="javascript:groupChanged(this);" ' +
                                       'class="list-group-item" ' +
                                       'data-group-name="'+model.title+'" ' +
                                        'data-group-id="'+model.id+'">' +
                                       '<span>'+model.title+'</span>' +
                                        '<i class="fa-chevron-right fa" style="margin-left:5px;"></i>' +
                                         '</li>');
                                      $('#country-groups').append(li);
                                });
                                
                                if(groups_found==0){
                                    $("#country-groups").hide();
                                    $("#cg-placeholder").html("Ülke grubu yok. Yeni bir tane ekleyin.").show();
                                    $("#cg-form-editor").hide();
                                }else{
                                    $("#cg-placeholder").html("").hide();
                                    $("#country-groups").show();
                                }
                                
                                if(select_cg_id){
                                    sel = $('#country-groups li[data-group-id="'+select_cg_id+'"]');
                                    if(sel.length){sel.click();}
                                }
                            },
                            error:function(jqXHR,textStatus,errorThrown ){
                                new swal("Veri Alınamıyor",textStatus.toUpperCase() +": "+errorThrown,'error');
                            },
                            complete:function(){
                                
                            }
                        });
                    }
                    
                    function groupChanged(li){
                        
                        clearDetails();
                        $("#cg_id").val("");
                        
                        $('.country-groups li').removeClass("active");
                        li = $(li);
                        li.addClass("active");
                        $("#cg-form-editor").show();
                        $("#remove_cg").show();
                        
                        var csrf = $("meta[name='csrf-token']").attr('content');
                        var group_name = li.data("group-name");
                        
                        let group_id = li.data("group-id");
                        
                        //alert(li.data("group-id"));
                        $("#cg_id").val(group_id);
                        $("#group_name").val(group_name).closest("div").addClass("focus");
                        
                        /*var owner = $("#cg_owner").val();
                        var owner_id = $("#cg_owner_id").val();*/
                        var data_ = {/*"owner":owner, "owner_id":owner_id,*/ "group_id":group_id, "token_":csrf}; 
                        
                         $.ajax({
                            type:"GET",
                            url:"$groupsCountriesRoute",
                            data:data_,
                            dataType:"json",
                            beforeSend:function(){},
                            success:function(response){
                           //     console.log(response);
                                $.each(response,function(ind,el){
                                    $('#countries option[value="'+el+'"]').prop("selected",true);
                                });
                                $('#countries').data('plugin_lwMultiSelect').updateList();
                            },
                            error:function(jqXHR,textStatus,errorThrown ){
                                new swal("Veri Alınamıyor",textStatus.toUpperCase() +": "+errorThrown,'error');
                            },
                            complete:function(){
                                $('#countries').data('plugin_lwMultiSelect').updateList();
                            }
                        });
                        
                        return false;
                    }
                    
                    function clearGroupsList(){
                        $("#country-groups li").remove();
                        $("#country-groups").hide();
                        $("#cg_id").val("");
                        $("#remove_cg").hide();
                        clearDetails();
                    }
                    
                    function clearDetails(){
                        var data = $('#countries').data('plugin_lwMultiSelect');
                        if(data!=undefined){
                            $('#countries').data('plugin_lwMultiSelect').removeAll();
                        }
                        $("#group_name").val("").closest("div.focus").removeClass("focus");
                    }
                    
                    function newCountryGroup(){
                        clearDetails();
                        $("#cg_id").val("new");
                        $("#cg-form-editor").show();
                        
                        $("#country-groups").hide();
                        $("#add_cg,#remove_cg").hide();
                        $("#cg-placeholder").html("Yeni bir ülke grubu ekliyorsunuz.").show();
                        
                        $("#cancel_button").show();
                    }
                    
                    function removeCountryGroup(){
                        
                        var group_id = $("#cg_id").val();
                        var group_name = $("#group_name").val();
                        
                        var csrf = $("meta[name='csrf-token']").attr('content');
                        
                        if(group_id =="" || group_id=="new"){
                            new swal("Seçili grup yok.", "Önce silmek istediğiniz grubu 'Ülke Grupları' listesinden seçin",'error');
                            return false;
                        }
                        
                        var data_ = {"group_id":group_id, "_token":csrf}; 
                        
                        swal({
                          title: "Emin misiniz?",
                          text: "Grup silinecek: "+group_name,
                          type: "warning",
                          showIcon:false,
                          showCancelButton: true,
                          confirmButtonColor: '#666',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Evet',
                          cancelButtonText: 'Hayır',
                          imageUrl: "$delete_img",
                          imageWidth: 200,
                          imageHeight: 200,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                            $.ajax({
                                type:"POST",
                                url:"$groupRemoveRoute",
                                data:data_,
                                dataType:"json",
                                success:function(response){
                                    //  console.log(response);
                                    if(response.status=="success"){
                                        cgOwnerChanged();
                                        new swal("Grup Silindi!", "",'success');
                                    }else if(response.status=="fail"){
                                        new swal("Grup Silinemedi!", response.fail_message,'error');
                                    }
                                },
                                error:function(jqXHR,textStatus,errorThrown ){
                                    new swal("Veri Alınamıyor",textStatus.toUpperCase() +": "+errorThrown,'error');
                                },
                            });
                          }
                        });
                        
                        
                        
                        
                    }
                    
                    function cancelEdit(){
                        clearDetails();
                        
                        $("#cg-form-editor").hide();
                        val = $("#cg_id").val();
                        $("#cg_id").val("");
                        if(val!="" && val!="new"){
                            last_sel = $('#country-groups li[data-group-id="'+val+'"]');
                            if(last_sel.length){last_sel.click();}
                        }else if(val=="new"){
                            $("#add_cg").show();
                            if($('#country-groups li').length){
                                $("#country-groups").show();
                                $("#cg-placeholder").html("").hide();
                                $('#country-groups li.active').click();
                            }else{
                                $("#cg-placeholder").html("Ülke grubu yok. Yeni bir tane ekleyin.").show();                                
                            }
                        }
                    }
                    
                    function saveCountryGroup(){
                        var owner = $("#cg_owner").val();
                        var owner_id = $("#cg_owner_id").val();
                        
                        $.ajax({
                          type: $("form#country_group_form").attr("method"),
                          url: $("form#country_group_form").attr("action"),
                          data: $("form#country_group_form").serialize(), 
                          dataType:"json",
                          //or your custom data either as object {foo: "bar", ...} or foo=bar&...
                          success: function(response) {
                              if(response.status=="success"){
                                  var id = response.model.id;
                                  cgOwnerChanged(id);
                              }
                          },
                            error:function(jqXHR,textStatus,errorThrown ){
                                new swal("Hata oluştu",textStatus.toUpperCase() +": "+errorThrown,'error');
                            }
                        });
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                
SCRIPTFUNCS;

            $renderables_script = '<script type="text/javascript">' . $renderables_script . "</script>\n";


            return [
                self::STYLE => [
                    self::TYPE => self::BLANK,
                    self::PARAMS => ["html" => [self::TAG => self::STYLE]],
                    self::CONTENTS => [
                        ".lwms-cf { display: block; }
                        .lwms-main { width: 100%; /*margin-top:22px;*/}
                        .lwms-main .lwms-left, .lwms-main .lwms-right { width: 47%; }
                        .lwms-main ul.lwms-list { height: 400px; }
                        .lwms-main .lwms-left { margin-right: 6%; }
                        .country-groups > li { cursor:pointer; }
                        ul.country-groups>li.list-group-item{ border-radius:0; }",
                    ]
                ],
                "bosluk" => [
                    self::TYPE => self::BLANK,
                    self::PARAMS => [
                        "html" => [
                            self::TAG => "div",
                            self::ATTRIBUTES => [
                                self::STYLE => "margin-top:20px; margin-bottom:20px;"
                            ]
                        ]
                    ]
                ],
                "cannot_use_editor" => [
                    self::TYPE => self::BLANK,
                    self::PARAMS => [
                        "html" => [
                            self::TAG => "div",
                            self::ATTRIBUTES => [
                                self::CLASS1 => "alert alert-warning"
                            ],
                        ]
                    ],
                    self::OPTIONS => [
                        self::IGNORED_IF => !$form_ignored_if
                    ],
                    self::CONTENTS => [
                        "<p><b>İşlem yapabilmek için gerekli koşullar sağlanmadı.</b></p>",
                        "<br/>",
                        $form_ignorance_text
                    ]
                ],


                "form" => [
                    self::TYPE => "form",
                    self::PARAMS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                "id" => "country_group_form",
                                "method" => "POST",
                                "action" => url(route("country_groups.save")),
                                self::CLASS1 => ""
                            ]
                        ]
                    ],
                    self::DATA => [
                        "stacks" => [
                            "scripts" => $renderables_script
                        ]
                    ],
                    self::OPTIONS => [
                        self::IGNORED_IF => $form_ignored_if
                    ],
                    self::CONTENTS => [
                        "ownerselection" => [
                            self::TYPE => "selectwithlabel",
                            self::PARAMS => [
                                "owner_type" => $owner_type,
                                "owner_id" => $owner_id,
                                self::VALUE => "<print>owner_type</print>-<print>owner_id</print>",
                                "html" => [
                                    self::ATTRIBUTES => function () use ($can_change_owner) {
                                        $return = ["name" => "owner"];
                                        if (!$can_change_owner) {
                                            $return["disabled"] = "disabled";
                                        } else {
                                            $return["class"] = "nice";
                                        }
                                        $return["id"] = "selectOwner";
                                        return $return;
                                    }
                                ]
                            ],
                            self::OPTIONS => [
                                self::IGNORED_IF => !$can_change_owner,
                                "show_default_option" => true
                            ],
                            self::DATA => [
                                "values" => function () use ($selectable_owners, $owner_type, $owner_id, $can_change_owner) {

                                    $results = [];
                                    if ($can_change_owner) {
                                        foreach ($selectable_owners as $sa) {
                                            $rekey = $sa->owner_type . "-" . $sa->owner_id;
                                            $results[$rekey] = $rekey;
                                        }
                                    }
                                    return $results;
                                }
                            ]
                        ],
                        "owner" => [
                            self::TYPE => self::INPUT,
                            self::PARAMS => [
                                "html" => [
                                    self::ATTRIBUTES => [
                                        "name" => "cg_owner",
                                        "id" => "cg_owner",
                                        self::TYPE => self::HIDDEN,
                                        self::VALUE => $owner_type
                                    ]
                                ]
                            ]
                        ],
                        "owner_id" => [
                            self::TYPE => self::INPUT,
                            self::PARAMS => [
                                "html" => [
                                    self::ATTRIBUTES => [
                                        "name" => "cg_owner_id",
                                        "id" => "cg_owner_id",
                                        self::TYPE => self::HIDDEN,
                                        self::VALUE => $owner_id
                                    ]
                                ]
                            ]
                        ],
                        "selected_cg_id" => [
                            self::TYPE => self::INPUT,
                            self::PARAMS => [
                                "html" => [
                                    self::ATTRIBUTES => [
                                        "name" => "cg_id",
                                        "id" => "cg_id",
                                        self::TYPE => self::HIDDEN,
                                        self::VALUE => ""
                                    ]
                                ]
                            ]
                        ],
                        "row" => [
                            self::TYPE => self::BLANK,
                            self::PARAMS => [
                                "html" => [
                                    self::TAG => "div",
                                    self::ATTRIBUTES => [
                                        self::CLASS1 => "row",
                                        "id" => "cg-form-controls"
                                    ]
                                ]
                            ],
                            self::CONTENTS => [
                                "left_panel" => [
                                    self::TYPE => self::BLANK,
                                    self::PARAMS => [
                                        "html" => [
                                            self::TAG => "div",
                                            self::ATTRIBUTES => [
                                                self::CLASS1 => "mc-md-4"
                                            ]
                                        ]
                                    ],
                                    self::CONTENTS => [
                                        "placeholder" => [
                                            self::TYPE => self::BLANK,
                                            self::PARAMS => [
                                                "html" => [
                                                    self::TAG => "div",
                                                    self::ATTRIBUTES => [
                                                        "id" => "cg-placeholder",
                                                        self::CLASS1 => "alert alert-info",
                                                        self::STYLE => "display:none;"
                                                    ]
                                                ]
                                            ],
                                            self::CONTENTS => [
                                                ""
                                            ]
                                        ],
                                        "groups_list" => [
                                            self::TYPE => "ul",
                                            self::PARAMS => [
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        self::CLASS1 => "list-group country-groups",
                                                        "id" => "country-groups",
                                                        self::STYLE => "margin-top:22px; min-height: 478px; border:1px solid #ccc;background-color: #fff;"
                                                    ]
                                                ]
                                            ],
                                            self::CONTENTS => [
                                                [
                                                    self::TYPE => "li",
                                                    self::PARAMS => [
                                                        "html" => [
                                                            self::ATTRIBUTES => [
                                                                self::CLASS1 => "list-group-item active",
                                                                "onclick" => "javascript:groupChanged(this);",
                                                                "data-group-name" => "Kuzey Amerika",
                                                                "data-group-id" => "1"
                                                            ]
                                                        ]
                                                    ],
                                                    self::CONTENTS => [
                                                        "span" => [
                                                            self::TYPE => self::BLANK,
                                                            self::PARAMS => [
                                                                "html" => [
                                                                    self::TAG => "span",
                                                                ]
                                                            ],
                                                            self::CONTENTS => [
                                                                "Kuzey Amerika",
                                                                [
                                                                    self::TYPE => self::FAICON,
                                                                    self::PARAMS => [
                                                                        "html" => [
                                                                            self::ATTRIBUTES => [
                                                                                self::STYLE => self::MARGIN_LEFT_5_PX
                                                                            ]
                                                                        ]
                                                                    ],
                                                                    self::OPTIONS => [
                                                                        self::ICONNAME => "chevron-right",
                                                                        self::SIZE_X => 1,
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]

                                                ],
                                            ]
                                        ],
                                        "groups_toolbar" => [
                                            self::TYPE => self::BLANK,
                                            self::PARAMS => [
                                                "html" => [
                                                    self::TAG => "div",
                                                    self::ATTRIBUTES => [
                                                        self::CLASS1 => "btn-group",
                                                        "role" => "group",
                                                        "aria-label" => "Ülke Grupları Araç Çubuğu",
                                                        self::STYLE => "margin-top:10px;"
                                                    ]
                                                ]
                                            ],
                                            self::CONTENTS => [
                                                [
                                                    self::TYPE => self::BUTTON,
                                                    self::PARAMS => [
                                                        "html" => [
                                                            self::ATTRIBUTES => [
                                                                self::TYPE => self::BUTTON,
                                                                self::CLASS1 => "btn mp-btn-primary",
                                                                "id" => "add_cg"
                                                            ]
                                                        ]
                                                    ],
                                                    self::CONTENTS => [
                                                        [
                                                            self::TYPE => self::FAICON,
                                                            self::PARAMS => [
                                                                "html" => [
                                                                    self::ATTRIBUTES => [
                                                                        self::STYLE => self::MARGIN_LEFT_5_PX
                                                                    ]
                                                                ]
                                                            ],
                                                            self::OPTIONS => [
                                                                self::ICONNAME => "plus",
                                                                self::SIZE_X => 1,
                                                            ]
                                                        ],
                                                        "Ekle"
                                                    ]
                                                ],
                                                [
                                                    self::TYPE => self::BUTTON,
                                                    self::PARAMS => [
                                                        "html" => [
                                                            self::ATTRIBUTES => [
                                                                self::TYPE => self::BUTTON,
                                                                self::CLASS1 => "btn mp-btn-danger",
                                                                "id" => "remove_cg"
                                                            ]
                                                        ]
                                                    ],
                                                    self::CONTENTS => [
                                                        [
                                                            self::TYPE => self::FAICON,
                                                            self::PARAMS => [
                                                                "html" => [
                                                                    self::ATTRIBUTES => [
                                                                        self::STYLE => self::MARGIN_LEFT_5_PX
                                                                    ]
                                                                ]
                                                            ],
                                                            self::OPTIONS => [
                                                                self::ICONNAME => "trash-o",
                                                                self::SIZE_X => 1,
                                                            ]
                                                        ],
                                                        "Sil"
                                                    ]
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                "right_panel" => [
                                    self::TYPE => self::BLANK,
                                    self::PARAMS => [
                                        "html" => [
                                            self::TAG => "div",
                                            self::ATTRIBUTES => [
                                                self::CLASS1 => "mc-md-8",
                                                "id" => "cg-form-editor"
                                            ]
                                        ]
                                    ],
                                    self::CONTENTS => [
                                        "renamerow" => [
                                            self::TYPE => "inputwithlabel",
                                            self::PARAMS => [
                                                "title" => "Grup Adı",
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        "name" => "group_name",
                                                        self::CLASS1 => "",
                                                        "read-only" => "read-only",
                                                        "id" => "group_name"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        "transferbox" => [
                                            self::TYPE => "selectwithlabel",
                                            self::PARAMS => [
                                                "title" => "",//Ülkeleri Seçin
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        "multiple" => "multiple",
                                                        "name" => "countries[]",
                                                        self::CLASS1 => "transfer-selector",
                                                        "id" => "countries"
                                                    ]
                                                ]
                                            ],
                                            self::OPTIONS => [
                                                "show_default_option" => false
                                            ],
                                            self::DATA => [
                                                "values" => [
                                                    self::TYPE => "ds:MPCore->CountriesDS",
                                                    self::PARAMS => [
                                                        "pluck" => true,
                                                        "pluck_key" => "code",
                                                        "pluck_column" => "en",
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            self::TYPE => self::BUTTON,
                                            self::PARAMS => [
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        self::TYPE => self::BUTTON,
                                                        self::CLASS1 => "btn mp-btn-danger btn-cancel-cg-edit pull-right",
                                                        self::STYLE => "margin-right:15px;",
                                                        "id" => "cancel_button"
                                                    ]
                                                ],

                                            ],
                                            self::CONTENTS => [
                                                "saveicon" => [
                                                    self::TYPE => self::FAICON,
                                                    self::OPTIONS => [
                                                        self::ICONNAME => "abort",
                                                        self::SIZE_X => 1,
                                                    ]
                                                ],
                                                "caption" => "İptal"
                                            ]

                                        ],
                                        [
                                            self::TYPE => self::BUTTON,
                                            self::PARAMS => [
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        self::TYPE => self::BUTTON,
                                                        self::CLASS1 => "btn mp-btn-primary btn-save-cg pull-right",
                                                        self::STYLE => "margin-right:15px;",
                                                        "id" => "save_button"
                                                    ]
                                                ],

                                            ],
                                            self::CONTENTS => [
                                                "saveicon" => [
                                                    self::TYPE => self::FAICON,
                                                    self::OPTIONS => [
                                                        self::ICONNAME => "save",
                                                        self::SIZE_X => 1,
                                                    ]
                                                ],
                                                "caption" => "Kaydet"
                                            ]

                                        ]
                                    ]
                                ],
                                "bottom_panel" => [
                                    self::TYPE => self::BLANK,
                                    self::PARAMS => [
                                        "html" => [
                                            self::ATTRIBUTES => [
                                                self::CLASS1 => "mc-xs-12"
                                            ]
                                        ]
                                    ],
                                    self::CONTENTS => [


                                    ]
                                ],
                                "clearfix" => [
                                    self::TYPE => self::BLANK,
                                    self::PARAMS => [
                                        "html" => [
                                            self::TAG => "html",
                                            self::ATTRIBUTES => [
                                                self::CLASS1 => "clearfix"
                                            ]
                                        ],

                                    ]
                                ]
                            ]
                        ]
                    ]
                ]

            ];

        }
    }