<?php

namespace Mediapress\Modules\Interaction\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;

class Device extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'devices';
    protected $fillable = [
        "id",
        "name",
    ];
    public $timestamps = false;


    public static function boot()
    {

        parent::boot();


        static::creating(function ($model) {

            $model->created_at = Carbon::now();

        });
    }
}
