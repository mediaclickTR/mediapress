<?php

return [
    'modules'=>' Modüller',
    'create'=>' Modül Ekle',
    'edit'=>' Modül Düzenle',
    'name'=>' Modül Adı',
    'namespace'=>' Modül Namespace',
    'status'=>'Yayın Durumu',
    'slot'=>'Slot Seçimi',
    'slot.add'=>'Hızlı Slot Ekle',
    'slot.name'=>'Slot Adı',
    'slot.description'=>'Slot Açıklama',
];