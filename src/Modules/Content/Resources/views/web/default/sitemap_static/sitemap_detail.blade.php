@php
    $sitemap = $mediapress->data['sitemap'];
@endphp

<div class="container">
    <h1 class="my-4">{!! $sitemap->detail->name !!}</h1>
    @include("ContentWeb::default.layouts.breadcrumb")

    <div class="row">
        <div class="col-md-8">
            <h3 class="my-3">{!! $sitemap->detail->name !!}</h3>
            {!! $sitemap->detail->detail !!}
        </div>

        <div class="col-md-4">
            @if($sitemap->f_)
                @if(is_a($sitemap->f_, 'Illuminate\Database\Eloquent\Collection'))
                    <img class="img-fluid" src="{!! $sitemap->f_->first() !!}" alt="">
                @else
                    <img class="card-img-top img-fluid" src="{!! $sitemap->f_ !!}" alt="">
                @endif
            @else
                <img class="card-img-top img-fluid" src="http://placehold.it/380x230" alt="">
            @endif
        </div>

    </div>
</div>
