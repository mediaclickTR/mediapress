<?php

namespace Mediapress\Modules\Auth;

use Mediapress\Models\MPModule;
use App;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

Class Auth extends MPModule
{
    public $name = "Auth";
    public $url = "mp-admin/Auth";
    public $description = "Authorization module of Mediapress";
    public $author = "";
    private $plugins = [];

    /**
     * @return mixed
     */
    public function user()
    {
        return session("panel.user");
    }

    public function fillMenu($menu)
    {

        #region Header Menu > Settings > Panel Users > Set

        // Tab Name Set
        data_set($menu, 'header_menu.settings.cols.admins.name', trans("AuthPanel::menu_titles.admins"));

        $settings_cols_panel_users_rows_set = [
            [
                "type" => "submenu",
                "title" => trans("AuthPanel::menu_titles.admin_accounts"),
                "url" => route("Auth.admins.index")
            ],
            [
                "type" => "submenu",
                "title" => trans("AuthPanel::menu_titles.roles"),
                "url" => route("Auth.roles.index")
            ]
        ];

        return dataGetAndMerge($menu, 'header_menu.settings.cols.admins.rows',$settings_cols_panel_users_rows_set);
        #endregion



        $datasets_cols_data_rows_set = [];
        if(auth()->user()->role_id != 1) {
            $datasets_cols_data_rows_set = [
                [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans('MPCorePanel::general.profile'),
                    self::URL => route('Auth.profile.index'),
                    'target'=> "_self"
                ],
            ];
        }

    }
}
