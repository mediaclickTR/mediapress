<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Modules\Locale\Models\Province;
use Mediapress\Support\Database\CacheQueryBuilder;

class Country extends Model
{
    use CacheQueryBuilder;
    protected $table = 'countries';
    protected $connection = 'mysql';

    public $timestamps = false;

    protected $primaryKey = 'id';
    protected $fillable = ["code","native", "tr", "en"];


    public function countryGroups(){
        return $this->belongsToMany(CountryGroup::class,'country_group_country','country_code','country_group_id','code','id');
    }

    public function provinces() {
        return $this->hasMany(Province::class, 'country_id');
    }
}
