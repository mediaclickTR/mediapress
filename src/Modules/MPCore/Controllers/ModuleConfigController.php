<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;

use Mediapress\Modules\MPCore\Models\ModuleConfig;
use Mediapress\Modules\MPCore\Models\Module;
use Mediapress\Foundation\MPCache;

class ModuleConfigController extends Controller
{
    #region crud
    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE_MODULE_CONFIGS_CREATE = 'message.module_configs.create';
    public const MODULES = 'modules';
    public const MP_CORE_MODULE_CONFIGS_INDEX = 'MPCore.module_configs.index';

    public function index()
    {
        if(!userAction('message.module_configs.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $module_configs = ModuleConfig::paginate(25);
        return view('MPCorePanel::module_configs.index', compact('module_configs', self::MODULES));
    }

    public function create()
    {
        if(!userAction(self::MESSAGE_MODULE_CONFIGS_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $modules = \Mediapress\Models\Module::all();
        return view('MPCorePanel::module_configs.create', compact(self::MODULES));
    }

    public function edit($id)
    {
        $module_config = ModuleConfig::find($id);
        $modules = \Mediapress\Models\Module::all();
        return view('MPCorePanel::module_configs.edit', compact('module_config', self::MODULES));
    }

    public function store(Request $request)
    {
        if(!userAction(self::MESSAGE_MODULE_CONFIGS_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $data = request()->except(['_token']);

        $insert = ModuleConfig::insert($data);

        return redirect(route(self::MP_CORE_MODULE_CONFIGS_INDEX));
    }

    public function update(Request $request)
    {
        $data = request()->except(["_token"]);
        $update=ModuleConfig::where('id',$request->id)->update($data);
        return redirect(route(self::MP_CORE_MODULE_CONFIGS_INDEX));
    }

    public function delete($id)
    {
        if(!userAction('message.module_configs.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $module = ModuleConfig::find($id);

        if ($module){
            $module->delete();
        }

        return redirect(route(self::MP_CORE_MODULE_CONFIGS_INDEX));
    }
    #endregion

    private function deleteListRecursive($id, $bool = false)
    {
        dd($id);
    }

}