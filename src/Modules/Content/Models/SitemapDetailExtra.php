<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class SitemapDetailExtra extends Model {

    use CacheQueryBuilder;
    protected $table = 'sitemap_detail_extras';

    public $timestamps=false;

    protected $fillable = ["sitemap_detail_id","key", "value"];

    public function sitemapDetail()
    {
        return $this->belongsTo(SitemapDetail::class);
    }
    public function parent()
    {
        return $this->sitemapDetail->parent();
    }

    public function getValueAttribute($value){

        $sitemapId = optional($this->parent)->id;
        $websiteId = optional($this->parent->website)->id;
        $permissions = [
            "content.websites.website".$websiteId.".sitemaps.sitemap".$sitemapId.".main_update",
            "content.websites.website".$websiteId.".sitemaps.main_update",
            "content.websites.sitemaps.main_update",
        ];

        if (!activeUserCan($permissions)) {
            return $value;
        }
        return supFront($value,'value',$this);
    }
}
