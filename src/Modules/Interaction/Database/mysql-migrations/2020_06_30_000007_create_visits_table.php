<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitsTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('visits'))
            return;

		Schema::connection('mediapress_interaction')->create('visits', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cookie_id')->index('cookie_id');
            $table->unsignedBigInteger('session_id')->index('session_id');
            $table->unsignedInteger('url_id');
            $table->string('ip');
            $table->text('query_string')->nullable();
            $table->text('referer')->nullable();
            $table->string('hour')->nullable();
            $table->string('week')->nullable();
            $table->timestamp('created_at')->index('created_at')->nullable();
		});
	}

	public function down()
	{
		Schema::connection('mediapress_interaction')->dropIfExists('visits');
	}
}
