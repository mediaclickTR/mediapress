@csrf
@if(isset($maillist))
    <input type="hidden" name="id" value="{!! $maillist->id !!}">
@endif

<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.maillist_name") !!}</label>
    <input type="text" name="name" value="{{ old('name',  isset($maillist->name) ? $maillist->name : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.sender_name") !!}</label>
    <input type="text" name="from_name" value="{{ old('name',  isset($maillist->campaign_defaults->from_name) ? $maillist->campaign_defaults->from_name : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.sender_email") !!}</label>
    <input type="email" name="from_email" value="{{ old('name',  isset($maillist->campaign_defaults->from_email) ? $maillist->campaign_defaults->from_email : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("MPCorePanel::general.subject") !!}</label>
    <input type="text" name="subject" value="{{ old('name',  isset($maillist->campaign_defaults->subject) ? $maillist->campaign_defaults->subject : null) }}">
</div>
<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>