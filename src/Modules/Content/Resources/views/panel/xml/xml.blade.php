@php echo '<'.'?x'.'ml version="1.0" encoding="UTF-8"?>'; @endphp

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
<!-- created with Mediapress Sitemap Generator www.mediaclick.com.tr -->
@foreach($rows as $row)
        <url>
            <loc>{!! url(str_replace('&','&amp;',$row["link"])) !!}</loc>
@if(isset($row["alternates"]) && count($row["alternates"]))
@foreach($row["alternates"] as $key => $value)
            <xhtml:link
                rel="alternate"
                hreflang="{!! $key !!}"
                href="{!! str_replace('&','&amp;',$value) !!}"/>
@endforeach
@endif
            <changefreq>{!! $row["change_frequency"] !!}</changefreq>
            <priority>{!! $row["priority"]+0 !!}</priority>
        </url>
@endforeach
</urlset>
