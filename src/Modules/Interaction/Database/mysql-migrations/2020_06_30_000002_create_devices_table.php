<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('devices'))
            return;

		Schema::connection('mediapress_interaction')->create('devices', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('devices');
	}
}
