<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesTable extends Migration {

    public const MODULES = 'modules';

    public function up()
    {

        if (!Schema::hasTable(self::MODULES)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::MODULES, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('namespace');
                $table->integer('slot_id')->nullable();
                $table->integer('module_id')->nullable();
                $table->tinyInteger('status')->default('0');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::MODULES);
        Schema::enableForeignKeyConstraints();
    }
}
