<?php

return [
    'title' => 'Panel Dil Değişkenleri',
    'save_info' => 'Dikkat kaydetme işlemi otomatik yapılır. İlgili alanları doldurmanız yeterlidir.',
    'search' => 'Arama...',
    'variable'=>'Değişken',
    'default_lang_value'=>'Varsayılan Dil Değeri',
    'detail' => "Detay",
    'view'=>'Görüntüle',
    'languages'=>'Diller',
];
