<div class="lang-box">
    <form action="javascript:void(0);">
        <div class="item">
            <div class="row">
                <div class="col-4">
                    <span>{!! __("MPCorePanel::panel_translate.variable") !!}</span>
                </div>
                <div class="col-6">
                    <span>{!! __("MPCorePanel::panel_translate.default_lang_value") !!}</span>
                </div>
                <div class="col-2">
                    <span>{!! __("MPCorePanel::general.detail") !!}</span>
                </div>
            </div>
        </div>

        @foreach($data as $key => $parts)
            <div class="item" id="{{ $key }}">
                <div class="row">
                    <div class="col-4">
                        <span>{{ $key }}</span>
                    </div>
                    <div class="col-6">
                            <span>
                                {!! $parts['default_value'] !!}
                            </span>
                    </div>
                    <div class="col-2">
                        <button>{!!  __("MPCorePanel::panel_translate.view") !!}</button>
                    </div>
                    <div class="col-12 edit lang_area">
                        <div class="row">
                            <div class="col-12">
                                <span class="head">{!! __("MPCorePanel::panel_translate.languages") !!}</span>
                            </div>
                            @foreach($parts['parts'] as $languageId => $value)
                                @php
                                    $language = \Mediapress\Modules\MPCore\Models\Language::find($languageId);
                                @endphp
                                <div class="col-6">
                                    <div class="langCode{{ $language->code }}">
                                        <div class="form-group">
                                            <div class="language-header">
                                                <div class="language-flag">
                                                    <img src="{{ getFlag($language->code) }}"
                                                         alt="{!! $language->code !!}">
                                                </div>
                                                <div class="language-title">
                                                    {!! $language->name !!}
                                                </div>
                                            </div>
                                            {!! Form::text($parts['key'].'['.$languageId.']', $value, ["class"=>"lang_part"]) !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </form>
</div>
