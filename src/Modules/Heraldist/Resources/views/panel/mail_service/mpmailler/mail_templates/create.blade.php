@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
            <h1 class="title">{!! trans("HeraldistPanel::mail_service.create_template") !!}</h1>
            <div class="col-md-12 pa">
                <form action="{!! route('Heraldist.mpmailler.mail_templates.store') !!}" novalidate method="POST">
                    @include("HeraldistPanel::mail_service.mpmailler.mail_templates.form")
                    <div class="float-right">
                        <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                    </div>
                </form>
        </div>
    </div>
@endsection
