<?php

namespace Mediapress\Modules\Heraldist\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Exports\EbulletinExport;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Mediapress\DataTable\TableBuilderTrait;
use Yajra\DataTables\Html\Builder;
use Html;
use DataTables;
use Carbon\Carbon;

class EBulletinManagerController extends Controller
{
    use TableBuilderTrait;

    public const CREATED_AT = "created_at";

    public function index(Builder $builder)
    {
        $website_id = session("panel.website.id");
        if( ! activeUserCan([
            "heraldist.website".$website_id.".ebulletin.index",
            "heraldist.website.ebulletin.index"
        ])){
            return rejectResponse();
        }

        if (request()->get('list') && request()->get('list')=="all") {
            $dataTable = $this->columns($builder)->ajax(route('Heraldist.ebulletin.ajax', null));
        } else {
            $dataTable = $this->columns($builder)->ajax(route('Heraldist.ebulletin.ajax', session("panel.website")->id));
        }
        return view('HeraldistPanel::ebulletin.index', compact('dataTable'));

    }

    public function excelAll()
    {

        $website_id = session("panel.website.id");
        if( ! activeUserCan([
            "heraldist.website".$website_id.".ebulletin.export",
            "heraldist.website.ebulletin.export"
        ])){
            return rejectResponse();
        }

        $date =  Carbon::parse(date("d-m-Y"))->formatLocalized("%Y-%m-%d");

        $website_id = (session("panel.website")) ? session("panel.website")->id : 1;
        $data = Ebulletin::where("website_id",$website_id)->get(['id','name', "email", self::CREATED_AT]);

        return Excel::download(new EbulletinExport($data), trans("HeraldistPanel::ebulletin.ebulletins").' '.$date.'.xlsx');
    }

    public function excelNew()
    {
        $website_id = session("panel.website.id");
        if( ! activeUserCan([
            "heraldist.website".$website_id.".ebulletin.export",
            "heraldist.website.ebulletin.export"
        ])){
            return rejectResponse();
        }

        $date = str_replace("/", "-", isset($_GET["date"]) ? (!empty($_GET["date"]) ? $_GET["date"] : "01/01/2018") : "01/01/2017");

        $website_id = (session("panel.website")) ? session("panel.website")->id : 1;
        $data = Ebulletin::where("website_id",$website_id)
            ->where(self::CREATED_AT, ">=", Carbon::parse($date)->formatLocalized("%Y-%m-%d"))
            ->orderBy(self::CREATED_AT)
            ->select("id","name", "email", self::CREATED_AT)
            ->get();

        return Excel::download(new EbulletinExport($data), trans("HeraldistPanel::ebulletin.ebulletins").' '.$date.'.xlsx');
    }

    public function delete($id)
    {

        $ebulletin = Ebulletin::find($id);

        if ($ebulletin) {
            if( ! activeUserCan([
                "heraldist.website".$ebulletin->website_id.".ebulletin.delete",
                "heraldist.website.ebulletin.delete"
            ])){
                return rejectResponse();
            }
            $ebulletin->delete();
            return redirect()->back()->with('message', trans('MPCorePanel::general.success_message'));
        }
        return redirect()->back()->withErrors(['E-Bülten girdisi bulunamadı']);
    }

    public function selectedRemove(Request $request)
    {
        if ($request->checked) {

            $permissions = [
                "heraldist.website.ebulletin.delete"
            ];
            $websiteIds = Ebulletin::select("website_id")
                ->whereIn('id',$request->checked)
                ->distinct("website_id")
                ->get()->pluck("website_id")->toArray();

            foreach($websiteIds as $websiteId){
                array_unshift($permissions,"heraldist.website".$websiteId.".ebulletin.delete");
            }

            if( ! activeUserCan($permissions)){
                return rejectResponse();
            }

            foreach ($request->checked as $check) {
                Ebulletin::destroy($check);
            }
        } else {
            return redirect()->back()->withErrors([trans('MPCorePanel::general.danger_message')]);
        }

        return redirect()->back()->with('message', trans('MPCorePanel::general.success_message'));
    }

}
