@extends('ContentPanel::inc.module_main')
{{--@push("styles")
    <style>
        table#sitemaps-table td {
            padding: 8px 5px !important;
        }

        table#sitemaps-table td li {
            padding: 3px;
            min-height: 32px;
            border: 1px solid #eee;
        }
    </style>
@endpush--}}
@section('content')
    @include('MPCorePanel::inc.breadcrumb')

    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! __("ContentPanel::sitemap.sitemaps") !!}</div>
            </div>
            <div class="float-right">
                <button class="btn btn-primary btn-sm float-right" type="button" data-toggle="modal" data-target="#createSitemap"><i
                        class="fa fa-plus"></i> {!! __("ContentPanel::sitemap.new_add") !!}</button>
                @if(isMCAdmin())
                    <div class="float-right ml-2 mr-2">
                        <form action="{{ route("Content.sitemaps.export_json") }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-sm" type="button"><i class="fa fa-upload"></i>
                                {!! __("ContentPanel::sitemap.export_json") !!}
                            </button>
                        </form>
                    </div>
                    <div class="float-right">
                        <a href="{{ route("Content.sitemaps.import_json") }}" data-toggle="modal"
                           data-target="#importJsonSitemap" class="btn btn-primary btn-sm" type="button">
                            <i class="fa fa-download"></i>
                            {!! __("ContentPanel::sitemap.import_json") !!}
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="modal fade" id="createSitemap" tabindex="-1" role="dialog" aria-labelledby="createSitemap"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="get" action="{!! route('Content.sitemaps.create') !!}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.homepage") !!}
                                        <input type="radio" name="type" value="homepage">
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.search") !!}
                                        <input type="radio" name="type" value="search">
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.login_type") !!}
                                        <input type="radio" name="type" value="login">
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.register_type") !!}
                                        <input type="radio" name="type" value="register">
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.password_email_type") !!}
                                        <input type="radio" name="type" value="password email">
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.email_verify_type") !!}
                                        <input type="radio" name="type" value="email verify">
                                    </label>
                                </div>
                                <hr>
                                <div class="checkbox">
                                    <label>
                                        {!! __("ContentPanel::sitemap.other") !!}
                                        <input type="radio" name="type" value="null" checked>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">{!! __("ContentPanel::sitemap.cancel") !!}</button>
                            <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> {!! __("ContentPanel::sitemap.add") !!}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="importJsonSitemap" tabindex="-1" role="dialog" aria-labelledby="importJsonSitemap"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="{!! route('Content.sitemaps.import_json') !!}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="sitemap_json">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> {!! __("ContentPanel::sitemap.add") !!}</button>
                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">{!! __("ContentPanel::sitemap.cancel") !!}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="p-30">
            <table id="sitemaps-table" class="m-0 table table-striped">
                <thead>
                <tr>
                    <th>{!! __("ContentPanel::sitemap.id") !!}</th>
                    <th>{!! __("ContentPanel::sitemap.sitemap_type") !!}</th>
                    <th>{!! __("ContentPanel::sitemap.structure") !!}</th>
                    <th>{!! __("ContentPanel::sitemap.structural_features") !!}</th>
                    <th>{!! __("ContentPanel::sitemap.variations") !!}</th>
                    <th>{!! __("ContentPanel::sitemap.actions") !!}</th>
                </tr>
                </thead>
                @foreach($sitemaps as $key=>$sitemap)
                    <tr>
                        <td>{!! $sitemap->id !!}</td>
                        <td>
                            {!!  implode(" - ", array_filter([($sitemap->detail->name ?? ""),(isset($sitemap->sitemapType) ? '<strong>'.$sitemap->sitemapType->name.'</strong>' : ''),($sitemap->feature_tag ? '<span class="badge badge-primary">'.$sitemap->feature_tag.'</span>' :"")]))  !!}

                        </td>
                        <td style="padding-right: 5px;">
                            <b>{{ isset($sitemap->sitemapType) ? __("ContentPanel::sitemap.structure.".$sitemap->sitemapType->sitemap_type_type) : '-' }}</b>
                        </td>
                        <td>
                            <i class="fa fa-search {!! $sitemap->searchable ? "text-success" : "text-black-50" !!}"
                               title="{!! $sitemap->searchable ? "Aranabilir" : "Aranamaz" !!}"></i>
                            <i class="fa fa-file {!! $sitemap->detailPage ? "text-success" : "text-black-50" !!}"
                               title="{!! $sitemap->detailPage ? "Detayı Var" : "Detayı Yok" !!}"></i>
                            <i class="fa fa-folder {!! $sitemap->category ? "text-success" : "text-black-50" !!}"
                               title="{!! $sitemap->category ? "Kategoriler Etkin" : "Kategoriler Etkin Değil" !!}"></i>
                            <i class="fa fa-check-square {!! $sitemap->criteria ? "text-success" : "text-black-50" !!}"
                               title="{!! $sitemap->criteria ? "Kriterler Etkin" : "Kriterler Etkin Değil" !!}"></i>
                            <i class="fa fa-star {!! $sitemap->property ? "text-success" : "text-black-50" !!}"
                               title="{!! $sitemap->property ? "Özellikler Etkin" : "Özellikler Etkin Değil" !!}"></i>
                        </td>
                        <td style="border:0px solid #ddd; border-top:none;">
                            {{count($sitemap->details)/*.trans("ContentPanel::sitemap.language_country_group")*/}}
                        </td>
                        <td style="padding-left:5px;">
                            <a onclick="locationOnChange('{!! route('Content.sitemaps.edit',[$sitemap->id,$website->id]) !!}')"
                               href="#" title="{!! __("ContentPanel::sitemap.edit") !!}"><span
                                    class="fa fa-pen"></span></a>
                            <a onclick="locationOnChange('{!! route('Content.sitemaps.delete',[$sitemap->id,$website->id]) !!}')"
                               href="#" title="{!! __("ContentPanel::sitemap.delete") !!}"><span
                                    class="fa fa-trash"></span></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="row" style="clear: both;">
            <div class="col-12">
                {!! $sitemaps->links() !!}
            </div>
        </div>
    </div>
@endsection

{{--@push("styles")
    <style>
        .tr td {
            text-align: center !important;
        }
    </style>
@endpush--}}
