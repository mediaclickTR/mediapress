<?php

namespace Mediapress\Modules\Entity\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Mediapress\Modules\Entity\Models\Entities;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mediapress\Facades\Modules\Content;

class EntityController extends Controller
{
    public const ENTITY_ENTITIES_INDEX = 'Entity.entities.index';
    public const MESSAGE = 'message';
    public const ENTITY_PANEL_ENTITY_NAME_ENTITY = "EntityPanel::entity.name-entity";
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const EMAIL = 'email';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';
    public const ENTITY_PANEL_ENTITY_EMAIL_ENTITY = "EntityPanel::entity.email-entity";
    public const ACCESSDENIED = 'accessdenied';

    public function index()
    {
        if(! activeUserCan(["entity.entities.index",])){return rejectResponse();}

        $entities = Entities::paginate(25);
        return view('EntityPanel::entities.index', compact('entities'));
    }

    public function create()
    {
        if(! activeUserCan(["entity.entities.create",])){return rejectResponse();}

        return view("EntityPanel::entities.create");
    }

    public function edit($id)
    {
        if(! activeUserCan(["entity.entities.update",])){return rejectResponse();}

        $entity = Entities::find($id);

        return view('EntityPanel::entities.edit', compact("entity"));
    }

    public function store(Request $request)
    {

        if(! activeUserCan(["entity.entities.create",])){return rejectResponse();}

        /*
         * Validation
         */

        $fields = [
            'name' => trans(self::ENTITY_PANEL_ENTITY_NAME_ENTITY),
            'surname' => trans("EntityPanel::entity.surname-entity"),
            self::EMAIL => trans(self::ENTITY_PANEL_ENTITY_EMAIL_ENTITY),
            'description' => trans("EntityPanel::entity.description-entity"),
        ];

        $rules = [
            'name' => 'required|max:255',
            self::EMAIL => 'required|unique:entities',
        ];

        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_ENTITY_NAME_ENTITY)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_ENTITY_EMAIL_ENTITY)]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::ENTITY_PANEL_ENTITY_EMAIL_ENTITY)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $data = request()->except('_token');

        $insert=Entities::firstOrCreate($data);

        if ($insert){
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route(self::ENTITY_ENTITIES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {
        /*
         * Validation
         */
        if(! activeUserCan(["entity.entities.update",])){return rejectResponse();}

        $fields = [
            'name' => trans(self::ENTITY_PANEL_ENTITY_NAME_ENTITY),
            'surname' => trans("EntityPanel::entity.surname-entity"),
            self::EMAIL => trans(self::ENTITY_PANEL_ENTITY_EMAIL_ENTITY),
            'description' => trans("EntityPanel::entity.description-entity"),
        ];

        $rules = [
            'name' => 'required|max:255',
            //'email' => 'required|unique:entities',
        ];

        $messages = [

            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_ENTITY_NAME_ENTITY)]),
            //'email.required' => trans("MPCorePanel::validation.filled", ['filled',trans("EntityPanel::entity.email-entity")]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::ENTITY_PANEL_ENTITY_EMAIL_ENTITY)]),
        ];


        $datas = [
            'name' => $request->name,
            self::EMAIL => $request->email,
        ];

        $this->validate($request, $rules, $messages, $fields);

        if($request->password){
            $datas['password']=Hash::make($request->password);
        }

        $update=Entities::updateOrCreate(['id'=>$request->id],$datas);

        if ($update){
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::ENTITY_ENTITIES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {

        if(! activeUserCan(["entity.entities.delete",])){return rejectResponse();}

        $data=Entities::find($id);

        if($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect(route(self::ENTITY_ENTITIES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
