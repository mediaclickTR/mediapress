@extends('HeraldistPanel::inc.module_main')
@section('content')
@include('MPCorePanel::inc.breadcrumb')
<div class="page-content">
    <h1 class="title">{!! trans("HeraldistPanel::mail_service.edit_campaign") !!}</h1>
    @include("MPCorePanel::inc.errors")
    <div class="row">
        <div class="col-md-12">
            <form action="{!! route('Heraldist.mailchimp.campaigns.update') !!}" novalidate method="POST">
                @include("HeraldistPanel::mail_service.mailchimp.campaigns.form")
            </form>
        </div>
    </div>
@endsection
