<?php

return [
    'modal_title' => "Site Logos",
    'desktop_logo' => "Desktop Logo",
    'mobile_logo' => "Mobile Logo",
    'email_logo' => "Email Logo",
    'favicon' => "Favicon" ,
];
