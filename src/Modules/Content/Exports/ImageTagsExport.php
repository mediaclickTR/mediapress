<?php

namespace Mediapress\Modules\Content\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\FileManager\Models\MFileGeneral;

class ImageTagsExport implements FromArray
{

    public function array(): array
    {
        $hold = [];

        $headingRow = ['id', 'url', 'image', 'title', 'alt_tag'];
        $hold[] = $headingRow;

        $images = MFileGeneral::whereHas('mfile', function ($q) {
            $q->where('mimeclass', 'image');
        })
            ->whereHasMorph('model', '*', function ($q, $type) {
                if (in_array($type, [Page::class, Category::class])) {
                    $q->whereHas('sitemap', function ($que){
                        $que->whereHas('websites', function ($query) {
                            $query->where('id', session('panel.website.id'));

                        });
                    });
                } elseif (in_array($type, [PageDetail::class, CategoryDetail::class])) {
                    $q->whereHas('parent', function ($que) {
                        $que->whereHas('sitemap', function ($quer){
                            $quer->whereHas('websites', function ($query) {
                                $query->where('id', session('panel.website.id'));
                            });
                        });
                    });
                } elseif ($type == MenuDetail::class) {
                    $q->whereHas('menu', function ($que) {
                        $que->where('menus.website_id', session('panel.website.id'));
                    });
                } elseif ($type == SitemapDetail::class) {
                    $q->whereHas('sitemap', function ($que) {
                        $que->whereHas('websites', function ($query) {
                            $query->where('id', session('panel.website.id'));
                        });
                    });
                } else {
                    $q->whereHas('websites', function ($query) {
                        $query->where('id', session('panel.website.id'));
                    });
                }
            })
            ->orderBy('model_type')
            ->orderBy('model_id')
            ->get();


        foreach ($images as $image) {
            $tempImage = image($image->mfile_id);
            $model = $image->model;

            if (in_array(get_class($model), [Page::class, Category::class, Sitemap::class])) {
                $url = $model->detail->url;
            } elseif (in_array(get_class($model), [PageDetail::class, CategoryDetail::class, SitemapDetail::class])) {
                $url = $model->url;
            } else {
                $url = 'N\A';
            }

            $hold[] = [
                'id' => $image->id,
                'url' => $url->url,
                'image' => $tempImage->url,
                'title' => $image->details[session('panel.active_language.id')]['title'] ?? null,
                'alt_tag' => $image->details[session('panel.active_language.id')]['tag'] ?? null,
            ];
        }

        return $hold;
    }
}
