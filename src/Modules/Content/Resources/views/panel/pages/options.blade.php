@foreach($categories as $category)
    @php if(!$category->detail){ continue; } @endphp
    <option value="{!! $category->id !!}">
        @for($i=0;$i<$category->depth;$i++)
            &nbsp;&nbsp;&nbsp;
        @endfor
        {!! $category->detail->name !!}
    </option>
    @if($category->children)
        @include('ContentPanel::pages.options',['categories'=>$category->children])
    @endif
@endforeach
