<?php

namespace Mediapress\Modules\Interaction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Models\MPModule;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Interaction\Models\Device;
use Mediapress\Modules\Interaction\Models\NotFoundVisit;
use Mediapress\Modules\Interaction\Models\OperatingSystem;
use Mediapress\Modules\Interaction\Models\Visit;
use Mediapress\Modules\MPCore\Models\Url;

use Mediapress\Modules\Interaction\Models\Browser;

class Interaction extends MPModule
{
    public const REFERER = 'referer';
    public $name = "Interaction";
    public $url = "mp-admin/Interaction";
    public $description = "Interaction";
    public $author = "";
    public $menus = [];
    private $plugins = [];


    public function checkCookie()
    {
        if(env('INTERACTION_ACTIVE')){
            $request = Request::capture();
            if ($request->segment(1) == 'mp-admin') {
                return null;
            }
            $ua = new UserAgent();

            if($ua->getDevice() == 'robot'){
                return null;
            }
            $cookieValue = Cookie::get('mediapress');

            if (!$cookieValue) {
                $cookie = $this->createCookie();
            } else {
                $cookie = \Mediapress\Modules\Interaction\Models\Cookie::where('hash', $cookieValue)->first();
                if (!$cookie || is_null($cookie->id)) {
                    $cookie = $this->createCookie();
                }
            }

            $sessionValue = Session::get('mediapressSession');

            $session = null;
            if ($sessionValue) {
                $session = \Mediapress\Modules\Interaction\Models\Session::where('hash', $sessionValue)->first();
            }

            if (is_null($session)) {
                $sessionHash = $this->makeUniqueId();

                $session = \Mediapress\Modules\Interaction\Models\Session::updateOrCreate(
                    ['hash' => $sessionHash],
                    [
                        'hash' => $sessionHash,
                        'cookie_id' => $cookie->id
                    ]
                );

                Session::put('mediapressSession', $sessionHash);

            }

            return $this->makeRequest($cookie, $session);
        }

    }

    private function createCookie()
    {
        $ua = new UserAgent();

        if($ua->getDevice() == 'robot'){
            return $ua;
        }
        $browser = Browser::firstOrCreate(['name' => trim($ua->browser())]);
        $device = Device::firstOrCreate(['name' => trim($ua->getDevice())]);
        $operatingSystem = OperatingSystem::firstOrCreate(['name' => trim($ua->platform())]);


        $hold['browser_id'] = $browser->id;
        $hold['device_id'] = $device->id;
        $hold['operating_system_id'] = $operatingSystem->id;
        $hold['user_id'] = auth()->id();
        $hold['hash'] = $this->makeUniqueId();

        $cookie = \Mediapress\Modules\Interaction\Models\Cookie::firstOrCreate(
            ['hash' => $hold['hash']],
            $hold
        );

        Cookie::queue(Cookie::forever('mediapress', $hold['hash']));

        return $cookie;
    }

    private function makeUniqueId()
    {
        return md5(uniqid(time(), true));
    }

    private function makeRequest($cookie, $session)
    {
        if(env('INTERACTION_ACTIVE')){
            $request = Request::capture();

            if (\Str::startsWith($request->path(), "vendor/") || \Str::startsWith($request->path(), "assets/")) {
                return;
            }

            $visit = [
                'cookie_id' => $cookie->id,
                'session_id' => $session->id
            ];

            $query_string = '';
            $i = 0;
            $utm = [];
            foreach ($request->all() as $key => $value) {
                if (!is_array($value)) {
                    if (strpos($key, 'utm_') !== false) {
                        $utm[$key] = $value;
                    }
                    if ($value) {
                        if ($i == 0) {
                            $query_string .= '?' . $key . '=' . urlencode($value);
                        } else {
                            $query_string .= '&' . $key . '=' . urlencode($value);
                        }
                    }
                    $i++;
                }
            }

            if (count($utm)) {
                Session::put('mediapressUtm', $utm);
            }

            $referer = $request->header(self::REFERER);
            if ($referer) {
                $parsedUrl = parse_url($referer);
                if (isset($parsedUrl['host'])) {
                    $host = $request->server('HTTP_HOST');
                    if ($host != $parsedUrl['host']) {
                        Session::put('mediapressSource', $referer);
                    }
                }
            }



            $visit['query_string'] = $query_string == "" ? null : $query_string;
            $visit[self::REFERER] = parse_url($request->headers->get(self::REFERER), PHP_URL_HOST);

            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            if (!isset($_SERVER["REMOTE_ADDR"])) {
                $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
            }


            $visit['ip'] = $_SERVER['REMOTE_ADDR'];
            $visit['hour'] = date('H');
            $visit['week'] = date('w');


            $url = $this->getUrl();

            if(is_null($url)) {
                $visit['url'] = '/' . $request->path();
                return NotFoundVisit::create($visit);
            } else {
                $visit['url_id'] = $url->id;
                return Visit::create($visit);
            }
        }

    }

    private function getUrl() {

        $url = null;

        $request = request();
        $parsedData = array_merge(["path" => "/"], parse_url($request->fullUrl()));

        $domain = $parsedData["host"] = str_replace("www.", "", $parsedData["host"]);
        $domainHold = [$domain, "www.".$domain];

        $website = Website::whereIn('slug', $domainHold)->first();
        $parent = null;

        if ($website && $website->website && $website->type == 'alias') {
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold) {
                $q->whereIn("slug", $domainHold);
            })->with("dynamicUrl")->orderBy("created_at")->first();

        }else{
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold) {
                $q->whereIn("slug", $domainHold);
            })->with("dynamicUrl")->orderBy("created_at")->first();
        }
        if (!$url) {
            $folder = $request->segment(1);
            $parsedData['path'] = mb_substr($parsedData['path'],(strlen($folder)+1));
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold,$folder) {
                $q->where("slug", $folder)->whereHas('website',function ($query) use($domainHold){
                    $query->whereIn('slug',$domainHold);
                });
            })->with("dynamicUrl")->orderBy("created_at")->first();
        }

        return $url;
    }

}
