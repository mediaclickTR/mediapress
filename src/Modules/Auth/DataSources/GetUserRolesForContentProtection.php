<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 15.11.2018
     * Time: 09:59
     */

    namespace Mediapress\Modules\Auth\DataSources;


    use Mediapress\Foundation\DataSource;
    use Mediapress\Modules\Auth\Models\Role;
    use Mediapress\Modules\MPCore\Models\Language;

    class GetUserRolesForContentProtection extends DataSource
    {
        public function getData(){
            //TODO:şimdilik panel admin rollerini döndürüyoruz, hazır olduğu zaman user rollerine çevrilecek
            $pluck=($this->params["pluck"] ?? false) && $this->params["pluck"];
            if($pluck){
                $key = $this->params["pluck_key"] ?? "id";
                $value = $this->params["pluck_column"] ?? "id";
                return Role::status()->orderby("title")->get()->pluck($value,$key);
            }
            return Role::status()->orderby("title")->get();
        }
    }