@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
               <div class="topPage">
                   <div class="float-left">
                       <div class="title m-0">{!! trans("EntityPanel::user.users") !!}</div>
                   </div>
                   <div class="float-right">
                       <select id="multiple-processings" onchange="locationOnChange(this.value);">
                           <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                           <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!}  value="?list=all">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                       </select>
                       @if(request()->get('list') && request()->get('list')=="all")
                           <a href="{!! route("Entity.users.index") !!}" class="filter-block">
                               <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                           </a>
                       @endif
                       <a class="btn btn-primary ml-2" href="{!! route('Entity.users.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                   </div>
               </div>
                <div class="float-left">

                </div>
                <div class="float-right">
                </div>
            <div class="p-30">
                <div class="table-field">
                    @if(count($users) == 0)
                        <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                    @else

                        <table>
                            <thead>
                            <tr>
                                <th>{!! trans("MPCorePanel::general.name") !!}</th>
                                <th>{!! trans("MPCorePanel::general.email") !!}</th>
                                <th>{!! trans("MPCorePanel::general.website") !!}</th>
                                <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                                <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                            </tr>
                            </thead>
                            @foreach($users as $user)
                                <tr>
                                    <td>{!! $user->name !!}</td>
                                    <td>{!! $user->email !!}</td>
                                    <td>{!! (isset($user->website)) ? $user->website->domain : '-' !!}</td>
                                    <td>{!! $user->created_at !!}</td>
                                    <td>
                                        <select onchange="locationOnChange(this.value);">
                                            <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                            <option value="{!! route('Entity.users.edit',$user->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                            <option value="{!! route('Entity.users.delete',$user->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                </div>
            </div>
            <div class="float-left">
                {!! $users->links() !!}
            </div>
            <div class="float-right">
                <select>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
            @endif

    </div>
@endsection
