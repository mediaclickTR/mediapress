<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Http\Request;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Foundation\ClusterArray;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\Content\Models\Menu;
use Illuminate\Support\Facades\Cache;

class MenuController extends Controller
{
    #region Menu CRUD
    public const ACCESSDENIED = 'accessdenied';
    public const CONTENT_MENUS_INDEX = "Content.menus.index";
    public const PANEL_WEBSITE = 'panel.website';
    public const MP_CORE_PANEL_GENERAL_DANGER_MESSAGE = 'MPCorePanel::general.danger_message';
    public const STATUS = 'status';
    public const MENU_ID = 'menu_id';
    public const ERROR = 'error';
    public const WEBSITE_ID = "website_id";
    public const PARENT = 'parent';
    public const OUT_LINK = 'out_link';
    public const TARGETS = 'targets';
    public const VIEW = 'view';
    public const TYPES = 'types';
    public const TOKEN = '_token';
    public const LANGUAGE_ID = 'language_id';
    public const URL_ID = 'url_id';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const MESSAGE = 'message';

    public function index()
    {
        $website_id = session(self::PANEL_WEBSITE)->id;
        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.index",
            "content.websites.menus.index",
        ])){
            return rejectResponse();
        }
        if( auth()->user()->country_group ){
            $country_group = session(self::PANEL_WEBSITE)->countryGroups->where('id', auth()->user()->country_group)->first();
            $country_group_id = auth()->user()->country_group;
            $language_id = $country_group->languages->first()->id;
        }else{
            $country_group_id = session(self::PANEL_WEBSITE)->countryGroups->first()->id;
            $language_id = session(self::PANEL_WEBSITE)->countryGroups->first()->languages->first()->id;
        }

        $menus = Menu::where(self::WEBSITE_ID, session(self::PANEL_WEBSITE)->id)->paginate(25);

        return view('ContentPanel::menus.index', compact('menus', 'language_id', 'country_group_id'));
    }

    public function create()
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.create",
            "content.websites.menus.create",
        ])){
            return rejectResponse();
        }

        $create = Menu::preCreate();
        if ($create) {
            return redirect(route("Content.menus.edit", $create->id));
        } else {
            throw new Exception("Hata oluştu", json_encode($create));
        }
    }

    public function edit($id)
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.update",
            "content.websites.menus.update",
        ])){
            return rejectResponse();
        }

        $menu = Menu::find($id);
        $current_website = session(self::PANEL_WEBSITE);
        $menu_types = [__("ContentPanel::menu.header_menu"), __("ContentPanel::menu.footer_menu"), __("ContentPanel::menu.sidebar_menu"), __("ContentPanel::menu.other_menu")];

        return view('ContentPanel::menus.edit', compact('menu', 'id',  'current_website', 'menu_types'));
    }

    public function store(Request $request)
    {


        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.create",
            "content.websites.menus.create",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN, 'slug');
        $data += ['slug' => slug($request->slug)];
        $data += [self::WEBSITE_ID => $request->website];
        $insert = Menu::firstOrCreate($data);
        // Log
        if ($insert) {
            UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $insert);
        }

        return redirect(route(self::CONTENT_MENUS_INDEX))->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.update",
            "content.websites.menus.update",
        ])){
            return rejectResponse();
        }

        $id = $request->id;
        $data = request()->except(self::TOKEN, 'slug');
        $data += ['slug' => slug($request->slug)];
        $data += [self::WEBSITE_ID => $request->website];
        $menu = Menu::findOrFail($id);
        $update = $menu->update($data);

        // Log
        if ($update) {
            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $menu);
        }
        return redirect(route(self::CONTENT_MENUS_INDEX))->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.delete",
            "content.websites.menus.delete",
        ])){
            return rejectResponse();
        }

        $menu = Menu::find($id);
        if ($menu) {
            $menu->delete();
        }

        return redirect(route(self::CONTENT_MENUS_INDEX))->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    #endregion

    #region DETAIL CRUD

    public function detailsCreate($country_group_id, $language_id, $id)
    {

        $menu = $this->nestableGetList($id, $language_id, $country_group_id, self::PARENT);
        $country_groups = session(self::PANEL_WEBSITE)->countryGroups;

        $language = Language::find($language_id);

        $types = [__("ContentPanel::menu.inlink"), __("ContentPanel::menu.outlink"), __("ContentPanel::menu.emptylink")];
        $view = [__("ContentPanel::menu.dropdown_menu"), __("ContentPanel::menu.mega_menu"), __("ContentPanel::menu.single_menu")];
        $targets = ['_self' => __("ContentPanel::menu.self"), '_blank' => __("ContentPanel::menu.blank")];
        $status = [1 => __("ContentPanel::general.status_title.active"), 2 => __("ContentPanel::general.status_title.passive")];
        $form_data = [
            self::TYPES => $types,
            self::VIEW => $view,
            self::TARGETS => $targets,
            self::STATUS => $status,
        ];

        $urls = Url::where("website_id",session("panel.website.id"))
            ->where('type', 'original')
            ->whereHasMorph('model', '*', function($q) use($country_group_id) {
                $q->where('country_group_id', $country_group_id);
            })
            ->with('website')
            ->get(['id','url', 'website_id']);

        $data_options = json_decode('{"key":"","file_type":"image","required":"required","title":"","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","pixels_x_min":"","pixels_x_max":"","pixels_y_min":"","pixels_y_max":"","pixels_x":"","pixels_y":"","filesize_min":"","filesize_max":"","files_max":"1","additional_rules":""}', 1);


        return view('ContentPanel::menus.details', compact('form_data', 'language', 'urls', 'id', 'menu', 'country_groups', 'language_id', 'country_group_id', 'data_options'));
    }

    public function detailsStore(Request $request)
    {
        $data = request()->except(self::TOKEN, self::TARGETS);

        if( isset( $data["file_id"] ) ) {
            $data["file_id"] = str_replace(["[","]"], "", $data["file_id"]);
        } else {
            $data["file_id"] = NULL;
        }

        $insert = MenuDetail::firstOrCreate($data);

        // Log
        if ($insert) {
            Cache::flush();
            UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $insert);
            return redirect()->back()->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, __(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
        }
    }

    public function detailsEdit($menu_id, $id)
    {
        if (!userAction('menu.detail.edit', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $menu = MenuDetail::find($id);

        $types = [__("ContentPanel::menu.inlink"), __("ContentPanel::menu.outlink"), __("ContentPanel::menu.emptylink")];
        $view = [__("ContentPanel::menu.dropdown_menu"), __("ContentPanel::menu.mega_menu"), __("ContentPanel::menu.single_menu")];
        $targets = ['_self' => __("ContentPanel::menu.self"), '_blank' => __("ContentPanel::menu.blank")];
        $status = [1 => __("ContentPanel::general.status_title.active"), 2 => __("ContentPanel::general.status_title.passive")];

        $form_data = [
            self::TYPES => $types,
            self::VIEW => $view,
            self::TARGETS => $targets,
            self::STATUS => $status,
        ];

        $urls = Url::where("website_id",session("panel.website.id"))
            ->where(function($q){
                $q->where('type', 'original')
                    ->orWhere('type', 'category');
            })
            ->with('website')
            ->get(['id','url', 'website_id']);

        $data_options = json_decode('{"key":"","file_type":"image","required":"required","title":"","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","pixels_x_min":"","pixels_x_max":"","pixels_y_min":"","pixels_y_max":"","pixels_x":"","pixels_y":"","filesize_min":"","filesize_max":"","files_max":"1","additional_rules":""}', 1);


        return view('ContentPanel::menus.details_edit', compact('form_data', 'menu', 'urls', self::MENU_ID, 'id', "data_options"));
    }

    public function detailsUpdate(Request $request, $id)
    {
        $data = request()->except(self::TOKEN, self::TARGETS, self::URL_ID, self::OUT_LINK);

        if ($request->type == 0) {
            $data += [self::URL_ID => $request->url_id];
            $data += [self::OUT_LINK => null];
        } else {
            $data += [self::OUT_LINK => $request->out_link];
            $data += [self::URL_ID => null];
        }
        if ($request->target) {
            $data += ['target' => $request->target];
        }

        if( ! isset($data["file_id"]) || $data["file_id"] == "[]" ) {
            $data["file_id"] = NULL;
        } else {
            $data["file_id"] = str_replace(["[", "]"], "", $data["file_id"]);
        }

        $update = MenuDetail::updateOrCreate(['id' => $id], $data);

        // Log
        if ($update) {
            Cache::flush();
            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $update);
            return redirect()->route("Content.menus.details", ['country_group_id' => $update->country_group_id, self::LANGUAGE_ID => $update->language_id, 'id' => $update->menu_id])->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, __(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
        }

    }

    public function detailsDelete($id)
    {
        if (!userAction('message.menus.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = MenuDetail::findOrFail($id);

        if ($data) {
            $this->deleteListRecursive($data->id, false, request()->get('child'));
            UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $data);
            return redirect()->back()->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }
    }

    public function updateList(Request $request)
    {
        $cluster = new ClusterArray();

        $array = $this->buildTree(json_decode($request->menu_json), self::PARENT);

        foreach ($cluster->cluster($array, 0, 0, 0, null, true) as $menupart) {
            $menupart['draft'] = 0;
            MenuDetail::updateOrCreate(['id' => $menupart['id']], $menupart);
        }

        if ($cluster->buildArray($array, self::PARENT, 0, 0, true)) {
            Cache::flush();
            return redirect()->back()->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, __(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
    }
    #endregion

    #region COPY
    public function copy($menu_id)
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.create",
            "content.websites.menus.create",
        ])){
            return rejectResponse();
        }

        $country_groups = CountryGroup::where('owner_id', $this->getWebsiteSession()->id)
            ->get();


        return view('ContentPanel::menus.copy', compact('country_groups', self::MENU_ID));
    }

    public function copyAjax()
    {
        $data = request()->except(self::TOKEN);
        $languages = CountryGroup::where('owner_id', $this->getWebsiteSession()->id)
            ->find($data['country_id'])->languages->pluck('name', 'id');


        return $languages;
    }

    public function postCopy(Request $request)
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".menus.create",
            "content.websites.menus.create",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN);

        $fields = [
            'source_country_group' => __("ContentPanel::menu.source_country_group"),
            'target_country_group' => __("ContentPanel::menu.target_country_group"),
            'source_language' => __("ContentPanel::menu.source_language"),
            'target_language' => __("ContentPanel::menu.target_language"),
        ];
        $rules = [
            'source_country_group' => 'required',
            'target_country_group' => 'required',
            'source_language' => 'required',
            'target_language' => 'required',
        ];
        $messages = [
            'source_country_group.required' => __("MPCorePanel::validation.filled", ['filled', __("ContentPanel::menu.source_country_group")]),
            'target_country_group.required' => __("MPCorePanel::validation.filled", ['filled', __("ContentPanel::menu.target_country_group")]),
            'source_language.required' => __("MPCorePanel::validation.filled", ['filled', __("ContentPanel::menu.source_language")]),
            'target_language.required' => __("MPCorePanel::validation.filled", ['filled', __("ContentPanel::menu.target_language")]),
        ];

        $this->validate($request, $rules, $messages, $fields);


        $menu_id = $request->menu_id;
        $source_country_group = $request->source_country_group;
        $target_country_group = $request->target_country_group;
        $source_language = $request->source_language;
        $target_language = $request->target_language;


        //Delete target values

        MenuDetail::where('menu_id', $menu_id)
            ->where('country_group_id', $target_country_group)
            ->where(self::LANGUAGE_ID, $target_language)->delete();



        $menus = MenuDetail::where('menu_id', $menu_id)
            ->where('country_group_id', $source_country_group)
            ->orderBy('lft')
            ->where(self::LANGUAGE_ID, $source_language)->get()->toArray();

        $plus = MenuDetail::withTrashed()->orderBy('id', 'desc')->first(['id'])->id;

        if ($menus) {
            $menuArray = [];
            foreach ($menus as $menu) {
                $menu['id'] = $menu['id'] + $plus;
                $menu[self::LANGUAGE_ID] = $target_language * 1;
                $menu['country_group_id'] = $target_country_group * 1;

                if ($menu['url_id']) {
                    $url = Url::find($menu['url_id']);
                    $newUrl = null;
                    if ($url->type == 'original') {
                        $model = $url->relation;
                        if (isset($model->parent)) {
                            $newUrlModel = $model->parent->details()->where(self::LANGUAGE_ID, $target_language)->
                            where('country_group_id',$target_country_group)->first();
                            if ($newUrlModel && $newUrlModel->url) {
                                $newUrl = $newUrlModel->url->id;
                            }
                        }

                    } elseif ($url->type == 'category') {
                        $model = $url->relation;
                        if ($model->parent) {
                            $newUrlModel = $model->parent->details()->where(self::LANGUAGE_ID, $target_language)->
                            where('country_group_id',$target_country_group)->first();
                            if ($newUrlModel->categoryUrl) {
                                $newUrl = $newUrlModel->categoryUrl->id;
                            }
                        }
                    }
                    $menu['url_id'] = $newUrl;
                }


                if ($menu['parent']) {
                    $menu['parent'] = $menu['parent'] + $plus;
                }

                $menuArray[] = $menu;
            }

            MenuDetail::insert($menuArray);

            return redirect()->route("Content.menus.details", ['country_group_id' => $target_country_group, self::LANGUAGE_ID => $target_language, 'id' => $menu_id])->with(self::MESSAGE, __(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, __("ContentPanel::menu.no_data_source_lang"));
        }
    }

    private function deleteListRecursive($id, $bool = false, $child = 0)
    {
        $finded = MenuDetail::where(self::PARENT, $id)->get();

        if ($finded) {
            if($child == 1) {
                foreach ($finded as $find) {
                    MenuDetail::find($find->id)->delete();
                    $this->deleteListRecursive($find->id, true, 0);
                }
            } else {
                foreach ($finded as $find) {
                    $childModel = MenuDetail::find($find->id);
                    $childModel->update([
                        'parent' => 0,
                        'depth' => 0
                    ]);
                    $this->deleteListRecursive($find->id, true, $child);
                }
            }
        }
        if (!$bool) {
            MenuDetail::find($id)->delete();
            return true;
        }
    }

    #endregion

    #region Cluster Staff
    private function nestableGetList($id, $language_id, $country_group_id, $parent_id)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = $parent_id;
        $list = MenuDetail::where(self::MENU_ID, $id)
            ->where(self::LANGUAGE_ID, $language_id)->where('country_group_id', $country_group_id)->orderBy("lft")
            ->get(["id", $parent_id, 'name', self::STATUS]);
        $list = $list->toArray();
        foreach ($list as $key => $value) {
            if (isset($value['name'])) {
                $list[$key]['name'] = str_replace(["'", '"'], ["\'", '\"'], $value['name']);
            }
        }

        return str_replace("\\\\", "\\", json_encode($cluster->buildFromDB($list)));
    }

    private function buildTree($array, $parent_column = "parent")
    {
        $ls = null;
        foreach ($array as $parent) {

            if ($parent->id == 0) {
                continue;
            }
            if (isset($parent->children)) {
                $ls[] = [
                    "id" => $parent->id, $parent_column => 0,
                    "children" => $this->children($parent->children, $parent_column, $parent->id)
                ];
            } else {
                $ls[] = ["id" => $parent->id, $parent_column => 0];
            }
        }


        return $ls;
    }

    private function children($array, $parent_column, $parent)
    {
        $childList = null;

        foreach ($array as $child) {
            if (isset($child->children)) {
                $childList[] = [
                    "id" => $child->id, $parent_column => $parent, "children" => $this->children($child->children, $parent_column, $child->id)
                ];
            } else {
                $childList[] = ["id" => $child->id, $parent_column => $parent];
            }
        }

        return $childList;
    }
    #endregion
}
