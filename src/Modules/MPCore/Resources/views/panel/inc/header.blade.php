<header class="site-head">
    <i class="bg"></i>
    <div class="container-fluid">
        <div class="logo">
            <a href="{!! route("panel.dashboard") !!}">
                <img src="{!! asset($public_url."images/logo.png") !!}" alt="">
            </a>
        </div>
        @includeWhen(Auth::user() || true,"MPCorePanel::inc.menu")
    </div>
</header>
