<?php

namespace Mediapress\Modules\Heraldist\Foundation;


use Collective\Html\FormFacade;
use Mediapress\Modules\Heraldist\Models\Form;

class FormRenderer //implements HeraldistInterface
{
    public const REQUIRED = 'required';
    public const CLASS_NAME = 'className';
    public const CHILDREN = 'children';
    public const PLACEHOLDER = 'placeholder';
    public $form;
    public $source;
    public $fields = [];
    public $url;
    public $hidden;
    public $style;
    public $script;
    public $rules = [];
    public $labels;

    public function __construct(Form $form)
    {
        $this->form = $form;
        $this->source = json_decode($form->formbuilder_json, 1);
        $this->url = route('form.store');
        $this->hidden();
        $this->style();
        $this->script();
    }

    public function getFieldList()
    {

        return $this->source;
    }

    public function render()
    {


        $this->createFields();

    }

    public function createFields()
    {
        $this->build();


    }

    public function prepareField($field)
    {
        $fieldAttributes = [];
        if (isset($field[self::REQUIRED]) && $field[self::REQUIRED]) {
            $fieldAttributes[self::REQUIRED] = self::REQUIRED;
        }
        if (isset($field[self::CLASS_NAME]) && $field[self::CLASS_NAME]) {
            $fieldAttributes['class'] = $field[self::CLASS_NAME];
        }
        if (!isset($field['value'])) {
            $field['value'] = null;
        }
        if (isset($field[self::PLACEHOLDER]) && $field[self::PLACEHOLDER]) {
            $fieldAttributes[self::PLACEHOLDER] = $field[self::PLACEHOLDER];
        }
        $field['attr'] = $fieldAttributes;

        return $field;
    }

    private function hidden()
    {
        $mediapress = mediapress();

        $hidden_field = '';
        $hidden_field .= csrf_field();
        $hidden_field .= FormFacade::hidden('_id', encrypt($this->form->id));
        $hidden_field .= FormFacade::hidden('_trackid', '');
        if ($mediapress->visit) {
            $hidden_field .= FormFacade::hidden('_visit_id', encrypt($mediapress->visit->id));
        }

        if (isset($mediapress->extra_form_fields)) {
            foreach ($mediapress->extra_form_fields['extras'] as $key => $value) {
                $hidden_field .= FormFacade::hidden($value['field_key'], serialize($value['field_value']));
            }
        }


        $this->hidden = $hidden_field;
    }

    private function style()
    {
        $this->style = '<link rel="stylesheet" href="' . mp_asset("vendor/mediapress/css/intlTelInput.min.css") . '" />';

    }


    private function script()
    {
        if(mediapress()->activeLanguage->code == 'tr') {
            $phoneCodes = '"tr"';
        } else {
            $phoneCodes = '"' . mediapress()->activeLanguage->code . '","us", "gb"';
        }
        $this->script  = '';
        if(config('mediapress.flow.record')){


        $this->script .=
            '<script type="text/javascript"> var _paq = window._paq || [];
    _paq.push([\'trackPageView\']);
    _paq.push([\'enableLinkTracking\']);
    (function() {
        var u=\'' . url('/') . '/\';
        _paq.push([\'setTrackerUrl\', u+\'matomo\']);
        _paq.push([\'setSiteId\', \'1\']);
        var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];
        g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'vendor/mediapress/flow/flow.js\'; s.parentNode.insertBefore(g,s);
    })();
</script>';
        }
  $this->script .= '
<script src="' . mp_asset("vendor/mediapress/js/intlTelInput.min.js") . '"></script>
<script src="' . mp_asset("vendor/mediapress/js/jquery.mask.js") . '"></script>
<script src="' . mp_asset("vendor/mediapress/js/utils.js") . '"></script>
<script>
    var phoneScript = [];
    $.each($(".telmask"), function (key, value) {
        phoneScript.push(window.intlTelInput(value, {
            preferredCountries: ['.$phoneCodes.'],
            separateDialCode: true,
            autoPlaceholder: "polite",
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                return selectedCountryPlaceholder.replace(/[0-9]/g, "0");
            }
        }));
        var obj = $(value);

        obj.on("focus", function() {
            var activePlaceholder = obj.attr("placeholder");
            var newMask = activePlaceholder.replace(/[0-9]/g, "0");
            obj.mask(newMask);
        });

        obj.on("countrychange", function (e, countryData) {
            obj.val("");
            obj.attr("maxlength", 20);
        });
    });


    function movePlus(){
        let objs = $(".telmask");
        $.each(objs, function (key, value) {
            obj = $(value);
            var flag = obj.closest(".iti--allow-dropdown").find("div.iti__selected-dial-code");
            if (obj.val().indexOf("+") == -1 && obj.val() !== "") {
                obj.val(flag.text() + " " + obj.val());
            }
        });
    }

</script>';

    }

    private function build()
    {


        $this->fields = [];

        if($this->source) {
            foreach ($this->source as $key => $objects) {
                if (is_array($objects)) {
                    foreach ($objects as $id => $object) {
                        $object['key'] = $key;
                        $this->fields[$id] = $object;
                    }
                }
            }
            $this->buildTree();
        }
    }

    private function buildTree()
    {
        $stage = null;
        foreach ($this->fields as $key => $item) {
            $item = $this->checkChildren($item);
            $this->fields[$key] = $item;
            if ($item['key'] == 'stages') {
                $stage = $key;
            }
        }

        $this->fields = $this->fields[$stage][self::CHILDREN];
    }

    private function addChildren($item, int $key)
    {

        $child = $this->fields[$item[self::CHILDREN][$key]];
        $child = $this->checkChildren($child);
        $item[self::CHILDREN][$key] = $child;
        return $item;
    }

    private function checkChildren($item)
    {

        $this->checkRules($item);
        if (isset($item[self::CHILDREN])) {
            foreach ($item[self::CHILDREN] as $key => $child) {
                $item = $this->addChildren($item, $key);
            }
        }

        return $item;
    }

    private function checkRules($item)
    {
        if (isset($item['attrs']['rules'])) {
            $this->rules[hash('ripemd128', $item['id'])] = $item['attrs']['rules'];
        }
        if (isset($item['config']['label'])) {
            $field = new Field($item);

            $this->labels[hash('ripemd128', $item['id'])] = $item['config']['label'];
        }
    }


}
