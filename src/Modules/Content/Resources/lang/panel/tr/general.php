<?php

return [
    "status_title" => [
        "passive" => "Pasif",
        "active" => "Aktif",
        "draft" => "Taslak",
        "predraft" => "Öntaslak",
        "pending" => "Bekleyen",
        "postdate" => "İleri Tarihli",
    ],

    'status' => 'Durum',
    'order' => 'Sıra',
    'name' => 'İsim',
    'added_by' => 'Ekleyen',
    'created_at' => 'Oluşturulma Tarihi',
    'updated_at' => 'Son Güncelleme Tarihi',
    'actions' => 'İşlemler',

    "add_new" => "Yeni Ekle",

    "selection" => "Seçiniz",
    "edit" => "Düzenle",
    "delete" => "Sil",
    "cancel" => "İptal",
    "save" => "Kaydet",


    "go_back_list" => "&nbsp;&nbsp;Listeleme Ekranına Dön",



    "swal" => [
        "are_you_sure" => "Emin Misiniz?",
    ],
    "yes" => "Evet",
    "no" => "Hayır",

    "no_data" => "Kayıtlı veri bulunamadı!",



    'max_file_size' => 'Max dosya boyutu :size MB',

    //----------------------

    "select" => "Seçiniz",


    "protected_content" => "Şifre Korumalı İçerik",
    "protected_content_login" => "Sayfaya İlerle",
    "postdate_publish" => "İleri Tarihli",


    "save_as" => [
        "passive" => "Pasif Olarak Kaydet",
        "active" => "Kaydet ve Yayınla",
        "draft" => "Taslak Olarak Kaydet",
        "postdate" => "İleri Tarihli Kaydet",
    ]
];
