<?php

return [
    'admins' => 'Admins',
    'admin_accounts' => 'Admin Accounts',
    'roles' => 'Roles',
    'permissions' => 'Permissions'
];
