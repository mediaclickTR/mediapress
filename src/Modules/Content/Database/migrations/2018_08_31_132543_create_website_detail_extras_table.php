<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateWebsiteDetailExtrasTable extends Migration
    {

        public const WEBSITE_DETAIL_EXTRAS = "website_detail_extras";

        public function up()
        {
            if (!Schema::hasTable(self::WEBSITE_DETAIL_EXTRAS)) {
                Schema::create(self::WEBSITE_DETAIL_EXTRAS, function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('website_detail_id')->index();
                    $table->string('key');
                    $table->text('value');
                    $table->timestamps();
                });
            }
        }

        public function down()
        {
            if (Schema::hasTable(self::WEBSITE_DETAIL_EXTRAS)) {
                Schema::drop(self::WEBSITE_DETAIL_EXTRAS);
            }
        }
    }
