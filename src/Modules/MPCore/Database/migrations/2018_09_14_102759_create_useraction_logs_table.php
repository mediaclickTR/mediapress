<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseractionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('useraction_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('website_id');
            $table->unsignedInteger('user_id');
//            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
//            $table->foreign('user_id')->references('id')->on('admins')->onDelete('cascade');
            $table->integer('module_id')->nullable();
            $table->string('controller')->nullable();
            $table->string('action')->nullable();
            $table->integer('model_id')->nullable();
            $table->string('model_type')->nullable();
            $table->enum('context_type', array('default', 'success','warning','danger','info'));
            $table->string('context_icon')->nullable();
            $table->string('content')->nullable();
            $table->integer('priority')->nullable ();
            $table->integer('parent_id')->nullable();
            $table->string('scope_1')->nullable();
            $table->string('scope_2')->nullable();
            $table->string('scope_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('useraction_logs');
    }
}
