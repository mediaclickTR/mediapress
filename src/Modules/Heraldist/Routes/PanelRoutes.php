<?php
/**
 * Created by PhpStorm.
 * User: Saban Koparal
 * E-Mail: saban.koparal@mediaclick.com.tr
 */

Route::group(['middleware' => 'panel.auth'], function () {
    Route::group(['prefix' => 'Heraldist', 'as'=>'Heraldist.'], function () {

        /**
         * Base Modules
         * Include ebulletin, forms, messages
         */

        Route::group(['prefix' => 'Ebulletins', 'as' => 'ebulletin.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'EBulletinManagerController@index']);
            Route::get('/list', ['as' => 'list.get', 'uses' => 'EBulletinManagerController@memberList']);
            Route::get('/excel/all', ['as' => 'excel.all', 'uses' => 'EBulletinManagerController@excelAll']);
            Route::get('/excel/new', ['as' => 'excel.new', 'uses' => 'EBulletinManagerController@excelNew']);
            Route::post('/list', ['as' => 'list.post', 'uses' => 'EBulletinManagerController@postUrlList']);
            Route::get('/ajax/{website_id?}', ['as' => 'ajax', 'uses' => 'EBulletinManagerController@ajax']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'EBulletinManagerController@formCreate']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'EBulletinManagerController@delete']);
            Route::post('selectedRemove', ['as' => 'selectedRemove', 'uses' => 'EBulletinManagerController@selectedRemove']);
            Route::get('getData', ['as' => 'getData', 'uses' => 'EBulletinManagerController@getData']);

        });

        Route::group(['prefix' => 'Forms', 'as' => 'forms.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'FormController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'FormController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'FormController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'FormController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => 'FormController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'FormController@delete']);
            Route::get('/{id}/details', ['as' => 'details', 'uses' => 'FormController@details']);
            Route::get('/test', ['as' => 'test', 'uses' => 'FormController@test']);
            Route::post('/{id}/details/create', ['as' => 'detail.createOrUpdate', 'uses' => 'FormController@detailsCreateOrUpdate']);
        });

        Route::group(['prefix' => 'Messages', 'as' => 'message.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'MessageController@index']);
            Route::get('show/{form_id}', ['as' => 'show', 'uses' => 'MessageController@show']);
            Route::get('show/{form_id}/export', ['as' => 'export', 'uses' => 'MessageController@export']);
            Route::get('detail/{id}', ['as' => 'detail', 'uses' => 'MessageController@detail']);
            Route::get('notRead/{id}', ['as' => 'notRead', 'uses' => 'MessageController@notRead']);
            Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'MessageController@delete']);
            Route::get('inbox/{id}', ['as' => 'inbox', 'uses' => 'MessageController@inbox']);
            Route::post('changeReadYes', ['as' => 'changeReadYes', 'uses' => 'MessageController@changeReadYes']);
            Route::get('changeReadNo/{id}', ['as' => 'changeReadNo', 'uses' => 'MessageController@changeReadNo']);
            Route::get('markUnread/{id}', ['as' => 'markUnread', 'uses' => 'MessageController@markUnread']);
            Route::get('downloadFile/{path}', ['as' => 'downloadFile', 'uses' => 'MessageController@downloadFile']);
            Route::post('selectedRemove', ['as' => 'selectedRemove', 'uses' => 'MessageController@selectedRemove']);
        });

        /**
         * Mailservice
         * Include MPMailler and Mailchimp mail service
         *
         */

        Route::group(['prefix' => 'Mailservice'], function () {
            // Module Service choosing page
            Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\PanelMailServiceController@index']);

            /**
             * MPMailler Mail Service
             */
            Route::group(['prefix' => 'MPMailler'], function () {
                // Module Service processing page
                Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\MPMailler\PanelMailServiceMPMaillerController@index']);

                Route::group(['prefix' => 'Maillists', 'as' => 'mpmailler.maillists.'], function () {
                    Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@index']);
                    Route::get('/create', ['as' => 'create', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@create']);
                    Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@store']);
                    Route::post('/update', ['as' => 'update', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@update']);
                    Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@delete']);
                    Route::get('{id}/mailSend', ['as' => 'mailsend', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@mailSend']);
                    Route::get('/{id}/mailSendSuccess', ['as' => 'mailsendsuccess', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@mailSendSuccess']);
                    Route::get('/{id}/sendedmails', ['as' => 'sendedmails', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@sendedMails']);
                    Route::get('/{id}/{maillist_id}/sendedmaildelete', ['as' => 'sendedmaildelete', 'uses' => 'Mailservice\MPMailler\PanelMaillistController@sendedMailDelete']);
                });
                Route::group(['prefix' => 'Mailtemplates', 'as' => 'mpmailler.mail_templates.'], function () {
                    Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@index']);
                    Route::get('/create', ['as' => 'create', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@create']);
                    Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@edit']);
                    Route::get('{id}/show', ['as' => 'show', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@show']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@store']);
                    Route::post('/update', ['as' => 'update', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@update']);
                    Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'Mailservice\MPMailler\PanelMailTemplateController@delete']);
                });
                Route::group(['prefix' => 'EmailSender', 'as' => 'mpmailler.email_senders.'], function () {
                    Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@index']);
                    Route::get('/sendeds', ['as' => 'sendeds', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@sendeds']);
                    Route::get('/create', ['as' => 'create', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@create']);
                    Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@edit']);
                    Route::get('{id}/show', ['as' => 'show', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@show']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@store']);
                    Route::post('/update', ['as' => 'update', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@update']);
                    Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@delete']);
                    Route::post('selectedRemove', ['as' => 'selectedRemove', 'uses' => 'Mailservice\MPMailler\PanelEmailSenderController@selectedRemove']);
                });

                Route::group(['prefix' => 'ContactManagement', 'as' => 'mpmailler.contact_management.'], function () {
                    Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\MPMailler\ContactManagementController@index']);
                    Route::get('/create', ['as' => 'create', 'uses' => 'Mailservice\MPMailler\ContactManagementController@create']);
                    Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'Mailservice\MPMailler\ContactManagementController@edit']);
                    Route::get('{id}/show', ['as' => 'show', 'uses' => 'Mailservice\MPMailler\ContactManagementController@show']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'Mailservice\MPMailler\ContactManagementController@store']);
                    Route::post('/update', ['as' => 'update', 'uses' => 'Mailservice\MPMailler\ContactManagementController@update']);
                    Route::post('/send', ['as' => 'send', 'uses' => 'Mailservice\MPMailler\ContactManagementController@send']);
                    Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'Mailservice\MPMailler\ContactManagementController@delete']);
                    Route::post('export', ['as' => 'export', 'uses' => 'Mailservice\MPMailler\ContactManagementController@export']);
                    Route::post('/import', ['as' => 'import', 'uses' => 'Mailservice\MPMailler\ContactManagementController@import']);
                });
            });

            /**
             * Server (Mailchimp) Mail Service
             */

            Route::group(['prefix' => 'Mailchimp', 'as' => 'mailchimp.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'Mailservice\Mailchimp\PanelMaillistController@index']);
                Route::get('/{id}/members', ['as' => 'members.index', 'uses' => 'Mailservice\Mailchimp\PanelMembersController@index']);
            });
        });
    });
});
