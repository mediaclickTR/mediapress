<?php

namespace Mediapress\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Mediapress\Models\Module;

class ReplaceWords extends Command
{
    protected $signature = 'mp:replace {--file=* : Update words in the given file type [controller, renderable]}';


    public function handle()
    {
        if ($this->option('file')) {

            foreach ($this->option('file') as $file) {

                if ($file == 'controller') {
                    $this->replaceInControllers();
                }

                if ($file == 'renderable') {
                    $this->replaceInRenderables();
                }
                $this->info("The words in the " . $file . "s have been updated !!");
            }

        }
    }


    private function replaceInControllers()
    {
        $keyAndValues = [
            "ajaxCategoryCriteria" => "ajaxCriteriaFilter",
            "filterCategoryCriteria" => "filterCriteriaFunc",
        ];

        $websites = array_diff(scandir(app_path("Modules/Content")), array('.', '..'));

        foreach ($websites as $website) {
            if (!\Str::startsWith($website, "Website")) {
                continue;
            }
            $controllers = array_diff(scandir(app_path("Modules/Content/" . $website . "/Http/Controllers/Web")), array('.', '..'));

            foreach ($controllers as $controllerName) {
                $renderable = app_path("Modules/Content/" . $website . "/Http/Controllers/Web/" . $controllerName);
                $oldContent = file_get_contents($renderable);

                $newContent = str_replace(array_keys($keyAndValues), array_values($keyAndValues), $oldContent);

                file_put_contents($renderable, $newContent);
            }
        }
    }

    private function replaceInRenderables()
    {
        $keyAndValues = [
            "pixels_x_min" => "min_width",
            "pixels_x_max" => "max_width",
            "pixels_y_min" => "min_height",
            "pixels_y_max" => "max_height",
            "pixels_x" => "width",
            "pixels_y" => "height",
            "filesize_min" => "min_filesize",
            "filesize_max" => "max_filesize",
            "files_max" => "max_file_count",
        ];

        $websites = array_diff(scandir(app_path("Modules/Content")), array('.', '..'));

        foreach ($websites as $website) {
            if (!\Str::startsWith($website, "Website")) {
                continue;
            }
            $renderableDirs = array_diff(scandir(app_path("Modules/Content/" . $website . "/AllBuilder/Renderables")), array('.', '..'));

            foreach ($renderableDirs as $renderableDir) {
                $renderables = array_diff(scandir(app_path("Modules/Content/" . $website . "/AllBuilder/Renderables/" . $renderableDir)), array('.', '..'));

                foreach ($renderables as $renderableName) {
                    $renderable = app_path("Modules/Content/" . $website . "/AllBuilder/Renderables/" . $renderableDir . "/" . $renderableName);
                    $oldContent = file_get_contents($renderable);

                    $newContent = str_replace(array_keys($keyAndValues), array_values($keyAndValues), $oldContent);

                    file_put_contents($renderable, $newContent);
                }
            }
        }
    }
}
