<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\MPMailler;

use Mediapress\Http\Controllers\PanelController as Controller;

class PanelMailServiceMPMaillerController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.mpmailler.processing');
    }
}