<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlotSeeder extends Seeder
{
    public const DESCRIPTION = 'description';
    public const SLOT = 'slot';
    public const ID = 'id';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slots')->delete();
        DB::table('slots')->insert([
            [self::ID => 1, self::SLOT => 'Core', self::DESCRIPTION => 'Core Slot'],
            [self::ID => 2, self::SLOT => 'Content', self::DESCRIPTION => 'Content manager'],
            [self::ID => 3, self::SLOT => 'Entity', self::DESCRIPTION => 'User and entity manager'],
            [self::ID => 4, self::SLOT => 'Messaging', self::DESCRIPTION => 'Form and message manager'],
            [self::ID => 5, self::SLOT => 'Auth', self::DESCRIPTION => 'Auth manager'],
            [self::ID => 6, self::SLOT => 'TagManager', self::DESCRIPTION => 'Tag Manager']
        ]);

    }
}
