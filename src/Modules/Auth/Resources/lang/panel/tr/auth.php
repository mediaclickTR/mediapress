<?php

return [
    'failed' => 'E-Posta adresiniz veya şifreniz hatalı tekrar deneyiniz.',
    "sections"=>[
        "roles"=>"Roller",
        "admins"=>"Yöneticiler",
        "abilities"=>"Yetkiler"
    ],
    "actions"=>[
        "manage_components"=>"Elemanları Yönet",
        "list" => "Listele",
        "create" => "Ekleme",
        "update" => "Güncelleme",
        "delete" => "Silme",
    ]
];
