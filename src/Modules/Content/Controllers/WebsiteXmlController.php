<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Content\Models\LanguageWebsite;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Support\Facades\DB;

class WebsiteXmlController extends Controller
{
    public const PAGES = 'pages';
    public const DEFAULT1 = 'default';
    public const SITEMAP = 'sitemap';
    public const CONTENT_PANEL_XML_MAIN = 'ContentPanel::xml.main';
    public const DEFAULT_LANG = 'defaultLang';
    public const CONTENT_TYPE = 'Content-Type';
    public const APPLICATION_XHTML_XML = 'application/xhtml+xml';
    public const MODEL_TYPE = ":model_type";
    public const DATA_ID = 'data_id';
    public const CONTENT_PANEL_XML_XML = 'ContentPanel::xml.xml';
    private $defaultLimit = 1500;

    private $keys = [
        self::PAGES => "Mediapress\Modules\Content\Models\PageDetail",
        'sitemaps' => "Mediapress\Modules\Content\Models\SitemapDetail",
        'categories' => "Mediapress\Modules\Content\Models\CategoryDetail"
    ];

    private $queries = [
        self::PAGES => "SELECT u.id as url_id, pd.page_id as data_id, u.model_type as model_type, pd.language_id as lang_id, u.url as url, meta.indexed FROM urls as u
                    INNER JOIN (SELECT pad.id, pad.page_id, pad.name,pad.language_id FROM `page_details` as pad inner join pages as p on pad.page_id = p.id where p.deleted_at IS null) as pd on u.model_id = pd.id
					LEFT JOIN metas as meta on u.id = meta.url_id
                    WHERE u.type = 'original' AND u.deleted_at IS null AND u.model_type = :model_type AND (meta.indexed = 1 OR  meta.indexed IS null) order by pd.page_id LIMIT :start,:limit",

        'categories' => "SELECT u.id as url_id, cd.category_id as data_id, u.model_type as model_type, cd.language_id as lang_id, u.url as url, meta.indexed FROM urls as u
                    INNER JOIN (SELECT cad.id, cad.category_id, cad.language_id FROM `category_details` as cad inner join categories as c on cad.category_id = c.id where c.deleted_at IS null) as cd on u.model_id = cd.id
                    LEFT JOIN metas as meta on u.id = meta.url_id
                    WHERE u.type = 'original' AND u.deleted_at IS null AND u.model_type = :model_type AND (meta.indexed = 1 OR  meta.indexed IS null) order by cd.category_id",

        'sitemaps' => "SELECT u.id as url_id, sd.sitemap_id as data_id, u.model_type as model_type, sd.language_id as lang_id, u.url as url, meta.indexed FROM urls as u
                    INNER JOIN (SELECT sid.id, sid.sitemap_id, sid.language_id FROM `sitemap_details` as sid inner join sitemaps as s on sid.sitemap_id = s.id where s.deleted_at IS null) as sd on u.model_id = sd.id
                    LEFT JOIN metas as meta on u.id = meta.url_id
                    WHERE u.type = 'original' AND u.deleted_at IS null AND u.model_type = :model_type AND (meta.indexed = 1 OR  meta.indexed IS null) order by sd.sitemap_id"
    ];

    private function defaultLang()
    {
        $defaultLangId = LanguageWebsite::where(self::DEFAULT1, '1')->first()->language_id;
        return Language::where('id', $defaultLangId)->select('id', 'code')->first();
    }

    private function getLanguages()
    {
        $db = DB::select("SELECT language_id as langId FROM `language_website` ORDER BY `default` DESC");
        $lang = [];
        foreach ($db as $data) {
            $langCode = Language::where('id', $data->langId)->select('id', 'code')->first()->code;
            $lang[] = $langCode;
        }
        return array_unique($lang);
    }

    public function mainXmlPage($xml)
    {
        if ($xml == self::SITEMAP) {
            $defaultLang = $this->defaultLang()->code;
            foreach ($this->keys as $key => $value) {
                $data[$key][$defaultLang] = $this->setUrl([self::DEFAULT1, $key]);
            }
            return response()->view(self::CONTENT_PANEL_XML_MAIN, compact('data', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
        }
    }

    public function xmlPage($type, $xml)
    {
        if ($xml != self::SITEMAP){
            notFound();
        }
        if (!array_key_exists($type, $this->keys)){
            notFound();
        }
        if ($type == self::PAGES) {
            return $this->mainPageForPages($xml, $this->getCount());
        } else {
            $defaultLang = $this->defaultLang()->code;
            $db = DB::select($this->queries[$type], [self::MODEL_TYPE => $this->keys[$type]]);
            $db = collect($db)->groupBy(self::DATA_ID);
            $urls = [];
            foreach ($db as $key => $data) {
                foreach ($data as $key => $value) {
                    $langCode = Language::where('id', $value->lang_id)->select('id', 'code')->first()->code;
                    $urls[$value->data_id][$langCode] = url($value->url);
                }

            }
        }
        return response()->view(self::CONTENT_PANEL_XML_XML, compact('urls', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
    }

    public function mainPageForPages($xml, $count)
    {
        if ($xml == self::SITEMAP) {
            $defaultLang = $this->defaultLang()->code;
            if ($count == 1) {
                return $this->xmlPageForPages(self::PAGES, 1, $xml);
            } else {
                for ($i = 1; $i <= $count; $i++) {
                    foreach ($this->getLanguages() as $langs) {
                        $data[$i][$langs] = $this->setUrl([$langs == $defaultLang ? self::DEFAULT1 : $langs, self::PAGES, $i]);
                    }
                }
            }
            return response()->view(self::CONTENT_PANEL_XML_MAIN, compact('data', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
        }
    }

    public function xmlPageForPages($type, $limit, $xml)
    {
        if ($xml != self::SITEMAP){
            notFound();
        }
        if ($type != self::PAGES){
            notFound();
        }
        $defaultLang = $this->defaultLang()->code;
        $db = DB::select($this->queries[$type], [self::MODEL_TYPE => $this->keys[$type], ":start" => ($limit - 1) * $this->defaultLimit, ":limit" => $this->defaultLimit]);
        $db = collect($db)->groupBy(self::DATA_ID);
        $urls = [];
        foreach ($db as $key => $data) {
            foreach ($data as $key => $value) {
                $langCode = Language::where('id', $value->lang_id)->select('id', 'code')->first()->code;
                $urls[$value->data_id][$langCode] = url($value->url);
            }

        }
        return response()->view(self::CONTENT_PANEL_XML_XML, compact('urls', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
    }

    public function mainPageLang($lang, $xml)
    {
        if ($xml == self::SITEMAP) {
            $defaultLang = $lang;
            foreach ($this->keys as $key => $value) {
                $data[$key][$defaultLang] = $this->setUrl([$defaultLang, $key]);
            }
            return response()->view(self::CONTENT_PANEL_XML_MAIN, compact('data', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
        }
    }

    public function xmlPageLang($lang, $type, $xml)
    {
        if ($xml != self::SITEMAP){
            notFound();
        }
        if (!array_key_exists($type, $this->keys)){
            notFound();
        }

        if ($type == self::PAGES) {
            return $this->mainPageForPagesLang($xml, $this->getCount($lang), $lang);
        } else {
            $defaultLang = Language::where('code', strtolower($lang))->select('id', 'code')->first()->code;
            $db = DB::select($this->queries[$type], [self::MODEL_TYPE => $this->keys[$type]]);
            $db = collect($db)->groupBy(self::DATA_ID);
            $urls = [];
            foreach ($db as $key => $data) {
                foreach ($data as $key => $value) {
                    $langCode = Language::where('id', $value->lang_id)->select('id', 'code')->first()->code;
                    $urls[$value->data_id][$langCode] = url($value->url);
                }

            }
        }

        return response()->view(self::CONTENT_PANEL_XML_XML, compact('urls', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
    }

    public function mainPageForPagesLang($xml, $count, $lang)
    {
        if ($xml == self::SITEMAP) {
            $defaultLang = $lang;
            if ($count == 1) {
                return $this->xmlPageForPagesLang($lang, self::PAGES, 1, $xml);
            } else {
                for ($i = 1; $i <= $count; $i++) {
                    foreach ($this->getLanguages() as $langs) {
                        $data[$i][$langs] = $this->setUrl([$langs == $this->defaultLang()->code ? self::DEFAULT1 : $langs, self::PAGES, $i]);
                    }
                }
            }
            return response()->view(self::CONTENT_PANEL_XML_MAIN, compact('data', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
        }
    }

    public function xmlPageForPagesLang($lang, $type, $limit, $xml)
    {
        if ($xml != self::SITEMAP){
            notFound();
        }
        if ($type != self::PAGES){
            notFound();
        }
        $db = DB::select($this->queries[$type], [self::MODEL_TYPE => $this->keys[$type], ":start" => ($limit - 1) * $this->defaultLimit, ":limit" => $this->defaultLimit]);
        $db = collect($db)->groupBy(self::DATA_ID);
        $urls = [];
        foreach ($db as $key => $data) {
            foreach ($data as $key => $value) {
                $langCode = Language::where('id', $value->lang_id)->select('id', 'code')->first()->code;
                $urls[$value->data_id][$langCode] = url($value->url);
            }
        }
        $defaultLang = $lang;
        return response()->view(self::CONTENT_PANEL_XML_XML, compact('urls', self::DEFAULT_LANG))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
    }

    public function setUrl($url)
    {
        $last = "";
        $data = explode('/', $_SERVER['REQUEST_URI']);
        foreach ($url as $d) {
            $last .= "/" . $d;
        }
        $last .= "/" . last($data);
        return url($last);
    }

    public function getCount($lang = null)
    {
        $pages_count = "SELECT COUNT(*) as total FROM urls as u
                    INNER JOIN (SELECT pad.id, pad.page_id, pad.name,pad.language_id FROM `page_details` as pad inner join pages as p on pad.page_id = p.id where p.deleted_at IS null AND pad.language_id = :lang_id) as pd on u.model_id = pd.id
                    WHERE u.type = 'original' AND u.deleted_at IS null AND u.model_type = :model_type";

        if ($lang == null) {
            $defaultLangId = Language::where(self::DEFAULT1, '1')->first()->id;
        } else {
            $defaultLangId = Language::where('code', $lang)->select('id', 'code')->first()->id;
        }
        $count = DB::select($pages_count, [self::MODEL_TYPE => $this->keys[self::PAGES], ":lang_id" => $defaultLangId]);
        return ceil(array_shift($count)->total / $this->defaultLimit);

    }
}
