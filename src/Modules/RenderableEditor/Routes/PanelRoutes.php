<?php
/**
 * Created by PhpStorm.
 * User: Saban Koparal
 * E-Mail: saban.koparal@mediaclick.com.tr
 */

Route::group(['middleware' => 'panel.auth'], function()
{
    Route::group(['prefix' => 'RenderableEditor'], function () {

        /**
         * Base Modules
         */

        Route::group(['prefix' => 'Editor', 'as' => 'admin.editors.'], function()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'EditorController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'EditorController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'EditorController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'EditorController@edit']);
            Route::get('/{id}/show', ['as' => 'edit', 'uses' => 'EditorController@show']);
            Route::post('/update', ['as' => 'update', 'uses' => 'EditorController@update']);
            Route::post('/cloud', ['as' => 'cloud', 'uses' => 'EditorController@cloud']);
            Route::get('/layouts', ['as' => 'layouts', 'uses' => 'EditorController@layouts']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'EditorController@delete']);
            Route::post('/ajax/RenderableOption/{tag?}', ['as' => 'ajax.post', 'uses' => 'AjaxEditorController@ajaxGet']);
        });




    });
});