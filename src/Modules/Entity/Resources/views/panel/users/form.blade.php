@if(!isset($user))
    {{ Form::open(['route' => 'Entity.users.store']) }}
@else
    {{ Form::model($user, ['route' => ['Entity.users.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$user->id) !!}
    <input type="hidden" name="old_email" value="{!! $user->email !!}">
@endif
@csrf
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group rel">
                    <label>{!! trans("EntityPanel::entitylist.select_websites") !!}</label><br/><br/>
                    {!!Form::select('website_id', $websites,null, ['class' => 'nice','required', 'placeholder'=>trans("MPCorePanel::general.selection")])!!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.name-user") !!}</label>
            {!!Form::text('name', null, ["placeholder"=>trans("EntityPanel::user.name-user"), "class"=>"validate[required]"])!!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.email-user") !!}</label>
            {!!Form::text('email', null, ["placeholder"=>trans("EntityPanel::user.email-user"), "class"=>"validate[required]"])!!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.password-user") !!}</label>
            <input type="password" id="password" placeholder="{!! trans("EntityPanel::user.password-user") !!}" name="password">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.repeat-password-user") !!}</label>
            {!!Form::password('password_confirmation', null, ["placeholder"=>trans("EntityPanel::user.repeat-password-user")])!!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.network_label") !!}</label>
            {!! Form::select('extras[network]', array('0' => 'Hayır', '1' => 'Evet'), @$user->extras()->where('user_extras.key','network')->first()->value); !!}
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::user.network_name_label") !!}</label>
            {!!Form::text('extras[network_name]', @$user->extras()->where('user_extras.key','network_name')->first()->value, ["placeholder"=>trans("EntityPanel::user.network_name_text"), "class"=>"validate[required]"])!!}
        </div>
    </div>
</div>
<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

@push('specific.scripts')
    <script type="text/javascript" src="{!! asset("vendor/mediapress/js/jquery.password-validation.js") !!}"></script>
@endpush

@push("scripts")
    <script>
        //$('#password').passwordStrength();
    </script>
@endpush
