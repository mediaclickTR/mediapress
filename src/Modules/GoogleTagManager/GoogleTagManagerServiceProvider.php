<?php

namespace Mediapress\Modules\GoogleTagManager;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\GoogleTagManager\Facades\GoogleTagManager;

class GoogleTagManagerServiceProvider extends ServiceProvider
{
    public const TAGMANAGER = "GoogleTagManager";
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = self::TAGMANAGER;
    protected $namespace = 'Mediapress\Modules\GoogleTagManager';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->publishActions(__DIR__);

        $files = $this->app['files']->files(__DIR__ . '/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }

    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias(self::TAGMANAGER, GoogleTagManager::class);
        app()->bind(self::TAGMANAGER, function () {
            return new \Mediapress\Modules\GoogleTagManager\GoogleTagManager;
        });
    }
}
