<div class="modal fade" id="sendCloudModal" tabindex="-2" role="dialog" aria-labelledby="sendCloudModal2"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="javascript:sendToCloudAjax();" class="form-holder-ajax">
                <div class="modal-header">
                    <h5 class="modal-title" id="#sendToCloud">SendToCloud</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="structure" class="json_output">
                    <input type="hidden" name="username"
                           value="{!! \Illuminate\Support\Facades\Auth::guard('admin')->user()->username !!}">
                    <input type="text" name="name" class="form-control" required placeholder="Layout Name">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>