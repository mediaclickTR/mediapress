@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title">{!! __("ContentPanel::menu.copy_menu") !!}</div>
            </div>
        </div>
        <div class="p-30">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::open(['route' => 'Content.menus.copy']) }}
                    @csrf

                    {!!Form::hidden('menu_id', $menu_id)!!}


                    <div class="row">

                        <div class="" style="margin-right: 20%">
                            <div class="" id="source_country_group">
                                <div class="tit">{{ __('ContentPanel::menu.source_country_group') }}</div>
                                <select class="nice" name="source_country_group">
                                    <option value="">{{ __('ContentPanel::general.selection') }}</option>
                                    @foreach($country_groups as $group)
                                        <option
                                            {{old('source_country_group') == $group->id ? 'selected' : ''}} value="{{$group->id}}">{{$group->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="source_lang" id="source_language" style="display: none">
                                <div class="tit">{{ __('ContentPanel::menu.source_language') }}</div>
                                <select class="nice lang" name="source_language">
                                    <option value="">{{ __('ContentPanel::general.selection') }}</option>
                                </select>
                            </div>
                        </div>


                        <div class="rel">

                            <div class="" id="target_country_group">
                                <div class="tit">{{ __('ContentPanel::menu.target_country_group') }}</div>
                                <select class="nice" name="target_country_group">
                                    <option value="">{{ __('ContentPanel::general.selection') }}</option>
                                    @foreach($country_groups as $group)
                                        <option {{old('target_country_group') == $group->id ? 'selected' : ''}} value="{{$group->id}}">{{$group->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="target_lang" id="target_language" style="display: none">
                                <div class="tit">{{ __('ContentPanel::menu.target_language') }}</div>
                                <select class="nice lang" name="target_language">
                                    <option value="">{{ __('ContentPanel::general.selection') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="float-right">
                        <button
                            class="btn btn-primary"
                            id="submit-btn">{!! __("ContentPanel::menu.copy_menu") !!}</button>
                    </div>
                    <div class="clearfix"></div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        @if(old('source_country_group'))
            ajax("{{old('source_country_group')}}", 'source_language');
        @endif

        @if(old('target_country_group'))
            ajax("{{old('target_country_group')}}", 'target_language');
        @endif


        $('select[name="source_country_group"]').on('change', function () {
            var country_group_id = $(this).val();

            ajax(country_group_id, 'source_language');
        });

        $('select[name="target_country_group"]').on('change', function () {
            var country_group_id = $(this).val();

            ajax(country_group_id, 'target_language')
        });

        function ajax(country_id, type) {
            $.ajax({
                'url': "{{route('Content.menus.copyAjax')}}",
                'data': {'country_id': country_id, '_token': "{{csrf_token()}}"},
                'method': "post",
                success: function (result) {
                    content = '';
                    $.each(result, function (k, v) {
                        if((k == "{{old('source_language')}}" && type == 'source_language') || (k == "{{old('target_language')}}" && type == 'target_language')) {
                            content += '<option selected value="' + k + '">' + v + '</option>'
                        } else {
                            content += '<option value="' + k + '">' + v + '</option>'
                        }
                    });

                    $('select[name="' + type + '"]').append(content);
                    $('#' + type).show();
                }
            });
        }


        $('#submit-btn').on('click', function (e) {
            e.preventDefault();
            var obj = $(this);
            swal({
                title: '{{ __("ContentPanel::general.swal.are_you_sure") }}',
                text: '{!! __("ContentPanel::menu.copy.info") !!}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! __("ContentPanel::general.yes") !!}',
                cancelButtonText: '{!! __("ContentPanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    obj.closest('form').submit();
                }
            });
        });
    </script>
@endpush
