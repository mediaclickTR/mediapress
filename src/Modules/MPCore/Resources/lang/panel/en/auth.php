<?php

return [
    "general" => [
        "shared_abilities" => "Common Autorization",
        "recorded_items" => "Saved Items",
    ],
    "sections" => [
        "userlogs" => "User Actions",
        "translations" => "Translations",
        "modules" => "Modules",
        "module" => "Modul",
    ],
    "actions" => [
        "view" => "See / Open",
        "list" => "List",
        "create" => "Create",
        "update" => "Update",
        "delete" => "Delete",
        "reorder" => "Sort",
        "import" => "Import",
        "export" => "Export",
        "auth" => "Authorize",
        "export2excel" => "Import to Excel",
        "update_level1_settings" => "1. Change Level Settings",
        "update_level2_settings" => "2. Change Level Settings",
        "update_level3_settings" => "3. Change Level Settings",
    ]
];
