<?php

namespace Mediapress\Facades;

use Illuminate\Support\Facades\Facade;

class DataSourceEngine extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Foundation\DataSourceEngine::class;
    }

}