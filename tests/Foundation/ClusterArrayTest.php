<?php


namespace Mediapress\Tests\Foundation;


use Mediapress\Foundation\ClusterArray;
use PHPUnit\Framework\TestCase;

class ClusterArrayTest extends TestCase
{
    public const PARENT = 'parent';
    public const NAME = 'name';
    public const ID = 'id';

    public $array = [
        [self::ID => 5,'menu_part_id' => 5, self::NAME => 'one', self::PARENT => 1],
        [self::ID => 6,'menu_part_id' => 5, self::NAME => 'two', self::PARENT => 1],
        [self::ID => 7,'menu_part_id' => 5, self::NAME => 'three', self::PARENT => 5]
    ];


    /**
     * @covers \Mediapress\Foundation\ClusterArray
     */
    public function testCluster(){

        $cluster = new ClusterArray();

        $tree = $cluster->cluster($this->array,1,0,0,1);

        $this->assertArrayHasKey('children', $tree[0]);
        $tree = $cluster->cluster($this->array);
        $this->assertArrayNotHasKey('children', $tree[0]);
        $tree = $cluster->buildFromDB($this->array, 1);
        $this->assertArrayHasKey('children', $tree[0]);
        $tree = $cluster->buildTreeWithNestableParameters($this->array, ['id'=>5],5,1);
        $this->assertArrayNotHasKey('children', $tree[0]);
    }
    /**
     * @covers \Mediapress\Foundation\ClusterArray::buildArray
     */
    public function testBuildArray()
    {
        $cluster = new ClusterArray();
        $tree = $cluster->buildArray($this->array, 1);

        $this->assertArrayHasKey('children', $tree[0]);
    }

    /**
     * @covers \Mediapress\Foundation\ClusterArray::unBuildTree
     */
    public function testUnBuildTree(){
        $cluster = new ClusterArray();
        $tree = $cluster->buildArray($this->array, 1);
        $untree = $cluster->unBuildTree($tree);
        $this->assertArrayHasKey(2, $untree);
    }


}