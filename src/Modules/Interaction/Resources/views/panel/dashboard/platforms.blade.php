<div class="bd-example miniBoxes">
    <div class="row">
        <div class="col">
            <div class="card p-4">
                <div class="card-title color m-0">{{ __("InteractionPanel::dashboard.platforms.title") }}</div>
                <div class="card-body p-0">
                    <div id="platform-pie" class="pieChartAll"></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card p-4">
                <div class="card-title color m-0">{{ __("InteractionPanel::dashboard.platforms.hourly") }}</div>
                <div class="card-body p-0">
                    <div id="pieTimes" class="pieChartAll"></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card p-4">
                <div class="card-title color m-0">{{ __("InteractionPanel::dashboard.platforms.referers") }}</div>
                <div class="card-body p-0">
                    <div id="referer-pie" class="pieChartAll"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.graphData.hourly = @json($data['hourly']);
    window.graphData.platform = @json($data['platform']);
    window.graphData.referers = @json($data['referers']);
</script>
<script src="{!! asset('vendor/mediapress/js/dashboardPlatform.js') !!}"></script>
<script src="{!! asset('vendor/mediapress/js/dashboardTimes.js') !!}"></script>
