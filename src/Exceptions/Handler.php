<?php

namespace Mediapress\Exceptions;

use Exception;
use Mediapress\Foundation\Mediapress;
use Mediapress\Foundation\MediapressBasic;
use ReflectionException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\App;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
        /*NotFoundException::class,
        RedirectFoundException::class,
        AuthenticationException::class,*/
    ];

    protected $notCustomArray = [
        /*NotFoundException::class,
        ReflectionException::class,
        AuthenticationException::class,
        RedirectFoundException::class,
        InvalidArgumentException::class,
        ClassNotFoundException::class*/
    ];

    public function decideCustomError($e)
    {
        foreach ($this->notCustomArray as $item) {
            if ($e instanceof $item) {
                return false;
            }
            abort();

        }
        return true;
    }

    public function render($request, Exception $exception)
    {

        if ($exception instanceof NotFoundHttpException) {


            /** @var Mediapress $mediapress */
            $mediapress = mediapress();
            return response()->view('errors.404', compact('mediapress'))->setStatusCode(404);

        } elseif ($exception instanceof RedirectFoundException) {
            return redirect()->to($exception->getMessage())->setStatusCode($exception->getCode());
        } elseif (!App::environment('Local') && ($this->decideCustomError($exception))) {
            if (view()->exists('errors.custom')) {
                return response()->view('errors.custom', compact('exception'));
            }
        } elseif (App::environment('Local')) {
            if (view()->exists('errors.custom.Local')) {
                return response()->view('errors.custom.Local', compact('exception'));
            }
        }
        return parent::render($request, $exception);
    }

    public function unauthenticated($request, AuthenticationException $exception)
    {
        return redirect()->guest(panel('login'));
    }
}
