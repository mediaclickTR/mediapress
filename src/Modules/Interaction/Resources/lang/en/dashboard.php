<?php

return [

    "title" => "Dashboard",

    "dates" => [
        'today' => 'Today',
        'yesterday' => 'Yesterday',
        'week' => 'Last 7 Days',
        'month' => 'Last 30 Days',
        'three-month' => 'Last 90 Days',
        'six-month' => 'Last 180 Days',
        'year' => 'Last 365 Days',
    ],

    "connection_warning" => ":type analytics information could not be matched. Please check your information.",
    "google_connect" => "Connect With Google Analytics",

    "interaction_title" => "You're viewing Mediapress Analytics data right now.",
    "google_title" => "You're viewing Google Analytics data right now.",
    "google_disconnect" => "Disconnect From Google Analytics",

    "google_connect_title" => "Mediapress Google Link Tool",

    "gender" => [
        "title" => "Gender",
        "male" => "Male",
        "female" => "Female"
    ],

    "ages" => "Age Distribution",
    "browser" => "Internet Browser",
    "os" => "Operating System",
    "countries" => "Visitors by Country",


    "online" => "Online",
    "load_time" => "Avg. Opening Time",
    "visit_time" => "Avg. Session Duration",
    "bounces" => "Bounce Rate",

    "avg_visit" => "Average Page Visit",

    "platforms" => [
        "title" => "Platform",
        "hourly" => "Weekly Density by Hour",
        "referers" => "Traffic Channels",
    ],

    "stats" => [
        "refresh" => "Refresh",
        "visits" => "Pageview",
        "session" => "Session",
        "cookie" => "User",
    ],

    "most_visited" => "Most Visited Pages",
    "affinities" => "Visitors' Interests",
];
