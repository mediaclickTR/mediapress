<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ebulletin extends Model
{
    use SoftDeletes;
    protected $table = 'e_bulletins';
    protected $primaryKey = 'id';
    protected $fillable = ['website_id','name','email','phone', 'activated'];

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
}
