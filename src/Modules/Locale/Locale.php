<?php

namespace Mediapress\Modules\Locale;

use Mediapress\Models\MPModule;

class Locale extends MPModule
{
    public $name = "Locale";
    public $url = "mp-admin/Locale";
    public $description = "Locale";
    public $author = "";
    public $menus = [];
    public $plugins = [];

}
