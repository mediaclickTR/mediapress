<?php

    namespace Mediapress\Foundation;

    class ImageClosure
    {
        private $gd;
        private $path;
        private $methods;

        public function __construct($gd, &$path)
        {
            $this->gd = $gd;
            $this->path = &$path;
            $this->methods = [];
        }

        public function __call($name, $arguments)
        {
            if (!method_exists($this, $name)) {
                $argText = array_filter($arguments, function ($el) {
                    return gettype($el) != "object";
                });
                $this->path .= "__$name-" . implode('_', $argText);
                $this->methods[][$name] = $arguments;
                return $this;
            }
        }

        public function getGD()
        {
            return $this->gd;
        }

        public function apply()
        {
            foreach ($this->methods as $met){
                foreach ($met as $key => $arg){
                    $this->gd->{$key}(...$arg);
                }
            }
        }

        public function save()
        {
            return $this->gd->save(...func_get_args());
        }

    }