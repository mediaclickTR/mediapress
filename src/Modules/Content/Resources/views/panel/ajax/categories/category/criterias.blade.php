<div class="modal-header">
    <h4 class="modal-title">{!! trans("ContentPanel::categories.criterias") !!} ({!! $category->detail->name !!})</h4>
    <button type="button" class="close float-right" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Kapat</span>
        </button>
        Bu kategoriye bağlamak istediğiniz kriterleri seçin.
    </div>
    <form method="POST">
        @csrf
        <div class="form-group rel">
            <select name="criterias[]" id="pre-selected-options" multiple="multiple" class="criterias">
                @foreach($criterias as $item)
                    <option {{ (!$item->categories->isEmpty()) ? 'selected': ''  }} value="{{ $item->id }}">{{ ($item->details->first()) ? $item->details->first()->name : '-' }}</option>
                @endforeach
            </select>
        </div>
        <input type="hidden" name="category_id" value="{{ $data['parent_id'] }}">
        <div class="float-right">
            <button type="submit" class="btn btn-primary" id="property_submit">{!! trans("MPCorePanel::general.save") !!}</button>
        </div>
    </form>
</div>
<script>
    $(document).ready(function ()
    {
        $('#pre-selected-options').multiSelect({
            selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
            selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
        });

        //fonksiyonlar("popup");
        $('#property_submit').click(function(e){
            e.preventDefault();
            var criterias = $(".criterias").val();
            var category_id = $("input[name='category_id']").val();
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ route('Content.categories.category.criteriaSave', $category->sitemap_id) }}",
                method: 'POST',
                data: {category_id:category_id,criterias:criterias,_token:token},
                success: function(data) {
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    });
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        })
    });
</script>
