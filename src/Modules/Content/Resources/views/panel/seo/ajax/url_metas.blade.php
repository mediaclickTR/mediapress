<div class="modal-header">
    <h2>{!! trans("ContentPanel::seo.url.metas", ['url'=>$url->url]) !!}</h2>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:closeModal(this);">×
    </button>
</div>
<div class="modal-body">
    <div class="content">
        <div class="right">
            <a class="btn btn-primary" href="#" onclick="javascript:closeModal(this);"> <i class="fa fa-backward"></i>{!! trans("ContentPanel::seo.back.metas") !!}</a>
        </div>
        <div class="clearfix"></div>
        <div id="modalJsGrid">
        </div>
    </div>
</div>

    <script>

        @if(\App::getLocale() != 'en')
        jsGrid.locale("tr");
        @endif

        var metas = {!! json_encode($data['metas']) !!};

        var meta_types = {!! json_encode($data['metaTypes']) !!};

        $(function() {

                $("#modalJsGrid").jsGrid({
                    height: "500px",
                    width: "100%",

                    filtering: true,
                    inserting: true,
                    editing: true,
                    sorting: true,
                    paging: true,
                    data: metas,

                    pageSize: 15,
                    pageButtonCount: 5,

                    controller: metas,

                    deleteConfirm:"{!! trans("MPCorePanel::general.sure_deletion") !!}",

                    fields: [
                        { name: "key", type: "select", items: meta_types, valueField: "key", textField: "value", title: "{!! trans("ContentPanel::seo.meta.type") !!}", width: 100, validate: "required"},
                        { name: "desktop_value", title: "{!! trans("ContentPanel::seo.desktop.value") !!}", type: "text", width: 100,validate: "required"},
                        { type: "control" }
                    ],

                    onItemUpdating: function (args) {
                        var item = args.item;
                        var p1, p2, p3, p4, p5, p6;
                        var data_ = {};

                        p1 = 'key[' + item.id + '][key]';
                        p2 = 'key[' + item.id + '][desktop_value]';


                        data_[p1] = item.key;
                        data_[p2] = item.desktop_value;

                        data_['_token'] = '{!! csrf_token() !!}';
                        return $.ajax({
                            type: "POST",
                            url: "{!! route("Content.seo.listUpdate") !!}",
                            data: data_,
                            async: false,
                            dataType: "json",
                            success: function (data) {
                                console.log(data);
                            },
                            error: function (status1) {
                            }
                        });
                    },
                    onItemInserting: function(args) {
                        var item = args.item;
                        var data_ = {};
                        data_["url_id"] = {!! $url->id !!};
                        data_["key"] = item.key;
                        data_["value"] = item.value;
                        data_['_token'] = '{!! csrf_token() !!}';

                        return $.ajax({
                            type: "POST",
                            url: "{!! route("Content.seo.storeMeta") !!}",
                            data: data_,
                            async: false,
                            dataType: "json",
                            success: function (data) {
                                if (data[0] == true){
                                    swal({
                                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                                    });
                                }else{
                                    swal({
                                        title: '{{ trans("MPCorePanel::general.danger_title") }}',
                                        text: '{{ trans("ContentPanel::seo.insert.error.message") }}',
                                        type: 'error',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                                    });
                                    args.cancel = true
                                }

                            },
                            error: function (status1) {
                            }
                        });
                    },

                    controller: {
                        loadData: function (filter) {
                            return $.grep(metas, function (item) {
                                return (
                                    (!filter.key || item.key.indexOf(filter.key) > -1) &&
                                    (!filter.value || item.value.indexOf(filter.value) > -1)
                                );
                            });
                        },
                        deleteItem: function(item) {
                            var data_ = {};
                            data_["item_id"] = item.id;
                            data_['_token'] = '{!! csrf_token() !!}';
                            return $.ajax({
                                type: "POST",
                                url: "{!! route("Content.seo.listDelete") !!}",
                                data: data_,
                                async: false,
                                dataType: "json",
                                success: function (data) {
                                    swal({
                                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                                    })
                                },
                                error: function (status1) {
                                }
                            });
                        },

                    }
                });
            });


        $(document).ready(function(){
            $("#insertMeta").click(function(){
                $("#insertField").toggle();
            });
        });

    </script>

@push("styles")
    <style>
        #insertField{
            display:none;
        }
    </style>
@endpush
