<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;

class PopupCustomUrl extends Model
{

    protected $table = 'popup_custom_urls';

    public $timestamps = false;

    protected $fillable = [
        "popup_id",
        "url",
    ];


    public function popup()
    {
        return $this->belongsTo(Popup::class);
    }
}
