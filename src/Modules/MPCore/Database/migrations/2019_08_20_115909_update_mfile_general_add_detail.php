<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class UpdateMfileGeneralAddDetail extends Migration
    {

        public function up()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('mfile_general')) {
                $schema->table('mfile_general', function (Blueprint $table) {
                    $table->text('details')->nullable();
                });
            }
        }

        public function down()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('mfile_general')) {
                $schema->table('mfile_general', function (Blueprint $table) {
                    $table->dropColumn('details');
                });
            }
        }
    }
