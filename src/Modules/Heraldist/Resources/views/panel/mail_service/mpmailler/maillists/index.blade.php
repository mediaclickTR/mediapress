@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.maillists") !!}</h1>
            @if(count($maillists)==0)
            <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                <select id="multiple-processings" onchange="locationOnChange(this.value);">
                    <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                    <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!}  value="?list=all">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                </select>
                @if(request()->get('list') && request()->get('list')=="all")
                    <a href="{!! route("Heraldist.mpmailler.maillists.index") !!}" class="filter-block">
                        <i class="fa fa-remove"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                    </a>
                @endif
            </div>
            <div class="float-rght">
                <a class="btn btn-primary" href="{!! route('Heraldist.mpmailler.maillists.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
            @else
            <div class="float-left">
                <select class="nice" id="multiple-processings" onchange="locationOnChange(this.value);">
                    <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                    <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!}  value="?list=all">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                </select>
                @if(request()->get('list') && request()->get('list')=="all")
                    <a href="{!! route("Heraldist.mpmailler.maillists.index") !!}" class="filter-block">
                        <i class="fa fa-remove"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                    </a>
                @endif
            </div>
            <div class="float-right">
                <div class="add">
                    <a href="{!! route('Heraldist.mpmailler.maillists.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
            </div>
            <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.website") !!}</th>
                    <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($maillists as $maillist)
                    <tr>
                        <td>{!! $maillist->name !!}</td>
                        <td>
                            @if($maillist->websites())
                                @foreach($maillist->websites as $site)
                                    {!! $site->slug !!}
                                @endforeach
                            @endif
                        </td>
                        <td>{!! $maillist->created_at !!}</td>
                        <td>
                            <select onchange="locationOnChange(this.value);">
                                <option value="javascript:void(0)">{!! trans("MPCorePanel::general.selection") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.maillists.edit',$maillist->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.maillists.delete',$maillist->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="float-left">
                {!! $maillists->links() !!}
            </div>
            <div class="float-right">
                <select class="nice">
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
        @endif
@endsection
