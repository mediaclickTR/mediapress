<?php


if (class_exists(config('mediapress.login_controller'))) {

    Route::post('/login', config('mediapress.login_controller') . '@login')->name('login');
} else {
    Route::post('/login', "WebLoginController@login")->name('login');

}

if (class_exists(config('mediapress.register_controller'))) {
    Route::post('/register', config('mediapress.register_controller') . '@register')->name('register');
} else {
    Route::post('/register', "WebRegisterController@register")->name('register');
}


if (class_exists(config('mediapress.password_email_controller'))) {
    Route::post('/password/email', config('mediapress.password_email_controller') . '@sendResetLinkEmail')->name('password.email');
} else {
    Route::post('/password/email', "WebForgotPasswordController@sendResetLinkEmail")->name('password.email');
}

if (class_exists(config('mediapress.password_reset_controller'))) {
    Route::post('/password/reset', config('mediapress.password_reset_controller') . '@reset')->name('password.update');
} else {
    Route::post('/password/reset', "WebResetPasswordController@reset")->name('password.update');
}


if (class_exists(config('mediapress.email_verify_controller'))) {
    Route::post('/email/resend', config('mediapress.email_verify_controller') . '@resend')->name('verification.resend');
} else {
    Route::post('/email/resend', "WebEmailVerifyController@resend")->name('verification.resend');
}
Route::get('email/verify/{id}/{hash}', 'WebEmailVerifyController@verify')->name('verification.verify');
Route::get('password/reset/{token}', 'WebResetPasswordController@showResetForm')->name('password.reset');


Route::get('/logout', "WebLoginController@logout")->name('logout');
