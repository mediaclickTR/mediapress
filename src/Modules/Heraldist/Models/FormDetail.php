<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class FormDetail extends Model
{
    //use SoftDeletes;
    public const MODEL = 'model';
    protected $table = 'form_details';

    public $timestamps = true;

    //protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = [
        'form_id',
        'label',
        'name',
        'type',
        'list_option',
        'rules',
        'parameters',
        self::MODEL,
        'where',
        'order',
        'values',
        'select',
        'region_status',
        'region_label',
        'region_name',
        'text',
        'div_attr',
        'datasource'
    ];
    public function form()
    {
        return $this->belongsTo(Form::class);
    }
    public function extras()
    {
        return $this->morphMany(Extra::class, self::MODEL);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, self::MODEL);
    }
}
