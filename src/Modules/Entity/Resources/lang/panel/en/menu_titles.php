<?php

return [
    'users' => 'Members / Member List',
    "companies" => "Companies",
    "entitylists" => "Member Lists",
    "ideas" => "Ideas",
    "comments" => "Comments",
];
