<?php

use Illuminate\Database\Migrations\Migration;
use \Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreateSitemapsTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('sitemaps', function (Blueprint $table) {
            $table->increments('id');
            $table->string("sitemap_type_id")->nullable();
            $table->string("feature_tag")->nullable();

            $table->boolean("detailPage")->default(false);
            $table->boolean("searchable")->default(false);
            $table->boolean("category")->default(false);
            $table->boolean("criteria")->default(false);
            $table->boolean("property")->default(false);
            $table->string("filesystem")->nullable()->description("Public File System Key");
            $table->string("sfilesystem")->nullable()->description("Secure File System Key");

            $table->boolean("show_in_panel_menu")->default(false);
            $table->boolean("featured_in_panel_menu")->default(false);

            $table->defaultFields(); // executed on \Mediapress\Modules\MPCore\Foundation\Blueprint
            $table->integer("custom")->nullable();

            $table->text("ctex_1")->nullable();
            $table->text("ctex_2")->nullable();

            $table->string("cvar_1", 300)->nullable();
            $table->string("cvar_2", 300)->nullable();
            $table->string("cvar_3", 300)->nullable();
            $table->string("cvar_4", 300)->nullable();
            $table->string("cvar_5", 300)->nullable();

            $table->integer("cint_1")->nullable();
            $table->string("cint_2")->nullable();
            $table->string("cint_3")->nullable();
            $table->string("cint_4")->nullable();
            $table->string("cint_5")->nullable();

            $table->date("cdat_1")->nullable();
            $table->date("cdat_2")->nullable();

            $table->decimal("cdec_1", 20, 4)->nullable();
            $table->decimal("cdec_2", 20, 4)->nullable();
            $table->decimal("cdec_3", 20, 4)->nullable();
            $table->decimal("cdec_4", 20, 4)->nullable();


            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
    //    Schema::dropIndex("website_sitemap_index");
        Schema::dropIfExists('sitemaps');
        Schema::dropIfExists('sitemap_website');
        Schema::enableForeignKeyConstraints();
    }
}
