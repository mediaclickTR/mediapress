@csrf
@if(isset($setting))
    <input type="hidden" value="{!! $setting->id !!}" name="id">
@endif

<div class="form-group rel focus">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.type') !!}</div>
    <select name="vtype" id="type" required>
        <option value="">{!! trans("MPCorePanel::general.selection") !!}</option>
        <option value="string" @if(isset($setting) && $setting->vtype=="string") selected @endif>String</option>
        <option value="integer" @if(isset($setting) && $setting->vtype=="integer") selected @endif>Integer</option>
        <option value="boolean" @if(isset($setting) && $setting->vtype=="boolean") selected @endif>Boolean</option>
        <option value="json" @if(isset($setting) && $setting->vtype=="json") selected @endif>JSON</option>
        <option value="array" @if(isset($setting) && $setting->vtype=="array") selected @endif>Array</option>
        <option value="serialize" @if(isset($setting) && $setting->vtype=="serialize") selected @endif>Serialize</option>
    </select>
</div>

<div class="form-group rel focus">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.group-title') !!}</div>
    <input type="text" name="group" value="{{ old('group',  isset($setting->group) ? $setting->group : null) }}">
</div>

<div class="form-group rel focus">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.title') !!}</div>
    <input type="text" name="title" value="{{ old('title',  isset($setting->title) ? $setting->title : null) }}">
</div>

<div class="form-group rel focus">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.key') !!}</div>
    <input type="text" name="key" value="{{ old('key',  isset($setting->key) ? $setting->key : null) }}">
</div>

<div class="form-group rel focus">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.value') !!}</div>
    <input type="text" name="value" value="{{ old('value',  isset($setting->value) ? $setting->value : null) }}">
</div>

<div class="form-group">
    <div class="tit">{!! trans('MPCorePanel::setting_sun.params') !!}</div>
    <textarea style="min-height:200px;" name="params" placeholder="Radio, checkbox veya select seçimlerde JSON tipinde seçimler girilmelidir.">{{ old('params',  isset($setting->params) ? $setting->params : null) }}</textarea>
</div>
<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! trans('MPCorePanel::general.save') !!}</button>
</div>
@push("styles")
    <style>
        select{
            width: 100% !important;
        }
    </style>
@endpush
@push('scripts')

    <script type="application/javascript">
        /*
        $(document).ready(function(){
            $("#type").change(function(){
                var value = $(this).val();
                if(value == "checkbox" || value == "radio" || value == "select"){
                    $("#ektralar").show();
                }
                else{
                    $("#ektralar").hide();
                }
            });
        });
        */
    </script>

@endpush
