<?php

    namespace Mediapress\Modules\Content\Models;

    use Mediapress\Modules\MPCore\Models\BaseModel;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Mediapress\Modules\Content\Models\SitemapXmlBlock;
    use Mediapress\Support\Database\CacheQueryBuilder;

    class SitemapXml extends BaseModel
    {
        use CacheQueryBuilder;
        use SoftDeletes;

        protected $table = 'sitemap_xmls';

        public $timestamps = true;

        protected $fillable = [
            "title",
            "filename",
            "type",
            'renew_freq',
            'website_id',
            "status"
        ];

        protected $dates = ['deleted_at'];


        public function blocks(){
            return $this->hasMany(SitemapXmlBlock::class);
        }

        public function sitemaps(){
            return $this->hasManyThrough(Sitemap::class,SitemapXmlBlock::class,"sitemap_xml_id","id","id","sitemap_id");
        }
        
        public function website(){
            return $this->belongsTo(Website::class);
        }

    }
