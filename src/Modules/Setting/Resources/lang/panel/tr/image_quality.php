<?php

return [
    'modal_title' => "Görsel Ayarları",
    'quality' => "Kalite Oranı = ",
    'original_size' => "Orijinal Boyut = ",
    'new_size' => "Yeni Boyut = ",
];
