<?php

namespace Mediapress\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Form::macro('selectRangeWithZero', function($name, $start, $end, $selected = null, $max = null, $attributes = [])
        {

            $items = [];


            if($start > $end) {
                for ($i=$start; $i>$end; $i = $i-1) {
                    $items[$i . ""] = str_pad($i,strlen($max),0,STR_PAD_LEFT);

                }
            }  else {
                for ($i=$start; $i<$end; $i++) {
                    $items[$i . ""] = str_pad($i,strlen($max),0,STR_PAD_LEFT);
                }
            }



            $items[$end] = $end;

            return Form::select($name, $items, $selected, $attributes);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
