<?php

namespace Mediapress\Modules\Auth\Controllers;

use App\Http\Controllers\Controller;
use Html;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Foundation\MPCache;
use Mediapress\Models\Url;
use Mediapress\Models\Image;
use Mediapress\Modules\Auth\Models\Role;
use Mediapress\Modules\Auth\Models\Role as BRole;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Models\Menu;
use Mediapress\Models\MenuDetail;
use Mediapress\Models\LanguageWebsite;
use Silber\Bouncer\BouncerFacade as Bouncer;
use File;
use Validator;
use Cache;
use DB;

class PanelRoleController extends Controller
{
    use TableBuilderTrait;

    public const TITLE = "title";
    public const ACCESSDENIED = 'accessdenied';
    public const ACTIONS = 'actions';

    public function __construct()
    {
        Bouncer::useRoleModel(BRole::class);
    }

    public function index(Builder $builder)
    {

        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.index"])) {
            return rejectResponse();
        }

        $dataTable = $this->columns($builder)->ajax(route('Auth.roles.ajax'));
        //return dd($dataTable);

        return view("AuthPanel::roles.index", compact("dataTable"));
    }


    /**
     * @param FormBuilder $formBuilder
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function formCreate()
    {

        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.create"])) {
            return rejectResponse();
        }

        if ($create = Role::preCreate()) {
            return redirect(route("Auth.roles.edit", [$create->id, "isNew" => "yes"]));
        } else {
            throw new \Exception("Rol Modeli Oluşturulamadı : " . json_encode($create));
        }
    }


    public function edit(Role $role)
    {

        $request = request();
        // Permissions Checkpoint
        if ($role->status == 3 && $request->has("isNew") && $request->isNew == "yes") {
            if (!activeUserCan(["auth.roles.create"])) {
                return rejectResponse(null, null, "redirect_back");
            }
        } else {
            if (!activeUserCan(["auth.roles.update"])) {
                return rejectResponse(null, null, "redirect_back");
            }
        }

        return view("AuthPanel::roles.edit", compact("role"));
    }

    public function update(Role $role)
    {

        $request = request();

        // Permissions Checkpoint
        if ($role->status == 3 && $request->has("isNew") && $request->isNew == "yes") {
            if (!activeUserCan(["auth.roles.create"])) {
                return rejectResponse(null, null, "redirect_back");
            }
        } else {
            if (!activeUserCan(["auth.roles.update"])) {
                return rejectResponse(null, null, "redirect_back");
            }
        }

        $redirect_permission_page = $role->status == 3 && activeUserCan(["auth.roles.abilities.index"]);

        try {
            $role->title = $request->get(self::TITLE);
            $role->name = $request->get("name");
            $role->status = 1;
            $role->save();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if ($redirect_permission_page) {
            return redirect()->to(route("Auth.roles.abilities", $role));
        } else {
            return redirect()->to(route("Auth.roles.index"));
        }
    }

    public function abilities2(Request $request, $id = 0)
    {

        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.abilities.index"])) {
            return rejectResponse(null, null, "redirect_back");
        }

        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }

        $title = "Yetkiler";
        $module_name = null;
        $modules = ModulesEngine::getMPModules(true);
        $modules_arr = [];
        if ($request->has("module_name") && $request->module_name && in_array(array_keys($modules))) {
            $modules_arr = [$request->module_name];
        }
        $headers_only = true;
        if ($request->has("mode") && $request->mode == "module_actions") {
            $headers_only = false;
        }


        $actions = ModulesEngine::getModulesActions($modules_arr, $headers_only);


        return view("AuthPanel::roles.abilities2", compact("id", "role", "actions", "title"));

        //return dd($actions);


        //$actions = getActions(null, true, true);
        /*return $actions;

        $abilities = $role->getAbilities();
        $forbidden = $role->getForbiddenAbilities();

        $title = "Yetkiler";


        return view("AuthPanel::roles.abilities2", compact("id", "role", "abilities", "forbidden", self::ACTIONS, self::TITLE, "button"));*/

    }

    public function ajaxModuleActions(Request $request, $id)
    {


        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.abilities.index"])) {
            return rejectResponse();
        }


        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }

        $modules = ModulesEngine::getMPModules();
        if ($request->has("module_key") && $request->module_key && in_array($request->module_key, array_keys($modules))) {
            $module_key = $request->module_key;
        }

        $actions = ModulesEngine::getModuleActions($module_key, $modules[$module_key], false);


        if (!(count($actions["actions"]) || count($actions["subs"]) || count($actions["variations"]))) {
            return '<br><div><p class="text-center">'.trans("AuthPanel::roles.error.no_data").'...</p></div>';
        }

        return view("AuthPanel::roles.actions", compact('actions', 'role'));

    }

    public function storeAbilities(Request $request, $id = 0)
    {

        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.abilities.update"])) {
            return rejectResponse();
        }

        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }

        $data = $request->input()['actions'] ?? [];
        $mode = $request->input()['mode'];
        //dd($mode);
        if ($mode == "everything") {
            Bouncer::unforbid($role->name)->everything();
            Bouncer::allow($role->name)->everything();
            data_set($data, '*', null);
        } elseif ($mode == "nothing") {
            Bouncer::disallow($role->name)->everything();
            Bouncer::forbid($role->name)->everything();
            data_set($data, '*', null);
        } elseif ($mode == "custom") {
            Bouncer::disallow($role->name)->everything();
            Bouncer::unforbid($role->name)->everything();
        }
        foreach ($data as $actkey => $action) {
            switch ($action) {
                case "allow":
                    Bouncer::unforbid($role->name)->to($actkey);
                    Bouncer::allow($role->name)->to($actkey);
                    break;
                case "deny":
                    Bouncer::disallow($role->name)->to($actkey);
                    Bouncer::forbid($role->name)->to($actkey);
                    break;
                case "":

                    Bouncer::disallow($role->name)->to($actkey);
                    Bouncer::unforbid($role->name)->to($actkey);
                    break;
            }
        }
        //Cache::forget(cache_key('mainMenu'));
        return redirect()->to(route("Auth.roles.abilities", $id))->with(['status' => "İzinler başarıyla kaydedilmiştir."]);
    }

    /**
     * @param FormBuilder $formBuilder
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function save(FormBuilder $formBuilder, Collection $data)
    {
        if (!userAction('role.update', true, false)) {
            return [
                false, trans("panel::general.ajax.access_error")
            ];
        }

        $id = $data->pull("parameters.id");

        $role = BRole::find($id);

        if ($role) {
            $role = \Arr::first(FormStore::store($formBuilder, "role", "default", [], [
                "find" => ["id", "=", $id], "data" => $data->except("parameters")->toArray()
            ]));
        } else {
            $role = \Arr::first(FormStore::store($formBuilder, "role", "default", [], [
                "data" => $data->except("parameters")->toArray()
            ]));
        }

        if ($role) {
            return [
                true, trans("panel::general.ajax.create_success", ["name" => $role->title])
            ];
        } else {
            return [
                false, trans("panel::general.ajax.create_error", ["name" => $role->title])
            ];
        }
    }


    /**
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {

        // Permissions Checkpoint
        if (!activeUserCan(["auth.roles.delete"])) {
            return rejectResponse();
        }

        if (!userAction('role.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        if (!$id) {
            return redirect()->back()->withErrors(trans("panel::roles.crud.no_record"));
        }

        $role = BRole::find($id);

        if (!$role) {
            return redirect()->back()->withErrors(trans("panel::roles.crud.no_record"));
        }
        if ($role->delete()) {
            return redirect()->back();
        }

        return [false, trans("panel::roles.crud.delete_failed", ["name" => $role->title])];
    }
}
