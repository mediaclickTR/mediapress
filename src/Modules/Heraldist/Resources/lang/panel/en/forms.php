<?php

return [
    'select_websites' => "Select Website (s) to Link List to",
    'forms' => 'Forms',
    'form_create' => 'Create Form',
    'form_edit' => 'Edit Form',
    'success_message' => 'Success Message',
    'error_message' => 'Error Message',
    'button_name' => 'Button Name',
    'buton_name' => 'Button Name',
    'receiver_email' => 'Recipient E-Mail (Can Be Blank)',

    'send_user' => 'Send e-mail to the user whose form has been filled in?',
    'send_user_1' => 'Yes, send it',
    'send_user_2' => 'No, do not send',

    'recaptcha_key' => 'Google ReCaptcha Key',
    'recaptcha_secret' => 'Google ReCaptcha Secret',
    /*
     * Forms/details
     */
    'form_detail' => 'Form Detail',
    'inbox' => 'Inbox',
    'form.inputs' => 'Manage Form Elements',
    'lang.part.save.info' => 'Attention logging is done automatically. It is enough to fill in the relevant fields.',
    'notes' => 'Notes'
];
