<?php

namespace Mediapress\Modules\Flow\Facades;

use Illuminate\Support\Facades\Facade;

class Flow extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\Flow\Flow::class;
    }
}