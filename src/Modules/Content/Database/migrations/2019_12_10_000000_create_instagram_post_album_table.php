<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramPostAlbumTable extends Migration {

	public function up()
	{
        Schema::create('instagram_post_album', function(Blueprint $table) {

            $table->integer("album_id");
            $table->integer("post_id");
		});

	}

	public function down()
	{
		Schema::drop('instagram_post_album');
	}
}
