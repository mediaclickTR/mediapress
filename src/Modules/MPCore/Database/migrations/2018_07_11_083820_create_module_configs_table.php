<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('module_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id');
            $table->string('config_name');
            $table->string('key');
            $table->string('value')->nullable;
            $table->string('type');
            $table->string('details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_configs');
    }
}
