<?php

    namespace Mediapress\Modules;

    use Illuminate\Support\ServiceProvider;
    use Illuminate\Support\Facades\Route;
    use View;
    use Mediapress\Modules\MPCore\Facades\MPCore;
    use Mediapress\Models\MPModule;

    class Module extends ServiceProvider
    {
        public const CONFIG = 'Config';
        public const ACTIONS_PHP = 'actions.php';
        public const RESOURCES = 'Resources';
        public const PANEL = 'panel';

        /**
         * Bootstrap services.
         *
         * @return void
         */
        protected $module_name = "Module";
        protected $namespace= '';
        protected $load_self_views_for_panel = false;
        protected $load_self_views_for_web = false;
        protected $load_self_langs_for_panel = false;
        protected $load_self_langs_for_web= false;


        public function boot()
        {
            $this->map();

            if($this->load_self_views_for_panel){
                $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . self::PANEL, $this->module_name.'Panel');
            }
            if($this->load_self_views_for_web){
                $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'web', $this->module_name.'mediapress');
            }

            $this->loadMigrationsFrom(__DIR__.DIRECTORY_SEPARATOR.$this->module_name . DIRECTORY_SEPARATOR . 'Database'. DIRECTORY_SEPARATOR . 'migrations');

            if($this->load_self_langs_for_panel){
                $this->loadTranslationsFrom(__DIR__.DIRECTORY_SEPARATOR.$this->module_name . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . self::PANEL, self::PANEL);
            }
            if($this->load_self_langs_for_web){
                $this->loadTranslationsFrom(__DIR__.DIRECTORY_SEPARATOR.$this->module_name . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'front', 'front');
            }



            //$this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'front', 'front');
            /*$web_routes=__DIR__.DIRECTORY_SEPARATOR.'Web'.DIRECTORY_SEPARATOR.'Routes.php';
            if(file_exists($web_routes)){include_once $web_routes;}

            $adm_routes=__DIR__.DIRECTORY_SEPARATOR.'Web'.DIRECTORY_SEPARATOR.'Routes.php';
            if(file_exists($adm_routes)){include_once $adm_routes;}*/
        }

        /**
         * Register services.
         *
         * @return void
         */
        public function register()
        {
            //
        }

        protected function publishActions($dir){
            $actions_config = $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . strtolower($this->module_name)."_module_actions.php";
            if(is_file($actions_config)){
                $this->publishes([
                    //$actions_config => config_path(strtolower($this->module_name).'_module_actions.php')
                ]);
                $this->mergeConfigFrom($actions_config, strtolower($this->module_name).'_module_actions');
            }
        }


        protected function getConfigBasename($file)
        {
            return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
        }

        public function map()
        {
         /*   $this->mapPanelRoutes();

            $this->mapWebRoutes();*/

            //
        }

        /**
         * Define the "web" routes for the application.
         *
         * These routes all receive session state, CSRF protection, etc.
         *
         * @return void
         */
        protected function mapWebRoutes()
        {

            $routes_file = __DIR__.DIRECTORY_SEPARATOR. $this->module_name .DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'WebRoutes.php';
            Route::group([
                'middleware' => ['web', 'mediapress.after'],
                'namespace' => $this->namespace . '\Controllers',
            ], function ($router) use ($routes_file) {
                if(is_file($routes_file)){
                    include_once $routes_file;
                }

                $this->injectDynamicWebRoutes();

            });
        }

        /**
         * Define the "api" routes for the application.
         *
         * These routes are typically stateless.
         *
         * @return void
         */
        protected function mapPanelRoutes()
        {
            $routes_file = __DIR__ . DIRECTORY_SEPARATOR . $this->module_name . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'PanelRoutes.php';
            Route::group([
                'middleware' => 'web',
                'namespace' => $this->namespace . '\Controllers',
                'prefix' => 'mp-admin',
            ], function ($router) use ($routes_file) {

                //Route::auth();
                if (is_file($routes_file)) {
                    include_once $routes_file;
                }

                $this->injectDynamicPanelRoutes();

            });
        }

        public function  injectDynamicWebRoutes(){

        }

        public function  injectDynamicPanelRoutes(){

        }

        public function modifyMenu(array $menu, $type)
        {
            return $menu;
        }

        public function translate($key)
        {
            return trans($key);
        }

        protected function mergeConfig($path, $key)
        {
            $config = config($key);

            foreach (require $path as $k => $v) {
                if (is_array($v)) {
                    if (isset($config[$k])) {
                        //dump($config[$k],$v);
                        $config[$k] = array_merge($config[$k], $v);
                    } else {
                        $config[$k] = $v;
                    }

                } else {
                    $config[$k] = $v;
                }
            }
            config([$key=>$config]);
        }
    }
