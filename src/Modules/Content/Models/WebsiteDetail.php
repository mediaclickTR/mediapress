<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\HasMFiles;
use Mediapress\Modules\Content\Traits\IsDetail;

class WebsiteDetail extends BaseModel
{
    use SoftDeletes;
    use HasMFiles;
    use IsDetail;

    protected $table = 'website_details';

    public $timestamps = true;

    protected $fillable = [
        'website_id',
        'language_id',
        'country_group_id',
        'name',
        'slug',
        'deleted_at'
    ];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function parent()
    {
        return $this->belongsTo(Website::class, 'website_id', 'id');
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }
    
    public function url(){
    
        return $this->morphOne(Url::class, 'model')->withTrashed()->withDefault(['url'=>'/'])->whereIn('type',['original','reserved'])->orderBy('type','asc');
    }


    public function extras()
    {
        return $this->hasMany(WebsiteDetailExtra::class);
    }

    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value)) {
            return $extra->value;
        }

        return null;
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function setSlugAttribute($value)
    {
        if (mb_substr($value, 0, 1) != '/') {
            $this->attributes['slug'] = '/' . $value;
        } else {
            $this->attributes['slug'] = $value;
        }
    }


    public function getParentIdAttribute(){
        return $this->website_id;
    }

    public function countryGroups()
    {
        return $this->morphMany(CountryGroup::class, 'owner');
    }

}
