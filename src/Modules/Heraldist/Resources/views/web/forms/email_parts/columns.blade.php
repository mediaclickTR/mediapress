@if(isset($field))

    <td width="{!! $field['config']['width'] !!}">
        @include('HeraldistWeb::forms.email_main',['fields'=>$field->children])
    </td>
@endif