<?php
/**
 * Created by PhpStorm.
 * User: Saban Koparal
 * E-Mail: saban.koparal@mediaclick.com.tr
 */

Route::group(['middleware' => 'panel.auth'], function()
{


        /**
         * Base Modules
         */

        Route::group(['prefix' => 'Flow', 'as' => 'admin.flows.'], function()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'FlowPanelController@index']);
            Route::get('/show', ['as' => 'show', 'uses' => 'FlowPanelController@show']);

            Route::get('/plugins/HeatmapSessionRecording/angularjs/sessionvis/sessionvis.directive.html', ['as' => 'sessionvisDirective', 'uses' => 'FlowPanelController@sessionvisDirective']);
       });





});