<?php

return [
    'admins'=>'Yöneticiler',
    'website'=>'Web Site',
    'create-admin'=>'Yönetici Ekle',
    'edit-admin'=>'Yönetici Düzenle',
    'role-admin'=>'Rol',
    'language'=>'Dil',
    'username-admin'=>'Kullanıcı Adı',
    'phone-admin'=>'Telefon',
    'name-admin'=>'Yönetici Adı',
    'surname-admin'=>'Yönetici Soyadı',
    'email-admin'=>'Yönetici E-Posta',
    'password-admin'=>'Yönetici Şifre',
    'password-admi-small'=>'Harf, sayı ve özel karakter (@$!%*#?.&) içermelidir.',
    'repeat-password-admin'=>'Yönetici Şifre Tekrar',
    'roles'=>'Roller',
    'select_zone' => 'Bölge Seçiniz'
];
