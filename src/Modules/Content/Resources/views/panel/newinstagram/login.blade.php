@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }

        .turn {
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
    </style>
@endpush

@php
    if (!isset($message)){
        $message = Session::get('instagram_message');
        $messageType = Session::get('instagram_message_type');
        Session::forget('instagram_message_type');
        Session::forget('instagram_message');
    }

@endphp

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title float-left w-100">
            Instagram


            {{--            <div class="float-right" >--}}
            {{--                <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>--}}
            {{--            </div>--}}

            {{--            <div class="float-right" style="margin-right: 10px;">--}}
            {{--                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>--}}
            {{--            </div>--}}

            {{--            <div class="float-right" style="margin-right: 10px;">--}}
            {{--                <a role="button" href="{!! route('Content.instagram.index') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Albümler</a>--}}
            {{--            </div>--}}

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 50px;">
                    <div class="text-center">
                        @if($messageType != null && $messageType == "error")
                            <div class="alert alert-danger" role="alert">
                                {!! $message !!}
                            </div>
                        @elseif($messageType != null && $messageType == "success")
                            <div class="alert alert-success" role="alert">
                                {!! $message !!}
                            </div>
                        @endif

                    </div>

                    @if(isset($verifyUrl) && $needVerify == true)
                        <div class="text-center">
                            <a href="{!! $verifyUrl !!}">
                                <button type="button" class="btn btn-outline-secondary">Doğrula</button>
                            </a>
                        </div>

                    @endif


                </div>
            </div>
        </div>
    </div>

@endsection
@push("scripts")
    <script type="text/javascript">
        $("#save").click(function (e) {
            e.preventDefault();

            var appId = $("#appId").val();
            var appSecretId = $("#appSecretId").val();

            if (appId.length < 10 || appSecretId.length < 15) {
                alert("Lütfen Bilgileri kontrol ediniz");
            } else {
                $.post("   ", {
                    "_token": "{{ csrf_token() }}",
                    "appId": appId,
                    "appSecretID": appSecretId
                })
                    .done(function (data) {
                        alert("Data Loaded: " + data);
                    });
            }
        });

        $("#verify").click(function (e) {
            e.preventDefault();
            var appId = $("#id").val();
            var redirectUrl = $("#redirect_url").val();
            //alert(redirectUrl);
            if (appId.length < 10 || redirectUrl.length < 15) {
                alert("Lütfen Bilgileri kontrol ediniz");
            } else {
                var url = "https://api.instagram.com/oauth/authorize?client_id=" + appId + "&redirect_uri=" + redirectUrl + "&scope=user_profile,user_media&response_type=code";
                window.location.href = url;
            }
        });
    </script>
@endpush