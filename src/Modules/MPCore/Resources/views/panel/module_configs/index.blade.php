@php
use Mediapress\Modules\MPCore\Models\Module;
@endphp
@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">Modül Özellikleri</h1>
            <div class="float-right">
                <a class="btn btn-primary" href="{!! route('MPCore.module_configs.create') !!}"> <i class="fa fa-plus"></i> Yeni Ekle</a>
            </div>
            <table>
                <thead>
                <tr>
                    <th>Modül Adı</th>
                    <th>Adı</th>
                    <th>Key</th>
                    <th>Value</th>
                    <th>Son Güncelleme</th>
                    <th>İşlemler</th>
                </tr>
                </thead>
                @foreach($module_configs as $module)
                    @php  $module_name = \Mediapress\Models\Module::find($module->module_id)->name; @endphp
                    <tr>
                        <td>{!! $module_name !!}</td>
                        <td>{!! $module->config_name !!}</td>
                        <td>{!! $module->key !!}</td>
                        <td>{!! $module->value !!}</td>
                        <td>{!! $module->update_at !!}</td>
                        <td>
                            <select class="nice" onchange="locationOnChange(this.value);">
                                <option value="#">Seçim Yapınız</option>
                                <option value="{!! route('MPCore.module_configs.edit',$module->id) !!}">Düzelt</option>
                                <option value="{!! route('MPCore.module_configs.delete',$module->id ) !!}">Sil</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
@endsection