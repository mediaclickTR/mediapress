<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePopupCustomUrlsTable extends Migration {

	public function up()
	{
        if (Schema::hasTable('popup_custom_urls'))
            return;

		Schema::create('popup_custom_urls', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('popup_id')->unsigned()->index();
            $table->text('url');
		});
	}

	public function down()
	{
		Schema::dropIfExists('popup_custom_urls');
	}
}
