<table class="table">
    <thead>
    <tr>
        <th>Düzenleyen</th>
        <th>Tarih</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($backups as $backup)
        <tr>
            <td>{!! $backup->admin->username !!}</td>
            <td>{!! \Carbon\Carbon::parse($backup->created_at)->format('d/m/Y H:i:s') !!}</td>
            <td><a href="javascript:void(0);" onclick="javascript:loadBackup($(this))" data-content='@json($backup->content)' class="btn btn-primary">Geri Yükle</a></td>
        </tr>
    @endforeach
    </tbody>
</table>