<?php

namespace Mediapress\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Mediapress\Extentions\FileCacheStore;
use Mediapress\Extentions\RedisCacheStore;

class CacheServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Auth $auth)
    {

        Cache::extend('file', function ($app) {
            return Cache::repository(new FileCacheStore);
        });
        Cache::extend('redis', function ($app) {
            return Cache::repository(new RedisCacheStore);
        });
    }
}
