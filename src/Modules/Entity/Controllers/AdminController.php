<?php

namespace Mediapress\Modules\Entity\Controllers;

use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Models\Page;
use Mediapress\Models\Sitemap;
use Mediapress\DataTable\TableBuilderTrait;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Models\Role;
use Yajra\Datatables\Html\Builder;


class AdminController extends Controller
{
    use TableBuilderTrait;
    public const ACCESSDENIED = 'accessdenied';
    public const ADMINS = "admins";
    public const DEFAULT = "default";

    /** Admins List
     * @param Builder $builder
     * @return mixed
     */

    public function __construct()
    {

        Bouncer::useRoleModel(Role::class);
    }

    public function index(Builder $builder)
    {
        if(! activeUserCan(["auth.admins.index",])){return rejectResponse();}

        $dataTable = $this->columns($builder)->ajax(route('Entity.users.ajax',null));

        return $this->view("pages.admins.index", compact('dataTable'));
    }

    public function formCreate(FormBuilder $formBuilder, $id = 0)
    {


        $admin = new Admin();
        if ($id)
        {
            if(!userAction('admin.update',true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.edit-title", ["name" => "Yönetici"]);
            $button = trans("panel::general.edit");
            $data = [
                "find" => [
                    "id", "=", $id
                ]
            ];
            $admin = Admin::find($id);
        }
        else
        {
            if(!userAction('admin.create',true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.create-title", ["name" => "Yönetici"]);
            $button = trans("panel::general.save");
            $data = [];
        }

        $form = PanelFormBuilder::create(self::ADMINS, self::DEFAULT, $formBuilder, [], $data);

        return $this->view("pages.admins.form", compact("id", "form", "title", "button","admin"));
    }

    public function save(FormBuilder $formBuilder, Collection $data)
    {
        $id = $data->pull("parameters.id");
        $roles_to_assign = $data->get("default___roles[]");
        //$roles = $data->pull("parameters");
        $data->put("default___password", bcrypt($data->get("default___password")));
        Bouncer::useRoleModel(Role::class);

        // Cache::forget(cache_key('mainMenu'));

        if ($id)
        {
            // düzenle çalışınca yetkilendirilme eklenecek
            $admin = \Arr::first(FormStore::store($formBuilder, self::ADMINS, self::DEFAULT, [], [
                "find" => ["id", "=", $id], "data" => $data->except("parameters")->toArray()
            ]));


            $roles_assigned = $admin->roles()->get()->pluck('name')->toArray();
            if ($admin)
            {

                // first retract current roles from user
                foreach($roles_assigned as $ra){$admin->retract($ra);}
                // later then assign requested roles
                foreach($roles_to_assign as $rta){Bouncer::assign($rta)->to($admin);}

                return [
                    true, trans("panel::general.ajax.edit_success", ["name" => $admin->name])
                ];
            }

            return [
                false, trans("panel::general.ajax.edit_error", ["name" => $admin->name])
            ];
        } else {

            $admin = \Arr::first(FormStore::store($formBuilder, self::ADMINS, self::DEFAULT, [], [
                "data" => $data->except("parameters")->toArray()
            ]));
            if ($admin)
            {

                // assign requested roles
                foreach($roles_to_assign as $rta){Bouncer::assign($rta)->to($admin);}
                /*Bouncer::sync($admin)->roles($roles_to_assign);*/

                return [
                    true, trans("panel::general.ajax.edit_success", ["name" => $admin->name])
                ];
            }

            return [
                false, trans("panel::general.ajax.edit_error", ["name" => $admin->name])
            ];
        }
    }

    public function delete(Collection $data)
    {
        if(!userAction('admin.delete',true,false)){
            return [
                false, trans("panel::general.ajax.access_error")
            ];
        }

        $id = $data->pull("parameters.id");
        if (! $id){
            return [false, "Geçersiz Id"];
        }

        $admin = Admin::find($id);

        if (! $admin){
            return [false, "Yönetici Kaydı Bulunamadı!"];
        }

        if ($admin->delete())
        {
            return [true, trans("panel::general.ajax.delete_success", ["name" => $admin->name])];
        }

        return [false, trans("panel::general.ajax.delete_error", ["name" => $admin->name])];
    }
}
