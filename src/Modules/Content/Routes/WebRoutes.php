<?php

/*Route::get('{xml}.xml', 'WebsiteXmlController@mainXmlPage');
Route::get('default/{type}/{xml}.xml', 'WebsiteXmlController@xmlPage');
Route::get('default/{type}/{limit}/{xml}.xml', 'WebsiteXmlController@xmlPageForPages');


Route::get('{lang}/{xml}.xml', 'WebsiteXmlController@mainPageLang');
Route::get('{lang}/{type}/{xml}.xml', 'WebsiteXmlController@xmlPageLang');
Route::get('{lang}/{type}/{limit}/{xml}.xml', 'WebsiteXmlController@xmlPageForPagesLang');*/

Route::get('/robots.txt',"SeoController@robots");
Route::get('{xml}.xml', 'WebsiteSitemapXmlController@index');
Route::get('{subfolder}/{xml}.xml', 'WebsiteSitemapXmlController@index');
Route::get('{subfolder}/{subfolder2}/{xml}.xml', 'WebsiteSitemapXmlController@index');

Route::get('setPopupImpressions', '\Mediapress\Modules\Content\Controllers\PopupController@setPopupImpressions')->name('setPopupImpressions');


Route::post('/ajax/category-criteria-filter', "\Mediapress\Modules\Content\Controllers\WebContentController@ajaxCriteriaFilter")->name('ajaxCriteriaFilter');
