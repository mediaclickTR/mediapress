<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryDetailsTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->create('category_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->integer('country_group_id')->unsigned()->index();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->longText('detail')->nullable();

            $table->integer("status")->default(3);
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('category_details');
    }
}
