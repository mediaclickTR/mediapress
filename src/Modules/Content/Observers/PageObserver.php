<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 21.03.2019
 * Time: 17:45
 */

namespace Mediapress\Modules\Content\Observers;

use Mediapress\Modules\Content\Models\CategoryPage;
use Mediapress\Modules\Content\Models\CriteriaPage;
use Mediapress\Modules\Content\Models\Page;
use Illuminate\Support\Facades\Cache;
use DB;

class PageObserver
{
    public function updated(Page $page)
    {

        if ($page->sitemap->autoOrder == 1) {

            if($page->sitemap->subPageOrder == 1) {
                Page::where('sitemap_id', $page->sitemap_id)
                    ->where('order', '>=', $page->order)
                    ->where('id', '!=', $page->id)
                    ->where('page_id', $page->page_id)
                    ->where('status', 1)
                    ->increment('order', 1);

                DB::statement(DB::raw("SET @r=0"));
                DB::statement(DB::raw("UPDATE `pages` SET `order`=@r:=(@r+1) WHERE (`status`=1 or `status`=5 ) AND `sitemap_id`='{$page->sitemap_id}' AND `page_id`='{$page->page_id}' AND `deleted_at` IS NULL ORDER BY `order` ASC ,`updated_at`"));

            } else {
                Page::where('sitemap_id', $page->sitemap_id)
                    ->where('order', '>=', $page->order)
                    ->where('id', '!=', $page->id)
                    ->where('status', 1)
                    ->increment('order', 1);

                DB::statement(DB::raw("SET @r=0"));
                DB::statement(DB::raw("UPDATE `pages` SET `order`=@r:=(@r+1) WHERE (`status`=1 or `status`=5 ) AND `sitemap_id`='{$page->sitemap_id}' AND `deleted_at` IS NULL ORDER BY `order` ASC ,`updated_at`"));
            }
        }

        Cache::flush();
    }

    public function deleting(Page $object)
    {
        if ($object->isForceDeleting()) {
            $object->details()->forceDelete();
        }

        $object->details()->delete();

        if ($object->extras->isNotEmpty()) {
            $object->extras()->delete();
        }


        CategoryPage::where('page_id', $object->id)->delete();
        CriteriaPage::where('page_id', $object->id)->delete();

        Cache::flush();
    }

    public function deleted(Page $page)
    {
        if ($page->sitemap->autoOrder == 1) {
            DB::statement(DB::raw("SET @r=0"));
            DB::statement(DB::raw("UPDATE `pages` SET `order`=@r:=(@r+1) WHERE (`status`=1 or `status`=5 ) AND `sitemap_id`='{$page->sitemap_id}' AND `page_id`='{$page->page_id}' AND `deleted_at` IS NULL ORDER BY `order` ASC ,`updated_at`"));
        }

        Cache::flush();
    }
}
