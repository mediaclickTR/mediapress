<?php

namespace Mediapress\Modules\Entity\Models;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class Entities extends Model
{

    protected $table = 'entities';
    public $timestamps = true;

    protected $fillable = [
      "name",
      "surname",
      "email",
      "phone",
      "description"
    ];

    public function model()
    {
        return $this->morphMany(EntitylistPivot::class, 'model');
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

}
