<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mediapress 4.0 Routes</title>
</head>
<body>

<h1>Routes ({!! count($routes) !!})</h1>
<select name="method" onchange="location = this.value;">
    <option value="">Method Filtrele</option>
    <option value="?">Hepsi</option>
    <option value="?method=GET">GET</option>
    <option value="?method=POST">POST</option>
</select>
<table style='width:100%'>
    <tr>
        <td width='10%'><h4>Method</h4></td>
        <td width='10%'><h4>Route</h4></td>
        <td width='10%'><h4>Name</h4></td>
        <td width='70%'><h4>Action</h4></td>
        </tr>
    @foreach($routes as $route)
    <tr>
        <td>{!! $route->methods()[0] !!}</td>
        <td>{!! $route->uri() !!}</td>
        <td>{!! $route->getName() !!}</td>
        <td>{!! $route->getActionName() !!}</td>
    </tr>
    @endforeach
    </table>
</body>
</html>