@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! __("ContentPanel::menu.menus") !!}
                </div>
            </div>
            <div class="float-right">
                <a href="{!! route('Content.menus.create') !!}" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i>
                    {!! __("ContentPanel::general.add_new") !!}
                </a>
            </div>
        </div>
            <div class="p-30">
                <div class="table-field">
                    @if(count($menus) <= 0)
                        <div class="alert alert-warning">{!! __("ContentPanel::general.no_data") !!}</div>
                    @else
                        <table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{!! __("ContentPanel::general.status") !!}</th>
                                <th>{!! __("ContentPanel::general.name") !!}</th>
                                <th>{!! __("ContentPanel::general.created_at") !!}</th>
                                <th>{!! __("ContentPanel::general.updated_at") !!}</th>
                                <th>{!! __("ContentPanel::general.actions") !!}</th>
                            </tr>
                            </thead>
                            @foreach($menus as $key=>$menu)
                                <tr>
                                    <td>{!! $menu->id !!}</td>
                                    <td class="status">
                                        @if($menu->status == 1)
                                            <i class="active" style="background: #7fcc46"></i>{{ __('ContentPanel::general.status_title.active') }}
                                        @else
                                            <i class="passive" style="background: #dc3545"></i>{{ __('ContentPanel::general.status_title.passive') }}
                                        @endif
                                    </td>
                                    <td>{!! $menu->name !!}</td>
                                    <td>{!! $menu->created_at !!}</td>
                                    <td>{!! $menu->updated_at !!}</td>
                                    <td>
                                        <a href="{!! route("Content.menus.details",  [$country_group_id, $language_id, $menu->id]) !!}" class="mr-2" title="{!! __("ContentPanel::menu.menu_detail") !!}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{!! route('Content.menus.delete',$menu->id) !!}" title="{!! __("ContentPanel::general.delete") !!}">
                                            <i class="fa fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
@endsection
