<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Exports\ImageTagsExport;
use Mediapress\Modules\Content\Imports\ImageTagsImport;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Models\Language;
use Maatwebsite\Excel\Facades\Excel;

class ImageController extends Controller
{
    public function index()
    {
        return view('ContentPanel::image_tags.index');
    }

    public function getImageTags()
    {
        $filter = request()->get('filter');
        $images = MFileGeneral::whereHas('mfile', function ($q) {
            $q->where('mimeclass', 'image');
        })
            ->whereHasMorph('model', '*', function ($q, $type) use ($filter) {
                if (in_array($type, [Page::class, Category::class])) {
                    $q = $q->whereHas('sitemap', function ($que){
                        $que->whereHas('websites', function ($query) {
                            $query->where('id', session('panel.website.id'));

                        });
                    });

                    if ($filter['url']) {
                        $q = $q->whereHas('details', function ($que) use ($filter) {
                            $que->whereHas('url', function ($query) use ($filter) {
                                $query->where('url', $filter['url']);
                            });
                        });
                    }

                    return $q;

                } elseif (in_array($type, [PageDetail::class, CategoryDetail::class])) {
                    $q = $q->whereHas('parent', function ($que) {
                        $que->whereHas('sitemap', function ($quer){
                            $quer->whereHas('websites', function ($query) {
                                $query->where('id', session('panel.website.id'));
                            });
                        });
                    });

                    if ($filter['url']) {
                        $q = $q->whereHas('url', function ($que) use ($filter) {
                            $que->where('url', $filter['url']);
                        });
                    }

                    return $q;
                } elseif ($type == MenuDetail::class) {
                    $q->whereHas('menu', function ($que) {
                        $que->where('menus.website_id', session('panel.website.id'));
                    });
                } elseif ($type == SitemapDetail::class) {
                    $q = $q->whereHas('sitemap', function ($que) use ($filter) {
                        $que->whereHas('websites', function ($query) {
                            $query->where('id', session('panel.website.id'));
                        });
                    });

                    if ($filter['url']) {
                        $q = $q->whereHas('url', function ($que) use ($filter) {
                            $que->where('url', $filter['url']);
                        });
                    }

                    return $q;
                } else {
                    $q = $q->whereHas('websites', function ($query) use ($filter) {
                        $query->where('id', session('panel.website.id'));
                    });


                    if ($filter['url']) {
                        $q = $q->whereHas('details', function ($q) use ($filter) {
                            $q->whereHas('url', function ($que) use ($filter) {
                                $que->where('url', $filter['url']);
                            });
                        });
                    }

                    return $q;
                }
            })
            ->orderBy('model_type')
            ->orderBy('model_id');

        if($filter['title']) {
            $temp = '%"title":%' . $filter['title'] . '%';
            $images = $images->where('details', 'like', $temp);
        }

        if($filter['alt_tag']) {
            $temp = '%"tag":%' . $filter['alt_tag'] . '%';
            $images = $images->where('details', 'like', $temp);
        }

        $imageCount = $images->count();
        $pageSize = request()->get('filter.pageSize') ?: 20;
        $images = $images->paginate($pageSize, ["*"], 'filter.pageIndex');

        $hold = [];

        foreach ($images as $image) {
            $tempImage = image($image->mfile_id)->resize(['h' => 50]);
            $model = $image->model;

            if (in_array(get_class($model), [Page::class, Category::class, Sitemap::class])) {
                $url = $model->detail->url;
                $default = $model->detail->name;
            } elseif (in_array(get_class($model), [PageDetail::class, CategoryDetail::class, SitemapDetail::class])) {
                $url = $model->url;
                $default = $model->name;
            } else {
                $url = 'N\A';
                $default = '';
            }


            $hold[] = [
                'id' => $image->id,
                'url' => $url->url,
                'image' => '<img src="' . $tempImage->url . '" height="50" style="max-width: 100%; margin:0 auto;dipslay: block">',
                'title' => $image->details[session('panel.active_language.id')]['title'] ?? null,
                'alt_tag' => $image->details[session('panel.active_language.id')]['tag'] ?? null,
                'default_text' => $default,
            ];
        }

        return ['list' => $hold, 'count' => $imageCount];
    }

    public function updateImageTag(Request $request)
    {
        $item = $request->get('item');
        $id = $item['id'];

        $mfileGeneral = MFileGeneral::find($id);

        if($mfileGeneral) {
            $activeLanguageId = session('panel.active_language.id');
            $details = $mfileGeneral->details;

            $details[$activeLanguageId] = [
                'title' => $item['title'],
                'tag' => $item['alt_tag'],
            ];

            $mfileGeneral->update(['details' => $details]);
            $mfileGeneral->save();
        }
    }

    public function getImageTagDetails(Request $request)
    {
        $id = $request->get('id');
        $url = $request->get('url');
        $mfileGeneral = MFileGeneral::find($id);
        $languages = [];
        $details = [];
        $file = null;

        if($mfileGeneral) {
            $file = image($mfileGeneral->mfile_id)->resize(['h' => 200]);
            $model = $mfileGeneral->model;

            if($model->language_id) {
                $language = Language::where('id', $model->language_id)->first();
                $languages[] = [
                    'id' => $language->id,
                    'code' => $language->code,
                ];
            } else {
                $languageIds = $model->find($mfileGeneral->model_id)->details->pluck('language_id')->toArray();

                $hold = Language::whereIn('id', $languageIds)->get();

                foreach ($hold as $language) {
                    $languages[] = [
                        'id' => $language->id,
                        'code' => $language->code,
                    ];
                }
            }

            $details = $mfileGeneral->details;
        }

        return view('ContentPanel::image_tags.modal',
            [
                "file" => $file,
                "id" => $id,
                "url" => $url,
                "details" => $details,
                "languages" => $languages,
            ])->render();
    }

    public function saveImageTagDetails(Request $request)
    {
        $id = $request->get('id');
        $data = $request->except('_token', 'id');

        $mfileGeneral = MFileGeneral::find($id);

        $mfileGeneral->update(['details' => $data['details']]);
    }

    public function excelExport()
    {
        return Excel::download((new ImageTagsExport()), 'image-tags.xlsx');
    }

    public function excelImport(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $file = $request->file('file');
        $fileName = $file->store(null, 'public');
        $fileName = storage_path('app/public/'. $fileName);


        Excel::import(new ImageTagsImport(), $fileName);

        @unlink($fileName);


        return redirect()->route('Content.image_tags.index');
    }
}
