<?php


namespace Mediapress\Modules\Content\Foundation;


use Mediapress\Foundation\UserAgent\UserAgent;

class Slide
{
    public const DESKTOP = 'desktop';

    /**
     * Slide constructor.
     * @param array $array
     */
    public function __construct(array $array)
    {
        foreach ($array as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function image($i = 0)
    {
        $device = $this->device();
        if (isset($this->files) && isset($this->files[$device]) && isset(json_decode($this->files[$device],1)[0])) {
            $imageList = $this->files[$device];
        } else {
            $imageList = $this->files[self::DESKTOP];
        }
        $images = json_decode($imageList, 1);

        if (isset($images[$i])) {
            $return = false;
            $image = image($images[$i]);
            if($device == 'tablet'){
                $return = $image->resize(['w'=>'1366']);
            }
            elseif($device == 'mobile'){
                $return = $image->resize(['w'=>'450']);
            }else{
                $return = $image->resize(['w'=>'1920']);
            }
            if($return){
                return $return;
            }
        }
        return image(null);
    }

    public function text($i){
        $device = $this->device();
        $text = null;
       if(isset($this->texts) && isset($this->texts[$i])){
            $text = $this->texts[$i];
       }
        if (isset($text[$device]) ) {
            $line = $text[$device];

        } else {
            $line = $text[self::DESKTOP];
        }
        if($line){
            if($href = $this->url('text',$i)){
                return '<div class="line'.$i.'"><a '.$href.'>'.$line.'</a></div>';
            }else{
                return '<div class="line'.$i.'">'.$line.'</div>';
            }

        }

    }

    public function button($i){
        $device = $this->device();
        $button = null;
        if(isset($this->buttons) && isset($this->buttons[$i])){
            $button = $this->buttons[$i];
        }
        if (isset($text[$device]) ) {
            $text = $button[$device];

        } else {
            $text = $button[self::DESKTOP];
        }

        if($text){
            return '<div class="more more'.$i.'">
               <a '.$this->url('button',$i).'>'.$text.'</a>
           </div>';
        }

    }

    public function url($type,$i){
        if(isset($this->url[$type]) && isset($this->url[$type][$i])){
            return 'href="'.$this->url[$type][$i]['text'].'" target="'.$this->url[$type][$i]['type'].'"';
        }
    }

    private function device(){
        $ua = new UserAgent();
        return $ua->getDevice();
    }
}
