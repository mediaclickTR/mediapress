<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PanelMenuDetail extends Model
{

    protected $table = 'panel_menu_details';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = ["menu_id", "sitemap_id", "parent", 'lft', 'rgt', "depth", "draft"];

    public function menu()
    {
        return $this->belongsTo(PanelMenu::class);
    }


    public function children()
    {
        return $this->hasMany(PanelMenuDetail::class,  'parent', 'id');
    }

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
}
