<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelMenuDetailsTable extends Migration {

    public const PANEL_MENU_DETAILS = 'panel_menu_details';

    public function up()
    {

        if (!Schema::hasTable(self::PANEL_MENU_DETAILS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::PANEL_MENU_DETAILS, function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('menu_id')->nullable()->unsigned();
                $table->integer('sitemap_id')->nullable()->unsigned();
                $table->integer('parent')->nullable()->unsigned();
                $table->integer('lft')->nullable()->unsigned();
                $table->integer('rgt')->nullable()->unsigned();
                $table->integer('depth')->nullable()->unsigned();
                $table->integer('draft')->nullable()->unsigned();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::PANEL_MENU_DETAILS);
        Schema::enableForeignKeyConstraints();
    }
}
