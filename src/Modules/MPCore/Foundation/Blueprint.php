<?php

namespace Mediapress\Modules\MPCore\Foundation;

class Blueprint extends \Illuminate\Database\Schema\Blueprint
{
    public function defaultFields(){
        $this->integer("admin_id")->unsigned()->nullable();
        $this->integer("status")->default(3);
    }

    public function nestable($parent = ""){
        $this->integer("lft")->unsigned()->nullable();
        $this->integer("rgt")->unsigned()->nullable();
        $this->integer("depth")->unsigned()->nullable();
        if($parent != ""){
            $this->integer($parent)->unsigned()->nullable()->index();
        }
    }

    public function orderable(){
        $this->integer("order")->unsigned()->nullable();
    }

}