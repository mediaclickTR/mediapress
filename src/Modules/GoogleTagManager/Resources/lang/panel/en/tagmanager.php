<?php

return [
    'connection_warning' => 'Your Google Tag Manager information could not be matched. Please check your information.',
    'Google_connect '=>' Connect with Google Tag Manager',
    'nodata' => 'Could not get data from Google Tag Manager please refresh the page'
];
