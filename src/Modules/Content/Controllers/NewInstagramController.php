<?php

namespace Mediapress\Modules\Content\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\AlbumPost;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\Content\Models\SocialMedia;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Illuminate\Routing\Redirector;

use Mediapress\Modules\Content\Models\InstagramPosts;
use Mediapress\Modules\Content\Models\Album;

use MetzWeb\Instagram\Instagram;


class NewInstagramController extends Controller
{
    use TableBuilderTrait;

    private $userID = null;
    private $accessToken = null;
    private $appID = null;
    private $appSecret = null;
    private $tokenFile = null;
    private $information = null;
    private $APP_NAME = null;
    private $PANEL_AUTH_SCOPE = "mp-admin/Content/NewInstagram/auth";
    private $DIRECTORY_SEPARATOR = "/";
    private $HTTPS = "https://";


    public function __construct()
    {
        $this->tokenFile = storage_path('app/public/instagram-cache/token.json');
        if(!file_exists(storage_path('app/public/instagram-cache'))){
            mkdir(storage_path('app/public/instagram-cache'), 0777);
        }
        if (!file_exists($this->tokenFile)){
            touch($this->tokenFile);
        }
        $this->information = json_decode(file_get_contents($this->tokenFile), 1);
        $this->appID = env("INSTAGRAM_APP_ID");
        $this->appSecret = env("INSTAGRAM_APP_SECRET_ID");
        $this->userID = $this->information['userID'] ?? '';
        $this->accessToken = $this->information['accessToken'] ?? '';
        if(isset( $_SERVER["HTTP_HOST"])){
            $this->APP_NAME = $_SERVER["HTTP_HOST"];
        }
    }

    /**
     * Route Functions
     **/

    public function index(Request $request)
    {

        if ($this->appID == null || $this->appSecret == null || $this->appID == "" || $this->appSecret == "") {
            $message = "Lütfen ENV Üzerinden <strong> INSTAGRAM_APP_ID </strong> ve <strong> INSTAGRAM_APP_SECRET_ID </strong> değişkenlerini tanımlayınız";
            return redirect()->route('Content.newinstagram.login')->with([
                "instagram_message" => $message, "instagram_message_type" => "error"
            ]);
        }

        if ($this->userID == null || $this->accessToken == null) {
            $message = "Lütfen Instagram Doğrulamasını Gerçekleştiriniz!";
            return redirect()->route('Content.newinstagram.login')->with([
                "instagram_message" => $message, "instagram_message_type" => "error"
            ]);
        }

        return view('ContentPanel::newinstagram.index');

    }

    public function login()
    {
        $needVerify = true;
        $verifyUrl = null;

        $verifyUrl = "https://api.instagram.com/oauth/authorize?".
            "client_id=".$this->appID
            ."&redirect_uri=".$this->HTTPS.$this->APP_NAME.$this->DIRECTORY_SEPARATOR   .$this->PANEL_AUTH_SCOPE
            ."&scope=user_profile,user_media"
            ."&response_type=code";

        if ($this->appID == null || $this->appSecret == null || $this->appID == "" || $this->appSecret == "") {
            $needVerify = false;

        }

        return view("ContentPanel::newinstagram.login", compact("verifyUrl", "needVerify"));
    }

    public function userTimelineRoute()
    {
        $posts = $this->getUserTimeLine(10);

        if (!empty($posts)) {
            return view('ContentPanel::newinstagram.user-timeline', compact("posts"));
        }

        return redirect()->route('Content.newinstagram.index');
    }

    /**
     * Helper Functions
     **/

    public function getUserTimeLine($limit = 5, $page = 1)
    {
        $data = array(
            "fields" => "id,media_type,media_url,permalink,thumbnail_url",
            "limit" => $limit,
            "access_token" => $this->accessToken
        );


        $url = 'https://graph.instagram.com/me/media';
        $result = $this->requestTypeGet($url, $data);
        if ($result == false) {
            return [];
        }
        return $result["data"];
    }

    public function getUserInformations()
    {
        $data = array(
            "fields" => "id,username",
            "access_token" => $this->accessToken
        );

        $params = '';

        foreach ($data as $key => $value) {
            $params .= $key.'='.$value.'&';
        }

        $params = trim($params, '&');

        $url = 'https://graph.instagram.com/me';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);
        return json_decode($result, 1);
    }

    public function requestTypeGet($url, $data = null)
    {
        $params = '';
        foreach ($data as $key => $value) {
            $params .= $key.'='.$value.'&';
        }

        $params = trim($params, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        $result = json_decode($json, 1);
        curl_close($ch);

        $ex = false;
        try {
            $control = $result["data"];
        } catch (\Exception $exception) {
            Cache::store('file_not_deleted')->forget("instagram_access_token");
            Cache::store('file_not_deleted')->forget("instagram_user_id");
            return false;
        }

        return $result;
    }

    /**
     * Auth Process
     **/

    public function auth(Request $request)
    {
        $this->authToken = $request->code;

        $url = "https://api.instagram.com/oauth/access_token";
        $postData = '';

        $params = array(
            "client_id" => $this->appID,
            "client_secret" => $this->appSecret,
            "grant_type" => "authorization_code",
            "redirect_uri" => $this->HTTPS.$this->APP_NAME.$this->DIRECTORY_SEPARATOR.$this->PANEL_AUTH_SCOPE,
            "code" => $this->authToken
        );
        foreach ($params as $k => $v) {
            $postData .= $k.'='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);
        curl_close($ch);
        $arr = json_decode($output, 1);

        $result = [
          'userID' => $arr['user_id'],
          'accessToken' => $arr['access_token'],
        ];
        file_put_contents($this->tokenFile, json_encode($result));
        $this->information = json_decode(file_get_contents($this->tokenFile), 1);

        try {
            $this->accessToken = $arr["access_token"];
            $this->userID = $arr["user_id"];
        } catch (\Exception $exception) {
            $message = json_decode($output, 1);
            return redirect()->route('Content.newinstagram.login')->with([
                "message" => $message["error_type"]."-".$message["error_message"], "message_type" => "error"
            ]);
        }

        $this->getLongLivedAccessToken();

        return redirect()->route('Content.newinstagram.index');

    }

    public function getLongLivedAccessToken()
    {
        $data = array(
            "grant_type" => "ig_exchange_token",
            "client_secret" => $this->appSecret,
            "access_token" => $this->accessToken
        );

        $url = 'https://graph.instagram.com/access_token';

        $params = '';
        foreach ($data as $key => $value) {
            $params .= $key.'='.$value.'&';
        }

        $params = trim($params, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        $result = json_decode($json, 1);
        curl_close($ch);

        try {
            $this->accessToken = $result["access_token"];
        } catch (\Exception $exception) {
            $message = json_decode($result, 1);
            return redirect()->route('Content.newinstagram.login')->with([
                "message" => $message["error_type"]."-".$message["error_message"], "message_type" => "error"
            ]);
        }

        $this->information['accessToken'] = $result["access_token"];
        file_put_contents($this->tokenFile, json_encode($this->information));

    }

    public function deauth()
    {

        file_put_contents($this->tokenFile, "");

        return redirect()->route('Content.newinstagram.index');
    }


}
