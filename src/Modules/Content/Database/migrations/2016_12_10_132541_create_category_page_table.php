<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPageTable extends Migration
{

    public const CATEGORY_PAGE = 'category_page';

    public function up()
    {
        if (!Schema::hasTable(self::CATEGORY_PAGE)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CATEGORY_PAGE, function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('page_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CATEGORY_PAGE);
    }
}
