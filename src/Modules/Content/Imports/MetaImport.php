<?php

namespace Mediapress\Modules\Content\Imports;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Mediapress\Modules\Content\Models\Meta;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MetaImport implements ToModel, WithHeadingRow, WithChunkReading
{
    const title = "title";
    const description = "description";
    const url_id = "url_id";
    const id = "id";
    const key = "key";

    public function model(array $row)
    {
        $defaultMetaTypes = ['title', 'description', 'keywords'];
        foreach ($defaultMetaTypes as $metaType){
            $meta = Meta::where(self::url_id, $row[self::id])->where(self::key, $metaType)->first();
            if ($meta){
                $meta->update([
                    "desktop_value"  => $row[$metaType],
                ]);
            }
        }
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

}
