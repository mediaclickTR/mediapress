<?php

return [
    'page' => 'Page',
    'pages' => 'Pages',
    'create_page' => 'Create Page',
    'edit_page' => 'Edit Page',
    'new_page' => 'New Page',
    'name_page' => 'Page Name',
    'name' => 'Name',
    'sort' => 'Order',
    'password_invalid' => 'Invalid Password',
    'edit_order' => "Edit Order",
    'status' => [
        'PASSIVE' => 'PASSIVE',
        'ACTIVE' => 'ACTIVE',
        'DRAFT' => 'DRAFT',
        'PREDRAFT' => 'PRE-DRAFT',
        'PENDING' => 'PENDING',
        'POSTDATE' => 'POST-DATED'
    ],
    'goto_page' => 'Go to Page',
    'page_structure_slug' => 'Page Structure Slug',
    'page_content' => 'Page Content',
    'all_categories' => 'Select Category',
    'page_not_found' => 'Page Not Found',
    'page_not_found_by_id' => 'Page Not Found: (id::id)',

    'preview_box_text' => 'There is no any snapshot for search engine result before you save.',

    'order_page' => [
        'info' => 'After updating the rank, the rankings of other pages are automatically updated.',
        'status' => 'Status',
        'order' => 'Order',
        'category_name' => 'Category Name',
        'page_name' => 'Page Name',
        'active' => 'Active',
        'passive' => 'Passive',
    ],
];
