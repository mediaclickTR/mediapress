<?php

namespace Mediapress\Modules\Content\Exceptions;

class PageRenderableNotFoundException extends \Exception
{
    public $path;

    public function __construct(string $message = "", $path=null)
    {
        parent::__construct($message);
        $this->path = $path;
    }

}

