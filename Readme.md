
# Mediapress

## Lavarel based CMS Framework


###Installation

Adding MediaPress to composer.json

>```composer require mediapress/mediapress "^4.3"```

Update ```.env``` File
>Update your DB connection

>Update APP_URL=[domain]


Install migrations

>```php artisan migrate```

Seed your database

>```php artisan db:seed```

Publish assets, config and views

>```php artisan vendor:publish```

Install Mediapress

>```php artisan mp:install```
