<?php

namespace Mediapress\Foundation;

use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\CountryGroupLanguage;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\Content\Models\Meta;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\MediapressTrait;
use Mediapress\QuickAccess\Models\SeoContent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * Class Mediapress
 * @package Mediapress\Modules\MPCore\Foundation
 * @property Model|PageDetail|CategoryDetail|SitemapDetail relation
 * @property Language defaultLanguage
 * @property Url url
 * @property Website website
 * @property Url homePageUrl
 * @property CountryGroup activeCountryGroup
 * @property Language activeLanguage
 * @property Collection<Meta> metas
 * @property Collection<Category> categories
 * @property Sitemap sitemap
 * @property Model|Sitemap|Category|Page parent
 */
class Mediapress
{
    use MediapressTrait;

    const DETAIL_STR = "detail";
    const PAGES_DOT_ID_STR = "pages.id";

    public $data;
    public $panel = false;

    public function website()
    {
        if (property_exists(self::class, "url")) {
            return $this->url->website();
        } else {

            $subsite = self::attemptToDetectWebsite();
            if ($subsite) {
                return $subsite;
            }
            $website = Website::where('slug', str_replace("www.", "", request()->getHost()))->first();
            if (!$website) {
                $website = Website::first();
            }
            return $website;
        }
    }

    protected function attemptToDetectWebsite()
    {
        $domain = request()->getHost();
        $path = preg_split('/\//', request()->path());
        $path = $path[0];

        if ($path) {
            $subfolder_site = Website::where('slug', $path)->first();
            if ($subfolder_site) {
                $main_site = Website::where('slug', $domain)->first();
                if ($main_site && $subfolder_site->website_id == $main_site->id) {
                    return $subfolder_site;
                }
            }
        }

        return false;
    }

    //Languages grupla

    public function defaultLanguage()
    {
        return $this->website->defaultLanguage();
    }

    public function activeCountryGroup()
    {
        if (isset($this->activeCountryGroup)) {
            return $this->activeCountryGroup;
        }
        if (is_a($this->relation, CategoryDetail::class) || is_a($this->relation,
                PageDetail::class) || is_a($this->relation, SitemapDetail::class)) {
            /** @var CategoryDetail $relation */
            return $this->relation->countryGroup();
        } elseif (is_null($this->relation)) {
            $website = $this->website();
            if ($website) {
                return $website->predictCountryGroup();
            }
        }

        throw new \InvalidArgumentException(__CLASS__ . " New Type Founded : " . get_class($this->relation), __LINE__);

    }

    public function activeLanguage()
    {
        if (!isset($this->url)) {

            if(!isset($this->activeLanguage)){
                $this->activeLanguage = $this->website->predictLanguage();
            }
            return $this->activeLanguage;
        } else {
            if (is_a($this->relation, CategoryDetail::class) ||
                is_a($this->relation, PageDetail::class) ||
                is_a($this->relation, SitemapDetail::class)) {
                /** @var CategoryDetail $relation */

                $this->activeLanguage = $this->relation->language()->where('id', $this->relation->language_id)->first();
                return $this->activeLanguage;
            } else {
                throw new \InvalidArgumentException(__CLASS__ . " New Type Founded : " . get_class($this->relation),
                    __LINE__);
            }
        }


    }

    public function addNew()
    {
        if (!isset($this->url)) {
            return false;
        } else {

            $sitemap = $this->sitemap;
            $type = $sitemap->sitemapType;
            if ($type->sitemap_type_type == 'dynamic') {
                return '/mp-admin/Content/Pages/' . $sitemap->id . '/create';
            }
            return false;

        }
    }

    public function metas()
    {
        return $this->url->metas();
    }

    public function relation()
    {
        return $this->url->model();
    }

    public function sitemap()
    {
        return optional($this->parent)->sitemap();
    }

    public function parent()
    {
        if (is_a($this->relation, CategoryDetail::class)) {
            return $this->relation->category();
        } else {
            if (is_a($this->relation, PageDetail::class)) {
                return $this->relation->page();
            } else {
                if (is_a($this->relation, SitemapDetail::class)) {
                    return $this->relation->sitemap();
                }
            }
        }
    }

    public function homePageUrl()
    {

        $website_id = $this->website->id;
        $activeLanguage = $this->activeLanguage->id;
        $activeCountryGroup = $this->activeCountryGroup->id;


        $homepage = SitemapDetail::where('language_id', $activeLanguage)->where('country_group_id',
            $activeCountryGroup)->whereHas('sitemap', function ($q) use ($website_id) {
            $q->where("feature_tag", 'homepage')->whereHas('websites', function ($q) use ($website_id) {
                $q->where("id", $website_id);
            });
        })->first();

        if ($homepage) {
            return $this->homePageUrl = $homepage->url;
        }

        return new Url(["url" => "/"]);
    }

    public function breadcrumb(array $injections = [])
    {

        $settings = \Mediapress\Modules\MPCore\Models\SettingSun::where('group', 'Breadcrumb')
            ->where('website_id', $this->website->id)
            ->pluck('value', 'key');


        $breadcrumb = breadcrumb();

        if (isset($settings['breadcrumb.add_homepage']) && $settings['breadcrumb.add_homepage'] == 0) {
            $breadcrumb = $breadcrumb->addHomePage(false);
        }
        if (isset($settings['breadcrumb.add_domain']) && $settings['breadcrumb.add_domain'] == 0) {
            $breadcrumb = $breadcrumb->addDomain(false);
        }

        if (isset($settings['breadcrumb.string_type']) && $settings['breadcrumb.string_type']) {
            $breadcrumb = $breadcrumb->setStringType($settings['breadcrumb.string_type']);
        }

        if (is_a($this->relation, SitemapDetail::class)) {

            if (isset($settings['breadcrumb.menu_slug']) && $settings['breadcrumb.menu_slug']) {
                $breadcrumb = $breadcrumb->setMenu($settings['breadcrumb.menu_slug']);
            }

        } elseif (is_a($this->relation, PageDetail::class)) {

            if ($this->parent->category()) {
                if (isset($settings['breadcrumb.category_depth']) && $settings['breadcrumb.category_depth']) {
                    $breadcrumb = $breadcrumb->setCategoryDepth($settings['breadcrumb.category_depth'] * 1);
                }
            } else {
                if (isset($settings['breadcrumb.menu_slug']) && $settings['breadcrumb.menu_slug']) {
                    $breadcrumb = $breadcrumb->setMenu($settings['breadcrumb.menu_slug']);
                }
            }

        } else {
            if (isset($settings['breadcrumb.category_depth']) && $settings['breadcrumb.category_depth']) {
                $breadcrumb = $breadcrumb->setCategoryDepth($settings['breadcrumb.category_depth'] * 1);
            }
        }

        $breadcrumb = $breadcrumb->setInjections($injections)->get();

        return $breadcrumb;
    }

    public function categories()
    {
        if ($this->parent) {
            if (is_a($this->parent, Sitemap::class) || is_a($this->parent, Page::class)) {
                return $this->parent->categories();
            } else {
                if (is_a($this->parent, Category::class)) {
                    return $this->parent->children();
                }
            }
        }
    }


    public function otherCountryGroupLanguages($exceptActive = false, $sort = null)
    {
        //TODO: yapılacak
        $relation = $this->relation;

        if (!$relation || ($relation->sitemap && $relation->sitemap->feature_tag == 'search')) {
            return collect([]);
        }
        $activeCountryGroupId = $relation->countryGroup->id;
        $activeLanguageId = $relation->language->id;

        $details = $this->parent->details()->get();
        $others = [];

        foreach ($details as $detail) {
            $language = $detail->language;
            $countryGroup = $detail->countryGroup;

            if ($exceptActive === false && $activeCountryGroupId == $countryGroup->id && $activeLanguageId == $language->id) {
                continue;
            }

            $url = $detail->url()->with("model.countryGroup")->first();
            if (is_null($url)) {
                continue;
            }

            $others[] = [
                'language' => $language,
                'language_code' => $language->code,
                'language_id' => $language->id,
                'country_group' => $countryGroup,
                'country_group_code' => $countryGroup->code,
                'country_group_id' => $countryGroup->id,
                'name' => strip_tags($detail->name),
                'code' => strtoupper($countryGroup->code) . '_' . strtoupper($language->code),
                'url' => $url,
                'active' => ($activeCountryGroupId == $countryGroup->id && $activeLanguageId == $language->id)
            ];
        }

        $collect = collect($others);
        if ($sort) {

            $collect = $collect->sortByDesc('active');
        }

        return $collect;
    }

    public function otherCountryGroups($exceptActive = 1, $sort = null)
    {
        //TODO: yapılacak
        $relation = $this->relation;

        if (!$relation || ($relation->sitemap && $relation->sitemap->feature_tag == 'search')) {
            return collect([]);
        }
        $active_code = $relation->countryGroup->code;


        $defaultCountryGroupLanguage = CountryGroupLanguage::where('default', 1)
            ->pluck('language_id', 'country_group_id')
            ->toArray();

        $details = $this->parent->details()
            ->whereIn('country_group_id', array_keys($defaultCountryGroupLanguage))
            ->whereIn('language_id', array_values($defaultCountryGroupLanguage))
            ->get();

        $others = [];

        foreach ($details as $detail) {
            $language = $detail->language;
            $countryGroup = $detail->countryGroup;
            $url = $detail->url()->with("model.countryGroup")->first();

            if (is_null($url)) {
                continue;
            }

            if (($exceptActive == 1 && $countryGroup->code != $active_code) || $exceptActive != 1) {
                $others[] = [
                    'language' => $language,
                    'language_code' => $language->code,
                    'language_id' => $language->id,
                    'country_group' => $url->model->countryGroup,
                    'country_group_code' => $url->model->countryGroup->code,
                    'country_group_id' => $url->model->countryGroup->id,
                    'name' => strip_tags($detail->name),
                    'url' => $url,
                    'active' => $language->code == $active_code
                ];
            }
        }
        $collect = collect($others);
        if ($sort) {

            $collect = $collect->sortByDesc('active');
        }

        return $collect;
    }

    public function otherLanguages($exceptActive = 1, $sort = null)
    {
        //TODO: yapılacak
        $relation = $this->relation;
        if (is_null($relation)) {
            $active_code = $this->defaultLanguage()->code;
            $activeCountryGroupId = $this->activeCountryGroup()->id;
        } else {
            $active_code = $relation->language->code;
            $activeCountryGroupId = $relation->country_group_id;
        }


        $homePage = Sitemap::where('feature_tag', 'homepage')->first();
        $homePageDetails = $homePage->details()->where('country_group_id', $activeCountryGroupId)->get();;

        $homePageOthers = [];
        foreach ($homePageDetails as $detail) {
            $language = $detail->language;
            $url = $detail->url()->with("model.countryGroup")->first();

            if (is_null($url)) {
                continue;
            }
            if (($exceptActive == 1 && $language->code != $active_code) || $exceptActive != 1 || is_null($relation)) {
                $homePageOthers[$url->model->countryGroup->code . $language->code] = [
                    'language' => $language,
                    'language_code' => $language->code,
                    'language_id' => $language->id,
                    'country_group' => $url->model->countryGroup,
                    'country_group_code' => $url->model->countryGroup->code,
                    'country_group_id' => $url->model->countryGroup->id,
                    'name' => $detail->name,
                    'url' => $url,
                    'active' => $language->code == $active_code
                ];
            }
        }

        if (!$relation || ($relation->sitemap && $relation->sitemap->feature_tag == 'search')) {
            return collect($homePageOthers);
        }

        $details = $this->parent->details()->where('country_group_id', $activeCountryGroupId)->get();
        $others = [];
        foreach ($details as $detail) {
            $language = $detail->language;
            $url = $detail->url()->with("model.countryGroup")->first();
            if( $this->url->type == 'category' ){
                $url = Url::where('model_id', $detail->id)->where('type', 'category')->first();
            }
            if (is_null($url)) {
                continue;
            }

            if (($exceptActive == 1 && $language->code != $active_code) || $exceptActive != 1) {
                $others[$url->model->countryGroup->code . $language->code] = [
                    'language' => $language,
                    'language_code' => $language->code,
                    'language_id' => $language->id,
                    'country_group' => $url->model->countryGroup,
                    'country_group_code' => $url->model->countryGroup->code,
                    'country_group_id' => $url->model->countryGroup->id,
                    'name' => $detail->name,
                    'url' => $url,
                    'active' => $language->code == $active_code
                ];
            }
        }
        $collect = collect(array_merge($homePageOthers, $others));
        if ($sort) {

            $collect = $collect->sortByDesc('active');
        }

        return $collect;
    }

    public function otherLanguagesforMeta($exceptActive = 1, $sort = null)
    {
        //TODO: yapılacak
        $relation = $this->relation;

        if (is_null($relation)) {
            $active_code = $this->defaultLanguage()->code;
            $activeCountryGroupId = $this->activeCountryGroup()->id;
        } else {
            $active_code = $relation->language->code;
            $activeCountryGroupId = $relation->country_group_id;
        }

        $countryGroups = \DB::table('country_groups')->get()->pluck('id')->toArray();
        $homePage = \DB::table('sitemaps')->where('feature_tag', 'homepage')->first();
        $homePageDetails = \DB::table('sitemap_details')
            ->select(
                'sitemap_details.id',
                'sitemap_details.name',
                'sitemap_details.language_id',
                'sitemap_details.country_group_id',
                'country_groups.code as country_group_code'
            )
            ->join('country_groups', function($join){
                $join->on('country_groups.id', '=', 'sitemap_details.country_group_id');
            })
            ->where('sitemap_id', $homePage->id)
            ->whereIn('country_group_id', $countryGroups)
            ->get();
        $homePageOthers = [];
        foreach ($homePageDetails as $detail) {
            $language = \DB::table('languages')->where('id', $detail->language_id)->first();
            $country_group = \DB::table('country_groups')->where('id', $detail->country_group_id)->first();
            $url = \DB::table('urls')->where('model_id', $detail->id)->where('model_type', 'Mediapress\Modules\Content\Models\SitemapDetail')->first();

            if (is_null($url)) {
                continue;
            }
            if (($exceptActive == 1 && $language->code != $active_code) || $exceptActive != 1 || is_null($relation)) {
                $homePageOthers[$country_group->code . $language->code] = [
                    'language' => $language,
                    'language_code' => $language->code,
                    'language_id' => $language->id,
                    'country_group' => $country_group,
                    'country_group_code' => $country_group->code,
                    'country_group_id' => $country_group->id,
                    'name' => $detail->name,
                    'url' => $url,
                    'active' => $language->code == $active_code
                ];
            }
        }

        if (!$relation || ($relation->sitemap && $relation->sitemap->feature_tag == 'search')) {
            return collect($homePageOthers);
        }

        $details = $this->parent->details()->whereIn('country_group_id', $countryGroups)->get();
        $others = [];
        foreach ($details as $detail) {
            $language = $detail->language;
            $url = $detail->url()->with("model.countryGroup")->first();
            if( $this->url->type == 'category' ){
                $url = Url::where('model_id', $detail->id)->where('type', 'category')->first();
            }
            if (is_null($url)) {
                continue;
            }

            if (($exceptActive == 1 && $language->code != $active_code) || $exceptActive != 1) {
                $others[$url->model->countryGroup->code . $language->code] = [
                    'language' => $language,
                    'language_code' => $language->code,
                    'language_id' => $language->id,
                    'country_group' => $url->model->countryGroup,
                    'country_group_code' => $url->model->countryGroup->code,
                    'country_group_id' => $url->model->countryGroup->id,
                    'name' => $detail->name,
                    'url' => $url,
                    'active' => $language->code == $active_code
                ];
            }
        }
        $collect = collect(array_merge($homePageOthers, $others));
        if ($sort) {

            $collect = $collect->sortByDesc('active');
        }

        return $collect;
    }

    public function menu($slug)
    {
        return menu(['slug' => $slug], $this->website, $this->activeCountryGroup,
            $this->activeLanguage)->get()->toTree(['parent_id' => 'parent']);
    }

    public function form($slug)
    {
        return Form::whereHas("websites", function ($q) {
            $q->where("id", $this->website->id);
        })->where(compact("slug"))->first();
    }

    public function loginForUrl($url, $password)
    {
        if (!(is_string($url) || is_a($url, Url::class))) {
            throw new \InvalidArgumentException("Parameter sent is not a valid url");
        }

        if (is_string($url)) {
            $url = Url::where('url', $this->next)->first();
        }

        if (is_null($url) || !$url->exists) {
            throw new \InvalidArgumentException("Parameter sent is not a valid url");
        }

        if (Hash::check($password, $url->password)) {
            session()->put("password_for_" . $url->id, true);
            return true;
        }

        $model = $url->model;
        if ($model instanceof PageDetail || $model instanceof SitemapDetail) {
            if ($password == $model->parent->password) {
                session()->put("password_for_" . $url->id, true);
                return true;
            }
        }

        return false;
    }

    public function tagManager($part = 'head')
    {
        $tagManager = SettingSun::where([
            'key' => "google.tagmanager.container.publicId", "website_id" => $this->website->id
        ])->first();

        if ($tagManager && $part == 'head') {
            return '   <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'' . $tagManager->value . '\');</script>
<!-- End Google Tag Manager -->';
        } elseif ($tagManager && $part == 'body') {
            return '<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . $tagManager->value . '"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
        }
        return null;
    }

    public function seoContent()
    {
        $seoContent = SeoContent::select('h1', 'h2', 'detail')->where('url_id', $this->url->id)->first();

        if ($seoContent) {
            return $seoContent->toArray();
        }
        return [
            "h1" => null,
            "h2" => null,
            "detail" => null
        ];
    }
}
