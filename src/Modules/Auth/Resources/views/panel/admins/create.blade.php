@extends('AuthPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("AuthPanel::admin.create-admin") !!}</div>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="col-md-12 p-0">
                @include("AuthPanel::admins.form")
            </div>
        </div>
    </div>
@endsection
