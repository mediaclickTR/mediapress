@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")

        <div class="p-30 mt-4 pb-0">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="{!! route('MPCore.translate.index') !!}"
                       class="lang-btn active show">
                        <i class="far fa-2x fa-language mt-0"></i>
                        {!! __('MPCorePanel::translate.tab.langPart') !!}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('Content.export_pages.selectLanguage') !!}"
                       class="lang-btn">
                        <i class="fas fa-file-export"></i>
                        {!! __('MPCorePanel::translate.tab.exportContent') !!}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('Content.import_pages.selectLanguage') !!}"
                       class="lang-btn">
                        <i class="fas fa-file-import"></i>
                        {!! __('MPCorePanel::translate.tab.importContent') !!}
                    </a>
                </li>
            </ul>
        </div>

        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! __("MPCorePanel::translate.langPart_title") !!}</div>
            </div>
            <div class="float-right">
                <div class="row">
                    <div class="p-0 mr-2">
                        <a href="javascript:void(0);" class="btn btn-secondary float-right"
                           data-toggle="modal" data-target="#importModal"
                           title="Import">
                            <i class="fas fa-file-import"></i>
                        </a>
                    </div>
                    <div class="p-0">
                        <a href="{!! route("MPCore.translate.export") !!}" class="btn btn-secondary float-right"
                           data-toggle="tooltip"
                           data-placement="left" title="Export">
                            <i class="fas fa-file-export"></i>
                        </a>
                    </div>
                    <div class="p-0 ml-2">
                        <button type="button" class="btn btn-success float-right"
                                data-toggle="tooltip"
                                data-placement="left"
                                title="{!! __("MPCorePanel::translate.save_info") !!}">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>


                    <div class="col">
                        <input type="text" class="form-control" id="lang-search" placeholder="{{ __('MPCorePanel::translate.search') }}"
                               autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="p-30">
            <div class="language-content">
                @include('MPCorePanel::translate.ajax')
            </div>
        </div>
    </div>

    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excel Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{!! route("MPCore.translate.import") !!}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group mt-3">
                            <input type="file" class="form-control-file" name="excelFile">
                        </div>
                        <div class="p-2">
                            <button type="submit" class="btn btn-secondary float-right mb-3 mt-3">{{ __('MPCorePanel::general.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")
    <style>
        .btn i {
            margin-right: 0px;
        }
    </style>
@endpush

@push('scripts')
    <script>


        $('.page-content').delegate('.open_tab', 'click', function (e) {
            e.preventDefault();
        });


        $('.page-content').delegate('.lang_part', 'change blur', function (e) {
            var input = this;
            var data = $(this).serialize() + '&_token={!! csrf_token() !!}';
            $.ajax({
                type: "POST",
                url: "{!! route("MPCore.translate.save") !!}",
                data: data,
                async: false,
                dataType: "json",
                success: function (data) {
                    $(input).css('border-color', '#155724');
                    setTimeout(function () {
                        $(input).css('border-color', '#d2d2d2');
                    }, 2000);
                    /*
                    const toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    toast({
                        type: 'success',
                        title: '{!! __("MPCorePanel::general.success_message") !!}'
                    })
                    */
                },
                error: function (status1) {
                    console.log(status1);
                }
            });
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

    </script>

    <!-- LangPart Search -->
    <script>

        var _changeInterval = null;
        $('#lang-search').on('keyup', function () {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function () {
                clearInterval(_changeInterval);
                search = $('#lang-search').val();

                $.ajax({
                    'url': "{!! route("MPCore.translate.search") !!}",
                    'data': {'_token': "{!! csrf_token() !!}", search_key: search},
                    'method': 'post',
                    success: function (response) {
                        $('.language-content').html(response);

                        var pana = $('.lang-box .item button');
                        pana.click(function () {
                            if ($(this).parent().parent().parent().hasClass('open')) {
                                $(this).parent().parent().parent().removeClass('open');
                                $(this).prev().removeClass('open');
                                $(this).parent().parent().parent().find('.edit-center').slideUp();
                                $(this).parent().parent().parent().find('.text').slideUp();
                                $(this).parent().parent().parent().find('.edit').slideUp();
                            } else {
                                pana.parent().parent().parent().find('.edit-center').slideUp();
                                pana.parent().parent().parent().find('.edit').slideUp();
                                pana.parent().parent().parent().find('.text').slideUp();
                                pana.parent().parent().parent().removeClass('open');
                                pana.prev().removeClass('open');
                                $(this).parent().parent().parent().addClass('open');
                                $(this).prev().addClass('open');
                                $(this).parent().parent().parent().find('.edit-center').slideDown();
                                $(this).parent().parent().parent().find('.text').slideDown();
                                $(this).parent().parent().parent().find('.edit').slideDown();
                            }
                        });
                    }
                });
            }, 400);
        });


    </script>
@endpush

@push("styles")
    <style>
        .language-flag {
            float: left;
            padding: 5px;
        }

        .language-title {
            line-height: 20px;
            float: left;
            padding: 5px;
        }

        .lang_area {
            margin-top: 10px;
        }

        .lang-box form .edit .form-group input {
            width: 100% !important;
        }

        .head {
            border-bottom: 1px solid #f2f2f2;
            margin-top: 10px;
            padding: 10px;
        }
    </style>
@endpush
