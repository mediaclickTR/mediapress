<div id="mcTopTooltip" style="display: none" data-bar-place="{{ $popup->place ?: 'top' }}">
    <div id="allTopTooltip" data-bar-bg="{{ $popup->background }}" data-bar-color="{{ $popup->color }}">

        @if($popup->button_type == 1)
            <button type="button" id="mcTopTooltipClose"
                    class="mcPopupClose"
                    data-dismiss="modal"
                    aria-label="Close"
                    data-bar-button-bg="{{ $popup->button_bg }}"
                    data-bar-button-color="{{ $popup->button_color }}">
                <span aria-hidden="true">&times;</span>
            </button>
        @endif

        <div id="mctoolTipText" data-bar-a-color="{{ $popup->a_color }}"
             style="{{ $popup->font ? "font-family: '$popup->font'" : '' }}">
            @if($popup->name_display == 1)
                <h5 class="mcTopTooltipTitle" data-bar-title-size="{{ $popup->title_size }}">{{$popup->name}}</h5>
            @endif

            {!! $popup->content !!}

            @if($popup->button_type == 2)
                <div class="button">
                    <button class="btn confirmBtn" id="mcTopTooltipClose" data-buttonText-color="{{ $popup->button_color }}"
                            data-buttonBg-color="{{ $popup->button_bg }}">{{ $popup->button_text }}</button>
                </div>
            @endif
        </div>
    </div>
</div>


<script>
    var $topHeight = $('#mcTopTooltip').height();
    var $place = $('#mcTopTooltip').attr('data-bar-place');
    $('#mcTopTooltip').each(function () {
        $('body').addClass($place);

        $(this).addClass('' + $place + '');
        $('#mcTopTooltip.bottom').css('bottom', '-' + $topHeight + 'px');
        $('#mcTopTooltip.top').css('top', '-' + $topHeight + 'px');
        $(this).show();
    });

    $('#allTopTooltip').each(function () {
        var $topBg = $(this).attr("data-bar-bg");
        var $topColor = $(this).attr("data-bar-color");
        @if($popup->button_type == 1)
            var $topbuttonBg = $(this).find('.mcPopupClose').attr("data-bar-button-bg");
            var $topbuttonColor = $(this).find('.mcPopupClose').attr("data-bar-button-color");
            $(this).find('.mcPopupClose').css({
                'backgroundColor': '' + $topbuttonBg + '',
                'color': '' + $topbuttonColor + ''
            });
        @else
            var $topbuttonBg = $(this).find('.button a,button').attr("data-buttonBg-color");
            var $topbuttonColor = $(this).find('.button a,button').attr("data-buttonText-color");

            $(this).find('.button a,button').css({
                'backgroundColor': '' + $topbuttonBg + '',
                'color': '' + $topbuttonColor + ''
            });
        @endif

        var $topTitleSize = $(this).find('.mcTopTooltipTitle').attr("data-bar-title-size");
        var $aTagColor = $(this).find('#mctoolTipText').attr("data-bar-a-color");

        $(this).css({
            'backgroundColor': '' + $topBg + '',
            'height': '' + $topHeight + 'px'
        });
        $(this).find('#mctoolTipText').css({
            'color': '' + $topColor + '',
            'font-family': '{{ $popup->font }}, sans-serif !important;'
        });
        $(this).find('.mcTopTooltipTitle').css({
            'font-size': '' + $topTitleSize + 'px'
        });

        $(this).find('#mctoolTipText a').css(
            'cssText', "color: " + $aTagColor + " !important"
        );

        var $buttonTextWidth = $(this).find('.button').width() + 30;
        $('#mctoolTipText').css('paddingRight', '' + $buttonTextWidth + 'px');
    });


    @if($popup->conditions == 1)
        $(window).scroll(function () {
            if ($(document).scrollTop() > {{ $popup->condition_pixel }}) {
                $('#mcTopTooltip').addClass('active');
                if ($('#mcTopTooltip').hasClass('top')) {
                    $('body').css('marginTop', $topHeight + 'px');
                }
                $('#mcTopTooltip.top').css('top', 0);
                $('#mcTopTooltip.bottom').css('bottom', 0);

            }
        });
    @else
        @if($popup->condition_time > 0)

            $(window).on('load', function () {
                setTimeout(function () {
                    $('#mcTopTooltip').addClass('active');
                    if ($('#mcTopTooltip').hasClass('top')) {
                        $('body').css('marginTop', $topHeight + 'px');
                    }

                    $('#mcTopTooltip.top').css('top', 0);
                    $('#mcTopTooltip.bottom').css('bottom', 0);

                },{{$popup->condition_time*1000}});
            });
        @else
            $(window).on('load', function () {
                setTimeout(function () {
                    $('#mcTopTooltip').addClass('active');
                    if ($('#mcTopTooltip').hasClass('top')) {
                        $('body').css('marginTop', $topHeight + 'px');
                    }
                    $('#mcTopTooltip.top').css('top', 0);
                    $('#mcTopTooltip.bottom').css('bottom', 0);
                }, 100);
            });
        @endif
    @endif

    @if($popup->display_time_type == 1)
        setTimeout(function () {
            $('#mcTopTooltip').removeClass('active');

            if ($('#mcTopTooltip').hasClass('top')) {
                $('body').css('marginTop', $topHeight + 'px');
            }
                $('#mcTopTooltip.top').css('top', 0);
                $('#mcTopTooltip.bottom').css('bottom', 0);

        }, {{ ($popup->display_time + $popup->condition_time) *1000}});
    @endif

    $('#mcTopTooltipClose').click(function () {
        $('#mcTopTooltip.top').css('top', -$topHeight + 'px');
        $('#mcTopTooltip.bottom').css('bottom', -$topHeight + 'px');
        $('body').css('marginTop', 0 + 'px');
        $('#mcTopTooltip').removeClass('active');
    });

</script>
