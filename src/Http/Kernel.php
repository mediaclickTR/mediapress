<?php

namespace Mediapress\Http;


use Mediapress\Http\Middleware\AfterMiddleware;
use Mediapress\Http\Middleware\AuthenticateMiddleware;
use Mediapress\Http\Middleware\DefaultLng;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Mediapress\Http\Middleware\Flow;
use Mediapress\Modules\Auth\Middleware\RedirectIfAuthenticated;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [];


    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'panel.auth' => AuthenticateMiddleware::class,
        'defaultLng' => DefaultLng::class,
        'flow' => Flow::class,
        'mediapress.after' => AfterMiddleware::class,
        'guest' => RedirectIfAuthenticated::class
    ];

}
