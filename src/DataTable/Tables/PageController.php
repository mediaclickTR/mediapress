<?php

namespace Mediapress\DataTable\Tables;

use Html;
use Mediapress\DataTable\DataTableInterface;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\MPCore\Models\CountryGroupLanguage;
use Yajra\DataTables\Facades\DataTables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

const SITEMAP_ID = 'sitemap_id';

class PageController implements DataTableInterface
{

    const DATA = 'data';
    const NAME = 'name';
    const TITLE = 'title';
    const FOOTER = 'footer';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const SITEMAP_ID = 'sitemap_id';

    public function columns(Builder $builder)
    {
        $request = request();
        return $builder->parameters([
            "lengthMenu" => [25, 50, 75, 100 ],
            "pageLength" => session('pageLength.sitemaps.' . $request->segments()[count($request->segments()) - 1], 25)
        ])->columns([
            [
                self::DATA => 'order',
                self::NAME => 'order',
                self::TITLE => trans("MPCorePanel::general.order"),
                self::FOOTER => trans("MPCorePanel::general.order")
            ],
            [
                self::DATA => 'status',
                self::NAME => 'status',
                self::TITLE => '<i class="fa fa-info-circle"></i>',//trans("MPCorePanel::general.status"),
                self::FOOTER => '<i class="fa fa-info-circle"></i>',//trans("MPCorePanel::general.status"),
            ],
            [
                self::DATA => 'detail.name',
                self::NAME => 'detail.name',
                self::TITLE => trans("MPCorePanel::general.name"),
                self::FOOTER => trans("MPCorePanel::general.name")
            ],
            /*[
                self::DATA => 'detail.slug',
                self::NAME => 'detail.slug',
                self::TITLE => trans("MPCorePanel::general.slug"),
                self::FOOTER => trans("MPCorePanel::general.slug")
            ],*/
            [
                self::DATA => 'detail.detail',
                self::NAME => 'detail.detail',
                self::TITLE => trans("MPCorePanel::general.detail"),
                self::FOOTER => trans("MPCorePanel::general.detail")
            ],
            /* [
                 self::DATA => self::CREATED_AT,
                 self::NAME => self::CREATED_AT,
                 self::TITLE => trans("MPCorePanel::general.created_at_sm"),
                 self::FOOTER => trans("MPCorePanel::general.created_at_sm")
             ],*/
            [
                self::DATA => self::UPDATED_AT,
                self::NAME => self::UPDATED_AT,
                self::TITLE => trans("MPCorePanel::general.updated_at_sm"),
                self::FOOTER => trans("MPCorePanel::general.updated_at_sm")
            ]
        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);
    }

    public function ajax($website_id, Sitemap $sitemap, $parameter = null)
    {
        $request = request();

        $url = parse_url($request->header('referer'));
        if (isset($url['query'])) {
            parse_str('&' . $url['query'], $array);
        }
        $category_id = $array['category'] ?? null;

        if ($category_id) {
            $pages = Page::select("pages.id", "pages.order", "pages.status", "pages.password", "pages.allowed_role_id", self::CREATED_AT, self::UPDATED_AT)
                ->with(["detail" => function ($query) {
                    $query->select("id", "language_id", "country_group_id", "page_id", "name", "detail", "slug")
                        ->when(auth()->user()->country_group, function($query){
                            $language = CountryGroupLanguage::where('country_group_id', auth()->user()->country_group)->where('default', 1)->first();
                            return $query->where('country_group_id', auth()->user()->country_group)->where('language_id', $language->language_id)->where('name', '!=', "");
                        });
                }])
                ->whereHas('categories', function ($q) use ($category_id) {
                    $q->where('id', $category_id);
                })
                ->when(auth()->user()->country_group, function($query){
                    $language = CountryGroupLanguage::where('country_group_id', auth()->user()->country_group)->where('default', 1)->first();
                    return $query->whereHas('detail', function($q)use($language){
                        return $q->where('country_group_id', auth()->user()->country_group)
                            ->where('language_id', $language->language_id)
                            ->where('name', '!=', "");
                    });

                })
                ->where('pages.status', '!=', 3)
                ->where(self::SITEMAP_ID, $sitemap->id)
                ->distinct('pages.id');
        } else {
            $pages = Page::select("pages.id", "pages.order", "pages.status", "pages.password", "pages.allowed_role_id", self::CREATED_AT, self::UPDATED_AT)
                ->with(["detail" => function ($query) {
                    $query->select("id", "language_id", "country_group_id", "page_id", "name", "detail", "slug")
                        ->when(auth()->user()->country_group, function($query){
                            $language = CountryGroupLanguage::where('country_group_id', auth()->user()->country_group)->where('default', 1)->first();
                            return $query->where('country_group_id', auth()->user()->country_group)->where('language_id', $language->language_id)->where('name', '!=', "");
                        });
                }])
                ->when(auth()->user()->country_group, function($query){
                    $language = CountryGroupLanguage::where('country_group_id', auth()->user()->country_group)->where('default', 1)->first();
                    return $query->whereHas('detail', function($q)use($language){
                        return $q->where('country_group_id', auth()->user()->country_group)
                            ->where('language_id', $language->language_id)
                            ->where('name', '!=', "");
                    });

                })
                ->where('pages.status', '!=', 3)
                ->where(self::SITEMAP_ID, $sitemap->id)
                ->distinct('pages.id');
        }


        $page_id = $request->page_id;
        if ($page_id && is_numeric($page_id)) {
            $pages = $pages->where('page_id', $page_id);
        }

        $searchKey = $request->get('search')['value'] ?? "";
        if($searchKey) {
            request()->merge(['search' => [
                'value' => strto('lower', $searchKey),
                'regex' => "false",
            ]]);
        }

        session(['pageLength.sitemaps.' . $request->segments()[count($request->segments()) - 1] => $request->get('length')]);

        return Datatables::eloquent($pages)
            ->editColumn('status', function ($page) {
                $status_class = null;
                $statusses = [
                    0 => "passive",
                    1 => "active",
                    2 => "draft",
                    3 => "predraft",
                    4 => "pending",
                    5 => "postdate",
                ];
                switch ($page->status) {
                    case 0:
                        //status PASSIVE
                        $status_class = "danger";
                        break;
                    case 1:
                        //status ACTIVE
                        $status_class = "active";
                        break;
                    case 2:
                        //status DRAFT
                        $status_class = "passive";
                        break;
                    case 3:
                        //status PREDRAFT
                        $status_class = "passive";
                        break;
                    case 4:
                        //status PENDING
                        $status_class = "pending";
                        break;
                    case 5:
                        //status POSTDATE
                        $status_class = "pending";
                        break;
                }

                $status_circle = $status_class ? '<i class="status_circle mr-1 ' . $status_class . '" title="' . (isset($statusses[$page->status]) ? trans("ContentPanel::general.status_title." . ($statusses[$page->status])) : "") . '"></i>' : "";
                $protected_content = $page->password || $page->allowed_role_id ? '<i class="fa fa-lock" title="' . trans("ContentPanel::general.protected_content") . '"></i>' : "";
                $postdate_publish = dateFilled($page, "published_at") ? '<i class="fa fa-calendar" title="' . trans("ContentPanel::general.postdate_publish") . " (" . $page->published_at . ")" . '"></i>' : "";


                return $status_circle . $protected_content . $postdate_publish;
            })
            ->editColumn('detail.name', function ($page) use($sitemap) {
                $route_params = [SITEMAP_ID => $sitemap->id, 'id' => $page->id];
                if (request()->has('page_id') && request()->page_id) {
                    $route_params["page_id"] = request()->page_id;
                }
                return '<a href="' . route('Content.pages.edit', $route_params) . '"><span class="listMiniText">' . Str::title($page->detail ? $page->detail->name :" << NO DETAIL OBJ. FOUND >> ") . '</span></a>';
            })
            ->editColumn('detail.detail', function ($page) {
                $detail = $page->detail->detail ?? "";
                $detail = strip_tags($detail);
                return mb_strlen($detail) > 70 ? "<span class='listMiniText'>" . Str::title(mb_substr($detail, 0, 70)) . "...</span>" : Str::title($detail);
            })
            ->addColumn('action', function ($page) use ($sitemap) {

                $route_params = [SITEMAP_ID => $sitemap->id, 'id' => $page->id];
                if (request()->has('page_id') && request()->page_id) {
                    $route_params["page_id"] = request()->page_id;
                }

                $children = $sitemap->children()->with(['detail' => function ($q) {
                    $q->select('sitemap_id', 'name');
                }])->get(['id'])->pluck('detail.name', 'id')->toArray();

                $actions = '<a href="' . route('Content.pages.edit', $route_params) . '" title="' . trans("MPCorePanel::general.edit") . '"><span class="fa fa-pen"></span></a>';
                foreach ($children as $sitemap_id => $child) {
                    $actions .= '<a href="' . route('Content.pages.index', ['sitemap_id' => $sitemap_id, 'page_id' => $page->id]) . '" title="' . $child . '">' . $child . '</a>';
                }
                return $actions .= '<a href="' . route('Content.pages.delete', $route_params) . '" class="needs-confirmation" title="' . trans("MPCorePanel::general.delete") . '" data-dialog-text="' . trans("MPCorePanel::general.action_cannot_undone") . '" data-dialog-cancellable="1" data-dialog-type="warning" ><span class="fa fa-trash"></span></a>';
            })->rawColumns(["status", "detail.name", "detail.detail", "action"])
            ->setRowId('tbl-{{$id}}')->make(true);

    }
}
