<?php

namespace Mediapress\Foundation;


use Kris\LaravelFormBuilder\Form;
use Mediapress\Http\Controllers\Panel\Controller;
use Mediapress\Models\Language;
use Mediapress\Models\LanguageWebsite;

class TabWriter
{
    public const CLASS_STR = "class";

    public static function create($forms, $language = null, $extraLanguageId = null)
    {
        if($extraLanguageId){
            $extraLanguageId = ' ';
        }
        if (! $forms)
        {
            return null;
        }
        foreach ($forms as $tabKey => $tab)
        {
            $attr = null;
            $divAttr = null;
            if ($language && is_array($tab) && isset($tab[$language]))
            {
                $tab = $tab[$language];

                $attr = \Arr::get($tab->getFormOptions(), "attr");

                $colors = Language::where("code", $language)->select("hexColor", "fontColor")->first();

                $divAttr = [
                    self::CLASS_STR => "dv-lng " . $language . $extraLanguageId . " " . \Arr::get($attr, self::CLASS_STR), "color" => $colors->hexColor, "font" => $colors->fontColor
                ];
            }

            if (is_null($attr))
            {
                $attr = \Arr::get($tab->getFormOptions(), "attr");
            }

            if (! isset($divAttr))
            {
                $divAttr = [
                    self::CLASS_STR => "dv-lng default" . $extraLanguageId . " " . \Arr::get($attr, self::CLASS_STR), "color" => null, "font" => null
                ];
            }

            if ($attr)
            {
                foreach (\Arr::except($attr, self::CLASS_STR) as $key => $item)
                {
                    $divAttr = \Arr::add($divAttr, $key, $item);
                }
            }

            $fields = $tab->getFields();

            self::functions($fields,$language);
            $field_name = end($fields)->getName();

            $field_name = str_replace("[]", null, $field_name);

            $result = $tab->renderUntil($field_name, false);

            if ($result){
                self::tabHtmlWriter($tab->getFormOptions()["tabName"], $tabKey, $result, $divAttr);
            }
        }
    }

    private static function tabHtmlWriter($tabName, $tabKey, $result, $divAttr = null)
    {
        $divAttr[self::CLASS_STR] = "box box-bordered box-color " . $divAttr[self::CLASS_STR] . " " . $tabKey;
        if(strstr($divAttr[self::CLASS_STR],'dv-lng') && !strstr($divAttr[self::CLASS_STR],'default')){
            $translate = explode(' ',explode('dv-lng ',$divAttr[self::CLASS_STR])[1]);
            if(isset($translate[2])){
                $translate[1] = $translate[2];
            }

        }
        echo " <div " . \Html::attributes($divAttr) . " style='padding:0;'>
                <div class=\"box-title\">
                  <h3 class=\"widget-title\">" . $tabName . "</h3>
                    <div class=\"actions\">
                        <a href=\"#\" class=\"btn btn-mini content-slideUp\">
                            <i class=\"fa fa-angle-down\"></i>
                        </a>
                    </div>
                </div>
                <div class=\"box-content\">" . $result . "</div></div>";
    }

    public static function languageBtnCreator($languages = null, $default = true, $extraLanguageId = null)
    {
        echo "<div class=\"box box-bordered box-color\">
                <div class=\"row\">
                    <div class=\"col-lg-12\">";

        $defaultLangCode = Language::getDefault()->code;

        if (is_null($languages))
        {
            $languages = app(Controller::class)->getWebsiteSession()->languages->pluck("code", "id");
        }

        foreach ($languages as $language)
        {
            $flag = Language::where("code", $language)->select("flag")->first()->flag;

            echo "<button type=\"button\" data-target=\"" . $language . $extraLanguageId . "\" class=\"btn btn-sm btn-language btn-language-frn ".($language == $defaultLangCode ? "defaultLng":"")."\">" . ($flag ? \Html::image(url("vendor/mediapress/img/flags/" . $flag), strtoupper($language)) : null) . "</button><i style='margin-left: 4px;' data-disableLang='".$language . $extraLanguageId."' class='fa fa-times text-danger'></i>\n";
        }

        if ($default){
            echo "<button type=\"button\" data-target=\"default" . $extraLanguageId . "\" class=\"btn btn-sm btn-language\">" . trans("panel::general.general") . "</button>\n";
        }

        echo "</div></div></div>";
    }

    private static function functions($fields,$language){

        if($language == 'tr' ||   $language == 'default'){
            foreach ($fields as $fieldlist){
                $options = $fieldlist->getOptions();
                if(isset($options['functions'])) {
                    echo '<script>'.$options['functions'].'</script>';
                }
            }
        }

    }
}
