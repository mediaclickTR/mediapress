<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Models\Slot;
use Mediapress\Models\Module;
use Mediapress\Modules\MPCore\Models\ModuleConfig;

class ModuleController extends Controller
{

    public const ACCESSDENIED = 'accessdenied';
    public const TOKEN = '_token';
    public const MP_CORE_TECHNICAL_MODULES_INDEX = 'MPCore.technical.modules.index';
    public const MESSAGE = "message";
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = "MPCorePanel::general.success_message";

    public function index()
    {
        if(!userAction('message.modules.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $modules = Module::paginate(25);
        return view('MPCorePanel::technical.modules.index', compact('modules'));
    }

    public function create()
    {
        if(!userAction('message.modules.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $slots = Slot::get()->pluck("slot","id");
        return view('MPCorePanel::technical.modules.create', compact('slots'));
    }

    public function edit($id)
    {
        $module = Module::find($id);
        $slots = Slot::get()->pluck("slot","id");
        return view('MPCorePanel::technical.modules.edit', compact('module', 'slots'));
    }

    public function store(Request $request)
    {
        if(!userAction('message.modules.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $data = request()->except([self::TOKEN]);
        Module::insert($data);

        return redirect(route(self::MP_CORE_TECHNICAL_MODULES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {
        $data = request()->except([self::TOKEN]);
        Module::where('id',$request->id)->update($data);
        return redirect(route(self::MP_CORE_TECHNICAL_MODULES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function manage($id)
    {
        $module_id = $id;
        $module_configs = ModuleConfig::where("module_id",$id)->get();
        return view("MPCorePanel::technical.modules.manage", compact("module_configs",'module_id'));
    }

    public function manageStore(Request $request)
    {
        $data = request()->except([self::TOKEN]);
        foreach ($data as $key => $value)
        {
            // Böyle bir kolon varsa valueyu güncelle
            $isconfig = ModuleConfig::where('key',$key)->first();
            if($isconfig){
                ModuleConfig::where('key', $key)->update(['value' => $value]);
            }
        }
         return redirect(route(self::MP_CORE_TECHNICAL_MODULES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(!userAction('message.modules.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $menu = Module::find($id);
        if ($menu){
            $menu->delete();
        }

        return redirect(route(self::MP_CORE_TECHNICAL_MODULES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    /*
    public function slotStore(Request $request)
    {
        $data = request()->except("_token");
        $save = Slot::insert($data);
        if($save){
            return redirect()->back()->with("message", trans("MPCorePanel::general.success_message"));
        }else{
            return redirect()->back()->with("error", trans("MPCorePanel::general.error_message"));
        }
    }
    */

}