<?php

namespace Mediapress\Modules\Interaction\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Cache;

class HomeController extends Controller
{

    public function index()
    {
        $data=[];

        if(settingSunFindValue('mpcore.analytics.yandex.cache') && settingSunFindValue('mpcore.analytics.yandex.counter_id') && settingSunFindValue('mpcore.analytics.yandex.token')){
            $data['yandex'] = true;
        }

        return view("MPCorePanel::analytics.home", compact('data'));
    }
}
