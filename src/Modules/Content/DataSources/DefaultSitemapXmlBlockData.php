<?php
namespace Mediapress\Modules\Content\DataSources;

use Mediapress\Foundation\DataSource;

class DefaultSitemapXmlBlockData extends DataSource
{
    public function getData()
    {
        $sitemap_xml_type = $this->params["sitemap_xml_type"] ?? "custom";
        $detail_priority = (isset($this->params["sitemap_feature"]) && $this->params["sitemap_feature"]=="homepage") ? 1.0 : 0.8;
        if($sitemap_xml_type == "default"){
            return [
                "include_detail"=>true,
                "detail_priority"=>$detail_priority,
                "include_categories"=>true,
                "categories_priority"=>0.64,
                "include_pages"=>true,
                "pages_priority"=>0.51,
                'detail_change_frequency' => 'daily',
                'categories_change_frequency' => 'daily',
                'pages_change_frequency' => 'weekly',
            ];
        }

        return [
            "include_detail"=>true,
            "detail_priority"=>$detail_priority,
            "include_categories"=>true,
            "categories_priority"=>0.64,
            "include_pages"=>true,
            "pages_priority"=>0.51,
            'detail_change_frequency' => 'daily',
            'categories_change_frequency' => 'daily',
            'pages_change_frequency' => 'weekly',
        ];
    }
}
