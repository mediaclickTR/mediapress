<?php

return [
    'modules' => ' Modules',
    'create' => 'Add Module',
    'edit' => 'Edit Module',
    'name' => 'Module Name',
    'namespace' => 'Module Namespace',
    'status' => 'Release Status',
    'slot ' => ' Slot Selection',
    'slot.add ' => ' Quick Slot Add',
    'slot.name ' => ' Slot Name',
    'slot.description' => 'Slot Description',
];
