<?php

    namespace Mediapress\Modules\Content\Models;

    use Mediapress\Modules\MPCore\Models\BaseModel;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class SitemapXmlBlock extends BaseModel
    {

        protected $table = 'sitemap_xml_blocks';

        public $timestamps = false;

        protected $fillable = [
            "sitemap_xml_id",
            "sitemap_id",
            "include_detail",
            "detail_priority",
            "detail_change_frequency",
            "include_categories",
            "categories_priority",
            "categories_change_frequency",
            "include_pages",
            "pages_priority",
            "pages_change_frequency",
        ];

        public function sitemap_xml(){
            return $this->belongsTo(SitemapXml::class);
        }

        public function sitemap(){
            return $this->belongsTo(Sitemap::class);
        }

    }
