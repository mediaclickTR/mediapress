<div id="mcBottomTooltip{{$popup->id}}" data-tooltip-fullbg="{{ $popup->full_bg }}" data-tooltip-place="{{ $popup->place ?: 'right' }}" style="display: none">
    <div id="allBottomTooltip{{$popup->id}}"
         class="{{ $popup->content_only_image == 1 ? 'imageTooltipBox ' . ($popup->place ?: 'right') : '' }}"
         data-tooltip-bg="{{ $popup->background }}"
         data-tooltip-color="{{ $popup->color }}"
         data-tooltip-border-radius="{{ $popup->border_radius }}">

        <button type="button" id="mcBottomTooltipClose{{$popup->id}}"
                class="mcPopupClose"
                data-dismiss="modal"
                aria-label="Close"
                data-tooltip-button-bg="{{ $popup->button_bg }}"
                data-tooltip-button-color="{{ $popup->button_color }}">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mctoolTipText{{$popup->id}}" data-tooltip-a-color="{{ $popup->a_color }}">
            <div class="cont d-flex align-items-center">
                <div class="text" style="{{ $popup->font ? "font-family: '$popup->font'" : '' }}">
                    @if($popup->name_display == 1)
                        <h5 class="mcBottomTooltipTitle" data-tooltip-title-size="{{ $popup->title_size }}" >{{$popup->name}}</h5>
                    @endif
                    {!! $popup->content !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#mcBottomTooltip{{$popup->id}}').each(function(){
        var $bottomFullBg = $(this).attr("data-tooltip-fullbg");
        var $place = $(this).attr('data-tooltip-place');
        $(this).addClass($bottomFullBg);
        $(this).addClass($place);
    });

    $('#allBottomTooltip{{$popup->id}}').each(function(){
        var $bottomBg = $(this).attr("data-tooltip-bg");
        var $bottomColor = $(this).attr("data-tooltip-color");
        var $bottomFullBg = $(this).attr("data-tooltip-fullbg");
        var $bottombuttonBg = $(this).find('.mcPopupClose').attr("data-tooltip-button-bg");
        var $bottombuttonColor = $(this).find('.mcPopupClose').attr("data-tooltip-button-color");
        var $bottomTitleSize = $(this).find('.mcBottomTooltipTitle').attr("data-tooltip-title-size");
        var $aTagColor = $(this).find('#mctoolTipText{{$popup->id}}').attr("data-tooltip-a-color");
        var $borderRadius = $(this).attr("data-tooltip-border-radius");

        $(this).addClass($bottomFullBg);
        $(this).css({
            'backgroundColor': '' + $bottomBg +'',
            'border-radius': $borderRadius + 'px'
        });
        $(this).find('#mctoolTipText{{$popup->id}}').css({
            'color': '' + $bottomColor +''
        });
        $(this).find('.mcPopupClose').css({
            'backgroundColor': '' + $bottombuttonBg +'',
            'color': '' + $bottombuttonColor +''
        });
        $(this).find('.mcBottomTooltipTitle').css({
            'font-size': '' + $bottomTitleSize +'px'
        });
        $(this).find('#mctoolTipText{{$popup->id}} a').css(
            'cssText', "color: " + $aTagColor + " !important"
        );
    });

    $('#mcBottomTooltipClose{{$popup->id}}').click(function () {
        $('#mcBottomTooltip{{$popup->id}}').stop().fadeOut(250);
    });

    @if($popup->conditions == 1)
    $(window).scroll(function () {
        if ($(document).scrollTop() > {{ $popup->condition_pixel }}) {
            $('#mcBottomTooltip{{$popup->id}}').fadeIn(250);
        }
    });
    @else
    @if($popup->condition_time > 0)
    setTimeout(function () {
        $('#mcBottomTooltip{{$popup->id}}').fadeIn(250);
    }, {{$popup->condition_time * 1000}});
    @else
    setTimeout(function () {
        $('#mcBottomTooltip{{$popup->id}}').fadeIn(250);
    }, 100);
    @endif
    @endif

    @if($popup->display_time_type == 1)
    setTimeout(function () {
        $('#mcBottomTooltip{{$popup->id}}').stop().fadeOut(250);
    }, {{ ($popup->display_time + $popup->condition_time) * 1000}});
    @endif
</script>
