<?php

namespace Mediapress\Modules\Entity\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mediapress\Modules\Entity\Models\User;
use Mediapress\Modules\Entity\Models\Entities;
use Mediapress\Modules\Entity\Models\EntitylistPivot;
use Mediapress\Modules\Entity\Models\EntityList;

class EntitylistDetailController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const USERS = 'users';
    public const LIST_ID = 'list_id';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index($list_id)
    {
        if(!userAction('message.forms.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $users = EntitylistPivot::where("entitylist_id",$list_id)->paginate(25);
        $entitylist = Entitylist::find($list_id);

        return view('EntityPanel::entitylist.listdetail.index', compact(self::USERS,'entitylist', self::LIST_ID));
    }

    public function create($list_id)
    {
        if(!userAction('message.entitylistdetail.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $users = User::all();
        $entities = Entities::all();
        return view("EntityPanel::entitylist.listdetail.create",compact(self::USERS,'entities', self::LIST_ID));
    }

    public function edit($list_id,$id)
    {
        $user = EntitylistPivot::find($id);
        $modulusers = $user->model_type::get();
        return view('EntityPanel::entitylist.listdetail.edit', compact("modulusers","user", self::LIST_ID));
    }

    public function store(Request $request,$list_id)
    {
        if(!userAction('message.entitylistdetail.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        /*
          * Validation
          */

        $fields = [
            self::USERS => trans("MPCorePanel::general.users"),
        ];

        $rules = [
            self::USERS => 'required|max:255',
        ];

        $messages = [
            'users.required' => trans("MPCorePanel::validation.filled", ['filled',trans("MPCorePanel::general.users")]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        foreach ($request->users as $user)
        {
            // Gelen değerdeki id'yi string'den kurtar
            $userid = substr_replace($user,'', -5);

            if(strstr($user,"user")) { // user model
                $user = User::find($userid);
            }
            elseif(strstr($user,"enti")){ //entity model
                $user = Entities::find($userid);
            }

            $list = new EntitylistPivot;
            $save = $user->model()->save($list);
            // Log
            if ($save) {
                EntitylistPivot::where("id",$save->id)->update(["entitylist_id"=>$list_id]); // kayda ait list_id'yi güncelle
                UserActionLog::create(__CLASS__."@".__FUNCTION__,$save);
            }

        }
        return redirect()->route('Entity.entitylist.listdetail.index',$list_id)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request,$list_id)
    {
        $update=EntitylistPivot::updateOrCreate(['id'=>$request->id],['model_id'=>$request->userable_id]);
        if ($update) {
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route('Entity.entitylist.listdetail.index',$list_id))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function detail($list_id,$id)
    {
        $user = EntitylistPivot::findOrFail($id);
        $moduluser = $user->model_type::find($user->model_id);
        return view('EntityPanel::entitylist.listdetail.detail', compact("moduluser","user", self::LIST_ID));
    }

    public function delete($list_id,$id)
    {
        if(!userAction('message.entitylistdetail.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $data=EntitylistPivot::find($id);
        if($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function selectedRemove(Request $request)
    {
        if($request->checked) {
            foreach ($request->checked as $check){
                EntitylistPivot::destroy($check);
            }
        }
        else{
            return redirect()->back()->withErrors([trans('MPCorePanel::general.danger_message')]);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}