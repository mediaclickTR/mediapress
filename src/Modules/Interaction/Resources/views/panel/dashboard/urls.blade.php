<div class="bd-example pb-5">
    <div class="row">
        <div class="col">
            <div class="card p-4">
                <div class="card-title color">{{ __("InteractionPanel::dashboard.most_visited") }}</div>
                <div class="card-body p-0">
                    <ul class="list-group">
                        @foreach($data['urls'] as $url)
                            <li class="d-flex justify-content-between align-items-center">
                                <a href="{{ url($url['url']) }}" target="_blank">{{ $url['name'] ?? "" }}</a>
                                <span class="badge badge-pill">{{ $url['count'] }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        @if($repoName == 'GoogleRepository')
            <div class="col">
                <div class="card p-4">
                    <div class="card-title color">{{ __("InteractionPanel::dashboard.affinities") }}</div>
                    <div class="card-body p-0">
                        <ul class="list-group">
                            @foreach($data['affinities'] as $affinity)
                                <li class="d-flex">
                                    <div class="col pl-0">
                                        {!! $affinity['key'] !!}
                                    </div>
                                    <div class="col pr-0">
                                        <div class="row">
                                            <div class="col-9 p-0">
                                                <div class="progress-bar" role="progressbar" style="width:  {!! $affinity['percent']*5 !!}%"
                                                     aria-valuenow="10" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                            <div class="col-3">
                                                <span class="badge badge-pill p-0"> {!! $affinity['value'] !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
