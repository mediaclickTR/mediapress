<?php

    namespace Mediapress\Foundation;

    use ArrayAccess;
    use Exception;



    class HtmlElement implements  ArrayAccess
    {

        private $void_elements_list = array("area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr");
        private $nowhitespace_elements_list = array("pre", "code", "textarea");
        private $classes = array();
        private $self_closing = FALSE;
        private $attrvaluepairs = array();
        private $id_ = "";
        private $_contents = array();
        private $tag = "";

        public const CLASSNAME =  "class";
        // region HELPERS

        public function html_encrypt($str)
        {
            return htmlentities($str, ENT_QUOTES | ENT_IGNORE, "UTF-8");
        }

        public function html_decrypt($str)
        {
            return html_entity_decode($str, ENT_QUOTES | ENT_IGNORE, "UTF-8");
        }
        //endregion

        /////////////////////// BEGIN /////////////////////////
        ///
        public function __construct($tag = NULL, $self_closing = NULL)
        {
            $this->tag_name($tag, $self_closing);
            return $this;
        }

        public function children($unique_key = NULL)
        {
            if (!is_null($unique_key)) {
                return isset($this->_contents[$unique_key]) ? $this->_contents[$unique_key] : NULL;
            }

            return $this->_contents;

        }

        public function contents(){
            return  $this->_contents;
        }

        public function tag_name($tag_name = NULL, $self_closing = NULL)
        {
            if (is_null($tag_name)) {
                return $this;
            } else {
                if (!is_string($tag_name)) {
                    throw new \Exception ("Expected a string expression for tag name, " . gettype($tag_name) . " given.", NULL, NULL);
                }
                $this->tag = $tag_name;
                if (!is_null($self_closing)) {
                    $this->self_closing = $self_closing;
                } else {
                    if (array_search($tag_name, $this->void_elements_list)) {
                        $this->self_closing = TRUE;
                    }
                }
                return $this;
            }
        }

        public function has_class($class_name)
        {

            if (!is_string($class_name)) {
                return FALSE;
            }
            $value = isset($this->attrvaluepairs[self::CLASSNAME]) ? $this->attrvaluepairs[self::CLASSNAME] : "";
            if ($value && (array_search($class_name, explode(" ", $value)) !== FALSE)) {
                    return $class_name;
            }
            return FALSE;

        }

        public function add_class($classes)
        {

            if (!is_string($classes) && !is_array($classes)) {
                return FALSE;
            }
            if (is_string($classes)){
                $classes = [$classes];
            }


            $value = isset($this->attrvaluepairs[self::CLASSNAME]) ? $this->attrvaluepairs[self::CLASSNAME] : FALSE;
            $explode = explode(" ", $value);

            $merged = array_unique(array_merge($explode, $classes));
            $this->attrvaluepairs[self::CLASSNAME] = trim(implode(" ", $merged));

            return $this;

        }

        public function data($data_name, $value = NULL)
        {

            if (!is_string($data_name)) {
                return NULL;
            }
            if (func_num_args() == 2) {
                $this->attrvaluepairs["data-$data_name"] = $value;
                return $this;
            }

            return isset($this->attrvaluepairs["data-$data_name"]) ? $this->attrvaluepairs["data-$data_name"] : NULL;

        }

        public function remove_class($class_name)
        {

            $value = isset($this->attrvaluepairs[self::CLASSNAME]) ? $this->attrvaluepairs[self::CLASSNAME] : FALSE;
            if ($value) {
                $explode = explode(" ", $value);
                $ind = array_search($class_name, $explode);
                if ($ind !== FALSE) {
                    unset($explode[$ind]);
                    $implode = implode(" ", $explode);
                    if ($implode) {
                        $this->attrvaluepairs[self::CLASSNAME] = $implode;
                    }
                }
            }
            return $this;

        }

        public function clear_classes()
        {

            if (isset($this->attrvaluepairs[self::CLASSNAME])) {
                $this->attrvaluepairs[self::CLASSNAME] = "";
            }

            return $this;

        }

        //////////////////// END ///////////////////////////////////

        //////////////////// ATTR ////////////////////////////////////////
        public function get_attr($attr)
        {

            if ($attr == "id") {
                return $this->get_id();
            }
            return isset($this->attrvaluepairs[$attr]) ? $this->attrvaluepairs[$attr] : FALSE;

        }

        public function has_attr($attr)
        {

            return isset($this->attrvaluepairs[$attr]) ? array($attr => $this->attrvaluepairs[$attr]) : FALSE;

        }


        public function set_attr($attr, $value = NULL){
            return $this->add_attr($attr,$value);
        }

        public function add_attr($attr, $value = NULL)
        {

            if ($attr == "id") {
                return $this->set_id($value);
            }

            if (is_string($value) || is_numeric($value) || is_null($value)) {
                $this->attrvaluepairs[$attr] = $value;
            }

            return $this;

        }

        public function remove_attr($attr)
        {

            if (isset($this->attrvaluepairs[$attr])) {
                unset($this->attrvaluepairs[$attr]);
            }

            return $this;

        }

        public function clear_attributes()
        {

            $this->attrvaluepairs = array();

            return $this;

        }

        //////////////////// END ATTR ////////////////////////////////////

        public function set_id($id = NULL)
        {

            if (is_string($id) || is_numeric($id) || is_null($id)) {
                $this->id_ = $id;
            }

            return $this;

        }

        public function get_id()
        {

            return $this->id_;

        }

        public function get_tag()
        {

            return $this->tag;

        }

        /////////////////////////////////////////////////////////////////
        public function add_content($content)
        {
            if (!$content) {
                return $this;
            }
            if (!$this->self_closing) {
                if (!is_array($content) && !is_string($content) && !is_a($content, "Mediapress\\Foundation\\HtmlElement") && !is_numeric($content)) {
                    //throw new Exception("Please submit a string, array or HTMLElement", 1002, NULL)
                    return $this;
                }
                if (is_array($content)) {
                    foreach ($content as $c) {
                        $this->add_content($c);
                    }
                } elseif (is_numeric($content)) {
                    $this->_contents[] = $content . "";
                } else {
                    $this->_contents[] = $content;
                }
            }
            // else // throw new Exception("Self-closing/non-void elements cannot have content", 1003, NULL)
            return $this;
        }



        public function remove_content($ind)
        {

            if (isset($this->_contents[$ind])) {
                unset($this->_contents[$ind]);
            }

            return $this;

        }

        public function clear_content()
        {

            $this->_contents = array();

            return $this;

        }

        /////////////////////////////////////////////////////////////////
        public function dump($indent = 0)
        {

            echo $this->give_html($indent);

        }

        /////////////////////////////////////////////////////////////////////////////
        public function give_html($indent = 0)
        {
            $indent_str = "";
            if ($this->tag && $indent) {
                $indent_str = str_pad($indent_str, $indent, "\t", STR_PAD_LEFT);
            }
            if (!$this->tag) {
                return "";
            }
            $id_str = $this->id_ ? "id=\"$this->id_\"" : "";
            $attrs = array();
            foreach ($this->attrvaluepairs as $key => $value) {
                if (!is_null($value)) {
                    //$attrs[]="$key=\"". $this->html_encrypt($value)."\""
                    $attrs[] = "$key=\"" . str_replace('"','&quot;',$value) . "\"";
                } else {
                    $attrs[] = $key;
                }
            }

            $attr_str = implode(" ", $attrs);

            if ($this->self_closing == TRUE) {
                $str = ($this->tag ? "$indent_str<" . implode(" ", array_filter([$this->tag, $id_str, $attr_str])) . "/>" : "");
            } else {
                $content_str = "";
                foreach ($this->_contents as $content) {

                    $cstr_to_add = "";

                    if (is_string($content)) {
                        $cstr_to_add = "$indent_str\t" . $content;
                    } else if (is_a($content, "Mediapress\\Foundation\\HtmlElement")) {
                        $cstr_to_add = $content->give_html($indent + ($this->tag ? 1 : 0));
                    }
                    if ($cstr_to_add) {
                        $content_str .= $cstr_to_add . "\n";
                    }
                }
                //if($this->tag === ":null")
                if (in_array($this->tag, $this->nowhitespace_elements_list)) {
                    $str = ($this->tag ? "$indent_str<" . implode(" ", array_filter([$this->tag, $id_str, $attr_str])) . ">" : "") . ($content_str ? trim($content_str) : "") . ($this->tag ? "</$this->tag>" : "");
                } else {
                    $str = ($this->tag ? "$indent_str<" . implode(" ", array_filter([$this->tag, $id_str, $attr_str])) . ">\n" : "") . ($content_str ? $content_str : "") . ($this->tag ? $indent_str . "</$this->tag>" : "");
                }
                //str_pad($str, $indent*4, "&nbsp;", STR_PAD_LEFT)
            }

            return $str;

        }

        public function offsetExists($offset)
        {
            if(isset($this->{$offset})){
                return true;
            }
            return false;
        }

        public function offsetGet($offset)
        {
            if(isset($this->{$offset})){
                return $this->{$offset};
            }
            return null;
        }

        public function offsetSet($offset, $value)
        {
            if(isset($this->{$offset})){
                $this->{$offset} = $value;
            }
        }

        public function offsetUnset($offset)
        {
            if(isset($this->{$offset})){
                unset(  $this->{$offset} );
            }
        }

    }
