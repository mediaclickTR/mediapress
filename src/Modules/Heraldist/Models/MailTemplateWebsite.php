<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class MailTemplateWebsite extends Model
{
    protected $table = 'mail_templates';

    public $timestamps = false;

    protected $fillable = [
        "mail_template_id",
        "website_id",
    ];

    public function websites()
    {
        return $this->belongsToMany(Website::class)->withPivot('mail_template_id');
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
}
