<?php

namespace Mediapress\Modules\MPCore\Facades;

use Illuminate\Support\Facades\Facade;

class MPCore extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'MPCore';
    }

}