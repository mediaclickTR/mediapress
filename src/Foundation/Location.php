<?php

namespace Mediapress\Foundation;

use Mediapress\Modules\Locale\Models\Province;
use Mediapress\Modules\MPCore\Models\Country;

class Location
{
    /** @var Country */
    public $country = null;
    public $cities = [];

    public function __construct($country = null)
    {
        if ($country) {
            $this->country = Country::where('code', $country)->first();
            if($country == 'TR'){
                $cityList = $this->country->provinces()->where('id','<','82')->get(['id', 'name'])->sortBy(function($q){
                    return iconv('UTF-8', 'ASCII//TRANSLIT', $q->name);
                });
            }else{
                $cityList = $this->country->provinces()->orderBy('name')->get(['id', 'name']);
            }
        } else {
            $cityList = Province::all();
        }
        $this->getDistricts($cityList);
    }

    public function cities($array = [])
    {
        return $this->cities;
    }

    private function getDistricts($cityList)
    {
        foreach ($cityList as $key => $value) {
           $city = [
                'id' => $value['id'],
                'name' => $value['name'],
                'districts' =>  []
            ];
            $districts = $value->counties()->get(['id', 'name'])->sortBy(function($q){
                return iconv('UTF-8', 'ASCII//TRANSLIT', $q->name);
            })->toArray();
            foreach ($districts as $district){
                $city['districts']['_'.$district['id']] = $district;
            }
            $this->cities['_' . $value['id']] = $city;
        }
    }
}
