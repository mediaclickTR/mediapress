<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Mediapress\Modules\Content\Models\Website;

class WebsiteSeeder extends Seeder
{
    public const DEFAULT1 = "default";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('websites')->delete();
        $parsedUrl = parse_url(config('mediapress.app_url'));
        $ssl = isset($parsedUrl["schema"]) ? $parsedUrl["schema"] === "https" : false;
        $parsedUrl = isset($parsedUrl["host"]) ? $parsedUrl["host"] : $parsedUrl["path"];
        DB::table('websites')->insert([
            [
                "admin_id" => 0,
                "type" => "domain",
                "target" => "internal",
                "sort" => 1,
                "order" => 1,
                self::DEFAULT1 => 1,
                "name" => $parsedUrl,
                "slug" => $parsedUrl,
                "ssl" => $ssl,
                self::DEFAULT1 => 1,
                "use_deflng_code" => 1,
                "regions" => 0,
                "variation_template" => "{lng}",
                "status" => 1
            ],
        ]);

        DB::table('language_website')->delete();
        DB::table('language_website')->insert([
            ["language_id" => 760, "website_id" => 1, self::DEFAULT1 => 1],
        ]);

        DB::table('country_groups')->insert([
            ["code" => "gl", "title" => "Global", "list_title" => "Global", "owner_type" => Website::class, "owner_id" => 1],
        ]);
    }
}
