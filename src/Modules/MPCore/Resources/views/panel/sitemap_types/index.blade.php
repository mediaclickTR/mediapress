@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("MPCorePanel::sitemap_type.sitemap_types") !!}</h1>
                <div class="float-right">
                        <a class="btn btn-primary" href="{!! route('MPCore.sitemap_types.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
                <div class="clearfix"></div>
                <table>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    </tr>
                    @foreach($sitemap_types as $key=>$sitemap_type)
                        <tr>
                            <td>{!! $sitemap_type->id !!}</td>
                            <td>
                                <select onchange="locationOnChange(this.value);">
                                    <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                    <option value="{!! route('MPCore.sitemap_types.edit',$sitemap_type->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                    <option value="{!! route('MPCore.sitemap_types.delete',$sitemap_type->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </table>
    </div>
@endsection
