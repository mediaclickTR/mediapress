<?php

namespace Mediapress\Modules\Comment\Controllers;

use Dawnstar\Models\FormResult;
use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Modules\Comment\Models\Comment;
use Mediapress\Modules\Comment\Models\CommentStructure;

class CommentController extends Controller
{
    private $ip;
    private $userAgent;

    public function __construct()
    {
        $this->ip = $this->getClientIp();
        $this->userAgent = $this->getClientUserAgent();
    }

    public function index(int $commentStructureId)
    {
        if(!activeUserCan(["comment.index"])){return rejectResponse();}

        $commentStructure = CommentStructure::find($commentStructureId);
        $comments = $commentStructure->comments;

        return view('CommentView::comment.index', compact('commentStructure', 'comments'));
    }

    public function save(Request $request, int $commentStructureId)
    {
        $data = $request->except('_token', 'g-recaptcha-response');

        $commentStructure = CommentStructure::find($commentStructureId);

        $data['comment_structure_id'] = $commentStructure->id;
        $data['ip'] = $this->ip;
        $data['user_agent'] = $this->userAgent;

        if($commentStructure->condition == 2 && auth()->check()) {
            $user = auth()->user();
            if(!isset($data['fullname'])) {
                $data['fullname'] = $user->name;
            }
            if(!isset($data['email'])) {
                $data['email'] = $user->email;
            }
            $data['user_id'] = auth()->id();
        }

        $data['status'] = 1;
        if($commentStructure->show_time == 2) {
            $data['status'] = 2;
        }

        Comment::firstOrCreate($data);


        return redirect()->back()->with('comment_message', langPartAttr('comment.success_message'));
    }

    public function update(Request $request, int $commentStructureId, int $id)
    {
        $status = $request->get('status');

        $comment = Comment::find($id);

        if($comment) {
            $comment->update(['status' => $status]);
        }

        return redirect()->route('CommentStructure.comments.index', ['commentStructureId' => $commentStructureId]);
    }

    public function delete(int $commentStructureId, int $id)
    {

        $comment = Comment::find($id);

        if($comment) {
            $comment->delete();
        }
        return redirect()->route('CommentStructure.comments.index', ['commentStructureId' => $commentStructureId]);
    }

    public function updateReadStatus(Request $request, int $commentStructureId)
    {
        $id = $request->get('id');

        $comment = Comment::find($id);

        if($comment) {
            $comment->update(['read' => 1]);
        }
    }

    private function getClientIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private function getClientUserAgent() {
        return request()->userAgent();
    }
}
