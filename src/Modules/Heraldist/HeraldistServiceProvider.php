<?php

namespace Mediapress\Modules\Heraldist;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\Heraldist\Facades\Heraldist;

class HeraldistServiceProvider extends ServiceProvider
{

    public const HERALDIST = "Heraldist";
    public const RESOURCES = 'Resources';
    public const VIEWS = 'views';
    protected $module_name = self::HERALDIST;

    protected $namespace = 'Mediapress\Modules\Heraldist';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . self::VIEWS . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . self::VIEWS . DIRECTORY_SEPARATOR . 'web', $this->module_name.'Web');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . self::VIEWS . DIRECTORY_SEPARATOR . 'render', 'FormRender');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        //$this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name.'Database');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Publishes' => resource_path(self::VIEWS .DIRECTORY_SEPARATOR.'vendor')], $this->module_name.'Publishes'.DIRECTORY_SEPARATOR.'mail');
        //$this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Localization' => resource_path('lang')], $this->module_name.'Localization');
        $this->publishActions(__DIR__);
        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias(self::HERALDIST, Heraldist::class);
        app()->bind(self::HERALDIST, function() {
            return new \Mediapress\Modules\Heraldist\Heraldist;
        });
    }
    
    protected function mergeConfig($path, $key)
    {
        
        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }
                
            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }
}
