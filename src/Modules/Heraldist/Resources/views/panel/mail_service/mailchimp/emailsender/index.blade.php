@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
            <h1 class="title">{!! trans("HeraldistPanel::mail_service.emailsender") !!} ({!! $total !!})</h1>
            @if(count($emailsenders)<=0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                <div class="float-left">
                        <a href="{!! route('Heraldist.mailchimp.emailsender.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
            @else
                <div class="float-right">
                    <a href="{!! route('Heraldist.mailchimp.emailsender.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.id") !!}</th>
                        <th>{!! trans("MPCorePanel::general.name") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.maillist_name") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.template_id") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.sender_name") !!}</th>
                        <th>{!! trans("MPCorePanel::general.status") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.sended_emails") !!}</th>
                        <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.multiple_emailsend") !!}</th>
                        <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                    </tr>
                    </thead>
                    @foreach($emailsenders as $emailsender)
                        <tr>
                            <td>{!! $emailsender->id !!}</td>
                            <td>{!! $emailsender->settings->title !!}</td>
                            <td>{!! $emailsender->recipients->list_name !!}</td>
                            <td>{!! $emailsender->settings->template_id !!}</td>
                            <td>{!! $emailsender->settings->from_name !!}</td>
                            <td>{!! $emailsender->status == 'sent' ? '<span class="text-success"><i class="fa fa-check" style="display:inline !important"></i> Gönderildi</span>' : '<span class="text-info">Gönderilebilir!</span>' !!}</td>
                            <td style="text-align:center !important;">{!! $emailsender->emails_sent !!}</td>
                            <td>{!! substr($emailsender->create_time,0,-6) !!}</td>
                            <td>
                                @if($emailsender->status!="sent")
                                    <a href="{!! route('Heraldist.mailchimp.emailsender.send',$emailsender->id) !!}" class="text-success">
                                        <i class="fa fa-envelope" style="display:inline !important"></i> {!! trans("HeraldistPanel::mail_service.mailsend") !!}</a>
                                @else
                                    {!! trans("HeraldistPanel::mail_service.sended_email") !!}
                                @endif
                            </td>
                            <td>
                                <select onchange="locationOnChange(this.value);">
                                    <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                       @if($emailsender->status!="sent")
                                        <option value="{!! route('Heraldist.mailchimp.emailsender.edit',$emailsender->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                        <option value="{!! route('Heraldist.mailchimp.emailsender.send',$emailsender->id) !!}">{!! trans("HeraldistPanel::mail_service.multiple_emailsend") !!}</option>
                                       @endif
                                    <option value="{!! route('Heraldist.mailchimp.emailsender.delete',$emailsender->id) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </table>
        @endif
@endsection
