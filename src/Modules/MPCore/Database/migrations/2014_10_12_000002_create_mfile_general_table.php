<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMFileGeneralTable extends Migration
{
    public const MFILE_GENERAL = 'mfile_general';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable(self::MFILE_GENERAL)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::MFILE_GENERAL, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('mfile_id');
                $table->string('model_type',255);
                $table->integer('model_id');
                $table->string('file_key',255);
                $table->integer('ordernum');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::MFILE_GENERAL)) {
            Schema::dropIfExists(self::MFILE_GENERAL);
        }
    }
}
