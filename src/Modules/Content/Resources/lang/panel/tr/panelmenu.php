<?php

return [
    'panelmenus'=>'Panel Menüleri',
    'name'=>'Menü Adı',
    'create_menu'=>'Menü Ekle',
    'edit_menu'=>'Menü Düzenle',
    'sitemap'=>'Sayfa Yapısı',
    'empty_menu_detail'=>"<b>:menu</b> menüsüne ait detay bulunamadı. Sağ kısımdan ekleyebilirsiniz!",
    'info' => 'Yeni ekleyeceğiniz başlık, sol tarafta en alta eklenecektir.Bu başlığı istediğiniz yere sürükleyerek,kaydet butonuna basınız.',
    'select_sitemap' => 'Sayfa Yapısı Seçiniz',
];
