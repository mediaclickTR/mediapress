<?php

namespace Mediapress\Modules\Heraldist\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\Heraldist\Models\Message;

class FormSender extends Mailable
{
    use Queueable, SerializesModels;

    protected $ebulletin;
    protected $sender_email;
    protected $sender_name;
    public $subject;
    public $view;
    public $message;

    public function __construct(Message $message)
    {
        $this->form = $message->form;
        $this->message = $message;

        $this->subject = langPartAttr($this->form->name,$this->form->name);
        $this->sender_email = config('mediapress.form_mail.sender_email');
        $this->sender_name = config('mediapress.form_mail.sender_name');
        if (file_exists(resource_path("views/vendor/mail/form.blade.php"))) {
            $this->view = config('mediapress.form_mail.view_page');
        }else{
            $this->view = 'HeraldistWeb::forms.email';
        }
        // $this->message = env("FORM_MAIL_MESSAGE", langPart($form->success,$form->success));
    }

    public function build()
    {
        $data = [
            'subject'=>$this->subject,
            'sender_email'=>$this->sender_email,
            'sender_name'=>$this->sender_name,
            'message'=>$this->message,
            'view'=>$this->view,
        ];
        $rendered = $this->form->email($this,$data);
    }
}
