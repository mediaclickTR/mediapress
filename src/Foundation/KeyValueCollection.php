<?php

namespace Mediapress\Foundation;

use Illuminate\Database\Eloquent\Collection;

class KeyValueCollection extends Collection
{
    
    protected $getResultsObject;
    
    public function __construct($items = [], $object = null)
    {
        $this->getResultsObject = $object;
        parent::__construct($items);
    }
    
    /**
     * get stored Key Value pair inside of eloquent model
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $model = $this->where('key', $name)->first();
        return $model ? $model->value : null;
    }
    
    /**
     * set stored Key Value pair inside of eloquent model
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (!is_null($this->getResultsObject)) {
            $model = $this->getResultsObject->firstOrNew(["key" => $name]);
            if (is_array($value)) {
                foreach ($value as $vk => $vv) {
                    $model->$vk = $vv;
                }
            } else {
                $model->value = $value;
            }
            $model->save();
        }
    }
    
}