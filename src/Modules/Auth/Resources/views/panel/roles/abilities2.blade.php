@extends("AuthPanel::inc.module_main")
@push("styles")
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/selector.css") !!}"/>
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/jquery/jquery-ui.css") !!}">
@endpush
@php
    $forbidden_abilities = $role->getForbiddenAbilities()->pluck("name")->toArray();
    $abilities = $role->getAbilities(false)->pluck("name")->toArray();
    $forbidden = null; $allowed = null;
    $forbidden = in_array("*", $forbidden_abilities) ? true : false;

    if( ! $forbidden){
        $allowed = in_array("*", $abilities) || $role->can("*") ? true : false;
    }
@endphp

@push('scripts')
    <script src="{!! asset("/vendor/mediapress/js/jquery-ui.js") !!}"></script>
    <script>
        $(function () {
            $('[name=mode]').on('ifChecked', function () {
                var mode = $(this).val();
                switch (mode) {
                    case "everything":
                    case "nothing":
                        $('div#accordion').slideUp(200);
                        break;
                    case "custom":
                        $('div#accordion').slideDown(200);
                        break;
                    default:
                        break;
                }
            });
            $('[name=mode]:checked').trigger('ifChecked');
        });
    </script>
@endpush

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{ $title }} - {{ $role->title }} ({{ $role->name }})</div>
            </div>
            <div class="float-right">
                <a href="{{route("Auth.roles.index")}}" class="btn btn-primary btn-sm"><i
                        class="fa fa-arrow-left"></i> {{trans("MPCorePanel::general.back")}}</a>
            </div>
        </div>
        <div class="p-30">
            <form action="{{route("Auth.roles.store_abilities", $role->id)}}" class="form-horizontal" id="saveAbilities" autocomplete="off" method="post">
                {!! csrf_field() !!}

                <div class="form-group">
                    <div class="checkbox">
                        <label class="active">
                            <input type="radio" name="mode"
                                   value="everything" {!! $allowed ? "checked" : "" !!}>
                            {{trans('AuthPanel::roles.ability.allow')}}
                        </label>
                        <label class="passive">
                            <input type="radio" name="mode" value="nothing" {!! $forbidden ? "checked" : "" !!}>
                            {{trans('AuthPanel::roles.ability.no_permission')}}
                        </label>
                        <label>
                            <input type="radio" name="mode"
                                   value="custom" {!! (!($allowed || $forbidden) ? "checked" : "") !!}>
                            {{trans('AuthPanel::roles.ability.special')}}
                        </label>
                    </div>
                </div>
                <div id="accordion">
                    <style>

                        .card{
                            margin-left:1px;
                        }

                        .card-body{
                            min-height: 100px;
                            background-color: transparent;
                        }

                        .card-body .form-group {
                            padding-left: 5px;
                            margin-bottom: 5px;
                            background-color: #fdf9eb;
                        }

                        .card-body .form-group:hover {
                            background-color: #fff8dc; /*cornsilk*/
                            /*background-color: #f0f8ff;*/
                        }

                        .card .card:first-of-type {
                            clear: top;
                            margin-top: 20px;
                        }

                        .card .card{
                            border-color: #f6f6f6 !important;
                        }

                        .card .card .card-header {
                            background-color: #f6f6f6 !important;
                        }

                        .card .card .card-header h5 {
                            font-size:1rem;
                        }

                        .card .name{
                            margin: 20px 0 5px;
                        }

                        .card-body .form-group span {
                            min-width: 210px;
                            margin-right: 10px;
                        }

                        .card-body .form-group>.checkbox{
                            display:inline-block;
                        }

                        .permission_description{
                            display: inline-block;
                            max-height: 22px;
                            overflow: hidden;
                            line-height: 22px !important;
                            font: 15px Comfortaa-Light;
                            vertical-align: top;
                            width: 43%;
                            margin: 0px 3px 0px 30px;
                            transition:max-height 0.5s,color 0.5s;
                            color: #bbb;
                        }

                        .permission_description:hover{
                            max-height: 500px;
                            overflow: visible;
                            color: #333;
                        }

                        #loader-overlay {
                            display: block;
                            position: absolute;
                            background-color:#fff;
                            width: 96%;
                            /*height: auto;*/
                            /*min-height: 150px;*/
                            /*top:36px;*/
                            left:2%;
                            /*bottom:0;*/
                            font: 15px Comfortaa-Regular;
                        }

                        #loader-overlay div#loader-center {
                            width: 150px;
                            /*min-height: 110px;*/
                            /*position: absolute;*/
                            margin: auto;
                            /*top: 20px;*/
                            /*bottom: 0;*/
                            /*left: 0;*/
                            /*right: 0;*/
                            text-align: center;
                        }

                        #loader-overlay div#loader-center img {
                            width: 40px;
                            height: 40px;
                            border-radius: 30px;

                            -webkit-animation-name: spin;
                            -webkit-animation-duration: 2000ms;
                            -webkit-animation-iteration-count: infinite;
                            -webkit-animation-timing-function: linear;
                            -moz-animation-name: spin;
                            -moz-animation-duration: 2000ms;
                            -moz-animation-iteration-count: infinite;
                            -moz-animation-timing-function: linear;
                            -ms-animation-name: spin;
                            -ms-animation-duration: 2000ms;
                            -ms-animation-iteration-count: infinite;
                            -ms-animation-timing-function: linear;

                            animation-name: spin;
                            animation-duration: 2000ms;
                            animation-iteration-count: infinite;
                            animation-timing-function: linear;
                        }

                        #loader-overlay div#loader-center p#loader-text {
                            display: block;
                            width: 100%;
                            /* position: absolute; */
                            /* top: 75px; */
                            color: #999;
                            margin-top: 5px;
                        }

                        @-ms-keyframes spin {
                            0% {
                                -ms-transform: rotate(0deg);
                                border-radius: 10px;
                            }
                            50% {
                                -ms-transform: rotate(180deg);
                                border-radius: 30px;
                            }
                            100% {
                                -ms-transform: rotate(360deg);
                                border-radius: 10px;
                            }
                        }

                        @-moz-keyframes spin {
                            0% {
                                -moz-transform: rotate(0deg);
                                border-radius: 10px;
                            }
                            50% {
                                -moz-transform: rotate(180deg);
                                border-radius: 30px;
                            }
                            100% {
                                -ms-transform: rotate(360deg);
                                border-radius: 10px;
                            }
                        }

                        @-webkit-keyframes spin {
                            0% {
                                -webkit-transform: rotate(0deg);
                                border-radius: 10px;
                            }
                            50% {
                                -webkit-transform: rotate(180deg);
                                border-radius: 30px;
                            }
                            100% {
                                -ms-transform: rotate(360deg);
                                border-radius: 10px;
                            }
                        }

                        @keyframes spin {
                            0% {
                                transform: rotate(0deg);
                                border-radius: 10px;
                            }
                            50% {
                                transform: rotate(180deg);
                                border-radius: 30px;
                            }
                            100% {
                                transform: rotate(360deg);
                                border-radius: 10px;
                            }
                        }


                    </style>

                    {{--            <div class="card" id="cardAuth" data-module="Auth">
                                    <div class="card-header" id="headingAuth">
                                        <h5 class="mb-0">
                                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseAuth"
                                                    aria-expanded="true" aria-controls="collapseAuth">
                                                Rol ve Yetkiler
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseAuth" class="collapse show" aria-labelledby="headingAuth" data-parent="#accordion"
                                         style="">
                                        <div class="card-body">
                                            <div id="accordion_5d398148ef767"></div>

                                        </div>
                                    </div>
                                </div>--}}
                    @foreach($actions as $key => $action)
                        <div class="card subs-unloaded" id="card{{$key}}" data-module="{{$key}}">
                            <div class="card-header" id="heading{{ $key }}">
                                <h5 class="mb-0">
                                    <button type="button" class="btn btn-link" data-toggle="collapse"
                                            data-target="#collapse{{ $key}}" aria-expanded="false"
                                            aria-controls="collapse{{ Str::title($key)}}">
                                        {{ $action["name"] }}
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse{{ $key }}" class="collapse" aria-labelledby="heading{{ $key }}"
                                 data-parent="#accordion">
                                <div class="card-body">

                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--@php dump($actions); @endphp--}}
                </div>
                <div class="float-right">
                    <button class="btn btn-primary">{{trans('MPCorePanel::general.save')}}</button>
                </div>
            </form>
        </div>
    </div>
    <div>
        <pre><code>
                @php
                    //dd(\DB::getQueryLog());
                @endphp
            </code></pre>
    </div>
@endsection
@push("scripts")
    <script>
        $(function () {
            $(".accordion").accordion({
                heightStyle: "content"
            });
        });

        var token;

        $(document).ready(function(){
            token = $("meta[name='csrf-token']").attr("content");
        });

        function fillModuleSubs(module_key_) {
            var el = $("#card" + module_key_);
            $(el).removeClass("subs-unloaded");
            if (!el.length) {
                console.error(module_key_ + " modülü eylemlerini doldurmak için #card" + module_key_ + " seçicisiyle nesne arandı ancak bulunamadı");
                return false;
            }

            var loader = '<div id="loader-overlay" style="display:none;"><div id="loader-center"><img src="{{ asset("/vendor/mediapress/images/default.jpg") }}"/><p id="loader-text">{!! trans('AuthPanel::roles.loading') !!}...</p></div></div>';
            $('.card-body', el).html(loader);
            var loader_el = $("#loader-overlay",el);
            loader_el.fadeIn(1000,function(){
                $.ajax({
                    url: '{{ route('Auth.roles.module_actions', $id) }}',
                    method: 'POST',
                    data: {module_key: module_key_, _token: token},
                    success: function (data) {

                        $(".card-body", el).append(data).animate(
                          {'background-color':'#f3dc19'},300,
                          function() {
                              $(this).animate({'background-color': '#fff'}, 500);
                          }
                        );
                        $(".accordion").accordion({
                            heightStyle: "content"
                        });
                        $('.show > .card-body > .form-group > .checkbox, .show > .card-body > ._accordion > .grouper > .form-group > .checkbox', el).iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue'
                        });

                        loader_el.fadeOut(300);
                    },
                    error: function (jqXHR, status, errorThrown) {
                        $(el).addClass("subs-unloaded");
                        if(jqXHR.responseJSON !== undefined && jqXHR.responseJSON.message =="CSRF token mismatch."){
                            $(".card-body", el).append('<p class="text-danger text-center"><strong>{!! trans('AuthPanel::roles.error.title') !!}:</strong><br><br> '+ "{{ trans('AuthPanel::roles.error.timeout') }}."+ "<br><br>"+'<button class="btn btn-primary" onclick="location.reload();">{!! trans('AuthPanel::roles.error.reload') !!}</button>' + '</p>');
                        }else{
                            $(".card-body", el).append('<p class="text-danger text-center"><strong>{!! trans('AuthPanel::roles.error.title') !!}:</strong><br><br> '+ errorThrown+  (jqXHR.responseJSON !== undefined && jqXHR.responseJSON.message !== undefined ? ": "+jqXHR.responseJSON.message : errorThrown) + "<br><br>"+'<button class="btn btn-primary" onclick="fillModuleSubs(\''+module_key_+'\')">{!! trans('AuthPanel::roles.error.try_againg') !!}</button>' + '</p>');
                        }
                        loader_el.fadeOut(1000);
                    }
                });

            });
        }

        $(document).ready(function () {
            $("html").delegate('div#accordion .card.subs-unloaded .card-header h5 button', 'click', function () {
                var module_key = $(this).closest(".card").data("module");
                $(this).closest(".card.subs-unloaded").removeClass("subs-unloaded");
                fillModuleSubs(module_key);

            });

            $("html").delegate('div#accordion .card-header h5 button', 'click', function () {
                var card_body = $(this).closest(".card").find(".card-body").first();
                $(card_body).children('.form-group').children(".checkbox").iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
            });
        });
    </script>
@endpush
