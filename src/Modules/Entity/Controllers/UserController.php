<?php

namespace Mediapress\Modules\Entity\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Entity\Models\UserExtra;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Mediapress\Modules\Entity\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE = 'message';
    public const ENTITY_USERS_INDEX = 'Entity.users.index';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const EMAIL = 'email';
    public const ENTITY_PANEL_USER_PASS_WORD_USER = "EntityPanel::user.password-user";
    public const PASS_WORD = 'password';
    public const WEBSITE_ID = 'website_id';
    public const NAME = 'name';
    public const CONFIRMED = 'confirmed';
    public const ENTITY_PANEL_USER_EMAIL_USER = "EntityPanel::user.email-user";
    public const EMAIL_UNIQUE = 'email.unique';
    public const ENTITY_PANEL_USER_PASS_WORD_USER1 = self::ENTITY_PANEL_USER_PASS_WORD_USER;
    public const REQUIRED = 'required';
    public const ENTITY_PANEL_USER_NAME_USER = "EntityPanel::user.name-user";
    public const ENTITY_PANEL_USER_WEBSITE = "EntityPanel::user.website";
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';
    public const MP_CORE_PANEL_VALIDATION_UNIQUE = "MPCorePanel::validation.unique";
    public const UNIQUE = 'unique';

    public function index()
    {


        if(! activeUserCan(["entity.users.index",])){return rejectResponse();}

        $website_id = session("panel.website")->id;

        if (request()->get('list') && request()->get('list')=="all") {
            $users = User::paginate(25);
        }
        else{
            $users = User::where(self::WEBSITE_ID,$website_id)->paginate(25);
        }
        return view('EntityPanel::users.index', compact('users'));
    }

    public function create()
    {
        if(! activeUserCan(["entity.users.create",])){return rejectResponse();}

        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck("slug", "id");

        return view("EntityPanel::users.create", compact('websites'));
    }

    public function edit($id)
    {
        if(! activeUserCan(["entity.users.update",])){return rejectResponse();}
        $user = User::find($id);
        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck("slug", "id");
        return view('EntityPanel::users.edit', compact("user", 'websites'));
    }

    public function store(Request $request)
    {

        $fields = [
            self::NAME => trans(self::ENTITY_PANEL_USER_NAME_USER),
            self::EMAIL => trans(self::ENTITY_PANEL_USER_EMAIL_USER),
            self::PASS_WORD => trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1),
            self::WEBSITE_ID => trans(self::ENTITY_PANEL_USER_WEBSITE),
        ];

        $rules = [
            self::NAME => 'required|max:255',
            self::EMAIL => 'required|unique:users',
            self::PASS_WORD => 'required|confirmed',
            self::WEBSITE_ID => self::REQUIRED,
        ];

        $messages = [

            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_NAME_USER)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_EMAIL_USER)]),
            'password.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1)]),
            self::EMAIL_UNIQUE =>  trans(self::MP_CORE_PANEL_VALIDATION_UNIQUE, [self::UNIQUE,trans(self::ENTITY_PANEL_USER_EMAIL_USER)]),
            'website_id.required' =>  trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_WEBSITE)]),
        ];

        $datas = [
            self::NAME => $request->name,
            self::EMAIL => $request->email,
            self::PASS_WORD => Hash::make($request->password),
            self::WEBSITE_ID => $request->website_id,
        ];

        $user = User::where("username",$datas["name"])->orWhere("email",$datas["email"])->first();
        if($user){
            if(! activeUserCan(["entity.users.update",])){return rejectResponse();}
        }else{
            if(! activeUserCan(["entity.users.create",])){return rejectResponse();}
        }

        $this->validate($request, $rules, $messages, $fields);

        $insert=User::firstOrCreate($datas);

        if ($insert){
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route(self::ENTITY_USERS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        if(! activeUserCan(["entity.users.update",])){return rejectResponse();}

        $user = User::find($request->id);
        /*
         * Validation
         */

        $fields = [
            self::NAME => trans(self::ENTITY_PANEL_USER_NAME_USER),
            self::EMAIL => trans(self::ENTITY_PANEL_USER_EMAIL_USER),
            self::PASS_WORD => trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1),
            self::WEBSITE_ID => trans(self::ENTITY_PANEL_USER_WEBSITE)
        ];

        $rules = [
            self::NAME => 'required|max:255',
            self::WEBSITE_ID => self::REQUIRED,
            self::EMAIL => 'unique:users,email,'.$user->id,
            self::PASS_WORD => self::CONFIRMED
        ];

        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_NAME_USER)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_EMAIL_USER)]),
            self::EMAIL_UNIQUE =>  trans(self::MP_CORE_PANEL_VALIDATION_UNIQUE, [self::UNIQUE,trans(self::ENTITY_PANEL_USER_EMAIL_USER)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1)]),
            'website_id.required' =>  trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::ENTITY_PANEL_USER_WEBSITE)]),
        ];

        if($request->old_email!=$request->email) {
            $messages += [self::EMAIL_UNIQUE =>  trans(self::MP_CORE_PANEL_VALIDATION_UNIQUE, [self::UNIQUE,trans(self::ENTITY_PANEL_USER_PASS_WORD_USER1)])];
        }

        $datas = [
            self::NAME => $request->name,
            self::EMAIL => $request->email,
            self::WEBSITE_ID => $request->website_id,
            self::PASS_WORD => Hash::make($request->password)
        ];


        if($request->password){
            $datas[self::PASS_WORD]=Hash::make($request->password);
        }

        $this->validate($request, $rules, $messages, $fields);

        $update=User::updateOrCreate(['id'=>$request->id],$datas);
        if( $request->input('extras') ){
            foreach( $request->extras as $key => $value ){
                UserExtra::updateOrCreate(
                    [
                        'user_id' => $update->id,
                        'key' => $key
                    ],
                    [
                        'value' => $value
                    ]
                );
            }
        }
        if ($update){
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::ENTITY_USERS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {

        if(! activeUserCan(["entity.users.delete",])){return rejectResponse();}

        if(!userAction('message.users.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data=User::find($id);

        if($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect(route(self::ENTITY_USERS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
