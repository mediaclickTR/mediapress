<?php

namespace Mediapress\Modules\Entity;

use Mediapress\Contracts\EntityInterface;
use Mediapress\Contracts\IEntityModule;
use Mediapress\Models\MPModule;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Entity\Models\EntityList;
use Mediapress\Modules\Entity\Models\EntitylistPivot;
use Mediapress\Modules\Entity\Models\User;
use Mediapress\Modules\Entity\Models\Entities;

class Entity extends MPModule implements IEntityModule
{
    public const SUBMENU = "submenu";
    public const TYPE = "type";
    public const TITLE = "title";
    public const URL = "url";
    public $name = "Entity";
    public $url = "mp-admin/Entity";
    public $description = "Core content manager of Mediapress";
    public $author = "";
    public $menus = [];
    private $plugins = [];
    public $website_id;

    public function __construct()
    {
        $website = session("panel.website");
        $this->website_id = $website->id;
    }

    public function fillMenu($menu)
    {
        #region Header Menu > Datasets > Users > Set

        $loginSitemap = Sitemap::where('feature_tag', 'login')->first();
        if($loginSitemap) {
            $content_cols_modules_rows_set = [
                [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans("EntityPanel::menu_titles.users"),
                    self::URL =>route("Entity.users.index")
                ],
            ];

            $menu = dataGetAndMerge($menu, 'header_menu.content.cols.modules.rows',$content_cols_modules_rows_set);
        }
        #endregion

        #region Header Menu > Author > Set

        // Tab Name & Image & Author Title Set
        data_set($menu, 'header_menu.author.cols.user_menus.name',trans("MPCorePanel::menu_titles.author"));
        data_set($menu, 'header_menu.author.cols.user_menus.author_title',  auth()->user()->username);
        data_set($menu, 'header_menu.author.cols.user_menus.author_image',  get_gravatar(auth()->user()->email,27));

        $datasets_cols_data_rows_set = [];
        if(auth()->user()->role_id != 1) {
            $datasets_cols_data_rows_set = [
                [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans('MPCorePanel::general.profile'),
                    self::URL => route('Auth.profile.index'),
                    'target'=> "_self"
                ],
            ];
        }

        $datasets_cols_data_rows_set = array_merge($datasets_cols_data_rows_set, [
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans('MPCorePanel::general.preview_site'),
                self::URL => "//" . session('panel.website.slug'),
                'target'=> "_blank"
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans('MPCorePanel::general.logout'),
                self::URL => route('panel.logout'),
                "target"=> "_self",
                "onclick"=>"event.preventDefault(); document.getElementById('logout-form').submit();",
                "additions"=>'<form id="logout-form" action="'.route("panel.logout").'" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="'.csrf_token().'"/>
                          </form>'
            ]
        ]);

        return dataGetAndMerge($menu, 'header_menu.author.cols.user_menus.rows',$datasets_cols_data_rows_set);
        #endregion

    }
    /**
     * @return array
     */
    public function getEntitylists()
    {
        return EntityList::whereHas('websites', function($query){
            $query->where('id',$this->website_id);
        })->get();
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getEntitylist($id)
    {
        return Entitylist::where("id", $id)->first();
    }

    public function getUsers()
    {
        return User::where("website_id", $this->website_id);
    }

    public function getEntities()
    {
        return Entities::get();
    }

    /**
     * @param integer $pivot_id
     * @return array
     */
    public function getEntitylistPivot($pivot_id)
    {
        return EntitylistPivot::find($pivot_id);
    }

    /**
     * @param integer $entitylist_id
     * @return array
     */
    public function getEntitylistPivotWithEntitylistID($entitylist_id)
    {
        return EntitylistPivot::where("entitylist_id",$entitylist_id)->get();
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getEntitylistDetailWithID($id)
    {
        return EntitylistPivot::where("id",$id)->first();
    }

    /**
     * @param string $model
     * @param integer $entitylist_id
     * @return array
     */
    public function getEntityWithUserableID($model,$userable_id)
    {
        return $model::where("id",$userable_id)->first();
    }

    /**
     * @return array $data
     */
    public function getMaillist()
    {
        $table_names = [trans("MPCorePanel::general.users"), trans("HeraldistPanel::mail_service.e_bulletin")];
        $tables = ['users','e_bulletins'];
        $names = ['name','name'];
        $columns = ['email','email'];

        $data['rows'] = createEmailList($tables,$columns,$table_names,$names);
        // Gruplama için gerekli
        $data['table_names'] = $table_names;

        return $data;
    }

    public function getPanelMenus(){

    }
    public function serveClasses(){


    }
    public function getModuleClasses(){

    }
    public function getMPModuleNames(){

    }

}
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 11.06.2018
 * Time: 13:59
 */
