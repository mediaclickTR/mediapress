@php
    $sitemap = $mediapress->data['sitemap'];
    $category = $mediapress->data['category'];
    $pages = $mediapress->data['pages'] ?? [];
    $criterias = $mediapress->data['criterias'] ?? [];
@endphp
<div class="container">
    <h1 class="my-4">{!! $sitemap->detail->name !!}</h1>

    @include("ContentWeb::default.layouts.breadcrumb")

    <div class="col-md-12">
        @if($category->f_)
            @if(is_a($category->f_, 'Illuminate\Database\Eloquent\Collection'))
                <img class="img-fluid" src="{!! image($category->f_->first()->id)->resize(['w' => 1050, 'h' => 300]) !!}" width="1050" height="300" alt="">
            @else
                <img class="card-img-top img-fluid" src="{!! image($category->f_->id)->resize(['w' => 1050, 'h' => 300]) !!}" width="1050" height="300" alt="">
            @endif
        @else
            <img class="card-img-top img-fluid" src="http://placehold.it/1050x300" width="1050" height="300" alt="">
        @endif
        <h3 class="my-3">{!! $category->detail->name !!}</h3>
        {!! $category->detail->detail !!}
    </div>
    <div class="row">
        <div class="col-md-4">
            @foreach($criterias as $criteria)
                <div class="item">
                    <span>{!! $criteria->detail->name !!}</span>
                    <div class="form-group">
                        <ul>
                            @foreach($criteria->children as $children)
                                <li>
                                    <label>
                                        <input type="checkbox" class="filter-checkbox" data-id="{{$children->id}}"
                                               data-parent-id="{{$criteria->id}}">
                                        {!! $children->detail->name !!}
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-md-8 lists">
            @include('ContentWeb::default.sitemap_dynamic_category_criteria.category_ajax')
        </div>
    </div>
</div>

@push('scripts')
    <script>
        var selected_ids = {};

        $('.filter-checkbox').on('click', function () {
            var parent_id = $(this).attr('data-parent-id');
            var id = $(this).attr('data-id');


            if (!Array.isArray(selected_ids[parent_id])) {
                selected_ids[parent_id] = [];
            }


            var index = selected_ids[parent_id].indexOf(id);
            if (index !== -1) {
                selected_ids[parent_id].splice(index, 1);
            } else {
                selected_ids[parent_id].push(id);
            }

            $.ajax({
                'data': {
                    'selected_ids': selected_ids,
                    'category_id': "{{$mediapress->parent->id}}",
                    '_token': "{{csrf_token()}}"
                },
                'url': "{{ route("ajaxCriteriaFilter") }}",
                'method': "post",
                success: function (result) {
                    $('div.lists').html(result);
                }
            })
        });


        $('body').delegate('.lists > ul > li > .page-link', 'click', function (e) {
            e.preventDefault();

            var page = $(this).html();

            $.ajax({
                'url': "{{ route("ajaxCriteriaFilter") }}",
                'data': {
                    'page': page,
                    'selected_ids': selected_ids,
                    'category_id': "{{$mediapress->parent->id}}",
                    '_token': "{{csrf_token()}}"
                },
                'method': 'post',
                success: function (result) {
                    $('div.lists').html(result);
                }
            });
        });

    </script>
@endpush
