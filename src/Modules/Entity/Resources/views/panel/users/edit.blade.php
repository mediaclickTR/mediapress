@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">{!! trans("EntityPanel::user.edit-user") !!}</div>
            </div>
        </div>
        <div class="p-30">
            <div class="col-md-12">
                @include("EntityPanel::users.form")
            </div>
        </div>
    </div>
@endsection
