<?php

return [
    'menus' => 'Menus',
    'menu_detail' => 'Menu Detail',
    'edit_menu' => 'Edit Menu',
    'settings' => 'Settings',

    'status' => 'Status',
    'name' => 'Menu Name',
    'slug' => 'Menu Slug',
    'type' => 'Menu Type',
    'sections' => 'Menu Section',

    'select_url' => 'Select Url',
    'link_types' => 'Menu Links',
    'self' => 'In Itself (_self)',
    'blank' => 'In New Window (_blank)',

    'view' => 'Menu View',
    'dropdown_menu' => 'Dropdown Menu',
    'mega_menu' => 'Mega Menu',
    'single_menu' => 'Single Menu',

    'targets' => 'Target Type (Target)',
    'inlink' => 'In Link',
    'outlink' => 'Out Link',
    'emptylink' => 'Empty Link',

    'header.menu' => 'Header Menu',
    'footer.menu' => 'Footer Menu',
    'sidebar.menu' => 'Sidebar Menu',
    'other.menu' => 'Other Menu',

    'empty_menu_detail' => 'No menu in :language has been added yet. You can add it from the Add New section on the right.',
    'info' => 'The new title will be added to the left. Drag this title to the place you want and press the save button.',

    'copy_menu' => 'Copy Menu',
    'source_country_group' => 'Source Country Group Code',
    'target_country_group' => 'Target Country Group Code',
    'source_language' => 'Source Language',
    'target_language' => 'Target Language',

    'new_page_1' => "Or create",
    'new_page_2' => "new page",

    'delete.info' => 'The information you delete will be permanently removed from the system.',
    'delete.child_info' => 'Do you want to delete sub-menus of the menu you deleted?',
    'draft.info' => 'Did you save the draft?',
    'copy.info' => 'All records of the target language will be deleted. Do you confirm?',
    'no_data_source_lang' => 'No menu information for the source language was found. Please choose another language.',
];
