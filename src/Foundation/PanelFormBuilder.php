<?php

namespace Mediapress\Foundation;

use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\File;

class PanelFormBuilder
{
    public const MODEL = "model";
    public const SITEMAP_ID = 'sitemap_id';
    public const WHERE = "where";
    public const LANGUAGES = "languages";
    public const OPTIONS = "options";
    private static $only = [
        "find", self::MODEL, "select", "pluck", self::WHERE, "whereHas", "whereNull", "orderBy", self::SITEMAP_ID
    ];

    private static function getFileContent($directory, $file, $data)
    {
        $path = resource_path('views' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'mediapress' . DIRECTORY_SEPARATOR . 'form-builder' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $file . ".json");

        if (File::exists($path)){
            return File::get($path, true);
        }

        $path = mediapress_path('Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel' . DIRECTORY_SEPARATOR . 'mediapress-builder' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $file . ".json");

        if (File::exists($path)) {
            return File::get($path, true);
        }
        if ($file == 'static') {
            $id = \Arr::get($data, self::SITEMAP_ID);
            dd('Form Builder için "' . $directory . DIRECTORY_SEPARATOR . $file . '.json" veya "' . $directory . DIRECTORY_SEPARATOR . $file . '.' . $id . '.json" dosyası bulunamadı!');
        }


        dd('Form Builder için "' . $directory . DIRECTORY_SEPARATOR . $file . '.json" dosyası bulunamadı!');
    }

    public static function create($directory, $file, FormBuilder $formBuilder, array $options = [], $data = [])
    {
        if (!$directory) {
            dd("Form oluşturucu için modül belirtilmelidir");
        }

        $fileContent = self::getFileContent($directory, $file, $data);

        $tabs = json_decode($fileContent, true);

        if (isset($data[self::LANGUAGES])) {
            $languages = $data[self::LANGUAGES];
        } else if (isset($data[self::SITEMAP_ID])) {
            $languages = sitemapLanguages($data[self::SITEMAP_ID]);
        } else {
            $languages = session("website")->languages->pluck("code", "id");
        }

        $form = null;
        if ($tabs) {
            foreach ($tabs as $key => $tab) {
                $tab["key"] = $key;
                $form[$key] = self::tabCreator($tab, $formBuilder, $options, $data, $languages);
            }
        }
        return $form;
    }

    private static function tabCreator($tabFields, $formBuilder, $options, $data, $languages)
    {
        $form = false;

        $options += ["tabName" => FieldTranslator::trans($tabFields["tabName"])];

        //Tab'ın attr bilgileri
        $options += ["attr" => \Arr::get($tabFields, "attr")];


        if (\Arr::get($tabFields, self::LANGUAGES)) {
            foreach ($languages as $id => $language) {
                $form[$language] = self::formCreator($tabFields, $formBuilder, $options, $data, [
                    "id" => $id, "code" => $language
                ]);
            }
        } else {
            $form = self::formCreator($tabFields, $formBuilder, $options, $data);
        }

        return $form;
    }

    /**
     * @param Form $tab
     * @param FormBuilder $formBuilder
     * @param array $options
     * @param array $data
     * @param array $language
     * @return \Kris\LaravelFormBuilder\Form
     */
    private static function formCreator($tab, FormBuilder $formBuilder, $options, $data = null, $language = [])
    {

        $form = $formBuilder->plain($options);
        foreach ($tab["fields"] as $field) {
            if (isset($field["data"][self::MODEL])) {
                $model = $field["data"][self::MODEL];

                unset($field["data"][self::MODEL]);
            } else {
                $model = $tab[self::MODEL];
            }

            $field[self::OPTIONS][self::MODEL] = $model;


            if (isset($language["code"])) {
                $fieldName = $tab["key"] . "." . $field["name"] . "[" . $language["code"] . "]";
            } else {
                $fieldName = $tab["key"] . "." . $field["name"];
            }

            foreach (\Arr::only($data, self::$only) as $key => $item) {
                if ($key == self::WHERE) {
                    $field["data"][self::WHERE][] = $item;
                    continue;
                }

                $field["data"][$key] = $item;
            }

            //ilk relation içerisine dil koşulu atılıyor
            if (!empty($language)) {
                $explode = explode(".", $field["name"]);

                if ($explode[0] != "extras") {
                    $field["data"][$explode[0]][self::WHERE][] = [
                        "language_id", "=", $language["id"]
                    ];
                }
            }

            // Eğer options>auto-wrapper: auto ise, oluşturulan alanın kapsayıcı div'ine alan ismini class olarak atıyor.
            if (isset($field[self::OPTIONS]['auto-wrapper']) && $field[self::OPTIONS]['auto-wrapper'] == true && isset($tab["key"])) {
                $field[self::OPTIONS]['wrapper']['class'] = "form-group " . $tab["key"] . '___' . self::fieldNameReplace($field["name"]);
            }

            //field'ın veri yüklemeleri yapılıyor

            DataBuilder::fieldDataBinder($model, $field);
            if (isset($field['name']) && $field['name'] == 'detail.slug' && isset($language['id']) && isset($field['data']['find'][2]) && isset($field['data']['detail'])) {
                $url_id = -1;
                $dbModel = (new $model())->where($field['data']['find'][0], $field['data']['find'][1], $field['data']['find'][2])->first()->detail();
                foreach ($field['data']['detail'][self::WHERE] as $where) {
                    if (isset($where[2])) {
                        $dbModel = $dbModel->where($where[0], $where[1], $where[2]);
                    }
                }
                if ($dbModel->first()) {
                    $dbUrl = $dbModel->first()->url;
                    if (isset($dbUrl->url)) {
                        $url_id = $dbUrl->id;
                    }
                }
                if (isset($field[self::OPTIONS]['attr'])) {
                    $field[self::OPTIONS]['attr']['data-url-id'] = $url_id;
                } else {
                    $field[self::OPTIONS]['attr'] = ['data-url-id' => $url_id];
                }
            }

            FieldTranslator::translator($field);

            $fieldName = self::fieldNameReplace($fieldName);
            if (isset($field[self::OPTIONS]["javascript"])) {
                JavascriptBuilder::createScript($fieldName, $field[self::OPTIONS]["javascript"], $language);
            }

            $form->add($fieldName, $field["type"], $field[self::OPTIONS]);


        }

        return $form;
    }

    public static function fieldNameReplace($fieldName, $process = true)
    {
        if ($process) {
            return str_replace([
                ".*", "."
            ], [
                null, "___"
            ], $fieldName);
        } else {
            return str_replace("___", ".", $fieldName);
        }
    }
}
