<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class CountryGroupLanguage extends Model
{
    use CacheQueryBuilder;
    protected $table = 'country_group_language';

    public $timestamps = false;

    protected $fillable = [
        'language_id',
        'country_group_id',
        'default',
    ];

    public function languages()
    {
        return $this->belongsToMany(CountryGroupLanguage::class);
    }

    public function country_groups()
    {
        return $this->belongsToMany(CountryGroup::class);
    }

}
