<?php

namespace Mediapress\Modules\MPCore\Controllers\Technical;
use Illuminate\Support\Facades\Route;

class RouteController
{
    public function index()
    {
        $routes = Route::getRoutes();

        if (request()->has("method")){
            $routes = $routes->getRoutesByMethod()[request()->get("method")];
        }
        return view("MPCorePanel::routes.index", compact("routes"));
    }
}