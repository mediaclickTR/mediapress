<?php

namespace Mediapress\Foundation;

use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\Content\Models\Menu as Model;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Illuminate\Database\Eloquent\Collection;
use Cache;
use Illuminate\Http\Request;


class Menu
{
    public const PARENT_ID = 'parent_id';

    public $category = 1;
    public $criteria = 0;
    public $property = 0;
    public $depth = 0;
    public $slug = false;
    public $id = false;
    public $items;
    public $menu = null;
    public $model = null;
    /** @var Request $request */
    public $request = null;
    public $language;
    public $website;

    public function __construct($array = [], $website, $activeCountryGroup, $activeLanguage)
    {
        $this->website = $website;
        $this->language = $activeLanguage;
        $this->countryGroup = $activeCountryGroup;

        $this->items = new Collection();
        if (isset($array['category'])) {
            $this->category = $array['category'];
        }
        if (isset($array['criteria'])) {
            $this->criteria = $array['criteria'];
        }
        if (isset($array['property'])) {
            $this->property = $array['property'];
        }
        if (isset($array['depth'])) {
            $this->depth = $array['depth'];
        }
        if (isset($array['slug'])) {
            $this->slug = $array['slug'];
        } elseif (isset($array['id'])) {
            $this->id = $array['id'];
        } else if (!isset($array['link'])) {
            dd('Menu slug ');
        }

        $this->setParameters();

    }

    public function setParameters()
    {
        if ($this->id && !$this->slug) {
            $this->model = Model::select(['id', 'slug'])->where("website_id", $this->website->id)->find($this->id);
        }
        if ($this->slug && !$this->id) {
            $this->model = Model::select(['id', 'slug'])->where('slug', $this->slug)->where("website_id", $this->website->id)->first();
        }
        if ($this->model) {
            $this->slug = $this->model->slug;
            $this->id = $this->model->id;
        }
        unset($this->model);

    }

    public function get()
    {
        if ($this->id) {
            $menuHolder = Cache::rememberForever(cache_key($this->countryGroup->id . $this->language->id . 'menu-' . $this->id.'.'.webp()), function () {
                $menuHolder = [];
                $menuHolder['menu'] = Model::with([
                    'details' => function ($query) {
                        $query->where('language_id', $this->language->id)
                            ->where('country_group_id', $this->countryGroup->id)
                            ->with('url.model')
                            ->orderBy('lft')
                            ->where('status', 1);
                    }
                ])->where("website_id", $this->website->id)->find($this->id);

                if (isset($menuHolder['menu']->details)) {

                    $items = new Collection();
                    foreach ($menuHolder['menu']->details as $detail) {
                        if ($detail->parentModel && $detail->parentModel->status != 1) {
                            continue;
                        }

                        if ($detail->parentModel && $detail->parentModel->parentModel && $detail->parentModel->parentModel->status != 1) {
                            continue;
                        }
                        if ($detail->url) {
                            $model = $detail->url->model;

                            if ($model instanceof SitemapDetail && $model->sitemap) {
                                $sitemap = $model->sitemap;
                                unset($detail->url->model);

                                if ($sitemap->category == '1' && config("mediapress.categories")) {
                                    $detail->setRelation('categories', cluster($model->sitemap->categories()->orderBy('lft')->get(), [self::PARENT_ID => 'category_id']));
                                }

                                if ($sitemap->criteria == '1') {
                                    $detail->setRelation('criterias', cluster($model->sitemap->criterias, [self::PARENT_ID => 'criteria_id']));
                                }
                                $pages = config('mediapress.pages');
                                if (is_numeric($pages)) {
                                    $detail->setRelation('pages', cluster($model->sitemap->pagesOrdered()->limit($pages)->get(), [self::PARENT_ID => 'page_id']));
                                } elseif ($pages) {
                                    $detail->setRelation('pages', cluster($model->sitemap->pagesOrdered()->get(), [self::PARENT_ID => 'page_id']));
                                }


                            }
                        }

                        $detail->image = $detail->file_id ? image($detail->file_id) : null;

                        $items[] = $detail;
                    }

                    $menuHolder['items'] = $items;
                }
                return $menuHolder;
            });
            $this->menu = $menuHolder['menu'];
            $this->items = $menuHolder['items'];
        }

        return $this;
    }

    public function toTree($parameters)
    {
        if (config('mediapress.cache')) {
            return Cache::rememberForever(cache_key($this->countryGroup->id . $this->language->id . 'menu' . $this->menu->id . '-tree' . webp()), function () use ($parameters) {
                return cluster($this->items, $parameters);
            });
        }

        return cluster($this->items, $parameters);
    }

    public function link(MenuDetail $detail, $attributes = [])
    {
        $link = null;

        if ($detail->url && $detail->type == 3) {
            $link = $detail->url->url;
        }

        if ($detail->out_link && $detail->type == 2) {
            $link = $detail->out_link;
        }
        $attributes['target'] = $detail->target;

        return link_to($link, $detail->name, $attributes);
    }
}
