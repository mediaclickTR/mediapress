<div class="bd-example white p-4 mb-4 fullBoxes">
    <div class="row">
        <div class="col">
            <div class="title color mb-4">{{ __("InteractionPanel::dashboard.gender.title") }}</div>
            <div class="row">
                <div class="col-3">
                    <div class="text mt-5 p-3">
                        <span>{{ __("InteractionPanel::dashboard.gender.male") }}</span>
                        <i class="blue">%{!! number_format((isset($data['genders']['male']) ? $data['genders']['male'] : 0)/(array_sum($data['genders']) ? array_sum($data['genders']) : 1)*100,1) !!}</i>
                        <em class="blue">{!! number_format((isset($data['genders']['male']) ? $data['genders']['male'] : 0),0,',','.') !!}</em>
                    </div>
                </div>
                <div class="col-6">
                    <div id="gender" class="otherMini"></div>
                </div>
                <div class="col-3">
                    <div class="text mt-5 p-3">
                        <span>{{ __("InteractionPanel::dashboard.gender.female") }}</span>
                        <i class="orange">%{!! number_format((isset($data['genders']['female']) ? $data['genders']['female'] : 0)/(array_sum($data['genders']) ? array_sum($data['genders']) : 1)*100,1) !!}</i>
                        <em class="orange">{!! number_format((isset($data['genders']['female']) ? $data['genders']['female'] : 0),0,',','.') !!}</em>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="title color mb-4">{{ __("InteractionPanel::dashboard.ages") }}</div>
            <div id="ageDistribution" class="otherMini"></div>
        </div>
    </div>
</div>

<script>
    window.graphData.gender = @json($data['gender']);
    window.graphData.genders = @json($data['genders']);
    window.graphData.ages = @json($data['ages']);
</script>

<script src="{!! asset('vendor/mediapress/js/dashboardGenderAge.js') !!}"></script>
