@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! $title !!}</div>
            </div>
        </div>
        <div class="p-30 mt-4">
            <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.slug") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sitemaps as $sitemap)
                    <tr>
                        <td>{!! $sitemap->id !!}</td>
                        <td>{!! ($sitemap->detail) ? $sitemap->detail->name : '' !!}</td>
                        <td>{!! ($sitemap->detail) ? $sitemap->detail->slug : '' !!}</td>
                        <td>
                            @php $types = ["category", "criteria", "property"]; @endphp
                            @foreach($types as $_type)
                                @if($type == $_type)
                                    <a href="{!! route('Content.categories.'.$type.'.create',$sitemap->id) !!}" title="{!! trans("MPCorePanel::general.edit") !!}">
                                        <span class="fa fa-pen"></span>
                                    </a>
                                @endif
                            @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
