<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSitemapsTableAddSitemapId extends Migration
{

    public const SITEMAPS = 'sitemaps';

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAPS)) {
            $schema->table(self::SITEMAPS, function (Blueprint $table) {
                $table->integer('sitemap_id')->nullable();
            });
        }
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAPS)) {
            $schema->table(self::SITEMAPS, function (Blueprint $table) {
                $table->dropColumn("sitemap_id");
            });
        }
    }
}
