<div class="modal fade show" id="copyLanguageModal" tabindex="-1" role="dialog" aria-labelledby="copyLanguageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-md-12 mb-5 mt-5 text-center">
                    Hangi dillere kopyalamak istiyorsanız onları seçmeniz gerekmektedir.
                    <div class="text">

                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="page_name">
                    <div class="row">
                        @foreach($page->sitemap->details as $detail)
                            <div class="col-md-3">
                                <div class="form-check checkbox">
                                    <input type="checkbox" class="form-check-input" id="{{ $detail->countryGroup->code . '_' . $detail->language->code }}" name="target[]" value="{{ $detail->country_group_id . ',' . $detail->language_id }}">
                                    <label class="form-check-label" for="{{ $detail->countryGroup->code . '_' . $detail->language->code }}">{{ strtoupper($detail->language->code) . ' (' . strtoupper($detail->countryGroup->code) . ')' }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="mt-4 mb-4 buttons float-right">
                        <button class="btn btn-outline-dark" id="selectAllLangs">Tümünü Seç</button>
                        <button class="btn btn-outline-dark" onclick="copyDetail()">Kopyala</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .form-check > .icheckbox_square-blue {
        float: inherit !important;
    }
</style>
<!-- Tag added -->
<script>
    setTimeout(function(){
        $(document).on('click', '#selectAllLangs', function(){
            $('#copyLanguageModal input').each(function(){
                $(this).iCheck('check');
            } );
        });
    },2000 );
</script>
