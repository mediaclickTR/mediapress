<?php

use Illuminate\Support\Facades\Schema;
use Mediapress\Modules\MPCore\Foundation\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('menus', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('website_id')->unsigned()->nullable();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->integer('type')->nullable()->unsigned();
            $table->string('sections')->nullable();

            $table->defaultFields();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
