<?php

return [
    'admins' => 'Administrators',
    'website' => 'Web Site',
    'create-admin' => 'Add Admin',
    'edit-admin' => 'Edit Admin',
    'role-admin' => 'Role',
    'language' => 'Language',
    'username-admin' => 'Username',
    'phone-admin' => 'Phone',
    'name-admin' => 'Admin Name',
    'surname-admin' => 'Admin Surname',
    'email-admin' => 'Admin Email',
    'password-admin' => 'Admin Password',
    'password-admin-small' => 'It must contain letters, numbers, and special characters (@ $!% * #?. &).',
    'repeat-password-admin' => 'Repeat Admin Password',
    'roles' => 'Roles',
    'select_zone' => 'Select Zone'
];
