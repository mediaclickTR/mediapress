<?php

namespace Mediapress\Modules\MPCore\Controllers;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\MPCore\Models\Url;

class RedirectController extends Controller
{

    public $redirectUrl;
    public $code;

    /***
     * Parse edilen url'i birleştiriyor
     * @param string $parsed_url
     * @return string
     * @see http://php.net/manual/tr/function.parse-url.php#106731
     */
    public function __construct($redirectUrl = null,$code=302)
    {
        $this->redirectUrl = $redirectUrl;
        $this->code = $code;
    }

    public function main()
    {
       
        return redirect($this->redirectUrl,$this->code);
    }

    public function redirectEdit(Request $request)
    {
        $getUrl = $request->url;


        $url = Url::where('url', '/' . $getUrl)->where('type', 'original')->first();

        if ($url) {
            $relation = $url->relation;

            if ($relation) {
                $parent = $relation->parent;

                if ($parent instanceof Page) {
                    return redirect(route('Content.pages.edit', ['sitemap_id' => $parent->sitemap_id, 'id' => $parent->id, 'onSave' => $getUrl]),$this->code);
                }
                else if ($parent instanceof Sitemap) {

                    return redirect(route('Content.sitemaps.mainEdit', ['sitemap' => $parent->id,  'onSave' => $getUrl]),$this->code);
                }

                else if ($parent instanceof Category) {
                    return redirect(route('Content.categories.category.create', ['sitemap_id' => $parent->sitemap_id])."?quickAccess=".$parent->id,$this->code);
                }



            }


        }


          return redirect('/mp-admin');

    }

}
