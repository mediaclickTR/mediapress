<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagePopupTable extends Migration {

	public function up()
	{
        if (Schema::hasTable('popup_languages'))
            return;

		Schema::create('popup_languages', function(Blueprint $table) {
            $table->integer('popup_id')->unsigned()->index();
			$table->integer('country_group_id')->unsigned()->index();
			$table->integer('language_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::dropIfExists('popup_languages');
	}
}
