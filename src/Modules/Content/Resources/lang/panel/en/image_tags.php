<?php
return [
    "url" => "URL",
    "image" => "Image",
    "title" => "Title",
    "alt_tag" => "Alt Tag",
    "detail" => "Detail",
];
