@if(!isset($role))
    {{ Form::open(['route' => route('Auth.roles.update',$role)]) }}
@else
    {{ Form::model($role, ['route' => ['Auth.roles.update',$role, "isNew"=>request()->isNew ? "yes":"no"], 'method' => 'POST']) }}
@endif
@csrf

<div class="row">
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::roles.slug") !!}</label>
            {!!Form::text('name', null, ["placeholder"=>trans("AuthPanel::roles.slug"), "class"=>"validate[required]"])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::roles.title") !!}</label>
            {!!Form::text('title', null, ["placeholder"=>trans("AuthPanel::roles.title"), "class"=>"validate[required]"])!!}
        </div>
    </div>
</div>

<div class="float-right">
    @if($role->status == 3)
        <button class="btn btn-primary">{!! trans("AuthPanel::roles.save_and_edit_perm") !!}</button>
    @else
        <a class="btn btn-primary" href="{{route("Auth.roles.abilities",$role)}}">{!! trans("AuthPanel::roles.edit_perm") !!}</a>
        <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
    @endif
</div>


