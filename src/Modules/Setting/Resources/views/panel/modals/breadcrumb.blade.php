@php
    $menus = \Mediapress\Modules\Content\Models\Menu::where('status', 1)->get(['slug', 'name', 'id']);
@endphp

<div class="modal fade bd-example-modal-lg" id="breadcrumb" tabindex="-1" role="dialog"
     aria-labelledby="breadcrumbLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="breadcrumbLabel">{!! __('SettingPanel::breadcrumb.modal_title') !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row col-md-12">
                    <ul>
                        <li>
                            <label>{!! __('SettingPanel::breadcrumb.add_homepage') !!}</label>
                            <div class="form-group">

                                @if(isset($settings['breadcrumb.add_homepage']))
                                    <input
                                        {!! $settings['breadcrumb.add_homepage'] == 1 ? 'checked' : '' !!} type="radio"
                                        name="add_homepage" value="1"> {!! __("SettingPanel::general.yes") !!}
                                    <input
                                        {!! $settings['breadcrumb.add_homepage'] == 0 ? 'checked' : '' !!} type="radio"
                                        name="add_homepage" value="0"> {!! __("SettingPanel::general.no") !!}
                                @else
                                    <input type="radio" name="add_homepage" value="1"
                                           checked> {!! __("SettingPanel::general.yes") !!}
                                    <input type="radio" name="add_homepage"
                                           value="0"> {!! __("SettingPanel::general.no") !!}
                                @endif
                            </div>
                        </li>
                        <li>
                            <label>{!! __('SettingPanel::breadcrumb.add_domain') !!}</label>
                            <div class="form-group">
                                @if(isset($settings['breadcrumb.add_domain']))
                                    <input {!! $settings['breadcrumb.add_domain'] == 1 ? 'checked' : '' !!} type="radio"
                                           name="add_domain" value="1"> {!! __("SettingPanel::general.yes") !!}
                                    <input {!! $settings['breadcrumb.add_domain'] == 0 ? 'checked' : '' !!} type="radio"
                                           name="add_domain" value="0"> {!! __("SettingPanel::general.no") !!}
                                @else
                                    <input type="radio" name="add_domain" value="1"
                                           checked> {!! __("SettingPanel::general.yes") !!}
                                    <input type="radio" name="add_domain"
                                           value="0"> {!! __("SettingPanel::general.no") !!}
                                @endif

                            </div>
                        </li>
                        <li>
                            <label>{!! __('SettingPanel::breadcrumb.menu_slug') !!}</label>
                            <div class="form-group">
                                <select name="menu_slug">
                                    <option value="">{!! __("SettingPanel::general.select") !!}</option>
                                    @foreach($menus as $menu)

                                        @if(isset($settings['breadcrumb.menu_slug']) && $settings['breadcrumb.menu_slug'] == $menu->slug)
                                            <option selected value="{!! $menu->slug !!}">{!! $menu->name !!}</option>
                                        @else
                                            <option value="{!! $menu->slug !!}">{!! $menu->name !!}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li>
                            <label>{!! __('SettingPanel::breadcrumb.string_type.title') !!}</label>
                            <div class="form-group">
                                <select name="string_type">
                                    <option value="">{!! __("SettingPanel::general.select") !!}</option>
                                    @if(isset($settings['breadcrumb.string_type']))
                                        <option
                                            {!! $settings['breadcrumb.string_type'] == "all_word_first_char" ? 'selected' : '' !!}
                                            value="all_word_first_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_first_char') !!}
                                        </option>
                                        <option
                                            {!! $settings['breadcrumb.string_type'] == "first_word_first_char" ? 'selected' : '' !!}
                                            value="first_word_first_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_first_char') !!}
                                        </option>
                                        <option
                                            {!! $settings['breadcrumb.string_type'] == "all_word_all_char" ? 'selected' : '' !!}
                                            value="all_word_all_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_all_char') !!}
                                        </option>
                                        <option
                                            {!! $settings['breadcrumb.string_type'] == "all_word_no_char" ? 'selected' : '' !!}
                                            value="all_word_no_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_no_char') !!}
                                        </option>
                                    @else
                                        <option value="all_word_first_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_first_char') !!}
                                        </option>
                                        <option value="first_word_first_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_first_char') !!}
                                        </option>
                                        <option value="all_word_all_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_all_char') !!}
                                        </option>
                                        <option value="all_word_no_char">
                                            {!! __('SettingPanel::breadcrumb.string_type.all_word_no_char') !!}
                                        </option>
                                    @endif
                                </select>
                            </div>
                        </li>
                        <li>
                            <label>{!! __('SettingPanel::breadcrumb.category_depth') !!}</label>
                            <div class="form-group">
                                @if(isset($settings['breadcrumb.category_depth']))
                                    <input type="number" class="form-control" name="category_depth"
                                           value="{!! $settings['breadcrumb.category_depth'] !!}">
                                @else
                                    <input type="number" class="form-control" name="category_depth" value="-1">
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{!! __("SettingPanel::general.close") !!}</button>
                <button type="button" class="btn btn-primary"
                        onclick="updateBreadcrumb()">{!! __("SettingPanel::general.save") !!}</button>
            </div>
        </div>
    </div>
</div>
