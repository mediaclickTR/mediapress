<?php

namespace Mediapress\Modules\GoogleTagManager;

use Illuminate\Http\Request;
use Mediapress\Models\MPModule;

Class GoogleTagManager extends MPModule
{
    public $name = "GoogleTagManager";
    public $url = "mp-admin/GoogleTagManager";
    public $menus;
    public $description = "Google Tag Manager of Mediapress";
    public $author = "";


    public function fillMenu($menu)
    {

        #region Header Menu > Datasets > Users > Set

        $modules_cols_seo_rows_set = [
            [
                "type" => "submenu",
                "title" => trans('GoogleTagManagerPanel::menu_titles.tagmanager'),
                "url" => route("admin.GoogleTagManager.index")
            ]
        ];
        return dataGetAndMerge($menu, 'header_menu.settings.cols.seo.rows', $modules_cols_seo_rows_set);
    }

}
