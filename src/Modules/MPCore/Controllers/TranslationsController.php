<?php

    namespace Mediapress\Modules\MPCore\Controllers;

    use Mediapress\Http\Controllers\PanelController as Controller;
    use Illuminate\Http\Request;
    use Mediapress\Models\LanguagePart;
    use View;

    class TranslationsController extends Controller
    {

        /**
         * Main function
         * @param Request $request
         * @return mixed
         */
        public function list(Request $request)
        {

            //return dd(CGF::getUngroupedCountriesOfOwner(["Mediapress\\Modules\\Content\\Models\\Website",2]));

            //http://pilot.local/mp-admin/MPCore/CountryGroups/manage?changesOwner=no&selectedOwnerId=1&selectedOwnerType=Mediapress%5CModules%5CContent%5CWebsite
            $params = $request->all();
            $renderable = "Mediapress\\Modules\\MPCore\\AllBuilder\\Renderables\\Translations";
            $renderable_object = new $renderable(["request_params" => $params]);

            return view("MPCorePanel::translations.list", compact("renderable_object"));
        }




    }