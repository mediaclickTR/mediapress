@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">{!! trans('MPCorePanel::setting_sun.settings') !!}</h1>
            @include("MPCorePanel::inc.errors")
            <div class="float-right">
                <a href="{!! route('MPCore.technical.setting_sun.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans('MPCorePanel::general.new_add') !!}</a>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>{!! trans('MPCorePanel::setting_sun.group-title') !!}</th>
                    <th>{!! trans('MPCorePanel::setting_sun.title') !!}</th>
                    <th>{!! trans('MPCorePanel::setting_sun.key') !!}</th>
                    <th>{!! trans('MPCorePanel::setting_sun.value') !!}</th>
                    <th>{!! trans('MPCorePanel::general.actions') !!}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($settingSuns as $setting)
                    <tr>
                        <td>{!! $setting['group'] !!}</td>
                        <td>{!! $setting['title'] !!}</td>
                        <td>{!! $setting['key'] !!}</td>
                        <td title="{!! $setting['value'] !!}">{!! substr($setting['value'],0,30)!!}
                            @if(strlen($setting['value'])>30) ... @endif
                        </td>
                        <td>
                            <select onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans('MPCorePanel::general.selection') !!}</option>
                                <option value="{!! route('MPCore.technical.setting_sun.edit',$setting->id) !!}">{!! trans('MPCorePanel::general.edit') !!}</option>
                                <option value="{!! route('MPCore.technical.setting_sun.delete',$setting->id ) !!}">{!! trans('MPCorePanel::general.delete') !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="float-left">
                {!! $settingSuns->links() !!}
            </div>
            <div class="float-right">
                <select>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
        </div>
@endsection
