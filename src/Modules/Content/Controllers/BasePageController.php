<?php

    namespace Mediapress\Modules\Content\Controllers;

    use Illuminate\Support\Facades\DB;
    use Mediapress\AllBuilder\Foundation\BuilderRenderable;
    use Mediapress\AllBuilder\Foundation\FormStorer;
    use Mediapress\Http\Controllers\PanelController as Controller;
    use Mediapress\Modules\Content\Facades\VariationTemplates;
    use Mediapress\Modules\Content\Models\PageDetail;
    use Mediapress\Modules\Content\Models\Sitemap;
    use Illuminate\Http\Request;
    use Mediapress\Modules\Content\Models\Page;
    use Mediapress\Modules\MPCore\Facades\UserActionLog;
    use Mediapress\Modules\MPCore\Facades\MPCore;
    use Mediapress\Modules\Content\Facades\Auth;
    use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;

    use Mediapress\DataTable\TableBuilderTrait;
    use Yajra\DataTables\Html\Builder;
    use Html;
    use DataTables;


    class BasePageController //extends Controller
    {

        use TableBuilderTrait;
        public const ACCESSDENIED = 'accessdenied';
        public const SITEMAP_ID = 'sitemap_id';
        public const PAGES = "Pages";
        public const CONTENT = "Content";

        /**
         * @return mixed
         */
        public function index($sitemap_id,Builder $builder)
        {
            if (!userAction('message.pages.index', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $sitemap = Sitemap::with("sitemapType")->findOrFail($sitemap_id);
            // Static veya Contracts sitemap type seçili ise edit yada create sayfasına yönlendir
            if ($sitemap->sitemapType->name == 'Static' || $sitemap->sitemapType->name == 'Contacts') {
                $page = $sitemap->pages()->first();
                // Sayfa bulunursa edit sayfasına yönlendir
                if ($page) {
                    return response()->redirectToRoute("Content.pages.edit", [
                        $sitemap_id, $page->id
                    ]);
                }
                return response()->redirectToRoute("Content.pages.create", $sitemap_id);
            }
            $dataTable = $this->columns($builder, $sitemap)->ajax(route('Content.pages.ajax', ['website_id'=>session("panel.website")->id,   self::SITEMAP_ID =>$sitemap_id]));

            return view('ContentPanel::pages.index', compact('sitemap', 'pages', self::SITEMAP_ID,'dataTable'));
        }

        public function create($sitemap_id)
        {
            if (!userAction('message.pages.create', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }

            DB::beginTransaction();
            try {
                $preCreate = Page::preCreate([self::SITEMAP_ID =>$sitemap_id]);
                $sitemap_details = Sitemap::find($sitemap_id)->details;

                foreach ($sitemap_details as $detail){
                    $detail_data = [
                        'page_id'=>$preCreate->id,
                        'language_id'=>$detail->language_id,
                        'country_group_id'=>$detail->country_group_id,
                    ];
                    PageDetail::insert($detail_data);
                }

                DB::commit();

                return redirect(route("Content.pages.edit",[self::SITEMAP_ID =>$sitemap_id, 'id'=>$preCreate->id]));

            } catch (\Exception $e) {
                DB::rollback();
                throw new Exception("Page veya Page Detail Ekleme Sırasında Hata Oluştu.");
            }

        }

        public function edit($sitemap_id, $id, Request $request)
        {
            if (!userAction('message.pages.edit', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }

            $page = Page::with(["sitemap.sitemapType"])->whereId($id)->first();
            $sitemap = Sitemap::find($sitemap_id);
            $languages = sitemapLanguages($sitemap_id);

            $params = $request->all();

            $object = Sitemap::where('id', $sitemap_id)->with(['detail'])->first();
            $sitemap_type = $page->sitemap->sitemapType->name;
            $page_specific = "Page_".$page->id;
            $sitemap_specific = $sitemap_type."_".$page->sitemap->id;
            $renderable_class=AllBuilderEngine::getBuilderClassPath([$page_specific,     self::PAGES], self::CONTENT, "App", $sitemap_specific);
            if( ! $renderable_class[0]){
                return "<strong> <code>" . implode("\n <br/>",$renderable_class[1])."</code></strong>\n<br/>n<br/> denemeleri başarısız oldu.";
            }else{
                $renderable_class=$renderable_class[1];
            }
            $renderable = new $renderable_class(["page" => &$page]);


            if (!is_a($renderable, BuilderRenderable::class)) {
                return "<strong><code>$renderable_class</code></strong> <br/>bir \n<br/><strong><code>" . BuilderRenderable::class . "</code></strong>\n<br/> türevi olmalıydı ama değil.";
            }

            return view('ContentPanel::pages.edit', compact('page', 'id', self::SITEMAP_ID, 'renderable'));
        }

        public function store(Request $request)
        {
            if (!userAction('message.pages.create', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }

            $data = request()->except('_token');
            $insert = Page::firstOrCreate($data);

            // Log
            if ($insert) {
                UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $insert);
            }

            return redirect(route("Content.pages.index"))->with('message', trans('MPCorePanel::general.success_message'));
        }

        public function update($id, Request $request)
        {


            $page = Page::with(["sitemap","sitemap.sitemapType"])->whereId($id)->first();
            if( ! $page){
                throw new Exception("Page kaydı bulunamadı:$id");
            }
            $sitemap = $page->sitemap;
            $languages = sitemapLanguages($sitemap->id);

            $request = Request::capture();

            $object = Sitemap::where('id', $sitemap->id)->with(['detail'])->first();
            $sitemap_type = $page->sitemap->sitemapType->name;
            $page_specific = "Page_".$page->id;
            $sitemap_specific = $sitemap_type."_".$page->sitemap->id;
            $renderable_class=AllBuilderEngine::getBuilderClassPath([$page_specific, self::PAGES], self::CONTENT, "App", $sitemap_specific);
            if( ! $renderable_class[0]){
                return "<strong><code>" . implode("\n<br/>",$renderable_class[1])."</code></strong>\n<br/>n<br/> denemeleri başarısız oldu.";
            }else{
                $renderable_class=$renderable_class[1];
            }

            $renderable = new $renderable_class(["page" => &$page]);

            $storer_class=AllBuilderEngine::getStorerClassPath([$page_specific, self::PAGES], self::CONTENT, "App", $sitemap_specific);
            if( ! $storer_class[0]){
                $storer_class_2 = AllBuilderEngine::getStorerClassPath(["PagesStorer"], self::CONTENT, ["Mediapress","App"], null);
                if(! $storer_class_2[0]){
                    return "<strong><code>" . implode("\n<br/>",array_merge($storer_class[1],$storer_class_2[1]))."</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
                }else{
                    $storer_class=$storer_class_2[1];
                }
                //return "<strong><code>" . implode("\n<br/>",$storer_class[1])."</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
            }else{
                $storer_class=$storer_class[1];
            }

            /** @var FormStorer $storer */
            $storer= new $storer_class($renderable, $request);


            $store=$storer->store();
            dump($storer->getLogs());
            dump($storer->getErrors());
            dump($storer->getWarnings());

            return dd($store);

            /*$id = $request->id;
            $data = request()->except('_token');
            $update = Page::updateOrCreate(['id' => $id], $data);

            // Log
            if ($update) {
                UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $update);
            }

            return redirect(route('Content.pages.index'))->with('message', trans('MPCorePanel::general.success_message'));
            */
        }

        public function delete($sitemap_id, $id)
        {
            if (!userAction('message.pages.delete', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }

            $page = Page::find($id);
            if ($page) {
                $page->delete();
                UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $page);
            }
            return redirect(route('Content.pages.index', $sitemap_id))->with('message', trans('MPCorePanel::general.success_message'));
        }
    }