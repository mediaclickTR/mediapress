<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfilesTable extends Migration
{
    public const MFILES = 'mfiles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable(self::MFILES)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::MFILES, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('mdisk_id')->nullable();
                $table->integer('mfolder_id')->nullable();
                $table->integer('mfile_id')->nullable();
                $table->string('name',500);
                $table->string('usecase')->default("original");
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::MFILES)) {
            Schema::dropIfExists(self::MFILES);
        }
    }
}
