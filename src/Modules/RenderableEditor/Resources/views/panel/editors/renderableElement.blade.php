<div class="modal fade" id="renderableElement" tabindex="-2" role="dialog" aria-labelledby="renderableElement2"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <form action="javascript:saveRenderableElement();">
                <input type="hidden" name="id">
                <input type="hidden" name="type">
            <div class="modal-header">
                <h5 class="modal-title" id="renderableElementTitle">-</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>