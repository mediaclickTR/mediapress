<?php

namespace Mediapress\Modules\Content\Models;


use Mediapress\Models\Eski\PageDetailPropertyDetail;
use Mediapress\Modules\Auth\Auth;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\CountryGroupCountry;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\HasMFiles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Traits\IsDetail;

class PageDetail extends BaseModel
{
    use HasMFiles;
    use SoftDeletes;
    use IsDetail;

    protected $table = 'page_details';

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "page_id",
        "language_id",
        "country_group_id",
        "manual_meta",
        "name",
        "slug",
        "detail",
        "status",

    ];


    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function parent()
    {
        return $this->belongsTo(Page::class);
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }


    public function properties()
    {
        return $this->hasMany(PageDetailPropertyDetail::class);
    }

    public function extras()
    {
        return $this->hasMany(PageDetailExtra::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'model')->withTrashed()->withDefault(['url' => '/'])->whereIn('type', ['original', 'reserved'])->orderBy('type', 'asc');
    }

    public function urls()
    {
        return $this->morphOne(Url::class, 'model')->withDefault(['url' => '/']);
    }


    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value)) {
            return $extra->value;
        }

        return null;
    }

    public function getParentIdAttribute()
    {
        return $this->page_id;
    }


    public function setSlugAttribute($value)
    {
        if (mb_substr($value, 0, 1) != '/') {
            $this->attributes['slug'] = '/' . $value;
        } else {
            $this->attributes['slug'] = $value;
        }

    }

    public function getDetailAttribute($value)
    {
        return supFront($value, 'detail', $this);
    }

    public function getNameAttribute($value)
    {
        return supFront($value, 'name', $this);
    }

}
