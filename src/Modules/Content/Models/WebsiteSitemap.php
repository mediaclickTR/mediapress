<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteSitemap extends Model
{
    protected $table = "sitemap_website";

    public $timestamps = false;

    public $fillable = ["website_id","sitemap_id"];
}
