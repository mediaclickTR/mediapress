<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGroupsTable extends Migration
{
    public const COUNTRY_GROUPS = 'country_groups';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable(self::COUNTRY_GROUPS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::COUNTRY_GROUPS, function (Blueprint $table) {
                $table->increments('id');
                $table->string('code', 2);
                $table->string('title', 190);
                $table->string('owner_type', 300);
                $table->integer('owner_id');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::COUNTRY_GROUPS);
    }
}
