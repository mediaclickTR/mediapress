<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Modules\Comment\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Entity\Models\User;

class Comment extends Model
{
    use SoftDeletes;
    protected $table = "comments";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function structure()
    {
        return $this->belongsTo(CommentStructure::class);
    }

    public function parent()
    {
        return $this->belongsTo(Comment::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    public function model()
    {
        return $this->morphTo(__FUNCTION__, 'model_class', 'model_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
