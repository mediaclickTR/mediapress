<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Foundation\BackupData;
use Mediapress\Modules\Content\Models\CriteriaPage;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Facades\MetaEngine;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\AllBuilder\Foundation\FormStorer;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\Content\Facades\Auth;
use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;

use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\Modules\MPCore\Models\Url;
use mysql_xdevapi\Exception;
use Yajra\DataTables\Html\Builder;
use Html;
use DataTables;


class PageController extends Controller
{

    use TableBuilderTrait;
    public const WEBSITE = "website";
    public const SITEMAP_ID = 'sitemap_id';
    public const SITEMAP = 'sitemap';
    public const PAGE_ID = 'page_id';
    public const LANGUAGE_ID = 'language_id';
    public const COUNTRY_GROUP_ID = 'country_group_id';
    public const WEBSITE_ID = 'website_id';
    public const ACCESSDENIED = 'accessdenied';


    /**
     * @return mixed
     */
    public function index($sitemap_id, Builder $builder)
    {

        $sitemap = Sitemap::with(["sitemapType", "detail"])->findOrFail($sitemap_id);

        if (!activeUserCan([
            "content.websites.website" . $sitemap->website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.index",
            "content.websites.website" . $sitemap->website->id . ".sitemaps.pages.index",
            "content.websites.sitemaps.pages.index",
        ])) {
            return rejectResponse();
        }

        // Static veya Contracts sitemap type seçili ise edit yada create sayfasına yönlendir
        if ($sitemap->sitemapType->sitemap_type_type == 'static') {
            $page = $sitemap->pages()->first();
            if ($page) {
                return response()->redirectToRoute("Content.pages.edit", [
                    $sitemap_id, $page->id
                ]);
            }
            return response()->redirectToRoute("Content.pages.create", $sitemap_id);
        }


        $routeParams = ['sitemap_id' => $sitemap_id];
        $dataTableParams = [self::WEBSITE_ID => session("panel.website")->id, self::SITEMAP_ID => $sitemap_id];
        $crumbs = [];

        if ($sitemap->sitemap_id) {
            $parent_page_id = request()->page_id;

            $parent_sitemap = Sitemap::with(["detail"])->findOrFail($sitemap->sitemap_id);


            $crumbs[] = [
                "key" => "sitemap_pages_index",
                "text" => $parent_sitemap->detail->name,
                "icon" => "sitemap",
                "href" => route('Content.pages.index', ['sitemap_id' => $parent_sitemap->id])
            ];

            if ($parent_page_id && is_numeric($parent_page_id)) {
                $parent_page = Page::with(["detail"])->find($parent_page_id);

                if ($parent_page) {

                    $dataTableParams['page_id'] = $routeParams['page_id'] = $parent_page_id;
                    $crumbs[] = [
                        "key" => "page_pages_index",
                        "text" => $parent_page->detail->name . ": " . $sitemap->detail->name,
                        "icon" => "files-o",
                        "href" => route('Content.pages.index', ['sitemap_id' => $parent_sitemap->id])
                    ];

                }

            } else {
                $crumbs[] = [
                    "key" => "sitemap_pages_index",
                    "text" => $sitemap->detail->name . "",
                    "icon" => "sitemap",
                    "href" => "javascript:void(0);"
                ];
            }

        } else {
            $crumbs[] = [
                "key" => "sitemap_pages_index",
                "text" => $sitemap->detail->name . "",
                "icon" => "sitemap",
                "href" => "javascript:void(0);"
            ];
        }


        $breadcrumb = Content::getBreadcrumb($crumbs);

        $categories = collect();
        if ($sitemap->category == 1) {
            $categories = $sitemap->categories()
                ->where('status', 1)
                ->whereHas('detail')
                ->orderBy('lft')
                ->get();
        }

        $createRoute = route('Content.pages.create', $routeParams);
        $dataTable = $this->columns($builder, $sitemap)->ajax(route('Content.pages.ajax', $dataTableParams));




        return view('ContentPanel::pages.index', compact('categories', self::SITEMAP,  self::SITEMAP_ID, 'dataTable', 'breadcrumb', 'createRoute'));
    }


    public function create($sitemap_id)
    {


        DB::beginTransaction();
        try {
            $pageId = request()->page_id;
            $createParams = [self::SITEMAP_ID => $sitemap_id];
            if ($pageId && is_numeric($pageId)) {
                $createParams['page_id'] = $pageId;
            }
            $preCreate = Page::preCreate($createParams);
            $sitemap = Sitemap::find($sitemap_id);
            $sitemap_details = $sitemap->details;

            if (!activeUserCan([
                "content.websites.website" . $sitemap->website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.create",
                "content.websites.website" . $sitemap->website->id . ".sitemaps.pages.create",
                "content.websites.sitemaps.pages.create",
            ])) {
                DB::rollback;
                return rejectResponse();
            }

            foreach ($sitemap_details as $detail) {
                $detail_data = [
                    self::PAGE_ID => $preCreate->id,
                    self::LANGUAGE_ID => $detail->language_id,
                    self::COUNTRY_GROUP_ID => $detail->country_group_id,
                ];
                PageDetail::create($detail_data);
            }

            DB::commit();

            return redirect(route("Content.pages.edit", [self::SITEMAP_ID => $sitemap_id, 'id' => $preCreate->id, 'isNew' => "yes"]));

        } catch (\Exception $e) {
            DB::rollback();
            $trace = $e->getTrace();
            foreach ($trace as $trace_) {
                if (isset($trace_["file"]) && $trace_["file"] == __FILE__) {
                    dump("Hata " . __FILE__ . " dosyasında " . $trace_["line"] . ". satırda tetiklendi.");
                    break;
                }
            }
            dump("Page | PageDetail oluşturulurken hata oluştu. Rollback yapıldı.");
            dump("Exception " . __FILE__ . " dosyasında " . (__LINE__ + 1) . ". satırda fırlatıldı.");
            throw $e;
        }

    }


    public function edit($sitemap_id, $id, Request $request)
    {

        $page = Page::whereId($id)->whereSitemapId($sitemap_id)->with(["sitemap", "extras"])->first();

        if (!$page) {
            throw new \Exception("Page kaydı bulunamadı:$id");
        }

        $sitemap = $page->sitemap;
        $website = $page->sitemap->website;
        $permissions = [
            "content.websites.website" . $website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.update",
            "content.websites.website" . $website->id . ".sitemaps.pages.update",
            "content.websites.sitemaps.pages.update",
        ];
        if ($request->has("isNew") && $request->isNew == "yes" && $page->status == 3) {
            $permissions = [
                "content.websites.website" . $website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.create",
                "content.websites.website" . $website->id . ".sitemaps.pages.create",
                "content.websites.sitemaps.pages.create",
            ];
        }

        if (!activeUserCan($permissions)) {
            return rejectResponse();
        }

        if ($website->id != session("panel.website.id")) {

            session(["panel.website" => $website]);

            return redirect()->route('panel.dashboard');
            //throw new \UnexpectedValueException("Yönetim panelinde website oturumu değiştirilmiş. Bir sayfa ya da sayfa yapısı, sadece oturumda seçilen websiteye aitse düzenlenebilir.");
        }

        $params = $request->all();
        $renderable_class = Content::getPageRenderableClassPath($page);


        Content::createUnexistentDetailsOfParent($page);

        $renderable = new $renderable_class(["page" => &$page, self::WEBSITE => $website, "request_params" => $params]);

        $metaset = json_encode([]); //ContentEngine::

        $routeParams = ['sitemap_id' => $sitemap_id];
        if ($sitemap->sitemap_id) {
            $parent_page_id = $page->page_id;
            $routeParams['page_id'] = $parent_page_id;
            $parent_sitemap = Sitemap::with(["detail"])->findOrFail($sitemap->sitemap_id);


            $crumbs[] = [
                "key" => "sitemap_pages_index",
                "text" => $parent_sitemap->detail->name,
                "icon" => "sitemap",
                "href" => route('Content.pages.index', ['sitemap_id' => $parent_sitemap->id])
            ];

            if ($parent_page_id && is_numeric($parent_page_id)) {
                $parent_page = Page::with(["detail"])->find($parent_page_id);

                if ($parent_page) {

                    $dataTableParams['page_id'] = $routeParams['page_id'] = $parent_page_id;
                    $crumbs[] = [
                        "key" => "page_pages_index",
                        "text" => $parent_page->detail->name . ": " . $sitemap->detail->name,
                        "icon" => "files-o",
                        "href" => route('Content.pages.index', ['sitemap_id' => $sitemap->id, 'page_id' => $parent_page_id])
                    ];

                }

            } else {
                $crumbs[] = [
                    "key" => "sitemap_pages_index",
                    "text" => $sitemap->detail->name . "",
                    "icon" => "sitemap",
                    "href" => route('Content.pages.index', ['sitemap_id' => $sitemap->id])
                ];
            }

        } else {
            $crumbs[] = [
                "key" => "sitemap_pages_index",
                "text" => $sitemap->detail->name . "",
                "icon" => "sitemap",
                "href" => route('Content.pages.index', ['sitemap_id' => $sitemap->id])
            ];
        }

        $crumbs[] = [
            "key" => "page_editor",
            "text" => $page->detail->name ?? "<sayfa>",
            "icon" => "file",
            "href" => "javascript:void(0);"
        ];


        $breadcrumb = Content::getBreadcrumb($crumbs);
        $createRoute = route('Content.pages.create', $routeParams);

        return view('ContentPanel::pages.edit', compact('page', 'id', self::SITEMAP_ID, 'renderable', 'metaset', 'breadcrumb', 'createRoute'));
    }


    public function update($sitemap_id, $id)
    {

        $page = Page::with([self::SITEMAP, "sitemap.sitemapType", 'extras'])->whereId($id)->first();

        if (!$page) {
            throw new \InvalidArgumentException("Page kaydı bulunamadı:$id");
        }
        $request = Request::capture();

        $website = $page->sitemap->website;

        $permissions = $updatePermissions = [
            "content.websites.website" . $website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.update",
            "content.websites.website" . $website->id . ".sitemaps.pages.update",
            "content.websites.sitemaps.pages.update",
        ];

        $createPermissions = [
            "content.websites.website" . $website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.create",
            "content.websites.website" . $website->id . ".sitemaps.pages.create",
            "content.websites.sitemaps.pages.create",
        ];

        $userCanCreateOnly = (activeUserCan($createPermissions) && !activeUserCan($updatePermissions));

        $createStatus = false;
        if($page->status == 3 && request()->get('isNew') == 'yes') {
            $createStatus = true;
            $permissions = $createPermissions;
        }


        $renderable_class = Content::getPageRenderableClassPath($page);

        $website = $page->sitemap->website;

        if ($website->id != session("panel.website.id")) {
            session(["panel.website" => $website]);

            return redirect()->route('panel.dashboard');

            //throw new \UnexpectedValueException("Yönetim panelinde website oturumu değiştirilmiş. Bir sayfa ya da sayfa yapısı, sadece oturumda seçilen websiteye aitse düzenlenebilir.");
        }

        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class(["page" => &$page, self::WEBSITE => $website]);

        $storer_class = Content::getPageStorerClassPath($page);

        /** @var FormStorer $storer */
        $storer = new $storer_class($renderable, $request);

        //return dd("Bitti");

        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $storer->setMainObjectData($page);
        //-------------------------------------------------------------

        $store = $storer->store();

        $backup = new BackupData();
        $backup->store($storer);
        //-------------------------------------------------------------

        if ($createStatus) {
            UserActionLog::create(__CLASS__ . "@" . 'create', $page);
        } else {
            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $page);
        }

        if (!$store) {
            return redirect()->back()->withErrors($storer->getErrors())->withInput();
        }

        if($userCanCreateOnly){
            return redirect()->to(route("Content.pages.index",[self::SITEMAP_ID => $sitemap_id]))->with('message', trans('MPCorePanel::general.success_message'));
        }


        if(session('panel.pageCreateSuccessPopupType') == 1) {
            $redirectRoute = route('Content.pages.index', ['sitemap_id' => $page->sitemap_id]);
        } elseif(session('panel.pageCreateSuccessPopupType') == 3) {
            $redirectRoute = route('Content.pages.create', ['sitemap_id' => $page->sitemap_id, 'page_id' => $page->page_id]);
        } else {
            $redirectRoute = route("Content.pages.edit",[self::SITEMAP_ID => $sitemap_id, 'id' => $page->id]);
        }

        return redirect()->to($redirectRoute)
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate')
            ->with(['pageCreate' => true]);
    }


    public function delete($sitemap_id, $id)
    {


        $page = Page::with(["details", "detail.urls"])->find($id);
        $parentId = $page->page_id;

        if (!activeUserCan([
            "content.websites.website" . $page->sitemap->website->id . ".sitemaps.sitemap" . $sitemap_id . ".pages.delete",
            "content.websites.website" . $page->sitemap->website->id . ".sitemaps.pages.delete",
            "content.websites.sitemaps.pages.delete",
        ])) {
            return rejectResponse();
        }

        if ($page && $page->delete()) {
            foreach ($page->details as $detail) {
                $detail->delete();
            }
            UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $page);
        }

        $route = route('Content.pages.index', ['sitemap_id' => $sitemap_id]);
        if($parentId) {
            $route = route('Content.pages.index', ['sitemap_id' => $sitemap_id, 'page_id' => $parentId]);
        }

        return redirect($route)->with('message', trans('MPCorePanel::general.success_message'));
    }

    public function batchUpdate($sitemap_id)
    {
        $sitemap = Sitemap::find($sitemap_id);
        return view('ContentPanel::pages.batch_update', compact('sitemap'));
    }

    public function getPagesForBatchUpdate($sitemap_id)
    {
        $sitemap = Sitemap::find($sitemap_id);
        $pages = Page::where('sitemap_id', $sitemap_id)
            ->with('detail')
            ->orderBy('order')
            ->get();

        $return = [];

        foreach ($pages as $page) {
            $return[] = [
              'id' => $page->id,
              'status' => $page->status * 1,
              'order' => $page->order * 1,
              'name' => $page->detail->name,
              'category_name' => $page->category() ? $page->category()->detail->name : ''
            ];
        }

        return response()->json(['categoryExist' => $sitemap->category == 1, 'pages' => $return]);
    }

    public function updatePageForBatchUpdate(Request $request)
    {
        $pageId = $request->get('id');

        $temp = [$request->get('type') => $request->get('value')];

        Page::find($pageId)->update($temp);
    }

    public function getPageCriteriaValues(Request $request)
    {
        $page_ids = $request->get("page_ids", []);
        $data = [];
        foreach ($page_ids as $page_id) {
            $data[$page_id] = CriteriaPage::where(self::PAGE_ID, $page_id)->get()->pluck('criteria_id')->toArray();
        }
        return $data;
    }

    public function getPagePropertyValues(Request $request)
    {
        $page_ids = $request->get("page_ids", []);
        $data = [];
        foreach ($page_ids as $page_id) {
            $data[$page_id] = \DB::table('page_property')->where(self::PAGE_ID, $page_id)->get()->pluck('value', 'property_id')->toArray();
        }
        return $data;
    }

    public function removeDetail($pdid)
    {
        $model = PageDetail::find($pdid);
        $delete = $model->delete();
        if (!$delete) {
            return [false, $model];
        }
        return [true, $model];
    }

    public function restoreDetail($pdid)
    {
        $model = PageDetail::withTrashed()->find($pdid);
        $do = $model->restore();
        if (!$do) {
            return [false, null];
        }
        return [true, PageDetail::find($pdid)->with("url")];
    }


    public function variationStatus(Page $page)
    {
        return response()->json(VariationTemplates::variationStatus($page));
    }

    public function checkPageDetailUrl($page_id, Request $request)
    {

        $result = [
            "request_params" => $request->all(),
            "status" => false,
            "info" => [
                "url_head" => null,
                "applicable_slug" => null,
                "possible_url" => null,
                "current_url_model" => null,
            ],
            "applicable_slug" => null, //again
            "messages" => [],
            "warnings" => [],
            "errors" => []
        ];

        try {
            // pick parameters from request
            $cg_id = $request->get(self::COUNTRY_GROUP_ID) * 1;
            $lng_id = $request->get(self::LANGUAGE_ID) * 1;
            $text2slug = $request->get("text2slug") ?? null;
            $slug2slug = $request->get("slug2slug") ?? null;

            // get page object with details
            $page = Page::whereId($page_id * 1)->with(["details", "sitemap.details"])->first();
            $page_detail = $page->details->where(self::COUNTRY_GROUP_ID, $cg_id)->where(self::LANGUAGE_ID, $lng_id)->first();
            // set info
            $result["info"]["current_url_model"] = $page_detail->url;


            if (is_null($page)) {
                throw new \InvalidArgumentException(trans("ContentPanel::page.page_not_found_by_id", ["id" => $page_id]));
            }

            //we'll use its url:
            $sitemap_detail = $page->sitemap->details->where(self::COUNTRY_GROUP_ID, $cg_id)->where(self::LANGUAGE_ID, "$lng_id")->first();

            if (is_null($sitemap_detail)) {
                throw new \InvalidArgumentException(trans("ContentPanel::sitemap.detail_not_found_by_id", ["id" => $page_id]));
            }

            //url which we'll take url string to build one for page detail
            $url = $sitemap_detail->url;
            //set info
            $result["info"]["url_head"] = $url->url;

            //nothing sent as a slug? really? this is a page folks, not detail of homepage sitemap.
            if (is_null($text2slug) && is_null($slug2slug)) {
                throw new \InvalidArgumentException(trans("MPCorePanel::general.status_messages.at_least_one_should_be_provided", ["mass" => "text2slug, slug2slug"]));
            }

            //text2slug has higher priority.
            //sluggified state of this will be page detail's slug:
            $slugify_this = $text2slug ?? $slug2slug;

            $slug = \Str::slug(Str::ascii($slugify_this));

            $url_full = rtrim($url->url, "/") . "/" . $slug;

            //check if one already exists

            /*if( ! is_null($check)){

            }*/


        } catch (\Exception $e) {
            $result["status"] = false;
            $result["errors"][] = $e->getMessage() . " (file:\"" . $e->getFile() . "\" line:" . $e->getLine() . ")";
        } finally {
            return response()->json($result);
        }


    }

    public function pageCreateSuccessPopupType()
    {
        $type = request()->get('type');

        session(['panel.pageCreateSuccessPopupType' => $type]);
    }

    private function getValidUrlString($url_string, $num = null)
    {
        if (!is_null($num)) {
            //$num = ($num+0)++;
        }
        $check = Url::where("url", $url_string)->first();
    }


    #region Helpers

    public function checkUrl(Request $request)
    {
        $website_id = $request->get(self::WEBSITE_ID) * 1;
        $page_id = $request->get(self::PAGE_ID) * 1;
        $language = $request->get("language") * 1;
        $country = $request->has("country") ? $request->get("country") : null;

        $slug = $request->get("slug");
        $cg = CountryGroup::find($country);
        $pd = PageDetail::where(self::PAGE_ID, $page_id)->where(self::LANGUAGE_ID, $language)->where(self::COUNTRY_GROUP_ID, $cg ? $cg->id : null)->first();
        if ($pd == null) {
            $pd = [
                "type" => PageDetail::class,
                "page" => Page::find($page_id),
                self::SITEMAP => Page::find($page_id)->sitemap,
                self::WEBSITE => Page::find($page_id)->sitemap->websites->first()
            ];
        }

        if ($website_id == 0) {
            $website_id = isset($pd[self::WEBSITE]) ? $pd[self::WEBSITE]->id : $pd->page->sitemap->websites()->first()->id;
        }

        $pattern = URLEngine::generateUrlString($pd, $cg, $language, $slug);
        return response()->json(URLEngine::checkPattern($website_id, $pattern["pattern"], $pattern["slug"]));
    }


    private function createUrl($websiteIds = [], $language, $slug, $model, $country = null)
    {
        $url = "/$language";
        if ($country) {
            $url .= "/$country";
        }
        $url .= "/$slug";
        foreach ($websiteIds as $wid) {
            $url = $this->checkUrlRecursive($wid, $url);
            Url::create([self::WEBSITE_ID => $wid, "url" => $url, "model_type" => get_class($model), "model_id" => $model->id]);
        }
    }

    private function checkUrlRecursive($websiteId, $language = null, $country = null, $url = null, $i = 1)
    {
        $pattern = '';
        $website = Website::find($websiteId);
        $website_default_lang = $website->defaultLanguage();

        if ($website->use_deflng_code == 0) {
            if ($website_default_lang->code == "$language" && $country == null) {
                $pattern = '';
            } else {
                if ($country == null) {
                    $pattern = $language;
                } else {
                    $pattern = "/" . $language . "_" . $country;
                }
            }
        } else {
            if ($country == null) {
                $pattern = $language;
            } else {
                $pattern = "/" . $language . "_" . $country;
            }
        }

        if ($url == "null") {
            return $pattern;
        }
        $query = $i > 1 ? $pattern . $url . "-$i" : $pattern . $url;

        $urlModel = Url::where(['url' => $query, self::WEBSITE_ID => $websiteId])->first();

        if ($urlModel != null) {
            return $this->checkUrlRecursive($websiteId, $language, $country, $url, ++$i);
        } else {
            if ($i > 1) {
                return $pattern . $url . "-$i";
            } else {
                return $pattern . $url;
            }
        }
    }
    #endregion

}
