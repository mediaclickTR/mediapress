<?php

namespace Mediapress\DataTable\Tables;

use Html;
use Mediapress\DataTable\DataTableInterface;
use Mediapress\Modules\Auth\Models\Role;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class PanelRoleController implements DataTableInterface
{
    const TITLE = 'title';
    const FOOTER = 'footer';

    public function columns(Builder $builder)
    {
        return $builder->columns([
            [
                'data' => 'name', 'name' => 'name', self::TITLE => trans("MPCorePanel::general.slug"), self::FOOTER => trans("MPCorePanel::general.role")
            ],
            [
                'data' => 'title', 'name' => 'title', self::TITLE => trans("MPCorePanel::general.title"), self::FOOTER => trans("MPCorePanel::general.role")
            ],
            [
                'data' => 'created_at', 'name' => 'created_at', self::TITLE => trans("MPCorePanel::general.created_at"), self::FOOTER => trans("MPCorePanel::general.created_at")
            ], [
                'data' => 'updated_at', 'name' => 'updated_at', self::TITLE => trans("MPCorePanel::general.updated_at"), self::FOOTER => trans("MPCorePanel::general.updated_at")
            ]
        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);

    }

    public function ajax($website_id = null)
    {
        $roles = Role::where("id", "<>", 1)->where("status", "<>", 3);

        return Datatables::eloquent($roles)
            ->addColumn('action', function ($role) {
                return
                    '<a href="'.route('Auth.roles.edit',  $role->id).'" class="m-1" title="' . trans("MPCorePanel::general.edit") . '"><span class="fa fa-pen"></span></a>'.
                    '<a href="'.route('Auth.roles.abilities', $role->id).'" class="m-1" title="' . trans("MPCorePanel::general.abilities") . '"><span class="fa fa-check-circle"></span></a>'.
                    '<a href="'.route('Auth.roles.delete', $role->id).'" class="m-1 needs-confirmation" title="' . trans("MPCorePanel::general.delete") . '" data-dialog-text="'.trans("MPCorePanel::general.action_cannot_undone").'" data-dialog-cancellable="1" data-dialog-type="warning" ><span class="fa fa-trash"></span></a>';
            })
            ->setRowId('tbl-{{$id}}')->make(true);
    }
}
