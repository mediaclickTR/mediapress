<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 15.11.2018
     * Time: 09:59
     */

    namespace Mediapress\Modules\MPCore\DataSources;
    use Mediapress\Modules\MPCore\Facades\CountryGroups as CGF;


    use Mediapress\Foundation\DataSource;

    class CountriesUngroupedDS extends DataSource
    {
        public function getData(){
            $owner = $this->params["owner"];
            $owner_id = $this->params["owner_id"];

            $results = CGF::getUngroupedCountriesOfOwner([$owner,$owner_id]);

            $pluck=($this->params["pluck"] ?? false) && $this->params["pluck"];
            if($pluck){
                $key = $this->params["pluck_key"] ?? "id";
                $value = $this->params["pluck_column"] ?? "id";
                return $results->pluck($value,$key);
            }

            return $results;

        }
    }