@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="dashboard">
        @include("MPCorePanel::inc.errors")
        <div class="page-top">
            <div class="title">
                {!! trans("MPCorePanel::analytics.detailedAnalytics") !!}
            </div>
            <div class="filter">
                <select class="nice" onchange="locationOnChange(this.value);">
                    @php($request = request())
                    <option @if($request->days)  @if($request->days=='0') selected @endif @endif value="{!! url()->current() !!}?days='0'">{!! trans("MPCorePanel::analytics.today") !!}</option>
                    <option @if($request->days) @if($request->days==1) selected @endif @endif value="{!! url()->current() !!}?days=1">{!! trans("MPCorePanel::analytics.yesterday") !!}</option>
                    <option @if($request->days==7 || !$request->days) selected @endif value="{!! url()->current() !!}?days=7">{!! trans("MPCorePanel::analytics.week") !!}</option>
                    <option @if($request->days) @if($request->days==30) selected @endif @endif value="{!! url()->current() !!}?days=30">{!! trans("MPCorePanel::analytics.month") !!}</option>
                    <option @if($request->days) @if($request->days==90) selected @endif @endif value="{!! url()->current() !!}?days=90">{!! trans("MPCorePanel::analytics.three-month") !!}</option>
                    <option @if($request->days) @if($request->days==365) selected @endif @endif value="{!! url()->current() !!}?days=365">{!! trans("MPCorePanel::analytics.year") !!}</option>
                </select>
            </div>
        </div>
        <div class="content">
            <div class="pro-lists">
                <div class="col-md-12 pa">
                    <div class="lang-box">
                        <form action="#">
                            <div class="item" style="background: #f6f6f6 !important;" >
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.pageAnalytics") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="pageAnalytics"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.date") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pageTitle") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pluralPageViews") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.singularPageViews") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.timeOnPage") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($pageAnalytics as $page)
                                                <tr>
                                                    <td>{!! $page['date'] !!}</td>
                                                    <td>{!! $page['pageTitle'] !!}</td>
                                                    <td>{!! $page['pluralPageViews'] !!}</td>
                                                    <td>{!! $page['singularPageViews'] !!}</td>
                                                    <td>{!! $page['timeOnPage'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.traffic-sources") !!}</span>

                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="trafficSources"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.source") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.medium") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pageviews") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.duration") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.exits") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($trafficSources as $source)
                                                <tr>
                                                    <td>{!! $source['source'] !!}</td>
                                                    <td>{!! $source['medium'] !!}</td>
                                                    <td>{!! $source['sessions'] !!}</td>
                                                    <td>{!! $source['pageviews'] !!}</td>
                                                    <td>{!! $source['duration'] !!}</td>
                                                    <td>{!! $source['exits'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.countries") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="countries"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.country") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.click") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($countries as $country)
                                                <tr>
                                                    <td>{!! $country['country'] !!}</td>
                                                    <td>{!! $country['click'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.search-engine-keywords") !!}</span>

                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="keywords"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.keyword") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($searchEngineKeywords as $keyword)
                                                <tr>
                                                    <td>{!! $keyword['keyword'] !!}</td>
                                                    <td>{!! $keyword['sessions'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.browsers-and-systems") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="browsers"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.operatingSystem") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.operatingSystemVersion") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.browser") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($browsersAndSystems as $browser)
                                                <tr>
                                                    <td>{!! $browser['operatingSystem'] !!}</td>
                                                    <td>{!! $browser['operatingSystemVersion'] !!}</td>
                                                    <td>{!! $browser['browser'] !!}</td>
                                                    <td>{!! $browser['sessions'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .nice ul {
            z-index:999 !important;
        }
        tr td {
            text-align:center;
        }

    </style>
@endpush

@push('scripts')
    <script src="{!! asset('vendor/mediapress/js/zingchart.min.js') !!}"></script>
    <script>
        $('a').click(function (e) {
            e.preventDefault();
        });

        var pageAnalytics = {
            "type": "line",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($pageAnalytics as $page)
                        "{!! $page['date'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($pageAnalytics as $page)
                            {!! $page['pluralPageViews'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.pluralPageViews") !!}"
                },
                {
                    "values": [
                        @foreach($pageAnalytics as $page)
                            {!! $page['singularPageViews'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.singularPageViews") !!}"
                }
            ]
        };
        zingchart.render({
            id: "pageAnalytics",
            data: pageAnalytics,
            height: "300",
            width: "100%"
        });

        var trafficSources = {
            "type": "bar",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($trafficSources as $source)
                        "{!! $source['source'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($trafficSources as $source)
                            {!! $source['pageviews'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.pageviews") !!}"
                },
                {
                    "values": [
                        @foreach($trafficSources as $source)
                            {!! $source['exits'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.exits") !!}"
                }
            ]
        };
        zingchart.render({
            id: "trafficSources",
            data: trafficSources,
            height: "300",
            width: "100%"
        });

        var countries = {
            "type": "area",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($countries as $country)
                        "{!! $country['country'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($countries as $country)
                            {!! $country['click'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.click") !!}"
                }
            ]
        };
        zingchart.render({
            id: "countries",
            data: countries,
            height: "300",
            width: "100%"
        });

        var keywords = {
            "type": "line",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($searchEngineKeywords as $keyword)
                        "{!! $keyword['keyword'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($searchEngineKeywords as $keyword)
                        {!! $keyword['sessions'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.sessions") !!}"
                }
            ]
        };
        zingchart.render({
            id: "keywords",
            data: keywords,
            height: "300",
            width: "100%"
        });

        var browsers = {
            "type": "area",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($browsersAndSystems as $browser)
                        "{!! $browser['browser'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($browsersAndSystems as $browser)
                        {!! $browser['sessions'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.sessions") !!}"
                }
            ]
        };
        zingchart.render({
            id: "browsers",
            data: browsers,
            height: "300",
            width: "100%"
        });

    </script>
@endpush
