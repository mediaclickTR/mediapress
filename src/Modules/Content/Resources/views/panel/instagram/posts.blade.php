<div class="row" >

    @php($postCounter = 0)
    @foreach($instagram as $post)
        <div class="col-md-6 post" style="display:flex; min-height: 320px; ">
            <div class="col-md-6 align-self-center">
                @if($post["type"] == "carousel")
                    <div id="carouselExampleControls{!! $postCounter !!}" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($post["carousel_media"] as $child)
                                <div class="carousel-item {!! $loop->iteration == 1 ? "active": "" !!}">
                                    <img src="{!! $child["media_url"] !!}" class="d-block w-100" alt="...">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                @elseif($post["type"] == "video")
                    <video width="250" controls>
                        <source src="{!! $post["media_url"] !!}" type="video/mp4">
                        Lütfen "Chrome" Tarayıcı İle Deneyiniz
                    </video>
                @else
                    <div class="col-md-6 align-self-center">
                        <img class="post-photo" src="{!! $post["thumbnail_photo"] !!}" alt="">
                    </div>
                @endif
            </div>
            <div class="col-md-6 align-self-center text-center">
                <p> <i class="far fa-heart"></i> {!! $post["likes"] !!} Beğenme </p>
                <p> <i class="far fa-comments"></i> {!! count($post["comments"]) !!} Yorum </p>
                <p>
                     <button type="button" class="btn btn-outline-success float-bottom save" data-id="{!! $post["id"] !!}">
                        <i class="fas fa-download"></i>İndir
                    </button>
                    <a href="{!! $post["post_url"] !!}" target="_blank">
                        <button type="button" class="btn btn-outline-secondary float-bottom">
                        <i class="fas fa-share"></i>
                        </button>
                    </a>

                </p>
            </div>
        </div>
    @php($postCounter ++)
    @endforeach
    <script>
        $(".save").click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            var button = $(this);

            $.post( "{!! route("Content.instagram.savePost") !!}", {
                "post": id,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
               if (data == "true"){
                   console.log("ok");
                   $(button).attr("disabled", true);
                   button.removeClass("btn-outline-success");
                   button.addClass("btn-success");
                   button.children(" i ").addClass("fa-check-circle");
                   button.children(" i ").removeClass("fa-download");
                   $(button).contents().filter(function() {
                       return this.nodeType == 3 && this.textContent.trim();
                   })[0].textContent = 'Kaydedildi';
               } else {
                   alert("hata");
               }
            });
        });
    </script>
</div>
