<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Foundation\ClusterArray;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Facades\CriteriaEngine;
use Mediapress\Modules\Content\Facades\PropertyEngine;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;

class PropertyController extends Controller
{

    public function index(Sitemap $sitemap)
    {

        $websiteId = session("panel.website.id");

        if(! activeUserCan([
            "content.websites.website$websiteId.sitemaps.properties.index",
            "content.websites.sitemaps.properties.index"
        ])
        ){
            return rejectResponse();
        }

        $cluster = new ClusterArray();
        $cluster->table_id = 'property_id';
        $default_language = session("panel.website")->defaultLanguage()->id;
        $nestable = $cluster->buildFromDB($sitemap->properties()->where("status", "!=", Property::PREDRAFT)
        ->with([
            'details' => function ($query) use ($default_language) {
                $query->where("language_id", $default_language)->select(["property_id", "name"]);
            }
        ])->get()->toArray());

        $crumbs = [
            [
                "key"=>"taxonomy_index",
                "text"=>"Sınıflandırma",//trans("ContentPanel::categories.categories"),
                "icon"=>"stream",
                "href"=>route("Content.categories.index")
            ],
            [
                "key"=>"properties_index",
                "text"=>trans("ContentPanel::categories.properties"),
                "icon"=>"list",
                "href"=>route("Content.categories.getList", "property")
            ],
            [
                "key"=>"properties_of_sitemap",
                "text"=>$sitemap->detail->name. " - ". trans("ContentPanel::categories.properties"),
                "icon"=>"sitemap",
                "href"=>route("Content.categories.property.create", $sitemap->id)
            ]
        ];
        $breadcrumb = Content::getBreadcrumb($crumbs);
        $metaset = "[]";

        return view("ContentPanel::categories.property.create", compact("sitemap", 'metaset', "nestable", 'breadcrumb'));
    }

    public function store(Request $request, Property $property)
    {
        return PropertyEngine::ajaxStore($request, $property);
    }

    public function delete(Request $request)
    {
        $property = Property::find($request->get("id"));
        $property->details()->delete();
        $property->delete();

        return redirect()->back()->with("message", trans('MPCorePanel::general.success_message'));
    }

    public function orderSave(Request $request)
    {
        if(PropertyEngine::propertyOrderSave($request)){
            return redirect()->back()->with("message", trans('MPCorePanel::general.success_message'));
        }else {
            return redirect()->back()->with("error", trans('MPCorePanel::general.danger_message'));
        }
    }

}
