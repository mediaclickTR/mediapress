<?php

namespace Mediapress\Providers;

use App;
use Mediapress\Facades\DataSourceEngine;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Http\Kernel;
use Mediapress\Exceptions\Handler;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Console\Events\CommandFinished;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Collective\Html\FormFacade;
use Collective\Html\HtmlFacade;
use Collective\Html\HtmlServiceProvider;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\DataTablesServiceProvider;
use Yajra\DataTables\HtmlServiceProvider as YajraHtmlServiceProvider;
use Silber\Bouncer\BouncerServiceProvider;
use Silber\Bouncer\BouncerFacade;
use Maatwebsite\Excel\ExcelServiceProvider;
use Maatwebsite\Excel\Facades\Excel;
use App\Providers\RouteServiceProvider as LaravelRouteServiceProvider;

class MediapressServiceProvider extends ServiceProvider
{
    public const PRODUCTION = 'production';
    public const CONFIG = 'config';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    protected $commands = [
        'Mediapress\Commands\Install',
        'Mediapress\Commands\Update',
        'Mediapress\Commands\SearchContent',
        'Mediapress\Commands\FixDebugbar',
        'Mediapress\Commands\FixInteraction',
        'Mediapress\Commands\ReplaceWords',
        'Mediapress\Commands\CreateDefaultAdmin',
    ];

    public function boot()
    {
        // TODO :: Her modülde sitemap type seeder'i olacak


        $this->init();
        $this->loaders();
        $this->bladeDirectives();

        if ($this->app->runningInConsole()) {
            if ($this->isConsoleCommandContains(['db:seed', '--seed'], ['--class', 'help', '-h'])) {
                $this->addSeedsAfterConsoleCommandFinished();
            }
        }

        $this->registerMPModules();
    }

    protected function init()
    {
        Schema::defaultStringLength(191);

        if (config('mediapress.app_env') == self::PRODUCTION || config('mediapress.app_env') == 'prod') {
            error_reporting(0);
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
        }

        if ($this->app->environment() == self::PRODUCTION || $this->app->environment() == 'prod') {
            error_reporting(0);
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
        }
    }

    protected function loaders()
    {
        //TODO: düzenlencek
        /*
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', 'mediapress');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views', 'front');
        $this->loadMigrationsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Database' . DIRECTORY_SEPARATOR . 'migrations');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', 'panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'front', 'front');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Published' . DIRECTORY_SEPARATOR . 'config' => config_path()], 'mediapress');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'mediapress' => public_path('vendor/mediapress')], 'mediapress');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel' . DIRECTORY_SEPARATOR . 'includes' => base_path('resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'mediapress' . DIRECTORY_SEPARATOR . 'includes')], 'mediapress');
        */

        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'slots.php' => config_path()
        ]);
    }

    protected function bladeDirectives()
    {
        Blade::directive('pushonce', function ($expression) {
            $domain = explode(':', trim(substr($expression, 1, -1)));
            $isDisplayed = '__pushonce_' . $domain[0] . '_' . $domain[1];
            return "<?php if(!isset(\$__env->{$isDisplayed})): \$__env->{$isDisplayed} = true; \$__env->startPush('{$domain[0]}'); ?>";
        });

        Blade::directive('endpushonce', function ($expression) {
            return '<?php $__env->stopPush(); endif; ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


        if (!(class_exists($class = 'App\Mediapress'))) {
            $class = 'Mediapress\Foundation\Mediapress';
        }

        $this->app->singleton($class, $class);
        $this->app->bind("Mediapress", $class);


        $loader = AliasLoader::getInstance();
        $files = $this->app['files']->files(__DIR__ . '/../Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
        $this->app->singleton(HttpKernel::class, Kernel::class);
        $this->app->singleton(ExceptionHandler::class, Handler::class);
        //$this->app->register(MPCoreServiceProvider::class);
        $this->app->register(LoaderServiceProvider::class);
        $this->app->register(LaravelRouteServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
        $this->app->singleton("DataSource", DataSourceEngine::class);

        $this->app->register(CacheServiceProvider::class);
        $this->app->register(HtmlServiceProvider::class);
        $this->app->register(FormServiceProvider::class);
        $this->app->register(DataTablesServiceProvider::class);
        $this->app->register(YajraHtmlServiceProvider::class);

        $this->app->register(BouncerServiceProvider::class);
        $this->app->register(ExcelServiceProvider::class);

        $loader->alias('Form', FormFacade::class);
        $loader->alias('Html', HtmlFacade::class);
        $loader->alias('DataTable', DataTables::class);
        $loader->alias('Bouncer', BouncerFacade::class);
        $loader->alias('Excel', Excel::class);


        $this->app->bind('ModulesEngine', \Mediapress\Foundation\ModulesEngine::class);
        $this->app->singleton(ModulesEngine::class);

        //Confs
        $this->app[self::CONFIG]->set("filesystems.disks.uploads", [
            "driver" => "local",
            "root" => public_path('uploads'),
            "url" => config('mediapress.app_url') . '/uploads',
            "visibility" => "public",
        ]);

        $this->commands($this->commands);

        $this->app[self::CONFIG]->set("database.connections.mysql.strict", false);
        $this->app[self::CONFIG]->set("app.local", 'tr');
        $this->app[self::CONFIG]->set("app.fallback_locale", 'tr');

    }

    private function registerMPModules()
    {
        $this->app->register(ValidationServiceProvider::class);

        $modules = ModulesEngine::getMPModules(true);
        foreach ($modules as $module_key => $module) {
            $assumed_service_provider_path = $module . "\\" . $module_key . "ServiceProvider";
            if (class_exists($assumed_service_provider_path)) {
                $this->app->register($assumed_service_provider_path);
            }
        }

    }

    private function getConfigBasename($file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    protected function mergeConfig($path, $key)
    {
        $config = $this->app[self::CONFIG]->get($key, []);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        $this->app[self::CONFIG]->set($key, $config);
    }


    /**
     * Get a value that indicates whether the current command in console
     * contains a string in the specified $fields.
     *
     * @param string|array $contain_options
     * @param string|array $exclude_options
     *
     * @return bool
     */
    protected function isConsoleCommandContains($contain_options, $exclude_options = null): bool
    {
        $args = Request::server('argv', null);
        if (is_array($args)) {
            $command = implode(' ', $args);
            if (\Str::contains($command, $contain_options) && ($exclude_options == null || !\Str::contains($command, $exclude_options))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add seeds from the $seed_path after the current command in console finished.
     */
    protected function addSeedsAfterConsoleCommandFinished()
    {
        Event::listen(CommandFinished::class, function (CommandFinished $event) {
            if ($event->output instanceof ConsoleOutput) {
                $this->addSeedsFrom(__DIR__ . "/../Modules/*/Database/Seeds");
            }
        });
    }

    /**
     * Register seeds.
     *
     * @param string $seeds_path
     * @return void
     */
    protected function addSeedsFrom($seeds_path)
    {
        $file_names = glob($seeds_path . '/*.php');
        foreach ($file_names as $filename) {
            $classes = $this->getClassesFromFile($filename);
            foreach ($classes as $class) {
                Artisan::call('db:seed', ['--class' => $class, '--force' => '']);
            }
        }
    }

    /**
     * Get full class names declared in the specified file.
     *
     * @param string $filename
     * @return array an array of class names.
     */
    private function getClassesFromFile(string $filename): array
    {
        // Get namespace of class (if vary)
        $namespace = "";
        $lines = file($filename);
        $namespaceLines = preg_grep('/^namespace /', $lines);
        if (is_array($namespaceLines)) {
            $namespaceLine = array_shift($namespaceLines);
            $match = array();
            preg_match('/^namespace (.*);$/', $namespaceLine, $match);
            $namespace = array_pop($match);
        }

        // Get name of all class has in the file.
        $classes = array();
        $php_code = file_get_contents($filename);
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) {
                $class_name = $tokens[$i][1];
                if ($namespace !== "") {
                    $classes[] = $namespace . "\\$class_name";
                } else {
                    $classes[] = $class_name;
                }
            }
        }

        return $classes;
    }


}
