<div class="modal fade" id="pageCreateSuccessModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="pageCreateSuccessModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="text-center">
                    {!! optional($page->detail)->name !!}
                </div>
            </div>
            <div class="modal-body">
                <div class="col-md-12 mb-5 mt-5 text-center">
                    <strong class="text-success">{!! trans('MPCorePanel::general.success_message') !!}</strong>
                    <br><br>
                    {!! trans('MPCorePanel::general.choose_below_options') !!}
                    <div class="mt-4 buttons">
                        <a href="{{ route('Content.pages.index', ['sitemap_id' => $page->sitemap_id]) }}{{  $page->page_id ? '?page_id=' . $page->page_id : '' }}" data-type="1" class="btn btn-outline-dark">{!! trans('MPCorePanel::general.back_to_list') !!}</a>
                        <a href="javascript:void(0);" data-type="2" class="btn btn-outline-dark" data-dismiss="modal" aria-label="Close">{!! trans('MPCorePanel::general.edit_again') !!}</a>
                        <a href="{{ $createRoute }}" data-type="3" class="btn btn-outline-dark">{!! trans('MPCorePanel::general.add_new') !!}</a>
                        @if($page->detail && $page->detail->url)
                            <a href="{!! url($page->detail->url->url) . '?preview=1' !!}" data-type="4" class="btn btn-outline-dark mt-3" target="_blank">{!! trans('MPCorePanel::general.preview_page') !!}</a>
                        @endif
                    </div>
                    <div class="mt-4">
                        <div class="form-check checkbox">
                            <input type="checkbox" class="form-check-input" id="sessionCheck">
                            <label class="form-check-label" for="sessionCheck">{!! trans('MPCorePanel::general.set_this_option_while_session') !!}.</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    @if(is_null(session('panel.pageCreateSuccessPopupType')))
        <script>
            $(document).ready(function () {
                var status = false;

                $('#pageCreateSuccessModal').modal('show');

                $('#pageCreateSuccessModal').on('shown.bs.modal', function () {
                    $('div.modal-backdrop.show').css('cssText', "opacity: 0.8 !important;");
                });


                $('#sessionCheck').on('ifToggled', function (event) {
                    status = event.target.checked;
                });

                $('.buttons > a').on('click', function (e) {
                    e.preventDefault();

                    var obj = $(this);

                    var type = obj.attr('data-type');

                    if(status == true) {
                        $.ajax({
                            'url': '{{ route('Content.pageCreateSuccessPopupType') }}',
                            'data': {'type': type},
                            'method': 'GET',
                            success: function () {
                                clickButton(obj, type);
                            }
                        })
                    } else {
                        clickButton(obj, type);
                    }
                })
            });

            function clickButton(obj, type) {
                if(type != 2) {
                    if(type == 4) {
                        window.open(obj.attr('href'), '_blank')
                    } else {
                        location.href = obj.attr('href');
                    }
                }
            }
        </script>
    @endif
@endpush

<style>
    .form-check > .icheckbox_square-blue {
        float: inherit !important;
    }
</style>
