<div class="row" >

    @php($postCounter = 0)
    @foreach($instagram as $post)
        <div class="col-md-6 post" style="display:flex; min-height: 320px; ">
            <div class="col-md-6 align-self-center">
                @if($post["type"] == "carousel")
                    <div id="carouselExampleControls{!! $postCounter !!}" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($post["carousel_media"] as $child)
                                <div class="carousel-item {!! $loop->iteration == 1 ? "active": "" !!}">
                                    <img src="{!! $child["media_url"] !!}" class="d-block w-100" alt="...">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">{!! __('ContentPanel::instagram.previous') !!}</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">{!! __('ContentPanel::instagram.next') !!}</span>
                        </a>
                    </div>
                @elseif($post["type"] == "video")
                    <video width="250" controls>
                        <source src="{!! $post["media_url"] !!}" type="video/mp4">
                        {!! __('ContentPanel::instagram.please_try_with_chrome') !!}
                    </video>
                @else
                    <div class="col-md-6 align-self-center">
                        <img class="post-photo" src="{!! $post["thumbnail_photo"] !!}" alt="">
                    </div>
                @endif
            </div>
            <div class="col-md-6 align-self-center text-center">
                <p> <i class="far fa-heart"></i> {!! $post["likes"] !!} {!! __('ContentPanel::instagram.likes') !!} </p>
                <p> <i class="far fa-comments"></i> {!! count($post["comments"]) !!} {!! __('ContentPanel::instagram.comment') !!} </p>
                <p>
                    <a href="{!! $post["post_url"] !!}" target="_blank">
                        <button type="button" class="btn btn-outline-secondary float-bottom">
                        <i class="fas fa-share"></i> {!! __('ContentPanel::instagram.goto_post') !!}
                        </button>
                    </a>
                </p>
                <p style="">
                    <input type="checkbox" class="custom-control-input" id="checkbox{!! $postCounter !!}" data-id="{!! $post["id"] !!}">
                    <label class="custom-control-label" for="checkbox{!! $postCounter !!}">{!! __('ContentPanel::instagram.add_to_album') !!}</label>
                </p>
            </div>
        </div>
    @php($postCounter ++)
    @endforeach
</div>
