<?php

use Illuminate\Database\Migrations\Migration;
use Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreateCriteriasTable extends Migration
{

    public const CRITERIAS = 'criterias';

    public function up()
    {
        if (!Schema::hasTable(self::CRITERIAS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CRITERIAS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sitemap_id')->unsigned()->index();
                $table->boolean('type')->nullable();

                $table->defaultFields();
                $table->nestable("criteria_id");

                $table->text("ctex_1")->nullable();
                $table->text("ctex_2")->nullable();

                $table->string("cvar_1", 300)->nullable();
                $table->string("cvar_2", 300)->nullable();
                $table->string("cvar_3", 300)->nullable();
                $table->string("cvar_4", 300)->nullable();
                $table->string("cvar_5", 300)->nullable();

                $table->integer("cint_1")->nullable();
                $table->string("cint_2")->nullable();
                $table->string("cint_3")->nullable();
                $table->string("cint_4")->nullable();
                $table->string("cint_5")->nullable();

                $table->date("cdat_1")->nullable();
                $table->date("cdat_2")->nullable();

                $table->decimal("cdec_1", 20, 4)->nullable();
                $table->decimal("cdec_2", 20, 4)->nullable();
                $table->decimal("cdec_3", 20, 4)->nullable();
                $table->decimal("cdec_4", 20, 4)->nullable();


                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CRITERIAS);
    }
}
