<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class WebsiteExtra extends Model {

    use CacheQueryBuilder;
	protected $table = 'website_extras';

    protected $fillable = ["website_id","key", "value"];

    public $timestamps=false;

    public function website()
    {
        return $this->belongsTo(Website::class);
	}
}
