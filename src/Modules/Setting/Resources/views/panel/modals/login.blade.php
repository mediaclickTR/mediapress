<div class="modal fade bd-example-modal-lg" id="login" tabindex="-1" role="dialog"
     aria-labelledby="loginLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageQualityLabel">{!! __('SettingPanel::login.modal_title') !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{!! route('Settings.login') !!}" method="post" id="loginSettingsForm">
                    @csrf
                    <div class="mt-4">
                        <div class="form-group">
                            <span>{!! __('SettingPanel::login.google2fa_title') !!}</span>
                        </div>

                        <div class="form-group row mt-4">
                            <label for="status" class="col-sm-2 col-form-label">Durumu</label>
                            <div class="col-md-6">
                                <select id="status" class="form-control" name="google2fa[status]">
                                    <option value="1" {{ config('mediapress.google2fa') ? 'selected' : '' }}>Aktif</option>
                                    <option value="2" {{ config('mediapress.google2fa') ? '' : 'selected' }}>Pasif</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-5">
                            <label for="status" class="col-sm-2 col-form-label">Zorunluluk</label>
                            <div class="col-md-6">
                                <select id="status" class="form-control" name="google2fa[required]">
                                    <option value="1" {{ config('mediapress.google2fa_required') ? 'selected' : '' }}>Aktif</option>
                                    <option value="2" {{ config('mediapress.google2fa_required') ? '' : 'selected' }}>Pasif</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <span>{!! __('SettingPanel::login.login_title') !!}</span>
                        </div>
                        <div class="form-group row mt-4">
                            <label for="status" class="col-sm-6 col-form-label">Durumu</label>
                            <div class="col-md-6">
                                <select id="status" class="form-control" name="login_limiter[status]">
                                    <option value="1" {{ isset($loginSettings['login_limiter.status']) && $loginSettings['login_limiter.status'] == 1 ? 'selected' : '' }}>Aktif</option>
                                    <option value="2" {{ isset($loginSettings['login_limiter.status']) && $loginSettings['login_limiter.status'] == 2 ? 'selected' : '' }}>Pasif</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="maxAttempt" class="col-sm-6 col-form-label">Kaç kere hatalı giriş yapabilir?</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="maxAttempt" name="login_limiter[max_attempt]" value="{{ $loginSettings['login_limiter.max_attempt'] ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="expireTime" class="col-sm-6 col-form-label">Engellendikten kaç dakika sonra tekrar deneyebilir?</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="expireTime" name="login_limiter[expire_time]" value="{{ $loginSettings['login_limiter.expire_time'] ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="whiteList" class="col-sm-12 col-form-label">
                                Bu sınırlamalara girmeyecek ip listesi
                                <select name="login_limiter[white_list][]" multiple="multiple" id="whiteList" style="width: 100%">
                                    @isset($loginSettings['login_limiter.white_list'])
                                        @foreach(json_decode($loginSettings['login_limiter.white_list'], 1) as $ip)
                                            <option value="{{ $ip }}" selected>{{ $ip }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </label>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="loginSettingsForm" class="btn btn-primary">{!! __('SettingPanel::general.save') !!}</button>
            </div>
        </div>
    </div>
</div>
