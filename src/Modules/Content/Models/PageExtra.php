<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class PageExtra extends Model {

    use CacheQueryBuilder;
    protected $table = 'page_extras';

    protected $fillable = ["page_id","key", "value"];

    public $timestamps=true;

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function parent()
    {
        return $this->belongsTo(Page::class);
    }
}
