<?php

return [
    'tab' => [
        'langPart' => "Language Variables",
        'exportContent' => "Content Importer",
        'importContent' => "Content Exporter",
    ],

    'langPart_title' => 'Language Variables',
    'save_info' => 'Attention recording is done automatically. Just fill in the relevant fields.',
    'search' => 'Search...',
    'variable' => 'Variable',
    'default_lang_value' => 'Default Language Value',
    'detail' => "Detail",
    'view' => 'View',
    'languages' => 'Languages',
    'no_data' => 'The language variable for the :website could not be found.',
];
