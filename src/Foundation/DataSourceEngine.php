<?php
/**
 * Created by PhpStorm.
 * User: genarge
 * Date: 10.09.2018
 * Time: 17:28
 */

namespace Mediapress\Foundation;


final class DataSourceEngine
{
    /**
     * @param $module_name
     * @param $datasource_name string data_source_name
     * @param array $args
     * @return mixed
     * @throws \Exception
     */
    public function getDataSource($module_name, $datasource_name, $args = [])
    {
        $arr = [
            "App\\Modules\\$module_name\\DataSources\\$datasource_name",
            "Mediapress\\Modules\\$module_name\\DataSources\\$datasource_name"
        ];

        $id = session("panel.website.id");
        if($id){
            array_unshift($arr,"App\\Modules\\$module_name\\Website$id\\DataSources\\$datasource_name");
        }

        foreach ($arr as $class) {
            if (class_exists($class)) {
                return new $class($args);
            }
        }

        throw new \InvalidArgumentException("DataSource not found: $module_name-$datasource_name");
    }
}
