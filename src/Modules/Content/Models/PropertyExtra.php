<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyExtra extends Model
{

    protected $table = 'property_extras';

    public $timestamps = false;

    protected $fillable = ["property_id", "key", "value"];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
