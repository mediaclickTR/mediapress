<?php

namespace Mediapress\Modules\Setting;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = "Setting";
    protected $namespace = 'Mediapress\Modules\Setting';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');

        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias("Setting", Setting::class);
        app()->bind("Setting", function() {
            return new \Mediapress\Modules\Setting\Setting;
        });
    }
}
