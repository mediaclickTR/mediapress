<?php

namespace Mediapress\Modules\MPCore\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\Meta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;
use Mediapress\Modules\Content\Foundation\UrlMetaWorker;

class Url extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;

    protected $table = 'urls';
    public $timestamps = true;
    protected $fillable = ['website_id', 'url', 'model_id', 'model_type', 'type', 'password'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ["password"];

    public function getType()
    {
        return $this->type;
    }

    public function website()
    {
        return $this->belongsTo(Website::class, "website_id");
    }

    public function dynamicUrl()
    {
        return $this->belongsTo(DynamicUrl::class, "url_id", "id");
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function relation()
    {
        return $this->morphTo('model');
    }

    public function redirect()
    {
        return $this->hasMany(Url::class, 'model_id');
    }

    public function meta()
    {
        return $this->hasOne(Meta::class);
    }

    public function metas()
    {
        return $this->morphMany(Meta::class, "url");
    }
    
    public function getMetaWorker(){
        return new UrlMetaWorker($this,$this->metas);
    }

    public function setUrlAttribute($value)
    {

        if (mb_substr($value, 0, 1) != '/') {
            $this->attributes['url'] = '/' . $value;
        } else {
            $this->attributes['url'] = $value;
        }

    }

    public function getUrlAttribute($value){

        if( $this->website && $this->website->type == "folder" && !mediapress('panel')) {
            return $this->website->slug .$value;
        }
        return $value;
    }
    
    public function getMetaWorkerAttribute(){
        return $this->getMetaWorker();
    }
    
    
    public function __toString()
    {
        return $this->getUrlAttribute($this->attributes['url']);
    }


}
