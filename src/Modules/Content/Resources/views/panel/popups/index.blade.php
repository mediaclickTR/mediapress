@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')

    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">{!! __("ContentPanel::popup.list_title") !!}</div>
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Content.popup.create') !!}" class="btn btn-primary btn-sm">
                    <i class="fas fa-plus"></i>
                    {!! __("ContentPanel::general.add_new") !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>{!! __("ContentPanel::general.status") !!}</th>
                    <th>{!! __("ContentPanel::general.name") !!}</th>
                    <th>{!! __("ContentPanel::popup.type") !!}</th>
                    <th>{!! __("ContentPanel::popup.device.title") !!}</th>
                    <th>{!! __("ContentPanel::popup.languages") !!}</th>
                    <th>{!! __("ContentPanel::general.updated_at") !!}</th>
                    <th>{!! __('ContentPanel::general.actions') !!}</th>
                </tr>
                </thead>
                <tbody class="popup-content">
                @foreach($popups as $popup)
                    <?php
                    $status = $popup->status;
                    if ($status != 0 && $popup->start_date && $popup->start_date != "" && $popup->finish_date && $popup->finish_date != "" && $popup->display_date_type == 1/*Belirli tarih aralığında gösterilecekse*/) {
                        $now = \Carbon\Carbon::now();
                        $start_date = \Carbon\Carbon::parse($popup->start_date);
                        $finish_date = \Carbon\Carbon::parse($popup->finish_date);

                        if ($start_date->lt($now) && $finish_date->gt($now)) {
                            $status = 1;
                        } elseif ($start_date->lt($now) && $finish_date->lte($now)) {
                            $status = 0;
                        } elseif ($start_date->gt($now)) {
                            $status = 2;
                        }
                    }
                    ?>
                    <tr>
                        <td>
                            {{ $popup->order }}
                        </td>
                        <td class="status">
                            @if($status == 0)
                                <i class="passive" style="background: #dc3545"></i> {{ __('ContentPanel::general.status_title.passive') }}
                            @elseif($status == 2)
                                <i class="future" style="background: #e4c703"></i> {{ __('ContentPanel::general.status_title.postdate') }}
                            @else
                                <i class="active" style="background: #7fcc46"></i> {{ __('ContentPanel::general.status_title.active') }}
                            @endif
                        </td>
                        <td><a href="{!! route('Content.popup.edit',$popup->id) !!}">{{ $popup->name }}</a></td>
                        <td>
                            @if($popup->type == 1)
                                Lightbox
                            @elseif($popup->type == 2)
                                Tooltip ({{ $popup->place == "left" ? __('ContentPanel::popup.positions.left_bottom') : __('ContentPanel::popup.positions.right_bottom') }})
                            @elseif($popup->type == 3)
                                Bar ({{ $popup->place == "bottom" ? __('ContentPanel::popup.positions.bottom') : __('ContentPanel::popup.positions.up') }})
                            @else
                                Perde
                            @endif
                        </td>
                        <td>
                            {{ implode(', ', $popup->devices) }}
                        </td>
                        <td>
                            {{ implode(', ', array_map('strtoupper', $popup->languages->pluck('code')->toArray())) }}
                        </td>
                        <td>
                            {!! \Carbon\Carbon::parse($popup->updated_at)->format('d.m.Y H:i') !!} /
                            {!! $popup->admin ? $popup->admin->short_name : '' !!}
                        </td>
                        <td>
                            <a href="{!! route('Content.popup.edit',$popup->id) !!}" class="mr-2" title="{!! __("ContentPanel::general.edit") !!}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" class="mr-2 copyBtn" data-id="{{ $popup->id }}" title="{!! __("ContentPanel::popup.copy") !!}" data-toggle="modal" data-target="#popupCopy">
                                <i class="fa fa-copy"></i>
                            </a>
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{{ route('Content.popup.delete',$popup->id) }}" title="{!! __("ContentPanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="popupCopy" tabindex="-1" role="dialog" aria-labelledby="popupCopyLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="popupCopyLabel">Bu Popup'ı Şuraya Klonla</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 mt-4 mb-3">
                        <form action="{{ route('Content.popup.clone') }}" class="popupCloneForm">
                            <input type="hidden" name="popup_id">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="CountryGroup">Hedef Ülke Grubu</label>
                                        <select class="form-control" name="country_group_id" id="CountryGroup">
                                            <option value="">Seçim Yapınız</option>
                                            @foreach($country_groups as $group)
                                                <option
                                                    {{old('target_country_group') == $group->id ? 'selected' : ''}}
                                                    value="{{$group->id}}">
                                                    {{$group->title}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="languageBox" style="display: none;">
                                        <label for="Language">Kaynak Dil</label>
                                        <select class="form-control" name="language_id" id="Language">

                                        </select>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit-btn">Kopyala</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.copyBtn').on('click', function () {
            $('[name="popup_id"]').val($(this).attr('data-id'))
        })

        $('select[name="country_group_id"]').on('change', function () {
            var country_group_id = $(this).val();


            $.ajax({
                'url': "{{route('Content.popup.getLanguages')}}",
                'data': {'country_group_id': country_group_id, 'popup_id': $('[name="popup_id"]').val()},
                'method': "GET",
                success: function (result) {
                    content = '<option value="">Seçim Yapınız</option>';
                    $.each(result, function (k, v) {
                        content += '<option value="' + k + '">' + v + '</option>'
                    });

                    $('select[name="language_id"]').append(content);
                    $('#languageBox').show();
                }
            });
        });


        $('#submit-btn').on('click', function (e) {
            e.preventDefault();

            $('form.popupCloneForm').submit();
        });

        $('#popupCopy').on('hidden.bs.modal', function (e) {
            $('select[name="country_group_id"]').val("");
            $('select[name="language_id"]').val("");
            $('select[name="language_id"]').html("");
            $('#languageBox').hide();
        })

        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ __("MPCorePanel::general.are_you_sure") }}',
                text: '{{ __("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! __("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! __("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
