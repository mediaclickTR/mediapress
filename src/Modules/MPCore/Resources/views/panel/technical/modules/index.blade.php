@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("MPCorePanel::module.modules") !!}</h1>
            <div class="float-right">
                    <a href="{!! route('MPCore.technical.modules.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
            <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    <th>{!! trans("MPCorePanel::general.status") !!}</th>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("MPCorePanel::module.slot.name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($modules as $modul)
                    <tr>
                        <td>{{ $modul->id }}</td>
                        <td><i class="{{ ($modul->status== 1) ? 'aktif' : 'sifreli' }}"></i></td>
                        <td>{{ $modul->name }}</td>
                        <td><i class="fa fa-check" style="color:green"></i>{{ $modul->slot->slot ?? "-" }}</td>
                        <td>
                            <select onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                <option value="{!! route('MPCore.technical.modules.edit',$modul->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                {{-- <option value="{!! route('MPCore.technical.modules.manage',$modul->id) !!}">{!! trans("MPCorePanel::module.edit") !!}</option>--}}
                                <option value="{!! route('MPCore.technical.modules.delete',$modul->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="edition">
                <ul>
                    <li>{!! trans("MPCorePanel::general.status_active") !!}</li>
                    <li>{!! trans("MPCorePanel::general.status_passive") !!}</li>
                </ul>
            </div>
    </div>
@endsection
@push("styles")
    <style>
        .fa{
            display: inline;
            margin-right: 10px;
        }
        table tbody tr td i {
            display: block;
        }
    </style>
@endpush
