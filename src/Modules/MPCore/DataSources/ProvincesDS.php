<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 15.11.2018
     * Time: 09:59
     */

    namespace Mediapress\Modules\MPCore\DataSources;


    use Mediapress\Foundation\DataSource;
    use Mediapress\Modules\Locale\Models\Province;
    use Mediapress\Modules\MPCore\Models\Country;

    class ProvincesDS extends DataSource
    {
        public function getData(){
            $id = $this->params;

            $pluck=($this->params["pluck"] ?? false) && $this->params["pluck"];
            if($pluck){
                $key = $this->params["pluck_key"] ?? "id";
                $value = $this->params["pluck_column"] ?? "id";

                return Province::get()->pluck($value,$key);
            }
            if($this->params[0]==""||$this->params[0]==0){
                return Province::get();
            }else{
                return Province::where('country_id',$id)->get();
            }
        }
    }
