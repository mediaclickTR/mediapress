<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

const MANUAL_META = "manual_meta";
class UpdateAllDetailsTablesAddManualMetaColumn extends Migration
{
    public const SITEMAP_DETAILS = 'sitemap_details';
    public const PAGE_DETAILS = 'page_details';
    public const CATEGORY_DETAILS = 'category_details';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable(self::SITEMAP_DETAILS)) {
            Schema::table(self::SITEMAP_DETAILS, function (Blueprint $table) {
                $table->boolean(MANUAL_META)->default(false);
            });
        }
        if( Schema::hasTable(self::PAGE_DETAILS)) {
            Schema::table(self::PAGE_DETAILS, function (Blueprint $table) {
                $table->boolean(MANUAL_META)->default(false);
            });
        }
        if( Schema::hasTable(self::CATEGORY_DETAILS)) {
            Schema::table(self::CATEGORY_DETAILS, function (Blueprint $table) {
                $table->boolean(MANUAL_META)->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::SITEMAP_DETAILS)) {
            Schema::table(self::SITEMAP_DETAILS, function (Blueprint $table) {
                $table->dropColumn(MANUAL_META);
            });
        }
        if( Schema::hasTable(self::CATEGORY_DETAILS)) {
            Schema::table(self::CATEGORY_DETAILS, function (Blueprint $table) {
                $table->dropColumn(MANUAL_META);
            });
        }
        if( Schema::hasTable(self::PAGE_DETAILS)) {
            Schema::table(self::PAGE_DETAILS, function (Blueprint $table) {
                $table->dropColumn(MANUAL_META);
            });
        }
    }
}
