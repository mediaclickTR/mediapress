@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
                <div class="float-left">
                    <div class="title">"{!! $entitylist->name !!}" {!! trans("EntityPanel::entitylist.entitylist") !!} ({!! count($users) !!})</div>
                </div>
                <div class="float-right">
                    <ul class="pager wizard">
                        <li class="previous">
                            <a style="color:#333 !important;font-weight:bold !important;" href="{!! route("Entity.entitylist.index") !!}"><i class="fa fa-angle-double-left"></i>
                                {!! trans("EntityPanel::entitylistdetail.back-entitylist") !!}</a>
                        </li>
                    </ul>
                </div>

            <div class="clearfix"></div>
                @if(count($users)==0)
                    <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                    <div class="float-right">
                        <a class="btn btn-primary" href="{!! route('Entity.entitylist.listdetail.create',$list_id) !!}"> <i class="fa fa-plus"></i>
                            {!! trans("MPCorePanel::general.new_add") !!}
                        </a>
                    </div>
                @else
                <div class="table-field">
                <div class="float-left">
                    <select class="nice" id="multiple-processings">
                        <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                        <option value="remove">{!! trans('MPCorePanel::general.delete_selected') !!}</option>
                    </select>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{!! route('Entity.entitylist.listdetail.create',$list_id) !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
                <div class="clearfix"></div>
                <form action="{!! route("Entity.entitylist.listdetail.selectedRemove",$list_id) !!}" id="selectedRemove" method="POST">
                @csrf
                <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.check") !!}</th>
                    <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    <th>{!! trans("EntityPanel::entitylistdetail.user-name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($users as $user)
                    @php $relation = $user->model_type::find($user->model_id);@endphp
                    <tr>
                        <td>
                            <div class="checkbox">
                                <label for="male">
                                    <input type="checkbox" name="checked[]" value="{!! $user->id !!}" id="male">
                                </label>
                            </div>
                        </td>
                        <td>{!! $user->id !!}</td>
                        <td>{!! (isset($relation->name)) ? $relation->name : '-'!!}</td>
                        <td>{!! $user->created_at !!}</td>
                        <td>
                            <select onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                <option value="{!! route('Entity.entitylist.listdetail.edit',['list_id'=>$list_id,'id'=>$user->id]) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                <option value="{!! route('Entity.entitylist.listdetail.detail',['list_id'=>$list_id,'id'=>$user->id]) !!}">{!! trans("MPCorePanel::general.detail") !!}</option>
                                <option value="{!! route('Entity.entitylist.listdetail.delete',['list_id'=>$list_id,'id'=>$user->id]) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
                    </table>
                </form>
            @endif
    </div>
@endsection

@push("scripts")
    <script>
        $(document).ready(function(){
            $('#multiple-processings').change(function(){
                if($("#multiple-processings").val() == "remove"){
                    $("#selectedRemove").submit();
                }
            });
        });
    </script>
@endpush

@push('styles')
    <style>
        td {
            text-align: center !important;
        }
    </style>
@endpush
