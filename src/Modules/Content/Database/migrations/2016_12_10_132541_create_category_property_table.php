<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPropertyTable extends Migration
{

    public const CATEGORY_PROPERTY = 'category_property';

    public function up()
    {
        if (!Schema::hasTable(self::CATEGORY_PROPERTY)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CATEGORY_PROPERTY, function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('property_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CATEGORY_PROPERTY);
    }
}
