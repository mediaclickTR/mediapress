<?php

namespace Mediapress\Http\Controllers;


use Barryvdh\Debugbar\LaravelDebugbar;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\Controller as BaseController;
use Mediapress\Traits\PageTrait;
use Response;
use SimpleXMLElement;
use Illuminate\Support\Facades\Cache;
use Auth;

class WebController extends BaseController
{

    use PageTrait;

    public function main(Mediapress $mediapress)
    {

        if (config('mediapress.view_cache')) {

            $parameters = $mediapress->request->fullUrl() . @implode(',', $mediapress->request->all());
            if(Auth::check()){
                return $this->decide($mediapress);
            }

            return Cache::remember(cache_key($parameters.'-view:render'), 24 * 60, function () use ($mediapress) {
                return $this->decide($mediapress);
            });
        }
        return $this->decide($mediapress);
    }

    public function view(Mediapress $mediapress)
    {
        $return = false;
        if ($mediapress->request->api == 1) {
            if ($mediapress->request->f == 'json') {
                $return = response()->json($mediapress->data);
            } elseif ($mediapress->request->f == 'xml') {
                $data = array_to_xml($mediapress->data);
                $return = response($data)->header('Content-Type', 'application/xml');
            } else {
                $return = 'Required API Format';
            }
        }
        if($return){
            return $return;
        }

        /** @var \Illuminate\Http\Response $response */
        $response = response()->view('web.pages.' . $mediapress->getTemplate(), compact('mediapress'));

        if (config('mediapress.minify')) {
            $content = $response->getContent();
            $content = preg_replace('/ \/\/.+\n/m', '', $content);
            $content = preg_replace('!/\*.*?\*/!s', '', $content);
            $content = preg_replace('/\n\s*\n/', "\n", $content);
            $content = str_replace(array("\r\n", "\r", "\n"), "", $content);
            $content = preg_replace('/(\>)\s*(\<)/m', '$1$2', $content);
            $content = preg_replace('/\s+/', ' ', $content);
            $response->setContent($content);
        }
        return $response;
    }
}
