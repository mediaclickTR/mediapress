<?php

namespace Mediapress\Modules\Content\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use InstagramAPI\Instagram;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\AlbumPost;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\Content\Models\SocialMedia;
use Mediapress\Modules\MPCore\Facades\MPCore;

use Mediapress\Modules\Content\Models\InstagramPosts;
use Mediapress\Modules\Content\Models\Album;



class InstagramController extends Controller
{
    use TableBuilderTrait;

    public function index(Request $request)
    {
        if ($request->id != null){
            $alb = Album::where("id", $request->id)->withTrashed()->first();
            $name = $alb->name;
        }
        $albums = Album::orderBy("created_at", "DESC")->get();
//
//        $time = Carbon::createFromFormat("H:i:s", $albums[0]->sync_last);
//        $date = $time->timestamp;
//
//        $to = Carbon::createFromTimestamp($date);
//        $to = $to->addMinutes($albums[0]->sync_minutes);
//        $to = $to->timestamp;

        return view('ContentPanel::instagram.index',compact("albums", "name"));

    }

    public function sync(Request $request)
    {
        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        return view('ContentPanel::instagram.sync');
    }

    public function album(){

        $datas = InstagramPosts::all();
        return view('ContentPanel::instagram.album',compact("datas"));
    }

    public function allPosts(Request $request){
        $datas = InstagramPosts::orderBy("created_at", "DESC")->get();
        return view('ContentPanel::instagram.all-posts',compact("datas"));
    }

    public function saveAlbum(Request $request){
        /*$rules = [
            'album_name' => 'required',
            'album_slug' => 'required',
            'album_status' => 'required',
        ];
        $messages = [
            'album_name.required' => 'Albüm Adı Gereklidir',
            'album_slug.required' => 'Albüm Slug Alanı Gereklidir',
            'album_status.required' => 'Albüm Yayınlama Durumu Gereklidir',
        ];

        $this->validate($request, $rules, $messages);
*/


        $posts = $request->posts;
        $data =$request->data;

        $album = new Album();
        $album->name = $data["album_name"];
        $album->slug = $data["album_slug"];
        $album->status = $data["album_status"];
        $album->sync_data = $data["album_data"];
        $album->sync_limit = $data["album_limit"];
        $album->sync_minutes = $data["album_refresh"];
        $album->sync_priority = $posts;
        $album->save();

        //$this->syncAlbum($album);
    }

    public function syncAlbum($album){
        // Delete exist records
        $posts = AlbumPost::where("album_id", $album->id)->delete();

        $result = collect();

        // Control is there any prior post
        if ($album->sync_priority != null){
            // Save prior posts to pivot table
            $this->savePostsToAlbum($album->id, $album->sync_priority);
            $refresh = Album::where("id", $album->id)->first();
            $postIds = explode(",",$refresh->sync_priority);
            foreach ($postIds as $postId){
                if ($postId != ""){
                    $post = InstagramPosts::where("id", $postId)->first();
                    if ($post != null){
                        $result->put($post->id, json_decode($post->post_datas, 1));
                    }
                }
            }
        }

        if($album->sync_data != null && $album->sync_minutes != null){
            if(preg_match("/#/i", $album->sync_data)){
                $datas = $this->getHashtagDatas(str_replace("#", "",$album->sync_data),true, true, $album->sync_limit,true );
                if ($datas == false){
                    return false;
                }
                foreach ($datas as $data){
                    $control = InstagramPosts::where("instagram_id", $data["id"])->first();

                    $postId = 0;
                    if ($control == null){
                        $post = new InstagramPosts();
                        $post->instagram_id = $data["id"];
                        $post->post_datas = json_encode($data, 1);
                        $post->save();
                        $postId = $post->id;
                    }else{
                        $postId = $control->id;
                    }

                    $control2 = AlbumPost::where("album_id", $album->id)->where("post_id", $postId)->first();
                    if ($control2 == null){
                        $pivot = new AlbumPost();
                        $pivot->album_id = $album->id;
                        $pivot->post_id = $postId;
                        $pivot->save();
                    }


                    $result->put($postId, $data);
                }
            }else{
                $datas = $this->getUserDatas($album->sync_data, true ,true, $album->sync_limit, true);
                if ($datas == false){
                    return false;
                }
                foreach ($datas as $data){
                    $control = InstagramPosts::where("instagram_id", $data["id"])->first();

                    $postId = 0;
                    if ($control == null){
                        $post = new InstagramPosts();
                        $post->instagram_id = $data["id"];
                        $post->post_datas = json_encode($data, 1);
                        $post->save();
                        $postId = $post->id;
                    }else{
                        $postId = $control->id;
                    }

                    $control2 = AlbumPost::where("album_id", $album->id)->where("post_id", $postId)->first();
                    if ($control2 == null){
                        $pivot = new AlbumPost();
                        $pivot->album_id = $album->id;
                        $pivot->post_id = $postId;
                        $pivot->save();
                    }

                    $result->put($postId, $data);
                }
            }
        }

        $album->sync_last = Carbon::now();
        $album->last_posts_datas = json_encode($result->toArray(), 1);
        $album->save();
        return $result;
    }

    public function savePostsToAlbum($albumId, $posts){
        $postIds = explode(",",$posts);
        $status = false;
        $priority = "";
        foreach ($postIds as $id){
            if($id == "")
                continue;
            $control = AlbumPost::where("album_id", $albumId)->where("post_id", $id)->first();
            if ($control == null){
                // Control for priority images has selected on create album downloaded before
                if(!is_numeric($id)){
                    $id = $this->downloadPost($id)["id"];
                }
                $priority = $priority.",".$id;
                $pivot = new AlbumPost();
                $pivot->album_id = $albumId;
                $pivot->post_id = $id;
                $status = $pivot->save();
            }else{
                $status = true;
            }

        }
        $album = Album::where("id", $albumId)->first();
        $album->sync_priority = $priority;
        $album->save();
        if ($status == true)
            return "true";
        return "false";
    }

    public function downloadPost($id){
        $control = InstagramPosts::where("instagram_id", $id)->first();
        if ($control != null)
            return ["status" => true, "id"=> $control->id];
        $ig = $this->instagramLogin();
        $info = $ig->media->getInfo($id);
        $datas = $this->createCollection($info, 50, true, true)[0];

        $json = json_encode($datas);

        $post = new InstagramPosts();
        $post->instagram_id = $datas["id"];
        $post->post_datas = $json;

        $status = $post->save();
        return ["status" => $status, "id"=> $post->id];
    }

    public function getPosts($id, $function=null){
        $album = Album::where("id", $id)->first();
        $posts = collect();
        if ($album == null || ($album->status != 1 && $function == null))
            return $posts;
        if ($function != null)
            return $this->syncAlbum($album);

        $posts = Cache::remember('album'.base64_encode($id), $album->sync_minutes, function () use ($album) {
            $a = $this->syncAlbum($album);
            if ($a == false){
                return collect(json_decode($album->last_posts_datas, 1));
            }else{
                return $this->syncAlbum($album);
            }
        });

        return $posts;
    }

    public function savePost(Request $request){
        $id = $request->post;
        $status = $post->save();
        $result = $this->downloadPost($id);
        if ($result["status"] == true)
            return "true";
        return "false";
    }

    public function editAlbum(Request $request){
        $id = $request->route("id");
        $album = Album::where("id", $id)->first();
        $instagram = $album->posts(true);
        $album = Album::where("id", $id)->first();
        return view('ContentPanel::instagram.album_detail', compact("album", "datas", "instagram"));
    }

    public function ajaxEditAlbum(Request $request){
        $posts = $request->posts;
        $data =$request->data;

        $album = Album::where("id", $request->id)->first();
        $album->name = $data["album_name"];
        $album->slug = $data["album_slug"];
        $album->status = $data["album_status"];
        $album->sync_data = $data["album_data"];
        $album->sync_limit = $data["album_limit"];
        $album->sync_minutes = $data["album_refresh"];
        if ($posts != null){
            $album->sync_priority = $album->sync_priority.$posts;
        }
        $album->save();

        Cache::forget('album'.base64_encode($request->id));
    }

    public function syncWithUser(Request $request){
        $user = $request->user;
        $instagram = [];
        $instagram = $this->getUserDatas($user, true, true, 50);
        return view('ContentPanel::instagram.posts', compact("instagram"))->render();
    }

    public function syncWithHashtag(Request $request){
        $hashtag = $request->hashtag;
        $instagram = [];
        $instagram = $this->getHashtagDatas($hashtag, true, true, 20);
        return view('ContentPanel::instagram.posts', compact("instagram"))->render();
    }

    public function albumSyncUser(Request $request){
        $user = $request->user;
        $instagram = [];
        $instagram = $this->getUserDatas($user, true, true, 50);
        return view('ContentPanel::instagram.albumPosts', compact("instagram"))->render();
    }

    public function albumSyncHashtag(Request $request){
        $hashtag = $request->hashtag;
        $instagram = [];
        $instagram = $this->getHashtagDatas($hashtag, true, true, 20);
        return view('ContentPanel::instagram.albumPosts', compact("instagram"))->render();
    }

    public function deletePrioritiy(Request $request){
        $album = Album::where("id", $request->album)->first();
        $priortiy = "";

        foreach (explode(",", $album->sync_priority) as $prty){
            if ($prty == $request->id || $prty == "")
                continue;

            $priortiy = $priortiy.",".$prty;
        }


        $album->sync_priority = $priortiy;
        $album->save();
        Cache::forget('album'.base64_encode($request->album));
    }

    public function deleteCache(Request $request){
        $id = $request->id;
        Cache::forget('album'.base64_encode($request->id));
        $album = Album::where("id", $id)->first();
        $album->posts();

    }

    public function deleteAlbum(Request $request){
        $id = $request->id;
        $album = Album::where("id", $id)->first();
        if ($album != null){
            $status = $album->delete();
            return $status ? 'true' : 'false';
        }
    }

    /**
     * Instagram library Functions
     */
    function instagramLogin(){
        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        set_time_limit(100);
        date_default_timezone_set('UTC');
        require '../vendor/autoload.php';
        /////// CONFIG ///////
        $username = env("INSTAGRAM_USERNAME");
        $password = env("INSTAGRAM_PASSWORD");

        if ($username == "" || $username == null || $password == "" || $password == null){
            echo 'ENV Dosyasından INSTAGRAM_USERNAME ve INSTAGRAM_PASSWORD değişkenlerini tanımlayınız';
            exit(0);
        }
        $debug = false;
        $truncatedDebug = false;
        //////////////////////
        $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
        try {
            $ig->login($username, $password);
        } catch (\Exception $e) {
            return false;
        }
        return $ig;
    }


    function getUserDatas($user, $detail = false ,$comments = false, $limit = 10, $includeCarousel = true){
        $ig=$this->instagramLogin();
        if ($ig == false){
            return false;
        }

        $userId = $ig->people->getUserIdForName($user);
        $datas = $ig->timeline->getUserFeed($userId);
        if ($detail == false){
            return $this->createThumbNailCollection($datas,$limit, $includeCarousel);
        }elseif($detail == true) {
            return $this->createCollection($datas, $limit, $comments, $includeCarousel);
        }
    }

    function getHashtagDatas($hashtag, $detail = false, $comments = false, $limit = 10, $includeCarousel = true ){
        $ig = $this->instagramLogin();
        if ($ig == false){
            return false;
        }
        $datas = $ig->hashtag->getFeed($hashtag, \InstagramAPI\Signatures::generateUUID());
        if ($detail == false){
            return $this->createThumbNailCollection($datas, $limit, $includeCarousel);
        }elseif($detail == true) {
            return $this->createCollection($datas, $limit, $comments, $includeCarousel);
        }
    }

    function getUserInformation($user){
        $profileUrl = "https://www.instagram.com/";
        $ig = $this->instagramLogin();
        $id = $ig->people->getUserIdForName($user);
        $datas = $ig->people->getInfoById($id);
        $result = collect();
        $datas = $datas->asArray()["user"];
        $result->push([
            "user_name" => $datas["username"],
            "profile_picture" => $datas["hd_profile_pic_versions"][0]["url"],
            "followers" => $datas["follower_count"],
            "following" => $datas["following_count"],
            "medias" => $datas["media_count"],
            "profile_url" => $profileUrl.$datas["username"]
        ]);

        return $result;
    }

    function createCollection($datas, $limit, $comments = false, $includeCarousel = true){
        $instagramPostUrl = "https://www.instagram.com/p/";
        $posts = collect();
        $indexCounter = 0;
        foreach ($datas->getItems() as $index => $data) {
            if ($indexCounter == $limit){
                break;
            }
            $item = $data->asArray();
            ##### media_type => 1-> Resim, 2-> Video, 8-> Carausel #####
            switch ($item["media_type"]){

                case 1:{
                    $url = $item["image_versions2"]["candidates"][0]["url"];
                    $cmnt = [];
                    if ($comments == true){
                        $id = $item["id"];
                        $cmnt = $this->getComments($id);
                    }
                    $indexCounter ++;
                    $posts->push(["id"=>$item["id"], "type" => "image", "media_url" => $url,"post_url" => $instagramPostUrl.$item["code"], "thumbnail_photo" =>$url, "comments" => $cmnt, "likes" =>$item["like_count"]]);
                } break;

                case  2:{
                    $url = $item["video_versions"][0]["url"];
                    $cmnt = [];
                    if ($comments == true){
                        $id = $item["id"];
                        $cmnt = $this->getComments($id);
                    }
                    $indexCounter ++;
                    $posts->push(["id"=>$item["id"], "type" => "video", "media_url" => $url,"post_url" => $instagramPostUrl.$item["code"],"thumbnail_photo" =>$item["image_versions2"]["candidates"][0]["url"], "comments" => $cmnt, "likes" =>$item["like_count"]]);
                } break;

                case 8: {
                    $carousel= collect();
                    if (isset($item["carousel_media"]) && $includeCarousel == true){
                        $cmnt = [];
                        $thumbnailPhoto = "";
                        if ($comments == true){
                            $id = $item["id"];
                            $cmnt = $this->getComments($id);
                        }
                        foreach ($item["carousel_media"] as $c){
                            if ($c["media_type"] == 1){
                                $url = $c["image_versions2"]["candidates"][0]["url"];
                                $carousel->push(["type" => "image", "media_url" => $url]);
                            }elseif ($c["media_type"] == 2){
                                $url = $c["video_versions"][0]["url"];
                                $carousel->push(["type" => "video", "media_url" => $url]);
                            }
                            if ($thumbnailPhoto == ""){
                                $thumbnailPhoto = $c["image_versions2"]["candidates"][0]["url"];
                            }
                        }
                        $indexCounter ++;
                        $posts->push(["id"=>$item["id"], "type" => "carousel", "media_url" => "", "post_url" => $instagramPostUrl.$item["code"],"thumbnail_photo" =>$thumbnailPhoto, "comments" => $cmnt, "carousel_media" => $carousel, "likes" =>$item["like_count"]]);
                    }
                }
            }// End of switch case
        }// End of foreach
        return $posts;
    }

    function createThumbNailCollection($datas, $limit, $includeCarousel = true){
        $instagramPostUrl = "https://www.instagram.com/p/";
        $posts = collect();
        $indexCounter = 0;
        foreach ($datas->getItems() as $index => $data) {
            if ($indexCounter == $limit){
                break;
            }
            $item = $data->asArray();
            ##### media_type => 1-> Resim, 2-> Video, 8-> Carausel #####
            switch ($item["media_type"]){

                case 1:{
                    $url = $item["image_versions2"]["candidates"][0]["url"];
                    $indexCounter ++;
                    $posts->push(["type" => "image", "media_url" => $url,"post_url" => $instagramPostUrl.$item["code"],"thumbnail_photo" =>$url]);
                } break;

                case  2:{
                    $url = $item["video_versions"][0]["url"];
                    $indexCounter ++;
                    $posts->push(["type" => "video", "media_url" => $url,"post_url" => $instagramPostUrl.$item["code"],"thumbnail_photo" =>$item["image_versions2"]["candidates"][0]["url"]]);
                } break;

                case 8: {
                    if ($includeCarousel == true){
                        $thumbnailPhoto = "";
                        $indexCounter ++;
                        $carousel = collect();
                        foreach ($item["carousel_media"] as $c){
                            if ($c["media_type"] == 1){
                                $url = $c["image_versions2"]["candidates"][0]["url"];
                                $carousel->push(["type" => "image", "media_url" => $url]);
                            }elseif ($c["media_type"] == 2){
                                $url = $c["video_versions"][0]["url"];
                                $carousel->push(["type" => "video", "media_url" => $url]);
                            }
                            if ($thumbnailPhoto == ""){
                                $thumbnailPhoto = $c["image_versions2"]["candidates"][0]["url"];
                            }
                        }
                        $posts->push(["type" => "carousel", "media_url" => "",  "carousel_media" => $carousel, "post_url" => $instagramPostUrl.$item["code"],"thumbnail_photo" =>$thumbnailPhoto]);
                    }
                }
            }// End of switch case
        }// End of foreach
        return $posts;
    }

    public  function getComments($postId){
        $ig = $this->instagramLogin();
        $comments = $ig->media->getComments($postId)->asArray();
        $result = [];
        if (isset($comments["comments"]))
            foreach($comments["comments"] as $comment){
                array_push($result,["comment" =>$comment["text"], "user" =>$comment["user"]["username"]]);
            }
        return $result;
    }
}
