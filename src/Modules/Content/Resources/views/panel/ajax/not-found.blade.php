<div class="modal-header">
    <span class="close" onclick="popup_kapat()">&times;</span>
    <h2>{{trans("panel::ajax.not_found.title")}}</h2>
</div>

<div class="modal-body">
    <p><b>{{trans("panel::ajax.not_found.content")}}</b></p>
</div>