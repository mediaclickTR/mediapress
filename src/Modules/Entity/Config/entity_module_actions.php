<?php
return [


    "entity" => [
        'name' => 'EntityPanel::auth.sections.entity_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [
            "users" => [
                'name' => 'EntityPanel::auth.sections.users',
                'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Entity\Models\User::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => true,
                    "is_variation" => false,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'subs' => [
                    "user_lists" => [
                        'name' => 'EntityPanel::auth.sections.user_lists',
                        'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                        'item_model' => \Mediapress\Modules\Entity\Models\Userlist::class,
                        'item_id' => "*",
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ]
                    ]
                ],
            ],
            "entities" => [
                'name' => 'EntityPanel::auth.sections.entities',
                'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Entity\Models\Entity::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => true,
                    "is_variation" => false,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'subs' => [
                    "entity_lists" => [
                        'name' => 'EntityPanel::auth.sections.entity_lists',
                        'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                        'item_model' => \Mediapress\Modules\Entity\Models\EntityList::class,
                        'item_id' => "*",
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ]
                    ]
                ],
            ],
        ]
    ]
];
