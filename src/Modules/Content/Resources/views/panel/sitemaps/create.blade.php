@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! __("ContentPanel::sitemap.create_sitemap") !!}</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    @include("ContentPanel::sitemaps.form")
                </div>
            </div>
        </div>
    </div>
@endsection
