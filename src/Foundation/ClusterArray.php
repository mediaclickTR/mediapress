<?php
namespace Mediapress\Foundation;
/**
 * ClusterArray class.
 *
 * @author Görkem Duymaz <gorkemduymaz@gmail.com>
 */
class ClusterArray
{
    public const CHILDREN = 'children';
    public const DEPTH = 'depth';
    public const LFT = 'lft';
    public const RGT = 'rgt';


    public $table_id = 'parent';

    /**
     * Make array recursive and cluster
     *
     * @param array $elements
     * @param int $recursive
     * @param int $max
     * @param int $depth
     * @return array
     */
    public function cluster(array $elements, $recursive = 0, $max = 0, $depth = 0)
    {

        foreach ($elements as $element)
        {
            if (! isset($parentId) || $element[$this->table_id] < $parentId)
            {
                $parentId = $element[$this->table_id];
            }
        }

        if (! isset($parentId)){ return array(); }

        $cluster = $this->buildTree($elements, $parentId, $max, $depth);
        if ($recursive)
        {
            return $cluster;
        }

        return $this->unBuildTree($cluster);
    }

    /**
     *  Re-sort array non recursive
     *
     * @param $elements
     * @param array $branch
     * @return array
     */
    public function unBuildTree($elements, $branch = [])
    {
        foreach ($elements as $element)
        {

            if (isset($element[self::CHILDREN]))
            {
                $branch = $this->unBuildTree($element[self::CHILDREN], $branch);
                unset($element[self::CHILDREN]);
            }
            $branch[] = $element;
        }
        return $branch;
    }

    /**
     * Sort array as recursive
     *
     * @param array $elements
     * @param $parentId
     * @param int $max
     * @param int $depth
     * @return array
     */
    public function buildTree(array $elements, $parentId, $max = 0, $depth = 0)
    {

        $branch = array();
        foreach ($elements as $element)
        {

            if ($element[$this->table_id] == $parentId)
            {

                $element[self::LFT] = $max = $max + 1;
                $element[self::RGT] = $max + 1;
                $element[self::DEPTH] = $depth;
                if (isset($element[self::CHILDREN]))
                {
                    $children = $this->buildTree($element[self::CHILDREN], $element['id'], $max, $depth + 1);
                    if ($children)
                    {

                        $element[self::RGT] = $max = (isset(end($children)[self::RGT]) ? end($children)[self::RGT] : 1) + 1;
                        $element[self::CHILDREN] = $children;
                    } else
                    {
                        $element[self::RGT] = $max = $max + 1;
                    }
                }

                $branch[] = $element;
            }
        }

        return $branch;
    }

    public function buildFromDB(array $elements, $parentId = null)
    {
        $min = 99999999;
        if (! $parentId)
        {
            foreach ($elements as $element)
            {
                $min = min($min, $element[$this->table_id]);
            }
            $parentId = $min;
        }

        return $this->buildArray($elements, $parentId);
    }

    public function buildArray(array $elements, $parentId, $max = 0, $depth = 0)
    {
        $branch = array();

        foreach ($elements as $element)
        {
            if ($element[$this->table_id] == $parentId)
            {
                $element[self::LFT] = $max = $max + 1;
                $element[self::RGT] = $max + 1;
                $element[self::DEPTH] = $depth;
                $children = $this->buildArray($elements, $element['id'], $max, $depth + 1);

                if ($children)
                {
                    $element[self::RGT] = $max = end($children)[self::RGT] + 1;
                    $element[self::CHILDREN] = $children;
                } else
                {
                    $element[self::RGT] = $max = $max + 1;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    /**
     * Sort array as recursive and macth and put the parameter by id to id
     *
     * @param array $elements
     * @param array $parameters
     * @param int $id
     * @param int $parentId
     * @param int $max
     * @param int $depth
     * @return array
     */
    public function buildTreeWithNestableParameters(array $elements, $parameters, $id, $parentId, $max = 0, $depth = 0)
    {
        $parameters = $parameters == null ? [] : $parameters;
        $branch = array();
        foreach ($elements as $element)
        {
// parameter adder loop
            foreach ($parameters as $parameter)
            {
                if ($parameter['id'] == $element['id'])
                {
                    $element['parameter'] = $parameter['fields'];
                }
            }
// loop and
            if ($element[$this->table_id] == $parentId)
            {
                $element[self::LFT] = $max = $max + 1;
                $element[self::RGT] = $max + 1;
                $element[self::DEPTH] = $depth;
                $element['menu_part_id'] = $id;
                if (isset($element[self::CHILDREN]))
                {
                    $children = $this->buildTreeWithNestableParameters($element[self::CHILDREN], $parameters, $id, $element['id'], $max, $depth + 1);
                    if ($children)
                    {

                        $element[self::RGT] = $max = (isset(end($children)[self::RGT]) ? end($children)[self::RGT] : 1) + 1;
                        $element[self::CHILDREN] = $children;
                    } else
                    {
                        $element[self::RGT] = $max = $max + 1;
                    }
                }

                $branch[] = $element;
            }
        }

        return $branch;
    }

}
