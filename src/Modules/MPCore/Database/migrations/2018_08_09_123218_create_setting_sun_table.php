<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingSunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->create('setting_sun', function (Blueprint $table) {
            $table->increments('id');
            $table->string('website_id')->nullable();
            $table->string('group')->nullable();
            $table->string('title')->nullable();
            $table->string('key');
            $table->longText('value')->nullable();
            $table->string('vtype')->default('string')->nullable();
            $table->string('params')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_sun');
    }
}
