<?php

namespace Mediapress\Modules\Comment;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CommentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = "Comment";
    protected $namespace = 'Mediapress\Modules\Comment';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'View');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');

        $files = $this->app['files']->files(__DIR__ . '/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias("Comment", Comment::class);
        app()->bind("Comment", function() {
            return new \Mediapress\Modules\Comment\Comment;
        });
    }
}
