<?php

namespace Mediapress\Modules\Locale\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $connection = 'mediapress_locale';
    protected $table = 'koy';
    protected $fillable = ["name"];

    public function county()
    {
        return $this->belongsTo(County::class, 'ilce_id');
    }

    public function neighborhoods()
    {
        return $this->hasMany(Neighborhood::class, 'koy_id');
    }
}
