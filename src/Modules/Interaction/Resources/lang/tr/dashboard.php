<?php

return [

    "other" => "Diğer",

    "title" => "Kontrol Paneli",

    "dates" => [
        'today' => 'Bugün',
        'yesterday' => 'Dün',
        'week' => 'Son 7 Gün',
        'month' => 'Son 30 Gün',
        'three-month' => 'Son 90 Gün',
        'six-month' => 'Son 180 Gün',
        'year' => 'Son 365 Gün',
    ],

    "interaction_title" => "Şu an MediaPress analiz verilerini görüntülüyorsunuz.",
    "google_connect" => "Google Analytics Bağla",
    "google_title" => "Şu an Google Analytics analiz verilerini görüntülüyorsunuz.",
    "google_disconnect" => "Google Analytics Bağlantısını Kopar",

    "google_connect_title" => "Mediapress Google Bağlantı Aracı",

    "gender" => [
        "title" => "Cinsiyet",
        "male" => "Erkek",
        "female" => "Kadın"
    ],

    "ages" => "Yaş Dağılımı",
    "browser" => "Internet Tarayıcı",
    "os" => "İşletim Sistemi",
    "countries" => "Ülkelere Göre Ziyaretçiler",


    "online" => "Şu An Online",
    "load_time" => "Ort Açılış Süresi",
    "visit_time" => "Ort. Oturum Süresi",
    "bounces" => "Hemen Çıkma Oranı",

    "avg_visit" => "Ortalama Sayfa Ziyareti",

    "platforms" => [
        "title" => "Platform",
        "hourly" => "Saatlere Göre Haftalık Yoğunluk",
        "referers" => "Trafik Kanalları",
    ],

    "stats" => [
        "refresh" => "Yenile",
        "visits" => "Sayfa Görüntüleme",
        "session" => "Oturum",
        "cookie" => "Kullanıcı",
    ],

    "most_visited" => "En Çok Ziyaret Edilen Sayfalar",
    "affinities" => "Ziyaretçilerin İlgi Alanları",
];
