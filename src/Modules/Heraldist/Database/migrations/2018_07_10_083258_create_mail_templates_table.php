<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('mail_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mail_title');
            $table->string('mail_subject');
            $table->longText('mail_detail');
            $table->longText('body');
            $table->softDeletes();
            $table->timestamps();
        });

        // Pivot With websites
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('mail_template_website', function (Blueprint $table) {
            $table->integer('mail_template_id');
            $table->integer('website_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_templates');
        Schema::dropIfExists('mail_template_website');
    }
}
