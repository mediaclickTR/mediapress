<?php

return [
    'code_success' => 'Şifreniz Doğru!!',
    'code_error' => 'Şifreniz Geçersiz!!',
    'verify_btn' => 'Aktif Et',

    'register' => [
        'title' => "Google Authenticator Aktif Et",
        'description_1' => "Google Authenticator uygulamasını App Store ya da Google Play'den cep telefonunuza indirip kurun..",
        'description_2' => "Uygulamaya aşağıdaki QR kodu okutarak ya da kurulum anahtarını girerek ekleyin.",
        'description_3' => "Aşağıdaki kutuya mobil uygulamadaki 6 haneli kodu girin.",
        'secret_key' => "Kurulum Anahtarı: ",
    ],

    'verify' => [
        'title' => 'Google Authentication Şifresi',
        'login' => 'Giriş Yap',
    ],

    'warning' => [
        'title' => "<strong>Google 2FA</strong> doğrulama hesabınız aktif değil!",
    ]
];
