<?php

namespace Mediapress\Providers;

use Mediapress\Console\Terminal\Application;
use Mediapress\Console\Terminal\Kernel;
use Mediapress\Console\Terminal\TerminalManager;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class TerminalServiceProvider extends ServiceProvider
{
    /**
     * namespace.
     *
     * @var string
     */
    public const CONFIG = 'config';
    public const TERMINAL = 'terminal';


    /**
     * Bootstrap any application services.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Routing\Router $router
     */
    public function boot(Request $request, Router $router)
    {
        $config = $this->app[self::CONFIG][self::TERMINAL];
       /* if (in_array($request->getClientIp(), Arr::get($config, 'whitelists', [])) === true || Arr::get($config, 'enabled') === true) {

        }*/

        if ($this->app->runningInConsole() === true) {
            $this->handlePublishes();
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../Config/terminal.php', self::TERMINAL);

        $this->app->singleton(Application::class, function ($app) {
            $config = $app['con'.'fig']['term'.'inal'];
            $commands = $config['commands'];
            $artisan = new Application($app, $app['events'], $app->version());
            $artisan->resolveCommands($commands, true);

            return $artisan;
        });

        $this->app->singleton(Kernel::class, Kernel::class);

        $this->app->singleton(TerminalManager::class, function ($app) {
            $config = $app['config']['terminal'];

            return new TerminalManager($app->make(Kernel::class), array_merge($config, [
                'basePath' => $app->basePath(),
                'environment' => $app->environment(),
                'version' => $app->version(),
                'endpoint' => $app['url']->route(Arr::get($config, 'route.as').'endpoint'),
            ]));
        });
    }


    /**
     * handle publishes.
     */
    protected function handlePublishes()
    {

        $this->publishes([
            __DIR__.'/../Assets/terminal' => $this->app['path.public'].'/vendor/terminal',
        ], self::TERMINAL);
    }
}
