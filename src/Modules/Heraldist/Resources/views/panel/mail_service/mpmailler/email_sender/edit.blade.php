@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("MPCorePanel::general.edit") !!}</h1>
        <div class="col-md-12 pa">
                <form action="{!! route('Heraldist.mpmailler.mail_templates.update') !!}" method="POST">
                    @include("HeraldistPanel::mail_service.mpmailler.email_sender.form")
                </form>
        </div>
    </div>
@endsection
