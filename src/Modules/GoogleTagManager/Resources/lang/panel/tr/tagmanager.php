<?php


return [

    'connection_warning' => 'Google Tag Manager bilgileriniz eşlenemedi. Lütfen bilgilerinizi kontrol ediniz.',
    'google_connect' => 'Google Tag Manager ile Bağlan',
    'nodata' => 'Google Tag Managerdan veri alınamadı lütfen sayfayı yenileyin'

];