<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyPageTable extends Migration
{

    public const PROPERTY_PAGE = 'property_page';

    public function up()
    {
        if (!Schema::hasTable(self::PROPERTY_PAGE)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::PROPERTY_PAGE, function (Blueprint $table) {
                $table->integer('property_id')->unsigned()->index();
                $table->integer('page_id')->unsigned()->index();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::PROPERTY_PAGE);
    }
}
