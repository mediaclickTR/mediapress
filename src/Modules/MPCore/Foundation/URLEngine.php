<?php

namespace Mediapress\Modules\MPCore\Foundation;

use Illuminate\Support\Facades\DB;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\Country;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\RedirectUrl;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

final class URLEngine
{

    /**
     * Type for PAGES' AND SITEMAPS' MAIN URLS
     */
    const URL_TYPE_ORIGINAL = "original";

    /**
     * Dynamic type for a RedirectURL model
     */
    const URL_TYPE_REDIRECT = "redirect";

    /**
     * Type for CATEGORIES' MAIN URLS
     */
    const URL_TYPE_CATEGORY = "category";

    /**
     * Type for PAGES' AND SITEMAPS' RESERVED URLS
     */
    const URL_TYPE_RESERVED = "reserved";

    /**
     * Type for SITEMAPS' RESERVED CATEGORY URLS
     */
    const URL_TYPE_SMCRESERVED = "sm_cat_reserved";


    public function create($website, $url, $model, $type = null)
    {
        $_website_id = null;
        $_url = $url;
        $_type = null;

        try{

            DB::beginTransaction();

            //Parse website attribute
            if (is_a($website, Website::class)) {
                $_website_id = $website->id;
            } else if (is_int($website)) {
                $_website_id = $website;
            } else {
                throw new \Exception(__CLASS__ . " : Website attribute invalid", __LINE__);
            }

            //Parse url attribute
            if (!is_string($_url)) {
                throw new \Exception(__CLASS__ . " : Url path invalid must be string", __LINE__);
            }

            //Parse model attribute
            if ($model == null || !is_a($model, Model::class) || !method_exists(get_class($model), "url")) {
                throw new \Exception(__CLASS__ . " : Model invalid should be extends Model inside Eloquent and has url morph ", __LINE__);
            }

            $_type = is_null($type) || !$type ? self::URL_TYPE_ORIGINAL : $type;

            //Check redirect table for urls
            list($_exist, $__url, $__type) = $this->checkUrl($_website_id, $_url);

            if ($__type == self::URL_TYPE_REDIRECT) {
                $__url->delete();
            } else if ($__type == self::URL_TYPE_ORIGINAL || $__type == self::URL_TYPE_CATEGORY || $__type == self::URL_TYPE_RESERVED) {
                throw new \Exception(__CLASS__ . " : Big mistake here, url must not inside in url table", __LINE__);
            }

            $this->destroyTrashedUrl($_website_id, $_url);

            $_url = new Url(["website_id" => $_website_id, "url" => $_url, "type" => $_type]);
            //dump($_url,65465);
            $model->url()->save($_url);

            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return [false, $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine()];

        }
        return [true, null];


    }

    /**
     * @param Website|int|string $website
     * @param Url|RedirectUrl|int|string $url
     * @param null|Model $model
     * @param null|Model $ignore_active_url_for_model
     * @param boolean $orderOfUrlTable false is URL - Redirect | true is Redirect URL
     * @return array [ exists , urlModel, type ]
     * @throws \Exception
     */
    public function checkUrl($website, $url, $model = null, $ignore_active_url_for_model = null, $check_real_urls = true, $check_redirect_urls = true)
    {
        $_website_id = null;
        $_model_type = null;
        $_model_id = null;
        $_url = null;

        // region Regulate Params
        //Parse website attribute
        if (is_a($website, Website::class)) {
            $_website_id = $website->id;
        } else if (is_int($website)) {
            $_website_id = $website;
        } else if (is_string($website)) {
            $website = Website::where("slug", $website)->first();
            $_website_id = $website->id;
        } else {
            throw new \Exception(__CLASS__ . " : Website attribute invalid", __LINE__);
        }

        //parse model attribute
        if (is_a($model, Model::class)) {
            $_model_type = get_class($model);
            $_model_id = $model->id;
        } else if (is_array($model) && count($model) == 2 && is_string($model[0]) && is_int($model[1])) {
            list($_model_type, $_model_id) = $model;
        }

        //parse url attribute
        if (is_a($url, Url::class) || is_a($url, RedirectUrl::class)) {
            $_url = $url;
            $_type = $url->type;
            if ($_url->website_id != $_website_id) {
                //Nothing found
                return [false, null, null];
            }
        } else if (is_int($url)) {
            $_where["id"] = $url;
        } else if (is_string($url)) {
            $_where["url"] = strlen($url) && $url[0] != "/" ? "/" . $url : $url;
        } else {
            throw new \Exception(__CLASS__ . ": URL format is not an operatable one. ", __LINE__);
        }

        // endregion

        if ($_url == null) {
            $_where += ["website_id" => $_website_id];
            if ($_model_type != null && $_model_id != null) {
                $_where += ["model_type" => $_model_type, "model_id" => $_model_id];
            }
            if ($check_real_urls) {
                $_url = Url::where($_where)->first();
                if ($_url) {
                    $_type = $_url->type;
                }
            }
            if (!$_url) {
                if ($check_redirect_urls) {
                    $_url = RedirectUrl::where($_where)->first();
                    if ($_url) {
                        $_type = self::URL_TYPE_REDIRECT;
                    }
                }
            }
        }

        if ($_url) {
            /*dump(242544);
            dump($_url->model_type);
            dump($_url->model_id);
            dump($ignore_active_url_for_model);
            dump(get_class($ignore_active_url_for_model));
            $ignore_active_url_for_model->id,
            dump($_type);*/
            if (!is_null($ignore_active_url_for_model) && $_type != "redirect") {
                //dump(254132);
                $ignore_model_type = get_class($ignore_active_url_for_model);
                $ignore_model_id = $ignore_active_url_for_model->id;
                if ($_url->model_type == $ignore_model_type && $_url->model_id == $ignore_model_id) {
                    return [false, null, null];
                }
            }
            return [$_url->exists, $_url, $_type];
        } else {
            return [false, null, null];
        }
    }

    /**
     * @param Website|int $website
     * @param Url|int|string $url
     * @throws \Exception
     */
    public function delete($website, $url)
    {
        list($_exist, $_url, $_type) = $this->checkUrl($website->id, $url);
        if ($_type != self::URL_TYPE_REDIRECT) {
            RedirectUrl::where(["model_type" => get_class($_url), "model_id" => $_url->id])->delete();
            $_url->delete();
        } else {
            $_url->delete();
        }
    }

    /**
     * @param $website_model_or_id
     * @param $current_url_str
     * @param null $new_url_string
     * @param null $urlable_model
     * @param null $new_type
     * @return array
     * @throws \Exception
     */
    public function update($website_model_or_id, $current_url_str, $new_url_string = null, $urlable_model = null, $new_type = null)
    {
        if (!$this->validateUrlableModel($urlable_model)) {
            return [false, "The model sent is not an eloquent model."];
        }
        if (!is_null($new_url_string) && !is_string($new_url_string)) {
            return [false, " : New url text is not a valid url string."];
        }

        $website_id = $this->websiteID($website_model_or_id);
        if (!$website_id) {
            return [false, "Website attribute invalid (should be a website object or id or slug)"];
        }

        $redirect2delete = null;
        $redirect2save = null;
        $url2save = null;

        // for new url string
        list(
            $model_for_new_url_str_exist,
            $intended_new_url_model,
            $type_of_existing_url_model_for_new_str
            ) = $this->checkUrl($website_id, $new_url_string);

        // for existing url string

        list(
            $exists,
            $existing_url,
            $current_type
            ) = $this->checkUrl($website_id, $current_url_str);

        //return dd($model_for_new_url_str_exist, $intended_new_url_model, $type_of_existing_url_model_for_new_str, $website_id, $new_url_string,2837);

        if ($model_for_new_url_str_exist && $type_of_existing_url_model_for_new_str !== "redirect") {
            //dump("Yeni URL dizesini taşıyan bir model zaten var!!", $intended_new_url_model);
            //if () {
                return [false, "Url with this string is already in active use. URL ID (" . $intended_new_url_model->id . ")"];
            //}else{ //unnecessary "else", here just for cognition
                // TODO: Important:
                // While there is a redirect url model for the desired slug at this point,
                // as long as this is an update,
                // there must be a url instance already pointing this urlable.
                // Update it instead of creating one.
                /*$url2save = $this->createUrlModelFromRedirectedUrl($intended_new_url_model, $urlable_model, $new_type);
                $redirect2delete = $intended_new_url_model;*/
            //}
        } else {
            if($model_for_new_url_str_exist){
                $redirect2delete = $intended_new_url_model;
            }
            //dump("Yeni dize ($new_url_string) URL olarak kullanılabilir.");
            list($exists, $existing_url, $current_type) = $this->checkUrl($website_id, $current_url_str);
            if (!$exists || (!(is_a($existing_url, Url::class)))) {
                return [false, "Could not find any existing URL model which has url  \"" . $current_url_str . "\" to update"];
            }
            $url2save = $existing_url;
            if ($new_url_string && $new_url_string != $existing_url->url) {
                $url2save->url = $new_url_string;
            }

            if (!in_array($existing_url->type, ["reserved", "sm_cat_reserved"])) {
                //dump($existing_url,75858);
                $redirect2save = $this->createRedirectModelFor($existing_url);
                //dump($redirect2save,346);
            }
            //dump("Şu tipin uygulanması isteniyor: $new_type");
            if ($new_type && $new_type != $existing_url->type) {
                $url2save->type = $new_type;
            }


        }

        DB::beginTransaction();

        try {

            if ($redirect2delete) {
                $redirect2delete->forceDelete();
            }

            $this->destroyTrashedUrl($website_id, $new_url_string);

            if ($redirect2save) {
                $redirect2save->save();
            }

            $url2save->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return [false, $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine()];
        }

        return [true, null];

    }

    /**
     * @param Website|int $website
     * @param Url|int|string $url
     * @return array it has [ isExist , urlModel, type ]
     * @throws \Exception
     */
    public function exists($website, $url)
    {
        return $this->checkUrl($website->id, $url);
    }

    public function destroyTrashedUrl($website_or_id, $url_string)
    {
        $website_id = is_numeric($website_or_id) ? $website_or_id * 1 : $website_or_id->id;
        if (!strlen($url_string)) {
            return;
        }
        if ($url_string[0] != "/") {
            $url_string = "/" . $url_string;
        }
        $url = Url::where('website_id', $website_id)->where('url', $url_string)->withTrashed()->first();
        if ($url && !is_null($url->deleted_at)) {
            $fd = $url->forceDelete();
        }
    }


    private function validateUrlableModel($model = null)
    {

        if ($model != null && !is_a($model, Model::class)) {
            return [false, "The model sent is not an eloquent model."];
        }

        if ($model != null && !method_exists(get_class($model), "url")) {
            return [false, "The model sent has no url relationship defined."];
        }

        return [true, ""];

    }

    /* region Helpers */
    private function createRedirectModelFor(Url $url_model)
    {
        return new RedirectUrl(
            Arr::only($url_model->getOriginal(), ["website_id", "url"])
            + [
                "model_type" => get_class($url_model),
                "model_id" => $url_model->id,
            ]
        );
    }

    private function createUrlModelFromRedirectedUrl(RedirectUrl $redirect_url_model, $detail_model, $new_url_type)
    {

        $url = (new Url(
            Arr::only($redirect_url_model->getOriginal(), ["website_id", "url"])
            + [
                "type" => $new_url_type,
            ]
        ));
        $url->model_type = get_class($detail_model);
        $url->model_id = $detail_model->id;
        return $url;

    }

    private function websiteID($website_param = null)
    {

        if (is_a($website_param, Website::class)) {
            $_website_id = $website_param->id;
        } else if (is_int($website_param)) {
            $_website_id = $website_param;
        } else if (is_string($website_param)) {
            $website = Website::where("slug", $website_param)->first();
            $_website_id = $website->id;
        } else {
            return false;
        }

        return $_website_id;
    }

    /* endregion */

    public function checkPattern($website, $pattern, $slug, $ignore_active_url_for_model = null)
    {

        $this->cleanURL($pattern);
        $this->cleanURL($slug);

        //dump(325413, get_class($ignore_active_url_for_model), $ignore_active_url_for_model->id);

        $slug = Str::slug(Str::ascii($slug));
        $i = -1;
        do {
            $i++;
            $a = $this->checkUrl($website, "/" . $pattern . "/" . $slug . ($i > 0 ? "-$i" : ""), null, $ignore_active_url_for_model);
            list($_exist, $_url, $_type) = $a;
        } while ($_exist && $_type != self::URL_TYPE_REDIRECT);
        return ["pattern" => $pattern, "slug" => $slug . ($i > 0 ? "-$i" : "")];
    }

    public static function cleanURL(&$path)
    {
        return $path = trim(preg_replace("/\/{2,}/", '/', $path), '/');
    }


    private function patternReplacer(&$path, $data)
    {
        preg_match_all('/\{([^}]+)\}/', $path, $patterns);
        if(count($patterns)>1){
            $string = "";
            foreach ($patterns[1] as $pattern){
                if($pattern!='cg' && $pattern!='lng' && strpos($pattern,'?')>0){
                    $specialChar = $pattern;
                    $expression = explode('?',$pattern);
                    $condition = $expression[0];
                    if(in_array($condition,array_keys($data)) && $data[$condition]!=''){
                        $string = $expression[1];
                    }
                    if($string!=""){
                        $path = str_replace('{'.$specialChar.'}',$string,$path);
                    }else{
                        $path = str_replace('{'.$specialChar.'}','',$path);

                    }
                }
            }
        }

        return $path = str_ireplace(array_map(function ($i) {
            return $i == '_' ? $i : "{{$i}}";
        }, array_keys($data)), array_values($data), $path);
    }

    public function getPattern($website, $language, $country = null, $sitemaps = null, $categories = null)
    {
        $_pattern = "";
        $_website = null;
        $_language = null;
        $_country = null;
        $_sitemaps = [];
        $_categories = [];

        if (is_a($language, Language::class)) {
            $_language = $language;
        } else if (is_int($language)) {
            $_language = Language::find($language);
        } else if (is_string($language)) {
            $_language = Language::where("code", $language)->first();
        } else {
            throw new \Exception(__CLASS__ . " : Language attribute invalid", __LINE__);
        }

        if ($country != null) {
            $_country = $country;
            /*if (is_a($country, Country::class)) {
                $_country = $country;
            } else if (is_int($country)) {
                $_country = Country::find($country);
            } else if (is_string($country)) {
                $_country = Country::where(["code" => mb_strtoupper($country)])->first();
                dd($_country);
            } else {
                throw new \Exception(__CLASS__ . " : Country attribute invalid", __LINE__);
            }*/
        }

        if (is_a($website, Website::class)) {
            $_website = $website;
        } else if (is_int($website)) {
            $_website = Website::find($website);
        } else if (is_string($website)) {
            $_website = Website::where("slug", $website)->first();
        } else {
            throw new \Exception(__CLASS__ . " : Website attribute invalid", __LINE__);
        }

        if ($_website->use_deflng_code || $_website->defaultLanguage()->id != $_language->id) {
            $_pattern .= "/" . $_website->variation_template;
        }

        if (!is_null($sitemaps)) {
            if (!is_array($sitemaps))
                $sitemaps = [$sitemaps];
            $i = 0;
            foreach ($sitemaps as $sitemap) {
                $_sitemap = null;
                if (is_a($sitemap, Sitemap::class)) {
                    $_sitemap = $sitemap;
                } else if (is_int($sitemap)) {
                    $_sitemap = Sitemap::find($sitemap);
                } else {
                    throw new \Exception(__CLASS__ . " : Sitemap attribute invalid", __LINE__);
                }

                $_tmp_sd = $_sitemap->details()->where(["language_id" => $_language->id]);
                if (!is_null($_country)) {
                    $_tmp_sd = $_tmp_sd->whereHas("countryGroup", function ($q) use ($_country) {
                        $q->where("code", $_country->code);
                    });
                }
                $_tmp_sd = $_tmp_sd->first();
                if (!is_null($_tmp_sd)) {
                    $_pattern .= "/{S$i}";
                    $_sitemaps["S" . $i] = $_tmp_sd->slug;
                }
            }
        }

        if (!is_null($categories)) {
            if (!is_array($categories))
                $categories = [$categories];
            $i = 0;
            foreach ($categories as $category) {
                $_category = null;
                if (is_a($category, Category::class)) {
                    $_category = $category;
                } else if (is_int($category)) {
                    $_category = Category::find($category);
                } else {
                    throw new \Exception(__CLASS__ . " : Category attribute invalid", __LINE__);
                }

                $_pattern .= "/{C$i}";
                $_tmp_cat = $_category->detail()->where(["language_id" => $_language->id]);
                $_categories["C" . $i] = $_tmp_cat->first()->slug;
            }
        }

        if (!is_null($_country)) {
            //$_country = $_country->code;
        }
        $path = $this->patternReplacer($_pattern, ["cg" => $_country, "lng" => $_language->code] + $_sitemaps + $_categories);
        $this->cleanURL($path);
        return $path;
    }

    public function generateUrlString($obj, $country_group, $language, $slug)
    {
        /** @var Language $_language */
        $_language = null;
        /** @var CountryGroup $_country */
        $_country = null;

        $_language = objectifyOneParam(Language::class, $language);
        if (!$_language) {
            throw new \Exception(__CLASS__ . " : Language attribute invalid", __LINE__);
        }


        $_where = ["language_id" => $_language->id];
        $_replacer = [];
        if ($country_group != null) {
            $_country = objectifyOneParam(CountryGroup::class, $country_group);
            if (!$country_group) {
                throw new \Exception(__CLASS__ . " : Country attribute invalid", __LINE__);
            }
            $_where["country_group_id"] = $_country->id;
            $_replacer["cg"] = $_country->code != "gl" ? $_country->code : "";
        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
        }

        $_return = null;
        $type = is_array($obj) ? $obj["type"] : get_class($obj);
        if ($type === SitemapDetail::class) {
            $_return = $this->getPatternForSitemapDetail($obj, $_country, $_language, $slug);
        } else if ($type === PageDetail::class) {
            $_return = $this->getPatternForPageDetail($obj, $_country, $_language, $slug);
        } else if ($type === CategoryDetail::class) {
            $_return = $this->getPatternForCategoryDetail($obj, $_country, $_language, $slug);
        } else {
            throw new \Exception(__CLASS__ . " : obj attribute invalid. Should be one of SitemapDetail, PageDetail or CategoryDetail", __LINE__);
        }


        return array_combine(["pattern", "slug"], $_return);

    }

    public function getPatternForPageDetail($pd, $cg, $lg, $sl)
    {
        /** @var Sitemap $_sitemap */
        //dump($pd->parent->sitemap);

        $_sitemap = is_array($pd) ? $pd["sitemap"] : $pd->parent->sitemap;

        /** @var SitemapDetail $_sitemap_detail */
        $_sitemap_detail = null;
        /** @var Website $_website */
        $_website = is_array($pd) ? $pd["website"] : $_sitemap->websites->first();
        /** @var Language $_language */
        $_language = $lg;
        /** @var CountryGroup $_country */
        $_country = $cg;
        /** @var string $_slug */
        $_slug = $sl;

        $_where = ["language_id" => $_language->id];
        $_replacer = [];
        if ($_country != null && $_country->code != "gl") {
            $_where["country_group_id"] = $_country->id;
            $_replacer["cg"] = $_country->code;
            $_sitemap_detail = $_sitemap->details()->where("country_group_id", $_country->id)->where("language_id", $_language->id)->first();

        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
            $_sitemap_detail = $_sitemap->details()->where("language_id", $_language->id)->first();
        }

        $_replacer += [
            "lng" => !$_website->use_deflng_code && $_website->defaultLanguage()->id == $_language->id ? "" : $_language->code,
            "smd" => $_sitemap_detail->slug,
            "sl" => Str::slug(Str::ascii($_slug))
        ];

        $_template = $_website->variation_template . "/{smd}/";
        $_tmp = "{sl}";

        $path1 = $this->patternReplacer($_template, $_replacer);
        $path2 = $this->patternReplacer($_tmp, $_replacer);

        return [
            $this->cleanURL($path1),
            $this->cleanURL($path2)
        ];
    }

    public function getPatternForSitemapDetail($_sitemap_detail, $_country_group, $_language, $_slug)
    {
        /** @var Sitemap $_sitemap */
        $_sitemap = is_array($_sitemap_detail) ? $_sitemap_detail["sitemap"] : $_sitemap_detail->sitemap;
        /** @var Website $_website */
        $_website = is_array($_sitemap_detail) ? $_sitemap_detail["website"] : $_sitemap->websites->first();

        $_where = ["language_id" => $_language->id];
        $_replacer = [];
        if ($_country_group != null && $_country_group->code != "gl") {
            $_where["country_group_id"] = $_country_group->id;
            $_replacer["cg"] = $_country_group->code;
        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
        }

        $_replacer += [
            "lng" => !$_website->use_deflng_code && $_website->defaultLanguage()->id == $_language->id ? "" : $_language->code,
            "sl" => Str::slug(Str::ascii($_slug))
        ];
        $_template = $_website->variation_template;
        $_template = $this->patternReplacer($_template, $_replacer);


        $_tmp = "{sl}";
        $_tmp = $this->patternReplacer($_tmp, $_replacer);

        return [
            $this->cleanURL($_template),
            $this->cleanURL($_tmp)
        ];
    }

    public function getPatternForCategoryDetail($_category_detail, $_country, $_language, $_slug)
    {
        /** @var Sitemap $_sitemap */
        $_sitemap = is_array($_category_detail) ? $_category_detail["sitemap"] : $_category_detail->sitemap;
        /** @var SitemapDetail $_sitemap_detail */
        $_sitemap_detail = null;
        /** @var Website $_website */
        $_website = is_array($_category_detail) ? $_category_detail["website"] : $_sitemap->websites->first();

        $_where = ["language_id" => $_language->id];
        $_replacer = [];
        if ($_country != null && $_country->code != "gl") {
            $_where["country_group_id"] = $_country->id;
            $_replacer["cg"] = $_country->code;
        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
        }

        $_sitemap_detail = $_sitemap->details()->where($_where)->first();

        $_replacer += [
            "lng" => !$_website->use_deflng_code && $_website->defaultLanguage()->id == $_language->id ? "" : $_language->code,
            "smd" => $_sitemap_detail->category_slug,
            "sl" => Str::slug(Str::ascii($_slug))
        ];

        $_template = $_website->variation_template . "/{smd}/";
        $_tmp = "{sl}";

        $path1 = $this->patternReplacer($_template, $_replacer);
        $path2 = $this->patternReplacer($_tmp, $_replacer);

        /*$_template = $_website->variation_template;
        $_tmp = "{smd}/{sl}";

        $template = $this->patternReplacer($_template, $_replacer);
        $tmp = $this->patternReplacer($_tmp, $_replacer)."";*/
        return [
            $this->cleanURL(/*$template*/ $path1),
            $this->cleanURL(/*$tmp*/ $path2)
        ];
    }

    public function getSitemapUrlBase($sitemap_or_id, $cg_or_id, $lng_or_id, $urlbasefor = 'Mediapress\Modules\Content\Models\PageDetail', $slug = "")
    {

        $sitemap = objectifyOneParam('Mediapress\Modules\Content\Models\Sitemap', $sitemap_or_id);
        /** @var CountryGroup $country_group */
        $country_group = objectifyOneParam('Mediapress\Modules\MPCore\Models\CountryGroup', $cg_or_id);
        $language = objectifyOneParam('Mediapress\Modules\MPCore\Models\Language', $lng_or_id);

        /** @var Sitemap $sitemap */
        /** @var SitemapDetail $sitemap_detail */
        /** @var Website $website */
        $sitemap_detail = null;
        $website = $sitemap->websites->first();

        $_where = ["language_id" => $language->id];
        $_replacer = [];
        if ($country_group != null && $country_group->code != "gl") {
            $_where["country_group_id"] = $country_group->id;
            $_replacer["cg"] = $country_group->code;
        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
        }

        $sitemap_detail = $sitemap->details()->withTrashed()->where($_where)->first();

        $smd_slug =
            $urlbasefor == 'Mediapress\Modules\Content\Models\CategoryDetail' ?
                $sitemap_detail->category_slug :
                $sitemap_detail->slug;

        $_replacer += [
            "lng" => !$website->use_deflng_code && $website->defaultLanguage()->id == $language->id ? "" : $language->code,
            "smd" => $smd_slug,
            "sl" => Str::slug(Str::ascii($slug))
        ];

        $_template = $website->variation_template . "/{smd}/";
        $_tmp = "{sl}";

        $path1 = $this->patternReplacer($_template, $_replacer);
        $path2 = $this->patternReplacer($_tmp, $_replacer);

        return [
            $this->cleanURL($path1),
            $this->cleanURL($path2)
        ];
    }


    public function getWebsiteUrlBase($website_or_id, $cg_or_id, $lng_or_id, $slug = "")
    {

        $website = objectifyOneParam('Mediapress\Modules\Content\Models\Website', $website_or_id);
        /** @var CountryGroup $country_group */
        $country_group = objectifyOneParam('Mediapress\Modules\MPCore\Models\CountryGroup', $cg_or_id);
        $language = objectifyOneParam('Mediapress\Modules\MPCore\Models\Language', $lng_or_id);

        $_replacer = [
            "lng" => !$website->use_deflng_code && $website->defaultLanguage()->id == $language->id ? "" : $language->code,
            "cg" => "",
            "sl" => Str::slug(Str::ascii($slug))
        ];
        $_where = ["language_id" => $language->id];
        if ($country_group != null && $country_group->code != "gl") {
            $_where["country_group_id"] = $country_group->id;
            $_replacer["cg"] = $country_group->code;
        } else {
            $_replacer["cg"] = "";
            $_replacer["_"] = "";
        }

        $_template = $website->variation_template;
        $_tmp = "{sl}";

        $path1 = $this->patternReplacer($_template, $_replacer);
        $path2 = $this->patternReplacer($_tmp, $_replacer);

        if ($country_group != null && $country_group->code == "gl") {
            $path1 = str_replace('_', '', $path1);
        }

        return [
            $this->cleanURL($path1),
            $this->cleanURL($path2)
        ];

    }
}
