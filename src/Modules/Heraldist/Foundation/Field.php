<?php

namespace Mediapress\Modules\Heraldist\Foundation;


use ArrayAccess;
use Illuminate\Http\Request;
use Mediapress\Modules\Heraldist\Controllers\WebFormController;


class Field implements ArrayAccess
{

    public const REQUIRED = 'required';
    public const RULES = 'rules';
    public const VALUE = 'value';
    public const CLASS_NAME = 'className';
    public const PLACEHOLDER = 'placeholder';

    public function __construct($item)
    {
        foreach ($item as $key => $value) {
            $this->{$key} = $value;
        }

        if((isset($this->attrs[self::REQUIRED]) && $this->attrs[self::REQUIRED]) || (isset($this->attrs[self::RULES]) && strpos($this->attrs[self::RULES], self::REQUIRED) !== false)){
            $request = Request::capture();
            if($request->method() == 'POST'){
                $referer = $request->header('referer');
                $language = WebFormController::getLanguage($request, $referer);

                $this->config['label'] = langPart(strip_tags($this->config['label']),  strip_tags($this->config['label']),[],$language->id).' *';

            }else{
                $referer = $request->header('referer');
                $language = WebFormController::getLanguage($request, $referer);

                $this->config['label'] = langPart(strip_tags($this->config['label']),  strip_tags($this->config['label'])).' *';

            }
            $this->attrs[self::REQUIRED] = true;
        }elseif(isset($this->config['label'] ) && !in_array($this->config['label'],['row','column','Text Input','Email','key'])){
            $this->config['label'] = langPart(strip_tags($this->config['label']),  strip_tags($this->config['label']));
        }

        if($this->options) {

            foreach ($this->options as $key => $option) {

                $this->options[$key]['label'] = langPartAttr(strip_tags($option['label']),  strip_tags($option['label']));

                if(isset($option[self::VALUE]) && !is_numeric($option[self::VALUE])) {
                    $this->options[$key][self::VALUE] = langPartAttr(strip_tags($option[self::VALUE]),  $option[self::VALUE]);
                }
                if(old(hash('ripemd128', $this->id))) {
                    if(old(hash('ripemd128', $this->id)) == $this->options[$key][self::VALUE]) {
                        $this->options[$key]['selected'] = true;
                    } else {
                        $this->options[$key]['selected'] = false;
                    }

                }
            }
        }
    }

    public function attributes($array = [])
    {

        $name = hash('ripemd128', $this->id);

        if (isset($this->attrs['is_array'])) {
            $attributes = 'name="'.$name.'[]" ';
        }else{
            $attributes = 'name="'.$name.'" ';
        }

        $this->name =$name;

        if (in_array($this->tag, ['p', 'div', 'input', 'textarea', 'h1', 'h2', 'h3', 'h4','button'])) {

            if (!isset($this->attrs[self::CLASS_NAME])) {
                $this->attrs[self::CLASS_NAME] = '';
            }

            if(isset($this->attrs['type']) && !in_array($this->attrs['type'], ['radio', 'checkbox'])) {
                if (!isset($this->attrs[self::VALUE])) {
                    $this->attrs[self::VALUE] = '';
                }
                if (!isset($this->attrs['name'])) {
                    $this->attrs[self::VALUE] = '';
                }
            }
            foreach ($this->attrs as $key => $value) {
                if ($key == 'tag' || $key == self::RULES || $key=='name') {
                    continue;
                }
                elseif ($key == self::REQUIRED) {
                    if($value){
                        $attributes .= ' required ';
                    }
                }
                elseif ($key == self::CLASS_NAME) {

                    $attributes .= 'class="' . $this->merge($value, $array, 'class') . '" ';
                }
                elseif ($key == self::VALUE && $this->tag != "button") {
                    if (isset($this->attrs['is_array'])) {
                        $attributes .= ' value="' . old(hash('ripemd128', $this->id).".0",$value) . '" ';
                    }else{
                        $attributes .= ' value="' . old(hash('ripemd128', $this->id),$value) . '" ';
                    }
                }
                elseif ($key == self::PLACEHOLDER) {
                    $attributes .= ' placeholder="' . langPartAttr($value) . '" ';
                }
                elseif ($key == 'data-validation-error-msg') {
                    $attributes .= ' data-validation-error-msg="' . langPartAttr($value) . '" ';
                }
                else {
                    $attributes .= $key . '="' . $this->merge($value, $array, $key) . '" ';
                }
            }

        } else {
            foreach ($this->attrs as $key => $value) {
                if ($key == self::CLASS_NAME) {
                    $attributes .= ' class="' . $this->merge($value) . '" ';
                } else {
                    $attributes .= $key . '="' . $this->merge($value) . '" ';
                }
            }
        }

        return $attributes;
    }

    public function label($required = 1){

        $label = $this->config['label'];
        if(strpos($label,'*')!==false){
            $key = trim(str_replace('*','',$label));
            return langPart(strip_tags($key),$key). ($required ? ' <span class="required-dot">*</span>' :'');
        }
        return langPart(strip_tags($label),$label);
    }

    private function merge($value, $external = [], $key = null)
    {
        if (is_array($value)) {
            $value = @implode(' ', $value);
        }
        if (isset($external[$key])) {
            $value .= ' ' . $external[$key];
        }

        return $value;
    }

    public function __get($key)
    {
        if (isset($this->{$key})) {
            return $this->{$key};
        }
        return null;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {

        if (isset($this->{$offset})) {
            return $this->{$offset};
        }

        return null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }


}
