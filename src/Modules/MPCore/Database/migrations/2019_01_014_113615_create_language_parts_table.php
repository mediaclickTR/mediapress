<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagePartsTable extends Migration
{
    public const LANGUAGE_PARTS = 'language_parts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(self::LANGUAGE_PARTS)){
            return;
        }

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create(self::LANGUAGE_PARTS, function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('language_id')->unsigned()->nullable();
            $table->integer('country_group_id')->unsigned()->nullable();
            $table->integer('website_id')->unsigned()->nullable();
            $table->string('key');
            $table->text('value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::LANGUAGE_PARTS);
    }
}
