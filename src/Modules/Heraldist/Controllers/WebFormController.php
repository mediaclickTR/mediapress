<?php

namespace Mediapress\Modules\Heraldist\Controllers;

use GuzzleHttp\Client;
use Cache;
use Illuminate\Support\Facades\App;
use Mediapress\Foundation\MediapressBasic;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Heraldist\Mail\FormSender;
use Mediapress\Modules\Heraldist\Mail\UserFormSender;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Heraldist\Models\FormDetail;
use Mediapress\Modules\Locale\Models\Province;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\Heraldist\Models\Message;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Models\Url;
use Validator;
use Mail;
use Illuminate\Support\Facades\Storage;
use Mediapress\Modules\MPCore\Models\Country;

class WebFormController extends Controller
{

    public const FIELD = "field";
    public const SOURCE = 'source';
    public const VISIT_ID = 'visit_id';
    public const LABEL = 'label';

    public function fieldValidate(Request $request)
    {
        $id = $request->id;
        $language = Language::find($request->language);
        $field = FormDetail::find($id);

        $message = [
            'success' => true,
        ];
        if ($field && $field->rules) {
            $rule_messages = $this->ruleMessages($field->form->slug, $field->name, $field->rules, self::FIELD);
            $validator = Validator::make(
                [
                    self::FIELD => $request->value,
                ],
                [
                    self::FIELD => $field->rules
                ], $rule_messages, [self::FIELD => langPartAttr($field->label(), $field->label, [], $language->id)]);
            if ($validator->fails()) {
                App::setLocale($language->code);
                $errors = $validator->errors()->messages();
                $message = [
                    'success' => false,
                    'message' => (isset($errors[self::FIELD][0]) ? $errors[self::FIELD][0] : 'server error'),
                ];
            }
        }
        return response()->json($message);

    }

    public function ruleMessages($formName = null, $fieldName = 'all', $rules = [], $prefix = null)
    {
        $rules = explode("|", $rules);
        return array_reduce($rules, function ($result, $item) use ($formName, $fieldName, $prefix) {
            @$exp = explode(":", $item)[0];
            $result[($prefix != null ? $prefix : $fieldName) . ".$exp"] = langPartAttr("$formName.$fieldName.$exp");
            return $result;
        });
    }

    public function store(Request $request)
    {
        $formID = decrypt($request->_id);
        $error = 0;

        $referer = $this->clearReferer($request->header('referer'));

        $language = $this->getLanguage($request, $referer);


        $form = Form::find($formID);

        if( ! $request->has("mediapress-render") && $request->has("_called_class") ) {
            $class = decrypt($request->_called_class);
            return (new $class)->postForm( $form, $request );
        }

        $arr = $request->except( "mediapress-render" );
        $request = $request->merge($arr);

        $render = $form->render;

        $validation = $this->validation($render, $form);
        App::setLocale($language->code);

        $this->validate($request, $validation['rules'], $validation['messages'], $validation['labels']);

        $data = $this->collectData($request, $form);


        if (session('mediapressUtm')) {
            $data['utm'] = session('mediapressUtm');
        } else {
            $data['utm'] = null;
        }
        if (session('mediapressSource')) {
            $data[self::SOURCE] = session('mediapressSource');
        } else {
            $data[self::SOURCE] = null;
        }
        if ($request->_visit_id) {
            $data[self::VISIT_ID] = decrypt($request->_visit_id);
        } else {
            $data[self::VISIT_ID] = null;
        }

        if ($request->extra_fields) {
            foreach($request->extra_fields as $field){
                $field = unserialize($field);
                $data["data"][$field['key']] = ['label'=>$field['label'],'key'=>$field['key'],'type'=>'hidden','value'=>$field['value']];
            }
        }

        $data['agent'] = $this->userAgent();
        $data['track_id'] = $request->_trackid;
        $data['ip'] = $this->ip($request);

        $errorData = [];
        //try {
        $message = $this->saveForm($form, $data);
        $formClass = 'App\Mail\Form';
        if ($form->receiver) {
            $receivers = str_replace(' ', '', $form->receiver);
            $receivers = explode(',', $receivers);
            if(class_exists($formClass)) {
                foreach ($receivers as $receiver) {
                    Mail::to($receiver)->send(new $formClass($message));
                }
            } else{
                foreach ($receivers as $receiver) {
                    Mail::to($receiver)->send(new FormSender($message));
                }
            }
        }

        if($form->send_user == 1 && $message->email) {
            $receiver = $message->email;
            Mail::to($receiver)->send(new UserFormSender($message));
        }

        /*} catch (\Exception $e) {
            $error = $e;
            $errorData['error']=langPart($form->error, $form->error);
            $errorData['type'] = get_class($error);
            $errorData['message'] = $error->getMessage();
            $errorData['code'] = $error->getCode();
            $errorData['file'] = $error->getFile();
            $errorData['line'] = $error->getLine();
            $errorData['trace'] = $error->getTraceAsString();
        }*/
        if ($error) {
            return redirect($referer . '?error=true')->withErrors($errorData);
        }
        $parseReferer = parse_url($referer);

        $referer = $parseReferer['scheme'] . '://' . $parseReferer['host'] . $parseReferer['path'];
        $success_message = ($form->success) ? $form->success : 'form.success.message';

        return redirect($referer . '?success=true')->with('status', langPartAttr($success_message, $success_message,[],$language->id));

    }

    public function saveForm($form, $data)
    {

        $message = new Message();
        $message->data = $data['data'];
        $message->email = $data['email'];
        $message->form_id = $form->id;
        $message->name = $form->name;
        $message->agent = $data['agent'];
        $message->track_id = $data['track_id'];
        $message->visit_id = $data[self::VISIT_ID];
        $message->source = $data[self::SOURCE];
        $message->utm = $data['utm'];
        $message->ip = $data['ip'];
        $message->save();
        return $message;
    }

    public static function getLanguage(Request $request, $referer)
    {

        $parsedUrl = parse_url(preg_replace('/\?.*/', '', $referer));
        $language = null;
        if (isset($parsedUrl['path'])) {
            $url = Url::where('url', $parsedUrl['path'])->first();
            if ($url) {
                $relation = $url->relation;
                if ($relation) {
                    $language = $relation->language;
                }
            }

        }
        if (!$language) {
            $language = Language::find(616);
        }
        return $language;
    }

    private function validation($render, $form)
    {
        $messages = [];

        if (strlen($form->captcha_site_key) > 5) {
            $render->rules['g-recaptcha-response'] = 'required|captcha:'.$form->captcha_secret;
            $render->labels['g-recaptcha-response'] = 'ReCaptcha';
        }
        return ['rules' => $render->rules, 'messages' => $messages, 'labels' => $render->labels];
    }

    private function collectData(Request $request, $form)
    {
        $list = [];
        $email = '';
        //   unset($dataArray['g-recaptcha-response'], $dataArray['_token']);
        $dataArray = $request->all();

        foreach ($form->render->source['fields'] as $data) {
            if (!isset($data['id'])) {
                continue;
            }
            $key = hash('ripemd128', $data['id']);

            // $key = $data['name'];


            if (isset($dataArray[$key])) {
                $value = $dataArray[$key];
            }else{
                continue;
            }

            if (is_array($value)) {
                $value = implode(', ', $value);
            } elseif (is_file($value)) {
                $file = $request->file($key);
                // mediapress/../config/filesystems.php
                $file_name = $file->store(null, 'public');
                $value = "<a href='" . route("download.uploadedFile", ["filename" => $file_name]) . "' target='_blank'>" . langPart("panel.uploaded.file", "Yüklenmiş Dosyayı Görüntüle") . "</a>";
            }

            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $email = $value;
            }

            $list[$key] = [

                self::LABEL => isset($data[self::LABEL]) ? $data[self::LABEL] : langPartAttr($data['config']['label']),
                'key' => $key,
                'attr_type' => isset($data["attrs"]) && isset($data["attrs"]["type"]) ? $data["attrs"]["type"] : 'text',
                'type' => isset($data['tag']) ? $data['tag'] : null,
                'value' => $value,
            ];

        }


        return ['data' => $list, 'email' => $email];
    }

    private function userAgent()
    {
        $agent = new UserAgent();
        return [
            'platform' => $agent->platform(),
            'browser' => $agent->browser(),
            'device' => $agent->getDevice(),
            'version' => $agent->version($agent->browser()),
            'language' => $agent->languages(),
            'raw' => $agent->getUserAgent(),
            'headers' => $agent->getHttpHeaders(),
        ];
    }

    private function ip(Request $request)
    {
        if ($request->server('HTTP_CF_CONNECTING_IP')) {
            return $request->server('HTTP_CF_CONNECTING_IP');
        }
        return $request->server('REMOTE_ADDR');


    }

    private function checkCaptcha($form, Request $request)
    {
        if (strlen($form->captcha_site_key) > 5) {
            $client = new Client();

            $secret = $form->captcha_secret;
            $value = $request->get('g-recaptcha-response');
            $response = $client->post(
                'https://www.google.com/recaptcha/api/siteverify',
                ['form_params' =>
                    [
                        'secret' => $secret,
                        'response' => $value
                    ]
                ]
            );

            $body = json_decode((string)$response->getBody());

            return $body->success;
        }

        return true;
    }

    private function clearReferer($header)
    {
        $referer = parse_url($header);
        $url = $referer['scheme'] . '://' . $referer['host'] . $referer['path'];
        if (isset($referer['query'])) {


            $query = explode('&', str_replace('?', '&', $referer['query']));
            $queryString = [];
            foreach ($query as $q) {
                $q = explode('=', $q);
                $key = null;
                $value = null;
                if (isset($q[0])) {
                    $key = $q[0];
                }
                if (isset($q[1])) {
                    $value = $q[1];
                }
                if ($key != 'error' && $key != 'success') {
                    $queryString[$key] = $value;
                }
            }
            if (count($queryString)) {
                $url .= '?' . http_build_query($queryString);
            }
        }

        return $url;
    }

    public function changeCountry() {
        $country_id = request()->get('country_id') * 1;

        $country = Country::find($country_id);
        $province = array();
        if($country) {
            $temp_cities = $country->provinces;
            $name = $temp_cities->pluck('name', 'id')->toArray();
            $plate = $temp_cities->pluck('id', 'name')->toArray();
            $collator = new \Collator("tr_TR");
            $collator->sort($name);
            foreach ($name as $c) {
                $province[] = [
                    'id' => $plate[$c],
                    'name' => $c
                ];
            }
        }
        return $province;
    }

    public function changeProvince() {
         $province_id = request()->get('province_id') * 1;

        $province = Province::find($province_id);
        $cities = array();
        if($province) {
            $temp_cities = $province->counties;
            $name = $temp_cities->pluck('name', 'id')->toArray();
            $plate = $temp_cities->pluck('id', 'name')->toArray();
            $collator = new \Collator("tr_TR");
            $collator->sort($name);
            foreach ($name as $c) {
                $cities[] = [
                    'id' => $plate[$c],
                    'name' => $c
                ];
            }
        }
        return $cities;
    }

    public function uploadedFile( $filename )
    {
        return response()->file(storage_path("app/public/".$filename));
    }
}
