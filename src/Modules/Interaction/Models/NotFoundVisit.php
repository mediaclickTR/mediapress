<?php

namespace Mediapress\Modules\Interaction\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;

class NotFoundVisit extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'not_found_visits';

    protected $fillable = [
        "id",
        "cookie_id",
        "session_id",
        "url",
        "ip",
        "query_string",
        "referer",
    ];
    public $timestamps = false;


    public static function boot()
    {

        parent::boot();


        static::creating(function ($model) {

            $model->created_at = Carbon::now();

        });
    }


    public function cookie(){
        return $this->belongsTo(Cookie::class);
    }
    public function session(){
        return $this->belongsTo(Session::class);
    }
}
