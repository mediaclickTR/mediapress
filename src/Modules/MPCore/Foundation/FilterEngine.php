<?php

namespace Mediapress\Modules\MPCore\Foundation;

final class FilterEngine
{
    public const LIST_SIZE = 'list_size';
    public const QUERY = "query";

    public function filter($model, $relation_website = true)
    {
        $website_id = session("panel.website")->id;
        $paginate = 15;
        $queries = [];

        if ($relation_website){
            if (request()->get('list') && request()->get('list')=="all") {
                $queries['list'] = "all";
            }
            else{
                $model=$model->whereHas('websites', function($query) use ($website_id) {
                    $query->where('id', $website_id);
                });
            }
        }

        if (request()->has(self::LIST_SIZE)) {
            $paginate = request()->get(self::LIST_SIZE);
            $queries[self::LIST_SIZE] = $paginate;
        }

        $model = $model->paginate($paginate)->appends($queries);

        $url = request()->fullUrl();
        $parsed = isset(parse_url($url)[self::QUERY]) ? parse_url($url)[self::QUERY] : null;

        return [
            "model"=> $model,
            "queries"=>$queries,
            "parsed_url"=>$parsed
        ];

    }

    public function parseFilter($param)
    {
        $url = request()->fullUrl();
        $parsed = parse_url($url);
        if (isset($parsed[self::QUERY])){
            $query = $parsed[self::QUERY];
            parse_str($query, $params);
            unset($params[$param]);
            $params = http_build_query($params);
        }
    }
}