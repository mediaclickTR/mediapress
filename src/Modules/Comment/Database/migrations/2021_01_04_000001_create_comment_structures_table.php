<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('comment_structures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("website_id")->unsigned();
            $table->integer("admin_id")->unsigned();
            $table->tinyInteger("status")->default(2);
            $table->string("name");
            $table->string("key");
            $table->tinyInteger("show_time")->default(2);
            $table->tinyInteger("condition")->default(2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_structures');
    }
}
