@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")

        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{ __('MPCorePanel::panel_translate.title') }}</div>
            </div>
            <div class="float-right">
                <div class="row">
                    <div class="p-0 ml-2">
                        <button type="button" class="btn btn-success float-right"
                                data-toggle="tooltip"
                                data-placement="left"
                                title="{!! __("MPCorePanel::panel_translate.save_info") !!}">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" id="lang-search" placeholder="{{ __('MPCorePanel::panel_translate.search') }}"
                               autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="p-30">
            <div class="language-content">
                @include('MPCorePanel::panel_translate.ajax')
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>


        $('.page-content').delegate('.open_tab', 'click', function (e) {
            e.preventDefault();
        });


        $('.page-content').delegate('.lang_part', 'change blur', function (e) {
            var input = this;
            var data = $(this).serialize() + '&_token={!! csrf_token() !!}';
            $.ajax({
                type: "POST",
                url: "{!! route("MPCore.panel.translate.save") !!}",
                data: data,
                async: false,
                dataType: "json",
                success: function (data) {
                    $(input).css('border-color', '#155724');
                    setTimeout(function () {
                        $(input).css('border-color', '#d2d2d2');
                    }, 2000);
                },
                error: function (status1) {
                    console.log(status1);
                }
            });
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

    </script>

    <!-- LangPart Search -->
    <script>

        var _changeInterval = null;
        $('#lang-search').on('keyup', function () {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function () {
                clearInterval(_changeInterval);
                search = $('#lang-search').val();

                $.ajax({
                    'url': "{!! route("MPCore.panel.translate.search") !!}",
                    'data': {'_token': "{!! csrf_token() !!}", search_key: search},
                    'method': 'post',
                    success: function (response) {
                        $('.language-content').html(response);

                        var pana = $('.lang-box .item button');
                        pana.click(function () {
                            if ($(this).parent().parent().parent().hasClass('open')) {
                                $(this).parent().parent().parent().removeClass('open');
                                $(this).prev().removeClass('open');
                                $(this).parent().parent().parent().find('.edit-center').slideUp();
                                $(this).parent().parent().parent().find('.text').slideUp();
                                $(this).parent().parent().parent().find('.edit').slideUp();
                            } else {
                                pana.parent().parent().parent().find('.edit-center').slideUp();
                                pana.parent().parent().parent().find('.edit').slideUp();
                                pana.parent().parent().parent().find('.text').slideUp();
                                pana.parent().parent().parent().removeClass('open');
                                pana.prev().removeClass('open');
                                $(this).parent().parent().parent().addClass('open');
                                $(this).prev().addClass('open');
                                $(this).parent().parent().parent().find('.edit-center').slideDown();
                                $(this).parent().parent().parent().find('.text').slideDown();
                                $(this).parent().parent().parent().find('.edit').slideDown();
                            }
                        });
                    }
                });
            }, 400);
        });
    </script>
@endpush

@push("styles")
    <style>
        .language-flag {
            float: left;
            padding: 5px;
        }

        .language-title {
            line-height: 20px;
            float: left;
            padding: 5px;
        }

        .lang_area {
            margin-top: 10px;
        }

        .lang-box form .edit .form-group input {
            width: 100% !important;
        }

        .head {
            border-bottom: 1px solid #f2f2f2;
            margin-top: 10px;
            padding: 10px;
        }
    </style>
@endpush
