<?php

use Illuminate\Support\Facades\Cookie;
use Intervention\Image\ImageManagerStatic;
use Mediapress\Facades\URLEngine;
use Mediapress\Foundation\Browser;
use Mediapress\Foundation\Form;
use Mediapress\Foundation\Cart;
use Mediapress\Foundation\City;
use Mediapress\Foundation\Country;
use Mediapress\Foundation\Cluster;
use Mediapress\Foundation\Date;
use Mediapress\Foundation\Menu;
use Mediapress\Foundation\Popup;
use Mediapress\Modules\Content\Foundation\Slide;
use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Auth\Models\Role;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\MPCore\Models\Search;
use Mediapress\Modules\Auth\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;

const HTTP_HOST = "HTTP_HOST";
const LANGUAGE_ID = "language_id";
const CURRENCY = 'currency';
const DETAIL_STR_EXTRAS = 'detail.extras';
const ACTIVE = 'active';
const EXTRAS = 'extras';
const DETAIL_STR = 'detail';
const MEDIAPRESS_MODELS_CATEGORY_DETAIL_STR = "Mediapress\Models\CategoryDetail";
const MEDIAPRESS_MODELS_SITEMAP_DETAIL_STR = "Mediapress\Models\SitemapDetail";
const MEDIAPRESS_MODELS_PAGE_DETAIL_STR = "Mediapress\Models\PageDetail";
const MEDIAPRESS = 'Mediapress';
const PANEL_WEBSITE = "panel.website";
const TITLE = 'title';
const DESCRIPTION = 'description';
const INEFFECTIVE = 'ineffective';
const REASON = "reason";
const PARENT_DATA = "parent_data";
const CATEGORY_ID = 'category_id';
const NAME_AFFIX = "name_affix";
const ACTIONS_STR = "actions";
const VARIATIONS = 'variations';
const PARENT_CLASS = "parent_class";
const PARENT_SHORTNAME = "parent_shortname";
const CHILD_SHORTNAME = "child_shortname";
const ORDER = 'order';
if (!function_exists('refresh_token')) {

    function refresh_token($id, $secret, $token)
    {
        //"238284199418-701prjoophrmeimkm6ptp4uck6ecbv9a.apps.googleusercontent.com"
        // "ne_BBDBvFEphQJO3i7Dm-uLZ"
        return json_decode((new GuzzleHttp\Client())->request('POST', 'https://www.googleapis.com/oauth2/v4/token',
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'body' => "grant_type=refresh_token&client_id=" . $id . "&client_secret=" . $secret . "&refresh_token=" . $token
            ]
        )->getBody()->getContents());
    }
}
function base_url()
{
    $url = url('');
    return $url;
}

function mp_asset($url)
{
    if ($cdn = config("mediapress.cdn")) {
        $exp = explode('/', $url);
        if (isset($exp[0]) && ($exp[0] == 'http:' || $exp[0] == 'https:')) {
            $url = str_replace(base_url(), '', $url);
        }
        $url = $cdn . ltrim($url, '/');
    }
    return asset($url);
}

function form_build($slug, $mediapress)
{
    return new Form($slug, $mediapress);
}

function isPanel()
{
    $isPanel = Cookie::get('mediapress.isPanel') ?: null;
    if(is_null($isPanel)) {
        $isPanel = request()->segment(1) == 'mp-admin';
        Cookie::queue(Cookie::make('mediapress.isPanel', $isPanel, 24*60));
    }

    return $isPanel;
}

function panel($url = '')
{
    if ($url) {
        $url = '/' . $url;
    }

    return config('app.panel_url_slug') . $url;
}

function is_url($uri)
{
    if (preg_match('/^(http|https):\\/\\/[a-z0-9-_ğüşıöç]+([\\-\\.]{1}[a-z0-9-_ğüşıöç]+)*\\.[a-z]{2,8}' . '((:[0-9]{1,5})?\\/.*)?$/i', $uri)) {
        return $uri;
    } else {
        return false;
    }
}

function cache_key($key)
{
    if (!session(HTTP_HOST)) {
        $request = Request::capture();
        $host = $request->server(HTTP_HOST);
        session([HTTP_HOST => $host]);
    }
    return md5(session(HTTP_HOST)) . '-key:' . $key;
}


/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param int|string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param bool $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
{
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    if ($img) {
        $url = '<img src="' . $url . '"';
        foreach ($atts as $key => $val) {
            $url .= ' ' . $key . '="' . $val . '"';
        }
        $url .= ' />';
    }

    return $url;
}

function mediapress_path($url)
{
    return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $url;
}

/**
 * Image get Disk Path
 * @param $imageId
 * @return string
 */
function getImagePath($imageId)
{
    if (!$imageId) {
        return false;
    }

    $image = \Mediapress\FileManager\Models\MFile::find($imageId);

    if (!$image) {
        return false;
    }

    /** @var \Illuminate\Filesystem\FilesystemAdapter $storage */
    $path = \Storage::disk('local')->path($image->path());

    return $path;
}

/**
 * Image get Url Path
 * @param $imageId
 * @return string
 */
function getImageUrl($imageId)
{
    if (!$imageId || !is_numeric($imageId)) {
        return defaultImage();
    }


    $image = \Mediapress\Models\Image::find($imageId);
    if (!$image) {
        return false;
    }
    return mp_asset(str_replace(['\\', '///', '//'], '/', config('lfm.images_folder_name') . $image->gallery->path . "/" . $image->gallery->folder_name . "/" . $image->file_name));

}

/**
 * Return re-sized url for image url Also if file not exists create file
 *
 * @param $imageUrl
 * @param array $attributes
 * @return string
 */
function resizeImage($imageUrl, $attributes = [])
{
    $image = new \Mediapress\Foundation\Image($imageUrl);

    return $image->resize($attributes);
}

function image($imageUrl, $attributes = [])
{
    return new \Mediapress\Foundation\Image($imageUrl);

}

/**
 * Sitemap Detail'da aktif olan dil bilgilerini döndürür.
 * @param $sitemap_id
 * @return mixed
 */
function sitemapLanguages($sitemap_id)
{
    $languages = \Mediapress\Modules\Content\Models\Sitemap::find($sitemap_id)->details()->active()->pluck(LANGUAGE_ID);
    $languages = \Mediapress\Modules\MPCore\Models\Language::whereIn('id', $languages)->pluck("code", "id");

    //Türkçe dili veriler arasında varsa ve ilk dil değilse düzenleme yapılıyor
    if ($languages->has("760") && \Arr::first($languages) != "tr") {
        $languages->prepend("tr", 760);
    }

    if ($languages instanceof \Illuminate\Support\Collection) {
        return $languages->toArray();
    }

    return $languages;
}

function currency()
{
    if (!Cache::has(cache_key(CURRENCY))) {
        $currencyArray = new SimpleXMLElement(file_get_contents('http://www.tcmb.gov.tr/kurlar/today.xml'));
        $currency = [];
        /** @var SimpleXMLElement $array */
        foreach ($currencyArray as $array) {

            foreach ($array->attributes() as $k => $v) {
                if ($k == 'Kod') {
                    $key = '' . $v;
                }
            }
            $currency[$key] = [
                'name' => '' . $array->Isim, 'currencyName' => '' . $array->CurrencyName, 'buy' => (float)('' . $array->BanknoteBuying), 'sell' => (float)('' . $array->BanknoteSelling), 'forexBuy' => (float)('' . $array->ForexBuying), 'forexSell' => (float)('' . $array->ForexSelling),
            ];
        }
        $currency['TRY']['buy'] = 1;
        Cache::put(cache_key(CURRENCY), $currency, 60);
    } else {
        $currency = Cache::get(cache_key(CURRENCY));
    }

    return $currency;
}


function array_to_xml($data)
{
    $data = json_decode(json_encode($data), 1);
    $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
    array_to_xml_recursive($data, $xml_data);

    return $xml_data->asXML();

}

function array_to_xml_recursive($data, &$xml_data = null)
{
    foreach ($data as $key => $value) {
        if (is_numeric($key)) {
            $key = 'item' . $key; //dealing with <0/>..<n/> issues
        }
        if (is_array($value)) {
            $subnode = $xml_data->addChild($key);
            array_to_xml_recursive($value, $subnode);
        } else {
            $xml_data->addChild("$key", htmlspecialchars("$value"));
        }
    }
}

if (!function_exists('menu')) {
    function menu(array $array, $website, $activeCountryGroup, $activeLanguage)
    {
        return new Menu($array, $website, $activeCountryGroup, $activeLanguage);
    }
}

if (!function_exists('menu_link')) {
    function menu_link(\Mediapress\Models\MenuDetail $detail, $attributes = [])
    {
        return (new Menu(['link' => true]))->link($detail, $attributes);
    }
}

if (!function_exists('get_sitemap_detail')) {
    function get_sitemap_detail($sitemap_id)
    {
        $sm = Sitemap::where('id', $sitemap_id)->with(DETAIL_STR_EXTRAS)->first();
        return isset($sm->detail) ? $sm->detail : new SitemapDetail();
    }
}


if (!function_exists('getStaticPage')) {
    function getStaticPage($sitemap_id)
    {
        $sm = Sitemap::where('id', $sitemap_id)->first();
        if (!$sm) {
            return new Page();
        }
        $page = $sm->pages()->status(ACTIVE)->with([EXTRAS, DETAIL_STR, DETAIL_STR_EXTRAS])->first();
        return $page->exists ? $page : new Page();
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($number)
    {
        return number_format($number, ($number - floor($number) ? 1 : 0), ',', '.');
    }
}

if (!function_exists('getPagesByDetailsExtra')) {
    /**
     * @param string $extra_key
     * @param mixed $extra_value
     * @param int $language_id
     * @return Collection
     * @description Returns a collection of pages filtered by arguements..
     * @author Eray DEMİREL, erayera@gmail.com
     */
    function getPagesByDetailsExtra($language_id, $extra_key, $extra_value = null, $sitemap_id = null)
    {
        $build = PageDetail::join('page_detail_extras', 'page_detail_extras.page_detail_id', 'page_details.id')->where('page_details.language_id', $language_id)->where('page_detail_extras.key', $extra_key);
        if ($extra_value) {
            $build->where('page_detail_extras.value', $extra_value);
        }
        $ids = $build->select(['page_details.page_id'])->distinct('page_details.page_id')->get()->toArray();

        $pages = Page::with([DETAIL_STR_EXTRAS, EXTRAS, DETAIL_STR])->whereIn('id', $ids)->status(ACTIVE);
        if ($sitemap_id) {
            $pages->where('sitemap_id', $sitemap_id * 1);
        }
        return $pages->get();
    }
}
if (!function_exists('getPagesByExtra')) {
    /**
     * @param string $extra_key
     * @param mixed $extra_value
     * @param $order_by Field to order
     * @param $order_direction ASC | DESC
     * @param $sitemap_id integer Sitemap to limit pages
     * @return Collection
     * @description Returns a collection of pages filtered by arguements..
     * @author Eray DEMİREL, erayera@gmail.com
     */
    function getPagesByExtra($extra_key, $extra_value = null, $sitemap_id = null, $order_by = "or" . "der", $order_direction = "ASC")
    {
        $build = Page::join('page_detail_extras', 'page_detail_extras.page_id', 'pages.id')->where('page_detail_extras.page_detail_id', null)->where('page_detail_extras.key', $extra_key);
        if ($extra_value) {
            $build->where('page_detail_extras.value', $extra_value);
        }
        if ($sitemap_id) {
            $build->where('pages.sitemap_id', $sitemap_id * 1);
        }
        $ids = $build->select(['pages.id'])->get()->toArray();

        $pages = Page::with([DETAIL_STR_EXTRAS, EXTRAS, DETAIL_STR])->whereIn('id', $ids)->status(ACTIVE);

        if ($order_by) {
            if (!in_array($order_direction, ['ASC', 'DESC'])) {
                $order_direction = "ASC";
            }
            $pages->orderBy($order_by, $order_direction);
        }

        return $pages->get();
    }
}
if (!function_exists('getPagesBySitemap')) {
    /**
     * @param $sitemap_id integer
     * @param $order_by Field to order
     * @param $order_direction ASC | DESC
     * @return Collection
     * @description Returns a collection of pages filtered by sitemap id
     * @author Eray DEMİREL, erayera@gmail.com
     */
    function getPagesBySitemap($sitemap_id, $order_by = "order", $order_direction = "ASC")
    {
        $results = collect([]);

        $sm = Sitemap::find($sitemap_id);
        if ($sm) {
            $results = $sm->pages()->status(ACTIVE)->with([EXTRAS, DETAIL_STR, DETAIL_STR_EXTRAS]);
            if ($order_by) {
                if (!in_array($order_direction, ['ASC', 'DESC'])) {
                    $order_direction = "ASC";
                }
                $results->orderBy($order_by, $order_direction);
            }
            $results = $results->get();
        }
        return $results;
    }
}

if (!function_exists('getCurrents')) {
    /**
     * @param \Mediapress\Foundation\Mediapress $mediapress
     * @return array
     * @description Returns a set of current mp hierarchical dataset brought by mp relation.
     * @author Eray DEMİREL, erayera@gmail.com
     */
    function getCurrents(Mediapress\Foundation\Mediapress $mediapress)
    {

        $instance = get_class($mediapress->relation);

        $rel_tip = null;

        $aktif_kat = null;
        $aktif_sm = null;
        $aktif_pg = null;
        $outer_pg = null;
        $outer_kat = null;

        switch ($instance) {
            case MEDIAPRESS_MODELS_CATEGORY_DETAIL_STR:
                $rel_tip = "CategoryDetail";
                /** @var Category $aktif_kat */
                $aktif_kat = $mediapress->relation->category()->with([DETAIL_STR_EXTRAS, EXTRAS])->first();
                $outer_kat = isset($aktif_kat->parent) && $aktif_kat->parent ? $aktif_kat->parent()->with([DETAIL_STR_EXTRAS, EXTRAS])->first() : null;
                $aktif_sm = $aktif_kat->sitemap()->with([DETAIL_STR_EXTRAS, EXTRAS])->first();
                break;
            case MEDIAPRESS_MODELS_SITEMAP_DETAIL_STR:
                $rel_tip = "SitemapDetail";
                /** @var Sitemap $aktif_sm */
                $aktif_sm = $mediapress->relation->sitemap()->with(EXTRAS)->first();
                $outer_pg = $aktif_sm->pages()->with([DETAIL_STR_EXTRAS, EXTRAS])->status(ACTIVE)->first();
                break;
            case MEDIAPRESS_MODELS_PAGE_DETAIL_STR:
                $rel_tip = "PageDetail";
                /** @var Page $aktif_pg */
                $aktif_pg = $mediapress->relation->page()->with([DETAIL_STR_EXTRAS, EXTRAS])->first();
                $outer_pg = isset($aktif_pg->parent) && $aktif_pg->parent ? $aktif_pg->parent()->with([DETAIL_STR_EXTRAS, EXTRAS])->first() : null;
                $kat_id = null;
                if (isset($aktif_pg->category_id) && $aktif_pg->category_id) {
                    $kat_id += $aktif_pg->category_id + 0;
                    $kat = \Mediapress\Models\Category::whereId($kat_id)->with([DETAIL_STR_EXTRAS, EXTRAS])->first();
                    if ($kat && $kat->exists) {
                        $aktif_kat = $kat;
                    }
                }
                $aktif_sm = $aktif_pg->sitemap()->with([DETAIL_STR_EXTRAS, EXTRAS])->first();
                break;
            default:
                dd('No Instance');
        }
        return ["active_type" => $rel_tip, "active_sitemap" => $aktif_sm, "active_page" => $aktif_pg, "active_category" => $aktif_kat, 'outer_category' => $outer_kat, "outer_page" => $outer_pg];
    }
}

if (!function_exists("isActiveMenu")) {
    function isActiveMenu($uri)
    {
        //echo "Uri: ". $uri;
        //$uri=null;
        if (is_string($uri)) {
            $uri = ltrim($uri, "/");
        } elseif (is_a($uri, "Mediapress\Models\MenuDetail")) {
            $uri = $uri->url;
        }

        if (is_a($uri, "Mediapress\Models\Url")) {
            $uri = $uri->url;
            $uri = ltrim($uri, "/");
        }

        $active = "";

        if (Request::is($uri) || Request::is($uri . "/*")) {
            return ACTIVE;
        }

        return $active;
    }
}

if (!function_exists('popup')) {
    function popup()
    {
        return new Popup();
    }
}
if (!function_exists('cluster')) {

    function cluster($collection, $parameters = [], $destroy = 0)
    {
        if ($destroy) {
            return (new Cluster($collection, $parameters))->unBuild();
        } else {
            return (new Cluster($collection, $parameters))->toTree();
        }


    }
}


if (!function_exists('newCart')) {

    function newCart($parameter = null, $key = 'price', $quantity = 1, $extras = null)
    {
        $cart = new Cart();
        if (is_array($parameter)) {
            $cart->addArray($parameter, $key, $quantity, $extras);
        } elseif ($parameter) {
            $cart->addCart($parameter, $key, $quantity, $extras);
        }
        session(['cart' => $cart]);

        return $cart;
    }
}

if (!function_exists('primaryLanguage')) {

    function primaryLanguage()
    {
        return \Mediapress\Models\Website::where("default", "1")->with("sitemaps", "languages")->first()->defaultLanguage();
    }
}
if (!function_exists('mediapress')) {
    /**
     * Get / set the specified Mediapress.
     *
     * If an array is passed, we'll assume you want to put to the Mediapress.
     *
     * @param dynamic  key|key,value|null
     * @return mixed|\Mediapress\Modules\MPCore\Foundation\Mediapress
     *
     */
    function mediapress()
    {
        $args = func_get_args();
        if (empty($args)) {
            return app(MEDIAPRESS);
        } else if (isset($args[0]) && isset($args[1])) {
            return app(MEDIAPRESS)->{$args[0]} = $args[1];
        } else if (is_string($args[0])) {
            return app(MEDIAPRESS)->{$args[0]};
        }
    }
}

if (!function_exists('defaultLanguage')) {

    function defaultLanguage()
    {
        if (!session(PANEL_WEBSITE)) {
            session(["website" => \Mediapress\Models\Website::where("default", "1")->with("sitemaps", "languages")->first()]);
        }


        //        if(session("panel.website"))
        $default = session(PANEL_WEBSITE)->defaultLanguage;
        if (!$default) {
            $default = session(PANEL_WEBSITE)->languages->first();
        }
        return $default;
    }
}

if (!function_exists('nl2list')) {

    function nl2list($str, $tag = 'ul', $class = '')
    {

        $bits = explode("\n", $str);

        $class_string = $class ? ' class="' . $class . '"' : false;

        $newstring = '<' . $tag . $class_string . '>';

        foreach ($bits as $bit) {
            $newstring .= "<li>" . $bit . "</li>";
        }

        return $newstring . '</' . $tag . '>';

    }
}


if (!function_exists('cart')) {

    /**
     * @return Cart
     */
    function cart()
    {

        $unique = session('cart_id');

        if (!$unique) {
            $unique = uniqid();
            session(['cart_id' => $unique]);
        }
        $cart = getCart($unique);

        return $cart;
    }
}

if (!function_exists('getCart')) {

    function getCart($unique)
    {
        /** @var Cart $cart */
        $cart = new Cart([], $unique);
        return $cart;

    }
}

if (!function_exists('cartInfo')) {

    function cartInfo(array $information)
    {
        /** @var Cart $cart */
        $cart = cart();
        $cart->addInformation($information);
        session(['cart' => $cart]);

        return $cart;

    }
}

if (!function_exists('generateOrderId')) {

    function generateOrderId()
    {
        $id = randomString(1);
        for ($i = 1; $i <= 8; $i++) {
            if (rand(0, 1)) {
                $id .= randomString(1);
            } else {
                $id .= rand(0, 9);
            }
        }
        $askOrderId = Order::where('order_id', $id)->first();
        if ($askOrderId) {
            $id = generateOrderId();
        }
        return $id;
    }
}
if (!function_exists('randomString')) {

    function randomString($length = 6)
    {
        $str = "";
        $characters = range('A', 'Z');
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }

        return $str;
    }

}
if (!function_exists('ccMasking')) {

    function ccMasking($number, $maskingCharacter = 'X')
    {
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    }

}
if (!function_exists('defaultImage')) {

    function defaultImage()
    {
        return 'images/default.jpg';
    }

}
if (!function_exists('defaultLazyImage')) {

    function defaultLazyImage()
    {
        return '/images/lazy.svg';
    }

}

if (!function_exists('format_phone')) {

    function format_phone($number)
    {
        if (preg_match('/^\+(\d{2})(\d{3})(\d{3})(\d{4})$/', $number, $matches)) {
            $number = '+' . $matches[1] . ' (' . $matches[2] . ') ' . $matches[3] . '-' . $matches[4];
        } elseif (preg_match('/^\+(\d{2})(\d{3})(\d{4})$/', $number, $matches)) {
            $number = '+' . $matches[1] . ' ' . $matches[2] . '-' . $matches[3];
        }

        return $number;
    }

}

if (!function_exists('isMobile')) {

    function isMobile()
    {
        $browser = new Mediapress\Foundation\UserAgent\UserAgent();
        return $browser->isMobile();
    }

}

if (!function_exists('format_date')) {

    function format_date($date, $format = null)
    {
        return (new Date($date))->format($format);
    }

}


if (!function_exists('mp_strtoupper')) {

    function mp_strtoupper($str)
    {
        return mb_strtoupper(str_replace(['ı', 'i'], ['I', 'İ'], $str));
    }

}

if (!function_exists('mp_strtolower')) {

    function mp_strtolower($str)
    {
        return mb_strtolower(str_replace(['I', 'İ'], ['ı', 'i'], $str));
    }

}

if (!function_exists('verifyRecaptcha')) {

    function verifyRecaptcha($request)
    {
        $curl = new \anlutro\cURL\cURL();
        $response = $curl->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret' => config("mediapress.recaptcha_secret_key"), 'response' => $request['g-recaptcha-response'], 'remoteip' => $request->ip()
        ])->body;
        $response = json_decode($response);

        return $response->success;
    }

}
if (!function_exists('cities')) {

    function cities($pluck = [],$country = null)
    {
        $location = new \Mediapress\Foundation\Location($country);

        return $location->cities($pluck);
    }

}
if (!function_exists('getCity')) {

    function getCity($city)
    {
        $model = \Mediapress\Modules\Locale\Models\Province::find($city);
        if($model){
            return $model->name;
        }
        return null;
    }

}
if (!function_exists('countries')) {

    function Countries($pluck = [])
    {
        $country = new \Mediapress\Foundation\Countries();

        return $country->countries($pluck);
    }

}
if (!function_exists('getCountry')) {

    function getCountry($countryCode)
    {
        $country = new \Mediapress\Foundation\Countries();
        return new Country($country->getCountryName($countryCode));
    }

}
if (!function_exists('notFound')) {

    function notFound()
    {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('');
    }

}

if (!function_exists('video')) {

    function video($key, $parameters = [], $autoplay = 0)
    {
        return new \Mediapress\Foundation\Video($key, $parameters, $autoplay);
    }

}


if (!function_exists('transMp')) {

    function transMp($trans)
    {
        if (strpos($trans, ".")) {
            $translate = trans($trans);

            if ($translate != $trans) {
                return $translate;
            } else {
                $translate = trans("panel::" . $trans);

                if ($translate != "panel::" . $trans) {
                    return $translate;
                }
            }
        }

        return $trans;
    }

}
if (!function_exists('word_limit')) {

    function word_limit($str, $limit = 50)
    {
        return @implode(' ', array_slice(explode(' ', strip_tags($str)), 0, $limit));
    }

}
if (!function_exists(TITLE)) {

    function title($meta, $default = null, $main = null)
    {
        if (isset($meta[TITLE]) && $meta[TITLE]) {
            return $meta[TITLE] . ' | ' . $main;
        } else {
            return $default;
        }
    }

}

if (!function_exists(DESCRIPTION)) {

    function description($meta, $default = null)
    {
        if (isset($meta[DESCRIPTION]) && $meta[DESCRIPTION]) {
            return $meta[DESCRIPTION];
        } else {
            return $default;
        }
    }

}

if (!function_exists('gallery')) {
    function gallery($id)
    {
        return new \Mediapress\Foundation\Gallery($id);
    }
}

if (!function_exists('galleryCover')) {
    function galleryCover($gallery_id)
    {
        return PageGalleryImage::where('page_gallery_id', $gallery_id)->where('status', '1')->orderBy(ORDER)->first();
    }
}

if (!function_exists('liToArray')) {
    function liToArray($string, $seperator = ':', $type = 'ul', $limit = 2)
    {
        $html = str_get_html($string);
        if (!$html->root) {
            return [];
        }
        $list = $html->find($type . ' li');
        $array = [];
        foreach ($list as $l) {
            $parts = explode($seperator, $l->text());
            if (count($parts) > $limit) {
                $list = [];
                for ($i = 0; $i < count($parts); $i++) {
                    if ($i < ($limit - 1)) {
                        $list[] = $parts[$i];
                        unset($parts[$i]);
                    }
                }
                $list[] = @implode($seperator, $parts);
                $array[] = $list;
            } else {
                $array[] = $parts;
            }
        }
        return $array;
    }
}
if (!function_exists('arrayToLi')) {
    function arrayToLi($array, $seperator = ':', $type = 'li', $limit = 10)
    {

        $string = '<ul>';
        $i = 1;
        foreach ($array as $key => $value) {
            if ($i == $limit) {
                break;
            }
            if (is_array($value)) {
                $value = implode($seperator, $value);
            }
            if (!is_numeric($key)) {
                $value = ucfirst(str_replace('utm_', '', $key)) . $seperator . $value;
            }

            if (is_url($value)) {
                $string .= '<' . $type . '><a href="' . $value . '" target="_blank">' . $value . '</a></' . $type . '>';
            } else {
                $string .= '<' . $type . '>' . $value . '</' . $type . '>';
            }
            $i++;
        }


        $string .= '</ul>';

        return $string;
    }
}

if (!function_exists('str_get_html')) {
    function str_get_html($sting)
    {
        $html = new \Mediapress\Foundation\Htmldom\Htmldom();
        $html->str_get_html($sting);
        return $html;
    }
}
if (!function_exists('getUrlBySitemapId')) {
    function getUrlBySitemapId($sitemapId)
    {
        $mediapress = mediapress();
        return \Cache::rememberForever('getUrlBySitemapId' . $sitemapId . $mediapress->activeCountryGroup->id . $mediapress->activeLanguage->id, function () use($sitemapId){
            $sitemap = Sitemap::with(DETAIL_STR)->find($sitemapId);
            if ($sitemap && $sitemap->detail && $sitemap->detail->categoryUrl && isset($sitemap->detail->categoryUrl->url)) {
                return url($sitemap->detail->categoryUrl->url);
            } elseif ($sitemap && $sitemap->detail && $sitemap->detail->url && isset($sitemap->detail->url->url)) {
                return url($sitemap->detail->url->url);
            }
            return 'javascript:void(0);';
        });
    }
}
if (!function_exists('getSitemapName')) {
    function getSitemapName($sitemapId)
    {
        return SitemapDetail::where('sitemap_id', $sitemapId)->where(LANGUAGE_ID, 760)->withTrashed()->first()->name;
    }
}

if (!function_exists('objectDeleted')) {
    function objectDeleted($object)
    {
        if (!is_object($object)) {
            return false;
        }

        $class_name = get_class($object);


        if ($class_name == "Mediapress\Modules\Content\Models\Category"
            || $class_name == "Mediapress\Modules\Content\Models\Page"
            || $class_name == "Mediapress\Modules\Content\Models\Sitemap") {
            $details = $object->details()->get()->pluck("id");
            $detmodel = $class_name . "Detail";
            $ids = \Mediapress\Modules\MPCore\Models\Url::where('model_type', $detmodel)->whereIn('model_id', $details)->get()->pluck("id")->all();
            if (count($ids)) {
                \Mediapress\Modules\MPCore\Models\Url::destroy($ids);
                $meta_ids = \Mediapress\Modules\Content\Models\Meta::whereIn('url_id', $ids)->get('id')->all();
                if (count($meta_ids)) {
                    \Mediapress\Modules\Content\Models\Meta::destroy($meta_ids);
                }
            }
        }
        return true;

    }
}

if (!function_exists(INEFFECTIVE)) {
    function ineffective($object_to_analyze)
    {


        if (!is_object($object_to_analyze)) {
            return true;
        }

        $model_type = get_class($object_to_analyze);
        $parent = null;
        $parent_class = null;


        switch ($model_type) {
            case MEDIAPRESS_MODELS_SITEMAP_DETAIL_STR:
                if (
                    !$object_to_analyze->sitemap
                    || ($object_to_analyze->sitemap && $object_to_analyze->sitemap->deleted_at)
                    || ($object_to_analyze->sitemap && ineffective($object_to_analyze->sitemap))
                ) {
                    return true;
                }
                break;
            case MEDIAPRESS_MODELS_PAGE_DETAIL_STR:
                if (
                    !$object_to_analyze->page
                    || ($object_to_analyze->page && $object_to_analyze->page->deleted_at)
                    || ($object_to_analyze->page && ineffective($object_to_analyze->page))
                ) {
                    return true;
                }
                break;
            case MEDIAPRESS_MODELS_CATEGORY_DETAIL_STR:
                if (
                    !$object_to_analyze->category
                    || ($object_to_analyze->category && $object_to_analyze->category->deleted_at)
                    || ($object_to_analyze->category && ineffective($object_to_analyze->category))
                ) {
                    return true;
                }
                break;
            case 'Mediapress\Models\Sitemap':
                if (
                    $object_to_analyze->deleted_at
                    || !$object_to_analyze->website
                    || ($object_to_analyze->website && $object_to_analyze->website->deleted_at)
                ) {
                    return true;
                }
                break;
            case 'Mediapress\Models\Page':
                if (
                    $object_to_analyze->deleted_at
                    || !$object_to_analyze->sitemap
                    || ($object_to_analyze->page_id && !$object_to_analyze->page)
                    || ($object_to_analyze->page && ineffective($object_to_analyze->page))
                    || ($object_to_analyze->sitemap && ineffective($object_to_analyze->sitemap))
                ) {
                    return true;
                }
                break;
            case 'Mediapress\Models\Category':
                if (
                    $object_to_analyze->deleted_at
                    || !$object_to_analyze->sitemap
                    || ($object_to_analyze->category_id && !$object_to_analyze->parent)
                    || ($object_to_analyze->parent && ineffective($object_to_analyze->parent))
                    || ($object_to_analyze->sitemap && ineffective($object_to_analyze->sitemap))
                ) {
                    return true;
                }
                break;
            default:
                break;
        }

        return !$object_to_analyze;

    }
}
if (!function_exists('ineffectiveAdvanced')) {
    function ineffectiveAdvanced($object_to_analyze)
    {

        $returnSet = [INEFFECTIVE => false, "data" => [REASON => ""]];


        if (!is_object($object_to_analyze)) {
            return [INEFFECTIVE => true, "data" => [REASON => "Bir nesne değil: " . gettype($object_to_analyze)]];
        }

        $model_type = get_class($object_to_analyze);
        $parent = null;
        $parent_class = null;


        switch ($model_type) {
            case MEDIAPRESS_MODELS_SITEMAP_DETAIL_STR:
                if (!$object_to_analyze->sitemap) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i" . " yok"]];
                } else if ($object_to_analyze->sitemap->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i" . " silinmiş"]];
                } else if (ineffective($object_to_analyze->sitemap)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i" . " etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->sitemap)]];
                }
                break;
            case MEDIAPRESS_MODELS_PAGE_DETAIL_STR:
                if (!$object_to_analyze->page) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sayfası yok"]];
                } else if ($object_to_analyze->page->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sayfası silinmiş"]];
                } else if (ineffective($object_to_analyze->page)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sayfası etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->page)]];
                }
                break;
            case MEDIAPRESS_MODELS_CATEGORY_DETAIL_STR:
                if (!$object_to_analyze->category) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Kategorisi yok"]];
                } else if ($object_to_analyze->category->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Kategorisi silinmiş"]];
                } else if (ineffective($object_to_analyze->category)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Kategorisi etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->category)]];
                }
                break;
            case 'Mediapress\Models\Sitemap':
                if ($object_to_analyze->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sili" . "nmiş"]];
                } else if (!$object_to_analyze->website) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Websitesi yok"]];
                } else if ($object_to_analyze->website->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Websitesi silinmiş"]];
                }
                break;
            case 'Mediapress\Models\Page':
                if ($object_to_analyze->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Silinmiş"]];
                } else if (!$object_to_analyze->sitemap) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i yok"]];
                } else if ($object_to_analyze->sitemap->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i silinmiş"]];
                } else if ($object_to_analyze->page_id && !$object_to_analyze->page) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sayfası var görünüyor, yok"]];
                } else if ($object_to_analyze->page_id && ineffective($object_to_analyze->page)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sayfası etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->page)]];
                } else if (ineffective($object_to_analyze->sitemap)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->sitemap)]];
                }
                break;
            case 'Mediapress\Models\Category':
                if ($object_to_analyze->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Silinmiş"]];
                } else if (!$object_to_analyze->sitemap) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i yok"]];
                } else if ($object_to_analyze->sitemap->deleted_at) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i silinmiş"]];
                } else if ($object_to_analyze->category_id && !$object_to_analyze->parent) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Kategorisi var görünüyor, yok"]];
                } else if ($object_to_analyze->parent && ineffective($object_to_analyze->parent)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Kategorisi etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->parent)]];
                } else if (ineffective($object_to_analyze->sitemap)) {
                    return [INEFFECTIVE => true, "data" => [REASON => "Sitemap'i etkisiz", PARENT_DATA => ineffectiveAdvanced($object_to_analyze->sitemap)]];
                }
                break;
            default:
                break;
        }

        return [INEFFECTIVE => false, "data" => []];

    }
}

if (!function_exists('is_countable')) {
    function is_countable($var)
    {
        return is_array($var) || $var instanceof \Illuminate\Support\Collection;
    }
}

if (!function_exists('getCategoryName')) {
    function getCategoryName($cat_id)
    {
        $category = Category::where("id", $cat_id)->with(DETAIL_STR)->first();
        return $category ? $category->detail->name : "";
    }
}

if (!function_exists('getCategoryNamesOfPage')) {
    function getCategoryNamesOfPage($page_id, $language_id)
    {

        $category_ids = \Mediapress\Models\CategoryPage::where('page_id', $page_id)->get([CATEGORY_ID])->pluck(CATEGORY_ID);
        if ($category_ids) {
            //return $category_ids;
            return \Mediapress\Models\CategoryDetail::whereIn(CATEGORY_ID, $category_ids)->where(LANGUAGE_ID, $language_id)->get(["id", "name"])->pluck('name', 'id');
        }
        return [];
    }
}

if (!function_exists('limitWords')) {
    function limitWords($text, $words_max)
    {
        $exploded = explode(' ', $text, $words_max + 1);
        if (count($exploded) > $words_max) {
            array_pop($exploded);
        }
        return implode(' ', $exploded);
    }
}


if (!function_exists('getModules')) {
    function getModules($module_key = null)
    {
        $key = "app.modules" . ($module_key ? ".$module_key" : "");
        if ($module_key) {
            return [$module_key => config($key)];
        }
        return config($key);
    }
}


if (!function_exists('userAction')) {
    function userAction($ability, $can = true, $cannot = false, $extraparams = [])
    {
        return true;
        $admin = Session::get('panel.user');
        if ($admin->can($ability)) {
            $act = $can;
        } else {
            $act = $cannot;
        }

        if (is_callable($act)) {
            call_user_func($act, [Auth::user, $ability, $extraparams]);
        } else if (!is_numeric($act)) {
            return $act;
        } else {
            return $act;
        }
    }
}

if (!function_exists("activeUserCan")) {
    function activeUserCan(array $abilities_to_check = [])
    {
        if (!count($abilities_to_check)) {
            return false;
        }

        $user = session("panel.user");
        if (!$user) {
            return false;
        }


        return permissionCheck($user, $abilities_to_check);
    }
}

if (!function_exists("permissionCheck")) {
    /**
     * @param array $abilities_to_check
     * @return bool
     */
    function permissionCheck(\Mediapress\Modules\Auth\Models\Admin $admin, array $abilities_to_check): bool
    {
        // abilities_to_check parametresindeki kontrol edilecek ability'ler özelden genele şeklinde sıralanmış olmalıdır.
        // İzin verilmemiş / yasaklanmamış / tanımlanmamış izinler üzerinden geçtikçe false döndürmeyi bekler
        // İzin verilmiş / yasaklanmış izne rastladığında true ya da false döndürür.

        $allowed_abilities = $admin->getAbilities()->pluck("name")->toArray();
        $forbidden_abilities = $admin->getForbiddenAbilities()->pluck("name")->toArray();

        $forbidden = null;
        $allowed = null;

        $forbidden = in_array("*", $forbidden_abilities) ? true : false;

        if (!$forbidden) {
            $allowed = in_array("*", $allowed_abilities) || $admin->can("*") ? true : false;
            if ($allowed) {
                return true;
            }
        } else {
            return false;
        }

        foreach ($abilities_to_check as $ability) {

            $forbidden = null;
            $allowed = null;
            $forbidden = in_array($ability, $forbidden_abilities) ? true : false;
            if ($forbidden === false) {

                $allowed = in_array($ability, $allowed_abilities) ? true : false;
                if ($allowed) {
                    return true;
                }
            } else {
                return false;
            }
        }

        return false;
    }
}

if (!function_exists("notPermittedPage")) {
    function notPermittedPage($title = null, $message = null)
    {
        return redirect()->route("MPCore.not_permitted")->withErrors(["title" => $title, "message" => $message]);
    }
}

if (!function_exists("notPermittedRedirectBack")) {
    function notPermittedRedirectBack($title = null, $message = null)
    {
        $title = is_null($title) ? "Yetki Yok" : $title;
        $message = is_null($message) ? "Bu işlemi yapmak için yetkiniz bulunmamaktadır. Sistem yöneticiniz ile görüşün." : $message;

        return redirect()->back()->withErrors(["title" => $title, "message" => $message]);
    }
}
if (!function_exists("notPermittedRedirectTo")) {
    function notPermittedRedirectTo($route, $title = null, $message = null)
    {
        $title = is_null($title) ? "Yetki Yok" : $title;
        $message = is_null($message) ? "Bu işlemi yapmak için yetkiniz bulunmamaktadır. Sistem yöneticiniz ile görüşün." : $message;

        return redirect()->to($route)->withErrors(["title" => $title, "message" => $message]);
    }
}

if (!function_exists("errorPage")) {
    function errorPage($title = null, $message = null)
    {
        return redirect()->route("MPCore.error")->withErrors(["title" => $title, "message" => $message]);
    }
}

if (!function_exists("errorView")) {
    function errorView($title = null, $message = null)
    {
        return view("MPCorePanel::generic.error", compact("title", "message"));
    }
}

if (!function_exists("notPermittedView")) {
    function notPermittedView($title = null, $message = null)
    {
        $title = is_null($title) ? "Yetki Yok" : $title;
        $message = is_null($message) ? "Bu işlemi yapmak için yetkiniz bulunmamaktadır. Sistem yöneticiniz ile görüşün." : $message;

        return errorView($title, $message);
    }
}

if (!function_exists("createRejectResponse")) {
    function createRejectResponse($response_type, $default_response = null, $title = null, $message = null, $response_code = 401, $redirect_route = null)
    {
        switch ($response_type) {
            case "plain_text":
                return response($title . ": " . $message, $response_code)->header('Content-Type', 'text/plain');
                break;
            case "bs-alert":
                $html = '<div class="alert alert-danger" role = "alert" >
                    <strong > ' . $title . '</strong >
                    <p>' . $message . '</p>
                </div >';
                return response($html, $response_code)->header('Content-Type', 'text/plain');
                break;
            case "modal-body-content":
                $html = '<div class="modal-header"><h4 class="modal-title">' . $title . '</h4><button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button></div><div class="modal-body"><div class="alert alert-danger" role = "alert" >
                    <strong> ' . $title . '</strong>
                    <p>' . $message . '</p>
                </div></div>';
                return response($html, $response_code)->header('Content-Type', 'text/plain');
                break;
            case "json":
                return response($response_code)->json(["title" => $title, "message" => $message]);
                break;
            case "redirect_page":
                return notPermittedPage($title, $message);
                break;
            case "redirect_back":
                return notPermittedRedirectBack($title, $message);
                break;
            case "redirect_to":
                return notPermittedRedirectTo($redirect_route, $title, $message);
                break;
            case "view":
                return notPermittedView($title, $message);
                break;
            default:
                return $default_response ?? notPermittedPage($title, $message);
                break;

        }
    }
}


if (!function_exists("rejectResponse")) {
    function rejectResponse($title = null, $reject_message = null, $force_response_type = null, $redirect_route = null, $response_code = 401)
    {
        $title = is_null($title) ? "Yetki Yok" : $title;
        $reject_message = is_null($reject_message) ? "Bu işlemi yapmak için yetkiniz bulunmamaktadır. Sistem yöneticiniz ile görüşün." : $reject_message;
        if (request()->ajax()) {
            if (request()->wantsJson()) {
                $default_response = response($response_code)->json(["title" => $title, "message" => $reject_message]);
            }
            $default_response = response($title . ": " . $reject_message, $response_code)->header('Content-Type', 'text/plain');
        }
        $default_response = notPermittedView($title, $reject_message);
        return createRejectResponse($force_response_type, $default_response, $title, $reject_message, $response_code, $redirect_route);
    }
}


if (!function_exists('actionOnVariationForbidden')) {
    function actionOnVariationForbidden($ability)
    {

        if (!Auth::user()->isAn('SuperMCAdmin')) {
            $abilityQuery = DB::table('abilities')->where('name', $ability)->first();


            $result = DB::table('permissions')->where('ability_id', $abilityQuery->id)->first();

            if ($result != null) {
                if ($result->forbidden == 0) {
                    return true;
                } elseif ($result->forbidden == 1) {
                    return false;
                }
            } else {
                $ability = preg_replace("([0-9]+)", "", $ability);
                if (!userAction($ability, true, false)) {
                    return false;
                }
            }

        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////
/// YENİ PANEL

// Tablo, kolon ve kolon adına göre email listesi oluşturma
if (!function_exists('createEmailList')) {
    function createEmailList($tables, $columns, $table_names = null, $names = null)
    {
        $data = [];
        foreach ($tables as $key => $value) {
            $data[$key] = DB::table($value)->get();
        }

        $temp = [];
        $counter = 0;
        foreach ($data as $key => $value) {
            foreach ($data[$key] as $row) {
                $array = (array)$row;
                // aynı e-posta adresinden başka yoksa listeye ekle
                if (!in_array($array[$columns[$key]], $temp)) {
                    $temp[$counter]['email'] = $array[$columns[$key]]; // emailler
                    $temp[$counter]['username'] = $array[$names[$key]]; // emailler
                    $temp[$counter]['name'] = $table_names[$key]; // array içinde gruplama için ayırt edici özellik (tablo isimleri)
                }
                $counter++;
            }
        }
        return $temp;
    }
}

// Curl
if (!function_exists('curlGenerator')) {
    function curlGenerator($url, $apikey = null, $method = null, $json = null, $post = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($json) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        }
        if ($method) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        if ($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        if ($apikey) {
            curl_setopt($ch, CURLOPT_USERPWD, "anystring" . ":" . $apikey);
        }

        $headers = array();
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return json_decode($result);
    }
}

if (!function_exists('dataSource')) {
    function dataSource($dsClassName)
    {
        if (!class_exists($dsClassName)) {
            return new $dsClassName;
        } else {
            throw new Exception("DataSource class not found");
        }
    }
}
if (!function_exists('dataSourceData')) {
    function datasourceData($dsClassName)
    {
        if (class_exists($dsClassName)) {
            return (new $dsClassName)->getData();
        } else {
            throw new Exception("DataSource class not found");
        }
    }
}

if (!function_exists('settingSunFindValue')) {
    function settingSunFindValue($key)
    {
        $settings = Cache::get("settingSun");
        if (!$settings) {
            return null;
        }
        foreach ($settings as $setting) {
            if ($setting->key == $key) {
                return $setting->value;
            }
        }
        // DB de bulamazsa config'de ara
        if (config('mediapress.' . $key)) {
            return config('mediapress.' . $key);
        } else {
            return null;
        }
    }
}

if (!function_exists('settingSun')) {
    function settingSun($key)
    {
        $setting = \Mediapress\Modules\MPCore\Models\SettingSun::where('key', $key)
            ->first();

        if ($setting) {
            return $setting->value;
        }
        return null;
    }
}

if (!function_exists('hasMethod')) {
    function hasMethod($facade, $method)
    {
        $class_methods = get_class_methods(app($facade));
        if (in_array($method, $class_methods)) {
            return 1;
        } else {
            dd("| $method | adında bir method bulunamadı | $facade | Modülünü kontrol ediniz");
        }
    }
}

if (!function_exists('translateDate')) {
    function translateDate($f, $zt = 'now')
    {
        $z = date("$f", strtotime($zt));
        $donustur = array(
            'Monday' => 'Pazartesi',
            'Tuesday' => 'Salı',
            'Wednesday' => 'Çarşamba',
            'Thursday' => 'Perşembe',
            'Friday' => 'Cuma',
            'Saturday' => 'Cumartesi',
            'Sunday' => 'Pazar',
            'January' => 'Ocak',
            'February' => 'Şubat',
            'March' => 'Mart',
            'April' => 'Nisan',
            'May' => 'Ma' . 'yıs',
            'June' => 'Haziran',
            'July' => 'Temmuz',
            'August' => 'Ağustos',
            'September' => 'Eylül',
            'October' => 'Ekim',
            'November' => 'Kasım',
            'December' => 'Aralık',
            'Mon' => 'Pts',
            'Tue' => 'Sal',
            'Wed' => 'Çar',
            'Thu' => 'Per',
            'Fri' => 'Cum',
            'Sat' => 'Cts',
            'Sun' => 'Paz',
            'Jan' => 'Oca',
            'Feb' => 'Şub',
            'Mar' => 'Mar',
            'Apr' => 'Nis',
            'Jun' => 'Haz',
            'Jul' => 'Tem',
            'Aug' => 'Ağu',
            'Sep' => 'Eyl',
            'Oct' => 'Eki',
            'Nov' => 'Kas',
            'Dec' => 'Ara',
        );
        foreach ($donustur as $en => $tr) {
            $z = str_replace($en, $tr, $z);
        }
        if (strpos($z, 'Mayıs') !== false && strpos($f, 'F') === false) {
            $z = str_replace('Mayıs', 'May', $z);
        }
        return $z;
    }

}

if (!function_exists('ActivityLogger')) {
    function ActivityLogger($type)
    {
        event(new Mediapress\Events\ActivityLogged($type));
    }
}

if (!function_exists('slug')) {
    function slug($text)
    {
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
}

if (!function_exists("is_closure")) {
    function is_closure($t)
    {
        return is_object($t) && ($t instanceof Closure);
    }
}

if (!function_exists("statusTranslate")) {
    function statusTranslate($status)
    {
        if ($status == 0) {
            $text = trans("MPCorePanel::general.passive");
        } else if ($status == 1) {
            $text = trans("MPCorePanel::general.active");
        } else if ($status == 2) {
            $text = trans("MPCorePanel::general.draft");
        } else {
            $text = trans("MPCorePanel::general.predraft");
        }
        return $text;
    }
}

if (!function_exists("createBuilderRenderableObjects")) {
    function createBuilderRenderableObjects($module)
    {
        $value = \Mediapress\Facades\ModulesEngine::getModuleClasses()[\Illuminate\Support\Str::ucfirst($module)];
        return array_combine(array_map("\Str::snake", array_keys($value)), array_values($value));
    }
}

if (!function_exists("pluralToSingular")) {
    function pluralToSingular($word)
    {

        $rules = [
            'ss' => false,
            'os' => 'o',
            'ies' => 'y',
            'xes' => 'x',
            'oes' => 'o',
            'ies' => 'y',
            'ves' => 'f', // knives => knif! right=> knife.
            's' => ''
        ];

        foreach (array_keys($rules) as $key) {

            if (substr($word, (strlen($key) * -1)) != $key) {
                continue;
            }

            if ($key === false) {
                return $word;
            }

            return substr($word, 0, strlen($word) - strlen($key)) . $rules[$key];
        }
        return $word;
    }
}

if (!function_exists("getFlag")) {
    function getFlag($language)
    {
        $flag = "vendor/mediapress/images/flags/";
        if (is_a($language, \Mediapress\Modules\MPCore\Models\Language::class)) {
            $flag .= $language->flag;
        } else {
            $flag .= mb_strtoupper($language) . ".png";
        }
        return is_file(public_path($flag)) ? url($flag) : url("vendor/mediapress/images/flags/0.png");
    }
}

if (!function_exists("get_variation_local_title")) {
    function get_variation_local_title($detail, $put_flag = true, $flag_height = 13, $quote_char = "'")
    {
        $str = "";
        if ($detail->language) {
            if ($put_flag) {
                $imgsrc = getFlag($detail->language);
                $str .= $imgsrc ? '<img src=' . $quote_char . $imgsrc . $quote_char . ' height=' . $quote_char . $flag_height . $quote_char . ' /> ' : "";
            }
            $str .= strtoupper($detail->language->code);
        }

        if ($detail->countryGroup) {
            $str .= " (" . $detail->countryGroup->title . ")";
        }

        return $str;
    }
}

if (!function_exists("getVariationLocalTitle")) {
    function getVariationLocalTitle($country_group_title, $language_code, $put_flag = true, $flag_height = 13, $quote_char = "'")
    {
        $str = "";
        if ($language_code) {
            if ($put_flag) {
                $imgsrc = getFlag($language_code);
                $str .= $imgsrc ? '<img src=' . $quote_char . $imgsrc . $quote_char . ' height=' . $quote_char . $flag_height . $quote_char . ' /> ' : "";
            }
            $str .= strtoupper($language_code);
        }

        if ($country_group_title) {
            $str .= " (" . $country_group_title . ")";
        }

        return $str;
    }
}

if (!function_exists("getWebsiteRootUrl")) {
    function getWebsiteRootUrl($website_or_website_id)
    {
        $website = objectifyOneParam('Mediapress\Modules\Content\Models\Website', $website_or_website_id);

        $scheme = $website->ssl ? "https://" : 'http://';
        $domain = $website->slug;
        if ($website->type == 'folder' && $website->website) {
            $domain = $website->website->slug . '/' . $domain;
        }
        return "$scheme$domain";
    }
}

if (!function_exists("getWebsiteDetailUrl")) {
    function getWebsiteDetailUrl($website_or_website_id, $cg_or_cg_id, $lng_or_lng_id)
    {
        return URLEngine::getWebsiteUrlBase($website_or_website_id, $cg_or_cg_id, $lng_or_lng_id, "");
    }
}

if (!function_exists("getSitemapCategoryUrlBase")) {
    function getSitemapCategoryUrlBase($sitemap_or_sitemap_id, $cg_or_cg_id, $lng_or_lng_id)
    {
        $sitemap = objectifyOneParam('Mediapress\Modules\Content\Models\Sitemap', $sitemap_or_sitemap_id);
        $country_group = $cg_or_cg_id ? objectifyOneParam('Mediapress\Modules\MPCore\Models\CountryGroup', $cg_or_cg_id) : $sitemap->detail->countryGroup;
        $language = $lng_or_lng_id ? objectifyOneParam('Mediapress\Modules\MPCore\Models\Language', $lng_or_lng_id) : $sitemap->detail->language;
        return getWebsiteRootUrl($sitemap->website) . '/' . implode('/', \Mediapress\Facades\URLEngine::getSitemapUrlBase($sitemap, $country_group, $language, 'Mediapress\Modules\Content\Models\CategoryDetail'));
    }
}

if (!function_exists("getSitemapUrlBase")) {
    function getSitemapUrlBase($sitemap_or_sitemap_id, $cg_or_cg_id, $lng_or_lng_id)
    {
        $sitemap = objectifyOneParam('Mediapress\Modules\Content\Models\Sitemap', $sitemap_or_sitemap_id);
        $country_group = $cg_or_cg_id ? objectifyOneParam('Mediapress\Modules\MPCore\Models\CountryGroup', $cg_or_cg_id) : $sitemap->detail->countryGroup;
        $language = $lng_or_lng_id ? objectifyOneParam('Mediapress\Modules\MPCore\Models\Language', $lng_or_lng_id) : $sitemap->detail->language;
        return getWebsiteRootUrl($sitemap->website) . '/' . implode('/', \Mediapress\Facades\URLEngine::getSitemapUrlBase($sitemap, $country_group, $language, 'Mediapress\Modules\Content\Models\SitemapDetail'));
    }
}


if (!function_exists("getLanguageById")) {
    function getLanguageById($language_id)
    {
        $language = \Mediapress\Modules\MPCore\Models\Language::find($language_id);
        if ($language) {
            return $language;
        } else {
            return false;
        }
    }
}


if (!function_exists("socialMedia")) {
    function socialMedia($group_id = 1)
    {
        return \Mediapress\Modules\Content\Facades\SocialMediaEngine::socialMedia($group_id);
    }
}

if (!function_exists("langPart")) {
    function langPart($key, $value = null, $array = [], $language_id = null, $country_group_id = null)
    {
        return \Mediapress\Modules\MPCore\Facades\TranslateEngine::langPart($key, $value, $array, $language_id, $country_group_id);
    }
}
if (!function_exists("langPartAttr")) {
    function langPartAttr($key, $value = null, $array = [], $language_id = null, $country_group_id = null)
    {
        return strip_tags(langPart($key, $value, $array, $language_id, $country_group_id));
    }
}

if (!function_exists("panelLangPart")) {
    function panelLangPart($key, $value = null, $language_id = null)
    {
        return \Mediapress\Modules\MPCore\Facades\TranslateEngine::panelLangPart($key, $value, $language_id);
    }
}
if (!function_exists("getPanelLangPartKey")) {
    function getPanelLangPartKey($name, $title, $tab = false)
    {
        $request = request();

        $key = 'sitemap';

        //Sitemap ID
        if($request->getRequestUri() == '/mp-admin/ajax/popup/categoryCreate') {
            $key .= '->' . last(explode('/', url()->previous()));
        } elseif($request->getRequestUri() == '/mp-admin/ajax/popup/createCriteria') {
            $key .= '->' . last(explode('/', url()->previous()));
        } elseif($request->segment(4)) {
            $key .= '->' . $request->segment(4);
        }

        //Model Type
        if($request->segment(3) == 'Pages') {
            $key .= '->page';
        } elseif($request->getRequestUri() == '/mp-admin/ajax/popup/categoryCreate') {
            $key .= '->category';
        } elseif($request->getRequestUri() == '/mp-admin/ajax/popup/createCriteria') {
            $key .= '->criteria';
        }

        $name = str_replace(['sync:', '[]'], '', $name);

        $key .= '->' . ($tab ? 'tab->' : '') . last(explode('->', $name));

        return panelLangPart($key, $title);
    }
}

if (!function_exists("getCategoryCaptionAllChain")) {
    function getCategoryCaptionAllChain(int $cat_id, string $cat_name): string
    {
        $strs = [$cat_name];
        $cat = \Mediapress\Modules\Content\Models\Category::where("id", $cat_id)->first();
        while (true) {
            $cat = $cat->parent;
            if ($cat) {
                $strs[] = $cat->detail->name;
            } else {
                break;
            }
        }
        return implode(' >> ', array_reverse($strs));
    }
}

if (!function_exists("getCategoryCaptionSingleEllipsis")) {
    function getCategoryCaptionSingleEllipsis(int $cat_id, string $cat_name): string
    {
        $strs = [$cat_name];
        $cat = \Mediapress\Modules\Content\Models\Category::where("id", $cat_id)->first();
        $parent_count = 0;
        $last_parent_name = "";
        while (true) {
            $cat = $cat->parent;
            if ($cat) {
                $parent_count++;
                $last_parent_name = $cat->detail->name;
            } else {
                if ($parent_count) {
                    if ($parent_count > 1) {
                        $strs[] = "...";
                    }
                    $strs[] = $last_parent_name;
                }
                break;
            }
        }

        return implode(' >> ', array_reverse($strs));

    }
}

if (!function_exists("getCategoryNameMultiEllipsis")) {
    function getCategoryNameMultiEllipsis(int $cat_id, string $cat_name): string
    {
        $strs = [$cat_name];
        $cat = \Mediapress\Modules\Content\Models\Category::where("id", $cat_id)->first();
        $parent_count = 0;
        $last_parent_name = "";
        while (true) {
            $cat = $cat->parent;
            if ($cat) {
                $strs[] = "...";
                $parent_count++;
                $last_parent_name = $cat->detail->name;
            } else {
                if ($parent_count) {
                    array_pop($strs);
                    $strs[] = $last_parent_name;
                }
                break;
            }
        }

        return implode(' >> ', array_reverse($strs));
    }
}

if (!function_exists("getCategoryNameLastTwo")) {
    function getCategoryNameLastTwo(int $cat_id, string $cat_name): string
    {
        $strs = [$cat_name];
        $cat = \Mediapress\Modules\Content\Models\Category::where("id", $cat_id)->first();
        $parent_count = 0;
        while (true) {
            $cat = $cat->parent;
            if ($cat) {
                $parent_count++;
                if ($parent_count == 1) {
                    $strs[] = $cat->detail->name;
                } elseif ($parent_count == 2) {
                    $strs[] = "...";
                    break;
                }
            } else {
                break;
            }
        }
        return implode(' >> ', array_reverse($strs));

    }
}

if (!function_exists("getCategoryCaption")) {
    function getCategoryCaption(int $cat_id, string $cat_name, string $caption_mode): string
    {
        $result = null;
        $func = "getCategoryName" . \Str::camel($caption_mode);
        switch ($caption_mode) {
            case "all_chain":
            case "single_ellipsis":
            case "multi_ellipsis":
            case "last_two":
                $result = $func($cat_id, $cat_name);
                break;
            case "self_only":
            default:
                $result = $cat_name;
                break;

        }

        return $result;
    }
}

if (!function_exists("requestHasAndEqual")) {
    function requestHasAndEqual($request_name, $value)
    {
        if (request()->has($request_name) && request($request_name) == $value) {
            return true;
        }
        return false;
    }
}

if (!function_exists("getHttpSchemaWithHost")) {
    function getHttpSchemaWithHost()
    {
        $http_schema = request()->server('HTTP_X_FORWARDED_PROTO', request()->server('REQUEST_SCHEME', 'http'));
        return $http_schema . "://" . request()->header("host");
    }
}

if (!function_exists("panelBreadcrumb")) {
    function panelBreadcrumb($breadcrumb)
    {
        $breadcrumb = strtolower($breadcrumb);
        if (Lang::has("MPCorePanel::breadcrumb.modules." . $breadcrumb)) {
            $value = trans("MPCorePanel::breadcrumb.modules." . $breadcrumb);
        } elseif (Lang::has("MPCorePanel::breadcrumb.sub_modules." . $breadcrumb)) {
            $value = trans("MPCorePanel::breadcrumb.sub_modules." . $breadcrumb);
        } elseif (Lang::has("MPCorePanel::breadcrumb.actions." . $breadcrumb)) {
            $value = trans("MPCorePanel::breadcrumb.actions." . $breadcrumb);
        } else {
            $numeric_param = preg_replace('/[^0-9]/', '', $breadcrumb);
            if ($numeric_param != "") {
                $value = $breadcrumb;
            } else {
                if (Lang::has("MPCorePanel::breadcrumb.additions." . $breadcrumb)) {
                    $value = trans("MPCorePanel::breadcrumb.additions." . $breadcrumb);
                } else {
                    $value = null;
                }
            }
        }
        return $value;
    }
}

if (!function_exists("breadcrumb")) {
    function breadcrumb()
    {
        $mediapress = mediapress();

        return new \Mediapress\Modules\Content\Foundation\BreadCrumb($mediapress);
    }
}

if (!function_exists("getSimpleRelationData")) {
    function getSimpleRelationData($model_class, $childs_affix = "Extra", $parents_pk = "id")
    {
        //return $model_class;
        $resultset = [
            PARENT_CLASS => "",
            "child_class" => "",
            PARENT_SHORTNAME => "",
            "parent_flatname" => "",
            CHILD_SHORTNAME => "",
            "child_flatname" => "",
            "childs_foreign_key" => "",
            "possible_parent_table" => "",
            "possible_child_table" => "",
        ];

        $check = strstr($model_class, $childs_affix, true);
        $is_child = $check && ($check . $childs_affix) === $model_class;

        $resultset[PARENT_CLASS] = $is_child ? $check : $model_class;

        $ns = preg_split("/\\\/", $resultset[PARENT_CLASS]);
        $resultset[PARENT_SHORTNAME] = array_pop($ns);
        $resultset["parent_flatname"] = Str::snake($resultset[PARENT_SHORTNAME]);

        $resultset["childs_foreign_key"] = Str::snake($resultset[PARENT_SHORTNAME]) . "_" . $parents_pk;

        $resultset[CHILD_SHORTNAME] = $resultset[PARENT_SHORTNAME] . ucfirst($childs_affix);
        $resultset["child_flatname"] = Str::snake($resultset[CHILD_SHORTNAME]);
        array_push($ns, $resultset[CHILD_SHORTNAME]);
        $resultset["child_class"] = implode("\\", ($ns));

        $resultset["possible_parent_table"] = Str::plural($resultset["parent_flatname"]);
        $resultset["possible_child_table"] = Str::plural($resultset["child_flatname"]);


        return $resultset;

    }
}

if (!function_exists("objectify")) {
    function objectify($object_or_type_id_arr)
    {
        // return if it's a model already
        if (is_object($object_or_type_id_arr)) {
            return $object_or_type_id_arr;
        } elseif (is_array($object_or_type_id_arr)) {
            list($model_type, $model_id) = $object_or_type_id_arr;
            return $model_type::find($model_id);
        }

        throw new \InvalidArgumentException("Function \"objectify\" expects param1 to be an object or an array of model type and id, " . gettype($object_or_type_id_arr) . " given");

    }
}

if (!function_exists("objectifyOneParam")) {
    function objectifyOneParam($object_class, $param)
    {
        if (is_object($param)) {
            return $param;
        }

        return objectify([$object_class, $param]);

    }
}

if (!function_exists('is_robot')) {
    function is_robot($userAgent = null)
    {
        $ua = new Mediapress\Foundation\UserAgent\UserAgent();

        if($ua->robot() == false){
            return false;
        }
        return true;
    }
}

if (!function_exists('ip_details')) {
    function ip_details($ip = null)
    {
        if (!$ip) {
            if (request()->server('HTTP_CF_CONNECTING_IP')) {
                $ip = request()->server('HTTP_CF_CONNECTING_IP');
            } else {
                $ip = request()->server('REMOTE_ADDR');
            }
        }

        session()->has("geo_ip_info.".$ip) ? session("geo_ip_info.".$ip) : session(["geo_ip_info.".$ip => getGeo($ip)]);

        return session("geo_ip_info.".$ip);
    }
}

if (!function_exists('getGeo')) {
    function getGeo($ip)
    {
        try {
            $context = stream_context_create(array(
                'http' => array(
                    'follow_location' => false,
                    'timeout' => 2,
                ),
                'ssl' => array(
                    'verify_peer' => false,
                ),
            ));

            return json_decode(file_get_contents('https://geoipmediaclick.appspot.com/?ip=' . $ip, false, $context), true);

        } catch (\Exception $exception) {

            return array();
        }

    }

}

if (!function_exists('browser_languages')) {
    function browser_languages()
    {
        $re = '/([^-;,]*)(?:-([^;]*))?(?:;q=([0-9]\.[0-9]))?/m';
        $str = request()->server('HTTP_ACCEPT_LANGUAGE');

        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

        $list = [];
        foreach ($matches as $match){
            if(isset($match[1]) && $match[1]){
                $list[] = $match[1];
            }

        }
        $list = array_values(array_unique($list));

        return $list;
    }
}



if (!function_exists('dataGetAndMerge')) {
    function dataGetAndMerge($menu, $key, $added_arr)
    {
        $get_arr = data_get($menu, $key, []);
        $arr = data_set($menu, $key, array_merge($get_arr, $added_arr));

        return $arr;
    }
}

if (!function_exists("getRecommendedSplitSizeForSitemapXmls")) {
    function getRecommendedSplitSizeForSitemapXmls($website_or_website_id)
    {

        $website = objectifyOneParam('Mediapress\Modules\Content\Models\Website', $website_or_website_id);
        try {
            // detect possible url count for one entry (1 default + n alternate urls)
            $variations = 0;
            $cgs = $website->countryGroups()->with("languages")->get();
            foreach ($cgs as $cg) {
                $variations += count($cg->languages);
            }
            // compute how many entries fill projected quota (1200)
            $recommended_split_size = (1200 - (1200 % $variations)) / $variations;
        } finally {
            $recommended_split_size = $recommended_split_size ?: 120;
        }
        return $recommended_split_size;
    }
}


if (!function_exists('webp')) {
    function webp()
    {
        $webp = new Browser();
        if ($webp->supportWebP()) {
            return 1;
        }
        return 0;
    }
}

if (!function_exists("sidebar")) {
    function sidebar($slug, $depth = 0, $cgId = 1, $langId = 760)
    {
        $url = request()->path();

        $current = MenuDetail::where('status', 1)
            ->whereHas('url', function ($q) use ($url) {
                $q->where("url", "LIKE", "%$url%")
                    ->orWhere("out_link", "LIKE", "%$url%");

            })
            ->whereHas('menu', function ($q) use ($slug) {
                $q->where('slug', $slug);
            })
            ->first();

        $return = [];
        if (!is_null($current)) {
            $parents = MenuDetail::where('menu_id', $current->menu_id)
                ->where("lft", "<=", $current->lft)
                ->where("rgt", ">=", $current->rgt)
                ->where('depth', $depth)
                ->where("country_group_id", $cgId)
                ->where("language_id", $langId)
                ->orderBy('lft')
                ->get();

            $parent = $parents->first();
            $menus = MenuDetail::where('menu_id', $current->menu_id)
                ->where('status', 1)
                ->whereHas('menu')
                ->where("lft", ">=", $parent->lft)
                ->where("rgt", "<=", $parent->rgt)
                ->where("country_group_id", $cgId)
                ->where("language_id", $langId)
                ->orderBy('lft')
                ->get();

            $parent_arr = $parents->pluck(['id'])->toArray();
            array_push($parent_arr, $current->id);
            $sidebar_cluster = cluster($menus, ["parent_id" => "parent"]);
            $return = setSidebarRecursive($sidebar_cluster, $parent_arr);
        }

        return count($return) > 1 ? $return : array_shift($return);
    }

    function setSidebarRecursive($sidebar_cluster, $parents = array())
    {
        $hold = array();
        foreach ($sidebar_cluster as $sidebar) {
            if ($sidebar->url) {
                $sidebar_link = $sidebar->url->url;
            } else {
                $sidebar_link = 'javascript:void(0)';
            }

            $sidebar_temp = [
                'id' => $sidebar->id,
                'name' => $sidebar->name,
                'url' => $sidebar_link,
                'active' => in_array($sidebar->id, $parents),
                'target' => 'target=' . !is_null($sidebar->target) ? $sidebar->target : '_self',
            ];
            if ($sidebar->children->isNotEmpty()) {
                $sidebar_temp['children'] = setSidebarRecursive($sidebar->children, $parents);
            } else {
                $sidebar_temp['children'] = [];
            }
            $hold[] = $sidebar_temp;
        }
        return $hold;
    }
}


if (!function_exists('searchUrl')) {
    function searchUrl()
    {
        $sitemap = Sitemap::where('feature_tag', 'search')
            ->first();

        if ($sitemap && $sitemap->detail) {
            return url($sitemap->detail->url->url);
        }
        return "javascript:void(0);";
    }
}

if (!function_exists('searchQuery')) {
    function searchQuery($query, $per_page, $language_id)
    {
        return Search::where('language_id', $language_id)
            ->select('model_id', 'model_type', 'detail_name', 'detail_search_text', 'detail_detail', 'url_id')
            ->where('detail_name', '<>', '')
            ->whereHas('url')
            ->with('url', 'model')
            ->where(function ($q) use ($query) {
                $q->where('detail_name', 'like', '%' . $query . '%')
                    ->orWhere('detail_detail', 'like', '%' . $query . '%')
                    ->orWhere('detail_search_text', 'like', '%' . $query . '%')
                    ->orWhere('cvar_1', 'like', '%' . $query . '%')
                    ->orWhere('cvar_2', 'like', '%' . $query . '%')
                    ->orWhere('ctex_1', 'like', '%' . $query . '%')
                    ->orWhere('ctex_2', 'like', '%' . $query . '%')
                    ->orWhere('cint_1', 'like', '%' . $query . '%')
                    ->orWhere('cint_2', 'like', '%' . $query . '%')
                    ->orWhere('cdat_1', 'like', '%' . $query . '%')
                    ->orWhere('cdat_2', 'like', '%' . $query . '%')
                    ->orWhere('cdec_1', 'like', '%' . $query . '%')
                    ->orWhere('cdec_2', 'like', '%' . $query . '%')
                    ->orWhere('detail_value', 'like', '%' . $query . '%')
                    ->orWhere('value', 'like', '%' . $query . '%');
            })->orderByRaw("case
                when `detail_name` like ? then 1
                when `detail_name` like ? then 4
                when `detail_name` like ? then 2
                when `detail_name` like ? then 5
                when `detail_name` like ? then 16
                when `detail_name` like ? then 18
                when `detail_name` like ? then 3
                when `detail_name` like ? then 17

                when `detail_detail` like ? then 6
                when `detail_detail` like ? then 9
                when `detail_detail` like ? then 7
                when `detail_detail` like ? then 10
                when `detail_detail` like ? then 19
                when `detail_detail` like ? then 21
                when `detail_detail` like ? then 8
                when `detail_detail` like ? then 20

                when `detail_search_text` like ? then 11
                when `detail_search_text` like ? then 14
                when `detail_search_text` like ? then 12
                when `detail_search_text` like ? then 15
                when `detail_search_text` like ? then 22
                when `detail_search_text` like ? then 14
                when `detail_search_text` like ? then 13
                when `detail_search_text` like ? then 23

                when `detail_value` like ? then 24
                when `detail_value` like ? then 28
                when `detail_value` like ? then 25
                when `detail_value` like ? then 29
                when `detail_value` like ? then 22
                when `detail_value` like ? then 27
                when `detail_value` like ? then 26
                when `detail_value` like ? then 23

                when `value` like ? then 32
                when `value` like ? then 36
                when `value` like ? then 33
                when `value` like ? then 37
                when `value` like ? then 30
                when `value` like ? then 35
                when `value` like ? then 34
                when `value` like ? then 31

                else 1000 end", [
                $query . ' %',              //1
                $query . '%',               //4
                '% ' . $query . ' %',       //2
                '% ' . $query . '%',        //5
                '%' . $query . ' %',        //16
                '%' . $query . '%',         //18
                '% ' . $query . '',         //3
                '%' . $query . '',          //17

                $query . ' %',              //6
                $query . '%',               //9
                '% ' . $query . ' %',       //7
                '% ' . $query . '%',        //10
                '%' . $query . ' %',        //19
                '%' . $query . '%',         //21
                '% ' . $query . '',         //8
                '%' . $query . '',          //20

                $query . ' %',              //11
                $query . '%',               //14
                '% ' . $query . ' %',       //12
                '% ' . $query . '%',        //15
                '%' . $query . ' %',        //22
                '%' . $query . '%',         //14
                '% ' . $query . '',         //13
                '%' . $query . '',          //23

                $query . ' %',              //11
                $query . '%',               //14
                '% ' . $query . ' %',       //12
                '% ' . $query . '%',        //15
                '%' . $query . ' %',        //22
                '%' . $query . '%',         //14
                '% ' . $query . '',         //13
                '%' . $query . '',          //23

                $query . ' %',              //32
                $query . '%',               //36
                '% ' . $query . ' %',       //33
                '% ' . $query . '%',        //37
                '%' . $query . ' %',        //30
                '%' . $query . '%',         //35
                '% ' . $query . '',         //34
                '%' . $query . '',          //31

            ])->groupBy("detail_name")->paginate($per_page, ["*"], 'p');
    }
}

if (!function_exists("dateFilled")) {
    function dateFilled($model, $field)
    {
        return $model->getOriginal($field) && ($model->getOriginal($field) !== '0000-00-00 00:00:00');
    }
}

if (!function_exists("str_ucwords")) {
    function str_ucwords($str, $langCode = 760)
    {
        return ltrim(mb_convert_case(str_replace(array(' I', ' ı', ' İ', ' i'), array(' I', ' I', ' İ', ' İ'), ' ' . strto('lower', $str, $langCode)), MB_CASE_TITLE, "UTF-8"));
    }
}

if (!function_exists("str_ucfirst")) {
    function str_ucfirst($str, $langCode = 760)
    {
        if ($langCode == 760) {
            $tmp = preg_split(
                "//u", strto('lower', $str, $langCode), 2,
                PREG_SPLIT_NO_EMPTY
            );
            return mb_convert_case(str_replace(array('ı', 'ğ', 'ü', 'ş', 'i', 'ö', 'ç'), array('I', 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç'), $tmp[0]), MB_CASE_TITLE, "UTF-8") . $tmp[1];
        }
        return ucfirst(strtolower($str));
    }
}

if (!function_exists("strto")) {
    function strto($to, $str, $langCode = 760)
    {
        $return = "";
        if ($to == 'lower') {
            if ($langCode == 760) {
                return mb_strtolower(str_replace(array('I', 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç'), array('ı', 'ğ', 'ü', 'ş', 'i', 'ö', 'ç'), $str));
            }
            $return = mb_strtolower($str);
        } elseif ($to == 'upper') {
            if ($langCode == 760) {
                return mb_strtoupper(str_replace(array('ı', 'ğ', 'ü', 'ş', 'i', 'ö', 'ç'), array('I', 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç'), $str));
            }
            $return = mb_strtoupper($str);
        } else {
            trigger_error('Lütfen geçerli bir strto() parametresi giriniz.', E_USER_ERROR);
        }
        return $return;
    }
}

if (!function_exists("getStatusIndicatorCircle")) {
    function getStatusIndicatorCircle($status)
    {
        $status_class = null;
        $statusses = [
            0 => "passive",
            1 => "active",
            2 => "draft",
            3 => "predraft",
            4 => "pending",
            5 => "postdate",
        ];
        switch ($status) {
            case 0:
                //status PASSIVE
                $status_class = "passive";
                break;
            case 1:
                //status ACTIVE
                $status_class = "active";
                break;
            case 2:
                //status DRAFT
                $status_class = "passive";
                break;
            case 3:
                //status PREDRAFT
                $status_class = "passive";
                break;
            case 4:
                //status PENDING
                $status_class = "pending";
                break;
            case 5:
                //status POSTDATE
                $status_class = "pending";
                break;
        }

        return $status_class ? '<i class="status_circle mr-1 ' . $status_class . '" title="' . (isset($statusses[$status]) ? trans("ContentPanel::general.status_title." . ($statusses[$status])) : "") . '"></i>' : "";
    }
}

if (!function_exists("isMCAdmin")) {
    function isMCAdmin()
    {
        return session()->has("admin.mc") && session("admin.mc");
    }
}

if (!function_exists("getPathOfFileUrl")) {
    function getPathOfFileUrl()
    {
        $toreconstruct = request()->segments();
        $root = request()->root();

        array_pop($toreconstruct);
        array_unshift($toreconstruct, $root);

        return implode('/', $toreconstruct);
    }
}

if (!function_exists("formatBytes")) {
    function formatBytes($bytes, $precision = 2)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, $precision) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, $precision) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, $precision) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }
}

if (!function_exists("cacheKey")) {

    function cacheKey(array $data = array())
    {
        $request = request();
        $domain = $request->getHost();
        $protocol = $request->getScheme();
        $url = $request->getRequestUri();
        $mediapress = new \Mediapress\Foundation\Mediapress();
        $webp = function_exists('imagewebp') ? webp() : '';

        $key = $protocol . $domain . $url . $mediapress->activeLanguage->id . $webp . implode(".", $data);
        return hash('md5', $key);

    }
}

if (!function_exists("websiteType")) {

    function websiteType()
    {
        $return = 'live';
        if (auth('admin')->check()) {
            $user_email = auth('admin')->user()->email;
            if (strpos($user_email, "@mediaclick.com.tr") !== false || strpos($user_email, "@veron.com") !== false) {
                $server = explode('.', request()->server('HTTP_HOST'));
                if (last($server) == 'local') {
                    $return = 'local';
                } elseif ($server[1] == 'mclck') {
                    $return = 'mclck';
                }
            }
        }
        return $return;

    }
}
if (!function_exists("searchHighlight")) {

    function searchHighlight($key, $detail)
    {

        $detail = strip_tags($detail);
        $detail = preg_replace("/(\r\n|\n|\r)/m", '', $detail);
        $detail = preg_replace("/<\!--.*?-->/m", '', $detail);
        $detail = preg_replace("/(<([^>]+)>)/i", '', $detail);
        $detail = str_ireplace($key, '<b>' . $key . '</b>', $detail);

        $index = mb_strpos($detail, $key);

        if ($index >= 0) {
            if ($index - 150 < 0) {
                $index_start = 0;
                $start_dot = "";
            } else {
                $index_start = $index - 150;
                $start_dot = "...";
            }

            if ($index + mb_strlen($key) + 7 + 150 > mb_strlen($detail)) {
                $index_finish = mb_strlen($detail);
                $finish_dot = "";
            } else {
                $index_finish = $index + mb_strlen($key) + 7 + 150;
                $finish_dot = "...";
            }

            $detail = $start_dot . mb_substr($detail, $index_start, $index_finish - $index_start) . $finish_dot;
        } else {
            if (strlen($detail) > 300) {
                $detail = mb_substr($detail, 0, 300) . "...";
            }
        }

        return $detail;
    }
}


function supFront($value, $key, $class)
{
    if(is_null(config('mediapress.supFront'))) {
        $status = adminAuth() && !isPanel() && !isApi();
        config(['mediapress.supFront' => $status]);
    }

    if (!is_null($class) && config('mediapress.supFront')) {
        Bouncer::useRoleModel(Role::class);
        $tableName = $class->getTable();
        $permissions = supFrontPermissions($tableName, $class);
        $permissionStatus = count($permissions) > 0 ? activeUserCan($permissions) : true;
        if($permissionStatus) {

            $classId = ($class->id + 5348) * 294;
            $classArr = [
                "Mediapress\Modules\Content\Models\SitemapDetail" => 90,
                "Mediapress\Modules\Content\Models\SitemapDetailExtra" => 91,
                "Mediapress\Modules\Content\Models\CategoryDetail" => 92,
                "Mediapress\Modules\Content\Models\CategoryDetailExtra" => 93,
                "Mediapress\Modules\Content\Models\PageDetail" => 94,
                "Mediapress\Modules\Content\Models\PageDetailExtra" => 95,
                "Mediapress\Modules\Content\Models\MenuDetail" => 96,
                "Mediapress\Modules\MPCore\Models\LanguagePart" => 97,
            ];



            if(isset($classArr[get_class($class)])) {
                return '<mp
               :contenteditable="edit" class="edit-mp"
               :class="edit ? \' editable\' : \'\'"
               data-key="' . base64_encode($key) . '"
               data-id="' . $classId . '"
               data-model="' . $classArr[get_class($class)] . '"
               v-model="newdata"
               @input="onDivInput($event)"
               v-on:blur="sendData()">' . $value . '</mp>';
            }
            return $value;

        }
    }
    return $value;
}

function supFrontPermissions(string $tableName, $class)
{
    switch ($tableName) {
        case "sitemap_details" :
            return [
                "content.websites.website" . optional($class->parent->website)->id . ".sitemaps.sitemap" . optional($class->parent)->id . ".main_update",
                "content.websites.website" . optional($class->parent->website)->id . ".sitemaps.main_update",
                "content.websites.sitemaps.main_update",
            ];
            break;
        case "sitemap_detail_extras" :
            return [
                "content.websites.website" . optional($class->parent->website)->id . ".sitemaps.sitemap" . optional($class->parent)->id . ".main_update",
                "content.websites.website" . optional($class->parent->website)->id . ".sitemaps.main_update",
                "content.websites.sitemaps.main_update",
            ];
            break;
        case "category_details" :
            return [
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.sitemap" . optional($class->parent->sitemap)->id . ".categories.update",
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.categories.update",
                "content.websites.sitemaps.categories.update"
            ];
            break;
        case "page_details" :
            return [
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.sitemap" . optional($class->parent->sitemap)->id . ".pages.update",
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.pages.update",
                "content.websites.sitemaps.pages.update",
            ];
            break;
        case "page_detail_extras" :
            return [
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.sitemap" . optional($class->parent->sitemap)->id . ".pages.update",
                "content.websites.website" . optional($class->parent->sitemap->website)->id . ".sitemaps.pages.update",
                "content.websites.sitemaps.pages.update",
            ];
            break;
        case "language_parts" :
            return [
                "mpcore.translations.update"
            ];
            break;
        case "menu_details" :
            return [
                "content.websites.website" . optional($class->menu)->website_id . ".menus.update",
                "content.websites.menus.update",
            ];
            break;
        default:
            return [];
    }
}

function adminAuth()
{
    return (\Illuminate\Support\Facades\Auth::guard('admin')->check() && session("panel.website") && session("panel.user") && request()->get('preview') != 1);
}

function isApi() {
    return strpos(request()->url(), 'api/v4.1') !== false;
}


function slider($sliderId)
{
    $slider = Slider::find($sliderId);

    $holder = [];
    if ($slider) {
        $scenes = $slider->scenes()
            ->orderBy('order')
            ->where('status', 1)
            ->with('detail')
            ->get();

        foreach ($scenes as $scene) {
            $tempScene = new Slide([
                'time' => $scene->detail->time,
                'texts' => $scene->detail->texts,
                'buttons' => $scene->detail->buttons,
                'files' => $scene->detail->files,
                'url' => $scene->detail->url,
            ]);

            if (is_null($tempScene->image()->id) && is_null($scene->detail->video_url)) {
                continue;
            }
            $temp = [
                'image' => null,
                'video' => $scene->detail->video_url,
                'texts' => [],
                'buttons' => [],
                'urls' => [],
            ];
            if($slider->type == 'image') {
                $temp['image'] = $tempScene->image();

                for ($j = 1; $j <= $slider->button; $j++) {
                    $temp["buttons"][] = $tempScene->button($j);
                    $temp["urls"][] = $tempScene->url('button', $j);
                }
            }

            for ($i = 1; $i <= $slider->text; $i++) {
                $temp["texts"][] = $tempScene->text($i);
            }

            $holder[] = $temp;
        }
    }

    return $holder;
}

function getSitemapVariations($root_item)
{
    /*if($root_item["item_id"]=="*"){
        echo $root_item["name"];
        return [];
    }*/

    $sitemaps = \Mediapress\Modules\Content\Models\Sitemap::with('detail')->get();
    $sets = [];

    foreach ($sitemaps as $sm) {
        if (!$sm->detail) {
            continue;
        }
        $set = [];
        $set["name"] = 'ContentPanel::auth.sections.sitemap';
        $set["name_affix"] = $sm->detail->name;
        $set['item_model'] = \Mediapress\Modules\Content\Models\Sitemap::class;
        $set['item_id'] = $sm->id;
        $set['actions'] = [
            'update' => ['name' => "MPCorePanel::auth.actions.update",],
            'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
        ];
        $set['settings'] = [
            "auth_view_collapse" => "out"
        ];
        $set['data'] = [
            "is_root" => false,
            "is_variation" => true,
            "is_sub" => true,
            "descendant_of_sub" => true,
            "descendant_of_variation" => true
        ];
        $set['subs'] = $root_item['subs'];
        /*data_set($set['subs'], '*.data.descendant_of_sub', true);
        data_set($set['subs'], '*.data.descendant_of_variation', true);
        data_set($set['subs'], '*.data.is_sub', true);
        data_set($set['subs'], '*.data.is_variation', true);*/
        $sets['sitemap' . $sm->id] = $set;
    }

    return $sets;
}

function getSitemapXmlVariations($root_item)
{
    $websites = \Mediapress\Modules\Content\Models\Website::with(['detail', 'sitemapxmls'])->get();
    $sets = [];

    foreach ($websites as $website) {
        $sitemap_xmls = \Mediapress\Modules\Content\Models\SitemapXml::where("website_id", $website->id)->get();
        foreach ($sitemap_xmls as $sitemap_xml) {
            $set = [];
            $set["name"] = 'ContentPanel::auth.sections.sitemap_xml';
            $set["name_affix"] = "(" . $sitemap_xml->title . ") " . $website->name;
            $set['item_model'] = \Mediapress\Modules\Content\Models\Website::class;
            $set['item_id'] = $website->id;
            $set['actions'] = [
                'update' => ['name' => "MPCorePanel::auth.actions.update",],
                'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
            ];
            $set['settings'] = [
                "auth_view_collapse" => "out"
            ];
            $set['data'] = [
                "is_root" => false,
                "is_variation" => true,
                "is_sub" => false,
                "descendant_of_sub" => false,
                "descendant_of_variation" => false
            ];
            $set['subs'] = $root_item['subs'];

            data_set($set['subs'], '*.data.descendant_of_variation', true);

            $sets['sitemap_xml' . $sitemap_xml->id] = $set;
        }
    }

    return $sets;
}

function getVariations($root_item) {
    $websites = \Mediapress\Modules\Content\Models\Website::with('detail')->get();
    $sets = [];

    foreach ($websites as $website) {
        /*if (!$website->detail) {
            continue;
        }*/
        $set = [];
        $set["name"] = 'ContentPanel::auth.sections.website';
        $set["name_affix"] = $website->name;
        $set['item_model'] = \Mediapress\Modules\Content\Models\Website::class;
        $set['item_id'] = $website->id;
        $set['actions'] = [
            'update' => ['name' => "MPCorePanel::auth.actions.update",],
            'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
        ];
        $set['settings'] = [
            "auth_view_collapse" => "out"
        ];
        $set['data'] = [
            "is_root" => false,
            "is_variation" => true,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ];
        $set['subs'] = $root_item['subs'];
        //data_set($set,"subs.sitemaps.subs.*.data.is_sub",true);
        //data_set($set,"subs.*.data.descendant_of_variation",true);
        $set['subs']['sitemaps']['variations'] = function ($root_item) {
            /*if($root_item["item_id"]=="*"){
                echo $root_item["name"];
                return [];
            }*/

            $sitemaps = \Mediapress\Modules\Content\Models\Sitemap::with('detail')->get();
            $sets = [];

            foreach ($sitemaps as $sm) {
                if (!$sm->detail) {
                    continue;
                }
                $set = [];
                $set["name"] = 'ContentPanel::auth.sections.sitemap';
                $set["name_affix"] = $sm->detail->name;
                $set['item_model'] = \Mediapress\Modules\Content\Models\Sitemap::class;
                $set['item_id'] = $sm->id;
                $set['actions'] = [
                    'update' => ['name' => "MPCorePanel::auth.actions.update",],
                    'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                ];
                $set['settings'] = [
                    "auth_view_collapse" => "out"
                ];
                $set['data'] = [
                    "is_root" => false,
                    "is_variation" => true,
                    "is_sub" => true,
                    "descendant_of_sub" => true,
                    "descendant_of_variation" => true
                ];
                $set['subs'] = $root_item['subs'];
                /*data_set($set['subs'], '*.data.descendant_of_sub', true);
                data_set($set['subs'], '*.data.descendant_of_variation', true);
                data_set($set['subs'], '*.data.is_sub', true);
                data_set($set['subs'], '*.data.is_variation', true);*/
                $sets['sitemap' . $sm->id] = $set;
            }

            return $sets;
        };
        $set['subs']['sitemaps']['variations_title'] = 'ContentPanel::auth.sections.recorded_sitemaps';
        $set['subs']['seo']['subs']['sitemap_xmls']['variations'] = function ($root_item) {
            $websites = \Mediapress\Modules\Content\Models\Website::with(['detail', 'sitemapxmls'])->get();
            $sets = [];

            foreach ($websites as $website) {
                $sitemap_xmls = \Mediapress\Modules\Content\Models\SitemapXml::where("website_id", $website->id)->get();
                foreach ($sitemap_xmls as $sitemap_xml) {
                    $set = [];
                    $set["name"] = 'ContentPanel::auth.sections.sitemap_xml';
                    $set["name_affix"] = "(" . $sitemap_xml->title . ") " . $website->name;
                    $set['item_model'] = \Mediapress\Modules\Content\Models\Website::class;
                    $set['item_id'] = $website->id;
                    $set['actions'] = [
                        'update' => ['name' => "MPCorePanel::auth.actions.update",],
                        'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                    ];
                    $set['settings'] = [
                        "auth_view_collapse" => "out"
                    ];
                    $set['data'] = [
                        "is_root" => false,
                        "is_variation" => true,
                        "is_sub" => false,
                        "descendant_of_sub" => false,
                        "descendant_of_variation" => false
                    ];
                    $set['subs'] = $root_item['subs'];

                    data_set($set['subs'], '*.data.descendant_of_variation', true);

                    $sets['sitemap_xml' . $sitemap_xml->id] = $set;
                }
            }

            return $sets;
        };
        $set['subs']['seo']['subs']['sitemap_xmls']['variations_title'] = 'ContentPanel::auth.sections.recorded_sitemap_xmls';
        data_set($set['subs'], '*.data.descendant_of_variation', true);
        data_set($set['subs'], '*.subs.*.data.descendant_of_variation', true);
        data_set($set['subs'], '*.subs.*.subs.*.data.descendant_of_variation', true);
        /*data_set($set['subs'], '*.data.descendant_of_sub', true);
        data_set($set['subs'], '*.data.is_sub', true);
        data_set($set['subs'], 'sitemaps.subs.*.data.is_sub', true);*/

        $sets['website' . $website->id] = $set;
    }

    return $sets;
}
function getHeraldistVariations($root_item) {
    $websites = \Mediapress\Modules\Content\Models\Website::with('detail')->get();
    $sets = [];

    foreach ($websites as $website) {
        /*                if (!$website->detail) {
                            continue;
                        }*/
        $set = [];
        $set["name"] = $root_item["name"];
        $set["name_affix"] = $website->name;
        $set['item_model'] = $root_item["item_model"] ?? "";
        $set['item_id'] = $root_item["item_id"] ?? "";
        $set['actions'] = $root_item["actions"] ?? [];
        $set['settings'] = [
            "auth_view_collapse" => "out"
        ];
        $set['data'] = [
            "is_root" => false,
            "is_variation" => true,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ];
        $set['subs'] = $root_item['subs'];
        data_set($set['subs'], '*.data.descendant_of_variation', true);

        $sets['website' . $website->id] = $set;
    }

    return $sets;
}

function popupImpressions($popup, $buttonClick = true) {
    if($popup->impressions == 1) {
        $cookiePopups = \Cookie::get('_heim') ? decrypt(\Cookie::get('_heim')) : [];
        $minutes = 60*24*30;

        if(isset($cookiePopups[$popup->id])) {
            if($cookiePopups[$popup->id] >= $popup->impression_count) {
                return true;
            } else {
                $cookiePopups[$popup->id] += 1;
            }
        } else {
            $cookiePopups[$popup->id] = 1;
        }


        if($buttonClick) {
            Cookie::queue(Cookie::make('_heim', encrypt($cookiePopups), $minutes));
        }


    } elseif($popup->impressions == 2) {
        $sessionPopups = session('_heim');
        if(isset($sessionPopups[$popup->id])) {
            if($sessionPopups[$popup->id] >= $popup->impression_count) {
                return true;
            } else {
                $sessionPopups[$popup->id] += 1;
            }
        } else {
            $sessionPopups[$popup->id] = 1;
        }


        if($buttonClick) {
            session(['_heim' => $sessionPopups]);
        }
    }

    return false;
}

function comment(string $key, $parentModel = null) {
    $commentKit = new \Mediapress\Modules\Comment\Foundation\CommentKit();

    $comment = $commentKit->setKey($key);

    if($parentModel) {
        $comment = $comment->setParentModel($parentModel);
    }

    return $comment->init();
}

function parseHtmlHref($data){
    preg_match("/href='(.*?)'/", $data, $matchesUrl);
    if($matchesUrl[1]){
        $path = explode("/",parse_url($matchesUrl[1], PHP_URL_PATH));
        $path = $path[2];
        return $path;
    }
    return $data;
}
