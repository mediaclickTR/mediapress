<?php

return [

    'guards' => [
        'admin' => [
            'driver' => 'session',
            'provider' => 'admins',
        ],
    ],
    'providers' => [
        'admins' => [
            'driver' => 'eloquent',
            'model' => \Mediapress\Modules\Auth\Models\Admin::class,
        ],
        /*'users' => [
            'driver' => 'eloquent',
            'model' => \Mediapress\Models\User::class,
        ]*/
    ]
];
