<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitemapXmlBlocksTable extends Migration
{
    public const SITEMAP_XML_BLOCKS = 'sitemap_xml_blocks';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable(self::SITEMAP_XML_BLOCKS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::SITEMAP_XML_BLOCKS, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('sitemap_xml_id');
                $table->integer('sitemap_id');
                $table->tinyInteger('include_detail')->nullable()->default(null);
                $table->decimal('detail_priority',3,2)->nullable()->default(null);
                $table->string('detail_change_frequency',10)->nullable()->default(null);
                $table->tinyInteger('include_categories')->nullable()->default(null);
                $table->decimal('categories_priority',3,2)->nullable()->default(null);
                $table->string('categories_change_frequency',10)->nullable()->default(null);
                $table->tinyInteger('include_pages')->nullable()->default(null);
                $table->decimal('pages_priority',3,2)->nullable()->default(null);
                $table->string('pages_change_frequency',10)->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::SITEMAP_XML_BLOCKS);
    }
}
