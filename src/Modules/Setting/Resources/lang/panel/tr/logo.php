<?php

return [
    'modal_title' => "Site Logoları",
    'desktop_logo' => "Masaüstü Logo",
    'mobile_logo' => "Mobil Logo",
    'email_logo' => "E-posta Logo",
    'favicon' => "Favicon",
];
