@extends('HeraldistPanel::inc.module_main')
@section('content')
    @push('specific.styles')
        <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
    @endpush
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("HeraldistPanel::contact_management.contact.management.title") !!}</div>
        <div class="float-left">
            <a href="#" class="btn btn-primary" onclick="popup('cm_excel_import', 1);"><i class="fa fa-upload"></i> {!! trans("ContentPanel::seo.excel.import") !!}</a>
            <a href="#" id="export" class="btn btn-primary"> <i class="fa fa-file-excel"></i> {!! trans("ContentPanel::seo.excel.export") !!}</a>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="#" id="send"> {!! trans("HeraldistPanel::contact_management.send") !!}</a>
        </div>
        <div class="clearfix"></div>
        <div class="table-field">
            @if(count($data) == 0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
            @else
                <form action="" method="POST">
                @csrf
                <table>
                    <thead>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.check") !!}</th>
                        <th>{!! trans("HeraldistPanel::contact_management.record.datasource") !!}</th>
                        <th>{!! trans("HeraldistPanel::contact_management.record.total") !!}</th>
                        <th>{!! trans("HeraldistPanel::contact_management.available.email") !!}</th>
                        <th>{!! trans("HeraldistPanel::contact_management.available.phone") !!}</th>
                        <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                    </tr>
                    </thead>
                    @foreach($data as $key=>$value)
                        @if(count($value['sub'])>0)
                        @foreach($value['sub'] as $sub)
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label for="male">
                                            <input type="checkbox" name="checked" value="{!! $key !!}-{!! $sub['id'] !!}" id="checked">
                                        </label>
                                    </div>
                                </td>
                                <td>{!! $sub['name'] !!} ({!! $value['name'] !!})</td>
                                <td>{!! $sub['total'] !!}</td>
                                <td>{!! $sub['total'] !!}</td>
                                <td>{!! $sub['total'] !!}</td>
                                <td>
                                    <a href="{!! $sub['source_url'] !!}"><i class="fa fa-pen"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label for="male">
                                        <input type="checkbox" name="checked" value="{!! $key !!}-0">
                                    </label>
                                </div>
                            </td>
                            <td>{!! $value['name'] !!}</td>
                            <td>{!! $value['total'] !!}</td>
                            <td>{!! $value['total'] !!}</td>
                            <td>{!! $value['total'] !!}</td>
                            <td>
                                <a href="{!! $value['source_url'] !!}"><i class="fa fa-pen"></i></a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
                </form>
            @endif
        </div>
    </div>
@endsection

@push("styles")
    <style>
        tr>td,th{
            text-align:center !important;
        }
        .modal-dialog{
            max-width:70% !important;
        }
        select{
            width:100% !important;
        }
        #sending_emails{
            text-align: center;
            color:green;
            font-size:20px;
            font-weight: bold;
            display: none;
            width:100% !important;
            border:1px solid #f2f2f2;
            padding:5px;
        }
    </style>
@endpush

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function(){
            $('#send').click(function(){
                var ar = [];
                $.each($("input[name='checked']:checked"), function(){
                    ar.push($(this).val());
                });
                if(ar.length>0){
                    popup("contact_management_modal", ar);
                }else{
                    alert("Veri kaynağı seçiniz.")
                }
            });
            $('#export').click(function(e){
                e.preventDefault();
                var ar = [];
                $.each($("input[name='checked']:checked"), function(){
                    ar.push($(this).val());
                });
                if(ar.length>0){
                    excelExport(ar);
                }else{
                    alert("Veri kaynağı seçiniz.")
                }
            });
        });

        function excelExport(ar)
        {
            var selected = ar;
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ route('Heraldist.mpmailler.contact_management.export') }}",
                method: 'POST',
                data: {
                    selected:selected,
                    _token:token
                },
                success: function (data) {
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    });
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        }
    </script>
@endpush
