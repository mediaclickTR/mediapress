<?php

namespace Mediapress\Modules\Heraldist;
use Mediapress\Contracts\HeraldistInterface;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Jobs\SendEmailJob;
use Mediapress\Modules\Heraldist\Jobs\SendMailable;
use Illuminate\Support\Facades\Mail;
use Mediapress\Models\MPModule;
use Illuminate\Routing\Router;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Heraldist\Models\MailTemplate;

class Heraldist extends MPModule //implements HeraldistInterface
{
    public const SUBMENU = "submenu";
    public const TYPE = "type";
    public const TITLE = "title";
    public const URL = "url";
    public $name = "Heraldist";
    public $url = "mp-admin/Heraldist";
    public $description = "Core content manager of Mediapress";
    public $author = "";
    public $menus = [];
    private $plugins = [];

    private $selected_mail_list = null;

    public function install()
    {

    }

    public function fillMenu($menu)
    {

        #region Header Menu > Datasets > Forms > Set

        // Tab Name Set
        data_set($menu, 'header_menu.forms.cols.forms.name',trans("HeraldistPanel::menu_titles.forms"));

        $forms = $this->getForms();

        foreach ($forms as $form){
            $datasets_cols_forms_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => $form->name,
                    self::URL => route("Heraldist.message.show", $form->id)
            ];
        }
        $datasets_cols_forms_rows_set[] = [
            self::TYPE => self::SUBMENU,
            self::TITLE =>trans("HeraldistPanel::menu_titles.ebulletin"),
            self::URL => route("Heraldist.ebulletin.index")
        ];
        $datasets_cols_forms_rows_set[] = [
                self::TYPE => self::SUBMENU,
                self::TITLE =>trans("HeraldistPanel::menu_titles.forms.manage"),
                self::URL => route("Heraldist.forms.index")
        ];

        $menu = dataGetAndMerge($menu, 'header_menu.forms.cols.forms.rows',$datasets_cols_forms_rows_set);


        if(settingSun('heraldist.mailchimp.apikey')) {

            data_set($menu, 'header_menu.forms.cols.email_services.name',trans("HeraldistPanel::menu_titles.email_services"));

            $datasets_cols_email_services_rows_set[] = [
                self::TYPE => self::SUBMENU,
                self::TITLE => "Mailchimp",
                self::URL => url('mp-admin/Heraldist/Mailservice/Mailchimp')
            ];

            $menu = dataGetAndMerge($menu, 'header_menu.forms.cols.email_services.rows',$datasets_cols_email_services_rows_set);
        }

        return $menu;
    }

    public function getForms()
    {
        /** @var Website $website */
        $website = session("panel.website");
        $website_id = $website->id;

        return Form::whereHas('websites', function($query) use ($website_id) {
            $query->where('id', $website_id);
        })->get();

    }

    public function getForm($id){
        return Form::where("id",$id)->first();
    }

    public function getEbulletins()
    {
        /** @var Website $website */
        $website = session("panel.website");
        $website_id = $website->id;

        return Ebulletin::where("website_id", $website_id)->get();
    }

    public function getEmailTemplates()
    {
        return MailTemplate::get();
    }

    /**
     * @param array $data
     * @param string $emails, $title, $subject, $fromEmail
     * @return void
     */
    public function mailSender($data,$emails,$subject,$fromEmail)
    {
        try{
            Mail::send("HeraldistPanel::mail_service.mpmailler.mail_templates.template", $data, function ($message) use ($emails,$subject,$fromEmail) {
                $message->to($emails)->subject($subject);
                $message->from($fromEmail, $subject);
            });
        }
        catch(\Exception $e){
            dd("İşlem sırasında hata oluştu", $e);
        }
    }

    /**
     * @param array $data
     * @param string $emails, $title, $subject, $fromEmail
     * @param integer $queueTime
     * @return void
     */
    public function mailSenderWithQueue($data,$emails,$title,$subject,$fromEmail,$queueTime=1)
    {
        try{
            dispatch(new SendEmailJob($emails,$data,$subject,$fromEmail, $title))->delay(Carbon::now()->addSeconds($queueTime));
        }
        catch(\Exception $e){
            dd("İşlem sırasında hata oluştu", $e);
        }
    }

}
