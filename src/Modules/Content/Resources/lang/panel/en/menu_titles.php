<?php


return [
    'categories' => 'Categories',
    'criterias' => 'Criteria',
    'menus' => 'Menus',
    'social_media' => 'Social Media',
    'slider' => 'Slider Management',
    'popup' => 'Interaction Management',
    'users' => 'Members / Member List',
    'modules' => 'Modules',

    'structure' => 'Structure',
    'websites' => 'Site Structure',
    'sitemaps' => 'Page Structures',
    'seo' => "Meta Management",
    'image_tags' => "Image Tag Management",
    "meta_templates" => "Meta Templates",
    "robots" =>"Robots.txt",
    "sitemap_xml_editor" => "Sitemap Xml",
    "seo_virtual" => "Virtual Pages",
    "panel_menus" => "Mediapress Menu",
    "languages" => 'Languages',
    'export_pages' => 'Export',
];
