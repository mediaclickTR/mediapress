<?php

return [
    'failed' => 'Your e-mail address or password is incorrect, try again.',
    "sections" => [
        "roles" => "Roles",
        "admins" => "Administrators",
        "abilities" => "Authorizations"
    ],
    "actions" => [
        "manage_components" => "Manage Elements",
        "list" => "List",
        "create" => "Create",
        "update" => "Update",
        "delete" => "Delete",
    ]
];
