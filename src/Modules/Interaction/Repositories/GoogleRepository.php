<?php

namespace Mediapress\Modules\Interaction\Repositories;

use Carbon\Carbon;
use Mediapress\Modules\MPCore\Traits\GoogleAnalyticsTrait;

class GoogleRepository extends BaseRepository implements RepositoryInterface
{

    use GoogleAnalyticsTrait;

    public function alert(): bool
    {
        return true;
    }

    public function stats(): array
    {
        $counts = $this->getCounts($this->time);
        $tabs = $this->getDailyResults($this->time);

        $dates = $cookies = $sessions = $visits = [];
        foreach ($tabs as $key => $value) {
            $dates[] = Carbon::createFromFormat('Ymd', $value['ga:date'])->format('d-m-Y');
            $cookies[] = (int)$value['ga:users'];
            $sessions[] = (int)$value['ga:sessions'];
            $visits[] = (int)$value['ga:pageviews'];

        }
        return
            [
                'counts' => [
                    'cookies' => $counts['ga:users'],
                    'sessions' => $counts['ga:sessions'],
                    'visits' => $counts['ga:pageviews'],
                ],
                'tabs' => [
                    'cookies' => [
                        'categories' => $dates,
                        'values' => $cookies
                    ],
                    'sessions' => [
                        'categories' => $dates,
                        'values' => $sessions
                    ],
                    'visits' => [
                        'categories' => $dates,
                        'values' => $visits
                    ],
                ],
            ];
    }

    public function online(): array
    {
        $online = $this->getOnline();
        $loadTime = $this->getAvgLoadTime($this->time);
        $visitTime = $this->getAvgVisitTime($this->time);
        $bounces = $this->getBounces($this->time);

        return [
            'loadTime' => $loadTime,
            'visitTime' => $visitTime,
            'bounces' => $bounces,
            'online' => $online,
        ];
    }

    public function countries(): array
    {
        return $this->getSessionCountries($this->time);
    }

    public function platforms(): array
    {
        $platformData = $this->getDeviceData($this->time);
        $sourcesData = $this->getTrafficSources($this->time);
        $hourData = $this->getHourly($this->time);
        $platforms = [];
        foreach ($platformData as $value) {
            $platforms[$value['ga:deviceCategory']] = (int)$value['ga:pageviews'];
        }
        $referers = [];
        foreach ($sourcesData as $value) {
            $referers[$value['ga:source']] = (int)$value['ga:sessions'];
        }

        $hours = [];
        foreach ($hourData as $values) {

            $array = [];
            foreach ($values as $value) {
                $array[] = (int)$value;
            }
            $hours[] = $array;
        }

        return [
            'hourly' => $hours,
            'platform' => $this->pieChart($platforms, 'NA'),
            'referers' => $this->pieChart($referers, 'Direct'),

        ];
    }

    public function ages(): array
    {
        $agesData = $this->getAges($this->time);
        $genderData = $this->getGenders($this->time);
        $ages = [];
        foreach ($agesData as $values) {
            $ages[$values['ga:userAgeBracket']] = (int)$values['ga:pageviews'];
        }
        $genders = [];
        foreach ($genderData as $values) {
            $genders[$values['ga:userGender']] = (int)$values['ga:pageviews'];
        }
        return [
            'ages' => $this->pieChart($ages, 'NA'),
            'gender' => $this->pieChart($genders, 'NA'),
            'genders' => $genders
        ];
    }

    public function browsers(): array
    {
        $osData = $this->getOsData($this->time);
        $browserData = $this->getBrowserData($this->time);
        $os = [];
        foreach ($osData as $value) {
            $os[$value['ga:operatingSystem']] = (int)$value['ga:pageviews'];
        }

        $browser = [];
        foreach ($browserData as $value) {
            $browser[$value['ga:browser']] = (int)$value['ga:pageviews'];
        }
        return [
            'os' => $this->pieChart(array_splice($os, 0, 3), 'NA'),
            'browser' => $this->pieChart($browser, 'NA'),
        ];
    }

    public function urls(): array
    {
        $affinities = $this->getAffinity($this->time);
        $mostVisited = $this->getMostVisited($this->time);
        $urls = [];
        foreach ($mostVisited as $value) {
            $urls[$value['ga:pagePath']] = (int)$value['ga:pageviews'];
        }
        $urlData = $this->urlData($urls);
        return [
            'urls' => $urlData,
            'affinities' => $affinities
        ];
    }
}
