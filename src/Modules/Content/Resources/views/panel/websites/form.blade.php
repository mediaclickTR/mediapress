@if(!isset($website))
    {{ Form::open(['route' => 'Content.websites.store']) }}
@else
    {{ Form::model($website, ['route' => ['Content.websites.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$website->id) !!}
@endif
@csrf
<div id="website" class="tab-pane fade in">
    <div class="row">
        <div class="col">
            <div class="form-group col-md">
                <span class="tit">{!! trans("ContentPanel::website.internal_or_external") !!}</span>
                {!!Form::select('target', $targets,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => '', 'id'=>'target'])!!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6" id="type_field">
            <div class="form-group col-md">
                <span class="tit">{!! trans("ContentPanel::website.website_type") !!}</span>
                {!!Form::select('type', $website_types,null, ['class' => '', 'id'=>'type'])!!}
            </div>
        </div>
        <div class="col-6" id="parent_website_field">
            <div class="form-group col-md">
                <span class="tit">{!! trans("ContentPanel::website.parent_website") !!}</span>
                {!!Form::select('website_id', $websites,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => ''])!!}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-6">
            <div class="form-group col-md">
                <span class="tit">{!! trans("ContentPanel::website.name") !!}</span>
                {!!Form::text('name', null, ["placeholder"=>trans("ContentPanel::website.name"), "class"=>"validate[required]"])!!}
            </div>
        </div>
        <div class="col-6">
            <div class="form-group col-md">
                <span class="tit">{!! trans("ContentPanel::website.slug") !!}</span>
                {!!Form::text('slug', null, ["placeholder"=>trans("ContentPanel::website.slug"), "class"=>"validate[required]"])!!}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <span class="tit">{!! trans("ContentPanel::website.use_deflng_code") !!}
                    <a href="#" data-toggle="tooltip"
                       title="{!! trans("ContentPanel::website.userdeflng_code_note") !!}"
                       data-original-title="{!! trans("ContentPanel::website.userdeflng_code_note") !!}">
                    <i class="fa fa-question-circle"></i>
                    </a>
                </span>
                <div class="checkbox">
                    <label>
                        {!! trans("MPCorePanel::general.yes") !!}
                        <input type="radio" @if(isset($website) && $website->use_deflng_code==1) checked
                               @endif name="use_deflng_code" value="1">
                    </label>
                    <label>
                        {!! trans("MPCorePanel::general.no") !!}
                        <input type="radio" @if(isset($website)) @if($website->use_deflng_code==0) checked
                               @endif @endif name="use_deflng_code" value="0">
                    </label>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <span class="tit">{!! trans("ContentPanel::website.ssl") !!}</span>
                <div class="checkbox">
                    <label>
                        {!! trans("MPCorePanel::general.yes") !!}
                        <input type="radio" @if(isset($website) && $website->ssl==1) checked @endif name="ssl"
                               value="1">
                    </label>
                    <label>
                        {!! trans("MPCorePanel::general.no") !!}
                        <input type="radio" @if(isset($website) && $website->ssl==0) checked @endif name="ssl"
                               value="0">
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                 <span class="tit">
                             {!! trans("ContentPanel::website.regions") !!}
                     <a href="#" data-toggle="tooltip"
                        title="{!! trans("ContentPanel::website.regions_note") !!}"
                        data-original-title="{!! trans("ContentPanel::website.regions_note") !!}">
                        <i class="fa fa-question-circle"></i>
                     </a>
                 </span>
                <div class="checkbox">
                    <label>
                        {!! trans("MPCorePanel::general.yes") !!}
                        <input type="radio" @if(isset($website) && $website->regions==1) checked @endif checked
                               name="regions" id="regions" value="1">
                    </label>
                    <label>
                        {!! trans("MPCorePanel::general.no") !!}
                        <input type="radio" @if(isset($website) && $website->regions==0) checked
                               @endif name="regions" id="regions" value="0">
                    </label>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <span class="tit">{!! trans("ContentPanel::website.sort") !!}</span>
                {!!Form::text('sort', null, ["placeholder"=>trans("ContentPanel::website.sort"), "class"=>""])!!}
            </div>
        </div>
    </div>
</div>
<div id="language" class="tab-pane fade in active">
    @if($website->languages->isEmpty())
        <div class="row">
            <div class="col-8">
                <div class="form-group rel ap">
                    <span class="tit">{!! trans("ContentPanel::website.languages") !!}</span>
                    {!!Form::select('languages[]', $languages,$website->languages, ['class' => 'baseLanguages languages','multiple', 'id'=>'pre-selected-options'])!!}
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <span class="tit">{!! trans("ContentPanel::website.default_language") !!}</span><br/>
                    @if(isset($website) &&  $website->defaultLanguage())
                        {!!Form::text('',  $website->defaultLanguage()->name, ["placeholder"=>trans("ContentPanel::website.name"), "class"=>"validate[required]", "disabled"])!!}
                        {!!Form::hidden('default_language',  $website->defaultLanguage()->id)!!}
                    @else
                        {!!Form::select('default_language',[],null, ['placeholder'=>trans("MPCorePanel::general.selection"),'class' => 'default_language'])!!}
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="clearfix"></div>

    <div id="country_field">

        <div class="variation-title float-left">
            <div class="title">{!! trans("ContentPanel::website.country_group_language_variations") !!}</div>
        </div>

        <div class="float-right" id="addNewCG">
            <span class="btn btn-success"
                  onclick="javascript:cg_variation_create({{ $website->id }},0,$(this));">{!! trans("MPCorePanel::general.new_add") !!}</span>
        </div>

        <div class="clearfix"></div>

        <table class="table table-advance table-sm variations-table">
            <thead>
            <tr>
                <th style="width: 15%">Liste Adı</th>
                <th style="width: 20%">{!! trans("ContentPanel::website.country_group") !!}</th>
                <th>{!! trans("ContentPanel::website.language") !!}</th>
                <th>{!! trans("MPCorePanel::general.actions") !!}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <div class="clearfix"></div>
    </div>

</div>
<div id="details" class="tab-pane fade in">
    <p></p>
</div>
<div class="float-right">
    <button id="submit" class="btn btn-primary" type="submit">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

<div class="clearfix"></div>
{{ Form::close() }}

@push('scripts')
    <script>

        function detail_tabs() {
            $("#details").load('{{ route('Content.websites.detail_tabs_markup', isset($website) ? $website->id : 0) }}');
        }

        function cg_variation_create(website_id, id) {
            popup('cgCreateOrUpdate', {website_id, id});
        }

        function cg_variation_edit(website_id, id) {
            popup('cgCreateOrUpdate', {website_id, id});
        }

        function cg_variation_delete(website_id, id, element) {
            swal({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ trans("MPCorePanel::general.yes") }}',
                cancelButtonText: '{{ trans("MPCorePanel::general.cancel") }}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    variation_delete(website_id, id, element);
                }
            });

        }

        function variation_delete(website_id, id, element) {
            var token = $("meta[name='csrf-token']").attr("content");
            ;
            $.ajax({
                url: "{{ route('Content.websites.country_groups.remove', isset($website) ? $website->id : 0) }}",
                method: 'POST',
                data: {id: id, _token: token},
                success: function (data) {
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    });
                    countryGroupsEvent();
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        }

        function refreshVariationsPresentation() {

            var callback = function (response) {
                var table = $(".variations-table");
                $("tbody > tr", table).remove();
                var websiteid = "{!! $website->id !!}";
                var tr = "", lngtext = "";

                if (response[websiteid] != undefined) {

                    response = response[websiteid];

                    var cg, languages, cgtext, countryGroupLength;
                    if (typeof response.country_groups !== "undefined") {
                        countryGroupLength = Object.keys(response.country_groups).length;
                    } else {
                        countryGroupLength = 0;
                    }

                    if (countryGroupLength > 0) {
                        for (cgkey in response.country_groups) {
                            cg = response.country_groups[cgkey];
                            cgListText = cg.list_title;
                            cgtext = cg.title;
                            cgid = cg.id;
                            languages = cg.languages;

                            var countrGroupLangs = '';
                            for (key in languages) {
                                countrGroupLangs += languages[key].code + ', ';
                            }
                            countrGroupLangs = countrGroupLangs.replace(/,\s*$/, "");

                            tr = $("<tr></tr>");
                            lngtext = languages[key].name;
                            tr.attr("data-var-type", "cg-language").attr("data-lang-lenght", countrGroupLangs).attr("data-lang-id", key);
                            tr.append('<td>' + cgListText + '</td>')
                                .append('<td>' + cgtext + '</td>')
                                .append('<td>' + countrGroupLangs + '</td>')
                                .append('<td><a onclick="javascript:cg_variation_edit({{ $website->id }},' + cgid + ', $(this));" style="margin-right:5px">{!! trans("MPCorePanel::general.edit") !!}</a>' + (cg.code != 'gl' ? '<a onclick="javascript:cg_variation_delete({{ $website->id }},' + cgid + ', $(this));">{!! trans("MPCorePanel::general.delete") !!}</a>' : '') + '</td>');
                            $("tbody", table).append(tr);
                        }
                    } else {
                        tr = $("<tr></tr>");
                        tr.append('<td colspan="3">{!! trans("ContentPanel::website.doesnotexits.countrygroup") !!}</td>').append('<td></td>');
                        $("tbody", table).append(tr);
                    }
                }
            }

            getCGData(callback);
        }

        function getCGData(on_success) {

            var route = '{!! url(route("Content.websites.variation_templates",$website->id)) !!}';

            var csrf = $("meta[name='csrf-token']").attr('content');
            var data_ = {
                "token_": csrf
            };
            $.ajax({
                type: "GET",
                url: route,
                data: data_,
                dataType: "json",
                beforeSend: function () {
                },
                success: on_success,
                error: function (jqXHR, textStatus, errorThrown) {
                    new swal("Veri Alınamıyor", textStatus.toUpperCase() + ": " + errorThrown, 'error');
                },
                complete: function () {

                }
            });

            detail_tabs();
        }

        function countryGroupsEvent(event_name, event_data) {
            refreshVariationsPresentation();
        }

        $(document).ready(function () {

            refreshVariationsPresentation();
            detail_tabs();

            // Languages and Default Language Select Queries || Start
            var langs = JSON.parse('{!! json_encode($lang_codes) !!}');

            $("#tab-language").click(function () {
                var regions = $('#regions:checked').val();

                if (regions == null) {
                    alert({!! trans("ContentPanel::website.variation_select_info") !!});
                    return false;
                } else if (regions == 0) {
                    $("#addNewCG").hide();
                } else {
                    $("#addNewCG").show();
                }

                $(".country_group").not(":eq(0)").prop('required', true);
                $(".countryGroupLanguages").not(":eq(0)").prop('required', true);
            });

            var selected = [];

            var tempOption = `<option value="{value}">{name}</option>`;


            $(document).delegate("select.languages", "change", function (e) {

                $('[selected]', e.currentTarget).each(function (el, item) {
                    var code = langs[item.value];
                    var lang = item.innerText;
                    var id = item.value;
                    var item = $("div.languages > .lang_" + code);

                    if (item.length == 0) {

                        var tempOp = tempOption.replace(/{value}/gm, id).replace(/{name}/gm, lang);

                        if ($.inArray(id, selected) == -1) {

                            $(".default_language").append(tempOp);

                            $(".country_group").append(tempOp);
                            selected.push(id);

                        }
                    }
                    if (selected.length > 0 && regions == 1) {
                        $('#country_field').show();
                    }
                });

            });

            $("select.languages").trigger('change');

            $(document).delegate(".selectator_selected_item_remove", "mouseup", function (e) {
                // get id
                var id = e.currentTarget.parentNode.className.match(/value_(\d+)/i)[1];
                // Remove option
                $(".default_language option[value='" + id + "']").remove();
                $(".country_group option[value='" + id + "']").remove();
                $(".country_group option[value='" + id + "']").remove();
                // Array clear
                selected = $.grep(selected, function (value) {
                    return value != id;
                });
            });
            // Languages and Default Language Select Queries || End

            $("#type").change(function () {
                if ($(this).val() == "domain") {
                    $("#parent_website_field").hide();
                    if ($("#type_field").hasClass("col-6")) {
                        $("#type_field").toggleClass('col-6 col-12');
                    }
                } else {
                    $("#parent_website_field").show();
                    if ($("#type_field").hasClass("col-12")) {
                        $("#type_field").toggleClass('col-12 col-6');
                    }
                }
            });

            $("#type").trigger('change');

        });
    </script>
@endpush


@push("styles")
    <style>
        tr, th {
            text-align: center;
        }

        .modal-dialog {
            max-width: 60% !important;
        }

        select {
            width: 100% !important;
        }

        #country_field {
            padding: 10px !important;
            margin-bottom: 10px;
        }
    </style>
@endpush
