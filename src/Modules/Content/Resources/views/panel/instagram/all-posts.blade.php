@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }

        .post-photo{
            width: 200px;
            height: auto;
        }

        .post{
            margin-top: 20px;
        }

        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title" style="">
            <div class="float-left">
                Tüm Medyalar
            </div>
{{--            <div class="float-right" >--}}
{{--                <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>--}}
{{--            </div>--}}

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.allposts') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Tüm Medyalar</a>
            </div>
        </div>
        <div class="table-field">
            <div style="margin-top: 70px !important; border-top: 1px solid #111;">
                <div class="container" style="margin-top: 20px !important;">
                    <div class="row">
                        <div class="col-12">
                            <div id="posts" class="row animated-fadein"  >
                                @php($postCounter = 0)
                                @foreach($datas as $data)
                                    @php($post = json_decode($data->post_datas))
                                    <div class="col-md-6 post" style="display:flex">
                                        <div class="col-md-6 align-self-center">
                                            @if($post->type == "carousel")
                                                <div id="carouselExampleControls{!! $postCounter !!}" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        @foreach($post->carousel_media as $child)
                                                            <div class="carousel-item {!! $loop->iteration == 1 ? "active": "" !!}">
                                                                <img src="{!! $child->media_url !!}" class="d-block w-100" alt="...">
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            @elseif($post->type == "video")
                                                <video width="250" controls>
                                                    <source src="{!! $post->media_url !!}" type="video/mp4">
                                                    Lütfen "Chrome" Tarayıcı İle Deneyiniz
                                                </video>
                                            @else
                                                <div class="col-md-6 align-self-center">
                                                    <img class="post-photo" src="{!! $post->thumbnail_photo !!}" alt="">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 align-self-center text-center">

                                            <p> <i class="far fa-heart"></i> {!! $post->likes !!} Beğenme </p>
                                            <p> <i class="far fa-comments"></i> {!! count($post->comments) !!} Yorum </p>
                                            <p>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-4">
                                                        <a data-toggle="tooltip" data-placement="top" title="Detaylı Gör">
                                                            <button id="dLabel" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" type="button">
                                                                <i class="fas fa-eye"></i>
                                                            </button>
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button id="dLabel" class="btn btn-outline-danger " type="button" data-toggle="tooltip" data-placement="top" title="Sil">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <a href="{!! $post->post_url !!}" target="_blank">
                                                            <button id="dLabel" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Gönderiye Git">
                                                                <i class="fas fa-share"></i>
                                                            </button>
                                                        </a>

                                                    </div>
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                    @php($postCounter ++)
                                @endforeach
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   Bu Gönderiyi Silmek İstiyor Musunuz?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal</button>
                    <button type="button" class="btn btn-primary">Evet</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        var posts = "";
        $("#save").click(function () {
            $( "input[type=checkbox]:checked" ).each(function () {
                var val = $(this).data("id");
                posts = posts +","+ val;
            });

            $.post( "{!! route("Content.instagram.saveAlbum") !!}", {
                "posts": posts,
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {

            });
            console.log(posts);
        });

        $(".refresh").click(function (e) {
            e.preventDefault();
            var option = $(this).data("option");
            if (option == "refresh"){
                $("#schedule").show();
                $("#posts").hide();
            }
            else if(option == "posts"){
                $("#posts").show();
                $("#schedule").hide();
            }
        });




    </script>
@endpush
