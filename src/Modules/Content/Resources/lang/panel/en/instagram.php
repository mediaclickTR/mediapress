<?php

return [
    'previous' => 'Previous',
    'next' => 'Next',
    'please_try_with_chrome' => 'Please, try with Chrome Browser',
    'likes' => 'Likes',
    'comment' => 'Comment',
    'goto_post' => 'Goto Post',
    'add_to_album' => 'Add to Album'
];
