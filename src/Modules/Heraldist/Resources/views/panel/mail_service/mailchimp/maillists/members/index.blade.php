@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
            <div class="float-left">
                <h1 class="title">"{!! $maillist->name !!}" {!! trans('HeraldistPanel::mail_service.lists_user') !!} ({!! $total !!})</h1>
                <a href="{!! route('Heraldist.mailchimp.index') !!}"> <i class="fa fa-arrow-circle-left"></i> {!! trans('HeraldistPanel::mail_service.back_maillist') !!}</a>
            </div>
            @if(count($members)<=0)
                <div class="alert alert-warning">{!! trans('MPCorePanel::general.nodata') !!}</div>
            @else
            <table>
                <thead>
                <tr>
                    <th>{!! trans('MPCorePanel::general.id') !!}</th>
                    <th>{!! trans('MPCorePanel::general.email') !!}</th>
                </tr>
                </thead>
                @foreach($members as $member)
                <tr>
                    <td>{!! $member->id !!}</td>
                    <td>{!! $member->email_address !!}</td>
                </tr>
                @endforeach
            </table>
        </div>
        @endif
@endsection
