@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.emailsender") !!}</h1>
        <div class="col-md-12 pa">
            <div class="rootwizard">
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <ul>
                                <li><a href="#tab1" data-toggle="tab">1. {!! trans("HeraldistPanel::mail_service.select_maillist") !!}</a></li>
                                <li><a href="#tab2" data-toggle="tab">2. {!! trans("HeraldistPanel::mail_service.select_template") !!}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="bar" class="progress progress-striped active">
                    <div class="progress-bar"></div>
                </div>
                <div class="tab-content">
                    <form action="{!! route('Heraldist.mpmailler.email_senders.store') !!}" novalidate method="POST">
                        @include("HeraldistPanel::mail_service.mpmailler.email_sender.form")
                    </form>
                </div>
            </div>
        </div>
@endsection
