<?php

namespace Mediapress\Facades;

use Illuminate\Support\Facades\Facade;

class ModulesEngine extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Mediapress\Foundation\ModulesEngine::class;
    }
}