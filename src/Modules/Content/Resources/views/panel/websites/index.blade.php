@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
       <div class="topPage">
           @include("MPCorePanel::inc.errors")
           <div class="float-left">
               <div class="title m-0">{!! trans("ContentPanel::website.websites") !!}</div>
           </div>
           <div class="float-right">
               @if(count($websites)>0)
                       @if(!empty($queries))
                           <a href="{!! route("Content.websites.index") !!}" class="filter-block">
                               <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                           </a>
                       @endif
               @endif
               <a href="{!! route('Content.websites.create') !!}" class="btn btn-primary btn-sm"> <i
                       class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
           </div>
       </div>
       <div class="p-30">
           <table>
               <thead>
               <tr>
                   <th>{!! trans("MPCorePanel::general.status") !!}</th>
                   <th>{!! trans("ContentPanel::website.internal_external") !!}</th>
                   <th>{!! trans("ContentPanel::website.name") !!}</th>
                   <th>{!! trans("ContentPanel::website.slug") !!}</th>
                   <th>{!! trans("MPCorePanel::general.actions") !!}</th>
               </tr>
               </thead>
               @foreach($websites as $key=>$website)
                   <tr>
                       <td>{!! ($website->status==1) ? '<i class="aktif"></i>' : '<i class="sifreli"></i>' !!}</td>
                       <td>{!! ($website->target == "internal") ? trans("ContentPanel::website.internal") : trans("ContentPanel::website.external") !!}</td>
                       <td>{!! $website->name !!} </td>
                       <td>{!! $website->slug !!} </td>
                       <td>
                           <a href="{!! route('Content.websites.edit',$website->id ) !!}" class="m-1" title="{{trans("MPCorePanel::general.edit")}}">
                               <span class="fa fa-pen"></span>
                           </a>
                           <a href="{!! route('Content.websites.delete',$website->id ) !!}"
                              class="m-1 needs-confirmation"
                              title="  {{trans("MPCorePanel::general.delete")}}"
                              data-dialog-text="{{trans("MPCorePanel::general.action_cannot_undone")}}"
                              data-dialog-cancellable="1"
                              data-dialog-type="warning"><span
                                   class="fa fa-trash"></span></a>
                       </td>
                   </tr>
               @endforeach
           </table>

           <div class="float-left">
               {!! $websites->links() !!}
           </div>
           <div class="float-right">
               <select onchange="locationOnChange(this.value);" class="fs-13">
                   <option value="#">{!! trans('MPCorePanel::general.select.list.size') !!}</option>
                   <option
                       {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("Content.websites.index", array_merge($queries, ["list_size"=>"15"])) !!}">
                       15
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("Content.websites.index", array_merge($queries, ["list_size"=>"25"])) !!}">
                       25
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("Content.websites.index", array_merge($queries, ["list_size"=>"50"])) !!}">
                       50
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("Content.websites.index", array_merge($queries, ["list_size"=>"100"])) !!}">
                       100
                   </option>
               </select>
           </div>
       </div>
    </div>
@endsection
@push("styles")
    <style>
        td,th{
            text-align:center;
        }
    </style>
@endpush
@push("scripts")
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
    </script>
@endpush
