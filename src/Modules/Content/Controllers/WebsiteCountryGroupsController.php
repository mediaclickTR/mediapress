<?php

    namespace Mediapress\Modules\Content\Controllers;

    use Mediapress\Http\Controllers\PanelController as Controller;
    use Illuminate\Http\Request;
    use Mediapress\Modules\Content\Facades\Content;
    use Mediapress\Modules\Content\Models\LanguageWebsite;
    use Mediapress\Modules\Content\Models\Website;
    use Mediapress\Modules\MPCore\Facades\CountryGroups as CGF;
    use Mediapress\Modules\MPCore\Models\CountryGroup;
    use Illuminate\Support\Facades\DB;
    use Mediapress\Modules\MPCore\Models\CountryGroupLanguage;
    use View;

    class WebsiteCountryGroupsController extends Controller
    {

        const STR_GROUP_ID = "group_id";
        const STR_COUNTRY_GROUP_ID="country_group_id";
        public const STATUS = "status";
        public const FAIL_MESSAGE = "fail_message";

        /**
         * Main function
         * @param Request $request
         * @return mixed
         */
        public function manage($id,Request $request)
        {

            //return dd(CGF::getUngroupedCountriesOfOwner(["Mediapress\\Modules\\Content\\Models\\Website",2]));

            //http://pilot.local/mp-admin/MPCore/CountryGroups/manage?changesOwner=no&selectedOwnerId=1&selectedOwnerType=Mediapress%5CModules%5CContent%5CWebsite
            $params = $request->all();
            $params["changesOwner"]="no";
            $params["selectedOwnerType"]="Mediapress\\Modules\\Content\\Models\\Website";
            $params["selectedOwnerId"]=$id;
            $params["selectionPool"]="nonintersection";

            $renderable = "Mediapress\\Modules\\Content\\AllBuilder\\Renderables\\ManageCountryGroups";
            $renderable_object = new $renderable(["request_params" => $params]);

            return view("MPCorePanel::country_groups.manage", compact("renderable_object"));
        }





        /**
         *  Create / Update CountryGroup and sync countries
         * @param Request $request
         * @return string
         */
        public function saveCountryGroup(Request $request)
        {
            // A numeric value for an existing group or "new" keyword for the one which is gonna be created
            $cg_id = $request->cg_id;
            // Possible new | renewal group name
            $title = $request->title;
            $code = $request->code;
            // Country codes array like ["TR","GB",...]
            // CGF::setCountriesOfGroup() requires the countries param to be an array only
            $countries = $request->countries ?? [];


            $languages = $request->languages ?? [];

            // Detect | Prepare CountryGroup model at first
            $cg = $cg_id == "new" ? (new CountryGroup()) : CountryGroup::find($cg_id);
            // Owner of country group
            $cg->owner_type = $request->cg_owner_type;
            $cg->owner_id = $website_id = $request->cg_owner_id;

            // Assign new name
            $cg->title = $request->title;


            $cg->list_title = $request->list_title;

            // Assign code
            $cg->code = $request->code;
            // Default return data
            $result = [
                self::STATUS => "fail",
                "model" => null
            ];
            DB::beginTransaction();

            if ($cg->save()) {

                $defaulLang = $request->default_language;

                $tempLanguage = [];
                foreach ($languages as $language) {
                    $tempLanguage[] = [
                        'language_id' => $language,
                        'default' => $language == $defaulLang ? 1 : 0
                    ];
                }


                CGF::setCountriesOfGroup($cg, $countries);
                $save_languages = $cg->languages()->sync($tempLanguage);
                if (!$save_languages){
                    DB::rollback();
                    return json_encode($result);
                }
                $equate = Content::equateWebsiteDetailsAndVariations($website_id);
                DB::commit();
                $result[self::STATUS] = "success";
                $result["model"] = $cg->toArray();

            }else{
                DB::rollback();
                return json_encode($result);
            }

            return json_encode($result);

        }

        public function removeCountryGroup(Request $request){

            // A numeric value for an existing group which is gonna be deleted
            $cg_id = $request->id;

            $cg = CountryGroup::find($cg_id);
            $website_id = $cg->owner_id;

            $result = [
                self::STATUS => "fail",
                self::FAIL_MESSAGE =>""
            ];

            if( null == $cg ){
                $result[self::FAIL_MESSAGE]="Grup kayıtlı değil.";
            }elseif ($cg->countries()->sync([]) && $cg->languages()->sync([]) && $cg->delete()){
                $equate = Content::equateWebsiteDetailsAndVariations($website_id);
                $result=[self::STATUS =>"success"];
            }else{
                $result[self::FAIL_MESSAGE]="Grup bulundu ancak silinemedi.";
            }

            return response()->json($result);

        }


        /**
         * Return country codes array of CountryGroup like ["TR","GB",...]
         * @param Request $request
         * @return string
         */
        public function getGroupsCountriesAndLanguages(Request $request)
        {

            // Capture request
            $params = $request->all();
            // Default value to return
            $result = json_encode([]);

            // Stop if required parameters not sent
            if (!isset($params[self::STR_GROUP_ID]) || !$params[self::STR_GROUP_ID]) {
                return $result;
            }

            // Reassign param values (not to use array values directly)
            $group_id = $params[self::STR_GROUP_ID];

            // CGF::getCountriesOfGroup() may return false so "if" it first
            if ($result_ = CGF::getCountriesOfGroup($group_id)) {
                $result_ = $result_->pluck("code");
                $languages = LanguageWebsite::where(self::STR_COUNTRY_GROUP_ID,$group_id)->get()->pluck("language_id");
                $result=json_encode([$result_,$languages]);
            }

            return $result;
        }

        public function getCountryGroupsLanguages($id){
            $results = [];
            $groups =CGF::getGroupsOf([Website::class,$id]);
            $group_ids = $groups->pluck("id")->toArray();

            return response()->json($group_languages = LanguageWebsite::whereIn(self::STR_COUNTRY_GROUP_ID,$group_ids)->get()->groupBy(self::STR_COUNTRY_GROUP_ID)->toArray());


        }
    }
