@extends('RenderableEditorPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("RenderableEditorPanel::renderable.list") !!}</div>
            </div>
            <div class="float-right">
                {{--<a class="btn btn-primary" href="{!! route('Entity.entities.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>--}}
            </div>
        </div>

        <div class="p-30">
            <table class="table table-sm table-striped table-advance">
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($files as $file)
                    <tr>
                        <td>{!! $file->getRelativePathName() !!}</td>
                        <td>{!! date("d/m/Y H:i",$file->getCTime()) !!}</td>
                        <td>
                            <a href="{!! route('admin.editors.edit',encrypt($file->getPathName()) ) !!}" class="m-1" title="{{trans("MPCorePanel::general.edit")}}">
                                <span class="fa fa-pen"></span>
                            </a>
                            <a href="{!! route('admin.editors.delete',encrypt($file->getPathName()) ) !!}"
                               class="m-1 needs-confirmation"
                               title="  {{trans("MPCorePanel::general.delete")}}"
                               data-dialog-text="{{trans("MPCorePanel::general.action_cannot_undone")}}"
                               data-dialog-cancellable="1"
                               data-dialog-type="warning"><span
                                    class="fa fa-trash"></span></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
@endsection
