@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entitylistdetail.userlist-add") !!}</div>
        <form action="{!! route('Entity.entitylist.listdetail.store',$list_id) !!}" novalidate method="POST">
            @include("EntityPanel::entitylist.listdetail.form")
        </form>
    </div>
@endsection
