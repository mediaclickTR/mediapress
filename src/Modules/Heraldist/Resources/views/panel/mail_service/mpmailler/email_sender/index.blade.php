@extends("HeraldistPanel::inc.module_main")
@section('content')
    <div class="page-content">
        <div class="title">
            {!! trans("HeraldistPanel::mail_service.emailsender") !!}
        </div>
        <div class="row">
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler/EmailSender/create') !!}"><button class="btn btn-primary">{!! trans("HeraldistPanel::mail_service.emailsender") !!}</button></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="submit">
                        <a href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler/EmailSender/sendeds') !!}"><button class="btn btn-primary"> {!! trans("HeraldistPanel::mail_service.sendedemails") !!}</button></a>
                    </div>
                </div>
            </div>
        </div>
@endsection
