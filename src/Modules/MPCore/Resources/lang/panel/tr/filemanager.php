<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 27.02.2019
     * Time: 09:51
     */

    return [
        "param_infos" => [
            "filesize_max" => "Maksimum dosya boyutu :q KB",
            "extensions" => "İzin verilen dosya uzantıları :q",
            "files_max" => "Maksimum :q tane dosya yüklenebilir."
        ],
        "default_file_title"=>"Dosya",
        "params_not_present"=>"Değiştirgeler gönderilmedi.",
        "no_mdisk_found"=>"Disk yok.",


        "select_btn" => "Seç",
        "selected_files" => "Seçilenler",
        "add_new" => "Yeni Yükle",
        "media_library" => "Medya Kütüphanesi",
        "upload_url" => "Url'den Yükle",
        "user_log" => "Kullanıcı Logları",

        "model_type" => "Sayfa :",

        "file_type" => [
            "title" => "Dosya Tipi :",
            "all" => "Tümü",
            "image" => "Görsel",
            "file" => "Dosya",
            "video" => "Video",
            "voice" => "Ses",
        ],

        "order_type" => [
            "title" => "Sırala :",
            "date_1" => "Tarihe Göre (Büyükten Küçüğe)",
            "date_2" => "Tarihe Göre (Küçükten Büyüğe)",
            "a_z" => "Alfabetik (A -> Z)",
            "z_a" => "Alfabetik (Z -> A)",
            "size_1" => "Boyuta Göre (Büyükten Küçüğe)",
            "size_2" => "Boyuta Göre (Küçükten Büyüğe)",
        ],

        "date_type" => [
            "title" => "Tarih :",
            "all" => "Tümü",
        ],

        "dropzone" => [
            "title" => "Yüklemek için dosyaları bu alana sürükleyiniz",
            "title_2" => "— veya —",
            "from_computer" => "Bilgisayarınızdan dosyaları seçiniz",
        ],

        "own_files" => "Kendi Yüklediklerim",
        "file_search" => "Ara :",
        "file_url_address" => "Dosya Url Adresi :",
        "file_url_download" => "Yükle",

        "log" => [
            "file" => 'Dosya',
            "message" => 'Mesaj',
            "date" => 'Tarih',

            "message_detail" => '":website" sitesinde ":user_name" adlı kullanıcı, ":image_name" isimli dosyayı ekledi.',
        ]
    ];

