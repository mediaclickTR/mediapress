<?php

return [
    'title' => 'Panel Language Variables',
    'save_info' => 'Attention recording is done automatically. Just fill in the relevant fields.',
    'search' => 'Search...',
    'variable' => 'Variable',
    'default_lang_value' => 'Default Language Value',
    'detail' => "Detail",
    'view' => 'View',
    'languages' => 'Languages',
];
