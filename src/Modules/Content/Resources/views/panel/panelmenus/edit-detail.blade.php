@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::panelmenu.edit_menu") !!}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.panelmenus.details', ['id' => $panel_menu->menu_id]) }}" class="btn btn-light mr-3">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
       <div class="row">
           <div class="col-md-12">
               {{ Form::model($panel_menu, ['route' => ['Content.panelmenus.detailsEditPost',$panel_menu->id], 'method' => 'POST']) }}
               @csrf
               <input type="hidden" value="{!! $panel_menu->menu_id !!}" name="menu_id">
               <div class="form-group rel">
                   <div class="tit">{!! trans("ContentPanel::panelmenu.select_sitemap") !!}</div>
                   {!!Form::select('sitemap_id', $sitemaps,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
               </div>
               <div class="float-right">
                   <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
               </div>
               {{ Form::close() }}
           </div>
       </div>
</div>
@endsection
