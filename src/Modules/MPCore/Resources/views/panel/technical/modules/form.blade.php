@if(!isset($module))
    {{ Form::open(['route' => 'MPCore.technical.modules.store']) }}
@else
    {{ Form::model($module, ['route' => ['MPCore.technical.modules.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$module->id) !!}
@endif
@csrf

<div class="form-group  col-md">
    <div class="tit">{!! trans("MPCorePanel::module.slot") !!}</div>
    {!!Form::select('slot_id',$slots,null, ['placeholder' => trans("MPCorePanel::general.selection"),"required"=>"required"])!!}
</div>

<div class="form-group rel rel">
    <div class="tit">{!! trans("MPCorePanel::module.name") !!}</div>
    {!!Form::text('name', null, ["placeholder"=>trans("MPCorePanel::module.name"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel rel">
    <div class="tit">{!! trans("MPCorePanel::module.namespace") !!}</div>
    {!!Form::text('namespace', null, ["placeholder"=>trans("MPCorePanel::module.namespace"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel ap">
    <div class="tit">{!! trans("MPCorePanel::module.status") !!}</div>
    <div class="checkbox">
        <label>
            {!! trans("MPCorePanel::general.yes") !!}
            <input type="radio" @if(isset($module) && $module->use_deflng_code==1) checked @endif name="status" value="1">
        </label>
        <label>
            {!! trans("MPCorePanel::general.no") !!}
            <input type="radio" @if(isset($module)) @if($module->use_deflng_code==0) checked @endif @endif name="status" value="0">
        </label>
    </div>
</div>

<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

{{ Form::close() }}

@push("styles")
    <style>
        select {
            width: 100% !important;
        }
        tr td{
            text-align:center;
        }
    </style>
@endpush