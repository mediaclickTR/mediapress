@if($slider->screen == 3)
    <div class="container-fluid">
        <div class="row">
@endif

            <div class="slider">
                <div class="fullwidthbanner-container">
                    <div class="fullwidthbanner">
                        <ul>
                            @foreach($slides as $slide)
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-delay="{!! $slide->time*1000 !!}">
                                    <img src="{{ $slide->image() }}" data-bgfit="cover" data-bgposition="center center"
                                         data-bgrepeat="no-repeat" alt="">
                                    <div class="tp-caption tp-captions customin customout " data-x="300" data-y="200"
                                         data-speed="600" data-start="400" data-voffset="0" data-hoffset="0"
                                         data-easing="Fade"
                                         data-endspeed="500" data-endeasing="Fade"
                                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;">
                                        <div class="container-fluid">
                                            <div class="capBig">
                                                <div class="line">
                                                    @for($i=1;$i<=3;$i++)
                                                        {!! $slide->text($i) !!}
                                                    @endfor
                                                    @for($i=1;$i<=3;$i++)
                                                        {!! $slide->button($i) !!}
                                                    @endfor

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
@if($slider->screen == 3)
        </div>
    </div>
@endif
@push('scripts')
    <script>
        @if($slider->screen == 1)
        $(".slider .fullwidthbanner").revolution({
            startwidth: 980,
            autoplay: "true",
            navigationArrows: "block",
            startheight: 920,
            fullWidth: "on",
            fullScreen: "on",
            onHoverStop: "off"
        });
        @elseif($slider->screen == 2)
        $(".slider .fullwidthbanner").revolution({
            startwidth: 980,
            autoplay: "true",
            navigationArrows: "block",
            startheight: "{!! $slider->height !!}",
            fullWidth: "on",
            fullScreen: "off",
            onHoverStop: "off"
        });

        @else
        $(".slider .fullwidthbanner").revolution({
            startwidth: "{!! $slider->width !!}",
            startheight: "{!! $slider->height !!}",
            autoplay: "true",
            navigationArrows: "block",
            onHoverStop: "off"
        });

        @endif
    </script>
@endpush