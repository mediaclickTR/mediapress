@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! $main_menu->name !!}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.panelmenus.index') }}" class="btn btn-light mr-3">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="cat-list">
                        @if($menu!="[]")
                            <div class="dd" id="nestable">
                                <div class="panel panel-warning">
                                    <div class="dd span11" id="nestable-menu">
                                        <ol class="dd-list">
                                        </ol>
                                    </div>
                                    {{ Form::open(['route' => 'Content.panelmenus.updateList']) }}
                                    {!! Form::hidden('menu_json',$menu) !!}
                                    <div class="float-right">
                                        <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @else
                            <div class="alert alert-warning">{!! trans("ContentPanel::panelmenu.empty_menu_detail", ['menu'=>$main_menu->name]) !!}</div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="tab-list">
                        <div class="title mt-5">
                            {!! trans("MPCorePanel::general.new_add") !!}
                        </div>
                        <div class="form">
                            {{ Form::open(['route' => 'Content.panelmenus.detailsPost']) }}
                            @csrf
                            {!!Form::hidden('menu_id', $id)!!}
                            <div class="form-group">
                                <div class="tit">{!! trans("ContentPanel::panelmenu.select_sitemap") !!}</div>
                                {!!Form::select('sitemap_id', $sitemaps,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => ''])!!}
                            </div>
                            <div class="submit">
                                <button class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</button>
                            </div>
                            {{ Form::close() }}
                            <br/>
                            <div class="note">
                                <small>{!! trans("ContentPanel::panelmenu.info") !!}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.dd-list').delegate('.delete', 'click', function ()
        {
            var id = $(this).find('i').data('id');
            var detail = $(this).parents('.dd-list');
            swal({
                title : "Silecek misiniz?",
                text : "Sildiğiniz bilgiler kalıcı olarak sistemden kaldırılacaktır.",
                type : "info",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "Sil gitsin",
                cancelButtonText : "Hayır vazgeçtim",
                closeOnConfirm : false,
                closeOnCancel : false
            }).then(function (dismiss)
            {
                if (dismiss !== 'cancel')
                {
                    window.location = '{{ url('mp-admin/Content/PanelMenus/detailsDelete') }}/' + id + '';
                }
            });
        });
        $('.dd-list').delegate('.edit', 'click', function ()
        {
            var id = $(this).find('i').data('id');
            swal({
                title : "Dikkat",
                text : "Taslağınızı kaydettiniz mi ?",
                type : "info",
                showCancelButton : true,
                confirmButtonColor : "#DD6B55",
                confirmButtonText : "Evet, kaydettim",
                cancelButtonText : "Hayır, kaydedeceğim",
                closeOnConfirm : false,
                closeOnCancel : false
            }).then(function (dismiss)
            {
                if (dismiss !== 'cancel')
                {
                    window.location = '{{ route('Content.panelmenus.index') }}/' + id + '/edit-detail'
                }
            });
        });
        $(function ()
        {
            $("[name='MenuDetails___menu_id']").val({{ $id }});
            var obj = '{!! $menu !!}';
            var output = '';
            $.each(JSON.parse(obj), function (index, item)
            {
                output += buildItem(item);
            });
            $('#nestable-menu').nestable({'maxDepth': 1}).on('change', function ()
            {
                $('[name=menu_json]').val(JSON.stringify($('#nestable-menu').nestable('serialize')))
            });
            $(".dd-list").append(output);
            $("#nestable-output").val(obj);
        });
        function buildItem(item)
        {
            var html = "<li class='dd-item' data-id='" + item.id + "' >";
            html += "<span class='delete ' style='padding-left:15px !important;'><i class='fa fa-trash' data-id='" + item.id + "'></i></span>"
                + "<span class='edit'><i class='fa fa-edit' data-id='" + item.id + "'></i></span>"
                + "<div class='dd-handle'>" + item.name + "</div>";

            if (item.children)
            {
                html += "<ol class='dd-list'>";
                $.each(item.children, function (index, sub)
                {
                    html += buildItem(sub);
                });
                html += "</ol>";
            }
            html += "</li>";
            return html;
        }
    </script>
@endpush

@push('styles')
    <style>
        .dd-item span{
            color:#333 !important;
        }
        .page .page-content .content .cat-list ol li span{
            background: none;
        }
    </style>
@endpush
