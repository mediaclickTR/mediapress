<?php

return [
    'inbox'=>'Gelen Mesajlar',
    'last.website.updates'=>'Son Site Güncellemeleri',
    'empty.inbox'=> 'Gelen kutunuz boş.',
    'empty.user.actions'=> 'Aktif site güncellemesi bulunamadı.',
];