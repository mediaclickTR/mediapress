<?php

namespace Mediapress\Modules\Content\Foundation;

use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\BackupData;
use Mediapress\Foundation\ClusterArray;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;

class CriteriaEngine
{


    public const CRITERIAS = "Criterias";
    public const CONTENT = "Content";
    public const APP = "App";
    public const MEDIAPRESS = "Mediapress";
    public const SITEMAP = "Sitemap_";
    public const SITEMAPS = "Sitemaps";
    public const DETAIL = 'detail';

    public function ajaxCreate(Sitemap $sitemap = null, Criteria $parent = null, Criteria $edit = null)
    {
        if ($edit == null) {
            $preCreate = Criteria::preCreate(["sitemap_id" => $sitemap->id, "criteria_id" => $parent ? $parent->id : null]);
            foreach ($sitemap->details as $detail) {
                $preCreate->details()->create(['language_id' => $detail->language_id, 'country_group_id' => $detail->country_group_id]);
            }
            $title = trans("ContentPanel::categories.create_criteria");
        } else {
            Content::createUnexistentDetailsOfParent($edit);
            $title = $edit->detail->name ?? "";
            $title = ($title ? $title ." - ": ""). trans("ContentPanel::categories.edit_criteria");
            $preCreate = $edit;
        }

        $renderable_class = Content::getCriteriaRenderableClassPath($preCreate);

        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class(["criteria" => $preCreate]);

        if (!is_a($renderable, BuilderRenderable::class)) {
            return "<strong><code>$renderable_class</code></strong> <br/>bir \n<br/><strong><code>" . BuilderRenderable::class . "</code> </strong>\n<br/> türevi olmalıydı ama değil.";
        }
        return view("ContentPanel::categories.criteria.ajax", compact("renderable", "title"));
    }

    public function ajaxStore(Request $request, Criteria $criteria)
    {

        $renderable_class = Content::getCriteriaRenderableClassPath($criteria);
        $renderable = new $renderable_class(["criteria" => $criteria]);

        $storer_class = Content::getCriteriaStorerClassPath($criteria);

        $storer = new $storer_class($renderable, $request);
        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $storer->setMainObjectData($criteria);

        $storer->store();

        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $object_to_log = $storer->getMainObjectFromBucket();
        $serialized = serialize($object_to_log);

        $backup = new BackupData();
        $backup->store($storer);

        $errors = $storer->getErrors();
        $warnings = $storer->getWarnings();

        $redirect = redirect(route('Content.categories.criteria.create',$criteria->sitemap_id));

        if(count($warnings)){
            $redirect->with("warning",$warnings);
        }

        if(count($errors)){
            $redirect->withErrors($errors);
        }

        return $redirect;

    }

    public function criteriaOrderSave(Request $request)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = "criteria_id";

        $array = $this->buildTree(json_decode($request->nestable_output), $cluster->table_id);

        foreach ($cluster->cluster($array) as $criteria) {
            $criteriaModel = Criteria::find($criteria['id']);

            if($criteriaModel) {
                $criteriaModel->update($criteria);
            }
        }

        return true;
    }

    private function buildTree($array, $parent_column = "category_id")
    {
        $ls = null;

        foreach ($array as $parent) {
            if ($parent->id == 0) {
                continue;
            }

            if (isset($parent->children)) {
                $ls[] = [
                    "id" => $parent->id, $parent_column => 0, "children" => $this->children($parent->children, $parent_column, $parent->id)
                ];
            } else {
                $ls[] = ["id" => $parent->id, $parent_column => 0];
            }
        }

        return $ls;
    }

    public function nestableGetList($sitemap_id, $model, $parent_id)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = $parent_id;

        // $this->website->defaultLanguage()->id
        $default_language = 760;
        $list = $model::whereSitemapId($sitemap_id)->where('status','<>',Criteria::PREDRAFT)->orderBy("lft")->with([
            self::DETAIL => function ($query) use ($parent_id, $default_language) {
                $query->where("language_id", $default_language)->select([$parent_id, "name"]);
            }
        ])->get(["id", $parent_id, 'status']);

        $list = $list->toArray();

        foreach ($list as $key => $value) {
            if (isset($value[self::DETAIL]['name'])) {
                $list[$key][self::DETAIL]['name'] = str_replace(["'", '"'], ["\'", '\"'], $value[self::DETAIL]['name']);
            }
        }

        return str_replace("\\\\", "\\", json_encode($cluster->buildFromDB($list)));
    }

    private function children($array, $parent_column, $parent)
    {
        $childList = null;

        foreach ($array as $child) {
            if (isset($child->children)) {
                $childList[] = [
                    "id" => $child->id, $parent_column => $parent, "children" => $this->children($child->children, $parent_column, $child->id)
                ];
            } else {
                $childList[] = ["id" => $child->id, $parent_column => $parent];
            }
        }

        return $childList;
    }
}
