<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Database\Eloquent\Model;

class LanguageWebsite extends Model
{
    protected $table = "language_website";

    public $timestamps = false;

    protected $fillable = [
        'language_id',
        'website_id',
        'country_group_id',
        'default',
    ];

    public function website()
    {
        return $this->belongsTo(Website::class,"website_id");
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }
}
