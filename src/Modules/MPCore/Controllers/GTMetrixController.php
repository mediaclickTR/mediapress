<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\BaseController;
use Mediapress\FileManager\Models\MFile;

class GTMetrixController extends BaseController
{
    public function index() {

        $images = $this->getImages();
        $uploads = $this->getUploads();
        $js = $this->getJs();
        $css = $this->getCss();

        return view('mediapress::gtmetrix', compact('images', 'uploads', 'js', 'css'));
    }

    private function getImages() {
        $temp = [];
        foreach (glob(public_path('images/*')) as $image) {
            if(\Str::contains(mime_content_type($image), "image")) {
                $temp[] = image($image)->url;
            }
        }
        return $temp;
    }

    private function getUploads() {
        $temp = [];

        $images = MFile::select('id', 'mdisk_id', 'mfolder_id', 'usecase', 'fullname', 'mimeclass')
            ->where('mimeclass', 'image')
            ->orderByDesc('updated_at')
            ->get()
            ->take(50);

        foreach ($images as $image) {
            $hold = image($image->id);
            if(file_exists($hold->path)) {
                $temp[] = $hold->url;
            }
        }

        return $temp;
    }

    private function getJs() {
        $temp = [];
        foreach (glob(storage_path('app/assets/js/*')) as $js) {
            $js = str_replace(['.min.js', '.js'], '', basename($js));
            $temp[] = $js;
        }
        return implode('|', $temp);
    }

    private function getCss() {
        $temp = [];
        foreach (glob(storage_path('app/assets/css/*')) as $css) {
            $css = str_replace(['.min.css', '.css'], '', basename($css));
            $temp[] = $css;
        }
        return implode('|', $temp);
    }
}
