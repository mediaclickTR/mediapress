<?php

namespace Mediapress\Modules\Content\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Mediapress\FileManager\Models\MFileGeneral;
use Illuminate\Support\Collection;

class ImageTagsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $rows = $rows->toArray();

        unset($rows[0]);

        foreach ($rows as $row) {

            $mfileGeneral = MFileGeneral::find($row[0]);

            if($mfileGeneral) {
                $activeLanguageId = session('panel.active_language.id');
                $details = $mfileGeneral->details;

                $details[$activeLanguageId] = [
                    'title' => $row[3],
                    'tag' => $row[4],
                ];
            }

            MFileGeneral::updateOrCreate(
                [
                    'id' => $row[0],
                ],
                [
                    'details' => $details
                ]
            );
        }
    }
}
