@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::seo.metas") !!}</div>
            </div>
            <div class="float-right">
                <div class="float-left" style="margin-right:10px;">
                    <a href="#" class="btn btn-primary btn-sm" onclick="popup('getExcelImport', 1);"><i class="fa fa-upload"></i> {!! trans("ContentPanel::seo.excel.import") !!}</a>
                </div>
                <div class="float-right">
                    <a href="{!! route('Content.seo.excelExport') !!}" class="btn btn-primary btn-sm"> <i class="fa fa-file-excel"></i> {!! trans("ContentPanel::seo.excel.export") !!}</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

           <div class="p-30 mt-4">
               <div id="jsGrid">
               </div>
           </div>

@endsection

@push('scripts')

    <script type="text/javascript" src="{!! asset('vendor/mediapress/js/plugins/jsgrid/jsgrid.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/js/plugins/jsgrid/i18n/tr.js') !!}"></script>
    <script>

        @if(\App::getLocale() != 'en')
        jsGrid.locale("tr");
        @endif

        var index_types = {!! json_encode($data['index_types']) !!}

            $(function() {


                $("#jsGrid").jsGrid({
                    //height: "600px",
                    width: "100%",

                    filtering: true,
                    inserting: false,
                    editing: true,
                    sorting: true,
                    paging: true,
                    data: "{}",
                    pageButtonCount: 5,
                    autoload: true,
                    pageLoading: true,
                    pageSize: 20,
                    pageIndex: 1,

                    deleteConfirm: "{!! trans("MPCorePanel::general.sure_deletion") !!}",

                    fields: [
                        { name: "website", title: "{!! trans("ContentPanel::seo.website") !!}", type: "text", width: 100,editing: false},
                        { name: "language", title: "{!! trans("ContentPanel::seo.language") !!}", type: "text", width: 50,editing: false},
                        { name: "url", title: "{!! trans("ContentPanel::seo.url") !!}", type: "text", width: 150 ,editing: false},

                        { name: "desktop_title", title: "{!! trans("ContentPanel::seo.desktop.title") !!}", type: "text", width: 100 ,editing: true},
                        { name: "desktop_description", title: "{!! trans("ContentPanel::seo.desktop.description") !!}", type: "text", width: 100 ,editing: true},

                        { name: "indexed", type: "select", items: index_types, valueField: "key", textField: "value", title: "{!! trans("ContentPanel::seo.index.types") !!}", width: 130, validate: "required"},
                        {
                            name: "metas",
                            title: "{!! trans("ContentPanel::seo.desktop.all") !!}",
                            width: 50,
                            align: "center",
                            itemTemplate: function (value, item) {
                                var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                                var data_ = {};
                                data_['url_id'] = item.id;
                                data_['_token'] = '{!! csrf_token() !!}';
                                var $customButton = $("<a href='#' class='modalBtn' onclick=\"popup('urlMetasList'," + item.id + ");\"><i class='fa fa-external-link-alt modal-trigger'></i></a>");
                                return $result.add($customButton);
                            }
                        },
                        { type: "control", editButton: true, modeSwitchButton: false, deleteButton: false }
                    ],
                    onItemUpdated: function (args) {

                        var item = args.item;

                        var p1, p2, p3, p4, p5, p6;
                        var data_ = {};

                        p1 = 'key[' + item.id + '][title][desktop_value]';
                        p2 = 'key[' + item.id + '][description][desktop_value]';

                        p3 = 'key[' + item.id + '][indexed]';

                        data_[p1] = item.desktop_title;
                        data_[p2] = item.desktop_description;

                        data_[p3] = item.indexed;

                        data_['_token'] = '{!! csrf_token() !!}';
                        return $.ajax({
                            type: "POST",
                            url: "{!! route("Content.seo.metasQuickUpdate") !!}",
                            data: data_,
                            async: false,
                            dataType: "json",
                            success: function (data) {
                                //console.log(data);
                            },
                            error: function (status1) {
                            }
                        });
                    },
                    rowClick: function (args) {
                        var $target = $(args.event.target);

                        if ($target.closest("a.modalBtn").length) {
                            return;
                        }

                        if (this.editing) {
                            this.editItem($target.closest("tr"));
                        }
                    },
                    controller: {
                        loadData: function (filter) {
                            const deferred = $.Deferred();
                            var data_ = {};
                            data_["filter"] = filter;
                            data_['_token'] = '{!! csrf_token() !!}';
                            $.ajax({
                                type: "POST",
                                url: "{!! route("Content.seo.listFilter") !!}",
                                data: data_,
                                dataType: "json",
                                success: function (response) {
                                    deferred.resolve({
                                        data : response.list,
                                        itemsCount : response.count
                                    });
                                },
                            });
                            return deferred.promise();
                        },
                        deleteItem: function(item) {
                            var data_ = {};
                            data_["item_id"] = item.id;
                            data_['_token'] = '{!! csrf_token() !!}';
                            return $.ajax({
                                type: "POST",
                                url: "{!! route("Content.seo.listDelete") !!}",
                                data: data_,
                                async: false,
                                dataType: "json",
                                success: function (data) {
                                    swal({
                                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                                    })
                                },
                                error: function (status1) {
                                }
                            });
                        },

                    }
                });
            });

        function closeModal(el)
        {
            el=$(el);
            var modal = el.closest(".modal");
            var modal_content = el.parent();
            $(modal).modal('hide');
            $(".main-modal .modal-content").html(null);
            //$("#jsGrid").jsGrid("loadData");
        }
    </script>

@endpush

@push('styles')
    <style>
        td.jsgrid-cell.jsgrid-control-field.jsgrid-align-center input{float:left;margin:0 !important;}
        .jsgrid-grid-header{background: none;}
        .jsgrid-table{margin:0;}
        .jsgrid-filter-row>.jsgrid-cell{background: #fff !important;}
        .jsgrid-header-cell{font-size: 15px;white-space: nowrap;font-weight: 500;letter-spacing: -0.3px;overflow: hidden;}
        .modal-dialog{
            min-width: 60% !important;
        }
        tr td {
            text-align:center;
        }
        .jsgrid-pager-current-page{font-weight: 100 !important;}
        .jsgrid-header-row>.jsgrid-header-cell {
            background: #f3f4f9 !important;
        }
        .jsgrid-pager-container{
            margin-top:10px !important;
        }
        .jsgrid-pager-current-page{
            font: 13px 'Poppins', sans-serif;
            color: #272727!important;
            border:1px solid #d2d2d2!important;
            border-radius: 4px !important;
            padding: 4px 10px !important;
        }
        .jsgrid-pager-page a{
            font: 13px 'Poppins', sans-serif;
            color: #272727 !important;
        }
        .jsgrid-pager-page a:hover{
            color: #2196f3!important;
        }
        .jsgrid-pager-page{
            padding: 4px 12px !important;
        }
        .jsgrid-pager-nav-button a{
            font: 13px Comfortaa-Regular;
            color: #272727 !important;
        }
        .jsgrid-pager{font-size:13px;}
    </style>
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/plugins/jsgrid/jsgrid.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/plugins/jsgrid/jsgrid-theme.min.css') !!}"/>
@endpush
