<div class="menu">
    @foreach(collect($general_menus['header_menu'])->groupBy("position") as $key=>$base_menu)
        @if($key=="right")
            <div class="right">
                @foreach($base_menu as $menu)
                    @if($menu['type']=="languages")
                    <div class="langbox">
                        @if(isset($menu['cols']))
                            <ul>
                                @foreach($menu['cols'] as $key=>$col)
                                    <li>
                                        <a href="#"><img src="{!! asset($public_url."images/langs.svg") !!}" alt=""><!--{!! strtoupper($col['name']) !!}!--></a>
                                        <ul>
                                            @foreach($col['rows'] as $key=>$item)
                                                <li>
                                                    <a href="{!! route('MPCore.session.setLanguageSession', $item['id']) !!}">
                                                        {!! strtoupper($item['title']) !!}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach

                            </ul>
                        @endif
                    </div>
                    @elseif($menu['type']=="author")
                        <div class="author">
                            @if(isset($menu['cols']))
                                @foreach($menu['cols'] as $col)
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="images">
                                                    <img src="{!! $col['author_image'] !!}" class="resimatt" alt="">
                                                </div>
                                                @php
                                                    $exp = explode(" ", $col['author_title']);
                                                    $name = $exp[0];
                                                    $lastname = mb_substr(array_pop($exp), 0, 1).".";
                                                @endphp
                                                {!! $name. " ". $lastname  !!}
                                            </a>
                                            @if(isset($col['rows']))
                                                <ul>
                                                    @foreach($col['rows'] as $item)
                                                        <li>
                                                            <a @if(isset($item['onclick'])) onclick="{!! $item['onclick'] !!}" @endif href="{!! (isset($item['url'])) ? $item['url'] : '#' !!}" @if($item['target']=="_blank") target="_blank" @endif>
                                                                {!! $item['title'] !!}
                                                            </a>
                                                            @if(isset($item['additions']))
                                                                {!! $item['additions'] !!}
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    </ul>
                                @endforeach
                            @endif
                        </div>
                    @elseif($menu['type']=="sites")
                            @if(isset($menu['cols']))
                                <div class="sites">
                                    <ul>
                                        @foreach($menu['cols'] as $key=>$col)
                                            <li>
                                                <a href="#">{!! $col['name'] !!}</a>
                                                @if(count($col['rows'])>0)
                                                    <ul>
                                                        @foreach($col['rows'] as $key=>$item)
                                                            <li>
                                                                @if($item['target']=="external")
                                                                    <a href="{!! (isset($item['url'])) ? $item['url'] : '#' !!}" target="_blank">
                                                                        <i class="fa fa-external-link-alt"></i> {!! $item['title'] !!}
                                                                    </a>
                                                                @elseif($item['target']=="internal")
                                                                    <a href="{!! route('MPCore.session.setWebsiteSession', $item['id']) !!}">
                                                                        {!! $item['title'] !!}
                                                                    </a>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                    @endif
                @endforeach
            </div>
        @elseif($key == "left")
            <div class="left">
                <ul>
                    @foreach($base_menu as $menu)
                    <li  class="{{ isset($menu['cols']) && count($menu['cols']) > 1 ? 'pluralSub' : 'singleSub' }}">
                        <a href="{!! (isset($menu['url'])) ? $menu['url'] : '#' !!}"  @if(isset($menu['target']))  target="{!! $menu['target'] !!}"@endif>{!! $menu['name'] !!}</a>
                        @if(isset($menu['cols']))
                            <ul class="submenu">
                                @foreach($menu['cols'] as $key=>$col)
                                    @if(count($col['rows']) <= 0)
                                        @continue
                                    @endif
                                    <li>
                                        <ul>
                                            <li class="head">{!! $col['name'] !!}</li>
                                            @foreach($col['rows'] as $item)
                                                <li  class="{!! isset($item['children']) ? 'subTop' : '' !!}">

                                                    <a href="{!! (isset($item['url'])) ? $item['url'] : 'javascript:void(0);' !!}"
                                                       {!! isset($item['onclick']) ? 'onclick="'.$item['onclick'].'"' : '' !!}
                                                       target="{{ $item['target'] ?? '_self' }}">{!! $item['title'] !!} @if(isset($item['children']))<i class="fa fa-angle-right blue ml-2"></i>@endif</a>
                                                    @if(isset($item['children']))
                                                        <ul>
                                                            @foreach($item['children'] as $child)
                                                                <li>
                                                                    <a href="{!! (isset($child['url'])) ? $child['url'] : 'javascript:void(0);' !!}"
                                                                       target="{{ $child['target'] ?? '_self' }}">{!! $child['title'] !!}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endforeach
</div>
