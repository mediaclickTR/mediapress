<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSitemapXmlsTableAddRenewFreqColumn extends Migration
{

    public const SITEMAP_XMLS = 'sitemap_xmls';

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_XMLS)) {
            $schema->table(self::SITEMAP_XMLS, function (Blueprint $table) {
                $table->string('renew_freq',7)->default('weekly');
                
            });
        }
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_XMLS)) {
            $schema->table(self::SITEMAP_XMLS, function (Blueprint $table) {
                $table->dropColumn("renew_freq");
            });
        }
    }
}
