<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Models\Website;

class PanelLanguagePart extends BaseModel
{
    protected $table = 'panel_language_parts';
    public $timestamps = false;

    protected $guarded = ["id"];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }
    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
