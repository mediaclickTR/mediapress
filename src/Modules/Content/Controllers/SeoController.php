<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Modules\MPCore\Models\SettingSun;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Facades\MetaEngine;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\Content\Models\SitemapXml;
use Mediapress\Modules\Content\Models\SitemapXmlBlock;

class SeoController extends Controller
{
    // TODO ::
    // SEO formüller ({$content_title}, {$content_category} {$pagination}) modülü olacak.
    // Url meta düzenleme kısmında bu formülleri kullanarak meta oluşturabilecek.

    public const PANEL_WEBSITE = "panel.website";
    public const MESSAGE = 'message';
    public const WEBSITE_ID = 'website_id';
    public const GROUP = "group";
    public const TITLE = "title";
    public const ROBOTS_TXT = "Robots.txt";
    public const CONTENT_ROBOT = "content.robot";
    public const VALUE = 'value';

    public function listMetas()
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.index",
            "content.websites.seo.meta_management.index",
        ])){
            return rejectResponse();
        }
        $current_site = session(self::PANEL_WEBSITE)->id;
        if (request()->has("list") && request("list") == "all") {
            $data = MetaEngine::getUrls()[1];
        } else {
            $data = MetaEngine::getUrls($current_site)[1];
        }
        return view("ContentPanel::seo.metas", compact("data"));
    }

    public function storeMeta(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.update",
            "content.websites.seo.meta_management.update",
        ])){
            return rejectResponse();
        }
        return MetaEngine::storeMeta($request->all());
    }

    public function metasQuickUpdate(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.update",
            "content.websites.seo.meta_management.update",
        ])){
            return rejectResponse();
        }
        return MetaEngine::metasQuickUpdate($request->key);
    }

    public function listFilter(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.update",
            "content.websites.seo.meta_management.update",
        ])){
            return rejectResponse();
        }
        return MetaEngine::listFilter($request->filter);
    }

    public function listUpdate(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.update",
            "content.websites.seo.meta_management.update",
        ])){
            return rejectResponse();
        }
        return MetaEngine::listUpdate($request->key);
    }

    public function listDelete(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.delete",
            "content.websites.seo.meta_management.delete",
        ])){
            return rejectResponse();
        }
        return MetaEngine::listDelete($request->item_id);
    }

    #region Excel
    public function excelExport()
    {

        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.export",
            "content.websites.seo.meta_management.export",
        ])){
            return rejectResponse();
        }

        if (request()->has("list") && request("list") == "all") {
            $data = MetaEngine::excelExport();
        } else {
            $data = MetaEngine::excelExport($website_id);
        }
        return $data;
    }

    public function excelImport(Request $request)
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.meta_management.import",
            "content.websites.seo.meta_management.import",
        ])){
            return rejectResponse();
        }
        $this->validate($request,
            ['excel' => 'required'],
            ['excel.required' => trans("MPCorePanel::validation.filled", ['filled', trans("ContentPanel::seo.excel.select")])],
            ['excel' => trans("ContentPanel::seo.excel.select")]
        );
        $import = MetaEngine::excelImport($request->all());
        if ($import) {
            return redirect(route('Content.seo.metas'))->with(self::MESSAGE, trans('MPCorePanel::general.success_message'));
        } else {
            return redirect(route('Content.seo.metas'))->with(self::MESSAGE, trans('ContentPanel::seo.excel.import.error'));
        }
    }

    #endregion

    #region robots.txt
    public function robots()
    {

        $val = SettingSun::select(['id', 'value'])
            ->where([
                self::WEBSITE_ID => session(self::PANEL_WEBSITE)->id,
                self::GROUP => "Meta",
                self::TITLE => self::ROBOTS_TXT,
                "key" => self::CONTENT_ROBOT
            ])
            ->first();
        if ($val) {
            return response($val->value)->header('Content-Type', 'text/plain');
        } else {
            return response("User-Agent: *\nAllow: \nDisallow: /mp-admin")->header('Content-Type', 'text/plain');
        }
    }

    public function robotIndex()
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.robots.view",
            "content.websites.seo.robots.view",
        ])){
            return rejectResponse();
        }

        $robot = SettingSun::where([self::WEBSITE_ID => session(self::PANEL_WEBSITE)->id, self::GROUP => "Meta", self::TITLE => self::ROBOTS_TXT, "key" => self::CONTENT_ROBOT])->first([self::VALUE]);
        if ($robot) {
            $robot = $robot->value;
        } else {
            $robot = "";
        }
        return view("ContentPanel::seo.robot", ["robot" => $robot]);
    }

    public function robotUpdate()
    {
        $website_id = session(self::PANEL_WEBSITE)->id;

        if(! activeUserCan([
            "content.websites.website".$website_id.".seo.robots.update",
            "content.websites.seo.robots.update",
        ])){
            return rejectResponse();
        }

        try {
            SettingSun::updateOrCreate([self::WEBSITE_ID => session(self::PANEL_WEBSITE)->id, self::GROUP => "Meta", self::TITLE => self::ROBOTS_TXT, "key" => self::CONTENT_ROBOT], [self::VALUE => request('robot')]);
        } catch (\Exception $e) {
            return back()->withErrors([$e->getMessage()]);
        }
        return back()->with(self::MESSAGE, trans('MPCorePanel::general.success_message'));
    }

    #endregion



}
