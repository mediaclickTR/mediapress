<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaDetailsTable extends Migration
{

    public const CRITERIA_DETAILS = 'criteria_details';

    public function up()
    {
        if (!Schema::hasTable(self::CRITERIA_DETAILS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CRITERIA_DETAILS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('criteria_id')->unsigned()->index();
                $table->integer('country_group_id')->unsigned()->index();
                $table->integer('language_id')->unsigned()->index();
                $table->string('name');
                $table->string('slug')->nullable();
                $table->text('detail');

                $table->integer("status")->default(3);
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CRITERIA_DETAILS);
    }
}
