<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Foundation\MPCache;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\MPCore\Models\UserActionLogs;

class UserActionLogController extends Controller
{
    public function index()
    {

        if(! activeUserCan(["mpcore.userlogs.view",])){return rejectResponse();}

        $actions = UserActionLogs::orderBy("id","DESC");
        $queries = FilterEngine::filter($actions,false)['queries'];
        $actions = FilterEngine::filter($actions,false)['model'];

        return view("MPCorePanel::useraction_logs.index", compact('actions', 'queries'));
    }
}
