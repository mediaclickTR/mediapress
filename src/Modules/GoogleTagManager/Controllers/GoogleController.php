<?php

namespace Mediapress\Modules\GoogleTagManager\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Analytics;
use Illuminate\Support\Collection;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Mediapress\Modules\GoogleTagManager\Traits\GoogleTagManagerTrait;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public const TOTALS_FOR_ALL_RESULTS = 'totalsForAllResults';
    public const GA_PAGEVIEWS = 'ga:pageviews';
    public const GA_USERS = 'ga:users';
    protected $days;
    public $account_id;
    public $access_token;

    use GoogleTagManagerTrait;

    public function __construct()
    {
        $this->tagManagerInit();

        if(request('days') || request('days')=='0'){
            $this->days = (int)str_replace(array(' ', ','), '', request('days'));
        }
        else{
            $this->days = 7;
        }
    }

    public function login()
    {
        if(! activeUserCan(["googletagmanager.auth",])){return rejectResponse();}

        $website = session('panel.website.slug');

        $http_schema = request()->server('HTTP_X_FORWARDED_PROTO', request()->server('REQUEST_SCHEME', 'http'));
        $host = $http_schema . "://www." . str_replace('www.', '', $website);

        return redirect()->to("https://googleauth.mediapress.com.tr/login/google?host=".$host.'&type=tag-manager&redirectBackUrl='.getHttpSchemaWithHost());
    }

    public function oAuth()
    {
        if(! activeUserCan(["googletagmanager.auth",])){return rejectResponse();}

        $expire_date_check = SettingSun::where("key","google.tagmanager.expire.date")
                                            ->where("key","google.tagmanager.account.id")
                                            ->where("key","google.tagmanager.refresh.token")
                                            ->where("key","google.tagmanager.access.token")
                                            ->where("website_id",session("panel.website")->id)->first();

        if (!$expire_date_check || time() > $expire_date_check->value)
        {
            $contents = $this->refreshOrRegisterAuth(request("refresh_token"));

            if($contents) {
                return response("<script>var data = ".json_encode($contents).";window.close();</script>");
            }
            return response("<script>var data = {'status':false, 'message' : '".trans("MPCorePanel::analytics.tag_manager.data.notfound.with.website",['website'=>$host])."'};window.close();</script>");
        }
        else{
            return response("<script>window.close();</script>");
        }
    }

    public function index()
    {
        $conn = 1;
        $max=0;

        try {
            $data = [
                'visitorAndPageViews' => $this->fetchVisitorsAndPageViews($this->days)['rows'],
                'totalViews' => $this->getTotalViews($this->days)[self::TOTALS_FOR_ALL_RESULTS][self::GA_PAGEVIEWS],
                'users' => $this->getUsers($this->days)[self::TOTALS_FOR_ALL_RESULTS][self::GA_USERS],
                'uniquePageviews' => $this->getUniquePageViews($this->days)[self::TOTALS_FOR_ALL_RESULTS]['ga:uniquePageviews'],
                'organicSearch' => $this->getOrganicSearch($this->days)[self::TOTALS_FOR_ALL_RESULTS]['ga:organicSearches'],
            ];
            foreach ($data['visitorAndPageViews'] as $values) {
                if (($values[self::GA_USERS] + 0) > $max) {
                    $max = $values[self::GA_USERS] + 0;
                }
                if (($values[self::GA_PAGEVIEWS] + 0) > $max) {
                    $max = $values[self::GA_PAGEVIEWS] + 0;
                }
            }
        }

        catch (\Exception $e) {
            $conn = 0;
        }

        return view("MPCorePanel::analytics.google.index", compact('data','max','conn'));
    }

    public function detailedAnalytics()
    {
        $conn = 1;
        try {
        $pageAnalytics = $this->getPageAnalytics($this->days);
        $trafficSources = $this->getTrafficSources($this->days);
        $countries = $this->getSessionCountries($this->days);
        $browsersAndSystems = $this->getBrowserAndSystems($this->days);
        $searchEngineKeywords = $this->getSearchEngineKeywords($this->days);
        }
        catch (\Exception $e) {
            $conn = 0;
        }

        return view("MPCorePanel::analytics.google.pageAnalytics",
            compact('pageAnalytics',
                            'trafficSources',
                            'countries',
                            'browsersAndSystems',
                            'searchEngineKeywords',
                            'conn'
            ));
    }

}
