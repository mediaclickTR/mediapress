@extends('MPCorePanel::inc.app')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="p-30 mt-4">
            <div class="sitemapList">
                <h2>{!! __('ContentPanel::import_pages.upload_excel') !!}</h2>
                <div class="pt-3">
                    @include("MPCorePanel::inc.errors")
                    <form action="{!! route("Content.import_pages.import") !!}" method="post" enctype="multipart/form-data" id="importForm">
                        @csrf
                        <input type="hidden" name="language" value="{{request()->language}}">
                        <div class="form-group mt-3">
                            <input type="file" class="form-control-file" name="file">
                        </div>
                        <div class="p-2">
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-primary dropdown-toggle float-right" id="dropdownBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {!! __('ContentPanel::import_pages.import') !!}
                                </button>
                                <div class="dropdown-menu">
                                    <button type="submit" name="type" value="excel" class="dropdown-item">Excel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="successMessage d-none text-center">
                <h3></h3>
            </div>
        </div>
    </div>
@endsection

