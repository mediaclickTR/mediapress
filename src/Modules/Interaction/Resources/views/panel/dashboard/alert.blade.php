@if($repoName == 'InteractionRepository')
    <div class="alert alert-light" style="border:#ccc 1px solid">
        {!! trans("InteractionPanel::dashboard.interaction_title") !!}
        <a href="javascript:void(0);" id="google_connect" style="color:#6c757d" class="float-right" href="#">
            {!! trans("InteractionPanel::dashboard.google_connect") !!}
            <img src="{!! asset('vendor/mediapress/images/analytics.png') !!}" class="ml-2" width="70" alt="">
        </a>
        <div class="clearfix"></div>
    </div>

    <script>
        function responseDataPopup(data) {
            if (data.status) {
                location.reload();
            } else {
                var error = `<div class="alert alert-danger" role="alert" style="border:#ccc 1px solid"> ` + data.message + ` <button type="button" style="color:#000" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>`;
                $('.page-content').prepend(error);
            }
        }

        $.oauthpopup = function (options) {
            options.windowName = options.windowName || 'ConnectWithOAuth'; // should not include space for IE
            var w = 450;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            options.windowOptions = options.windowOptions || 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left;
            options.callback = options.callback || function () {
                window.location.reload();
            };
            var that = this;
            that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
            that._oauthInterval = window.setInterval(function () {
                if (that._oauthWindow.data) {
                    responseDataPopup(that._oauthWindow.data);
                }
                if (that._oauthWindow.closed) {
                    window.clearInterval(that._oauthInterval);
                    options.callback();
                }
            }, 100);
        };

        $('#google_connect').on("click", function () {
            $.oauthpopup({
                windowName: "{{ __("InteractionPanel::dashboard.google_connet_title") }}",
                path: "{!! route("Interaction.analytics.google.login").'?id='.session('panel.website')->id !!}",
                callback: function () {
                }
            });
        });
    </script>
@else
    <div class="alert alert-light" style="border:#ccc 1px solid">
        {!! trans("InteractionPanel::dashboard.google_title") !!}
        <a href="javascript:void(0);" id="google_disconnect" style="color:#6c757d" class="float-right" href="#">
            {!! trans("InteractionPanel::dashboard.google_disconnect") !!}
        </a>
        <div class="clearfix"></div>
    </div>

    <script>
        $('#google_disconnect').on("click", function () {

            $.ajax({
                'url': "{!! route('Interaction.analytics.google.googleAnalyticsDisconnect') !!}",
                'method': "GET",
                success: function (response) {
                    location.reload();
                }
            })
        });
    </script>
@endif
