@extends('MPCorePanel::inc.app')
@push("modals")
    <div class="modal fade" id="backup-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal</h4>

                    <button type="button" class="close" id="closePopup2" data-dismiss="modal" aria-hidden="true"
                            onclick="javascript:close_backup(this);">×
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    @include('ContentPanel::sitemaps.popupform')
@endpush
@push("styles")

@endpush
@push("scripts")
    <script src="{!! asset('vendor/mediapress/ContentModule/js/StaticSitemapEditor.js') !!}"></script>
@endpush
