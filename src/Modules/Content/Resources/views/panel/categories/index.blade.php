@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
        <div class="page-content p-0">
            <div class="topPage">
                <div class="float-left">
                    <div class="title m-0">
                        {!! trans("MPCorePanel::general.selection") !!}
                    </div>
                </div>
            </div>
            <div class="p-30 pt-4">
                <div class="row">
                    <div class="col-md-4">
                        <div class="item">
                            <div class="form-group">
                                <div class="submit">
                                    <a href="{{ route("Content.categories.getList", "category") }}"><button class="btn btn-primary">{!! trans("ContentPanel::categories.categories") !!} ({!! $data['countCategories'] !!})</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item">
                            <div class="submit">
                                <a href="{{ route("Content.categories.getList", "criteria") }}"><button class="btn btn-primary">{!! trans("ContentPanel::categories.criterias") !!} ({!! $data['countCriterias'] !!})</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item">
                            <div class="submit">
                                <a href="{{ route("Content.categories.getList", "property") }}"><button class="btn btn-primary">{!! trans("ContentPanel::categories.properties") !!} ({!! $data['countProperties'] !!})</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push("styles")
    <style>
        .btn-primary{
            width:100% !important;
        }
    </style>
@endpush
