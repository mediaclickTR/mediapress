<?php

namespace Mediapress\Modules\Content;

use Illuminate\Support\Facades\Route;
use Mediapress\Modules\Content\Foundation\CriteriaEngine;
use Mediapress\Modules\Content\Foundation\PropertyEngine;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Popup;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\SitemapXml;
use Mediapress\Modules\Content\Observers\CategoryDetailObserver;
use Mediapress\Modules\Content\Observers\PageDetailObserver;
use Mediapress\Modules\Content\Observers\PageObserver;
use Mediapress\Modules\Content\Observers\PopupObserver;
use Mediapress\Modules\Content\Observers\SceneObserver;
use Mediapress\Modules\Content\Observers\SitemapDetailObserver;
use Mediapress\Modules\Content\Observers\SitemapObserver;
use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\Content\Facades\Content;
use Illuminate\Support\Arr;
use Mediapress\Modules\MPCore\Models\BaseModel;

class ContentServiceProvider extends ServiceProvider
{
    public const CONTENT = "Content";
    public const RESOURCES = 'Resources';
    public const SITEMAP_PHP = 'sitemap.php';
    /**
     * Bootstrap services.
     *
     * @return void
     */
    protected $module_name = self::CONTENT;
    protected $namespace = 'Mediapress\Modules\Content';

    public function boot()
    {
        Parent::boot();
        PageDetail::observe(PageDetailObserver::class);
        SitemapDetail::observe(SitemapDetailObserver::class);
        CategoryDetail::observe(CategoryDetailObserver::class);
        Sitemap::observe(SitemapObserver::class);
        Page::observe(PageObserver::class);

        /**
         * This function is already used in the Module.php class.
         *
         * $this->map();
         */
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'web', $this->module_name . 'Web');

        if (file_exists(resource_path("views" . DIRECTORY_SEPARATOR . "popups" . DIRECTORY_SEPARATOR . "show.blade.php"))) {
            $this->loadViewsFrom(resource_path("views" . DIRECTORY_SEPARATOR . "popups"), "Popup");
        } else {
            $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'web/popups', "Popup");
        }

        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'web', $this->module_name . 'Web');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->publishes([
            __DIR__ . '/Assets' => public_path('vendor/mediapress/ContentModule'),
        ]);
        //$this->mergeConfigFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . "content_module_actions.php", "content_module_actions");
//        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name . 'Database');
        $this->publishActions(__DIR__);

    }

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param string $path
     * @param string $key
     * @return void
     */

    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @param array $original
     * @param array $merging
     * @return array
     */
    protected function contentModuleMergeConfig(array $original, array $merging)
    {
        $array = array_merge($original, $merging);
        foreach ($original as $key => $value) {
            if (!is_array($value)) {
                continue;
            }
            if (!Arr::exists($merging, $key)) {
                continue;
            }
            if (is_numeric($key)) {
                continue;
            }
            $array[$key] = $this->contentModuleMergeConfig($value, $merging[$key]);
        }
        return $array;
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('CategoryEngine', Content::class);
        app()->bind('CategoryEngine', function () {
            return new CategoryEngine;
        });
        $loader->alias('CriteriaEngine', Content::class);
        app()->bind('CriteriaEngine', function () {
            return new CriteriaEngine;
        });
        $loader->alias('PropertyEngine', Content::class);
        app()->bind('PropertyEngine', function () {
            return new PropertyEngine;
        });
        $loader->alias(self::CONTENT, Content::class);
        app()->bind(self::CONTENT, function () {
            return new \Mediapress\Modules\Content\Content;
        });
    }

    public function injectDynamicWebRoutes()
    {
        parent::injectDynamicWebRoutes();

        try {
            $sitemap_xmls = SitemapXml::where("status", BaseModel::ACTIVE)->get()->pluck("filename")->toArray();

            if (count($sitemap_xmls)) {
                $sitemap_xmls = implode("|", $sitemap_xmls);
                Route::get('{xml}.xml', 'WebsiteSitemapXmlController@index')->where("xml", $sitemap_xmls);
                Route::get('{subfolder}/{xml}.xml', 'WebsiteSitemapXmlController@index')->where("xml", $sitemap_xmls);
                Route::get('{subfolder}/{subfolder2}/{xml}.xml', 'WebsiteSitemapXmlController@index')->where("xml", $sitemap_xmls);
                //Route::namespace('Admin')->group(function () {
                /*Route::get('{lang}/{xml}.xml', 'WebsiteSitemapXmlController@sitemapXml')->where("xml", $sitemap_xmls);
                Route::get('{lang}/{xml}.xml', 'WebsiteSitemapXmlController@mainPageLang')->where("xml", $sitemap_xmls);
                Route::get('{lang}/{type}/{?page}/{xml}.xml', 'WebsiteSitemapXmlController@xmlPageLang')->where("xml", $sitemap_xmls);
                Route::get('{lang}/{type}/{limit}/{xml}.xml', 'WebsiteSitemapXmlController@xmlPageForPagesLang')->where("xml", $sitemap_xmls);*/
                //});
            }
        } catch (\Exception $e) {
            //pass
            $e->getMessage();
        }

    }


}
