@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">{{ $commentStructure->name }}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('CommentStructure.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form" style="margin: 0 auto; width: 60%">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('CommentStructure.update', ['id' => $commentStructure->id]) !!}" method="post">
                    @csrf

                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="name">{!! __('CommentPanel::create.labels.name') !!}</label>
                        </div>
                        <div class="col-md-7">
                            <input name="name" type="text" value="{!! old('name', $commentStructure->name) !!}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="name">{!! __('CommentPanel::create.labels.key') !!}</label>
                        </div>
                        <div class="col-md-7">
                            <input name="key" type="text"  value="{!! old('key', $commentStructure->key) !!}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5">
                            <label for="image-radio">{!! __('CommentPanel::create.labels.status') !!}</label>
                        </div>
                        <div class="col-md-7">
                            <div class="checkbox">
                                <label for="active">
                                    <input type="radio"
                                           name="status"
                                           id="active"
                                           value="1"
                                        {{ old('status', $commentStructure->status) == 1 ? 'checked' : '' }}>
                                    {!! __('CommentPanel::create.status.active') !!}
                                </label>
                                <label for="passive">
                                    <input type="radio"
                                           name="status"
                                           id="passive"
                                           value="2"
                                        {{ old('status', $commentStructure->status) == 2 ? 'checked' : '' }}>
                                    {!! __('CommentPanel::create.status.passive') !!}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('CommentPanel::create.labels.show_time') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="show_time_after">
                                <input type="radio"
                                       name="show_time"
                                       id="show_time_after"
                                       value="1"
                                    {{ old('show_time', $commentStructure->show_time) == 1 ? 'checked' : '' }}>
                                {!! __('CommentPanel::create.show_time.after') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="show_time_confirmed">
                                <input type="radio"
                                       name="show_time"
                                       id="show_time_confirmed"
                                       value="2"
                                    {{ old('show_time', $commentStructure->show_time) == 2 ? 'checked' : '' }}>
                                {!! __('CommentPanel::create.show_time.confirmed') !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('CommentPanel::create.labels.condition') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="condition_all">
                                <input type="radio"
                                       name="condition"
                                       id="condition_all"
                                       value="1"
                                    {{ old('condition', $commentStructure->condition) == 1 ? 'checked' : '' }}>
                                {!! __('CommentPanel::create.condition.all') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="condition_login">
                                <input type="radio"
                                       name="condition"
                                       id="condition_login"
                                       value="2"
                                    {{ old('condition', $commentStructure->condition) == 2 ? 'checked' : '' }}>
                                {!! __('CommentPanel::create.condition.login') !!}
                            </label>
                        </div>
                    </div>

                    <div class="submit">
                        <button class="btn btn-primary">{!! __('CommentPanel::create.save') !!}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
    <script>
        $('[name="name"]').on('keyup', function () {
            var value = $(this).val();
            $('[name="key"]').val(slugify(value));
        });

        slugify = function (text) {
            var map = {
                'çÇ': 'c',
                'ğĞ': 'g',
                'şŞ': 's',
                'üÜ': 'u',
                'ıİ': 'i',
                'öÖ': 'o'
            };
            for (var key in map) {
                text = text.replace(new RegExp('[' + key + ']', 'g'), map[key]);
            }
            return text.replace(/[^-a-zA-Z0-9\s]+/ig, '')
                .replace(/\s/gi, "-")
                .replace(/[-]+/gi, "-")
                .toLowerCase();
        }
    </script>
@endpush
