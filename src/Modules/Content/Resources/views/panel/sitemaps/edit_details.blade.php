@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! trans("ContentPanel::sitemap.edit_sitemap") !!}</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">

                </div>
            </div>
        </div>
    </div>
@endsection
