@php
    $active_role = isset($admin) && count($a=$admin->getRoles()) ? $a[0] : "";
@endphp
@if(!isset($admin))
    {{ Form::open(['route' => 'Auth.admins.store']) }}
@else
    {{ Form::model($admin, ['route' => ['Auth.admins.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$admin->id) !!}
@endif
@csrf

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group min-100">
                    <div class="tit">{!! trans("AuthPanel::admin.roles") !!}</div>
                    {!!Form::select('role_id', $roles, array_search($active_role,$roles->toArray()), ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group min-100">
                    <div class="tit">{!! trans("AuthPanel::admin.language") !!}</div>
                    {!!Form::select('language_id', $languages,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group min-100">
                    <div class="tit">{!! trans("AuthPanel::admin.select_zone") !!}</div>
                    {!!Form::select('country_group', $zones,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
                </div>
            </div>
            @isset($admin)
                <div class="col-md-3">
                    <div class="form-group min-100">
                        <div class="tit">API TOKEN</div>
                        <input type="text" value="{{ strtoupper($admin->api_token) }}" disabled>
                    </div>
                </div>
                @if(isMCAdmin() && $admin->google2fa)
                    <div class="col-md-3">
                        <div class="form-group min-100">
                            <div class="tit">GOOGLE 2FA</div>
                            <input type="text" name="google2fa" value="{{ $admin->google2fa }}">
                        </div>
                    </div>
                @endif
            @endisset
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.name-admin") !!}</label>
            {!!Form::text('first_name', null, ["placeholder"=>trans("AuthPanel::admin.name-admin"), "class"=>"validate[required]"])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.surname-admin") !!}</label>
            {!!Form::text('last_name', null, ["placeholder"=>trans("AuthPanel::admin.surname-admin")])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.email-admin") !!}</label>
            {!!Form::text('email', null, ["placeholder"=>trans("AuthPanel::admin.email-admin"), "class"=>"validate[required]"])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.phone-admin") !!}</label>
            {!!Form::text('phone', null, ["placeholder"=>trans("AuthPanel::admin.phone-admin")])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.password-admin") !!}</label>
            {!!Form::password('password', null, ["placeholder"=>trans("AuthPanel::admin.password-admin")])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus">
            <label>{!! trans("AuthPanel::admin.repeat-password-admin") !!}</label>
            {!!Form::password('password_confirmation', null, ["placeholder"=>trans("AuthPanel::admin.repeat-password-admin")])!!}
        </div>
    </div>
</div>

<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

{{ Form::close() }}
