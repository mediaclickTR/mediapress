<?php

return [
    'modal_title' => "BreadCrumb Ayarı",
    'add_homepage' => "Breadcrumb linklerinin başına Anasayfa eklensin mi? (<b>Anasayfa > Kurumsal</b> gibi...)",
    'add_domain' => "Breadcrumb urllerinin başında domain olsun mu? (<b>domain.com/biz-kimiz</b> | <b>/biz-kimiz</b>)",
    'menu_slug' => "İçerikler hangi menüden okunsun?",
    'category_depth' => "Kategorili yapılarda, soldan sağa kaç seviye kategori gözüksün? (Kategori öncesindeki seviyeler hariç. 0 yazıldığında en üst kategori, 1 yazıldığında en üst ve bir altındaki kategori gelir.)",

    'string_type' => [
        'title' => "İçerikler nasıl gözükmeli? <br><b>Kurumsal > Biz Kimiz</b><br><b>Kurumsal > Biz kimiz</b><br><b>KURUMSAL > BİZ KİMİZ</b><br><b>kurumsal > biz kimiz</b>",
        'all_word_first_char' => "Sadece ilk harfler büyük",
        'first_word_first_char' => "Sadece ilk kelimelerin ilk harfi büyük",
        'all_word_all_char' => "Tüm harfler büyük",
        'all_word_no_char' => "Tüm harfler küçük",
    ]
];
