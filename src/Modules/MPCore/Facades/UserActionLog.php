<?php

namespace Mediapress\Modules\MPCore\Facades;

use Illuminate\Support\Facades\Facade;

class UserActionLog extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\MPCore\Foundation\UserActionLog::class;
    }

}