<?php
return [
    "heraldist"=>[
        'name' => 'HeraldistPanel::auth.sections.heraldist_module_abilities',
        'node_type'=>"grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs'=>[
            'forms' => [
                'name' => 'HeraldistPanel::auth.sections.forms',
                'name_affix'=> "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Heraldist\Models\Form::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                    'manage_components' => [
                        'name' => "AuthPanel::auth.actions.manage_components",
                    ],
                ],
                'subs' => [
                ]
            ],
            "messages" => [
                'name' => 'HeraldistPanel::auth.sections.messages',
                'name_affix'=> "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Heraldist\Models\Message::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'index_contents' => [
                        'name' => "HeraldistPanel::auth.actions.list_inbox",
                    ],
                    'read_message' => [
                        'name' => "HeraldistPanel::auth.actions.read_message",
                    ],
                    'mark_as_unread' => [
                        'name' => "HeraldistPanel::auth.actions.mark_as_unread",
                    ],
                    'download_attachments' => [
                        'name' => "HeraldistPanel::auth.actions.download_attachments",
                    ],
                    'watch_flow_record' => [
                        'name' => "HeraldistPanel::auth.actions.watch_flow_record",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                ],
                'subs' => [
                ]
            ],
            "ebulletin" => [
                'name' => 'HeraldistPanel::auth.sections.ebulletin_lists',
                'name_affix'=> "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Entity\Models\EBulletinUser::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                        'variables' => []
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update"
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                    'export' => [
                        'name' => "MPCorePanel::auth.actions.export2excel",
                    ]
                ],
            ],
        ],
        'variations_title' => "Sitelerin Heraldist Yetkileri",
        'variations' => 'heraldist_variations'
    ]

];
