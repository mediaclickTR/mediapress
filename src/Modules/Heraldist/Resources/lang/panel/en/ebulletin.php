<?php

return [
    'ebulletins' => 'E-Bulletin Registrations',
    'excelall' => 'Transfer all members to excel',
    'name' => 'Name Surname',
    'email' => 'Email',
    'phone' => 'Phone',
    'subscription.date' => 'Subscription Date'
];
