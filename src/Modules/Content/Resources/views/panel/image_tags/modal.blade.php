<div class="modal-header">
    <h5 class="modal-title">{{ $url }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="images m-3">
                <img src="{!! $file !!}" style="max-width: 100%; max-height: 200px">
            </div>
        </div>

        <form id="imageTagDetailForm">
            @csrf
            <input type="hidden" name="id" value="{{$id}}">
            <div class="col-md-12 text-center mt-3">
                @if(count($languages) > 0)
                    <ul class="nav nav-tabs">
                        @foreach($languages as $language)
                            <li>
                                <a class="show {!! $loop->first ? 'active' : '' !!}"
                                   data-toggle="tab"
                                   href="#lang{!! $language['id'] !!}">{!! strtoupper($language['code']) !!}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content pl-0 pr-0 shadow-none">
                        @foreach($languages as $language)
                            <div id="lang{!! $language['id'] !!}"
                                 class="tab-pane fade {!! $loop->first ? 'in active show activess' : '' !!}"
                                 data-id="{!! $language['id'] !!}">
                                <div class="row mt-2">
                                    <div class="form-group row">
                                        <label for="title"
                                               class="col-sm-3 col-form-label">Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control"
                                                   name="details[{!! $language['id'] !!}][title]"
                                                   value="{{ $details[$language['id']]['title'] ?? "" }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alttext"
                                               class="col-sm-3 col-form-label">Tag</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control"
                                                   name="details[{!! $language['id'] !!}][tag]"
                                                   value="{{ $details[$language['id']]['tag'] ?? "" }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" id="imageTagDetailFormBtn" class="btn btn-primary">Kaydet</button>
</div>
