<div class="row">
    @foreach($pages as $page)
        <div class="col-lg-4 col-sm-6 mb-4">
            <div class="card h-100">
                <a href="{!! $page->detail->url !!}">
                    @if($page->f_)
                        @if(is_a($page->f_, 'Illuminate\Database\Eloquent\Collection'))
                            <img class="card-img-top" src="{!! $page->f_->first() !!}" height="200" alt="">
                        @else
                            <img class="card-img-top" src="{!! $page->f_ !!}" height="200" alt="">
                        @endif
                    @else
                        <img class="card-img-top" src="http://placehold.it/200x200" alt="">
                    @endif
                </a>
                <div class="card-body">
                    <div class="card-title">
                        <a href="{!! $page->detail->url !!}">{!! $page->detail->name !!}</a>
                    </div>
                    <p class="card-text small">
                        {!! $page->detail->detail ? \Str::limit(strip_tags($page->detail->detail), 80) : "" !!}
                    </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@if($pages instanceof \Illuminate\Pagination\LengthAwarePaginator )
    {!! $pages->render() !!}
@endif

