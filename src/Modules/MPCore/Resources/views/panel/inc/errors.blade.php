@if($errors->hasBag())
    <div class="alert alert-danger" role="alert">
{{--         {!! dump($errors) !!}--}}
        @foreach ($errors->all() as $key=>$error)
            <div>{!!  $error  !!}

                @if($key==0)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                @endif
            </div>
        @endforeach
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

@if(session()->has('message'))
    @php $body = session()->get('message'); $body = is_array($body) ? implode("\n<br/>",$body) : $body; @endphp
    <div class="alert alert-success alert-dismissible">
        <i class="fa fa-check"></i> {!! $body !!}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('success'))
    @php $body = session()->get('success'); $body = is_array($body) ? implode("\n<br/>",$body) : $body; @endphp
    <div class="alert alert-success alert-dismissible">
        <i class="fa fa-check"></i> {!! $body !!}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('default'))
    @php $body = session()->get('default'); $body = is_array($body) ? implode("\n<br/>",$body) : $body; @endphp
    <div class="alert alert-default alert-dismissible">
        {!! $body !!}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('warning'))
    @php $body = session()->get('warning'); $body = is_array($body) ? implode("\n<br/>",$body) : $body; @endphp
    <div class="alert alert-warning alert-dismissible">
        <i class="fa fa-warning"></i> {!! $body !!}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif

@if(session()->has('error'))
    @php $body = session()->get('error'); $body = is_array($body) ? implode("\n<br/>",$body) : $body; @endphp
    <div class="alert alert-danger alert-dismissible">
        <i class="fa fa-remove"></i> {!! $body !!}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
@endif
