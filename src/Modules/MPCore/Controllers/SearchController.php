<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\BaseController;
use Mediapress\Modules\MPCore\Models\Search;

class SearchController extends BaseController
{
    public $mediapress;
    public $language;
    protected $per_page = 10;
    protected $query = "";

    public function __construct()
    {
        $this->mediapress = mediapress();
        $this->language = $this->mediapress->relation->language;
    }

    protected function search($per_page = 10)
    {
        $this->query = strip_tags(request()->get('q'));

        $this->per_page = $per_page;

        try {
            $result = $this->getSearchContent($this->query);
        } catch (\Exception $exception) {
            $result = abort(422, $exception->getMessage());
        }

        return $result;
    }


    private function getSearchContent($query)
    {
        $results = searchQuery($query, $this->per_page, $this->language->id);
        $results->appends(['q' => request('q')]);
        return $results;
    }

}
