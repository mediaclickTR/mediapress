<?php

namespace Mediapress\Modules\RenderableEditor;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\RenderableEditor\Facades\RenderableEditor;

class RenderableEditorServiceProvider extends ServiceProvider
{
    public const RENDERABLE_EDITOR = "RenderableEditor";
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = self::RENDERABLE_EDITOR;
    protected $namespace= 'Mediapress\Modules\RenderableEditor';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name."Panel");
        $this->publishActions(__DIR__);

        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
        //$this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        //$this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name.'Database');
    }

    public function register()
    {

        $loader = AliasLoader::getInstance();
        $loader->alias(self::RENDERABLE_EDITOR, RenderableEditor::class);

        app()->bind(self::RENDERABLE_EDITOR, function() {
            return new \Mediapress\Modules\RenderableEditor\RenderableEditor;
        });
    }


    protected function mergeConfig($path, $key)
    {

        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }
}
