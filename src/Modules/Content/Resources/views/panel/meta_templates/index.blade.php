@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::sitemap.sitemaps") !!}</div>
            </div>
        </div>

        <div class="p-30">
            <table id="sitemaps-table" class="m-0 table table-striped">
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.row_num") !!}</th>
                    <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    <th>{!! trans("ContentPanel::sitemap.sitemap_type") !!}</th>
                    <th>{!! trans("ContentPanel::sitemap.structure") !!}</th>
                    <th>{!! trans("ContentPanel::sitemap.variations") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($sitemaps as $key=>$sitemap)
                    <tr>
                        <td>{{ $loop->iteration  }}</td>
                        <td>{!! $sitemap->id !!}</td>
                        <td>
                            {!!  implode(" - ", array_filter([($sitemap->detail->name ?? ""),(isset($sitemap->sitemapType) ? '<strong>'.$sitemap->sitemapType->name.'</strong>' : ''),($sitemap->feature_tag ? '<span class="badge badge-primary">'.$sitemap->feature_tag.'</span>' :"")]))  !!}

                        </td>
                        <td style="padding-right: 5px;">
                            <b>{{ isset($sitemap->sitemapType) ? trans("ContentPanel::sitemap.structure.".$sitemap->sitemapType->sitemap_type_type) : '-' }}</b>
                        </td>
                        <td style="border:0px solid #ddd; border-top:none;">
                            {{count($sitemap->details)/*.trans("ContentPanel::sitemap.language_country_group")*/}}
                        </td>
                        <td style="padding-left:5px;">
                            <a onclick="locationOnChange('{!! route('Content.meta_templates.edit',[$sitemap->id, $website->id]) !!}')"
                               href="#" title="{!! trans("MPCorePanel::general.edit") !!}">
                                <span class="fa fa-pen"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="row" style="clear: both;">
            <div class="col-12">
                {!! $sitemaps->links() !!}
            </div>
        </div>
    </div>
@endsection

{{--@push("styles")
    <style>
        .tr td {
            text-align: center !important;
        }
    </style>
@endpush--}}
