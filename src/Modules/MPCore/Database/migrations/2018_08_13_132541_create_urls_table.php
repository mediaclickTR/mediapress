<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

const WEBSITE_ID = 'website_id';
class CreateUrlsTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger(WEBSITE_ID);
            $table->string('url');
            $table->morphs("model");
            $table->string("type", 50)->nullable();
            $table->string("password")->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique([WEBSITE_ID, "url"], "url_unique");
//            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade')
        });

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('dynamic_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('url_id');
            $table->string('regex');
            $table->unsignedInteger(WEBSITE_ID);
            $table->timestamps();
            $table->softDeletes();

            $table->unique([WEBSITE_ID, "url_id"], "dynamic_url_unique");
//            $table->foreign('url_id')->references('id')->on('urls')->onDelete('cascade')
        });

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('redirect_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger(WEBSITE_ID);
            $table->string('url');
            $table->morphs("model");
            $table->timestamps();
            $table->softDeletes();

            $table->unique([WEBSITE_ID, "url"], "redirect_url_unique");
//            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade')
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
       // Schema::dropUnique("url_unique")
       // Schema::dropUnique("dynamic_url_unique")
       // Schema::dropUnique("redirect_url_unique")
        Schema::dropIfExists('urls');
        Schema::dropIfExists('dynamic_urls');
        Schema::dropIfExists('redirect_urls');
        Schema::enableForeignKeyConstraints();
    }
}
