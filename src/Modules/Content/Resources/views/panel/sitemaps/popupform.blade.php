<div class="modal fade" id="staticSitemapEditor" tabindex="-1" role="dialog" aria-labelledby="staticSitemapEditorTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="staticSitemapEditorTitle">{!! __('ContentPanel::sitemap.create_new_page_structure') !!}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="{!! __('ContentPanel::sitemap.close') !!}">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! __('ContentPanel::sitemap.close') !!}</button>
                <button id="saveStaticSitemapBtn" type="button" class="btn btn-primary">{!! __('ContentPanel::sitemap.save') !!}</button>
            </div>
        </div>
    </div>
</div>
