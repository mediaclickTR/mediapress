@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.mailchimp_emailtemplates") !!}</h1>
        <div class="alert alert-info"><i class="fa fa-info"></i> {!! trans("HeraldistPanel::mail_service.mailchimp_template_info") !!}</div>
            @if(count($templates)<=0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                <div class="float-right">
                    <a href="{!! route('Heraldist.mailchimp.mail_templates.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
            @else
                <div class="float-right">
                    <a href="{!! route('Heraldist.mailchimp.mail_templates.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.id") !!}</th>
                        <th>{!! trans("MPCorePanel::general.name") !!}</th>
                        <th>{!! trans("MPCorePanel::general.type") !!}</th>
                        <th>{!! trans("HeraldistPanel::mail_service.template_image") !!}</th>
                        <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                        <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                    </tr>
                    </thead>
                    @foreach($templates as $template)
                        <tr>
                            <td>{!! $template->id !!}</td>
                            <td>{!! $template->name !!}</td>
                            <td>{!! $template->type !!}</td>
                            <td>
                                <div class="images">
                                    <img src="{!! $template->thumbnail !!}" class="lineImage" data-image="{!! $template->thumbnail !!}" alt="">
                                </div>
                            </td>
                            <td>{!! $template->date_created !!}</td>
                            <td>
                                <select id="transaction">
                                    <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                    <option value="https://{!! $dataCenter !!}.Heraldist.mailchimp.com/templates/">{!! trans("HeraldistPanel::mail_service.previewin_mailchimp") !!}</option>
                                    <option value="{!! route('Heraldist.mailchimp.mail_templates.delete',$template->id) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </table>
        @endif
    </div>
@endsection

@push('styles')
    <style>
        .images img {
            height:50px !important;
        }
    </style>
@endpush
@push("scripts")
    <script>
        $('#transaction').change(function(){
            if(this.value == "https://us18.Heraldist.mailchimp.com/templates/"){
                window.open(this.value);
            }else{
                location = this.value;
            }
        });
    </script>
@endpush
