<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Support\Facades\Request;
use Mediapress\Modules\MPCore\Models\SitemapType;

class SitemapTypeController
{
    public const MP_CORE_PANEL_SITEMAP_TYPES_INDEX = "MPCorePanel::sitemap_types.index";

    public function index()
    {
        $sitemap_types = \Mediapress\Modules\Content\Models\SitemapType::get();

        return view(self::MP_CORE_PANEL_SITEMAP_TYPES_INDEX, compact('sitemap_types'));
    }


    public function create()
    {
        return view("MPCorePanel::sitemap_types.create");
    }


    public function edit()
    {
        return view("MPCorePanel::sitemap_types.edit");
    }


    public function store(Request $request)
    {
        dd("store");
        return view(self::MP_CORE_PANEL_SITEMAP_TYPES_INDEX);
    }


    public function update(Request $request)
    {
        dd("update");
        return view(self::MP_CORE_PANEL_SITEMAP_TYPES_INDEX);
    }

    public function delete(Request $request)
    {
        dd("delete");
        return view(self::MP_CORE_PANEL_SITEMAP_TYPES_INDEX);
    }

}