<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePopupLanguagesTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        if (!$schema->hasColumn('popup_languages', 'country_group_id')) {
            $schema->table('popup_languages', function (Blueprint $table) {
                $table->integer('country_group_id')->unsigned()->index();
            });
        }

    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        if (!$schema->hasColumn('popup_languages', 'country_group_id')) {
            $schema->table('popup_languages', function (Blueprint $table) {
                $table->dropColumn('country_group_id');
            });
        }
    }
}
