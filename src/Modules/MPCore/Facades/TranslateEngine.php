<?php

namespace Mediapress\Modules\MPCore\Facades;

use Illuminate\Support\Facades\Facade;

class TranslateEngine extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\MPCore\Foundation\TranslateEngine::class;
    }

}