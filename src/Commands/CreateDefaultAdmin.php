<?php

namespace Mediapress\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mediapress\Modules\Auth\Models\Role;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Modules\Auth\Models\Admin;

class CreateDefaultAdmin extends Command
{
    protected $signature = 'mp:default-admin';

    public $firstName;
    public $lastName;
    public $email;
    public $password;

    public function handle()
    {
        $this->firstName = $this->ask('Adınız');
        $this->lastName = $this->ask('Soyadınız');

        while (true) {
            $email = $this->ask('Giriş yaparken kullanılacak e-posta adresi');

            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->email = $email;
                break;
            } else {
                $this->error('E-posta adresi uygun değil!');
            }
        }

        $this->password = $this->secret('Giriş yaparken kullanılacak şifre nedir');

        $this->createAdmin();

        $this->info(PHP_EOL . 'Admin oluşturuldu. "/mp-admin" üzerinden email ve şifreniz ile giriş yapabilirsiniz.' . PHP_EOL);
    }

    private function createAdmin()
    {
        $data = [
            'role_id' => 2,
            'language_id' => 760,
            'first_name' => trim($this->firstName),
            'last_name' => trim($this->lastName),
            'username' => trim($this->firstName) . ' ' . trim($this->lastName),
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'api_token' => uniqid()
        ];

        $admin = Admin::create($data);


        Bouncer::assign(2)->to($admin);

        \DB::table('permissions')->insert(array(
            array('ability_id' => 1, 'entity_id' => 2, 'entity_type' => Role::class, 'forbidden' => 0)
        ));
    }
}
