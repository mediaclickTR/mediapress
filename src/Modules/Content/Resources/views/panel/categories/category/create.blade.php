@extends('ContentPanel::inc.module_main')
@push('styles')
    <style>
        .cat-list ol li span:not(:first-child) {
            background-image: none;
            font-size: 1.2em;
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::categories.categories")." (".$sitemap->detail->name.")" !!}</div>
            </div>
            <div class="float-right">
                <button class="btn btn-primary btn-sm" onclick="javascript:categoryCreate({{ $sitemap_id }},null, null);"><i
                        class="fa fa-plus"
                        aria-hidden="true"></i> {!! trans("ContentPanel::categories.add_main_category") !!}</button>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.pages.index', ['sitemap_id' => $sitemap->id]) }}" class="btn btn-sm btn-light mr-3">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>

        @if($sitemap->category != 1)
            <div class="alert alert-warning">
                {!! '<b>'.$sitemap->detail->name.'</b>'.trans("ContentPanel::categories.category.not.permission") !!}
            </div>
        @else

            <div class="p-30">
                <div class="mc-md-12">
                    <div class="cat-list pt-4 pb-0">
                        <div id="alert-for-reorder" class="alert alert-warning" role="alert" style="display:none;">
                            <strong>{!! trans("MPCorePanel::general.sorting.alert") !!}</strong>
                            <p>{!! trans("MPCorePanel::general.sorting.change-text") !!}
                                <button class="btn btn-link" type="button"
                                        onclick='javascript:document.getElementById("saveSortingForm").submit();'>{!! trans("MPCorePanel::general.sorting.save-now") !!}
                                </button>
                            </p>
                        </div>
                        @if($data->nestable!="[]")
                            <div class="dd" id="nestable">
                                <div class="panel panel-warning">
                                    <div class="dd span11" id="nestable-menu">
                                        <ol class="dd-list p-0">
                                        </ol>
                                    </div>
                                    {{ Form::open(['route' => ['Content.categories.category.orderSave', $sitemap->id],'id'=>'saveSortingForm']) }}
                                    {!! Form::hidden('nestable_output',$data->nestable) !!}
                                    <div class="float-right">
                                        <button
                                            class="btn btn-primary">{!! trans("MPCorePanel::general.sorting.save") !!}</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @else
                            <div
                                class="alert alert-warning">{!! trans("ContentPanel::categories.not_found_data", ['type'=>$type]) !!}</div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@push('scripts')

    <script src="{{ asset("/vendor/mediapress/js/jquery.bootstrap.wizard.min.js")}}"></script>
    <script src="{{ asset("/vendor/mediapress/js/fm.selectator.jquery.js")}}"></script>
    <script src="{{ asset("/vendor/mediapress/js/icheck.min.js")}}"></script>
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/ContentModule/js/DetailsEngine.js') !!}"></script>

    <script>
        CKEDITOR.disableAutoInline = true;
        //url_engine.setUrl("{{url(route("Content.checkUrl"))}}");

        var metaset = JSON.parse('{{ $metaset }}');

        $(document).ready(function () {


            renewCKEditorInstances('textarea.ckeditor');


            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });


            var details_engine = new DetailsEngine({});
            details_engine.init();

            document.addEventListener('detailNameChange', function (elem) {
                console.log(elem);
            }, false);

        });


        function createMetaVariablesModal(capsule) {
            let modal = $(".main-modal");
            $(".modal-body", modal).html("deneme");
            $(modal).modal("show");
        }


        function pageCategoriesSelectionChanged(page_id, category_ids_arr) {
            leaveCategoriesCriteriasOnly(category_ids_arr);
            leaveCategoriesPropertiesOnly(category_ids_arr);
        }

        function leaveCategoriesCriteriasOnly(category_ids_arr) {
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ url(route("Content.categories.related_criterias")) }}",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    var active_hidden = false;
                    $("#kriter .tablinks").each(function (index) {
                        var criid = $(this).data("cri-id");
                        //console.log(criid);
                        var tabcontent = $("#kriter .tabcontent[data-cri-id=" + criid + "]");
                        if (data.indexOf(criid) > -1) {
                            $(this).removeClass("hidden-cri").show();
                            tabcontent.removeClass("hidden-cri").show();
                            if ($(this).hasClass("active")) {
                                $("#kriter .tablinks").removeClass("active");
                                $(this).addClass("active");
                                $(this).click();
                            }
                        } else {
                            $(this).hide().addClass("hidden-cri");
                            tabcontent.hide().addClass("hidden-cri");
                        }
                    });
                    if ($("#kriter .tablinks").not(".hidden-cri").length) {
                        $("#kriter .tablinks").not(".hidden-cri").first().click();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function leaveCategoriesPropertiesOnly(category_ids_arr) {
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ url(route("Content.categories.related_properties")) }}",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    var active_hidden = false;
                    $("#ozellikler .tablinks").each(function (index) {
                        var propid = $(this).data("prop-id");
                        //console.log(propid);
                        var tabcontent = $("#ozellikler .tabcontent[data-prop-id=" + propid + "]");
                        if (data.indexOf(propid) > -1) {
                            $(this).removeClass("hidden-prop").show();
                            tabcontent.removeClass("hidden-prop").show();
                            if ($(this).hasClass("active")) {
                                $("#ozellikler .tablinks").removeClass("active");
                                $(this).addClass("active");
                                $(this).click();
                            }
                        } else {
                            $(this).hide().addClass("hidden-prop");
                            tabcontent.hide().addClass("hidden-prop");
                        }
                    });
                    if ($("#ozellikler .tablinks").not(".hidden-cri").length) {
                        $("#ozellikler .tablinks").not(".hidden-cri").first().click();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function pageCountryandProvinceSelectionChanged(country_id) {
            getProvinces(country_id);
        }

        function getProvinces(country_id){
            var token = $("meta[name='csrf-token']").attr("content");
            var value = parseInt($('#select-province').children("option:selected").val());

            if($('#select-province').length){
                $.ajax({
                    url: "{{ url(route("MPCore.country_province.get_values")) }}",
                    method: 'GET',
                    data: {country_id: country_id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        $('#select-province').html("");
                        $.each(data, function (ind, el) {
                            if (value === el.id) {
                                $('#select-province').append('<option  value="' + el.id + '" selected>' + el.name + '</option>');
                            } else {
                                $('#select-province').append('<option  value="' + el.id + '">' + el.name + '</option>');
                            }
                        });
                        $('#select-province').selectpicker('refresh');

                    },
                    error: function (xhr, status, errorThrown) {
                        //alert(false);
                    }
                });
            }
        }

        /*function applyForSlug(text_to_slugify) {
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    var active_hidden = false;
                    $("#kriter .tablinks").each(function (index) {
                        var criid = $(this).data("cri-id");
                        //console.log(criid);
                        var tabcontent = $("#kriter .tabcontent[data-cri-id=" + criid + "]");
                        if (data.indexOf(criid) > -1) {
                            $(this).removeClass("hidden-cri").show();
                            tabcontent.removeClass("hidden-cri").show();
                            if ($(this).hasClass("active")) {
                                $("#kriter .tablinks").removeClass("active");
                                $(this).addClass("active");
                                $(this).click();
                            }
                        } else {
                            $(this).hide().addClass("hidden-cri");
                            tabcontent.hide().addClass("hidden-cri");
                        }
                    });
                    if ($("#kriter .tablinks").not(".hidden-cri").length) {
                        $("#kriter .tablinks").not(".hidden-cri").first().click();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }*/


    </script>
    {{--<script src="{!! asset('vendor/mediapress/js/categories.js') !!}"></script>--}}
    <script>
        function renewRootWizard() {
            $('.rootwizard').bootstrapWizard({
                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('.rootwizard').find('.progress-bar').css({width: $percent + '%'});
                }
            });
        }

        function renewDetailsEngine() {

            window.details_engine = new DetailsEngine({});
            window.details_engine.init();
        }

        function renewCKEditorInstances(inSelector) {

            $(inSelector).each(function (key, item) {
                CKEDITOR.replace($(this).attr('id'),
                  {
                      language: 'tr',
                      //extraPlugins: 'bol,kopyala',allow_diskkeys%5B0%5D=azure&allow_diskkeys%5B1%5D=local&
                      filebrowserImageBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Görsel',
                      filebrowserBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Dosya',
                      allowedContent: {
                          script: true,
                          div: true,
                          $1: {
                              // This will set the default set of elements
                              elements: CKEDITOR.dtd,
                              attributes: true,
                              styles: true,
                              classes: true
                          }
                      },
                      entities: false,
                      entities_latin: false,
                      removeDialogTabs: null,//'image:advanced;link:advanced',
                      allowedContent: false,
                      pasteFromWordPromptCleanup: true,
                      pasteFromWordRemoveFontStyles: true,
                      forcePasteAsPlainText: true,
                      ignoreEmptyParagraph: true,
                      toolbar: [
                          {
                              name: 'clipboard',
                              groups: ['clipboard', 'undo'],
                              items: ['PasteFromWord', '-', 'Undo', 'Redo']
                          },
                          {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace']},
                          //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                          {
                              name: 'paragraph',
                              groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                              items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                          },
                          {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                          {name: 'links', items: ['Link', 'Unlink']},
                          '/',
                          {
                              name: 'basicstyles',
                              groups: ['basicstyles', 'cleanup'],
                              items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                          },
                          {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                          {name: 'colors', items: ['TextColor', 'BGColor']},
                          {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                          {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                      ]
                  });

                CKEDITOR.config.extraPlugins = 'justify,iframe,indentblock,indent,textindent';

            });
        }

        function renewICheck(selector) {
            $(selector).iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        }

        function renewOnOffControls(){
            $('.on-off ul li:last-child').click(function () {
                $(this).prev().find('.switch-toggle').addClass('on');
                $(this).prev().find('.switch-toggle').removeClass('off');
                $(this).prev().find("input").val($(this).prev().find('.switch-toggle').data('on')).prop("checked", true).trigger("change");
            });
            $('.on-off ul li:first-child').click(function () {
                $(this).next().find('.switch-toggle').addClass('off');
                $(this).next().find('.switch-toggle').removeClass('on');
                $(this).next().find("input").val($(this).next().find('.switch-toggle').data('off')).prop("checked", true).trigger("change");
            });
        }

        function renewSlugControlV2(){
            $(".slug-control-v2").each(function (ind, el) {
                var slugger = new DetailSlugControlV2(el);
            });
        }

        function categoryCreate(sitemap_id, parent_id) {
            popup('categoryCreate', {sitemap_id, parent_id});
        }

        function categoryEdit(sitemap_id, edit_id) {
            popup('categoryCreate', {edit_id});
        }

        function categoryCriteriasEdit(sitemap_id, parent_id) {
            popup('categoryCriteriasEdit', {sitemap_id, parent_id});

        }

        function categoryPropertiesEdit(sitemap_id, parent_id) {
            popup('categoryPropertiesEdit', {sitemap_id, parent_id});
        }

        $(document).ready(function () {

            $(".main-modal .modal-dialog").addClass("modal-lg").parent().on('shown.bs.modal', function () {
                renewRootWizard();
                renewCKEditorInstances('.main-modal textarea.ckeditor');
                renewICheck('.main-modal .checkbox');
                renewDetailsEngine();
                renewSlugControlV2();
                renewOnOffControls();
            });

            $(function () {
                var obj = {!! $data->nestable !!};
                var output = '';
                $.each(obj, function (index, item) {
                    output += buildItem(item);
                });
                $('#nestable-menu').nestable().on('change', function () {
                    $('[name=nestable_output]').val(JSON.stringify($('#nestable-menu').nestable('serialize')));
                    $('div#alert-for-reorder').slideDown(150);
                })
                $(".dd-list").append(output);
                $("#nestable_output").val(obj);
                @if(request()->get('quickAccess'))
                      $('li.dd-item[data-id="{{request()->get('quickAccess')}}"] > span:eq(3)').trigger('click');
                @endif
            });

            var statusHold = {
                1: {
                    'color': "#28a745",
                    'text': "{{ trans('MPCorePanel::general.status-titles.active') }}"
                },
                0: {
                    'color': "#dc3545",
                    'text': "{{ trans('MPCorePanel::general.status-titles.passive') }}"
                },
                2: {
                    'color': "#ffc107",
                    'text': "{{ trans('MPCorePanel::general.status-titles.draft') }}"
                }
            }

            function buildItem(item) {
                var html = "<li class='dd-item' data-id='" + item.id + "'>";
                html += "<div class='AllRight'><span style=\"float: right !important;text-align:center;\" title=\"{{ trans('MPCorePanel::general.category-icon-title.criteria') }}\" onclick=\"javascript:categoryCriteriasEdit({{ $sitemap_id }}," + item.id + ", $(this));\" class=\"cat-edit-criterias cat-action\"><i class=\"fa fa-tasks\"></i></span>"
                    + "<span style=\"float: right !important; text-align:center;\" title=\"{{ trans('MPCorePanel::general.category-icon-title.property') }}\" onclick=\"javascript:categoryPropertiesEdit({{ $sitemap_id }}," + item.id + ", $(this));\" class=\"cat-edit-properties cat-action\"><i class=\"fa fa-list\"></i></span>"
                    + "<span style=\"float: right !important; text-align:center;\" title=\"{{ trans('MPCorePanel::general.category-icon-title.delete') }}\" onclick=\"categoryDelete(" + item.id + ",$(this));\" class=\"cat-delete cat-action\"><i class=\"fa fa-trash\"></i></span>"
                    + "<span style=\"float: right !important; text-align:center;\" title=\"{{ trans('MPCorePanel::general.category-icon-title.edit') }}\"  onclick=\"javascript:categoryEdit({{ $sitemap_id }}," + item.id + ", $(this));\"><i class=\"fa fa-edit\"></i></span>"
                    + "<span style=\"float: right !important; text-align:center;\" title=\"{{ trans('MPCorePanel::general.category-icon-title.sub-category') }}\" onclick=\"javascript:categoryCreate({{ $sitemap_id }}," + item.id + ", $(this));\" class=\"cat-edit-criterias cat-action\"><i class=\"fa fa-plus\"></i></span>"
                    + "<span style=\"float: right !important; font-weight: 600; font-size: 13px; color: "+ statusHold[item.status].color +" !important;\" class=\"cat-action mr-5\">"+ statusHold[item.status].text +"</span></div>"
                  + "<div class='dd-handle'>" + item.detail.name + "</div>";
                if (item.children) {
                    html += "<ol class='dd-list'>";
                    $.each(item.children, function (index, sub) {
                        html += buildItem(sub);
                    });
                    html += "</ol>";
                }
                html += "</li>";
                return html;
            }

            $("html").delegate(".tab-list ul li a i.fa-plus", "click",function () {
                $(this).parent().toggleClass('disabled');
                $(this).toggleClass('fa-plus fa-close text-danger text-primary');
                var href = $(this).closest("a").attr("href");
                var detail_wrapper = $(href);
                restoreDetail(detail_wrapper);
            });

            $("html").delegate(".tab-list ul li a i.fa-close", "click", function () {
                $(this).parent().toggleClass('disabled');
                $(this).toggleClass('fa-close fa-plus text-danger text-primary');
                var href = $(this).closest("a").attr("href");
                var detail_wrapper = $(href);
                deleteDetail(detail_wrapper);
            });
        });

        function categoryDelete(id, element) {

            swal({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ trans("MPCorePanel::general.yes") }}',
                cancelButtonText: '{{ trans("MPCorePanel::general.cancel") }}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {

                if (isConfirm.value) {

                    swal({
                        title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                        text: '{{ trans("MPCorePanel::general.are_you_sure_sub_desc") }}',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.yes") }}',
                        cancelButtonText: '{{ trans("MPCorePanel::general.cancel") }}',
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }).then(function (isConfirm) {

                        if (isConfirm.value) {
                            deleteFinal(id, element, 1);
                        } else {
                            deleteFinal(id, element, 0);
                        }
                    });

                } else {
                    // İşlem ilk öneride iptal edildi
                }

            });

        }

        function deleteFinal(id, element, sub) {
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ route('Content.categories.category.delete') }}",
                method: 'POST',
                data: {id: id, sub: sub, _token: token},
                success: function (data) {
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    });
                    element.parent().remove();
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        }

        function tabForward(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#' + id + '"]').parent();
            active.next().find('a').trigger('click');
        }

        function tabBack(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#' + id + '"]').parent();
            active.prev().find('a').trigger('click');
        }

        function deleteDetail(detail_wrapper){
            var detail_type = $(detail_wrapper).attr("data-detail-type");
            var detail_id = $(detail_wrapper).attr("data-detail-id");

            $.ajax({
                'type': 'POST',
                'url': '{{ route("Content.deleteDetail") }}',
                'data': {
                    '_token': "{{ csrf_token() }}",
                    'detail_type' : detail_type,
                    'detail_id' : detail_id,
                },
                success: function (result) {
                    var selector = '.has-detail-in[data-detail-type="' + detail_type.replace(/\\/g, '\\\\') + '"][data-detail-id="' + detail_id + '"]';
                    //$(selector).addClass("remove-on-submit").addClass("formstorer-ignore-tree").hide();

                    $(selector).each(function(ind, el){
                        $(el).addClass("remove-on-submit").addClass("formstorer-ignore-tree").hide();
                        formstorerIgnoreTree(el);
                        var id = $(el).attr("id");
                        $(el).closest(".details-wrapper").find('[href="#'+id+'"]').addClass("disabled").closest(".details-wrapper").find("ul li:first a:not(.disabled)").click();
                    });

                    console.log(selector,$(selector).length);
                },
                error: function (result) {
                    //$('.has-detail-in[data-detail-type="' + detail_type + '"][data-detail-id="' + detail_id + '"]').removeClass("remove-on-submit");
                }
            });
        }

        function restoreDetail(detail_wrapper){

            var detail_type = $(detail_wrapper).attr("data-detail-type");
            var detail_id = $(detail_wrapper).attr("data-detail-id");

            $.ajax({
                'type': 'POST',
                'url': '{{ route("Content.restoreDetail") }}',
                'data': {
                    '_token': "{{ csrf_token() }}",
                    'detail_type' : detail_type,
                    'detail_id' : detail_id,
                },
                success: function (result) {
                    var selector = '.has-detail-in[data-detail-type="' + detail_type.replace(/\\/g, '\\\\') + '"][data-detail-id="' + detail_id + '"]';
                    $(selector).each(function(ind,el){
                        $(el).removeClass("remove-on-submit").removeClass("formstorer-ignore-tree").show();
                        formstorerUnignoreTree(el);
                        var id = $(el).attr("id");
                        $(el).closest(".details-wrapper").find('[href="#'+id+'"]').removeClass("disabled");
                    });
                },
                error: function (result) {
                    //$('.has-detail-in[data-detail-type="' + detail_type + '"][data-detail-id="' + detail_id + '"]').addClass("remove-on-submit");
                }
            });
        }

        function formstorerIgnoreTree(tree_root_el){
            var form = $(tree_root_el).closest("form");
            $("input, select, textarea", tree_root_el).each(function(input_ind, input_el){
                let input_name, selector;
                input_name = $(input_el).attr("name");
                if(input_name){
                    selector = 'input.formstorer_ignore_field[value="'+input_name+'"]';
                    if( ! $(selector, form).length){
                        let ignore_input = $('<input type="hidden" class="formstorer_ignore_field" name="formstorer_ignore_fields[]"/>');
                        $(ignore_input).val(input_name);
                        $(form).prepend(ignore_input);
                    }
                }
            });
        }

        function formstorerUnignoreTree(tree_root_el){
            var form = $(tree_root_el).closest("form");
            $("input, select, textarea", tree_root_el).each(function(input_ind, input_el){
                let input_name, selector;
                input_name = $(input_el).attr("name");
                if(input_name){
                    selector = 'input.formstorer_ignore_field[value="'+input_name+'"]';
                    $(selector, form).remove();
                }
            });

        }


    </script>
@endpush

@push('styles')
    <style>
        .dd {
            max-width: 100% !important;
        }

        .dd-item span {
            color: #333 !important;
        }

        .page .page-content .content .cat-list ol li span {
            background: none;
        }

        .page .page-content .content .cat-list ol li .dd-handle {
            padding: 15px 100px;
        }
    </style>
@endpush
