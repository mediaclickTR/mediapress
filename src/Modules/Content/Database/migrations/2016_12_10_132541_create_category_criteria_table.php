<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryCriteriaTable extends Migration
{

    public const CATEGORY_CRITERIA = 'category_criteria';

    public function up()
    {
        if (!Schema::hasTable(self::CATEGORY_CRITERIA)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CATEGORY_CRITERIA, function (Blueprint $table) {
                $table->integer('category_id')->unsigned();
                $table->integer('criteria_id')->unsigned();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CATEGORY_CRITERIA);
    }
}
