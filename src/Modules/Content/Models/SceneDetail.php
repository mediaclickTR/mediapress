<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\MPCore\Models\BaseModel;

class SceneDetail extends BaseModel
{
    use SoftDeletes;

    public const ARRAY1 = 'array';
    protected $table = "scene_details";

    public $timestamps = true;

    protected $dates = ["deleted_at"];

    public $fillable = [
        'scene_id',
        'language_id',
        'country_group_id',
        'texts',
        'buttons',
        'files',
        'url',
        'video_url',
    ];
    public $casts = [
        'texts' => self::ARRAY1,
        'buttons' => self::ARRAY1,
        'files' => self::ARRAY1,
        'url' => self::ARRAY1,
    ];


}
