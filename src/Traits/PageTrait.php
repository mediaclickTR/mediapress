<?php

namespace Mediapress\Traits;

use Mediapress\Exceptions\NotFoundException;
use Mediapress\Foundation\Mediapress;
use Mediapress\Models\CategoryDetail;
use Mediapress\Models\Language;
use Mediapress\Models\PageDetail;

trait PageTrait
{

    public $method = 'mainPage';

    public function decide(Mediapress $mediapress)
    {
        if ($mediapress->relation instanceof PageDetail) {
            if ((isset($this->pass) && $this->pass != 1) || !isset($this->pass)) {
                if (!isset($mediapress->relation->page->id)) {
                    throw new NotFoundException('Not Found');
                }
            }
            $this->method = 'pageDetail';
        } elseif ($mediapress->relation instanceof CategoryDetail) {
            if ((isset($this->pass) && $this->pass != 1) || !isset($this->pass)) {
                if (!isset($mediapress->relation->category->id)) {
                    throw new NotFoundException('Not Found');
                }
            }
            $this->method = 'category';
        }

     /*   $detail = $mediapress->sitemap->detail;
        if ($detail->category_slug !=$detail->slug && $detail->category_slug != '/' && $mediapress->requestUrl == $detail->slug) {
            return redirect(url($detail->category_slug))->setStatusCode(301);
        }*/
        return $this->{$this->method}($mediapress);
    }
}