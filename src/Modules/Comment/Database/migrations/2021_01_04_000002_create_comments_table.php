<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('comment_structure_id');
            $table->integer('parent_id')->nullable();
            $table->tinyInteger("status")->default(2);
            $table->string('model_class');
            $table->integer('model_id');
            $table->text("comment");
            $table->integer('user_id')->nullable();
            $table->string('fullname');
            $table->string('email');
            $table->string('ip');
            $table->text('user_agent');
            $table->tinyInteger('read')->default(2);
            $table->string("cvar")->nullable();
            $table->text("ctex")->nullable();
            $table->integer("cint")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
