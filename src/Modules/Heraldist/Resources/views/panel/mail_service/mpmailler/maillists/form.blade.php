@csrf
@if(isset($maillist))
    <input type="hidden" name="id" value="{!! $maillist->id !!}">
@endif

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
@endpush

<div class="form-group rel">
    <label>{!! trans("EntityPanel::entitylist.select_websites") !!}</label><br/><br/>
    {!!Form::select('website_id[]', $websites,isset($maillist) ? $maillist->websites->pluck("id") : session()->get('panel.website')->id, ['class' => 'multiple','multiple', 'required'])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.maillist_name") !!}</label>
    <input type="text" name="name" value="{{ old('name',  isset($maillist->name) ? $maillist->name : null) }}">
</div>
<div class="form-group rel focus">
    <label for="">{!! trans("HeraldistPanel::mail_service.email_select") !!}</label><br/>
    <select id='pre-selected-options' class="form-control" name="emails[]" multiple='multiple'>
        @foreach($data['table_names'] as $id => $row)
            @foreach($data['rows'] as $key=>$value)
                <optgroup label="{!! $row !!}">
                    @if($row==$value['name'])
                        <option @if(isset($maillist)) @if(in_array($value['email'],$emails)) selected @endif @endif value="{!! $value['email'] !!}-{!! $value['username'] !!}">{!! $value['email'] !!}</option>
                    @endif
                </optgroup>
            @endforeach
        @endforeach
    </select>
</div>
<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>
@push('scripts')
    <script type="application/javascript">
        $('#pre-selected-options').multiSelect({
            selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
            selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
        });
    </script>
@endpush

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function() {
            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
        });
    </script>
@endpush