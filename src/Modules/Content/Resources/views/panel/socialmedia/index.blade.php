@extends('ContentPanel::inc.module_main')
@php
    $country_group_id= $website->countryGroup->id;
    $default_lang_id= $website->defaultLanguage()->id;
@endphp
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {{ __('ContentPanel::social_media.social_media') }}
                </div>
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Content.socialmedia.create') !!}" class="btn btn-primary btn-sm">
                    <i class="fas fa-plus"></i>
                    {{ __('ContentPanel::general.add_new') }}
                </a>
            </div>
        </div>


        @foreach($groups as $socialmedia)
            <div class="p-30">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">{{ __('ContentPanel::general.status') }}</th>
                        <th scope="col">{{ __('ContentPanel::general.order') }}</th>
                        <th scope="col">{{ __('ContentPanel::social_media.link') }}</th>
                        <th scope="col">{{ __('ContentPanel::general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($socialmedia as $social)
                        <tr>
                            <th scope="row"><i class="fab fa-{!! $social->name !!}" style="font-size: 25px;"></i></th>
                            <td class="status">
                                @if($social->status == 1)
                                    <i class="active" style="background: #7fcc46"></i>{{ __('ContentPanel::general.status_title.active') }}
                                @else
                                    <i class="passive" style="background: #dc3545"></i>{{ __('ContentPanel::general.status_title.passive') }}
                                @endif
                            </td>
                            <td>
                                {{ $social->order }}
                            </td>
                            @if($social->link[$country_group_id][$default_lang_id])
                                <td>
                                    <a href="{{ $social->link[$country_group_id][$default_lang_id]['link'] }}">
                                        <u>
                                            {!!  $social->link[$country_group_id][$default_lang_id]['link'] !!}
                                        </u>
                                    </a>
                                </td>
                            @endif
                            <td>
                                <a onclick="locationOnChange('{!! route('Content.socialmedia.edit',$social->id) !!}')"
                                   href="#" title="{!! __("ContentPanel::general.edit") !!}"><span
                                            class="fa fa-pen"></span></a>
                                <a onclick="locationOnChange('{!! route('Content.socialmedia.delete',$social->id) !!}')"
                                   href="#" title="{!! __("ContentPanel::general.delete") !!}"><span
                                            class="fa fa-trash"></span></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
@endsection
