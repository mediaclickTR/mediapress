@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>Ülke Grupları</h1>
        <div class="col-md-12 pa">
            @if( isset($renderable_object) && is_a($renderable_object, \Mediapress\AllBuilder\Foundation\BuilderRenderable::class))
                @php $renderable_object->render(); @endphp
                @php
                    $push = $renderable_object->getStacks();
                @endphp
                @foreach($push as $stack => $p)
                    @push($stack) {!! implode("\n",$p) !!} @endpush
                @endforeach
            @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(e){






        });
    </script>
@endpush