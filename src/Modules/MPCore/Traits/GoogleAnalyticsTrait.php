<?php

namespace Mediapress\Modules\MPCore\Traits;

use Analytics;
use Illuminate\Support\Arr;
use Mediapress\Modules\Content\Models\Website;
use function GuzzleHttp\Psr7\build_query;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Mediapress\Modules\MPCore\Foundation\Period;
use Illuminate\Support\Facades\Cache;

trait GoogleAnalyticsTrait
{
    public function analyticsInit()
    {

        $website =  session('panel.website');
        if(!$website){
            $website = Website::find(request()->id);
        }
        if(!$website){
            $website = Website::first();
        }
        $this->account_id = 0;
        $this->access_token = 0;
        $account_id = SettingSun::where("key", "google.analytics.account.id")->where("website_id",
            $website->id)->first();
        $access_token = SettingSun::where("key", "google.analytics.access.token")->where("website_id",
           $website->id)->first();
        if ($account_id && $access_token) {
            $this->account_id = $account_id->value;
            $this->access_token = $access_token->value;
            $expire_date = SettingSun::where("key", "google.analytics.expire.date")->where("website_id",
                $website->id)->first();

            if (time() > $expire_date->value) {

                $refresh_token = SettingSun::where("key", "google.analytics.refresh.token")->where("website_id",
                    $website->id)->first();
                if (isset($refresh_token)) {
                    $this->refreshOrRegisterAuth($refresh_token->value);
                }
            }
        }

        if ($this->access_token !== 0) {
            return true;
        }
        return false;
    }

    public function refreshOrRegisterAuth($refresh_token)
    {
        $data = refresh_token(config('google_analytics.client_id',
            "238284199418-701prjoophrmeimkm6ptp4uck6ecbv9a.apps.googleusercontent.com"),
            config('google_analytics.client_secret', "ne_BBDBvFEphQJO3i7Dm-uLZ"), $refresh_token);
        $content = (
        new \GuzzleHttp\Client())
            ->request('GET',
                'https://www.googleapis.com/analytics/v3/management/accountSummaries?access_token='.$data->access_token)
            ->getBody()->getContents();

        $contents = json_decode($content, true);
        if ($contents) {
            foreach ($contents['items'] as $item) {
                foreach ($item['webProperties'] as $webProperty) {
                    if (strpos($webProperty['websiteUrl'], str_replace('www.', '', request()->getHost())) !== false) {
                        $account_id = end($webProperty['profiles'])['id'];
                    }
                }
            }
        }
        if (isset($account_id)) {
            $expire_date = time() + $data->expires_in;
            $access_token = $data->access_token;

            SettingSun::updateOrCreate([
                'key' => "google.analytics.expire.date", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Refresh Token Yenileme Zamanı",
                "value" => $expire_date
            ]);

            SettingSun::updateOrCreate([
                'key' => "google.analytics.access.token", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Access Token",
                "value" => $access_token
            ]);

            SettingSun::updateOrCreate([
                'key' => "google.analytics.refresh.token", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Refresh Token",
                "value" => $refresh_token
            ]);

            SettingSun::updateOrCreate([
                'key' => "google.analytics.account.id", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Account ID",
                "value" => $account_id
            ]);

            Cache::flush();

            return true;
        } else {
            $expire_date = time() + $data->expires_in;
            $access_token = $data->access_token;
            SettingSun::updateOrCreate([
                'key' => "google.analytics.expire.date", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Refresh Token Yenileme Zamanı",
                "value" => $expire_date
            ]);

            SettingSun::updateOrCreate([
                'key' => "google.analytics.access.token", "website_id" => session("panel.website")->id
            ], [
                "group" => "Analytics",
                "title" => "Google Access Token",
                "value" => $access_token
            ]);
            return false;
        }
    }

    public function analyticsPerformQuery($access_token, $account_id, $period, $param, $data = 'data/ga')
    {
        $params = build_query(array_merge([
            'ids' => 'ga:'.$account_id, 'access_token' => $access_token,
            'start-date' => $period->startDate->format("Y-m-d"), 'end-date' => $period->endDate->format("Y-m-d")
        ], $param));

        $data = (new \GuzzleHttp\Client())->request('GET', 'https://www.googleapis.com/analytics/v3/'.$data.'?'.$params)
            ->getBody()->getContents();
        $data = json_decode($data, true);

        if (isset($data["columnHeaders"])) {
            $dd = Arr::pluck($data["columnHeaders"], "name");
            if (isset($data['rows'])) {
                array_walk($data["rows"], function (&$i) use ($dd) {
                    $i = array_combine($dd, $i);
                });
            } else {
                $data['rows'] = [];
            }
        }

        return Arr::only($data, ["rows", "totalResults", "totalsForAllResults"]);
    }

    public function adapterFetchVisitorsAndPageViews($days)
    {

        $data = $this->fetchVisitorsAndPageViews($days)["rows"];
        $rows = collect($data)->groupBy("ga:date");

        $page_views = $rows->map(function ($row) {
            return $row->sum('ga:pageviews');
        });

        $user_views = $rows->map(function ($row) {
            return $row->sum('ga:users');
        });

        $max_pageviews = $page_views->max();
        $max_userviews = $user_views->max();

        if ($max_pageviews > $max_userviews) {
            $max = $max_pageviews;
        } else {
            $max = $max_userviews;
        }

        $max *= 2;

        return [
            "pageviews" => $page_views,
            "userviews" => $user_views,
            "max" => $max,
        ];
    }


    public function adapterGetPageAnalytics($days)
    {

        $data = $this->getPageAnalytics($days)["rows"];
        $rows = collect($data)->groupBy("ga:date");
        $page_titles = collect($data)->groupBy("ga:pageTitle");

        $page_views = $rows->map(function ($row) {
            return $row->sum('ga:pageviews');
        });

        $unique_pageviews = $rows->map(function ($row) {
            return $row->sum('ga:uniquePageviews');
        });

        $unique_pageviews = $rows->map(function ($row) {
            return $row->sum('ga:uniquePageviews');
        });

        $page_titles_unique_views = $page_titles->map(function ($row) {
            return $row->sum('ga:uniquePageviews');
        });

        $max_pageviews = $page_views->max();
        $unique_pageviews = $unique_pageviews->max();

        if ($max_pageviews > $unique_pageviews) {
            $max = $max_pageviews;
        } else {
            $max = $unique_pageviews;
        }

        $max *= 2;

        return [
            "pageviews" => $page_views,
            "uniquePageviews" => $unique_pageviews,
            "max" => $max,
            "rows" => $rows,
            "page_titles_unique_views" => $page_titles_unique_views
        ];
    }

    public function fetchVisitorsAndPageViews($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.fetchVisitorsAndPageViews'.$days, function () use ($period) {
            return $this->analyticsPerformQuery(
                $this->access_token,
                $this->account_id,
                $period,
                [
                    'metrics' => 'ga:users,ga:pageviews',
                    'dimensions' => 'ga:date,ga:pageTitle',
                ]
            );
        });
    }

    public function getUsers($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getUsers'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:users',
                    ]
                );
        })['totalsForAllResults']['ga:users'];
    }


    public function getCounts($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getCounts'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:users,ga:sessions,ga:pageviews',
                    ]
                );
        })['totalsForAllResults'];
    }

    public function getDailyResults($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getDailyResults'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:users,ga:sessions,ga:pageviews',
                        'dimensions' => 'ga:date'

                    ]
                );
        })['rows'];
    }

    public function getDeviceData($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getDeviceData'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:deviceCategory',
                        'sort' => '-ga:pageviews'
                    ]
                );
        })['rows'];
    }

    public function getOsData($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getOsData'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:operatingSystem',
                        'sort' => '-ga:pageviews',
                    ]
                );
        })['rows'];
    }

    public function getBrowserData($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getBrowserData'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:browser',
                        'sort' => '-ga:pageviews'
                    ]
                );
        })['rows'];
    }

    public function getMostVisited($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getMostVisited'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:pagePath',
                        'sort' => '-ga:pageviews',
                        'max-results' => 10
                    ]
                );
        })['rows'];
    }

    public function getAges($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getAges'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:userAgeBracket',
                        'sort' => 'ga:userAgeBracket',
                        'max-results' => 10
                    ]
                );
        })['rows'];
    }

    public function getAffinity($days)
    {
        $period = Period::days($days);
        $data = Cache::rememberForever('google.analytics.getSocial'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:sessions',
                        'dimensions' => 'ga:interestAffinityCategory',
                        'sort' => '-ga:sessions',
                        'max-results' => 10
                    ]
                );
        })['rows'];

        $array = [];
        $total = array_sum(Arr::pluck($data, 'ga:sessions'));


        foreach ($data as $value) {

            $array[] = [
                'key' => $value['ga:interestAffinityCategory'],
                'value' => (int) $value['ga:sessions'],
                'percent' => $value['ga:sessions'] / $total * 100,
            ];

        }
        return $array;
    }

    public function getGenders($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getGenders'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:userGender',
                        'sort' => '-ga:userGender',
                        'max-results' => 10
                    ]
                );
        })['rows'];
    }

    public function getAvgLoadTime($days)
    {
        $period = Period::days($days);
        $data = Cache::rememberForever('google.analytics.getAvgLoadTime'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:avgPageLoadTime',

                    ]
                );
        })['totalsForAllResults']['ga:avgPageLoadTime'];

        return number_format($data, 2);
    }

    public function getBounces($days)
    {
        $period = Period::days($days);
        $data = Cache::rememberForever('google.analytics.getBounces'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:bounceRate',

                    ]
                );
        })['totalsForAllResults']['ga:bounceRate'];

        return number_format($data, 2);
    }

    public function getAvgVisitTime($days)
    {
        $period = Period::days($days);
        $data = (int) Cache::rememberForever('google.analytics.getAvgVisitTime'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:avgSessionDuration',

                    ]
                );
        })['totalsForAllResults']['ga:avgSessionDuration'];

        $sec = $data % 60;
        $min = ($data - $sec) / 60;
        return str_pad($min, 2, 0, STR_PAD_LEFT).':'.str_pad($sec, 2, 0, STR_PAD_LEFT);

    }

    public function getHourly($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getHourly'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews',
                        'dimensions' => 'ga:dayOfWeek,ga:hour',
                    ]
                );
        })['rows'];
    }

    public function getOnline()
    {
        $period = Period::days(1);
        // return Cache::rememberForever('google.analytics.getHourly'.$days, function () use ($period) {
        return
            $this->analyticsPerformQuery(
                $this->access_token,
                $this->account_id,
                $period,
                [
                    'metrics' => 'rt:activeUsers',
                    //  'dimensions' => 'ga:dayOfWeek,ga:hour',
                ],
                'data/realtime'
            )['totalsForAllResults']['rt:activeUsers'];
        //  })['rows'];
    }

    public function getUniquePageViews($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getUniquePageViews'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:uniquePageviews',
                    ]
                );
        });
    }

    public function getTotalViews($days)
    {
        $period = Period::days($days);

        return Cache::rememberForever('google.analytics.getTotalViews'.$days, function () use ($period) {
            return $this->analyticsPerformQuery(
                $this->access_token,
                $this->account_id,
                $period,
                [
                    'metrics' => 'ga:pageviews',
                ]
            );
        })['totalsForAllResults']['ga:pageviews'];
    }

    public function getOrganicSearch($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getOrganicSearch'.$days, function () use ($period) {
            return $this->analyticsPerformQuery(
                $this->access_token,
                $this->account_id,
                $period,
                [
                    'metrics' => 'ga:pageviews,ga:organicSearches',
                    'dimensions' => 'ga:pageTitle,ga:medium'
                ]
            );
        });
    }

    public function getPageAnalytics($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getPageAnalytics'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:pageviews, ga:uniquePageviews, ga:timeOnPage,ga:users',
                        'dimensions' => 'ga:date,ga:pageTitle',
                    ]
                );
        });
    }

    public function getTrafficSources($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getTrafficSourcesData'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:sessions',
                        'dimensions' => 'ga:source',
                        'sort' => '-ga:sessions',
                        'max-results' => 10
                    ]
                );
        })['rows'];
    }

    public function getSessionCountries($days)
    {
        $period = Period::days($days);
        $data = Cache::rememberForever('google.analytics.getSessionCountrieswithISO'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:sessions',
                        'dimensions' => 'ga:countryIsoCode,ga:country',
                        'sort' => '-ga:sessions'
                    ]
                );
        })['rows'];

        $array = [];
        foreach ($data as $value) {
            $array[] = [
                'code' => strtoupper($value['ga:countryIsoCode']),
                'value' => (int) $value['ga:sessions'],
                'name' => $value['ga:country'],
            ];
        }
        return $array;
    }

    public function getBrowserAndSystems($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getBrowserAndSystems'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:sessions',
                        'dimensions' => 'ga:operatingSystem,ga:operatingSystemVersion,ga:browser',
                        'sort' => '-ga:sessions'
                    ]
                );
        });
    }

    public function getSearchEngineKeywords($days)
    {
        $period = Period::days($days);
        return Cache::rememberForever('google.analytics.getSearchEngineKeywords'.$days, function () use ($period) {
            return
                $this->analyticsPerformQuery(
                    $this->access_token,
                    $this->account_id,
                    $period,
                    [
                        'metrics' => 'ga:sessions, ga:users',
                        'dimensions' => 'ga:keyword',
                        'sort' => '-ga:sessions'
                    ]
                );
        });
    }
}
