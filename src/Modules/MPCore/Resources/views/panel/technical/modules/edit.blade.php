@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("MPCorePanel::module.edit") !!}</h1>
        <div class="col-md-12 pa">
           @include("MPCorePanel::technical.modules.form")
        </div>
    </div>
@endsection