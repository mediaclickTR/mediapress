<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    public const MESSAGES = 'messages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable(self::MESSAGES)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::MESSAGES, function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('form_id')->nullable();
                $table->string('name',150)->nullable();
                $table->string('subject')->nullable();
                $table->string('email',150)->nullable();
                $table->string('phone', 150)->nullable();
                $table->string('ip')->nullable();
                $table->text('data');
                $table->integer('read')->default('0');
                $table->integer('file')->default('0');
                $table->integer('deleted')->default('0');

                $table->text('ctex_1')->nullable();
                $table->text('ctex_2')->nullable();

                $table->string('cvar_1')->nullable();
                $table->string('cvar_2')->nullable();
                $table->string('cvar_3')->nullable();
                $table->string('cvar_4')->nullable();
                $table->string('cvar_5')->nullable();

                $table->integer('cint_1')->nullable();
                $table->integer('cint_2')->nullable();
                $table->integer('cint_3')->nullable();
                $table->integer('cint_4')->nullable();
                $table->integer('cint_5')->nullable();

                $table->decimal('cdec_1')->nullable();
                $table->decimal('cdec_2')->nullable();

                $table->date('cdat_1')->nullable();
                $table->date('cdat_2')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::MESSAGES);
        Schema::enableForeignKeyConstraints();
    }
}
