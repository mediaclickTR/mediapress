@extends('AuthPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{ __('AuthPanel::profile.page_title') }}</div>

            </div>
        </div>
        <div class="p-30">
            <div class="row">
                <div class="col-md-12">
                    <div class="step-tabs rootwizard mb-4">
                        <div class="navbar">
                            <div class="navbar-inner">
                                <div>
                                    <ul class="nav nav-pills">
                                        <li class="active">
                                            <a href="#information" role="tab" data-toggle="tab" class="active show">
                                                {{ __('AuthPanel::profile.information') }}
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#security" data-toggle="tab" class="">
                                                {{ __('AuthPanel::profile.security') }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::model($user, ['route' => ['Auth.profile.update'], 'method' => 'POST']) }}
                    @csrf
                    {!! Form::hidden("id",$user->id) !!}
                    <div class="tab-content pb-0">
                        <div id="information"role="tabpanel" class="tab-pane fade in active show">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{!! trans("AuthPanel::admin.name-admin") !!}</label>
                                        {!!Form::text('first_name', null, ["placeholder"=>trans("AuthPanel::admin.name-admin"), "class"=>"validate[required]"])!!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{!! trans("AuthPanel::admin.surname-admin") !!}</label>
                                        {!!Form::text('last_name', null, ["placeholder"=>trans("AuthPanel::admin.surname-admin")])!!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group rel focus">
                                        <label>{!! trans("AuthPanel::admin.email-admin") !!}</label>
                                        {!!Form::text('email', null, ["placeholder"=>trans("AuthPanel::admin.email-admin"), "class"=>"validate[required]"])!!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group rel focus">
                                        <label>{!! trans("AuthPanel::admin.phone-admin") !!}</label>
                                        {!!Form::text('phone', null, ["placeholder"=>trans("AuthPanel::admin.phone-admin")])!!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group rel focus">
                                        <label>{!! trans("AuthPanel::admin.password-admin") !!} <small>{!! trans("AuthPanel::admin.password-admin-small") !!}</small></label>
                                        {!!Form::password('password', null, ["placeholder"=>trans("AuthPanel::admin.password-admin")])!!}
                                    </div>
                                </div>
                            </div>
                            <div class="float-right">
                                <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                            </div>
                        </div>
                        <div id="security" role="tabpanel" class="tab-pane fade">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{ __('AuthPanel::profile.google_authenticator') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="all d-flex align-center newAll">
                                            <div class="type">
                                                @if($user->google2fa)
                                                    <i class="fal fa-check green"></i>{{ __('AuthPanel::profile.active') }}
                                                @else
                                                    <i class="fal fa-times red"></i>{{ __('AuthPanel::profile.pasive') }}
                                                @endif
                                            </div>
                                            <div class="buttonMini">
                                                <button type="button" class="btn-primary" id="{{ is_null($user->google2fa) ? 'google2faActivateBtn' : 'google2faDeactivateBtn'}}">
                                                    @if(is_null($user->google2fa))
                                                        {{ __('AuthPanel::profile.activate') }}
                                                    @else
                                                        {{ __('AuthPanel::profile.deactivate') }}
                                                    @endif
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{ __('AuthPanel::profile.google2fa_warning') }}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-check slide-check float-left p-0">
                                            {!!Form::checkbox('google2fa_warning', null, $user->google2fa_warning == 1 , ["class"=> "form-check-slide-input position-relative float-right", "id" => "google2fa_warning"])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="float-right">
                                <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#google2FaModal').on('hidden.bs.modal', function () {
            location.reload();
        });

        $('#google2faDeactivateBtn').on('click', function () {
            $.ajax({
                'url': "{{ route('Google2fa.deactivate2FA') }}",
                'method': 'GET',
                success: function (response) {
                    if(response.status == true) {
                        location.reload();
                    }
                }
            });
        })
    </script>
@endpush
