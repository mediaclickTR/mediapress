@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="col-md-12 pa">
            <div class="title">{!! trans("MPCorePanel::analytics.google_install_documentation") !!}</div>
                <iframe title="Google Analytic Doc" src="{!! asset("vendor/mediapress/docs/googleAnalyticsInstallDocumentation.pdf") !!}" width="100%" height="750px"></iframe>
                <div style="margin-top:20px;text-align:center !important;">
                    <a href="{!! route("Interaction.analytics.google.googleAnalyticsSetting") !!}" class="btn btn-primary" target="_blank"><i class="fa fa-google"></i> {!! trans("MPCorePanel::analytics.google_settings") !!}</a>
                </div>
            </div>
@endsection
