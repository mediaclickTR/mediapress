@extends('ContentPanel::inc.module_main')
@push("styles")
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
@endpush

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
@endpush

@section('content')
    <div class="float-left">
        <a href="{{ route('Content.slider.scenes.index', ['slider_id' => $slider->id]) }}" class="btn btn-secondary mb-4">
            <i class="fa fa-chevron-left"></i>
            {!! __('ContentPanel::general.go_back_list') !!}
        </a>
    </div>
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content slidepage">
        <div class="title">
            {!! __('ContentPanel::scene.edit_scene') !!}
        </div>
        <div class="col">
            <div class="row">
                <form action="{!! route('Content.slider.scenes.update',[$slider->id,$scene->id]) !!}" method="post">
                    {!! csrf_field() !!}

                    <div class="col-6 right-area">
                        <div class="row">

                            <div class="status">
                                <div class="form-group">
                                    <div class="checkbox" style="margin: 0">
                                        <label class="active p-0">
                                            {!! __("ContentPanel::general.status_title.active") !!}
                                            <input type="radio" name="status"
                                                   value="1" {!! old('status', $scene->status) == 1 ? 'checked' : '' !!}>
                                        </label>
                                        <label class="passive p-0">
                                            {!! __("ContentPanel::general.status_title.passive") !!}
                                            <input type="radio" name="status"
                                                   value="0" {!! old('status', $scene->status) == '0' ? 'checked' : '' !!}>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="order">
                                <label for="time">{!! __('ContentPanel::scene.order') !!}</label>
                                <input type="text" name="order" id="order" class="form-control" value="{{ $scene->order }}">
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs col-12">
                        @foreach($countryGroups as $countryGroup)
                            @foreach($countryGroup->languages as $language)
                                <li>
                                    <a {!! $loop->first && $loop->parent->first ? 'class="show active"' :'' !!} data-toggle="tab"
                                       href="#{!! $countryGroup->code . $language->code !!}">
                                        <img src="{!! asset('vendor/mediapress/images/flags/'.$language->flag) !!}"
                                            height="13" alt=""> {!! strtoupper($language->code) . " (" . $countryGroup->title . ")" !!}
                                    </a>
                                </li>
                            @endforeach
                        @endforeach

                    </ul>
                    <div class="tab-content">
                        @foreach($countryGroups as $countryGroup)
                            @foreach($countryGroup->languages as $language)
                                @php
                                    $detail = $scene->details()->where('country_group_id', $countryGroup->id)->where('language_id',$language->id)->first();
                                @endphp
                            <div id="{!! $countryGroup->code . $language->code !!}"
                                 class="tab-pane fade in  {!! $loop->first && $loop->parent->first ? 'show active' :'' !!}">

                                <div class="item row mb0">
                                    <div class="col-4 pl">
                                        <div class="head">
                                            <span>{!! __('ContentPanel::scene.video') !!}</span>
                                            <small>
                                                <em>*</em> {!! __('ContentPanel::scene.video_desc') !!}
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-8 pr">
                                        <div class="cont row mb0">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="text"
                                                           name="{{$countryGroup->id}}[{!! $language->id !!}][video_url]"
                                                            value="{{ $detail->video_url }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                @for($t=1;$t<=$slider->text;$t++)
                                    <div class="item row mb0">
                                        <div class="col-4 pl">
                                            <div class="head">
                                                <i>{{$t}}. {!! __('ContentPanel::scene.text_field') !!}</i>
                                            </div>
                                        </div>
                                        <div class="col-8 pr">
                                            <div class="cont row mb0">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <textarea
                                                            name="{{$countryGroup->id}}[{!! $language->id !!}][texts][{{$t}}][desktop]">{!! $detail->texts[$t]['desktop'] ?? '' !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <hr>
                                @endfor
                                <div class="submit">
                                    <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                                </div>

                            </div>
                            @endforeach
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <style>
        hr {
            margin-top: -30px;
            margin-bottom: 30px;
            clear: left;

        }

        .right-area {
            float: right;
            position: absolute;
            right: -120px;
            top: -60px;
        }

        .slidepage .right-area input {
            width: 70px !important;
            float: left !important;
        }

        .slidepage .right-area label {
            float: left !important;
            margin: 6px 3px 6px 20px !important;
        }
    </style>
@endpush
