<?php

return [
    "name"=>"Mediapress 4.0",
    "panel_url_slug"=>"/mp-admin",
    'site_search' => [
        'results_per_page' => config('mediapress.searchrpp')
    ],
    'seo' => [
        'render_keywords' => config('mediapress.renderkeywords')
    ]
];
