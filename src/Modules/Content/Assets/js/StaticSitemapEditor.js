
$(document).ready(function(){

    $('html').delegate('.open-static-sitemap-editor','click',function(e){
        $('#staticSitemapEditor')
            .data('sitemap-id', $(this).attr('data-sitemap-id'))
            .data('callback', $(this).attr('data-callback'));
    });


    $('#staticSitemapEditor').on('show.bs.modal',function(){
    });

    $('#staticSitemapEditor').on('shown.bs.modal',function(){
        var sitemap_id = $(this).attr('data-sitemap-id');
        var callback = $(this).attr('data-callback');
        var modal = $(this);

        var token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            url: '/mp-admin/Content/Sitemaps/popupStatic',//{{ route('Content.sitemaps.popup_static') }}
            data: {
                "_token": token,
                "sitemap_id":sitemap_id
            },
            success:function(data, textStatus, jqXHR){
                console.log(data,jqXHR);
                $(".modal-body>.row>.col",modal).html(data);
                $('#staticSitemapEditor div.modal-footer button').show().prop('disabled',false);
            },
            error: function(jqXHR, textStatus, errorThrown){
                let message = "Sayfa yapısı ekleme/düzenleme için açılamadı: " + "\n"  + "\n"+ errorThrown + "\n" + (jqXHR.responseText !== undefined ? jqXHR.responseText : errorThrown);
                $(".modal-body>.row>.col",modal).html(message);
            }
        });
    });

});
