<?php

namespace Mediapress\Contracts;

interface IContentModule extends IModule
{
    public function websites();

    public function internalWebsites();
}