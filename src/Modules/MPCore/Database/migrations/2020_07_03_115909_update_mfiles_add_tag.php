<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateMfilesAddTag extends Migration
{

    public function up()
    {
        if (! Schema::hasColumn('mfiles', 'tag')) {

            Schema::table('mfiles', function ($table) {
                $table->text('tag')->nullable()->after('size');
            });
        }
    }

    public function down()
    {
        Schema::table('mfiles', function($table)
        {
            $table->dropColumn('tag');
        });
    }
}
