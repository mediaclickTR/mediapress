<?php

namespace Mediapress\Foundation\ThirdParty;

use Mediapress\Foundation\Htmldom\Htmldom;
use Carbon\Carbon;

class TripAdvisor
{


    private $url;

    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function reviews()
    {
        /* This method will return agency reviews */

        $content = file_get_contents($this->url);
        $html = new Htmldom($content);


        $profile = $html->find('.attractions-supplier-profile-SupplierProfile__profile--1oRu9')[0];
        $list = [
            'logo' =>trim($html->find('.logo_svg')[0]->attr['src']),
            'heading' => trim($html->find('#HEADING')[0]->text()),
            'rating' => trim(str_replace('ui_bubble_rating bubble_', '', $html->find('.ui_bubble_rating')[0]->class)),
            'totalReview' => (int)str_replace(['(',')'],"",trim($html->find('.reviews_header_count')[0]->text())),
            'location' => trim($html->find('.attractions-supplier-profile-SupplierProfile__ranking--_Aqq4')[0]->text()),
            'reviews' => [],
        ];

        $reviews = $html->find('.review-container');

        foreach ($reviews as $key => $reviewContainer) {

            $user = [
                'name' => $reviewContainer->find('.info_text')[0]->text(),
                'avatar' => $reviewContainer->find('.avatar .basicImg')[0]->attr['data-lazyurl'],
            ];


            $review = [
                'date' => Carbon::parse($reviewContainer->find('.ratingDate')[0]->title)->format('d/m/y'),
                'noQuotes' => trim($reviewContainer->find('.noQuotes')[0]->text()),
                //  'reviewLocation' => $reviewContainer->find('.review_location_attribution a')[0]->text(),
                'entry' => str_replace('...More', '...', $reviewContainer->find('.entry p')[0]->text()),
                'rating' => trim(str_replace('ui_bubble_rating bubble_', '', $reviewContainer->find('.ui_bubble_rating')[0]->class)),
                'link' => trim($reviewContainer->find('.quote')[0]->firstChild()->attr['href']),

            ];
            $list['reviews'][] = [
                'user' => $user,
                'review' => $review
            ];
        }

        return $list;

    }

    public function getHotelReviews()
    {
        $content = file_get_contents($this->url);
        $html = new Htmldom($content);
        $list = [
            'heading' => trim($html->find('#HEADING')[0]->text()),
            'rating' => (int)trim(str_replace('ui_bubble_rating bubble_', '', $html->find('.ui_bubble_rating')[0]->class)),
            'totalReview' => (int)str_replace(['.',' '],"",trim($html->find('._3jEYFo-z')[0]->text())),
            'reviews' => [],
        ];
        try{
            $list['logo'] = trim($html->find('._1AlVlFFs')[0]->attr['src']);
        }catch (\Exception $exception){
            $list['logo'] = "https://seeklogo.com/images/T/tripadvisor-logo-6939149F8F-seeklogo.com.png";
        }

        $reviews = $html->find('._3hFEdNs8');
        foreach ($reviews as $key => $reviewContainer) {
            $date = "";
            if(isset($reviewContainer->find('.location-review-review-list-parts-EventDate__event_date--1epHa')[0])){
                $date1 = $reviewContainer->find('.location-review-review-list-parts-EventDate__event_date--1epHa')[0]->text();
                $date = explode(': ',$date1)[1];
            }
            $review = [
                'userName' => $reviewContainer->find('._2fxQ4TOx span a')[0]->text(),
                'date' => $date,
                // 'noQuotes' => trim($reviewContainer->find('.location-review-review-list-parts-ReviewTitle__reviewTitle--2GO9Z a span span')[0]->text()),
                // 'reviewLocation' => $reviewContainer->find('.review_location_attribution a')[0]->text(),
                'entry' => $reviewContainer->find('.IRsGHoPm span')[0]->text(),
                'rating' => trim(str_replace('ui_bubble_rating bubble_', '', $reviewContainer->find('.ui_bubble_rating')[0]->class)),
                // 'link' => trim($reviewContainer->find('.location-review-review-list-parts-ReviewTitle__reviewTitle--2GO9Z')[0]->firstChild()->attr['href']),

            ];
            $list['reviews'][] =$review;
        }

        return $list;
    }
}
