<?php

return [
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',

    'created_at_sm' => 'Created',
    'updated_at_sm' => 'Updated',
    'deleted_at_sm' => 'Deleted',

    "system" => "System",
    //--------
    'logout' => 'Logout',
    'preview_site' => 'Preview Website',
    'status' => 'Status',
    'upload' => 'Install',
    'save' => 'Save',
    'new_add' => 'New Add',
    'add' => 'Add',
    'edit' => 'Edit',
    'show' => 'Show',
    'change' => 'Change',
    'remove' => 'Remove',
    'delete' => 'Delete',
    'profile' => 'Profile',
    'preview' => 'Preview',
    'widget' => 'Eklentiler',
    'back' => 'Previous',
    'yes' => 'Yes',
    'no' => 'No',
    'next' => 'Forward',
    'previous' => 'Previous',
    'cancel' => 'Cancel',
    'transactions' => 'Processing',
    'selection' => 'Please Select',
    'list_selection' => 'Select in List',
    'general' => 'General',
    'create-title' => ':name Add',
    'edit-title' => ':name Edit',
    'selected-delete' => 'Selected Remove',
    'actions' => 'Actions',

    'send_user' => 'Formu doldurulan kullanıcıya e-posta gönderilsin mi?',

    'select.list.size' => 'Select List Size',

    'id' => 'ID',
    'name' => 'Name',
    'title' => 'Title',
    'type' => 'Type',
    'image' => 'Image',
    'name_entry' => 'Please Entry Name',

    'order' => 'Order',
    'order_entry' => 'Sıra giriniz',

    'users' => 'Users',

    'slug' => 'Slug',
    'slug_entry' => 'Please Entry Slug',

    'detail' => 'Detail',
    'detail_entry' => 'Please Entry Detail',

    'note' => 'Note',
    'note_entry' => 'Please Entry Note',

    'key' => 'Key',
    'key_entry' => 'Please Entry Key',

    'reads' => 'Reads',
    'read' => 'Read',
    'unread' => 'Not Read',
    'subject' => 'Subject',
    'email' => 'E-Mail',

    'nodata'=> 'No recorded data found.',
    'select_service'=> 'Select Service',
    'select_processing'=> 'Select Processing',

    'mpmailler' => 'MPMailler',
    'mailchimp' => 'Mailchimp',
    'eligible' => 'Eligible',
    'selected' => 'Chosen',

    'success_message'=>'The operation was successful',
    'choose_below_options' => 'Choose an option below.',
    'back_to_list' => 'Back to List',
    'edit_again' => 'Edit Again',
    'add_new' => 'Add New',
    'preview_page' => 'Preview Page',
    'set_this_option_while_session' => 'Set this option while this session.',

    'multiple_transactions' => 'Transactions',
    'all_website_records' => 'All Website Records',
    'clear_filters' => 'Clear Filters',
    'sorting'=>[
        'save'=>'Save Order',
        'save-now' => 'Save Now',
        'change-text' => 'You changed the order. Don\'t forget to save.',
        'alert' => 'Warning!',
    ],
    "status-titles" =>  [
        "active" => 'Active',
        "passive" => 'Passive',
        "draft" => 'Draft',
    ],
    "category-icon-title" =>  [
        "criteria" => 'Select Criteria',
        "property" => 'Select Property',
        "delete" => 'Delete',
        "edit" => 'Edit',
        "sub-category" => 'Add Sub Category',
    ],
];
