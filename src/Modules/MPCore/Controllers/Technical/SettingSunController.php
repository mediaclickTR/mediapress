<?php

namespace Mediapress\Modules\MPCore\Controllers\Technical;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Cache;

class SettingSunController extends Controller
{
    public const MP_CORE_TECHNICAL_SETTING_SUN_INDEX = "MPCore.technical.setting_sun.index";
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        $settingSuns = SettingSun::paginate(15);
        return view("MPCorePanel::technical.setting_sun.index", compact('settingSuns'));
    }
    public function create()
    {
        return view("MPCorePanel::technical.setting_sun.create");
    }
    public function store(Request $request)
    {
        $data = request()->except("_token");
        $create = SettingSun::insert($data);
        if ($create){
            Cache::forget('settingSun');
        }
        return redirect(route(self::MP_CORE_TECHNICAL_SETTING_SUN_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    public function edit($id)
    {
        $setting = SettingSun::find($id);
        return view("MPCorePanel::technical.setting_sun.edit", compact('setting','id'));
    }
    public function update(Request $request)
    {
        $data = request()->except('_token');
        $update = SettingSun::where('id',$request->id)->update($data);

        if ($update){
            Cache::forget('settingSun');
        }

        return redirect(route(self::MP_CORE_TECHNICAL_SETTING_SUN_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    public function delete($id)
    {
        $setting = SettingSun::find($id);
        if ($setting){
            $setting->delete();
        }
        return redirect(route(self::MP_CORE_TECHNICAL_SETTING_SUN_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}