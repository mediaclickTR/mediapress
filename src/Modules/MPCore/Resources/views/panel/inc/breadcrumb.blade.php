@isset($breadcrumb)
    <style type="text/css">
        .breadcrumb ul>li>a{
            max-width: 300px;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                @foreach($breadcrumb as $crumb)
                    <li data-key="{{$crumb['key']}}">
                        <a href="{{$crumb['href']}}" title="{{ $crumb['text'] }}" class="text-truncate">@if($crumb['icon'])<i class="fa fa-{{$crumb['icon']}}"></i> @endif{{$crumb['text']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endisset
