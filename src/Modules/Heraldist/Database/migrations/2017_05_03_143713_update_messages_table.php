<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMessagesTable extends Migration
{
    public const MESSAGES = 'messages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable(self::MESSAGES)) {
            Schema::table(self::MESSAGES, function (Blueprint $table) {
                $table->text('utm')->nullable();
                $table->text('source')->nullable();
                $table->text('agent')->nullable();
                $table->integer('visit_id')->nullable();
                $table->string('track_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable(self::MESSAGES)) {
            Schema::table(self::MESSAGES, function (Blueprint $table) {
                $table->dropColumn('utm');
                $table->dropColumn('source');
                $table->dropColumn('agent');
                $table->dropColumn('visit_id');
                $table->dropColumn('track_id');
            });
        }
    }
}
