<?php

namespace Mediapress\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class Module extends Model
{
    //use CacheQueryBuilder;
    use SoftDeletes;
    protected $table = 'modules';
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';
    protected $fillable = ['name', 'namespace','type','module_id','slot_id','status'];

    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }
}
