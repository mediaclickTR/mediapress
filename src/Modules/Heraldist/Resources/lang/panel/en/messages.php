<?php

return [
    'messages' => 'Messages',
    'forms' => 'Forms',
    'new_total' => 'New / Total',
    'gomessages' => 'Go to Messages',

    'open-new-page' => 'Open in new page',
    'check-unread' => 'Mark as unread',
    'form_messages' => 'Messages for form named :form',
    'empty_message_box' => 'Your message box empty.',
    'open.new.page' => 'Open in new page',
    'select_all' => 'Select All',
    'export_excel' => 'Export Excel',
    'download_file' => 'Download File',
    'form_detail' => [
        'date' => 'Date',
        'time' => 'Time',
        'referrer' => 'Referrer',
        'visited_pages' => 'Visited Pages',
        'utm'=> 'Utm Info',
        'form_recording' => 'Form Record',
        'watch_recording' => 'Watch Record',
    ],
];
