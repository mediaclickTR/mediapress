<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>Password Reset Form - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
        .form-gap {
            padding-top: 70px;
        } ul,li{padding: 0;margin: 0}   </style>

</head>
<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="form-gap"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                        <h2 class="text-center">{{ __('ContentPanel::general.protected_content') }}</h2>

                        <div class="panel-body">
                            @if($errors->any())

                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->all() as $key=>$error)
                                            {!!  $error !!}
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form id="register-form" action="{{route('web.url.auth')}}" role="form" autocomplete="off"
                                  class="form" method="post">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input id="password" name="password" placeholder="" class="form-control"
                                               type="password">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <input name="recover-submit" class="btn btn-lg btn-default btn-block"
                                           value="{{ __('ContentPanel::general.protected_content_login') }}"
                                           type="submit">
                                </div>
                                {!!Form::hidden('next',$next)!!}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
</script>
</body>
</html>
