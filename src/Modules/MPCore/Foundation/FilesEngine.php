<?php

    namespace Mediapress\Modules\MPCore\Foundation;

    use Illuminate\Http\UploadedFile;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Str;
    use Mediapress\FileManager\Models\MDisk;
    use Mediapress\FileManager\Models\MFile;
    use Mediapress\FileManager\Models\MFolder;
    use Illuminate\Support\Facades\Storage;
    use Symfony\Component\HttpFoundation\File\Exception\FileException;
    use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
    use Illuminate\Support\Facades\Session;

    class FilesEngine
    {

        const MFOLDER_ID_STR = "mfolder_id";
        const MDISK_ID_STR = "mdisk_id";
        const DISKKEY_STR = "diskkey";
        const UPLOADNAME_STR = "uploadname";
        const FILENAME_STR = "filename";
        const MIMECLASS_STR = "mimeclass";
        const MIMETYPE_STR = "mimetype";
        const MFOLDER_STR = "MFolder";
        const MFILE_STR = "MFile";
        const ACTION_STR = "action";
        const STATUS_STR = "status";
        const WARNINGS_STR = "warnings";
        const MESSAGES_STR = "messages";
        const ERRORS_STR = "errors";
        const EXCEPTIONFILE_GLUE_STR = " - File: ";
        const EXCEPTIONLINE_GLUE_STR = " - Line: ";

        public function createFolder($diskkey, $folder2create_name, $folder2create_folder_id = -1)
        {

            // Create a base array as an argument to create the folder model
            $new_folder_data = [
                "path" => $folder2create_name,
                self::MFOLDER_ID_STR => null,
                self::MDISK_ID_STR => null,
            ];

            // Bring an empty resultset
            $resultset = $this->resultset();
            // Fill it in place
            $resultset[self::ACTION_STR] = __FUNCTION__;


            try {

                //Means folder should have a parent folder:
                if ($folder2create_folder_id) {
                    $parent = MFolder::find($folder2create_folder_id);
                    // But  return if it does not exists:
                    if (is_null($parent)) {
                        throw new \InvalidArgumentException("Parent folder not found (id: $folder2create_folder_id)");
                    }
                    // It exists so we're here:
                    $new_folder_data[self::MFOLDER_ID_STR] = $parent->id;
                }

                //We always need an mdisk
                $mdisk = MDisk::where(self::DISKKEY_STR, $diskkey)->first();

                //Return if there isn't the intended one:
                if (is_null($mdisk)) {
                    throw new \InvalidArgumentException("MDisk not found (diskkey: $diskkey)");
                }
                //It is there:
                $new_folder_data[self::MDISK_ID_STR] = $mdisk->id;

                // Check if this folder already exists
                $check_existing = MFolder::where(self::MDISK_ID_STR, $mdisk->id);
                if ($folder2create_folder_id) {
                    $check_existing->where(self::MFOLDER_ID_STR, $parent->id);
                }
                $check_existing = $check_existing->where("path", $folder2create_name)->first();

                //If it already is:
                if (!is_null($check_existing)) {
                    //"Folder already exists (id: $check_existing->id)"
                    $resultset[self::STATUS_STR] = true;
                    $resultset[self::WARNINGS_STR][] = "A folder with this properties already exists. It's returned.";
                    $resultset["data"][self::MFOLDER_STR] = $check_existing;
                }

                //Physical folder path to create:
                $physical_path = $folder2create_folder_id ? $parent->getPath() . "/" . $folder2create_name : $folder2create_name;

                // Default is true, in case this folder physically exists
                $create_physical = true;
                if (!Storage::disk($diskkey)->exists($physical_path)) {
                    //While there is not an existing physical one, one should be created:
                    $create_physical = Storage::disk($diskkey)->makeDirectory($physical_path);
                }

                //If it neither already exists nor could be created, there is da problem:
                // Else we have it physically
                if (!$create_physical) {
                    throw new \InvalidArgumentException("Folder could not be created physically: $physical_path");
                }

                // Now create DB pointer for that physical folder:
                if (is_null($check_existing)) {
                    $create = MFolder::create($new_folder_data);
                    if ($create) {
                        //Voila!
                        $resultset[self::STATUS_STR] = true;
                        $resultset[self::MESSAGES_STR][] = "Folder successfully created.";
                        $resultset["data"][self::MFOLDER_STR] = $create;
                    } else {
                        $resultset[self::STATUS_STR] = false;
                        $resultset[self::ERRORS_STR][] = "Folder model could not be saved.";
                    }
                }

                return $resultset;

            } catch (\Exception $e) {
                $resultset[self::STATUS_STR] = false;
                $resultset[self::ERRORS_STR][] = $e->getMessage() . self::EXCEPTIONFILE_GLUE_STR . $e->getFile() . self::EXCEPTIONLINE_GLUE_STR . $e->getLine();
                return $resultset;
            } finally {
                return $resultset;
            }

        }

        public function saveUploadedFile(
            UploadedFile $request_file_obj,
            $diskkey_or_id,
            $folder_id = null,
            $desired_filename = null,
            $retry_with_renewed_names = true,
            $process_id=null
        )
        {

            // Create a base array as an argument to create the folder model
            $new_file_data = [
                self::MFOLDER_ID_STR => null,
                self::MDISK_ID_STR => null,
                "mfile_id" => null,
                self::UPLOADNAME_STR => null,
                "fullname" => null,
                self::FILENAME_STR => null,
                "extension" => null,
                self::MIMECLASS_STR => null,
                self::MIMETYPE_STR => null,
                "width" => null,
                "height" => null,
                "size" => null,
                "admin_id"=>null,
                "process_id"=>$process_id
            ];

            // Bring an empty resultset
            $resultset = $this->resultset();
            // Fill it in place
            $resultset[self::ACTION_STR] = __FUNCTION__;


            try {
                /** @var UploadedFile $request_file_obj */
                if (!$request_file_obj->isValid()) {
                    throw new FileNotFoundException("File does not exist at path {$request_file_obj->getPathname()}");
                }

                //MDISK
                $disk = $this->resolveDiskFromIdentifier($diskkey_or_id);

                if (is_null($disk)) {
                    throw new \InvalidArgumentException("Disk not found: $diskkey_or_id");
                }
                $new_file_data[self::MDISK_ID_STR] = $disk->id;

                //MFOLDER
                $folder = MFolder::find($folder_id);
                if (!is_null($folder_id) && !$folder) {
                    throw new \InvalidArgumentException("Folder not found: $folder_id");
                }
                $new_file_data[self::MFOLDER_ID_STR] = $folder_id;


                //OTHER KNOWN DATA
                $onlyfn = pathinfo($request_file_obj->getClientOriginalName())['filename'];
                $new_file_data[self::UPLOADNAME_STR] = trim($onlyfn);

                $new_file_data[self::MIMETYPE_STR] = $request_file_obj->getClientMimeType();

                $new_file_data[self::MIMECLASS_STR] = $new_file_data[self::MIMETYPE_STR] && (strpos($new_file_data[self::MIMETYPE_STR],'/')>-1) ? strstr($new_file_data[self::MIMETYPE_STR],'/',true)  : null;
                list($width, $height) = $new_file_data[self::MIMECLASS_STR] == "image" ? getimagesize($request_file_obj->getPathname()) : [null,null];
                $new_file_data["width"] = $width;
                $new_file_data["height"] = $height;
                $new_file_data["size"] = $request_file_obj->getSize();

                $admin = Session::get('panel.user');
                $new_file_data["admin_id"] = $admin ? $admin->id : -1;


                $original_ext = $request_file_obj->getClientOriginalExtension();


                $desired_filename = Str::slug(Str::ascii($desired_filename ?? $new_file_data[self::UPLOADNAME_STR]));
                //check if file with original name exists:
                $folder_path = is_null($folder_id) ? "" : $folder->getPath();

                $original_ext = trim(strtolower($original_ext));

                $new_filename = trim($desired_filename);
                $new_fullname = $new_filename . "." . $original_ext;
                $full_file_path = $folder_path . $new_fullname;

                $exists = Storage::disk($disk->diskkey)->exists($full_file_path);

                if($exists && !$retry_with_renewed_names){
                    throw new FileException("File already exists");
                }

                $counter = 0;
                while ($exists) {
                    $counter++;
                    $new_filename = trim($desired_filename . "-$counter");
                    $new_fullname = $new_filename . "." . $original_ext;
                    $full_file_path = $folder_path . $new_fullname;
                    $exists = Storage::disk($disk->diskkey)->exists($full_file_path);
                }

                $new_file_data[self::FILENAME_STR] = $new_filename;
                $new_file_data["extension"] = $original_ext;
                $new_file_data["fullname"] = $new_fullname;


                $folder_path_again = rtrim($folder_path,"/");

                $store = $request_file_obj->storeAs($folder_path_again, $new_fullname, $disk->diskkey);
                if (!$store) {
                    throw new FileException("Cannot save file \"$full_file_path\" to \"$disk->diskkey\" disk.");
                }

                $create = MFile::create($new_file_data);
                if (!$create) {
                    throw new FileException("Could not save MFile: $create");
                }

                $resultset[self::STATUS_STR] = true;
                $resultset[self::MESSAGES_STR][] = "File successfully created.";
                $resultset["data"][self::MFILE_STR] = $create;

                return $resultset;

            } catch (\Exception $e) {
                $resultset[self::STATUS_STR] = false;
                $resultset[self::ERRORS_STR][] = $e->getMessage() . self::EXCEPTIONFILE_GLUE_STR . $e->getFile() . self::EXCEPTIONLINE_GLUE_STR . $e->getLine();
                return $resultset;
            } finally {
                return $resultset;
            }

        }

        public function makeSurePathExists($diskkey, $folderPath)
        {

            // Bring an empty resultset
            $resultset = $this->resultset();
            // Fill it in place
            $resultset[self::ACTION_STR] = __FUNCTION__;

            // clean parameter from root slash and whitespace
            $folderPath = trim($folderPath,'\\/ ');

            if ("" == $folderPath) {
                $resultset[self::STATUS_STR] = false;
                $resultset[self::WARNINGS_STR][] = "Path which given to check was an empty string which assumed to refer disk root.";
                $resultset[self::ERRORS_STR][] = "Path which given to check was an empty string";
                return $resultset;
            }

            try {
                DB::beginTransaction();

                $exploded = explode("/", $folderPath);

                $folder_id = null;
                $folder = null;
                foreach ($exploded as $exp) {
                    $create = $this->createFolder($diskkey, $exp, $folder_id);
                    if ($create[self::STATUS_STR]) {
                        $folder = $create["data"][self::MFOLDER_STR];
                        $folder_id = $folder->id;
                    } else {
                        throw new \InvalidArgumentException(implode($create[self::ERRORS_STR]));
                    }
                }

                $resultset[self::STATUS_STR]=true;
                $resultset["data"][self::MFOLDER_STR] = $folder;

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $resultset[self::STATUS_STR] = false;
                $resultset[self::ERRORS_STR][] = $e->getMessage() . self::EXCEPTIONFILE_GLUE_STR . $e->getFile() . self::EXCEPTIONLINE_GLUE_STR . $e->getLine();
                return $resultset;
            }

            return $resultset;

        }

        public function easySaveUploadedFile(
            $diskkey_or_id,
            UploadedFile $request_file_obj,
            $folder_path_or_id = -1,
            $desired_filename = null,
            $process_id=null)
        {

            //preform variable
            $folder_path_or_id = trim(ltrim($folder_path_or_id, "\\/"));
            if ($folder_path_or_id == "") {
                $folder_path_or_id = null;
            }

            // Bring an empty resultset
            $resultset = $this->resultset();
            // Fill it in place
            $resultset[self::ACTION_STR] = __FUNCTION__;


            try {
                /** @var UploadedFile $request_file_obj */
                if (!$request_file_obj->isValid()) {
                    throw new FileNotFoundException("File does not exist at path {$request_file_obj->getPathname()}");
                }

                //MDISK
                $disk = $this->resolveDiskFromIdentifier($diskkey_or_id);
                if (is_null($disk)) {
                    throw new \InvalidArgumentException("Disk not found: $diskkey_or_id");
                }


                $new_file_data[self::MDISK_ID_STR] = $disk->id;

                //MFOLDER
                $folder = null;
                if (is_int($folder_path_or_id) && $folder_path_or_id > 0) {
                    $folder = MFolder::find($folder_path_or_id);
                } elseif (is_string($folder_path_or_id) && $folder_path_or_id != "") {
                    $makesure = $this->makeSurePathExists($disk->diskkey, $folder_path_or_id);
                    if ( ! $makesure[self::STATUS_STR]) {
                        throw new FileException(implode(" - ", $makesure[self::ERRORS_STR]));
                    }
                    $folder = $makesure["data"][self::MFOLDER_STR];
                }


                if (!is_null($folder_path_or_id) && !$folder) {
                    throw new \InvalidArgumentException("Folder not found / cannot be created: $folder_path_or_id");
                }


                $save_file = $this->saveUploadedFile($request_file_obj,$diskkey_or_id, ($folder ? $folder->id : $folder),$desired_filename,true,$process_id);

                if ( ! $save_file[self::STATUS_STR]) {
                    throw new FileException(implode(" - ", $save_file[self::ERRORS_STR]));
                }


                $resultset[self::STATUS_STR] = $save_file[self::STATUS_STR];
                $resultset[self::MESSAGES_STR][] = array_merge($resultset[self::MESSAGES_STR], $save_file[self::MESSAGES_STR]);
                $resultset[self::WARNINGS_STR][] = array_merge($resultset[self::WARNINGS_STR], $save_file[self::WARNINGS_STR]);
                $resultset["data"][self::MFILE_STR] = $save_file["data"][self::MFILE_STR];
                $file = $save_file["data"][self::MFILE_STR] ?? null;
                $resultset["data"]["original_url"] = $save_file[self::STATUS_STR] ? url($file->getUrl()) : "";

                return $resultset;

            } catch (\Exception $e) {
                $resultset[self::STATUS_STR] = false;
                $resultset[self::ERRORS_STR][] = $e->getMessage() . self::EXCEPTIONFILE_GLUE_STR . $e->getFile() . self::EXCEPTIONLINE_GLUE_STR . $e->getLine();
                return $resultset;
            } finally {
                return $resultset;
            }
        }

        private function resultset()
        {
            return [
                self::ACTION_STR => null,
                self::STATUS_STR => null,
                "data" => [],
                self::ERRORS_STR => [],
                self::WARNINGS_STR => [],
                self::MESSAGES_STR => [],
                "logs" => [],
            ];
        }

        private function resolveDiskFromIdentifier($diskkey_or_id){

            if (is_int($diskkey_or_id)) {
                $disk = MDisk::find($diskkey_or_id);
            } elseif (is_string($diskkey_or_id)) {
                $disk = MDisk::where(self::DISKKEY_STR, $diskkey_or_id)->first();
            }else{
                $disk = null;
            }

            return $disk;
        }


    }
