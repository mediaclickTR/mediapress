<?php

namespace Mediapress\Modules\MPCore;

use Mediapress\Contracts\ICoreModule;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Models\MPModule;

use Mediapress\Modules\Content\Facades\Content;

use Mediapress\Contracts\EntityInterface;
use Mediapress\Contracts\HeraldistInterface;
use Mediapress\Contracts\ContentInterface;
use Cache;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

class MPCore extends MPModule implements ICoreModule
{
    public const SUBMENU = "submenu";
    public const TITLE = "title";
    public const STATUS = 'status';
    public const POSITION = 'position';
    public const TARGET = 'target';
    public const SELF = '_self';
    public const LANGUAGES = "languages";
    public const RIGHT = 'right';
    public const ENTITY = "Entity";
    public $name = "MPCore";
    public $url = "mp-admin/MPCore";

    #region Menu

    public function baseMenu()
    {
        return [
            "header_menu" => [
                'dashboard' =>
                    [
                        'name' => trans("MPCorePanel::menu_titles.dashboard"),
                        'url' => route("panel.dashboard"),
                        self::STATUS => 1,
                        'type' => 'menu',
                        self::POSITION => 'left'
                    ],
                "content" =>
                    [
                        'name' => trans("MPCorePanel::menu_titles.content"),
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => 'menu',
                        self::POSITION => 'left',
                        'cols' => [
                            'modules' => [
                                "name" => "Modüller",
                                "rows" => [],
                            ],
                            'sitemaps' => [
                                "name" => "Sayfa Yapıları",
                                "rows" => [],
                            ],
                            'show_case' => [
                                "name" => "Öne Çıkarılan",
                                "rows" => [],
                            ],
                            'web_module' => [
                                "name" => "Web Modül",
                                "rows" => [],
                            ],
                        ],
                    ],
                "forms" =>
                    [
                        'name' => trans("HeraldistPanel::menu_titles.forms"),
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => 'menu',
                        self::POSITION => 'left',
                        'cols' => [
                            'email_services' => [
                                "name" => "E-posta Servisleri",
                                "rows" => [],
                            ],
                            'forms' => [
                                "name" => "Formlar",
                                "rows" => [],
                            ],
                        ]
                    ],
                "settings" =>
                    [
                        'name' => trans("MPCorePanel::menu_titles.settings"),
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => 'menu',
                        self::POSITION => 'left',
                        'cols' => [
                            'structure' => [
                                "name" => "Formlar",
                                "rows" => [],
                            ],
                            'seo' => [
                                "name" => "SEO",
                                "rows" => [],
                            ],
                            'admins' => [
                                "name" => "Yöneticiler",
                                "rows" => [],
                            ],
                            'system' => [
                                "name" => trans('MPCorePanel::general.system'),
                                "rows" => [],
                            ],
                        ]
                    ],
                self::LANGUAGES =>
                    [
                        'name' => self::LANGUAGES,
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => self::LANGUAGES,
                        self::POSITION => self::RIGHT,
                        'cols' => [
                            'language_lists' => [
                                "name" => "Diller",
                                "rows" => [],
                            ],
                        ],
                    ],
                "author" =>
                    [
                        'name' => trans("MPCorePanel::menu_titles.author"),
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => 'author',
                        self::POSITION => self::RIGHT,
                        'author_image' => '',
                        'author_title' => '',
                        'cols' => [
                            'user_menus' => [
                                "name" => "Kullanıcı Menüsü",
                                'author_image' => '',
                                'author_title' => '',
                                "rows" => [],
                            ]
                        ]
                    ],
                "sites" =>
                    [
                        'name' => 'mediaclick.com.tr',
                        'url' => '#',
                        self::TARGET => self::SELF,
                        self::STATUS => 1,
                        'type' => 'sites',
                        self::POSITION => self::RIGHT,
                        'cols' => [],
                    ],
            ]
        ];
    }

    public function buildMenu()
    {
        $baseMenu = $this->baseMenu();

        $modules = ModulesEngine::getMPModuleNames();
        if ($modules) {
            foreach ($modules as $module) {
                if ($this->hasMethod($module, "fillMenu")) {
                    $baseMenu = $module::fillMenu($baseMenu);
                }
            }
        }
        // Settings (MPCore)
        $baseMenu = $this->fillSettingMenu($baseMenu);

        if (function_exists('user_modify_panelmenu')) {
            $baseMenu = user_modify_panelmenu($baseMenu);
        }
        return $baseMenu;
    }

    public function fillSettingMenu($menu)
    {
        #region Modules
        $content_cols_modules_rows_set = [
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("MPCorePanel::menu_titles.translate_center"),
                "url" => route("MPCore.translate.index")
            ],
        ];

        if (activeUserCan(["filemanager.filemanager.menu_show",])) {
            $content_cols_modules_rows_set[] = [
                "type" => self::SUBMENU,
                self::TITLE => trans("MPCorePanel::menu_titles.file_manager"),
                "url" => "javascript:void(0);",
                "onclick" => "openFileManagerFromMenu()"
            ];
        }

        if(Route::has('Survey.index')) {
            $content_cols_modules_rows_set[] = [
                "type" => self::SUBMENU,
                self::TITLE => trans("MPCorePanel::menu_titles.survey"),
                "url" => route("Survey.index"),
            ];
        }


        $menu = dataGetAndMerge($menu, 'header_menu.content.cols.modules.rows', $content_cols_modules_rows_set);
        #endregion

        #region Settings


        $settings_cols_system_rows_set = [
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("MPCorePanel::menu_titles.panel_language_parts"),
                "url" => route("MPCore.panel.translate.index")
            ],
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("MPCorePanel::menu_titles.user_actions"),
                "url" => route("MPCore.logs.index")
            ],
            [
                "type" => self::SUBMENU,
                self::TITLE => trans('MPCorePanel::menu_titles.general_settings'),
                "url" => route('Settings.index'),
            ],
        ];

        if (activeUserCan(["tools.tools.menu_show",])) {
            $settings_cols_system_rows_set[] =
                [
                    "type" => self::SUBMENU,
                    self::TITLE => trans('MPCorePanel::menu_titles.tools'),
                    "url" => route('Tools.index'),
                ];
        }

        $menu = dataGetAndMerge($menu, 'header_menu.settings.cols.system.rows', $settings_cols_system_rows_set);

        #endregion

        #region Header Menu > Languages > Language lists > Set

        // Tab Name Set
        $current_language = session()->get('panel.active_language');
        $panel_languages = session()->get('panel.languages.languages');

        data_set($menu, 'header_menu.languages.cols.language_lists.name', $current_language->code);

        $languages_lists_set = [];
        if ($panel_languages) {
            foreach ($panel_languages as $panel_language) {
                if ($panel_language != $current_language->id) {
                    $language = Language::find($panel_language);

                    if (!$language) {
                        continue;
                    }

                    $languages_lists_set [] =
                        [
                            "type" => self::SUBMENU,
                            self::TITLE => $language->code,
                            "url" => "#",
                            self::TARGET => self::SELF,
                            'id' => $language->id
                        ];
                }
            }
        }

        return dataGetAndMerge($menu, 'header_menu.languages.cols.language_lists.rows', $languages_lists_set);

        #endregion
    }

    public function hasMethod($facade, $method)
    {
        if (!class_exists($facade)) {
            return null;
        }

        $class_methods = get_class_methods(app($facade));
        if (in_array($method, $class_methods)) {
            return true;
        }
    }


    #endregion

    #region Base module methods

    /**
     * @param string $key
     * @return string|null
     */
    public function settingSunFindValue($key)
    {
        $settings = Cache::get("settingSun");
        if (!$settings) {
            return null;;
        }
        foreach ($settings as $setting) {
            if ($setting->key == $key) {
                return $setting->value;
            }
        }
        // DB de bulamazsa config'de ara
        if (config('mediapress.' . $key)) {
            return config('mediapress.' . $key);
        } else {
            return null;
        }
    }

    #endregion

    #region Content

    /**
     * @return array
     */
    public function getWebsites()
    {
        if (hasMethod("Content", "websites")) {
            return Content::Websites();
        }
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        return Language::orderBy("sort", "ASC")->get();
    }

    public function modulesMap($function_name)
    {
        $modules = $this->getMPModuleNames();
        array_map($function_name, $modules);
    }

    /**
     * @return mixed
     */
    public function getInternalWebsites()
    {
        if (hasMethod("Content", "internalWebsites")) {
            return Content::internalWebsites();
        }
    }

    #endregion

    #region Entity

    /**
     * @return array
     */
    public function getEntityEntitylists()
    {
        if (hasMethod(self::ENTITY, "getEntitylists")) {
            return Entity::getEntitylists();
        }
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getEntityEntitylist($id)
    {
        if (hasMethod(self::ENTITY, "getEntitylist")) {
            return Entity::getEntitylist($id);
        }
    }

    /**
     * @param integer $pivot_id
     * @return array
     */
    public function getEntityEntitylistPivot($pivot_id)
    {
        if (hasMethod(self::ENTITY, "getEntitylistPivot")) {
            return Entity::getEntitylistPivot($pivot_id);
        }
    }

    /**
     * @param integer $Entitylist_id
     * @return array
     */
    public function getEntityEntitylistPivotWithEntitylistID($entitylist_id)
    {
        if (hasMethod(self::ENTITY, "getEntitylistPivotWithEntitylistID")) {
            return Entity::getEntitylistPivotWithEntitylistID($entitylist_id);
        }
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getEntityEntitylistDetailWithID($id)
    {
        return Entity::getEntitylistDetailWithID($id);
    }

    /**
     * @param string $model
     * @param integer $Entitylist_id
     * @return array
     */
    public function getEntityEntityWithUserableID($model, $entitylist_id)
    {
        if (hasMethod(self::ENTITY, "getEntityWithUserableID")) {
            return Entity::getEntityWithUserableID($model, $entitylist_id);
        }
    }

    /**
     * @return array $data
     */
    public function getEntityMaillist()
    {
        if (hasMethod(self::ENTITY, "getMaillist")) {
            return Entity::getMaillist();
        }
    }

    #endregion

    #region Heraldist

    /**
     * @param array $data
     * @param string $emails , $title, $subject, $fromEmail
     * @return void
     */
    public function jobHeraldistMailSender($data, $emails, $title, $subject, $fromEmail)
    {
        if (hasMethod("Heraldist", "mailSender")) {
            Heraldist::mailSender($data, $emails, $title, $subject, $fromEmail);
        }
    }

    /**
     * @param array $data
     * @param string $emails , $title, $subject, $fromEmail
     * @param integer $queueTime
     * @return void
     */
    public function jobHeraldistMailSenderWithQueue($data, $emails, $title, $subject, $fromEmail, $queueTime)
    {
        if (hasMethod("Heraldist", "mailSenderWithQueue")) {
            Heraldist::mailSenderWithQueue($data, $emails, $title, $subject, $fromEmail, $queueTime);
        }
    }

    #endregion

    #region EntityInterfaceFunctions
    /*public function getEntitylists($params = null)
    {
        // TODO: Implement getEntitylists() method.
        $isi_ustlenen_modul = "Mediapress\\Modules\\Entity\\Entity";
        $isi_ustlenen_modul = new $isi_ustlenen_modul();
        return $isi_ustlenen_modul->{__FUNCTION__}($params);
    }*/
    #endregion


}
