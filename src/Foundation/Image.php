<?php

namespace Mediapress\Foundation;

use Illuminate\Http\Request;
use Mediapress\Exceptions\NotImageException;
use PhpParser\Node\Expr\Cast\Object_;
use function PHPSTORM_META\type;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic;
use Intervention\Image\Image as Imager;
use Illuminate\Database\Eloquent\Collection;
use Mediapress\FileManager\Models\MFile;


class Image
{
    public const IMAGE = 'image';
    public const METHOD = 'method';
    public const WIDTH = 'width';
    public const HEIGHT = 'height';
    public const ROTATE = 'rotate';
    public const QUALITY = 'quality';
    public $id;
    public $url;
    public $path;
    public $basename;
    public $baseUrl;
    public $extension;
    public $size;

    public $originalUrl;
    public $originalPath;
    public $originalSize;
    public $attributes;
    public $model;
    public $resized = false;
    public $webp = false;
    public $optimize = true;


    public function __construct($imageUrl, $image_id = null)
    {


        if (is_object($imageUrl)) {
            if ($imageUrl instanceof Collection) {
                $imageUrl = $imageUrl->first();
            }

            $this->model = $imageUrl;

            $image_id = $imageUrl->id;

            $imageUrl = $imageUrl->getUrl();
        }

        if (is_numeric($imageUrl)) {

            $this->originalPath = $this->path = getImagePath($imageUrl);

            if (!is_file($this->path)) {
                $this->optimize = false;
                $this->originalPath = $this->path = public_path(defaultImage());
            }
            if (!$image_id) {
                $this->id = $imageUrl * 1;
            }
        } elseif ($imageUrl) {
            if (is_url($imageUrl)) {
                $request = Request::capture();
                $url = parse_url($imageUrl);
                if ($request->getHost() == $url['host']) {
                    $this->optimize = true;
                    $this->originalPath = $this->path = public_path($url['path']);
                } else {
                    $this->optimize = false;
                    $this->originalPath = $this->path = $imageUrl;
                }

            } else {
                if (file_exists(public_path($imageUrl))) {
                    $this->optimize = true;
                    $this->originalPath = $this->path = public_path($imageUrl);
                } else {
                    $this->optimize = false;
                    $this->originalPath = $this->path = $imageUrl;
                    if (!file_exists(public_path($this->originalPath))) {
                        $this->optimize = false;
                        $this->originalPath = $this->path = public_path(defaultImage());
                    }
                }
            }


        } else {
            $this->optimize = false;
            $this->originalPath = $this->path = public_path(defaultImage());
        }

        if (!$this->id && is_numeric($image_id)) {
            $this->id = $image_id * 1;
        }

        if ($this->id && $this->id >= 1 && !$this->model) {
            $this->model = MFile::find($this->id);
        }


        if (config("mediapress.support_webp")) {
            $this->checkWebPSupport();
        }
        if (request()->segment(1) == 'api' && \Str::contains(mime_content_type($this->path), "image")) {
            $this->optimize();
        } else {
            $this->render();
        }

        if ($this->model && $this->model->mimeclass != self::IMAGE) {
            return mp_asset((string)$this->url);
        }

        /*resim yolu public/ içermiyor ve render'da url = null atanıyorsa*/
        if ($this->url == null) {
            $this->url = $this->path;
        }

        if ($this->baseUrl == null) {
            $this->baseUrl = $this->url;
        }

        if (config('mediapress.vue_url')) {
            $this->originalUrl = str_replace(url(''), config('mediapress.vue_url'), $this->baseUrl);
            $this->url = str_replace(url(''), config('mediapress.vue_url'), $this->url);
        } else {
            $this->originalUrl = $this->url;
        }

        $this->setDriver();
    }

    public function setDriver()
    {
        ImageManagerStatic::configure(['driver' => 'gd']);


    }

    public function checkAttributes($attributes)
    {
        if (!isset($attributes[self::METHOD])) {
            $attributes[self::METHOD] = 'fit';
        }
        if (isset($attributes["size"])) {
            $explode = explode("x", str_replace(",", ".", $attributes["size"]));
            if (count($explode) == 2) {
                $attributes[self::WIDTH] = ceil($explode[0]);
                $attributes[self::HEIGHT] = ceil($explode[1]);
            }
        }
        if (isset($attributes['w'])) {
            $attributes[self::WIDTH] = $attributes['w'];
        }
        if (isset($attributes['h'])) {
            $attributes[self::HEIGHT] = $attributes['h'];
        }
        if (!isset($attributes[self::WIDTH])) {
            $attributes[self::WIDTH] = null;
        }
        if (!isset($attributes[self::HEIGHT])) {
            $attributes[self::HEIGHT] = null;
        }
        if (!isset($attributes[self::ROTATE])) {
            $attributes[self::ROTATE] = 0;
        }
        if (!isset($attributes[self::QUALITY])) {
            $attributes[self::QUALITY] = config('mediapress.image_quality');
        }
        return $attributes;
    }

    public function calculateAttributes()
    {
        $this->originalSize = getimagesize($this->originalPath);
        if (!$this->attributes[self::HEIGHT]) {
            if ($this->originalSize[0] != 0) {
                $this->attributes[self::HEIGHT] = (int)number_format(floor($this->attributes[self::WIDTH] * $this->originalSize[1] / $this->originalSize[0]), 0, '', '');
            } else {
                $this->attributes[self::HEIGHT] = null;
            }
        }
        if (!$this->attributes[self::WIDTH]) {
            if ($this->originalSize[1] != 0) {
                $this->attributes[self::WIDTH] = (int)number_format(floor($this->attributes[self::HEIGHT] * $this->originalSize[0] / $this->originalSize[1]), 0, '', '');
            } else {
                $this->attributes[self::WIDTH] = null;
            }
        }
    }

    /**
     * Return re-sized url for ImageManagerStatic url Also if file not exists create file
     *
     * @param array $attributes
     * @return string
     */
    public function resize($attributeArray = [])
    {
        if (!file_exists($this->originalPath) || $this->extension == 'svg') {
            return $this;
        }
        $this->attributes = $this->checkAttributes($attributeArray);

        try {

            if (($this->attributes[self::WIDTH] != null || $this->attributes[self::ROTATE] != null || $this->attributes[self::HEIGHT] != null) && strstr($this->path, '.')) {
                $fileName = explode('.', $this->path);
                $fileName[count($fileName) - 2] = $fileName[count($fileName) - 2] . (($this->attributes[self::WIDTH]) ? '_w' . $this->attributes[self::WIDTH] : '') . (($this->attributes[self::HEIGHT]) ? '_h' . $this->attributes[self::HEIGHT] : '') . (($this->attributes[self::ROTATE]) ? '_r' . $this->attributes[self::ROTATE] : '') . (($this->attributes[self::QUALITY]) ? '_q' . $this->attributes[self::QUALITY] : '');
                $this->path = implode('.', $fileName);
                if (!file_exists($this->path)) {
                    if (file_exists($this->originalPath)) {
                        if (!strstr(mime_content_type($this->originalPath), self::IMAGE)) {
                            return $this->originalPath;
                        }
                        $this->calculateAttributes();

                        if (($this->originalSize[0] < $this->attributes[self::WIDTH] && $this->originalSize[1] < $this->attributes[self::HEIGHT]) || $this->originalSize[0] < $this->attributes[self::WIDTH]) {
                            if ($this->attributes[self::METHOD] == 'resizeCanvas') {
                                ImageManagerStatic::make($this->originalPath)
                                    ->rotate($this->attributes[self::ROTATE])->save($this->path, $this->attributes[self::QUALITY]);
                            } else {

                                ImageManagerStatic::make($this->originalPath)
                                    /*   ->{$this->attributes[self::METHOD]}($this->attributes[self::WIDTH], $this->attributes[self::HEIGHT], function ($constraint) {
                                           //$constraint->upSize();
                                       })*/
                                    ->rotate($this->attributes[self::ROTATE])->save($this->path, $this->attributes[self::QUALITY]);
                            }
                        } else {
                            if ($this->attributes[self::METHOD] == 'resizeCanvas') {
                                ImageManagerStatic::make($this->originalPath)
                                    ->resize($this->attributes[self::WIDTH], (int)number_format(floor($this->attributes[self::WIDTH] * $this->originalSize[1] / $this->originalSize[0]), 0, '', ''))->{$this->attributes[self::METHOD]}($this->attributes[self::WIDTH], $this->attributes[self::HEIGHT], 'top-center')
                                    ->rotate($this->attributes[self::ROTATE])->save($this->path, $this->attributes[self::QUALITY]);
                            } else {
                                if ($this->originalSize[0] * $this->originalSize[1] < 5000 ^ 2) {
                                    ImageManagerStatic::make($this->originalPath)->{$this->attributes[self::METHOD]}($this->attributes[self::WIDTH], $this->attributes[self::HEIGHT], function ($constraint) {
                                        //$constraint->upSize();
                                    })->rotate($this->attributes[self::ROTATE])->save($this->path, $this->attributes[self::QUALITY]);
                                } else {
                                    $this->path = $this->originalPath;
                                }
                            }
                        }
                    } else {

                        self::resize(public_path(defaultImage()), $attributeArray);

                    }
                }
                $this->resized = $this->path;

                return $this->render();
            }

        } catch (\Exception $exception) {
            Log::error($exception);
        }

    }

    public function applyMethod(\Closure $func)
    {
        try {
            $fileName = explode('.', $this->path);
            $closure = new ImageClosure(ImageManagerStatic::make($this->originalPath), $fileName[count($fileName) - 2]);
            $func($closure);
            $this->path = implode('.', $fileName);

            if (!file_exists($this->path)) {
                if (file_exists($this->originalPath)) {
                    if (!strstr(mime_content_type($this->originalPath), self::IMAGE)) {
                        return $this->originalPath;
                    }
                } else {
                    $this->path = public_path(defaultImage());
                    return $this->applyMethod($func);
                }
                $closure->apply();
                $closure->save($this->path, $this->attributes[self::QUALITY] ?? null);
            }
            return $this->render();
        } catch (\Exception $exception) {
            Log::error($exception);
        }
    }

    public static function destroy($imageUrl)
    {
        $fileInfo = new \SplFileInfo($imageUrl);

        $search = File::glob($fileInfo->getPath() . DIRECTORY_SEPARATOR . explode(".", $fileInfo->getFilename())[0] . '_*.' . $fileInfo->getExtension(), GLOB_NOSORT);

        if ($search) {
            $results = preg_grep('/\_[wh]\d{1,4}/', $search);

            if ($results) {
                return File::delete($results);
            }
        }

        return false;
    }

    public function render()
    {

        if (file_exists($this->path)) {
            $this->size = @getimagesize($this->path);
        }
        $explode = explode('public/', str_replace(['\\', '///', '//'], '/', $this->path));
        if (isset($explode[1])) {
            if (config('mediapress.vue_url')) {
                $this->url = str_replace(url(''), config('mediapress.vue_url'), url($explode[1]));
            } else {
                $this->url = url($explode[1]);
            }
            $this->baseUrl = '/' . $explode[1];
        }
        $basename = basename($this->path);
        $this->extension = substr($basename, strrpos($basename, '.') + 1);
        $this->basename = mb_substr($basename, 0, -1 * (strlen($this->extension) + 1));


        return $this;
    }


    /**
     * @param array $attributes
     * @param array $options <p>
     * lazyLoad => true,
     * lazyLoadImagePath => defaultLazyImage(),
     * defaultSizeAttribute => true
     * </p>
     * @return string
     * @throws \Exception
     */
    public function html($attributes = array(), $options = array())
    {
        if ($this->optimize && $this->extension !== 'svg' && \Str::contains(mime_content_type($this->path), "image") && request()->segment(2) != 'FileManager') {
            $this->optimize();
        }
        $src = $this->url;
        $tempAttributes = "";

        if (isset($options['lazyLoad']) && $options['lazyLoad']) {
            $attributes['class'] = (isset($attributes['class']) ? $attributes['class'] . ' ' : '') . 'lazy';
            $src = $options['lazyLoadImagePath'] ?? defaultLazyImage();
            $dataSrc = $this->url;
        }

        if (!isset($attributes['alt'])) {
            $mediapress = mediapress();
            if ($this->model && $this->model->detail()['tag'] != '') {
                $alt = $this->model->detail()['tag'];
            } elseif ($mediapress->relation) {
                $alt = $mediapress->relation->name;
            } else {
                $alt = env('SITE_TITLE');
            }

            $attributes['alt'] = strip_tags($alt);
        }

        if (isset($options['defaultSizeAttribute']) && $options['defaultSizeAttribute']) {
            if (!isset($attributes['width']) && isset($this->size[0])) {
                $attributes['width'] = $this->size[0] ?? '';
            }
            if (!isset($attributes['height']) && isset($this->size[1])) {
                $attributes['height'] = $this->size[1];
            }
        }

        if(adminAuth() && !isMobile() && $this->model && $this->model->pivot) {
            $attributes['data-check'] = 'quickAccess';
            $attributes['data-checkId'] = $this->model->pivot->id;
        }

        foreach ($attributes as $key => $value) {
            $tempAttributes .= $key . '="' . $value . '" ';
        }

        return '<img src="' . $src . '" ' . (isset($dataSrc) ? 'data-src="' . $dataSrc . '" ' : '') . $tempAttributes . '>';
    }

    public function __toString()
    {

        if ($this->optimize && $this->extension !== 'svg' && \Str::contains(mime_content_type($this->path), "image") && request()->segment(2) != 'FileManager') {
            $this->optimize();
        }

        return mp_asset((string)$this->url);
    }

    private function checkWebPSupport()
    {
        if (session('webp')) {
            $this->webp = session('webp');
        } else {
            $browser = new Browser();
            $this->webp = $browser->supportWebP();
            session(['webp' => $this->webp]);
        }
    }

    public function optimize()
    {

        $fileName = explode('.', $this->path);
        $fileName[count($fileName) - 2] = $fileName[count($fileName) - 2] . '_op';
        $this->path = implode('.', $fileName);
        if ($this->webp) {
            $this->path = mb_substr($this->path, 0, -1 * (strlen($this->extension) + 1)) . '.webp';
            $this->extension = 'webp';
        }

        if (!file_exists($this->path)) {
            $this->makeOptimized();
        }

        $explode = explode('public/', str_replace(['\\', '///', '//'], '/', $this->path));
        if (isset($explode[1])) {

            $this->url = url($explode[1]);
        }


    }

    private function makeOptimized()
    {
        $this->render();
        if (strpos($this->basename, 'default') === false) {
            if (!file_exists($this->path)) {

                if (!$this->attributes) {
                    $this->attributes = $this->checkAttributes([]);
                }
                if ($this->resized) {
                    $path = $this->resized;
                } else {
                    $path = $this->originalPath;
                }
                $img = ImageManagerStatic::make($path);

                $img->encode($this->extension, $this->attributes[self::QUALITY]);
                $img->save($this->path);

            }
        }


    }

}
