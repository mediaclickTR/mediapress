<?php

return [
    'yes' => "Yes",
    'no' => "No",
    'select' => "Select",
    'close' => "Close",
    'save' => "Save",
    'active' => "Active",
    'passive' => "Passive",
];
