@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }

        .post-photo{
            width: 200px;
            height: auto;
        }

        .post{
            margin-top: 20px;
        }

        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title" style="">
            <div class="float-left">
                Instagram Verilerini Eşitle
            </div>
            <div class="float-right" >
                <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.allposts') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Tüm Medyalar</a>
            </div>
        </div>
        <div class="table-field">
            <div style="margin-top: 70px !important; border-top: 1px solid #111;">
                <div class="container" style="margin-top: 20px !important;">
                    <div class="row inline justify-content-md-center">
                        <div class="col">
                            <label for="formGroupExampleInput">Verileri Kullanıcı Adı Üzerinden Senkronize Et</label>
                            <input id="input-user" type="text" class="form-control" placeholder="instagram" value="aysirawedding">
                        </div>

                        <div class="col">
                            <a role="button" id="sync-user" href="javascript:void(0);" class="btn btn-primary form-control" style="margin-top: 30px;">
                                <i class="fas fa-sync"></i>Eşitle
                            </a>
                        </div>
                    </div>

                    <div id="postsDiv">

                    </div>

                    <div class="row inline justify-content-md-center mt-5">
                        <div class="col">
                            <label for="formGroupExampleInput">Verileri # Üzerinden Senkronize Et</label>
                            <input id="input-hashtag" type="text" class="form-control" placeholder="#instagram" value="aysiragelini">
                        </div>

                        <div class="col">
                            <a id="sync-hashtag" role="button" class="btn btn-primary form-control" style="margin-top: 30px;">
                                <i class="fas fa-sync"></i>Eşitle
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $("#sync-hashtag").click(function (e) {
            e.preventDefault();
            $("#sync-hashtag i").addClass("turn");
            var hashtag = $("#input-hashtag").val();
            $.post( "{!! route("Content.instagram.syncHashtag") !!}", {
                "hashtag": hashtag,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-hashtag i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });

        $("#sync-user").click(function (e) {
            e.preventDefault();
            $("#sync-user i").addClass("turn");
            var user = $("#input-user").val();
            $.post( "{!! route("Content.instagram.syncUser") !!}", {
                "user": user,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-user i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });
    </script>
@endpush
