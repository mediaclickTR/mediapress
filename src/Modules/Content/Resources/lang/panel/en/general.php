<?php

return [

    "status_title" => [
        'passive' => 'Passive',
        'active' => 'Active',
        'draft' => 'Draft',
        'predraft' => 'Pre-Draft',
        'pending' => 'Pending',
        'postdate' => 'Post-Dated',
    ],

    'status' => 'Status',
    'order' => 'Order',
    'name' => 'Name',
    'added_by' => 'Added By',
    'created_at' => 'Created Date',
    'updated_at' => 'Last Updated Date',
    'actions' => 'Actions',

    "add_new" => "Add New",

    "selection" => "Please Select",
    "edit" => "Edit",
    "delete" => "Delete",
    "cancel" => "Cancel",
    'save' => 'Save',


    'go_back_list' => '&nbsp;&nbsp;Return back to the List Page',



    "swal" => [
        'are_you_sure' => 'Are you sure?',
    ],
    'yes' => 'Yes',
    'no' => 'No',

    "no_data" => "No recorded data found!",


    'max_file_size' => 'Max file size :size MB',


    //----------------------

    'select' => 'Select',
    'protected_content' => 'Password Protected Content',
    "protected_content_login" => "Move to page",
    'postdate_publish' => 'Post-Dated Publish',

    'save_as' => [
        'passive' => 'Save as Passive',
        'active' => 'Save and Publish',
        'draft' => 'Save as Draft',
        'postdate' => 'Save as Post-Dated',
    ]
];
