<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateAdminGoogle2faTable extends Migration
{
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('admins', function (Blueprint $table) {
            $table->string('google2fa')->nullable()->after('api_token');
        });
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('admins', function (Blueprint $table) {
            $table->dropColumn('google2fa');
        });
    }
}
