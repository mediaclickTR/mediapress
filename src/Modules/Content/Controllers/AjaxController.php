<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\DB;
use Mediapress\Facades\Slots\MessagingSlot;
use Mediapress\Facades\URLEngine;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\Country;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\CountryGroupCountry;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Facades\MetaEngine;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\Content\Facades\PropertyEngine;
use Mediapress\Modules\Content\Facades\CriteriaEngine;
use Mediapress\Modules\Content\Facades\CategoryEngine;

class AjaxController
{
    public $data = [];

    public function __construct()
    {
        $request = Request::capture()->all();
        if(isset($request['data'])){
            $this->data =  $request["data"];
        }
    }
}