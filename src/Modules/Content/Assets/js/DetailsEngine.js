//custom event polyfill
(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();


const DetailsEngine = function ($params) {

    const engine = this;

    let default_params = {
        capsules_container: $("html"),
        slug_lookup_url: "/mp-admin/Content/checkUrl",
        meta_variables_url: "/mp-admin/Content/getMetaVariablesFor"
    };


    this.params = $.extend(default_params, $params);

    this.init = function () {
        $(".detail-capsule", $(engine.params.capsules_container)).each(function (ind, el) {
            let slugworker = new SlugWorker({
                "capsule": $(el),
                "url": engine.params.slug_lookup_url,
                "details_engine": engine
            });
            $(el).data("detail-slug-worker", slugworker);

            let metaworker = new MetaWorker({
                "capsule": $(el),
                "url": engine.params.meta_variables_url,
                "details_engine": engine
            });
            $(el).data("detail-meta-worker", metaworker);

            $(".detail-name", el).on("change", function (e) {
                var event = new CustomEvent("detailNameChanged", {bubbles:true, cancelable:true, detail:{DOMElement: this}});
                document.dispatchEvent(event);
            }).trigger("change");

            $(".detail-detail", el).on("change", function (e) {
                const event = document.createEvent('CustomEvent');
                event.initCustomEvent('detailDetailChange', true, true, {DOMElement: this, originalEvent: e});
                document.dispatchEvent(event);
            }).trigger("change");

            $(".slug-control", el).on("change", function (e) {
                const event = document.createEvent('CustomEvent');
                event.initCustomEvent('detailSlugChange', true, true, {DOMElement: this, originalEvent: e});
                document.dispatchEvent(event);
            }).trigger("change");

        });
        return engine;
    };
};


const SlugWorker = function ($params) {


    const worker = this;

    let default_params = {
        capsule: null,
        url: "",
        details_engine: null
    };

    this.params = $.extend(default_params, $params);

    this.capsule = null;
    this.setCapsule = function (capsule) {
        worker.capsule = capsule;
    };
    this.getCapsule = function () {
        return $(worker.capsule);
    };

    this.url = null;
    this.setUrl = function (url) {
        worker.url = url;
    };
    this.getUrl = function () {
        return worker.url;
    };

    this.engine = null;
    this.setEngine = function (engine) {
        worker.engine = engine;
    };
    this.getEngine = function () {
        return worker.engine;
    };


    this.control = null;

    this.init = function () {
        worker.setCapsule($(worker.params.capsule));
        worker.control = $(".detail-slug-control", worker.capsule);
        worker.setUrl(worker.params.url);
        worker.setEngine(worker.params.details_engine);
        worker.tieHandlers();
        return worker;
    };

    this.pickDetailDataFromCapsule = function () {
        return {
            "detail_type": $(worker.capsule).data("detail-type"),
            "detail_id": $(worker.capsule).data("detail-id"),
            "parent_id": $(worker.capsule).data("parent-id"),
            "language_id": $(worker.capsule).data("language-id"),
            "country_group_id": $(worker.capsule).data("country-group-id")
        };
    };

    this.getValidSlug = function (sluggable_text) {
        let detail_data = worker.pickDetailDataFromCapsule();
        let token = $("meta[name='csrf-token']").attr("content");
        let newslug = $.ajax({
            url: worker.url,//http://pilot.local/mp-admin/Content/Pages/checkUrl
            method: 'POST',
            async: false,
            dataType: "json",
            data: {
                "_token": token,
                "detail_type": detail_data.detail_type,
                "detail_id": detail_data.detail_id,
                "parent_id": detail_data.parent_id,
                "language_id": detail_data.language_id,
                "country_group_id": detail_data.country_group_id,
                "sluggable_text": sluggable_text
            }
        }).responseJSON;
        return newslug;

    };

    this.updateSlugFromSource = function () {

        let value = $(".detail-name", worker.capsule).val();
        let detail_data = worker.pickDetailDataFromCapsule(worker.capsule);
        let old_slug = $(".slug-control", $(worker.capsule)).val();
        let new_slug = worker.getValidSlug(value, detail_data);
        $(".slug-control", $(worker.capsule)).val("/"+ new_slug.slug).data("prefix", new_slug.pattern).addClass("focus");

    };

    this.tieHandlers = function () {

        $(".detail-name", worker.capsule).on("change", function (e) {

            let autoupdate = $("input.autoupdate", worker.control).prop("checked");

            if (!autoupdate) {
                return;
            }
            worker.updateSlugFromSource();

        });

        $(".autoupdate", worker.control).on("ifChanged change", function (e) {
            if ($(this).prop("checked")) {
                $(".manual-slug-btn", worker.control).prop("disabled", true);
                worker.updateSlugFromSource();
            } else {
                $(".manual-slug-btn", worker.control).prop("disabled", false);
            }
        });

        $(".manual-slug-btn", worker.control).on("click", function (e) {
            let inputValue = $(".slug-control", $(worker.capsule)).val();
            const {value: email} = Swal.fire({
                title: 'Slug ya da slug\'laştırılmak üzere metin girin:',
                input: 'text',
                inputValue: inputValue,
                inputPlaceholder: 'Yeni bir slug değeri belirtin',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Vazgeç'
            }).then((result) => {
                if (result.value) {
                    var detail_data = worker.pickDetailDataFromCapsule(worker.capsule);
                    var new_slug = worker.getValidSlug(result.value, detail_data);
                    Swal.fire({
                        title: 'Slug atansın mı?',
                        html:
                          'Girdiğiniz değer: <pre><code>' +
                          JSON.stringify(result.value) +
                          '</code></pre></br>' +
                          ' için uygun slug değeri:' +
                          '<code><pre>/' + new_slug.slug + '</pre></code>',
                        confirmButtonText: 'Onayla',
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'İptal Et'
                    }).then((confirm_question) => {
                        if (confirm_question.value) {
                            $(".slug-control", $(worker.capsule)).val("/" + new_slug.slug).data("prefix", new_slug.pattern).addClass("focus");
                        }
                    });
                }
            });
        });

        $(".sync-slug-btn", worker.control).on("click", function () {
            worker.updateSlugFromSource();
        });

    };

    return worker.init();


};


const MetaWorker = function ($params) {

    const worker = this;

    let default_params = {
        capsule: null,
        url: "",
        details_engine: null
    };

    this.params = $.extend(default_params, $params);


    this.storage = {
        "detail_data": {
            "detail_type": null,
            "detail_id": null,
            "parent_id": null,
            "language_id": null,
            "country_group_id": null,
            "manual_meta": null,
        },
        "manual_meta": null,
        "meta_variables": {},
        "meta_templates": {},
        "realtime_meta_variables": {},
        "realtime_meta_templates": {},
        "extracted_meta_variables": {},
        "flat_meta_variables": {},
        "meta_dom": null,
    };

    this.setManualMeta = function (value) {
        const old_value = worker.storage.manual_meta;
        worker.storage.manual_meta = value;
        if (old_value !== value) {
            worker.metaModChanged(old_value, value);
        }
        return worker.storage.manual_meta;
    };
    this.getManualMeta = function () {
        return worker.storage.manual_meta;
    };

    this.setRealtimeMetaVariable = function (varkey, value) {
        worker.storage.realtime_meta_variables[varkey] = value;
    };
    this.getRealtimeMetaVariable = function (varkey) {
        return typeof worker.storage.realtime_meta_variables[varkey] == "undefined" || worker.storage.realtime_meta_variables[varkey];
    };

    this.setRealtimeMetaTemplate = function (key, value) {
        worker.storage.realtime_meta_templates[key] = value;
    };
    this.getRealtimeMetaTemplate = function (key) {
        return typeof worker.storage.realtime_meta_templates[key] == "undefined" || worker.storage.realtime_meta_templates[key];
    };

    this.setMetaDom = function (value) {
        worker.storage.meta_dom = value;
    };
    this.getMetaDom = function () {
        return worker.storage.meta_dom;
    };

    this.setDetailData = function (value) {
        worker.storage.detail_data = value;
    };
    this.getDetailData = function () {
        return worker.storage.detail_data;
    };




    this.capsule = null;
    this.setCapsule = function (capsule) {
        worker.capsule = capsule;
    };
    this.getCapsule = function () {
        return $(worker.capsule);
    };

    this.url = null;
    this.setUrl = function (url) {
        worker.url = url;
    };
    this.getUrl = function () {
        return worker.url;
    };

    this.engine = null;
    this.setEngine = function (engine) {
        worker.engine = engine;
    };
    this.getEngine = function () {
        return worker.engine;
    };

    this.control = null;

    this.init = function () {
        worker.setCapsule($(worker.params.capsule));
        //worker.control = $(".detail-slug-control", worker.capsule);
        worker.setUrl(worker.params.url);
        worker.setEngine(worker.params.details_engine);
        worker.helpers.pickDetailDataFromCapsule();
        worker.storage.meta_variables = $(worker.capsule).data("meta-variables");
        worker.storage.meta_templates = $(worker.capsule).data("meta-templates");
        worker.helpers.detectMetaDOM();
        worker.helpers.extractMetaVariables();
        worker.tieHandlers();
        worker.setManualMeta($(".meta-mode-toggler", worker.storage.meta_dom).val());
        worker.fillMetaControlsModal();
        worker.previewSeo();
        worker.convertVariablesToBoxes();
        console.log("Meta Worker Log:", worker);
        return worker;
    };

    this.fillMetaControlsModal = function(){
        //console.log(worker.storage.extracted_meta_variables);
        $('.sModal ul>li',worker.storage.meta_dom).remove();
        $.each(worker.storage.flat_meta_variables,function(ind,el){
            $('.sModal ul',worker.storage.meta_dom).append('<li data-meta-key="'+ind+'" data-meta-value="'+worker.storage.extracted_meta_variables[ind].value+'" title="'+worker.storage.extracted_meta_variables[ind].description+'">'+worker.storage.extracted_meta_variables[ind].title+'</li>');
        });
        //console.log(worker);
        //$('.sModal ul',worker.storage.meta_dom);
    };


    this.convertVariablesToBoxes = function(){
        $('.divInput', worker.dom_el).each(function(ind,el){
            var value = rhtmlspecialchars($(el).html());
            $.each(worker.storage.extracted_meta_variables, function(meta_key, meta_data){
                var re = new RegExp('%'+meta_key+'%',"g");
                if(re.test(value)){
                    var box = worker.createVariableBox(meta_key, meta_data.value, meta_data.title, meta_data.description);
                    value = value.replace(re, box);
                }
            });
            $(el).html(value);
        });
    };

    this.createVariableBox = function(meta_key, meta_value, meta_title, meta_desc){
        return '<span contenteditable="false" data-meta-key="'+meta_key+'" data-meta-value="'+meta_value+'" title="'+meta_desc+'">'+meta_title+'<i>x</i></span><em contenteditable="false">&nbsp;</em>';
    };


    this.previewSeo = function () {
        let templates = null;
        if (worker.getManualMeta() === "1") {
            templates = worker.storage.realtime_meta_templates;
        } else {
            templates = worker.storage.meta_templates;
        }

        let title_resolved = worker.resolveTemplate(templates.title);
        let description_resolved = worker.resolveTemplate(templates.description);

        $(".seo-preview-title > i").html(title_resolved);
        $(".seo-preview-title > span").text("url burada olacak");
        $(".seo-preview-title").siblings("p").html(description_resolved);
    };

    this.metaModChanged = function (old_value, new_value) {
        if(new_value==1){
            $("input.meta-text-input", worker.storage.meta_dom).change();
        }
        worker.previewSeo();
    };

    this.resolveTemplate = function (template) {

        if( ! template){
            return "";
        }
        RegExp.quote = function(str) {
            return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
        };
        let regex = null;
        $.each(worker.storage.realtime_meta_variables,function(key,value){
            regex ='%'+ key + '%';
            var regobj = new RegExp(RegExp.quote(regex),"g");
            template.desktop_value = template.desktop_value.replace(regobj,value);
           // template.mobile_value = template.mobile_value.replace(regobj,value);
        });
        $.each(worker.storage.flat_meta_variables,function(key,value){
            regex ='%'+ key + '%';
            var regobj = new RegExp(RegExp.quote(regex),"g");
            template.desktop_value = template.desktop_value.replace(regobj,value);
          //  template.mobile_value = template.mobile_value.replace(regobj,value);
        });

        return template;


        //return "resolved text for template: " + template + " " + Math.random();
    };

    this.helpers = {

        "extractMetaVariables": function () {

            const variables = {};
            const flat_variables = {};
            $.each(worker.storage.meta_variables, function (metagroupind, metagroup) {
                $.each(metagroup.variables, function (varname, variable) {
                    variables[varname] = variable;
                    flat_variables[varname] = variable.value;
                });
            });
            worker.storage.extracted_meta_variables = variables;
            worker.storage.flat_meta_variables = flat_variables;

        },

        "pickRealtimeMetaTemplates": function () {
            const mmc = {
                "title": "",
                "description": "",
                "keywords": ""
            };

            if (worker.storage.meta_dom.length) {
                let field = $(".meta-title", worker.storage.meta_dom);
                mmc.title = field.length ? field.val() : null;
                field = $(".meta-title-mobile", worker.storage.meta_dom);
                mmc.title_mobile = field.length ? field.val() : null;
                field = $(".meta-description", worker.storage.meta_dom);
                mmc.description = field.length ? field.val() : null;
                field = $(".meta-keywords", worker.storage.meta_dom);
                mmc.keywords = field.length ? field.val() : null;
            }
            worker.storage.realtime_meta_templates = mmc;
        },

        "detectMetaDOM": function () {
            let classname = worker.storage.detail_data.detail_type.replace(/\\/g, '\\\\');
            let dom = $('.detail-metas-control-v2[data-detail-type="' + classname + '"][data-detail-id="' + worker.storage.detail_data.detail_id + '"]');
            //console.log(dom);
            worker.setManualMeta($(".meta-mode-toggler", dom).val());
            worker.setMetaDom(dom);
        },

        "pickDetailDataFromCapsule": function () {
            worker.setDetailData({
                "detail_type": $(worker.capsule).data("detail-type"),
                "detail_id": $(worker.capsule).data("detail-id"),
                "parent_type": $(worker.capsule).data("parent-type"),
                "parent_id": $(worker.capsule).data("parent-id"),
                "language_id": $(worker.capsule).data("language-id"),
                "country_group_id": $(worker.capsule).data("country-group-id")
            });
        }

    };


    this.tieHandlers = function () {
        //alert("tying handlers");
        $(".detail-name", worker.capsule).on("change", function (e) {

            //console.log($('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).length);
            $('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).html($(this).val());

            let dt = worker.storage.detail_data.detail_type;
            let recognized_key = null;

            if (dt === "Mediapress\\Modules\\Content\\Models\\PageDetail") {
                recognized_key = "page_title";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\SitemapDetail") {
                recognized_key = "sitemap_title";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\CategoryDetail") {
                recognized_key = "category_title";
            }

            if (recognized_key !== null) {
                worker.setRealtimeMetaVariable(recognized_key, $(this).val());
            }

        });

        $(".detail-detail", worker.capsule).on("change", function (e) {

            $('.seo-preview-box .seo-preview-title>i', worker.storage.meta_dom).html($(this).val());

            let dt = worker.storage.detail_data.detail_type;
            let recognized_key = null;

            if (dt === "Mediapress\\Modules\\Content\\Models\\PageDetail") {
                recognized_key = "page_detail";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\SitemapDetail") {
                recognized_key = "sitemap_detail";
            } else if (dt === "Mediapress\\Modules\\Content\\Models\\CategoryDetail") {
                recognized_key = "category_detail";
            }

            if (recognized_key !== null) {
                worker.setRealtimeMetaVariable(recognized_key, $(this).val());
            }
        });

        $(".meta-mode-toggler", worker.storage.meta_dom).change(function () {
            worker.setManualMeta($(this).val());
            worker.previewSeo();
            /*console.log(worker.storage.flat_meta_variables);
            console.log(worker.storage.realtime_meta_variables);
            console.log(worker.storage.meta_templates);
            console.log(worker.storage.realtime_meta_templates);*/
        });


        $("input.meta-text-input", worker.storage.meta_dom).change(function () {
            worker.setRealtimeMetaTemplate($(this).data("template-key"),$(this).val());
            worker.previewSeo();
        });
        $(".meta-mode-toggler", worker.storage.meta_dom).change();

    };

    return worker.init();

};


const DetailSlugControlV2 = function(domElement){
    var worker = this;
    this.last_response = {pattern:"", slug:""};
    this.dom_el = null;
    this.detail_data={
        detail_type : null,
        detail_id : null,
        parent_id : null,
        language_id : null,
        country_group_id : null
    };

    this.last_confirmed_slug = "";

    this.init = function(domElement){
        worker.dom_el = $(domElement);
        worker.pickDetailData();
        worker.tieHandlers();
        worker.dom_el.data("slug_worker",worker);

        return worker;
    };


    this.pickDetailData = function(){
        worker.last_response.slug =  worker.last_confirmed_slug = $(worker.dom_el).data("last-confirmed");

        worker.detail_data.detail_type = $(worker.dom_el).data('detail-type');
        worker.detail_data.detail_id = $(worker.dom_el).data('detail-id');
        worker.detail_data.parent_type = $(worker.dom_el).data('parent-type');
        worker.detail_data.parent_id = $(worker.dom_el).data('parent-id');
        worker.detail_data.language_id = $(worker.dom_el).data('language-id');
        worker.detail_data.country_group_id = $(worker.dom_el).data('country-group-id');

        return worker;
    };

    this.getValidSlug = function (sluggable_text, source_identifier) {
        let token = $("meta[name='csrf-token']").attr("content");
        let newslug = $.ajax({
            url: '/mp-admin/Content/checkUrl',
            method: 'POST',
            async: false,
            dataType: "json",
            data: {
                "_token": token,
                "detail_type": worker.detail_data.detail_type,
                "detail_id": worker.detail_data.detail_id,
                "parent_id": worker.detail_data.parent_id,
                "language_id": worker.detail_data.language_id,
                "country_group_id": worker.detail_data.country_group_id,
                "sluggable_text": sluggable_text
            },
            success:function(data, textStatus, jqXHR){
                worker.dom_el.attr("data-last-confirmed",data.slug);
            },
            error: function(jqXHR, textStatus, errorThrown){
                alert("Slug yenilenemedi: " + "\n"  + "\n"+ errorThrown + "\n" + (jqXHR.responseJSON.message !== undefined ? jqXHR.responseJSON.message : errorThrown));
                worker.cancel();
            }
        }).responseJSON;
        console.log("newslug",newslug);
        if(newslug.exception === undefined){
            worker.last_response = newslug;
            return newslug;
        }else{
            return worker.last_response;
        }
    };

    /*function applySlug(text_to_slug) {

        //var val = $('.slug-control .slug-input').val();
        var _slug = worker.getValidSlug(text_to_slug);
        $('.slug-input', worker.dom_el).val(_slug);

    }*/

    this.applySlug = function(text_to_slug, source_identifier){
        // source identifier: btn-confirm, external_code, title_sync
        var new_slug = worker.getValidSlug(text_to_slug, source_identifier);
        $(".slug-input",worker.dom_el).val(new_slug.slug);
        return worker;

    };

    this.editMode = function() {
        $(worker.dom_el).find('.btn-confirm, .btn-cancel').removeClass('d-none');
        $('.btn-edit, .loading', worker.dom_el).addClass('d-none');
        worker.inputEditModeOn();
        return worker;
    };

    this.waitingMode = function(){
        $('.btn-confirm, .btn-cancel, .loading',worker.dom_el).addClass('d-none');
        $('.btn-edit', worker.dom_el).removeClass('d-none');
        worker.inputEditModeOff();
        return worker;
    };

    this.loadingMode =  function() {
        $('.btn-confirm, .btn-cancel, .btn-edit',worker.dom_el).addClass('d-none');
        $('.loading', worker.dom_el).removeClass('d-none');
        worker.inputEditModeOff();
        return worker;
    };

    this.inputEditModeOff = function () {
        $('.slug-input', worker.dom_el).attr('readonly', 'readonly').addClass('bg-light')/*.addClass('text-white')*/;
        return worker;
    };

    this.inputEditModeOn = function () {
        $('.slug-input', worker.dom_el).removeAttr('readonly').removeClass('bg-light')/*.removeClass('text-white')*/;
        return worker;
    };

    this.setFocusOnInput = function(){
        let fld_length = $('.slug-input',worker.dom_el).val().length;
        $('.slug-input', worker.dom_el).focus()[0].setSelectionRange(fld_length, fld_length);
        return worker;
    };

    this.edit = function(){
        worker.editMode().setFocusOnInput();
        return worker;
    };

    this.cancel = function(){
        $(".slug-input", worker.dom_el).val(worker.dom_el.attr("data-last-confirmed"));
        worker.waitingMode();
        return worker;
    };

    this.confirm = function(){
        return worker.loadingMode().applySlug($('.slug-input', worker.dom_el).val(),"btn-confirm").waitingMode();
    };

    this.tieHandlers = function(){

        $(document).ready(function(){
            worker.waitingMode();
        });

        $('.btn-edit',worker.dom_el).click(function(e) {
            worker.edit();
        });

        $('.btn-cancel', worker.dom_el).click(function(e) {
            worker.cancel();
        });

        $('.btn-confirm', worker.dom_el).click(function(e) {
            worker.dom_el.attr("data-mode","manual");
            worker.confirm();
        });



        var detail_name = '.detail-capsule[data-detail-type="' + worker.detail_data.detail_type.replace(/\\/g, '\\\\') + '"][data-detail-id="' + worker.detail_data.detail_id + '"] .detail-name';
        $(detail_name).change(function(){
            if(worker.dom_el.attr("data-mode")=="auto"){
                if($(this).val()){
                    worker.loadingMode();
                    worker.applySlug($(this).val(), "title_sync");
                    worker.waitingMode();
                }
            }
        });

    };



    return worker.init(domElement);


};



$(document).ready(function () {

    $(".slug-control-v2").each(function (ind, el) {
        var slugger = new DetailSlugControlV2(el);
    });
});
