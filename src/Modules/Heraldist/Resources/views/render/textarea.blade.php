<div {!!  $field['divAttrs']  !!} >
    @if(isset($field['label']) && $field['label'])
        {!! Form::label($field['name'],$field['label'].(isset($field['required']) && $field['required'] ? ' *':''))  !!}
    @endif

    {!!  Form::textarea($field['name'], old($field['name'],$field['value']), $field['attr'])  !!}
    @if(isset($field['description']) && $field['description'])
        <small class="form-text text-muted">{!! $field['description'] !!}</small>
    @endif
</div>