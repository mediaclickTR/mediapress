<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\MPCore\Models\BaseModel;

class MenuDetail extends BaseModel
{

    use SoftDeletes;

    protected $table = 'menu_details';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = ["menu_id", "language_id", "country_group_id", "url_id", "type", "target", "name", "parent", "type_view", "out_link", 'lft', 'rgt', "depth", "draft", "status", "file_id"];

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    public function parentModel()
    {
        return $this->belongsTo(MenuDetail::class, 'parent', 'id');
    }

    public function children()
    {
        return $this->hasMany(MenuDetail::class, 'parent', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function url()
    {
        return $this->belongsTo(Url::class);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }

    public function getNameAttribute($value)
    {
        return supFront($value, 'name', $this);
    }
}
