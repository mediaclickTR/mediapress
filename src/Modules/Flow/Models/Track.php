<?php

namespace Mediapress\Modules\Flow\Models;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $connection = 'mediapress_flow';
    protected $table = 'tracks';

    protected $fillable=[
        'action_name',
        'idsite',
        'rec',
        'r',
        'h',
        'm',
        's',
        'url',
        'urlref',
        '_id',
        '_idts',
        '_idvc',
        '_idn',
        '_refts',
        '_viewts',
        'send_image',
        'pdf',
        'qt',
        'realp',
        'wma',
        'dir',
        'fla',
        'java',
        'gears',
        'ag',
        'cookie',
        'res',
        'gt_ms',
        'pv_id',
    ];

}
