<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('page_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->index();
            $table->unsignedInteger('language_id')->nullable();
            $table->unsignedInteger('country_group_id')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->text('detail');

            $table->integer("status")->default(3);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_details');
    }
}
