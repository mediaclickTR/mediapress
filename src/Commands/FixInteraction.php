<?php

namespace Mediapress\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class FixInteraction extends Command
{
    protected $signature = 'mp:interaction';


    public function handle()
    {
        if(config('mediapress.interaction.driver', 'sqlite') == 'sqlite' && Schema::connection("mediapress_interaction")->hasColumn('cookies','operation_system_id')) {
            \DB::connection('mediapress_interaction')
                ->statement("ALTER TABLE `cookies` RENAME to `cookies_bak`;");

            \DB::connection('mediapress_interaction')
                ->statement("CREATE TABLE cookies(id INTEGER, hash TEXT, user_id INTEGER, device_id INTEGER, operating_system_id INTEGER, browser_id INTEGER, created_at TEXT);");

            \DB::connection('mediapress_interaction')
                ->statement("INSERT INTO cookies(id, hash, user_id, device_id, operating_system_id, browser_id, created_at) SELECT id, hash, user_id, device_id, operation_system_id, browser_id, created_at FROM cookies_bak;");

            \DB::connection('mediapress_interaction')
                ->statement("DROP TABLE cookies_bak;");

            $this->info(PHP_EOL . "Interaction Fixed !!". PHP_EOL);
        }
    }


}
