@extends('ContentPanel::inc.module_main')
@push("styles")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/selector.css")}}"/>
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/cropper.min.css")}}"/>
@endpush

@php
    $details = $page->details()->whereHas('url', function ($q) {
                $q->where('type', 'original');
            })->get();

    $subPages = $page->sitemap->children;
@endphp

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    <span>
                        @if(request()->has("isNew") && request()->isNew=="yes")
                            {!! __("ContentPanel::page.new_page") !!}
                        @else
                            {!! __("ContentPanel::page.edit_page") !!}
                        @endif
                    </span>
                    <small class="text-muted text-truncate d-block" title="{{ optional($page->detail)->name ?? "" }}">{{ $page->detail ? \Str::limit($page->detail->name, 50) : "" }}</small>
                </div>
            </div>
            @if($details->count() > 0)
                <div class="float-right ml-2">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {!! __('ContentPanel::page.goto_page') !!}
                        </button>
                        <div class="dropdown-menu">
                            @foreach($details as $pageDetail)
                                <a class="dropdown-item" href="{{ url($pageDetail->url) . '?preview=1' }}" target="_blank">{{ strtoupper($pageDetail->countryGroup->code) . ' (' . strtoupper($pageDetail->language->code) . ')' }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if($subPages->count() > 0)
                <div class="float-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle ml-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {!! __('ContentPanel::page.page_content') !!}
                        </button>
                        <div class="dropdown-menu">
                            @foreach($subPages as $subPage)
                                <a class="dropdown-item"
                                   href="{{ route('Content.pages.index', ['sitemap_id' => $subPage->id, 'page_id' => $page->id]) }}">{{ $subPage->detail->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

            <div class="float-right">
                @include('MPCorePanel::inc.backup',['object'=>$page])
            </div>

            @if(config('app.env') == 'local')
                @php
                    $temp = str_replace(["App\\", "\\"], ["", "/"], $renderable->class) . '.php';
                @endphp
                <div class="float-right">
                    <a href="{!! route('admin.editors.edit',encrypt(app_path($temp)) ) !!}" target="_blank" class="btn btn-sm btn-secondary">Renderable</a>
                </div>
            @endif
            <div class="float-right">
                <a href="{{ route('Content.pages.index', ['sitemap_id' => $page->sitemap_id]) }}{{  $page->page_id ? '?page_id=' . $page->page_id : '' }}" class="btn btn-sm btn-light mr-2">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="pages float-left w-100 p-30">
            @php /** @var \Mediapress\AllBuilder\Foundation\BuilderRenderable $renderable */ @endphp
            @if( isset($renderable) && is_a($renderable, \Mediapress\AllBuilder\Foundation\BuilderRenderable::class))
                @php $renderable->render(); @endphp
                @php
                    $push = $renderable->getStacks();
                @endphp
                @foreach($push as $stack => $p)
                    @push($stack) {!! implode("\n",$p) !!} @endpush
                @endforeach
            @endif
        </div>
    </div>

    @if(session('pageCreate'))
        @include('ContentPanel::inc.page_create_success_popup', ['page' => $page, 'createRoute' => $createRoute])
    @endif

    @include('ContentPanel::inc.copy_language_popup', ['page' => $page])
@endsection

@push('styles')
    <style>
        .boxes {
            border: 1px solid #C2C2C2;
            padding: 30px;
            float: left;
            width: 100%;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
        }
        .boxes h2 {
            margin: 0;
            line-height: 20px;
        }
        .boxes h2 a {
            font: 18px 'Roboto', sans-serif;
            color: #1a0dab;
        }
        .boxes h2 a i {
            font: 18px 'Roboto', sans-serif;
            color: #1a0dab;
        }
        .boxes h2 a span {
            display: block;
            color: #006621 !important;
            font: 14px "Roboto", sans-serif !important;
            padding: 0 !important;
        }
        .boxes p {
            color: #545454;
            font: 13px 'Roboto', sans-serif;
            margin: 2px 0;
        }
    </style>
@endpush

@push('scripts')
    <script src=" {{asset("/vendor/mediapress/js/jquery.bootstrap.wizard.min.js")}}"></script>
    <script src="{{asset("/vendor/mediapress/js/fm.selectator.jquery.js")}}"></script>
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>
    <script src="{{asset("/vendor/mediapress/js/icheck.min.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="{!! asset('vendor/mediapress/ContentModule/js/DetailsEngine.js') !!}"></script>
    <script>
        CKEDITOR.disableAutoInline = true;
    </script>

    <script>
        $('.select2').select2({
            tags: true,
            maximumSelectionLength: 10,
            tokenSeparators: [',', ' '],
            placeholder: "En çok kullanılan etiketlerden seç ya da yeni bir etiket gir",
            width: '100%'
        });
        $('.step-tabs ul.nav-pills > li:first > a').addClass('active show');

        var metaset = JSON.parse('{{ $metaset }}');
        function closeAllTabs(){
            $('.details-wrapper .tab-content i.fa-close').trigger('click')
        }

        $(document).ready(function () {
            $('.rootwizard').bootstrapWizard({
                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('.rootwizard').find('.progress-bar').css({width: $percent + '%'});
                }
            });
            if($('.tab-pane:eq(0) .details-wrapper>ul>li').length >5 && $('.details-wrapper .tab-content i.fa-close').length){
                $('.tab-pane:eq(0) .details-wrapper').prepend('<a href="javascript:void(0)" onclick="closeAllTabs()" style="float: right; margin-top: 5px"><i class="fa fa-close text-danger"></i></a>');
            }

            $('textarea.ckeditor').each(function (key, item) {
                CKEDITOR.replace($(this).attr('id'),
                    {
                        language: 'tr',
                        filebrowserImageBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=G�rsel',
                        filebrowserBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Dosya',
                        allowedContent: {
                            script: true,
                            div: true,
                            $1: {
                                // This will set the default set of elements
                                elements: CKEDITOR.dtd,
                                attributes: true,
                                styles: true,
                                classes: true
                            }
                        },
                        entities: false,
                        entities_latin: false,
                        //removeDialogTabs: null,//'image:advanced;link:advanced',
                        pasteFromWordPromptCleanup: true,
                        pasteFromWordRemoveFontStyles: true,
                        forcePasteAsPlainText: true,
                        ignoreEmptyParagraph: true,
                        toolbar: [
                            {
                                name: 'clipboard',
                                groups: ['clipboard', 'undo'],
                                items: ['PasteFromWord', '-', 'Undo', 'Redo']
                            },
                            {
                                name: 'editing',
                                groups: ['find', 'selection', 'spellchecker'],
                                items: ['Find', 'Replace']
                            },
                            //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                            {
                                name: 'paragraph',
                                groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                            },
                            {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                            {name: 'links', items: ['Link', 'Unlink']},
                            '/',
                            {
                                name: 'basicstyles',
                                groups: ['basicstyles', 'cleanup'],
                                items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                            },
                            {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                            {name: 'colors', items: ['TextColor', 'BGColor']},
                            {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                        ]
                    });
                CKEDITOR.config.extraPlugins = 'justify,iframe,indentblock,indent,textindent';
            });

            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });

            var details_engine = new DetailsEngine({});
            details_engine.init();

            $('.formstorer-ignore-tree').each(function(ind,el){
                formstorerIgnoreTree(el);
            });

            $("html").delegate(".tab-list ul li a i.fa-plus", "click",function () {
                $(this).parent().toggleClass('disabled');
                $(this).toggleClass('fa-plus fa-close text-danger text-primary');
                var href = $(this).closest("a").attr("href");
                var detail_wrapper = $(href);
                restoreDetail(detail_wrapper);
            });

            $("html").delegate(".tab-list ul li a i.fa-close", "click", function () {
                $(this).parent().toggleClass('disabled');
                $(this).toggleClass('fa-close fa-plus text-danger text-primary');
                var href = $(this).closest("a").attr("href");
                var detail_wrapper = $(href);
                deleteDetail(detail_wrapper);
            });

            $('.sortable').delegate('.nav-tabs li a', 'click', function () {
                var id = $(this).attr('date-id');
                $('.activess').removeClass('active');
                $('.activess').removeClass('activess');
                $('div[data-id=' + id + ']').addClass('activess');
                var href = $(this).attr('href');
                $(href).toggleClass('activess active');
            });
        });

        function createMetaVariablesModal(capsule) {
            let modal = $(".main-modal");
            $(".modal-body", modal).html("deneme");
            $(modal).modal("show");
        }

        function pageCategoriesSelectionChanged(page_id, category_ids_arr) {
            refreshPageCriterias(page_id, category_ids_arr);
        }

        function pageCountryandProvinceSelectionChanged(country_id) {
            getProvinces(country_id);
        }

        function getProvinces(country_id){
            var token = $("meta[name='csrf-token']").attr("content");
            var value = parseInt($('#select-province').children("option:selected").val());
            $.ajax({
                url: "{{ url(route("MPCore.country_province.get_values")) }}",
                method: 'GET',
                data: {country_id: country_id, _token: token},
                dataType: "json",
                success: function (data) {
                    $('#select-province').html("");
                    $.each(data, function(ind,el){
                        if(value===el.id) {
                            $('#select-province').append('<option  value="' + el.id + '" selected>' + el.name + '</option>');
                        }else{
                            $('#select-province').append('<option  value="' + el.id + '">' + el.name + '</option>');
                        }
                    });
                    $('#select-province').selectpicker('refresh');

                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function refreshPageCriterias(page_id, category_ids_arr){
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ url(route("Content.categories.related_criterias_full")) }}",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    var select_criterias = $('#kriter').data('criteria-ids');
                    var page_id = $('#kriter').data('page-id');
                    var ids =[];
                    var tablink, tabcontent, checkbox, checkbox_div, label;
                    $.each(data, function(ind,el){
                        ids.push(el.id);
                        if(!$('#kriter .tablinks[data-cri-id='+el.id+']').length){
                            tablink = $('<a href="javascript:void(0);" class="tablinks" data-cri-id="'+el.id+'" onclick="switchVerticalTab (event, \''+el.id+'\')">'+el.detail.name+'</a>');
                            tabcontent = $('<div id="'+el.id+'" class="tabcontent" data-cri-id="'+el.id+'"></div>');
                            checkbox_div = $('<div class="checkbox"></div>');
                            $.each(el.children, function(c_ind, child){
                                label = $('<label for="cri'+child.id+'"> '+child.detail.name+' </label>');
                                checkbox = $('<input type="checkbox" name="page->'+page_id+'->sync:criterias[]" id="cri'+child.id+'" value="'+child.id+'">');
                                if(select_criterias.indexOf(child.id)>=0){
                                    $(checkbox).prop("checked",true);
                                }
                                $(label).append(checkbox);
                                $(checkbox_div).append(label);
                            });
                            $(tabcontent).append(checkbox_div);
                            $('#kriter .tab').append(tablink);
                            $('#kriter .txt').append(tabcontent);
                        }
                    });
                    $('#kriter .tablinks').each(function(){
                        var criid = $(this).data('cri-id');
                        if(ids.indexOf(criid)<0){
                            $('#kriter .tablinks[data-cri-id='+criid+']').remove();
                            $('#kriter .tabcontent[data-cri-id='+criid+']').remove();
                        }
                    });

                    if(!$('#kriter .tablinks.active').length){
                        var criid = $('#kriter .tablinks:first-of-type').addClass('active').data('cri-id');
                        $('#kriter .tabcontent[data-cri-id='+criid+']').css('display','block');
                    }

                    $('.checkbox').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function leaveCategoriesCriteriasOnly(category_ids_arr) {

            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ url(route("Content.categories.related_criterias")) }}",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    var active_hidden = false;
                    $("#kriter .tablinks").each(function (index) {
                        var criid = $(this).data("cri-id");
                        //console.log(criid);
                        var tabcontent = $("#kriter .tabcontent[data-cri-id=" + criid + "]");
                        if (data.indexOf(criid) > -1) {
                            $(this).removeClass("hidden-cri").show();
                            tabcontent.removeClass("hidden-cri").show();
                            if ($(this).hasClass("active")) {
                                $("#kriter .tablinks").removeClass("active");
                                $(this).addClass("active");
                                $(this).click();
                            }
                        } else {
                            $(this).hide().addClass("hidden-cri");
                            tabcontent.hide().addClass("hidden-cri");
                        }
                    });
                    if ($("#kriter .tablinks").not(".hidden-cri").length) {
                        $("#kriter .tablinks").not(".hidden-cri").first().click();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function leaveCategoriesPropertiesOnly(category_ids_arr) {
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ url(route("Content.categories.related_properties")) }}",
                method: 'POST',
                data: {category_ids: category_ids_arr, _token: token},
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    var active_hidden = false;
                    $("#ozellikler .tablinks").each(function (index) {
                        var propid = $(this).data("prop-id");
                        //console.log(propid);
                        var tabcontent = $("#ozellikler .tabcontent[data-prop-id=" + propid + "]");
                        if (data.indexOf(propid) > -1) {
                            $(this).removeClass("hidden-prop").show();
                            tabcontent.removeClass("hidden-prop").show();
                            if ($(this).hasClass("active")) {
                                $("#ozellikler .tablinks").removeClass("active");
                                $(this).addClass("active");
                                $(this).click();
                            }
                        } else {
                            $(this).hide().addClass("hidden-prop");
                            tabcontent.hide().addClass("hidden-prop");
                        }
                    });
                    if ($("#ozellikler .tablinks").not(".hidden-cri").length) {
                        $("#ozellikler .tablinks").not(".hidden-cri").first().click();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //alert(false);
                }
            });
        }

        function tabForward(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#' + id + '"]').parent();
            active.next().find('a').trigger('click');
        }

        function tabBack(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#' + id + '"]').parent();
            active.prev().find('a').trigger('click');
        }

        function deleteDetail(detail_wrapper){

            var detail_type = $(detail_wrapper).attr("data-detail-type");
            var detail_id = $(detail_wrapper).attr("data-detail-id");

            $.ajax({
                'type': 'POST',
                'url': '{{ route("Content.deleteDetail") }}',
                'data': {
                    '_token': "{{ csrf_token() }}",
                    'detail_type' : detail_type,
                    'detail_id' : detail_id,
                },
                success: function (result) {
                    var selector = '.has-detail-in[data-detail-type="' + detail_type.replace(/\\/g, '\\\\') + '"][data-detail-id="' + detail_id + '"]';
                    //$(selector).addClass("remove-on-submit").addClass("formstorer-ignore-tree").hide();

                    $(selector).each(function(ind, el){
                        $(el).addClass("remove-on-submit").addClass("formstorer-ignore-tree").hide();
                        formstorerIgnoreTree(el);
                        var id = $(el).attr("id");
                        $(el).closest(".details-wrapper").find('[href="#'+id+'"]').addClass("disabled").closest(".details-wrapper").find("ul li:first a:not(.disabled)").click();
                    });

                    console.log(selector,$(selector).length);
                },
                error: function (result) {
                    //$('.has-detail-in[data-detail-type="' + detail_type + '"][data-detail-id="' + detail_id + '"]').removeClass("remove-on-submit");
                }
            });
        }

        function restoreDetail(detail_wrapper){

            var detail_type = $(detail_wrapper).attr("data-detail-type");
            var detail_id = $(detail_wrapper).attr("data-detail-id");

            $.ajax({
                'type': 'POST',
                'url': '{{ route("Content.restoreDetail") }}',
                'data': {
                    '_token': "{{ csrf_token() }}",
                    'detail_type' : detail_type,
                    'detail_id' : detail_id,
                },
                success: function (result) {
                    var selector = '.has-detail-in[data-detail-type="' + detail_type.replace(/\\/g, '\\\\') + '"][data-detail-id="' + detail_id + '"]';
                    $(selector).each(function(ind,el){
                        $(el).removeClass("remove-on-submit").removeClass("formstorer-ignore-tree").show();
                        formstorerUnignoreTree(el);
                        var id = $(el).attr("id");
                        $(el).closest(".details-wrapper").find('[href="#'+id+'"]').removeClass("disabled");
                    });
                },
                error: function (result) {
                    //$('.has-detail-in[data-detail-type="' + detail_type + '"][data-detail-id="' + detail_id + '"]').addClass("remove-on-submit");
                }
            });
        }

        function formstorerIgnoreTree(tree_root_el){
            var form = $(tree_root_el).closest("form");
            $("input, select, textarea", tree_root_el).each(function(input_ind, input_el){
                let input_name, selector;
                input_name = $(input_el).attr("name");
                if(input_name){
                    selector = 'input.formstorer_ignore_field[value="'+input_name+'"]';
                    if( ! $(selector, form).length){
                        let ignore_input = $('<input type="hidden" class="formstorer_ignore_field" name="formstorer_ignore_fields[]"/>');
                        $(ignore_input).val(input_name);
                        $(form).prepend(ignore_input);
                    }
                }
            });
        }

        function formstorerUnignoreTree(tree_root_el){
            var form = $(tree_root_el).closest("form");
            $("input, select, textarea", tree_root_el).each(function(input_ind, input_el){
                let input_name, selector;
                input_name = $(input_el).attr("name");
                if(input_name){
                    selector = 'input.formstorer_ignore_field[value="'+input_name+'"]';
                    $(selector, form).remove();
                }
            });

        }

        function disableDetailTabs(detail_type, detail_id){

        }

        function enableDetailTabs(detail_type, detail_id){

        }
    </script>

    <!-- Save Button Start -->
    <script>

        $(document).ready(function() {


            updateSaveButton($('input[name$="status"]:checked'));
            $('li.next > a[id^="anchor-"]').html('{{ trans('MPCorePanel::general.next') }} <i class="fa-angle-double-right fa"> </i>');
            $('li.previous > a[id^="anchor-"]').html('<i class="fa-angle-left fa"></i> {{ trans('MPCorePanel::general.previous') }}');

            $(document).delegate('input[name$="status"]', 'ifChecked', function () {
                updateSaveButton($(this));
            })

            @if($page->status == 5)
                $("div.datepicks > input").datepicker("setDate", "{{ Carbon\Carbon::parse($page->published_at)->format('d/m/Y') }}");
            @endif
        })

        function updateSaveButton(el) {
            var value = el.val();
            var button = el.closest('div.form').find("button.btn-step-tabs-submit");

            button.removeClass("btn-warning btn-success btn-danger btn-secondary");
            if(value == 2) { //Draft
                button.html("{{ __('ContentPanel::general.save_as.draft') }}");
                button.addClass('btn-warning')
            } else if(value == 1) { //Active
                button.html("{{ __('ContentPanel::general.save_as.active') }}");
                button.addClass('btn-success')
            } else if(value == 0) { //Passive
                button.html("{{ __('ContentPanel::general.save_as.passive') }}");
                button.addClass('btn-danger')
            } else if(value == 5) { //Postdate
                button.html("{{ __('ContentPanel::general.save_as.postdate') }}");
                button.addClass('btn-secondary')
            }
        }


    </script>
    <!-- Save Button Finish -->

    @if($page->sitemap->reservedUrl == 1)
        <!-- PreviewBox Start -->
        <script>
            $(document).ready(function() {
                @if(request()->get('isNew') == 'yes' && $page->status == 3 && $page->detail)
                    @php
                        $page_detail = $page->detail;
                    @endphp
                    $('div[data-language-id="{{$page_detail->language_id}}"][data-country-group-id="{{$page_detail->country_group_id}}"] .previewBox').
                        html('' +
                        '<div class="boxes prev{{ $page_detail->language_id . '_' . $page_detail->country_group_id }}">' +
                        '    <h2>\n' +
                        '        <p> {{ __("ContentPanel::page.preview_box_text") }} </p>' +
                        '    </h2>\n' +
                        '</div>' +
                        '');
                @else
                    @foreach($page->details as $detail)
                        @php
                            $metas = $detail->url->metas;
                            $metaWorker = $detail->url->metaWorker;
                        @endphp
                        $('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] .previewBox').html('' +
                            '<div class="boxes prev{{ $detail->language_id . '_' . $detail->country_group_id }}">' +
                            '    <h2>\n' +
                            '        <a href="javascript:void(0);">' +
                            '            <i class="previewName"> {{ $metas->title->desktop_value ?: $metaWorker->title }} </i>\n' +
                            '            <span class="previewUrl"> {{ url($detail->url->url) }} </span>\n' +
                            '        </a>\n' +
                            '    </h2>\n' +
                            '    <p class="previewContent"> {{ $metas->description->desktop_value ?: $metaWorker->description }} </p>\n' +
                            '</div>' +
                            '');


                        $(document).delegate('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="title"] div.divInput', 'keyup' , function() {

                            var cloneEl = $(this).clone();
                            cloneEl.find('em').remove();
                            var spans = cloneEl.find('span');
                            if(spans.length > 0) {
                                $.each(spans, function (k, v) {
                                    $(v).replaceWith($(v).attr('data-meta-value'));
                                })
                            }

                            $('.boxes.prev{{ $detail->language_id . '_' . $detail->country_group_id }}').find('.previewName').html(cloneEl.html());
                        });

                        $(document).delegate('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="description"] div.divInput', 'keyup' , function() {

                            var cloneEl = $(this).clone();
                            cloneEl.find('em').remove();
                            var spans = cloneEl.find('span');
                            if(spans.length > 0) {
                                $.each(spans, function (k, v) {
                                    $(v).replaceWith($(v).attr('data-meta-value'));
                                })
                            }

                            $('.boxes.prev{{ $detail->language_id . '_' . $detail->country_group_id }}').find('.previewContent').html(cloneEl.html());
                        });

                        $(document).delegate('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.detail-metas-control-v2-outer-wrapper input[id^="toggleswitch"]', 'change', function () {
                            if($(this).val() == 'manual') {
                                $('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="title"] div.divInput').html("{{ $metas->title->desktop_value ?: $metaWorker->title }}");
                                $('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="description"] div.divInput').html("{{ $metas->description->desktop_value ?: $metaWorker->description }}");
                            }
                        })

                    @endforeach
                @endif
            })
        </script>
        <!-- PreviewBox Finish -->
    @endif


    <!-- Copy Button Start -->
    <script>
        var copyPageName;
        var copyPageDetail;

        $("body").delegate(".tab-list ul li a i.fa-copy", "click", function () {
            var cg = $(this).attr('data-cg');
            var lg = $(this).attr('data-lg');
            var tabId = $(this).closest('a').attr('href').replace('#', '');
            var editorID = $('div.detail-capsule[data-language-id="'+lg+'"][data-country-group-id="'+cg+'"] textarea')
                .filter(function() {
                    return this.name.match(/^page_detail->.*->detail/);
                }).attr('id');

            copyPageName = $('div[id$="'+tabId+'"] input.detail-name').val();
            copyPageDetail = CKEDITOR.instances[editorID].getData();

            if(copyPageName) {
                $('#copyLanguageModal div.text').html('<strong>'+copyPageName+'</strong> metni kopyalanacak!')

                $('#copyLanguageModal').modal('show');
            }
        });

        function copyDetail(event) {
            $.each($('input[name^=target]:checked'), function (k,v) {
                var value = $(this).val().split(',');
                var cg = value[0];
                var lg = value[1];

                $('div.detail-capsule[data-language-id="'+lg+'"][data-country-group-id="'+cg+'"] input.detail-name').val(copyPageName);
                $('div.detail-capsule[data-language-id="'+lg+'"][data-country-group-id="'+cg+'"] input.detail-name').trigger('change');
                var editorID = $('div.detail-capsule[data-language-id="'+lg+'"][data-country-group-id="'+cg+'"] textarea')
                    .filter(function() {
                        return this.name.match(/^page_detail->.*->detail/);
                    }).attr('id');

                CKEDITOR.instances[editorID].setData(copyPageDetail);
            })

            $('#copyLanguageModal').modal('hide');
        }
    </script>
    <!-- Copy Button Finish -->
@endpush

@if(function_exists('filemanager'))
    @php
        if($renderable->getParam('page')) {
            $languages = array_map('intval', $renderable->getParam('page')->details->pluck('language_id')->toArray());
            $model_id = $renderable->getParam('page')->id;
            $model_type = urlencode(get_class($renderable->getParam('page')));
        }
    @endphp

    @push('scripts')
        {!! filemanager($model_id, $model_type, $languages) !!}
    @endpush
@endif
