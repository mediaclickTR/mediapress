<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 15.11.2018
     * Time: 09:59
     */

    namespace Mediapress\Modules\MPCore\DataSources;


    use Mediapress\Foundation\DataSource;
    use Mediapress\Modules\MPCore\Models\Country;

    class CountriesDS extends DataSource
    {

        public function getData(){
            $pluck=($this->params["pluck"] ?? false) && $this->params["pluck"];
            if($pluck){
                $key = $this->params["pluck_key"] ?? "id";
                $value = $this->params["pluck_column"] ?? "tr";
                return Country::get()->pluck($value,$key);
            }

            return Country::get();
        }
    }
