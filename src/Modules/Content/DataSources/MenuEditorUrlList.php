<?php
namespace Mediapress\Modules\Content\DataSources;

use Mediapress\Foundation\DataSource;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\MPCore\Models\Url;

class MenuEditorUrlList extends DataSource
{
    public function getData()
    {
        $sitemap_ids = isset($this->params["sitemap_ids"]) ? $this->params["sitemap_ids"] : null;
        $website_id = session("panel.website.id");
        if((is_countable($sitemap_ids) && !count($sitemap_ids)) || (!is_null($sitemap_ids) && !is_countable($sitemap_ids)) || (!$website_id)){
            return [];
        }

        $urls = Url::where("website_id",session("panel.website.id"))->get()->pluck('url', 'id');


        return $urls;

    }
}
