<?php

namespace Mediapress\Foundation\ThirdParty;



class Instagram
{
    private $url;

    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function instagram()
    {

        $a = json_decode(file_get_contents($this->url),true);


        $all = [
            'biography' => $a['graphql']['user']['biography'],
            'edge_followed_by' => $a['graphql']['user']['edge_followed_by'],
            'edge_follow' => $a['graphql']['user']['edge_follow'],
            'full_name' => $a['graphql']['user']['full_name'],
            'username' => $a['graphql']['user']['username'],
            'main_url' => "https://www.instagram.com/" . $a['graphql']['user']['username'],
            'posts' => collect([]),

        ];


        foreach($a['graphql']['user']['edge_owner_to_timeline_media']['edges'] as $edge){

            $list = [
                'display_url' => $edge['node']['display_url'],
                'url' => "https://www.instagram.com/p/" . $edge['node']['shortcode'],
                'edge_media_to_comment' => $edge['node']['edge_media_to_caption']['edges'],
                'edge_liked_by' => $edge['node']['edge_liked_by'],
                'edge_media_preview_like' => $edge['node']['edge_media_preview_like'],
                'thumbnail_src' => $edge['node']['thumbnail_src'],
                'is_video' => $edge['node']['is_video'],

            ];

            $all['posts']->push($list);
        }

        return $all;
    }
}
