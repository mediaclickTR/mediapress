<?php

namespace Mediapress\Foundation;

use Mediapress\Foundation\HtmlElement;

class HtmlFactory{

    public function createBSFormGroup($label_or_lbltext, $input){
        return true;
    }

    public function createImg($src){
        return (new HtmlElement('img',true))->add_attr('src',$src)->add_class('img-responsive');
    }

    public function createImgDiv($src){
        return (new HtmlElement('div',false))->add_content($this->createImg($src));
    }

    public function createBSTabs($content_elements=[], $tab_keys=[] ){
        return new HtmlElement('div',false);
    }

}