<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSitemapDetailsTableAddMetaTemplatesColumn extends Migration
{

    public const SITEMAP_DETAILS = 'sitemap_details';

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_DETAILS)) {
            $schema->table(self::SITEMAP_DETAILS, function (Blueprint $table) {
                $table->text('meta_templates')->nullable();
            });
        }
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_DETAILS)) {
            $schema->table(self::SITEMAP_DETAILS, function (Blueprint $table) {
                $table->dropColumn("meta_templates");
            });
        }
    }
}
