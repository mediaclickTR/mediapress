<?php

namespace Mediapress\Modules\Heraldist\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Heraldist\Exports\FormMessageExport;
use Mediapress\Modules\Heraldist\Models\Form;
use Illuminate\Http\Request;
use Mediapress\Modules\Heraldist\Models\Message;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;


class MessageController extends Controller
{

    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE = 'message';
    public const SUBJECT = 'subject';
    public const VARNAME = self::MESSAGE;
    public const HERALDIST_MESSAGE_SHOW = 'Heraldist.message.show';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.index","heraldist.messages.index", ])){return rejectResponse();}


        $subjects = Message::groupBy(self::SUBJECT)->get();
        $counts = [];
        $totals = [];
        foreach ($subjects as $subject) {
            $count = Message::where(self::SUBJECT, $subject->subject)->where('deleted',0)->where('read', '0')->count();
            $total = Message::where(self::SUBJECT, $subject->subject)->where('deleted',0)->count();
            $counts[$subject->subject] = $count;
            $totals[$subject->subject] = $total;
        }

        return view('HeraldistPanel::message.index', compact('subjects', 'counts', 'totals'));
    }
    public function show($form_id,Request $request)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.index","heraldist.messages.index", ])){return rejectResponse();}


        $messages = Message::where('form_id',$form_id)->orderBy("created_at", "DESC")->paginate(15);
        $form = Form::find($form_id);

        return view('HeraldistPanel::message.show', compact('messages', 'form'));
    }

    public function export($form_id)
    {
        $form = Form::find($form_id);

        return Excel::download(new FormMessageExport($form), \Str::slug($form->name) . '.xlsx');
    }


    public function detail($id){

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.read_message","heraldist.messages.read_message", ])){return rejectResponse();}

        $message = Message::find($id);
        $message->read=1;
        $message->save();
        return view('HeraldistPanel::message.detail', compact(self::VARNAME,'act'));
    }
    public function notRead($id){

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.mark_as_unread","heraldist.messages.mark_as_unread", ])){return rejectResponse();}

        $message = Message::find($id);
        $message->read=0;
        $message->save();
        return redirect(route(self::HERALDIST_MESSAGE_SHOW,[self::SUBJECT =>$message->subject]))->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    public function delete($id){

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.delete","heraldist.messages.delete", ])){return rejectResponse();}

        $message = Message::find($id);
        $message->deleted=1;
        $message->save();
        return redirect(route(self::HERALDIST_MESSAGE_SHOW,[self::SUBJECT =>$message->subject]))->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    public function inbox($id){

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.restore_deleted","heraldist.messages.restore_deleted", ])){return rejectResponse();}

        $message = Message::find($id);
        $message->deleted=0;
        $message->save();
        return redirect(route(self::HERALDIST_MESSAGE_SHOW,[self::SUBJECT =>$message->subject]))->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    public function changeReadYes(Request $request)
    {
        $message = Message::find($request->id);
        $message->read=1;
        $message->save();
    }
    public function changeReadNo(Request $request)
    {
        $message = Message::find($request->id);
        $message->read=0;
        $message->save();
    }
    public function markUnread($id)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.mark_as_unread","heraldist.messages.mark_as_unread", ])){return rejectResponse();}

        $message = Message::find($id);
        $message->read=0;
        $message->save();
        return redirect()->back()->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function selectedRemove(Request $request)
    {

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.delete","heraldist.messages.delete", ])){return rejectResponse();}

        if($request->checkbox) {
            foreach ($request->checkbox as $check){
                $message = Message::find($check);
                if($message) {
                    $form_id = $message->form_id;
                    $message->delete();
                }
            }
        }

        if(isset($form_id)) {
            return redirect(route(self::HERALDIST_MESSAGE_SHOW, ['form_id' => $form_id]))->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function downloadFile($path)
    {

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.messages.download_attachments","heraldist.messages.download_attachments", ])){return rejectResponse();}


        if (auth()->check()){
            $path = decrypt($path);

            if(strpos($path,"href")){
                preg_match("/href='(.*?)'/", $path, $matchesUrl);
                if($matchesUrl[1]){
                    $path = explode("/",parse_url($matchesUrl[1], PHP_URL_PATH));
                    $path = $path[2];
                }
            }

            $path = str_replace("/storage/","",$path);
            return response()->download(storage_path("app/public/".$path));
        }
        return redirect()->route("login");
    }

}
