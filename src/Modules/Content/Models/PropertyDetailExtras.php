<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyDetailExtras extends Model
{

    protected $table = 'property_detail_extras';

    public $timestamps = false;

    protected $fillable = ["property_detail_id", "key", "value"];

    public function propertyDetail()
    {
        return $this->belongsTo(PropertyDetail::class);
    }
}
