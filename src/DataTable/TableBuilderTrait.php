<?php namespace Mediapress\DataTable;

use Mediapress\Http\Controllers\Panel\CategoryController;
use Mediapress\Modules\Content\Models\Sitemap;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\View;

trait TableBuilderTrait
{
    /**
     * @param Builder $builder
     * @param Sitemap|null $sitemap
     * @return mixed
     */
    public function columns(Builder $builder, Sitemap $sitemap = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
           $prefix = 'Category';
        }
        if($sitemap){
            $consoleLog = 'console.log("DataTable Path: \\\\App\\\\DataTable\\\\' . $prefix.$sitemap->sitemapType->name .'")';
            View::share("dataTablePathConsole", $consoleLog);
        }
        if ($sitemap && method_exists('\App\DataTable\\' .$prefix. $sitemap->sitemapType->name, 'columns')) {
            return app('\App\DataTable\\' . $prefix.$sitemap->sitemapType->name)->columns($builder);
        }
        return app('Mediapress\DataTable\Tables\\' . class_basename(__CLASS__))->columns($builder);
    }

    /**
     * @param Builder $builder
     * @param Sitemap|null $sitemap
     * @return string
     */
    public function dataTableName(Builder $builder, Sitemap $sitemap = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
            $prefix = 'Category';
        }
        return '\App\DataTable\\' .$prefix. $sitemap->sitemapType->name;
    }


    /**
     * @param null $sitemap_id
     * @param null $parameter
     * @return mixed
     */
    public function ajax($website_id,$sitemap_id = null, $parameter = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
            $prefix = 'Category';
        }
        if ($sitemap_id)
        {
            $sitemap = Sitemap::with("sitemapType")->findOrFail($sitemap_id);
            if (method_exists('\App\DataTable\\' . $prefix.$sitemap->sitemapType->name, 'ajax')) {
                return app('\App\DataTable\\' .$prefix. $sitemap->sitemapType->name)->ajax($website_id,$sitemap, $parameter);
            }
            if ($sitemap){
                return app('Mediapress\DataTable\Tables\\' . class_basename(__CLASS__))->ajax($website_id,$sitemap, $parameter);
            }
        }
        return app('Mediapress\DataTable\Tables\\' . class_basename(__CLASS__))->ajax($website_id,$sitemap_id, $parameter);
    }
}
