<?php

namespace Mediapress\Facades;

use Illuminate\Support\Facades\Facade;

class Builder extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'Builder';
    }



}