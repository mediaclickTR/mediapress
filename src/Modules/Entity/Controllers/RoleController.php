<?php

namespace Mediapress\Modules\Entity\Controllers;


use Carbon\Carbon;
use Html;
use http\Env\Response;
use Mediapress\Foundation\MPCache;
use Mediapress\Models\Url;
use Mediapress\Models\Image;
use Yajra\Datatables\Facades\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Models\Menu;
use Mediapress\Models\MenuDetail;
use Mediapress\Models\LanguageWebsite;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Models\Role as BRole;
use File;
use Validator;
use Cache;
use Illuminate\Support\Facades\Storage;
use DB;

/**
 * Class RoleController
 * @package Mediapress\Http\Controllers\Panel
 * @author Eray DEMİREL <eray.demirel@mediaclick.com.tr>
 */

class RoleController extends Controller
{


    public const TITLE = 'title';
    public const DATA = 'data';
    public const NAME = 'name';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const ACCESSDENIED = 'accessdenied';
    public const FOOTER = 'footer';
    public const LANGUAGES = "languages";
    public const DEFAULT1 = "default";

    public function __construct()
    {
        parent::__construct();
        Bouncer::useRoleModel(BRole::class);
    }

    public function index(Builder $builder)
    {
        if(!userAction('role.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $dataTable = $this->columns($builder)->ajax(route('Auth.roles.ajax'));

        return $this->view("pages.roles.index", compact("sitemap", 'dataTable'));
    }


    /**
     * @param Builder $builder
     * @return \Yajra\Datatables\Html\Builder
     */
    public function columns(Builder $builder)
    {
        return $builder->columns([
                [
                        self::DATA => 'id', self::NAME => 'id', self::TITLE => 'ID', self::FOOTER => 'ID',
                ], [
                        self::DATA => self::TITLE, self::NAME => self::TITLE, self::TITLE => trans("panel::authorization.title"), self::FOOTER => trans("panel::authorization.title")
                ],[
                        self::DATA => self::NAME, self::NAME => self::NAME, self::TITLE => trans("panel::general.name"), self::FOOTER => trans("panel::general.name")
                ], [
                        self::DATA => self::CREATED_AT, self::NAME => self::CREATED_AT, self::TITLE => trans("panel::general.created_at"), self::FOOTER => trans("panel::general.created_at")
                ], [
                        self::DATA => self::UPDATED_AT, self::NAME => self::UPDATED_AT, self::TITLE => trans("panel::general.updated_at"), self::FOOTER => trans("panel::general.updated_at")
                ]
        ])->addAction([self::TITLE => trans("panel::general.transactions")]);
    }


    /**
     * @return mixed
     */
    public function ajax()
    {
        $roles = BRole::select('*')->where(self::NAME,'!=','SuperMCAdmin');
        Carbon::setLocale('tr');

        return Datatables::eloquent($roles)->addColumn('action', function ($role) {
            return Html::link(route("Auth.roles.edit", $role->id), '<i class="fa fa-pencil-square-o"></i>', ["style" => "margin-left:5px", "class" => "btn btn-primary", "tit"."le" => trans("panel::general.btnEditTitle")], false, false) .
                    Html::link(route("Auth.roles.abilities", $role->id), '<i class="fa fa-check-square"></i>', ["style" => "margin-left:5px", "class" => "btn btn-primary", "title" => trans("panel::authorization.commandEditPermissions")], false, false) .'
                <button name="delete" onclick="rowDelete(' . $role->id . ')" class="btn btn-danger" title="' . trans("panel::general.btnDeleteTitle") . '"><i class="fa fa-trash-o"></i></button>';
        })->setRowId('tbl-{{$id}}')->make(true);
    }



    /**
     * @param FormBuilder $formBuilder
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function formCreate(FormBuilder $formBuilder, $id = 0)
    {

        $languages = $this->getWebsiteSession()->languages->pluck("code", "id");

        if ($id) {
            if(!userAction('role.update',true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.edit-title", [self::NAME => 'Rol']);
            $button = trans("panel::general.edit");

            $data = [
                    "find" => [
                            "id", "=", $id
                    ], self::LANGUAGES => $languages
            ];
        } else {
            if(!userAction('role.create',true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.create-title", [self::NAME => 'Rol']);
            $button = trans("panel::general.save");

            $data = [self::LANGUAGES => $languages];
        }

        $form = PanelFormBuilder::create("role", self::DEFAULT1, $formBuilder, [], $data);

        return $this->view("pages.roles.form", compact("id", self::LANGUAGES, "form", self::TITLE, "button"));
    }


    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Mediapress\Exceptions\NotFoundException
     */
    public function abilities(Request $request, $id = 0){


        $role = BRole::find($id);


        if( ! $role) {notFound();}

        if($request->method()=="POST"){

            $data = $request->input()['actions'];
            $mode = $request->input()['mode'];

            if($mode=="everything"){

                Bouncer::unforbid($role->name)->everything();

                Bouncer::allow($role->name)->everything();

                data_set($data,'*',null);
            }elseif($mode=="nothing"){
                Bouncer::disallow($role->name)->everything();
                Bouncer::forbid($role->name)->everything();
                data_set($data,'*',null);
            }elseif($mode=="custom"){
                Bouncer::disallow($role->name)->everything();
                Bouncer::unforbid($role->name)->everything();
            }
            foreach($data as $actkey => $action){
                if ($action == "allow") {
                    $role->unforbid($actkey);
                    $role->allow($actkey);
                } elseif ($action == "deny") {
                    $role->disallow($actkey);
                    $role->forbid($actkey);
                } elseif ($action == "") {
                    $role->disallow($actkey);
                    $role->unforbid($actkey);
                }
            }
            Cache::forget(cache_key('mainMenu'));
        }

        $actions = getActions(null,true, true);

        $abilities = $role->getAbilities();

        $title="Yetkiler";

        return $this->view("pages.roles.abilities", compact("id","role","abilities","actions", self::TITLE, "button"));

    }


    /**
     * @param FormBuilder $formBuilder
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function save(FormBuilder $formBuilder, Collection $data)
    {
        if(!userAction('role.update',true,false)){
            return [
                false, trans("panel::general.ajax.access_error")
            ];
        }

        $id = $data->pull("parameters.id");

        $role = BRole::find($id);

        if ($role) {
            $role = \Arr::first(FormStore::store($formBuilder, "role", self::DEFAULT1, [], [
                    "find" => ["id", "=", $id], self::DATA => $data->except("parameters")->toArray()
            ]));
        } else {
            $role = \Arr::first(FormStore::store($formBuilder, "role", self::DEFAULT1, [], [
                    self::DATA => $data->except("parameters")->toArray()
            ]));
        }

        if ($role) {
            return [
                    true, trans("panel::general.ajax.create_success", [self::NAME => $role->title])
            ];
        } else{
            return [
                    false, trans("panel::general.ajax.create_error", [self::NAME => $role->title])
            ];
        }
    }


    /**
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Collection $data)
    {
        if(!userAction('role.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $id = $data->pull("parameters.id");
        if (!$id){
            return [false, trans("panel::roles.crud.no_record")];
        }

        $role = BRole::find($id);

        if (!$role){
            return [false, trans("panel::roles.crud.no_record")];
        }
        if ($role->delete()){
            return [true, trans("panel::roles.crud.delete_successful", [self::NAME => $role->title])];
        }

        return [false, trans("panel::roles.crud.delete_failed", [self::NAME => $role->title])];
    }
}
