<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mediapress\Http\Controllers\PanelController as Controller;

use Mediapress\Modules\Content\Facades\CategoryEngine;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryCriteria;
use Mediapress\Modules\Content\Models\CategoryProperty;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\SceneDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\MPCore\Facades\MPCore;

class SceneController extends Controller
{

    public const REQUIRED = 'required';
    public const ACCESSDENIED = 'accessdenied';
    public const ORDER = 'order';
    public const SLIDER = 'slider';
    public const CONTENT_SLIDER_SCENES_INDEX = 'Content.slider.scenes.index';


    public function index($slider_id)
    {
        if (!userAction('message.scene.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $slider = Slider::find($slider_id);
        $scenes = $slider->scenes()->with('detail', 'admin')->orderBy(self::ORDER)->paginate(12);
        $viewName = $slider->type == 'video' ? "ContentPanel::scenes.index_video" : "ContentPanel::scenes.index";

        return view($viewName, compact(self::SLIDER, 'scenes'));
    }


    public function create($slider_id)
    {
        if (!userAction('message.scene.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $slider = Slider::find($slider_id);
        $website = $slider->website;
        $countryGroups = $website->countryGroups;

        $viewName = $slider->type == 'video' ? "ContentPanel::scenes.create_video" : "ContentPanel::scenes.create";

        return view($viewName, compact(self::SLIDER, 'countryGroups'));
    }

    public function store($slider_id, Request $request)
    {
        if (!userAction('message.scene.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = [];
        $data['admin_id'] = Auth::guard('admin')->user()->id;
        $data['time'] = 10;

        $data['slider_id'] = $slider_id;
        $scene = new Scene($data);
        $scene->time = $request->time;
        $scene->order = $request->order;
        $scene->status = $request->status;
        $scene->save();


        foreach ($request->except('_token', 'time', 'order', 'status') as $countryGroup => $languages) {
            foreach ($languages as $language => $set) {
                if (is_numeric($language)) {
                    $set['country_group_id'] = $countryGroup;
                    $set['language_id'] = $language;
                    $set['scene_id'] = $scene->id;
                    SceneDetail::create($set);
                }
            }
        }

        $this->orderScenes($slider_id);
        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_SCENES_INDEX, $slider_id));
    }

    public function edit($slider_id, $id)
    {
        if (!userAction('message.scene.edit', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $slider = Slider::find($slider_id);
        $scene = Scene::find($id);
        $website = $slider->website;
        $countryGroups = $website->countryGroups;
        $viewName = $slider->type == 'video' ? "ContentPanel::scenes.edit_video" : "ContentPanel::scenes.edit";

        return view($viewName, compact(self::SLIDER, 'scene', 'countryGroups'));
    }

    public function update($slider_id, $id, Request $request)
    {
        if (!userAction('message.scene.edit', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        //$this->validate($request, $this->rules, [], $this->labels);

        $scene = Scene::find($id);
        $scene->time = $request->time;
        $scene->order = $request->order;
        $scene->status = $request->status;
        $scene->save();

        foreach ($request->except('_token', 'time', 'order', 'status') as $countryGroup => $languages) {
            foreach ($languages as $language => $set) {
                if (is_numeric($language)) {
                    $detail = $scene->details()->where('country_group_id', $countryGroup)->where('language_id', $language)->first();
                    if ($detail) {
                        $detail->update($set);
                    } else {
                        $scene->details()->updateOrCreate(
                            [
                                'country_group_id' => $countryGroup,
                                'language_id' => $language,
                            ],
                            $set
                        );
                    }
                }
            }
        }

        $scene->save();

        $this->orderScenes($slider_id);
        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_SCENES_INDEX, $slider_id));
    }

    public function delete($slider_id, $id)
    {
        if (!userAction('message.scene.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $scene = Scene::find($id);

        $scene->delete();
        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_SCENES_INDEX, $slider_id));
    }

    private function orderScenes($slider_id)
    {
        $scenes = Scene::where('slider_id', $slider_id)->orderBy(self::ORDER)->orderByDesc('updated_at')->get();
        $i = 1;
        foreach ($scenes as $scene) {
            $scene->order = $i++;
            $scene->save();
        }


    }
}
