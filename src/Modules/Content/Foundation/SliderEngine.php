<?php

namespace Mediapress\Modules\Content\Foundation;


use Mediapress\Modules\Content\Models\Slider;

class SliderEngine
{
    public $slides = array();

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
        $this->mediapress = mediapress();
        $this->languageID = $this->mediapress->activeLanguage->id;
        $this->countryGroupId = $this->mediapress->activeCountryGroup->id;
    }


    public function render(){

        $slides = $this->slides();
        $slider = $this->slider;

        return view('ContentWeb::sliders.show',compact('slides','slider'));
    }

    private function slides()
    {
        $scenes = $this->slider->scenes()->orderBy('order')->where('status',1)->get();
        $slides = [];
        foreach ($scenes as $scene){
            $detail = $scene->details()->where('language_id', $this->languageID)->where('country_group_id', $this->countryGroupId)->first();

            $slides[] = new Slide([
                'time'=> $scene->time,
                'texts'=> $detail->texts,
                'buttons'=> $detail->buttons,
                'files'=> $detail->files,
                'url'=> $detail->url,
                'video'=> $detail->video_url,
            ]);
        }

        return $slides;
    }

    public function build()
    {
        $this->slides = $this->slides();
        return $this;
    }
}
