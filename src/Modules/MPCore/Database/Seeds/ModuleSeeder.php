<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->delete();

        $i = 0;
        $modules = [];
        foreach (File::directories(base_path('vendor/mediapress/mediapress/src/Modules')) as $dic) {
            $dic = basename($dic);
            $modules[] = ["name" => $dic, "namespace" => "Mediapress\\Modules\\" . $dic, "status" => 1, "slot_id" => null];
        }

        DB::table('modules')->insert($modules);
    }
}
