<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

const ENTITY_ID = 'entity_id';
const ENTITY_TYPE = 'entity_type';
const SCOPE = 'scope';
const CASCADE = 'cascade';
class CreateBouncerTable extends Migration
{

    public const ABILITIES = 'abilities';
    public const ROLES = 'roles';

    public function up()
    {

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        $schema->create(self::ABILITIES, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('title')->nullable();
            $table->integer(ENTITY_ID)->unsigned()->nullable();
            $table->string(ENTITY_TYPE, 150)->nullable();
            $table->boolean('only_owned')->default(false);
            $table->integer(SCOPE)->nullable()->index();
            $table->timestamps();
        });

        $schema->create(self::ROLES, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('title')->nullable();
            $table->integer('level')->unsigned()->nullable();
            $table->integer(SCOPE)->nullable()->index();
            $table->integer("admin_id")->unsigned()->nullable();
            $table->integer("status")->default(3);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(
                ['name', SCOPE],
                'roles_name_unique'
            );
        });

        $schema->create('assigned_roles', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->index();
            $table->integer(ENTITY_ID)->unsigned();
            $table->string(ENTITY_TYPE, 150);
            $table->integer(SCOPE)->nullable()->index();

            $table->index(
                [ENTITY_ID, ENTITY_TYPE, SCOPE],
                'assigned_roles_entity_index'
            );

            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onUpdate(CASCADE)->onDelete(CASCADE);
        });

        $schema->create('permissions', function (Blueprint $table) {
            $table->integer('ability_id')->unsigned()->index();
            $table->integer(ENTITY_ID)->unsigned();
            $table->string(ENTITY_TYPE, 150);
            $table->boolean('forbidden')->default(false);
            $table->integer(SCOPE)->nullable()->index();

            $table->index(
                [ENTITY_ID, ENTITY_TYPE, SCOPE],
                'permissions_entity_index'
            );

            $table->foreign('ability_id')
                ->references('id')->on('abilities')
                ->onUpdate(CASCADE)->onDelete(CASCADE);
        });
    }

    public function down()
    {
        Schema::drop('permissions');
        Schema::drop('assigned_roles');
        Schema::drop(self::ROLES);
        Schema::drop(self::ABILITIES);
    }
}
