<?php


namespace Mediapress\Modules\Content\Foundation;

use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\MenuDetail;
use Mediapress\Modules\Content\Models\Page;
use Carbon\Carbon;
use Mediapress\Modules\Content\Models\Sitemap;

class BreadCrumb
{
    public $breadcrumb = array();

    private $current_model;
    private $lang_model;
    private $url_model;
    private $menu_slug;
    private $add_homepage = true;
    private $add_domain = true;
    private $string_type = "all_word_first_char";
    private $category_depth = -1;
    private $injections = array();

    public function __construct($mediapress)
    {
        $this->url_model = $mediapress->url;
        $this->lang_model = $mediapress->activeLanguage();
        $this->current_model = $mediapress->parent;
    }

    public function setMenu(string $menu_slug): self
    {
        $this->menu_slug = $menu_slug;

        return $this;
    }

    public function setCategoryDepth(int $category_depth): self
    {
        $this->category_depth = $category_depth;

        return $this;
    }

    public function setStringType(string $string_type): self
    {
        $this->string_type = $string_type;

        return $this;
    }

    public function setInjections(array $injections): self
    {
        $this->injections = $injections;

        return $this;
    }

    public function addDomain(bool $add_domain): self
    {
        $this->add_domain = $add_domain;

        return $this;
    }

    public function addHomePage(bool $add_homepage): self
    {
        $this->add_homepage = $add_homepage;

        return $this;
    }

    public function get(): array
    {

        if($this->current_model) {
            $this->setCurrentModel();

            if ($this->menu_slug) {
                $this->setFromMenu();
            } else {
                $this->setFromSitemap();
            }
        }

        if($this->add_homepage) {
            $this->setHomePage();
        }

        $this->breadcrumb = array_reverse($this->breadcrumb);

        if(count($this->injections) > 0) {
            $this->injection();
        }

        return $this->breadcrumb;
    }

    public function injection(): void {

        krsort($this->injections, 1);

        foreach ($this->injections as $key => $inject) {

            if(!isset($this->breadcrumb[$key])) {
                $this->breadcrumb[$key] = $inject;
            } else {
                $temp = array_slice($this->breadcrumb, 0, $key);
                $this->breadcrumb = array_merge($temp, [$inject], array_slice($this->breadcrumb, $key));
            }
        }

    }

    public function setFromMenu(): void
    {

        $currentMenu = MenuDetail::where('url_id', $this->url_model->id)
            ->where('language_id', $this->lang_model->id)
            ->where('status', 1)
            ->whereHas('menu', function ($q) {
                $q->where('slug', $this->menu_slug);
            })
            ->first();

        if ($currentMenu) {

            $menus = MenuDetail::where("lft", "<", $currentMenu->lft)
                ->where('language_id', $this->current_model->sitemap->detail->language_id)
                ->where('menu_id', $currentMenu->menu_id)
                ->where("rgt", ">", $currentMenu->rgt)
                ->orderByDesc('lft')
                ->get();

            foreach ($menus as $menu) {
                $this->addBreadcrumb($menu,$menu->url ?: null);
            }

        } else {
            $currentMenu = MenuDetail::where('url_id', $this->current_model->sitemap->detail->url->id)
                ->where('language_id', $this->current_model->sitemap->detail->language_id)
                ->where('status', 1)
                ->whereHas('menu', function ($q) {
                    $q->where('slug', $this->menu_slug);
                })
                ->first();

            if ($currentMenu) {
                $menus = MenuDetail::where("lft", "<=", $currentMenu->lft)
                    ->where('language_id', $this->current_model->sitemap->detail->language_id)
                    ->where('menu_id', $currentMenu->menu_id)
                    ->where("rgt", ">=", $currentMenu->rgt)
                    ->orderByDesc('lft')
                    ->get();

                foreach ($menus as $menu) {
                    $this->addBreadcrumb($menu,$menu->url ?: null);
                }
            }
        }
    }

    public function setFromSitemap(): void
    {

        $categories = Category::where('sitemap_id', $this->current_model->sitemap_id)
            ->where('status', 1)
            ->orderByDesc('lft');

        if (get_class($this->current_model) == Page::class) {
            $category = $this->current_model->category();

            if($category && $category->lft) {
                $categories = $categories->where("lft", "<=", $category->lft)
                    ->where("rgt", ">=", $category->rgt)
                    ->where('depth', "<=", $this->category_depth);
            }

        } elseif (get_class($this->current_model) == Category::class) {
            $category = $this->current_model;

            if($category && $category->lft) {
                $categories = $categories->where("lft", "<", $category->lft)
                    ->where("rgt", ">", $category->rgt)
                    ->where('depth', "<", $this->category_depth);
            }
        }

        $categories = $categories->get();

        foreach ($categories as $category) {
            $this->addBreadcrumb($category->detail, $category->detail->url);
        }

        $this->setSitemap();
    }

    private function addBreadcrumb($detail, $url): void {

        $name = strip_tags($detail->name);

        switch ($this->string_type) {
            case "all_word_first_char":
                $temp_name = str_ucwords($name, $this->lang_model->id);
                break;
            case "first_word_first_char":
                $temp_name = str_ucfirst($name, $this->lang_model->id);
                break;
            case "all_word_all_char":
                $temp_name = strto('upper', $name, $this->lang_model->id);
                break;
            case "all_word_no_char":
                $temp_name = strto('lower', $name, $this->lang_model->id);
                break;
            default:
                $temp_name = $name;
        }

        $temp_name = supFront($temp_name,'name',$detail);

        array_push($this->breadcrumb, [
            'name' => $temp_name,
            'url' => $this->link($url),
        ]);
    }

    private function setCurrentModel(): void
    {
        $this->addBreadcrumb($this->current_model->detail, null);
    }

    private function setSitemap(): void
    {
        $sitemap = $this->current_model->sitemap;

        if($sitemap && get_class($this->current_model) != Sitemap::class) {
            $this->addBreadcrumb($sitemap->detail, $sitemap->detailPage == 1 ? $sitemap->detail->url : null);
        }
    }

    private function setHomePage(): void
    {
        $home_page = Sitemap::where('feature_tag', 'homepage')
            ->whereHas('detail')
            ->first();

        if($home_page) {
            $this->addBreadcrumb($home_page->detail, $home_page->detail->url);
        }
    }

    private function link($url_model = null)
    {
        $return = "javascript:void(0);";

        if ($url_model) {
            if ($this->add_domain) {
                $return = url($url_model->url);
            } else {
                $return = $url_model->url;
            }
        }

        return $return;
    }
}
