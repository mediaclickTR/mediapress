@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! trans("HeraldistPanel::messages.forms") !!}</h1>
        <div class="pro-lists">
            <table>
                <tr>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("HeraldistPanel::messages.new_total") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                @foreach($subjects as $key => $subject)
                    <tr>
                        <td style="text-align:center">{{ $subject->subject }}</td>
                        <td>
                            @if(!$counts[$subject->subject])
                                {{ $counts[$subject->subject].'/'.$totals[$subject->subject] }}
                            @else
                                <span class="aktif"> {{ $counts[$subject->subject].'/'.$totals[$subject->subject] }}</span>
                            @endif
                        </td>
                        <td style="text-align:center">
                            <a href="{{ route('Heraldist.message.show',['subject'=>$subject->subject] ) }}">{!! trans("HeraldistPanel::messages.gomessages") !!}</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
