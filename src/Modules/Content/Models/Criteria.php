<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\Extra;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Traits\HasMFiles;
use Mediapress\Modules\Content\Traits\HasDetails;

class Criteria extends BaseModel
{

    use SoftDeletes;
    use HasMFiles;
    use HasDetails;

    protected $table = 'criterias';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $fillable = ["sitemap_id", "criteria_id","admin_id", "type", "status","lft", "rgt", "depth", 'ctex_1', 'ctex_2', 'cvar_1', 'cvar_2', 'cvar_3', 'cvar_4', 'cvar_5', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cdat_1', 'cdat_2', 'cdec_1', 'cdec_2', 'cdec_3', 'cdec_4'];


    public function children()
    {
        return $this->hasMany(Criteria::class);
    }

    public function parent()
    {
        return $this->belongsTo(Criteria::class, 'criteria_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(CriteriaDetail::class);
    }

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }

    public function extras()
    {
        return $this->hasMany(CriteriaExtra::class);
    }
}
