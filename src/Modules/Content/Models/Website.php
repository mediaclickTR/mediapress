<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Traits\HasDetails;

class Website extends BaseModel
{
    use SoftDeletes;
    use HasDetails;

    public const REGIONS = "regions";
    public const DEFAULT1 = "default";
    public const LANGUAGES = "languages";
    public $timestamps = true;
    public $fillable = ["admin_id", "type", "target", "sort", "name", "slug", "ssl", "website_id", "use_deflng_code", "regions", "website_id", self::REGIONS, "status", "variation_template"];
    protected $table = "websites";
    protected $dates = ["deleted_at"];

    public function defaultWebsiteLanguage()
    {
        return $this->belongsToMany(Language::class, 'language_website')->wherePivot(self::DEFAULT1, 1);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, "admin_id", "id");
    }

    public function globalCountryGroup()
    {
        return $this->countryGroups()->where("code", "gl")->first();
    }

    public function languages()
    {
        return $this->countryGroups()->where("code", "gl")->first()->languages();
    }

    public function countries()
    {
        return $this->belongsToMany(Language::class)->withPivot(self::DEFAULT1);
    }

    public function sitemaps()
    {
        return $this->belongsToMany(Sitemap::class);
    }

    public function urls()
    {
        return $this->hasMany(Url::class);
    }

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function sitemapXmls()
    {
        return $this->hasMany(SitemapXml::class);
    }

    public function variations($flat = false)
    {
        if (!$flat) {
            return VariationTemplates::variations($this)[$this->id];
        } else {
            $vts = collect(VariationTemplates::variations($this)[$this->id]["country_groups"]);

            $flatset = [];
            foreach ($vts as $vt) {
                foreach ($vt[self::LANGUAGES] as $vtl) {
                    $flatset[] = [
                        "country_group_id" => $vt["id"],
                        "country_group_code" => $vt["code"],
                        "country_group_title" => $vt["title"],
                        "language_id" => $vtl["id"],
                        "language_code" => $vtl["code"],
                        "language_name" => $vtl["name"],
                        "language_flag" => $vtl["flag"],
                    ];
                }
            }
            return $flatset;
        }
    }

    /*public function variations()
    {
        return $this->hasMany(LanguageWebsite::class, "website_id", "id")->where("country_group_id", "!=", null)->groupBy("country_group_id");
    }

    public function variationLanguages()
    {
        return $this->hasMany(LanguageWebsite::class, "website_id", "id")->where("country_group_id", "!=", null)->groupBy("language_id");
    }*/

    public function defaultLanguage()
    {
        return $this->countryGroups()->where("code", "gl")->first()->languages()->where("country_group_language.default", 1)->first();
    }


    public function languageWebsite()
    {
        return $this->hasMany(LanguageWebsite::class);
    }

    public function countryGroup()
    {
        return $this->morphOne(CountryGroup::class, 'owner');
    }

    public function countryGroups()
    {
        return $this->morphMany(CountryGroup::class, 'owner');
    }

    public function details()
    {
        return $this->hasMany(WebsiteDetail::class);
    }


    public function extras()
    {
        return $this->hasMany(WebsiteExtra::class);
    }

    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value)) {
            return $extra->value;
        }

        return null;
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function predictLanguage($cg = null)
    {


        //TODO ülkeye göre yönlendirme
        //Check Robots
        if (is_robot()) {
            return $this->defaultLanguage();
        }

        //Check region redirect
        $region = $this->attributes[self::REGIONS];

        if ($region == 0 || !$cg) {
            $request = Request::capture();
            if ($request->segment(1)) {
                $language = Language::where('code', $request->segment(1))->first();
                if ($language) {
                    return $language;
                }
            }
            return $this->defaultLanguage();
        } else {

            $browserLanguages = browser_languages();
            $languages = $cg->languages->pluck('code', 'id')->toArray();

            foreach ($browserLanguages as $browserLanguage) {
                foreach ($languages as $id => $code) {
                    if ($code == $browserLanguage) {
                        return Language::find($id);
                    }

                }
            }
            $default = $cg->languages()->wherePivot('default', 1)->first();
            if ($default) {
                return $default;
            }
            return $cg->languages->first();
        }

    }

    public function predictCountryGroup()
    {

        $region = $this->attributes[self::REGIONS];
        if ($region == 0 || is_robot()) {
            return $this->globalCountryGroup();
        } else {
            $ipDetails = ip_details();
            if ($ipDetails) {
                if(isset($ipDetails["country"])){
                    $ipDetails = $ipDetails["country"]["iso_code"];
                }else{
                    return $this->globalCountryGroup();
                }
            } else {
                return $this->globalCountryGroup();
            }

            $countryGroup = CountryGroup::whereHas("countries", function ($q) use ($ipDetails) {
                $q->where("code", $ipDetails);
            })->first();


            if (is_null($countryGroup)) {
                return $this->globalCountryGroup();
            }
            return $countryGroup;
        }
    }

    public function isCountryGroupDifferent($relation)
    {

        if(is_robot()){
            return false;
        }
        $cg = $this->predictCountryGroup();
        $lg = $this->predictLanguage($cg);

        if($cg->id != $relation->country_group_id){
            return ['cg'=>$cg,'lg'=>$lg];

        }
        return false;
    }
}

/*
// Does GL cg has that lng?
$gl_has_lng = CountryGroup::where("code", "gl")->whereHas(self::LANGUAGES, function ($q) use ($language) {
    $q->where("id", $language->id);
})->first();
if(!is_null($gl_has_lng)){
    return $gl_has_lng;
}

// Does any other cg has that lng?
$who_has_lng = CountryGroup::whereHas(self::LANGUAGES, function ($q) use ($language) {
    $q->where("id", $language->id);
})->first();

if(!is_null($who_has_lng)){
    return $who_has_lng;
}*/
