<?php

namespace Mediapress\Modules\Content\Foundation;

use Mediapress\Modules\Content\Models\Sitemap;

/**
 * Class DefaultSitemapKit
 * @package Mediapress\Modules\Content\Foundation
 */
class DefaultSitemapKit
{

    /**
     * @var Sitemap
     */
    private $sitemap;
    /**
     * @var array
     */
    private $replaceKeys;
    /**
     * @var array
     */
    private $replaceValues;

    /**
     * @var string
     */
    private $sitemapType;
    /**
     * @var int
     */
    private $sitemapdetailPage;
    /**
     * @var int
     */
    private $sitemapCategory;
    /**
     * @var int
     */
    private $sitemapCriteria;

    /**
     * @var string
     */
    private $sitemapDetailFunc = "";
    /**
     * @var string
     */
    private $categoryDetailFunc = "";
    /**
     * @var string
     */
    private $pageDetailFunc = "";
    /**
     * @var string
     */
    private $ajaxCriteriaFilterFunc = "";

    /**
     * DefaultsKit constructor.
     */
    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;

        $this->sitemapType = $sitemap->sitemapType->sitemap_type_type;
        $this->sitemapdetailPage = (int) $sitemap->detailPage;
        $this->sitemapCategory = (int) $sitemap->category;
        $this->sitemapCriteria = (int) $sitemap->criteria;
    }

    /**
     * @param array $replaceKeys
     * @param array $replaceValues
     * @return void
     */
    public function init(array &$replaceKeys, array &$replaceValues): void
    {
        $this->replaceKeys = $replaceKeys;
        $this->replaceValues = $replaceValues;

        $this->setKeyAndValues();

        $replaceKeys = $this->replaceKeys;
        $replaceValues = $this->replaceValues;

        $this->createBlades();
    }

    /**
     * @return void
     */
    private function setKeyAndValues(): void
    {
        $this->replaceKeys[] = '{%SitemapDetailFunction%}';
        $this->replaceKeys[] = '{%CategoryDetailFunction%}';
        $this->replaceKeys[] = '{%PageDetailFunction%}';
        $this->replaceKeys[] = '{%ajaxCriteriaFilter%}';

        $this->setSitemapDetailFunc();
        $this->setCategoryDetailFunc();
        $this->setPageDetailFunc();
        $this->setAjaxFilterFunc();

        $this->replaceValues[] = $this->sitemapDetailFunc;
        $this->replaceValues[] = $this->categoryDetailFunc;
        $this->replaceValues[] = $this->pageDetailFunc;
        $this->replaceValues[] = $this->ajaxCriteriaFilterFunc;

    }

    //region functionContent

    /**
     * @return void
     */
    private function setSitemapDetailFunc(): void
    {
        if ($this->sitemapdetailPage) {
            $this->sitemapDetailFunc = 'public function SitemapDetail(Mediapress $mediapress) {' . "\n";
            $this->sitemapDetailFunc .= "\n\t\t" . 'return $this->sitemapDetailFunc([';

            if ($this->sitemapType == 'dynamic') {
                $this->sitemapDetailFunc .= '
            "pages" => [
                "select" => ["id", "status", "detail.id", "detail.page_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                "image" => ["cover"],
                "paginate" => 12,
                "order" => [
                    "by" => "order",
                    "direction" => "asc"
                ]
            ],';

                if ($this->sitemapCategory == 1) {
                    $this->sitemapDetailFunc .= '
            "categories" => [
                "select" => ["id", "category_id", "status", "detail.id", "detail.category_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                "image" => ["cover"],
            ],';
                }
            }

            $this->sitemapDetailFunc .= "\n\t\t" . ']);';
            $this->sitemapDetailFunc .= "\n\t" . '}';
        }
    }

    /**
     * @return void
     */
    private function setCategoryDetailFunc(): void
    {
        if ($this->sitemapType == 'dynamic' && $this->sitemapCategory == 1) {

            $this->categoryDetailFunc = 'public function CategoryDetail(Mediapress $mediapress) {' . "\n";
            $this->categoryDetailFunc .= "\n\t\t" . 'return $this->categoryDetailFunc([';
            $this->categoryDetailFunc .= '
                "children" => [
                    "select" => ["id", "category_id", "status", "detail.id", "detail.category_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                    "image" => ["cover"],
                ],
                "category" => [
                    "select" => ["id", "category_id", "status", "detail.id", "detail.category_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                    "image" => ["cover"],
                ],
                "pages" => [
                    "select" => ["id", "status", "detail.id", "detail.page_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                    "image" => ["cover"],
                    "paginate" => 12,
                    "order" => [
                        "by" => "order",
                        "direction" => "asc"
                    ]
                ],';

            if ($this->sitemapCriteria == 1) {

                $this->categoryDetailFunc .= '
                "criterias" => [
                    "select" => ["*"],
                ],';
            }

            $this->categoryDetailFunc .= "\n\t\t" . ']);';
            $this->categoryDetailFunc .= "\n\t" . '}';
        }
    }

    /**
     * @return void
     */
    private function setPageDetailFunc(): void
    {
        if ($this->sitemapType == 'dynamic') {
            $this->pageDetailFunc = 'public function PageDetail(Mediapress $mediapress) {' . "\n";
            $this->pageDetailFunc .= "\n\t\t" . 'return $this->pageDetailFunc([';

            $this->pageDetailFunc .= '
            "page" => [
                "select" => ["*"],
                "image" => ["cover"],
            ],';

            if ($this->sitemapCategory == 1) {

                $this->pageDetailFunc .= '
            "category" => [
                "select" => ["id", "category_id", "status", "detail.id", "detail.category_id", "detail.name", "detail.detail", "url.url", "url.model_id", "url.model_type"],
                "image" => ["cover"],
            ],';

                if ($this->sitemapCriteria == 1) {
                    $this->pageDetailFunc .= '
            "criterias" => [
                "select" => ["*"],
            ],';
                }
            }


            $this->pageDetailFunc .= "\n\t\t" . ']);';
            $this->pageDetailFunc .= "\n\t" . '}';
        }
    }

    /**
     * @return void
     */
    private function setAjaxFilterFunc(): void
    {
        if ($this->sitemapCategory == 1 && $this->sitemapCriteria == 1) {
            $this->ajaxCriteriaFilterFunc = 'public function ajaxCriteriaFilter () {' . "\n";
            $this->ajaxCriteriaFilterFunc .= "\n\t\t" . 'return $this->filterCriteriaFunc([';
            $this->ajaxCriteriaFilterFunc .= '
            "paginate" => 12,';
            $this->ajaxCriteriaFilterFunc .= "\n\t\t" . ']);';
            $this->ajaxCriteriaFilterFunc .= "\n\t" . '}';
        }
    }
    //endregion

    //region createBlades
    /**
     * @return void
     */
    private function createBlades(): void
    {
        if($this->sitemapdetailPage == 1) {
            $this->createSitemapBlade();
        }

        if($this->sitemapType == 'dynamic') {
            $this->createPageBlade();

            if($this->sitemapCategory == 1) {
                $this->createCategoryBlade();
            }
        }

    }

    /**
     * @return void
     */
    private function createSitemapBlade(): void
    {
        if($this->sitemap->feature_tag == 'search') {
            return;
        }

        if ($this->sitemapType == 'dynamic') {
            if ($this->sitemapCategory == 1) {
                $default_view = "sitemap_dynamic_category.sitemap_detail";
                if ($this->sitemapCriteria == 1) {
                    $default_view = "sitemap_dynamic_category_criteria.sitemap_detail";
                }
            } else {
                $default_view = "sitemap_dynamic.sitemap_detail";
            }
        } else {
            if ($this->sitemap->feature_tag == 'homepage') {
                $default_view = "homepage";
            } else {
                $default_view = "sitemap_static.sitemap_detail";
            }
        }

        $view_folder = resource_path('views/web/pages/' . mb_strtolower($this->sitemap->sitemapType->name));
        $view = $view_folder . "/sitemap.blade.php";

        if (!file_exists($view_folder)) {
            $oldmask = umask(0);
            mkdir($view_folder, 0777, true);
            umask($oldmask);
        }

        if (!file_exists($view)) {
            $replaced = "@extends('web.inc.app')\n\n@php\n\t".'$sitemap = $mediapress->data[\'sitemap\'];'."\n@endphp\n\n@section('content')\n\t@include('ContentWeb::default." . $default_view . "')\n@endsection";
            file_put_contents($view_folder . "/sitemap.blade.php", $replaced);
        }
    }

    /**
     * @return void
     */
    private function createPageBlade(): void
    {
        if ($this->sitemapCategory == 1) {
            $default_view = "sitemap_dynamic_category.page_detail";
            if ($this->sitemapCategory == 1) {
                $default_view = "sitemap_dynamic_category_criteria.page_detail";
            }
        } else {
            $default_view = "sitemap_dynamic.page_detail";
        }

        $view_folder = resource_path('views/web/pages/' . mb_strtolower($this->sitemap->sitemapType->name));
        $view = $view_folder . "/page.blade.php";

        if (!file_exists($view_folder)) {
            $oldmask = umask(0);
            mkdir($view_folder, 0777, true);
            umask($oldmask);
        }

        if (!file_exists($view)) {
            $replaced = "@extends('web.inc.app')\n\n@php\n\t".'$sitemap = $mediapress->data[\'sitemap\'];'."\n@endphp\n\n@section('content')\n\t@include('ContentWeb::default." . $default_view . "')\n@endsection";
            file_put_contents($view_folder . "/page.blade.php", $replaced);
        }
    }

    /**
     * @return void
     */
    private function createCategoryBlade(): void
    {
        $view_folder = resource_path('views/web/pages/' . mb_strtolower($this->sitemap->sitemapType->name));

        if (!file_exists($view_folder)) {
            $oldmask = umask(0);
            mkdir($view_folder, 0777, true);
            umask($oldmask);
        }

        if (!file_exists($view_folder . "/category_children.blade.php")) {
            if ($this->sitemapCriteria == 1) {
                $replaced_data = "@extends('web.inc.app')\n@section('content')\n\n\t@include('ContentWeb::default.sitemap_dynamic_category_criteria.category_children_detail')\n\n@endsection";
            } else {
                $replaced_data = "@extends('web.inc.app')\n@section('content')\n\n\t@include('ContentWeb::default.sitemap_dynamic_category.category_children_detail')\n\n@endsection";
            }
            file_put_contents($view_folder . "/category_children.blade.php", $replaced_data);
        }


        if (!file_exists($view_folder . "/category.blade.php")) {
            if ($this->sitemapCriteria == 1) {
                $replaced_data = "@extends('web.inc.app')\n@section('content')\n\n\t@include('ContentWeb::default.sitemap_dynamic_category_criteria.category_detail')\n\n@endsection";
            } else {
                $replaced_data = "@extends('web.inc.app')\n@section('content')\n\n\t@include('ContentWeb::default.sitemap_dynamic_category.category_detail')\n\n@endsection";
            }
            file_put_contents($view_folder . "/category.blade.php", $replaced_data);
        }

        if ($this->sitemapCriteria == 1 && !file_exists($view_folder . "/category_ajax.blade.php")) {
            $replaced_data = "@include('ContentWeb::default.sitemap_dynamic_category_criteria.category_ajax" . "')";
            file_put_contents($view_folder . "/category_ajax.blade.php", $replaced_data);
        }
    }
    //endregion
}
