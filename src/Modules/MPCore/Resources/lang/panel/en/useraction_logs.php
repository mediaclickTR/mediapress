<?php

return [
    'title' => 'User Actions',
    'website' => 'Website',
    'user' => 'User',
    'content' => 'Action Detail',
    'list_size' => 'List Count',
    'clear_filters' => 'Clear Filters',
];
