<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSender extends Model
{
    protected $table = 'email_sender';

    public $timestamps = true;

    protected $primaryKey = 'id';
    protected $fillable = ['maillist_id','mailtemplate_id'];

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function maillist()
    {
        return $this->belongsTo(Maillist::class);
    }

    public function email_template()
    {
        return $this->belongsTo(MailTemplate::class, "mailtemplate_id");
    }
}
