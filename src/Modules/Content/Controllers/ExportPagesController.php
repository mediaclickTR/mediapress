<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Exports\PagesExport;
use PhpOffice\PhpWord\Shared\Html;
use Maatwebsite\Excel\Facades\Excel;
use Mediapress\Http\Controllers\PanelController as Controller;

class ExportPagesController extends Controller
{
    public function selectLanguage()
    {
        $website = session('panel.website');
        $countryGroups = $website->countryGroups;

        return view('ContentPanel::export_pages.select_language', compact('countryGroups'));
    }

    public function index(Request $request)
    {
        $lang = $request->get('language');

        if ($lang) {
            $lang = explode(',', $lang);

            $sitemaps = Sitemap::whereHas('sitemapType', function ($q) {
                $q->where('sitemap_type_type', 'dynamic');
            })
                ->whereHas('detail', function ($q) use ($lang) {
                    $q->where('country_group_id', $lang[0])
                        ->where('language_id', $lang[1]);
                })
                ->whereHas('websites', function ($q) use ($lang) {
                    $q->where('id', session('panel.website.id'));
                })
                ->get();
        } else {
            return $this->selectLanguage();
        }


        return view('ContentPanel::export_pages.index', compact('sitemaps'));
    }

    public function export(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $tempLanguage = explode(',', $request->get('language'));
        $sitemaps = $request->get('sitemaps');

        if (count($tempLanguage) != 2) {
            return redirect()->route('Content.export_pages.selectLanguage');
        }

        if(is_null($sitemaps)) {
            return redirect()->route('Content.export_pages.index', ['language' => $request->get('language')])->withErrors(['message' => "Sitemap seçmeniz gerekmektedir!"]);
        }

        $countryGroupId = $tempLanguage[0] * 1;
        $languageId = $tempLanguage[1] * 1;
        $type = $request->get('type');

        if ($type == 'excel') {
            return $this->exportToExcel($countryGroupId, $languageId, $sitemaps);
        } elseif ($type == 'word') {
            return $this->exportToWord($countryGroupId, $languageId, $sitemaps);
        }
    }

    private function exportToExcel(int $countryGroupId, int $languageId, array $sitemaps) {

        $export = new PagesExport($countryGroupId, $languageId, $sitemaps);
        return Excel::download($export, 'pages.xlsx');
    }

    private function exportToWord(int $countryGroupId, int $languageId, array $sitemaps)
    {

        $baseUploadUrl = url('/uploads');
        $unique_id = uniqid();
        mkdir(storage_path('app/exports/' . $unique_id), 0755, true);

        $folders = Sitemap::with('sitemapType')
            ->whereIn('id', $sitemaps)
            ->get()
            ->pluck('sitemapType.name', 'id')
            ->toArray();


        $pages = Page::where('status', 1)
            ->whereIn('sitemap_id', $sitemaps)
            ->whereHas('details', function ($q) use ($countryGroupId, $languageId) {
                $q->where('country_group_id', $countryGroupId)
                    ->where('language_id', $languageId);
            })
            ->get(['id', 'sitemap_id']);


        foreach ($pages as $page) {

            try {
                $detail = $page->details()
                    ->where('country_group_id', $countryGroupId)
                    ->where('language_id', $languageId)
                    ->first();

                $detailText = strip_tags($detail->detail, '<p><ul><h1><h2><h3><h4><h5><h6><li><ol><br>');

                $phpWord = new \PhpOffice\PhpWord\PhpWord();
                $section = $phpWord->addSection();
                $section->addText($detail->name, array('size' => 20, 'bold' => true));
                Html::addHtml($section, '<p>&nbsp;</p>', false, false);
                Html::addHtml($section, str_replace('/uploads', $baseUploadUrl, $detailText), false, false);
                Html::addHtml($section, '<p>&nbsp;</p>', false, false);
                Html::addHtml($section, '<p>&nbsp;</p>', false, false);

                foreach ($detail->extras as $extra) {
                    $extraText = strip_tags($extra->value, '<p><ul><h1><h2><h3><h4><h5><h6><li><ol><br>');
                    Html::addHtml($section, $extra->key . ':&nbsp;' . $extraText, false, false);
                    Html::addHtml($section, '<p>&nbsp;</p>', false, false);
                }

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                $folderName = 'app/exports/' . $unique_id . '/' . $folders[$page->sitemap_id];
                $folder = storage_path($folderName);

                if (!file_exists($folder)) {
                    mkdir($folder, 0755, true);
                }

                $objWriter->save(storage_path($folderName . '/' . \Str::slug($detail->name)) . '.docx');

            } catch (\Exception $e) {

            }
        }

        $zip = new \ZipArchive();
        if (!file_exists(public_path('vendor/storage'))) {
            mkdir(public_path('vendor/storage'), 0755, true);
        }
        $zip->open(public_path('vendor/storage/' . $unique_id . '.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $folder = storage_path('app/exports/' . $unique_id);
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($folder),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($folder) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();
        $this->deleteDir($folder);
        return response()->download(public_path('vendor/storage/' . $unique_id . '.zip'));
    }

    private function deleteDir(string $dirPath): void
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
}
