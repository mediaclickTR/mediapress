<div class="modal fade" id="modal" tabindex="-3" role="dialog" aria-labelledby="modal2"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Type: <span class="type"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" class="form-holder">
                    <input type="hidden" name="id" class="item_id">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="options-tab" data-toggle="tab" href="#options" role="tab"
                               aria-controls="options" aria-selected="true">options</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="html-tab" data-toggle="tab" href="#html" role="tab"
                               aria-controls="html" aria-selected="false">html</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="attributes-tab" data-toggle="tab" href="#attributes" role="tab"
                               aria-controls="attributes" aria-selected="false">attr</a>
                        </li>
                        <li class="nav-item mfiler">
                            <a class="nav-link" id="attributes-tab" data-toggle="tab" href="#tags" role="tab"
                               aria-controls="tags" aria-selected="false">mpfile-tags</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="params-tab" data-toggle="tab" href="#params" role="tab"
                               aria-controls="params" aria-selected="false">params</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="options" role="tabpanel"
                             aria-labelledby="options-tab">

                        </div>
                        <div class="tab-pane fade" id="html" role="tabpanel" aria-labelledby="html-tab">...</div>
                        <div class="tab-pane fade" id="attributes" role="tabpanel" aria-labelledby="attributes-tab">

                        </div>
                        <div class="tab-pane fade mfiler" id="tags" role="tabpanel" aria-labelledby="tags-tab">


                        </div>
                        <div class="tab-pane fade" id="params" role="tabpanel" aria-labelledby="params-tab">...
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="pull-left btn btn-success mr-auto" href="javascript:void(0);"
                   onclick="javascript:addnew()">Add New</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="javascript:save();">Save changes</button>
            </div>
        </div>
    </div>
</div>

