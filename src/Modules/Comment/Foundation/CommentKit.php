<?php


namespace Mediapress\Modules\Comment\Foundation;

use http\Exception;
use Mediapress\Modules\Comment\Models\Comment;
use Mediapress\Modules\Comment\Models\CommentStructure;
use Mediapress\Survey\Models\Survey;
use Mediapress\Survey\Models\SurveyQuestion;

class CommentKit
{
    public $key;
    public $websiteId;
    public $showTime;

    public $showStatus;

    public $authCheck;

    public $saveUrl;

    public $commentStructure;
    public $comments;

    public $parentModel;
    public $hiddenInputs;


    /**
     * @param string $key
     * @return $this
     */
    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }
    /**
     * @param string $key
     * @return $this
     */
    public function setParentModel($parentModel): self
    {
        $this->parentModel = $parentModel;

        return $this;
    }

    /**
     * @return $this
     */
    public function init(): self
    {
        $this->setWebsiteId();
        $this->setCommentStructure();

        if (is_null($this->commentStructure)) {
            return $this;
        }
        $this->setShowStatus();
        $this->setSettings();
        $this->setConditions();
        $this->setComments();
        $this->setSaveUrl();
        $this->setHiddenInputs();

        return $this;
    }

    public function setWebsiteId(): void
    {
        $mediapress = mediapress();
        $this->websiteId = $mediapress->website->id;
    }

    private function setCommentStructure(): void
    {
        $this->commentStructure = CommentStructure::where('key', $this->key)
            ->where('status', 1)
            ->where('website_id', $this->websiteId)
            ->first();

    }

    private function setShowStatus()
    {
        $this->showStatus = true;
        if ($this->commentStructure->condition == 2 && !auth()->check()) {
            $this->showStatus = false;
        }
    }

    private function setConditions(): void
    {
        $this->authCheck = $this->commentStructure->condition == 2;
    }

    private function setSettings(): void
    {
        $this->showTime = $this->commentStructure->show_time;
    }

    private function setComments()
    {
        if(! $this->parentModel) {
            $mediapress = mediapress();
            $this->parentModel = $mediapress->parent;
        }

        $this->comments = $this->commentStructure->comments()
            ->where('status', 1)
            ->where('parent_id', '<>', null)
            ->where('model_id', $this->parentModel->id)
            ->where('model_class', get_class($this->parentModel))
            ->with(['children' => function ($q) {
                $q->where('status', 1);
            }])
            ->get();
    }

    private function setSaveUrl()
    {
        $this->saveUrl = route('commentSave', ['commentStructureId' => $this->commentStructure->id]);
    }

    private function setHiddenInputs()
    {
        if(! $this->parentModel) {
            $mediapress = mediapress();
            $this->parentModel = $mediapress->parent;
        }

        $content = '<input type="hidden" name="model_class" value="' . get_class($this->parentModel) . '">';
        $content .= '<input type="hidden" name="model_id" value="' . $this->parentModel->id . '">';

        $this->hiddenInputs = $content;
    }
}
