<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Support\Database\CacheQueryBuilder;

class Backup extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;
    protected $connection = 'mediapress_backup';
    protected $table = 'backups';
    protected $fillable = ["model_id", 'model_type','content','admin_id','ip'];
    protected $casts = ['content'=>'json'];


    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
