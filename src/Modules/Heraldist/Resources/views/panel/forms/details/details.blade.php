@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("HeraldistPanel::forms.form_detail") !!}</div>
            </div>
            <div class="float-right">
                <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#notes" role="button" aria-expanded="false" aria-controls="notes">
                    <i class="fa fa-file"></i>{!! trans("HeraldistPanel::forms.notes") !!}
                </a>
            </div>
        </div>

        {{--
           <div class="float-right">
               <select class="language-selector">
                   <option value="">{!! trans("MPCorePanel::general.select.language") !!}</option>
                   <option {!! (session('panel.active_language') && session('panel.active_language')->id == 760) ? 'selected' : '' !!} value="tr-TR">TR</option>
                   <option {!! (session('panel.active_language') && session('panel.active_language')->id == 616) ? 'selected' : '' !!} value="en-US">English</option>
               </select>
           </div>
           --}}

        <div class="p-30 mt-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="collapse" id="notes">
                        <div class="card card-body">
                            <ul>
                                <li>Array türünde input(lar) için <code>is_array</code> adında attr eklenmelidir.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <section id="main_content" class="inner">
                <form class="build-form clearfix"></form>
                <div class="render-form"></div>
            </section>

            <div class="container render-btn-wrap" id="editor-action-buttons">
            </div>
            <form id="form_post" action="{{ route('Heraldist.forms.detail.createOrUpdate',$form_id) }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="form_data" id="form_data">
            </form>
            {{--
            <form action="#" novalidate method="POST">
            <div id="build-wrap"></div>
            </form>
            --}}
        </div>
    </div>
@endsection
@push("styles")
    <style>
        .formeo.formeo-editor .formeo-controls{margin-left:30px;}
    </style>
    <link rel="stylesheet" href="{!! asset("vendor/mediapress/css/formeo.min.css") !!}">
@endpush
@push('scripts')
    <script src="{!! asset("vendor/mediapress/js/formeo.min.js") !!}"></script>
    <script>


        jQuery(function ($) {

            @if($form->formbuilder_json!=null || $form->formbuilder_json!="")
            sessionStorage.setItem('formeo-formData', '{!! addslashes($form->formbuilder_json) !!}');
            @else
            sessionStorage.setItem('formeo-formData', '{}');
            @endif


            //Güncelleme yapılacaksa
            {{--   @if($form->formbuilder_json)
                options = {
                fieldRemoveWarn: true, // defaults to false
                formData: "{!! addslashes($form->formbuilder_json) !!}"
            };
            var formBuilder = $(fbEditor).formBuilder(options);
                    @else
            var formBuilder = $(fbEditor).formBuilder();

            @endif

            var fbEditor = document.getElementById('build-wrap');

            document.getElementById('logJSON').addEventListener('click', function (e) {

                e.preventDefault();
                /*
                var datas = formBuilder.actions.getData('json');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('Heraldist.forms.detail.createOrUpdate',$form_id) }}",
                type: "post",
                data: {datas: JSON.stringify(datas)},
                dataType  : 'json',
                success:function(data) {
                    if(data==1)
                    {
                        swal({
                            title: '{{ trans("MPCorePanel::general.success_title") }}',
                            text: '{{ trans("MPCorePanel::general.success_message") }}',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                        });
                    }
                    else
                    {
                        swal({
                            title: '{{ trans("MPCorePanel::general.error_title") }}',
                            text: '{{ trans("MPCorePanel::general.error_message") }}',
                            type: 'danger',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                        });
                    }
                }
            });
            */
            });

            $('.language-selector').change(function () {
                var lang = this.value;
                formBuilder.actions.setLang(lang);
            });

            $(".language-selector").trigger('change');--}}
        });
    </script>
@endpush
@push("styles")
    <style>
        .form-wrap.form-builder .frmb .form-elements .false-label:first-child, .form-wrap.form-builder .frmb .form-elements label:first-child {
            padding-top: 0px !important;
        }

        label {
            position: relative !important;
        }

        input[type=checkbox], input[type=radio] {
            height: 20px !important;
        }

        .language-selector li {
            display: inline-block;
            padding: 10px;
            cursor: pointer;
            background: white;
            margin: 0;
            line-height: 18px;
        }
    </style>
@endpush
