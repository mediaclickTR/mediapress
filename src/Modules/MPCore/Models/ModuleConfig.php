<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class ModuleConfig extends Model
{
    use CacheQueryBuilder;
    protected $table = 'module_configs';

    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = ['module_id', 'config_name', 'key','value','type','details'];
}
