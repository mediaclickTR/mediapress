<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class DynamicUrl extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;

    protected $table = 'dynamic_urls';
    public $timestamps = true;
    protected $fillable = ['url_id', 'regex','website_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function website()
    {
        $this->hasManyThrough(Website::class, Url::class);
    }

    public function url()
    {
        return $this->hasOne(Url::class, "id", "url_id");
    }

    public function metas()
    {
        return $this->morphMany(Meta::class, "url");
    }
}
