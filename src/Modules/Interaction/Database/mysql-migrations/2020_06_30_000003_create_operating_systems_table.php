<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOperatingSystemsTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('operating_systems'))
            return;

		Schema::connection('mediapress_interaction')->create('operating_systems', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('operating_systems');
	}
}
