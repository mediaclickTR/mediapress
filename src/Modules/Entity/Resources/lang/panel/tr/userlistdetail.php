<?php

return [
  'user-name'=>'Üye Adı',
  'model-name'=>'Model',
  'user-selection'=>'Üye Seçimi',
  'user-update'=>'Üye Güncelle',
  'user-detail'=>'Üye Detayı',
  'userlist-add'=>'Listeye Üye Ekle',
];