<?php

namespace Mediapress\Foundation;

use Illuminate\Support\Facades\File;
use Mediapress\Models\Module;
use Mediapress\Models\Slot;

class ModulesEngine
{
    public $baseModules;
    public $modulesdir;

    public function __construct()
    {
        $this->baseModules = config("baseModules");
        $this->modulesdir = base_path('vendor/mediapress/mediapress/src/Modules');
    }


    public function getMPModules($only_actives=false)
    {
        $modules = [];
        if (!app()->runningInConsole()) {
            $all_modules = $only_actives ? Module::where("status",1)->get() : Module::all();
            if ($all_modules) {
                foreach ($all_modules as $module) {
                    $modules[$module->name] = $module->namespace;
                }
            } else {
                throw new \InvalidArgumentException("Modül bulunamadı.");
            }
        } else {
            $modules = $this->getMPModulesFromFilesystem();
        }
        return $modules;
    }

    public function getMPModulesFromFilesystem(){
        $modules = [];
        foreach (File::directories($this->modulesdir) as $dic) {
            $dic = basename($dic);
            $modules[$dic] = "Mediapress\\Modules\\" . $dic;
        }
        return $modules;
    }

    public function getMPSlots()
    {
        $slots = [];

        $all_slots = Slot::get();
        if ($all_slots) {
            foreach ($all_slots as $slot) {
                $slots [] = $slot->slot;
            }
        } else {
            throw new \InvalidArgumentException("Slot bulunamadı.");
        }
        return $slots;
    }

    public function getModulesActions($only_modules=[], $only_headers = false){
        $modules = \Arr::except(self::getMPModules(true),$only_modules);
        $actions = [];
        foreach ($modules as $module_name => $module_class){
            $actions[$module_name]=self::getModuleActions($module_name, $module_class, $only_headers);
        }
        return $actions;
    }

    public function getModuleActions($module_key, $module_class, $only_header=false){
        $container = [
            "key"=>$module_key,
            "name"=> trans($module_key."Panel::module_info.name"),
            "name_affix"=>"",
            'item_model' => $module_class,
            'item_id' => "*",
            'data' => [
                "is_root" => true,
                "is_variation" => false,
                "is_sub" => false,
                "descendant_of_sub" => false,
                "descendant_of_variation" => false
            ],
            'actions' => [],
            'subs' => [],
            'variations' => [],
            'settings' => []
        ];

        if( ! $only_header){
            $module_path = "".$module_class."\\".$module_key;
            if(class_exists($module_path) && method_exists($module_path,"fillActionsContainer")){
                $container = (new $module_path)->fillActionsContainer($container);
            }

        }

        return $container;

    }


    public function getMPModuleNames($only_actives = false)
    {
        $modules = $this->getMPModules($only_actives);
        $module_names = [];
        if ($modules) {
            foreach ($modules as $name => $namespace) {
                $module_names[] = $name;
            }
        } else {
            throw new \InvalidArgumentException("Modül bulunamadı.");
        }

        return $module_names;
    }

    // modullere ait nesneleri veren method
    public function getModuleClasses()
    {
        $modules = $this->getMPModules(true);
        $models = [];
        foreach ($modules as $name => $namespace) {
            // TODO: aşağıdaki bir satır ve bir blok, mp4 geriye dönük uyumluluk için. MP5'e tamamen geçildiğinde silinecek.
            $module_directory = base_path('vendor/mediapress/mediapress/src/Modules/' . $name . '/Models');
            if (file_exists($module_directory)) {
                //dump($module_directory);
                $model_files = File::allFiles($module_directory);
                foreach ($model_files as $file) {
                    $file_name = pathinfo($file, PATHINFO_FILENAME);
                    $file_root = "Mediapress\\Modules\\" . $name . "\\Models\\" . $file_name;
                    $models[$name][$file_name] = $file_root;
                }
            }
            // Buradan yukarısı, todo'ya kadar silinecek

            $module_directory = base_path('vendor/mediapress/'.strtolower($name).'/src/Models');
            if (file_exists($module_directory)) {
                $model_files = File::allFiles($module_directory);
                foreach ($model_files as $file) {
                    $file_name = pathinfo($file, PATHINFO_FILENAME);
                    $file_root = "Mediapress\\" . $name . "\\Models\\" . $file_name;
                    $models[$name][$file_name] = $file_root;
                }
            }
        }
        return $models;
    }

    public function getModulesOnSlots(array $only_slots = [])
    {
        return count($only_slots) ? Slot::whereIn('slot', $only_slots)->with("module")->get() : Slot::with("module")->get();
    }

}
