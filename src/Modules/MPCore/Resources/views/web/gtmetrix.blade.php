<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow"/>
    @if($css != "")
        <link rel="stylesheet" href="/assets/css/{{$css}}.css"/>
    @endif
</head>
<body>

<h3>Images</h3>
@foreach($images as $image)
    <img src="{{$image}}" alt="">
    <br>
@endforeach
<hr>

<h3>Uploads</h3>

@foreach($uploads as $upload)
    <img src="{{$upload}}" alt="">
    <br>
@endforeach
<hr>


@if($js != "")
    <script src="/assets/js/{{$js}}.js"></script>
@endif
</body>
</html>
