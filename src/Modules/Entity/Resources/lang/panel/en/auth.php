<?php

return [
    "sections" => [
        "users" => "Users",
        "user_lists" => "User Lists",
        "entities" => "Entity / Organization",
        "entity_lists" => "Entity / Organization Lists",

    ],
    "users" => "Users",
    "corporate" => "Corporate Entries",
];
