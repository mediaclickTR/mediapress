<?php

return [
    /*
     * MPMailler
     */
    'website' => 'Website',
    'using_queue' => 'Kuyruklama Kullanılsın mı? (Kuruklama yüksek miktarda email gönderiminde yarar sağlar)',
    'quick_create' => 'Hızlı Oluştur',
    'list_manage' => 'E-Posta Liste Yönetimi',
    'template_manage' => 'E-Posta Şablon Yönetimi',
    'emailsender' => 'E-Posta Gönderici',
    /*
     * Maillists
     */
    'maillists' => 'E-Posta Listeleri',
    'create_maillist' => 'E-Posta Listesi Oluştur',
    'edit_maillist' => 'E-Posta Listesini Düzenle',
    'maillist_name' => 'E-Posta Liste Adı',
    'email_select' => 'E-Posta Seçimi',
    'sendedemails' => 'Gönderilmiş E-Postalar',
    'mailsend' => 'E-Posta Gönder',
    'messages' => 'Mesajlar',
    'email_sender' => 'Mesajlar',
    'e_bulletin' => 'E-Bülten',
    'emailsender_name'=>'E-Posta Gönderim Adı',
    /*
     * Emailtemplates
     */
    'email_template_select' => 'E-Posta Şablon Seçimi',
    'emailtemplates' => 'E-Posta Şablonları',
    'create_template' => 'E-Posta Şablonu Oluştur',
    'edit_template' => 'E-Posta Şablonu Düzenle',
    'template_name' => 'Şablon Adı',
    'template_title' => 'Şablon Başlığı',
    'template_subject' => 'Şablon Konusu',
    'template_detail' => 'Şablon Detayı',
    'template_content' => 'Şablon İçeriği',
    'template_create_info' => 'Kullanıcı adı ( %username% ), Başlık( %mail_title% ), Konu ( %mail_subject% ) ve Detay( %mail_detail% ) parantez içerisindeki değişkenleri uygun yerlere yazarak kişiye özel e-postalar gönderebilirsiniz',
    /*
     * EmailSender
     */
    'newsend_email' => 'Yeni E-Posta Gönder',
    'select_maillist' => 'E-Posta Liste Seçimi',
    'select_template' => 'Şablon Seçimi',
    'create_quicklist' => 'Hızlı E-Posta Listesi Oluştur',
    'create_quicktemplate' => 'Hızlı Şablon Oluştur',
    'email_detail' => 'E-Posta Detayı',
    'sended_emails' => 'Gönderilen Adresler',

     ## Mailchimp ##

     /*
     * Maillists
     */
    'mailchimp_maillists' => 'Mailchimp E-Posta Listeleri',
    'mailchimp_listid' => 'List ID',
    'sender_email' => 'Gönderen E-Posta',
    'sender_name' => 'Gönderen Adı',

    /*
     * Maillist > Members
     */
    'email_selection' => 'E-Posta Seçimi',
    'create_user' => 'Üye Oluştur',
    'edit_user' => 'Üye Düzenle',
    'lists_user' => 'Liste Üyeleri',
    'back_maillist'=>'E-Posta listesine geri dön',
    /*
     * Mailtemplates
     */
    'mailchimp_emailtemplates'=>'Mailchimp E-Posta Şablonları',
    'template_image'=>'Şablon Resmi',
    'previewin_mailchimp'=>'Mailchimp Sitesinde Görüntüle',
    'mailchimp_template_info'=>'Mailchimp API e-posta şablonu düzenleme işlemine destek vermiyor. Bu yüzden şablon üzerinde değişiklik yapabilmek için silip tekrar kaydetmelisiniz..',
    /*
     * Campaigns
     */
    'campaigns' => 'Kampanyalar',
    'create_campaign' => 'Kampanya Oluştur',
    'edit_campaign' => 'Kampanya Düzenle',
    'campaign_name' => 'Kampanya Adı',
    'multiple_emailsend' => 'Gönderim Durumu',
    'template_id'=>'Şablon ID',
    'sended_email'=>'Gönderilmiş',
    'email_maillist'=>'E-Posta Listesi',
    'email_template'=>'E-Posta Şablonu',

    /*
     * Errors
     */

    'please_select_email'=>'Lütfen E-Posta Seçimi Yapınız',
    'not_found_email_template'=> "Böyle bir e-posta şablonu bulunamadı.",
    'not_found_email_list'=>"Böyle bir e-posta e-posta listesi bulunamadı.",
    'email_template_empty_fields'=>"Şablon seçimlerinizde boş alanlar var. Lütfen doldurup tekrar deneyiniz",
];
