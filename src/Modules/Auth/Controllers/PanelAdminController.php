<?php

namespace Mediapress\Modules\Auth\Controllers;

use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Auth\Models\Role;
use Illuminate\Support\Facades\Auth;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\DataTable\TableBuilderTrait;
use Yajra\DataTables\Html\Builder;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Facades\MPCore;

class PanelAdminController extends Controller
{
    use TableBuilderTrait;

    public const ADMIN = "admin";
    public const ROLE_ID = 'role_id';
    public const LANGUAGE_ID = 'language_id';
    public const FIRST_NAME = 'first_name';
    public const LAST_NAME = 'last_name';
    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASS_WORD = 'password';
    public const AUTH_PANEL_ADMIN_EMAIL_ADMIN = "AuthPanel::admin.email-admin";
    public const AUTH_PANEL_ADMIN_PASS_WORD_ADMIN = "AuthPanel::admin.password-admin";
    public const AUTH_PANEL_ADMIN_USERNAME_ADMIN = "AuthPanel::admin.username-admin";
    public const AUTH_PANEL_ADMIN_LAST_NAME_ADMIN = "AuthPanel::admin.last-name-admin";
    public const AUTH_PANEL_ADMIN_NAME_ADMIN = "AuthPanel::admin.name-admin";
    public const AUTH_PANEL_ADMIN_ROLE_ADMIN = "AuthPanel::admin.role-admin";
    public const REQUIRED = 'required';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';
    public const CONFIRMED = 'confirmed';
    public const AUTH_ADMINS_INDEX = "Auth.admins.index";
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const ERROR = 'error';
    public const MP_CORE_PANEL_GENERAL_ERROR_MESSAGE = 'MPCorePanel::general.error_message';

    public function __construct()
    {
        Bouncer::useRoleModel(Role::class);
    }

    public function index(Builder $builder)
    {
        if(! activeUserCan(["auth.admins.index",])){
            return rejectResponse();
        }
        $dataTable = $this->columns($builder)->ajax(route('Auth.admins.ajax'));

        return view("AuthPanel::admins.index", compact('dataTable'));
    }

    public function create()
    {

        if(! activeUserCan(["auth.admins.create",])){return rejectResponse();}

        $languages = MPCore::getLanguages()->whereIn('id', session()->get('panel.languages.languages'))->pluck("name", "id");
        $zones = CountryGroup::get()->pluck("list_title","id")->toArray();
        $roles = Role::where("name","<>","SuperMCAdmin")->status("1")->get()->pluck("name", "id");
        $form = "formbuilderden gelecek";

        return view("AuthPanel::admins.create", compact("roles","languages", "zones","form"));
    }

    public function edit($id)
    {
        if(! activeUserCan(["auth.admins.update",])){return rejectResponse();}

        $admin = Admin::find($id);
        $languages = MPCore::getLanguages()->whereIn('id', session()->get('panel.languages.languages'))->pluck("name", "id");
        $zones = CountryGroup::get()->pluck("list_title","id")->toArray();
        $roles = Role::where("name","<>","SuperMCAdmin")->get()->pluck("name", "id");
        $form = "formbuilderden gelecek";

        return view("AuthPanel::admins.create", compact(self::ADMIN,"roles","languages", "zones","id", "form",  self::ADMIN));
    }

    public function store(Request $request)
    {

        $roles_to_assign[] = $request->role_id;

        Bouncer::useRoleModel(Role::class);


        // Permissions Checkpoint
        if(! activeUserCan(["auth.admins.create"])){return rejectResponse();}
        /*
         * Validation
         */

        $fields = [
            self::ROLE_ID => trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN),
            self::LANGUAGE_ID => trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN),
            self::FIRST_NAME => trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN),
            self::LAST_NAME => trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN),
            'phone' => trans("AuthPanel::admin.phone-admin"),
            self::EMAIL => trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN),
            self::PASS_WORD => trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN),
        ];

        $rules = [
            //'role_id' => 'required',
            self::LANGUAGE_ID => self::REQUIRED,
            self::FIRST_NAME => self::REQUIRED,
            self::LAST_NAME => self::REQUIRED,
            self::EMAIL => 'required|unique:admins',
            self::PASS_WORD => 'required|confirmed',
        ];

        $messages = [
            'role_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN)]),
            'language_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans("AuthPanel::admin.language-admin")]),
            'first_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN)]),
            'last_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
            'password.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
        ];


        $data = $request->except("_token","password_confirmation", self::PASS_WORD);

        $data += [self::PASS_WORD =>bcrypt($request->password)];

        $this->validate($request, $rules, $messages, $fields);

        $data['username'] = trim($data['first_name']) . ' ' . trim($data['last_name']);

        $insert = Admin::firstOrCreate($data);

        if ($insert){

            $admin2 = Admin::find($insert->id);

            //return dd($request->all(), $insert);

            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            if($schema->hasColumn('admins', 'api_token')) {
                $insert->api_token = uniqid();
                $insert->save();
            }

            // assign requested roles
            foreach($roles_to_assign as $rta){
                Bouncer::assign($rta)->to($insert);
                Bouncer::assign($rta)->to($admin2);
            }
            // Log
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);

            return redirect()->route(self::AUTH_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));
    }

    public function update(Request $request)
    {

        //return dd($request->all());

        $admin = Admin::find($request->id);
        $roles_to_assign[] = $request->role_id;
        Bouncer::useRoleModel(Role::class);

        // Permissions Checkpoint
        if(! activeUserCan(["auth.admins.update"])){return rejectResponse();}

        /*
         * Validation
         */

        $fields = [
            self::ROLE_ID => trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN),
            self::LANGUAGE_ID => trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN),
            self::FIRST_NAME => trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN),
            self::LAST_NAME => trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN),
            'phone' => trans("AuthPanel::admin.phone-admin"),
            self::EMAIL => trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN),
            self::PASS_WORD => trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN),
        ];

        $rules = [
            self::ROLE_ID => self::REQUIRED,
            self::LANGUAGE_ID => self::REQUIRED,
            self::FIRST_NAME => self::REQUIRED,
            self::LAST_NAME => self::REQUIRED,
            self::EMAIL => 'unique:admins,email,'.$admin->id,
            self::PASS_WORD => self::CONFIRMED,
        ];

        $messages = [
            'role_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_ROLE_ADMIN)]),
            'language_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans("AuthPanel::admin.language-admin")]),
            'first_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN)]),
            'last_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
        ];


        $data = $request->except("_token","password_confirmation", self::PASS_WORD);
        if ($request->password) {
            $data += [self::PASS_WORD =>bcrypt($request->password)];
        }

        $this->validate($request, $rules, $messages, $fields);

        $data['username'] = trim($data['first_name']) . ' ' . trim($data['last_name']);

        $update=Admin::updateOrCreate(['id'=>$admin->id],$data);

        if ($update){

            $admin2 = Admin::find($update->id);
            //return dd($update->getRoles(), $admin2->getRoles());

            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            if($schema->hasColumn('admins', 'api_token')) {
                if(is_null($update->api_token)) {
                    $update->api_token = uniqid();
                    $update->save();
                }
            }
            $roles_assigned = $admin->getRoles()->toArray();

            // first retract current roles from user
            foreach($roles_assigned as $ra){
                Bouncer::retract($ra)->from($update);
                Bouncer::retract($ra)->from($admin2);
            }

            // assign requested roles
            foreach($roles_to_assign as $rta){
                Bouncer::assign($rta)->to($update);
                Bouncer::assign($rta)->to($admin2);
            }
            //dump($roles_to_assign);
            //return dd($update->roles()->get(),$admin2->roles()->get());
            // Log
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$update);

            return redirect()->route(self::AUTH_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));

    }

    public function delete($id)
    {

        // Permissions Checkpoint
        if(! activeUserCan(["auth.admins.delete"])){return rejectResponse();}


        $admin = Admin::findOrFail($id);

        if ($admin->delete()) {
            return redirect()->route(self::AUTH_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));
    }
}
