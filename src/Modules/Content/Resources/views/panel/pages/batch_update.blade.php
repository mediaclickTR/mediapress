@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{$sitemap->detail->name}}</div>
            </div>
            @if($sitemap->autoOrder == 1)
                <div class="float-right">
                    <span>{!! trans('ContentPanel::page.order_page.info') !!}</span>
                </div>
            @endif
        </div>
       <div class="p-30">
           <div class="table-field" id="vueTable">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!! trans('ContentPanel::page.order_page.status') !!}</th>
                        <th>{!! trans('ContentPanel::page.order_page.order') !!}</th>
                        <th v-if="categoryExist">{!! trans('ContentPanel::page.order_page.category_name') !!}</th>
                        <th style="text-align: inherit">{!! trans('ContentPanel::page.order_page.page_name') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr v-for="page in pages">
                            <th scope="row">@{{ page.id }}</th>
                            <th>
                                @{{ page.status ? active : passive }}
                            </th>
                            <th><input type="text" class="form-control w-25" :value="page.order" @keyup="updatePage(page.id, 'order', $event)"></th>
                            <th v-if="categoryExist">@{{ page.category_name }}</th>
                            <th>@{{ page.name }}</th>
                        </tr>
                    </tbody>
                </table>
           </div>
       </div>
    </div>
@endsection


@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script>
        var app = new Vue({
            el: '#vueTable',
            data: {
                categoryExist: false,
                pages: {},
                changeInterval:  null,
                active : "{{ trans('ContentPanel::page.order_page.active') }}",
                passive : "{{ trans('ContentPanel::page.order_page.passive') }}",
            },
            mounted: function() {
                this.getPages();
            },
            methods: {
                getPages: function () {

                    clearInterval(this.changeInterval);

                    var self = this;
                    this.changeInterval = setInterval(function () {
                        clearInterval(self.changeInterval);

                        axios.get('{{ route('Content.pages.getPagesForBatchUpdate', ['sitemap_id' => $sitemap->id]) }}').then(function (response) {
                            self.categoryExist = response.data.categoryExist;
                            self.pages = response.data.pages;
                        });
                    }, 300);
                },
                updatePage: function (pageId, type, event) {
                    var data = {
                        'id': pageId,
                        'type': type,
                        'value': parseInt(event.target.value)
                    }

                    var self = this;
                    axios.post('{{ route('Content.pages.updatePageForBatchUpdate', ['sitemap_id' => $sitemap->id]) }}', data).then(function (response) {
                        self.getPages();
                    });
                }
            }
        })
    </script>
@endpush
