<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Foundation\ClusterArray;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\CategoryEngine;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Facades\CriteriaEngine;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;

class CriteriaController extends Controller
{

    public function index(Sitemap $sitemap)
    {

        $websiteId = session("panel.website.id");

        if(! activeUserCan([
                "content.websites.website$websiteId.sitemaps.criterias.index",
                "content.websites.sitemaps.criterias.index"
            ])
        ){
            return rejectResponse();
        }

        $cluster = new ClusterArray();
        $cluster->table_id = 'criteria_id';
        $default_language = session("panel.website")->defaultLanguage()->id;


        $nestable = CriteriaEngine::nestableGetList($sitemap->id, Criteria::class, 'criteria_id');

        $crumbs = [
            [
                "key"=>"taxonomy_index",
                "text"=>"Sınıflandırma",//trans("ContentPanel::categories.categories"),
                "icon"=>"stream",
                "href"=>route("Content.categories.index")
            ],
            [
                "key"=>"criterias_index",
                "text"=>trans("ContentPanel::categories.criterias"),
                "icon"=>"tasks",
                "href"=>route("Content.categories.getList", "criteria")
            ],
            [
                "key"=>"criterias_of_sitemap",
                "text"=>$sitemap->detail->name. " - ". trans("ContentPanel::categories.criterias"),
                "icon"=>"sitemap",
                "href"=>route("Content.categories.criteria.create", $sitemap->id)
            ]
        ];
        $breadcrumb = Content::getBreadcrumb($crumbs);

        $metaset = "[]";

        return view("ContentPanel::categories.criteria.create", compact("sitemap", 'metaset', "nestable", 'breadcrumb'));
    }

    public function store(Request $request, Criteria $criteria)
    {
        return CriteriaEngine::ajaxStore($request, $criteria);
    }

    public function delete(Request $request)
    {
        $criteria = Criteria::find($request->get("id"));
        $criteria->details()->delete();
        $criteria->delete();
        return redirect()->back()->with("message", trans('MPCorePanel::general.success_message'));
    }

    public function orderSave(Request $request)
    {
        if(CriteriaEngine::criteriaOrderSave($request)){
            return redirect()->back()->with("message", trans('MPCorePanel::general.success_message'));
        }else {
            return redirect()->back()->with("error", trans('MPCorePanel::general.danger_message'));
        }
    }

}
