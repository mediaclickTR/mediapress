<?php

namespace Mediapress\Modules\Interaction\Repositories;


use Carbon\Carbon;
use Mediapress\Modules\MPCore\Models\Url;

class BaseRepository
{

    protected $time;
    protected $access_token;
    protected $account_id;

    public function __construct($time, $data)
    {
        $this->time = $time;
        $this->access_token = $data->access_token;
        $this->account_id = $data->account_id;
        $this->start = Carbon::now()->subDays($this->time)->setTime(0, 0, 0);
        $this->finish = Carbon::now()->setTime(23, 59, 59);
    }

    /**
     * @param array $array
     * @param string $default
     * @return array
     */
    protected function pieChart(array $array, string $default = 'N/A'): array
    {

        $colors = [
            '#39D2FF',
            '#FF8373',
            '#FFDA83',
            '#A3A0FB',
            '#FEB200',
            '#39D2FF'
        ];
        $array = collect($array);
        $total = $array->sum();
        $data = [];

        $i = -1;
        foreach ($array as $key => $value) {
            $data[] = [
                'y' => round($value * 100 / $total, 2),
                'name' => $key ? $key : $default,
                'color' => $colors[++$i % 6],
                'value' => $value
            ];
        }
        return $data;
    }

    /**
     * @param $urls
     * @return array
     */
    protected function urlData($urls): array
    {
        $urlData = [];
        foreach ($urls as $url => $count) {

            $tempUrl = Url::where('url', "/" . $url)->first();

            if ($tempUrl) {
                if ($tempUrl->type == 'redirect') {
                    $urlName = optional($tempUrl->model->model)->name;
                } else {
                    $urlName = optional($tempUrl->model)->name;
                }
                $urlData[] = [
                    'name' => $urlName,
                    'url' => $url,
                    'count' => $count
                ];
            } else {
                $urlData[] = [
                    'name' => $url,
                    'url' => $url,
                    'count' => $count
                ];
            }


        }
        return $urlData;
    }

    /**
     * @param $times
     * @return array
     */
    protected function replaceTimes($times): array
    {
        $data = [];
        foreach ($times as $time) {
            $data[] = [
                (int)$time->week, (int)$time->hour, (int)$time->count
            ];
        }
        return $data;
    }
}
