<?php

namespace Mediapress\Modules\Entity\Models;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class EntitylistPivot extends Model
{

    protected $table = 'entitylist_pivot';
    public $timestamps = false;

    protected $fillable = [
        "entitylist_id",
        "model_id",
        "model_type"
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function entitylist()
    {
        return $this->BelongsTo(EntityList::class, "entitylist_id","id");
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

}
