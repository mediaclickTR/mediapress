@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }

        .post-photo{
            width: 200px;
            height: auto;
        }

        .post{
            margin-top: 20px;
        }

        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title" style="">
            <div class="float-left">
                Albüm Oluştur
            </div>
{{--            <div class="float-right" >--}}
{{--                <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>--}}
{{--            </div>--}}

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.index') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Albümler</a>
            </div>
        </div>
        <form action="" id="album-form">
            <div class="table-field">
                <div style="margin-top: 70px !important; border-top: 1px solid #111;">
                    <div class="container" style="margin-top: 20px !important;">
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h5>Albüm Bilgileri</h5>
                                    <hr>
                                </div>

                                <div class="formgroup" style="margin-top: 10px">
                                    <label for="name">Albüm Adı</label>
                                    <input type="text" class="form-control" name="album_name" id="name" value="{!! $album->name !!}">
                                </div>
                                <div class="formgroup" style="margin-top: 10px">
                                    <label for="slug">Albüm Slug</label>
                                    <input type="text" class="form-control" name="album_slug" id="slug"  value="{!! $album->slug !!}">
                                </div>
                                <div class="formgroup " style="margin-top: 10px">
                                    <label for="status">Yayınlama Durumu</label>
                                    <select class="form-control" id="status" name="album_status">
                                        <option value="">Seçiniz</option>
                                        <option value="1" {!! $album->status == 1 ? "selected": "" !!}>Aktif</option>
                                        <option value="0" {!! $album->status == 0 ? "selected": "" !!}>Pasif</option>
                                    </select>
                                </div>
                                <div class="text-center" style="margin-top: 20px">
                                    <h5>Albüm Medyaları</h5>
                                    <hr>
                                    <p>
                                        <a id="refresh-now" role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Şimdi Yenile</a>
                                    </p>

                                </div>

                                <div class="col-xl-12" >
                                    @php($postCounter = 0)
                                    @foreach($instagram as $key => $post)
                                        <div class="col-md-6 post post-{!! $key !!}" style="display:flex; min-height: 320px;">
                                            <div class="col-md-6 align-self-center">
                                                @if($post["type"] == "carousel")
                                                    <div id="carouselExampleControls{!! $postCounter !!}" class="carousel slide" data-ride="carousel">
                                                        <div class="carousel-inner">
                                                            @foreach($post["carousel_media"] as $child)
                                                                <div class="carousel-item {!! $loop->iteration == 1 ? "active": "" !!}">
                                                                    <img src="{!! $child["media_url"] !!}" class="d-block w-100" alt="...">
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <a class="carousel-control-prev" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @elseif($post["type"] == "video")
                                                    <video width="250" controls>
                                                        <source src="{!! $post["media_url"] !!}" type="video/mp4">
                                                        Lütfen "Chrome" Tarayıcı İle Deneyiniz
                                                    </video>
                                                @else
                                                    <div class="col-md-6 align-self-center">
                                                        <img class="post-photo" src="{!! $post["thumbnail_photo"] !!}" alt="">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-6 align-self-center text-center">
                                                <p> <i class="far fa-heart"></i> {!! $post["likes"] !!} Beğenme </p>
                                                <p> <i class="far fa-comments"></i> {!! count($post["comments"]) !!} Yorum </p>

                                                @if(in_array($key, explode(",", $album->sync_priority)))
                                                    <p>
                                                        <div class="col-sm-6">
                                                        <button id="dLabel" class="btn btn-outline-primary " type="button" data-toggle="tooltip" data-placement="top" title="Öncelikli Gönderi">
                                                                <i class="fas fa-star"></i>
                                                            </button>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <button id="dLabel" class="btn btn-outline-danger delete-priority" type="button" data-toggle="tooltip" data-placement="top" title="Sil" data-id="{!! $key !!}">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </div>
                                                    </p>
                                                @endif
                                                {{--                                                <p>--}}
{{--                                                    <div class="col-sm-12">--}}
{{--                                                        <div class="col-sm-4">--}}
{{--                                                            <a data-toggle="tooltip" data-placement="top" title="Detaylı Gör">--}}
{{--                                                                <button id="dLabel" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" type="button">--}}
{{--                                                                    <i class="fas fa-eye"></i>--}}
{{--                                                                </button>--}}
{{--                                                            </a>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-sm-4">--}}
{{--                                                            <button id="dLabel" class="btn btn-outline-danger " type="button" data-toggle="tooltip" data-placement="top" title="Sil">--}}
{{--                                                                <i class="fas fa-trash"></i>--}}
{{--                                                            </button>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="col-sm-4">--}}
{{--                                                            <a href="{!! $post["post_url"] !!}" target="_blank">--}}
{{--                                                                <button id="dLabel" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Gönderiye Git">--}}
{{--                                                                    <i class="fas fa-share"></i>--}}
{{--                                                                </button>--}}
{{--                                                            </a>--}}

{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                </p>--}}
                                            </div>
                                        </div>
                                        @php($postCounter ++)
                                    @endforeach
                                </div>


                                <div class="row">
                                    <div id="accordion" class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Sürekli Yenile
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="padding-bottom: 30px;">
                                                <div class="card-body">
                                                    <div class="col-12">
                                                        <div class="formgroup">
                                                            <label for="limit">Kullanıcı Adı Veya Etiket Giriniz</label>
                                                            <input type="text" name="album_data" class="form-control" id="limit" placeholder="kullaniciadi, #etiket" value="{!! $album["sync_data"] !!}">
                                                        </div>
                                                        <div class="formgroup" style="margin-top: 10px">
                                                            <label for="limit">Limit Belirleyin</label>
                                                            <input type="number" name="album_limit"  class="form-control" id="limit" value="{!! $album["sync_limit"] !!}">
                                                        </div>
                                                        <div class="formgroup"  style="margin-top: 10px">
                                                            <label for="refresh">Yenileme Aralığı</label>

                                                            <select class="form-control" name="album_refresh" id="refresh">
                                                                <option value="">Seçiniz</option>
                                                                <option value="1440" {!! $album->sync_minutes == 1440 ? "selected": "" !!}>Günlük</option>
                                                                <option value="60" {!! $album->sync_minutes == 60 ? "selected": "" !!}>Saatlik</option>
                                                                <option value="1" {!! $album->sync_minutes == 1 ? "selected": "" !!}>Her Dakika</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        Medya Seç
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="padding-bottom: 30px;">
                                                <div class="card-body" >
                                                    <div class="row inline justify-content-md-center">
                                                        <div class="col">
                                                            <label for="formGroupExampleInput">Verileri Kullanıcı Adı Üzerinden Senkronize Et</label>
                                                            <input id="input-user" type="text" class="form-control" placeholder="instagram" value="instagram">
                                                        </div>

                                                        <div class="col">
                                                            <a role="button" id="sync-user" href="javascript:void(0);" class="btn btn-primary form-control" style="margin-top: 30px;">
                                                                <i class="fas fa-sync"></i>Eşitle
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="row inline justify-content-md-center mt-5">
                                                        <div class="col">
                                                            <label for="formGroupExampleInput">Verileri Konu Etiketi Üzerinden Senkronize Et</label>
                                                            <input id="input-hashtag" type="text" class="form-control" placeholder="#instagram" value="instagram">
                                                        </div>

                                                        <div class="col">
                                                            <a id="sync-hashtag" role="button" class="btn btn-primary form-control" style="margin-top: 30px;">
                                                                <i class="fas fa-sync"></i>Eşitle
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div id="postsDiv">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <a id="save" role="button" class="btn btn-primary form-control" style="margin-top: 30px; float: left; width: 100px">
                                Kaydet
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        var posts = "";
        $(".btn").click(function (e) {
            e.preventDefault();
        });

        $(".delete-priority").click(function () {
            var id = $(this).data("id");
            $.post( "{!! route("Content.instagram.deletePrioritiy") !!}", {
                "album": "{!! $album->id !!}",
                "id": id,
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {
                $(".post-"+id).hide();
                //location.reload();
            });
        });

        $("#refresh-now").click(function () {
            $.post( "{!! route("Content.instagram.deleteCache") !!}", {
                "id": "{!! $album->id !!}",
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {
                location.reload();
            });
        });

        $("#save").click(function () {
            $( "input[type=checkbox]:checked" ).each(function () {
                var val = $(this).data("id");
                posts = posts +","+ val;
            });
            var form = $("#album-form");
            var data = getFormData(form);

            $.post( "{!! route("Content.instagram.ajaxEditAlbum") !!}", {
                "id": "{!! $album->id !!}",
                "posts": posts,
                "data": data,
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {
                location.reload();
            });
        });

        $(".refresh").click(function (e) {
            e.preventDefault();
            var option = $(this).data("option");
            if (option == "refresh"){
                $("#schedule").show();
                $("#posts").hide();
            }
            else if(option == "posts"){
                $("#posts").show();
                $("#schedule").hide();
            }
        });

        function getFormData($form){
            var unindexed_array = $form.serializeArray();
            console.log(unindexed_array);
            var indexed_array = {};

            $.map(unindexed_array, function(n, i){
                indexed_array[n['name']] = n['value'];
            });

            return indexed_array;
        }

        $("#sync-hashtag").click(function (e) {
            e.preventDefault();
            $("#sync-hashtag i").addClass("turn");
            var hashtag = $("#input-hashtag").val();
            $.post( "{!! route("Content.instagram.albumSyncHashtag") !!}", {
                "hashtag": hashtag,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-hashtag i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });

        $("#sync-user").click(function (e) {
            e.preventDefault();
            $("#sync-user i").addClass("turn");
            var user = $("#input-user").val();
            $.post( "{!! route("Content.instagram.albumSyncUser") !!}", {
                "user": user,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-user i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });
    </script>
@endpush
