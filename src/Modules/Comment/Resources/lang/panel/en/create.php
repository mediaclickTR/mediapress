<?php

return [
    'save' => 'Save',

    'labels' => [
        'name' => 'Panel Name',
        'key' => 'Key',
        'status' => 'Status',
        'show_time' => 'Comment Display Time',
        'condition' => 'Post Comment Condition'
    ],

    'status' => [
        'active' => 'Active',
        'passive' => 'Passive'
    ],

    'show_time' => [
        'after' => 'Show Now',
        'confirmed' => 'After Confirmation',
    ],

    'condition' => [
        'all' => 'Anyone Available',
        'login' => 'Available After Login'
    ]
];
