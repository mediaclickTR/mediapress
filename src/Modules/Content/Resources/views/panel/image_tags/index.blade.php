@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">Görseller</div>
            </div>
            <div class="float-right">
                <div class="float-left" style="margin-right:10px;">
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importModal">
                        <i class="fa fa-upload"></i>
                        {!! trans("ContentPanel::seo.excel.import") !!}
                    </button>
                </div>
                <div class="float-right">
                    <a href="{!! route('Content.image_tags.excelExport') !!}" class="btn btn-primary btn-sm">
                        <i class="fa fa-file-excel"></i>
                        {!! trans("ContentPanel::seo.excel.export") !!}
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="jsgrid-grid-header jsgrid-header-scrollbar" id="tempTable" style="display: none">
                <table class="jsgrid-table">
                    <tbody>
                        <tr class="jsgrid-header-row">
                            <th class="jsgrid-header-cell" style="width: 30px;">#</th>
                            <th class="jsgrid-header-cell" style="width: 200px;">{!! trans('ContentPanel::image_tags.url') !!}</th>
                            <th class="jsgrid-header-cell" style="width: 75px;">{!! trans('ContentPanel::image_tags.image') !!}</th>
                            <th class="jsgrid-header-cell" style="width: 100px;">{!! trans('ContentPanel::image_tags.title') !!}</th>
                            <th class="jsgrid-header-cell" style="width: 100px;">{!! trans('ContentPanel::image_tags.alt_tag') !!}</th>
                            <th class="jsgrid-header-cell jsgrid-align-center" style="width: 50px;">{!! trans('ContentPanel::image_tags.detail') !!}</th>
                            <th class="jsgrid-header-cell jsgrid-control-field jsgrid-align-center"
                                style="width: 50px;"></th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="jsGrid">
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="imageTagDetailModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="importModal" aria-labelledby="importModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Excel File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{!! route("Content.image_tags.excelImport") !!}" method="post"
                              enctype="multipart/form-data" id="importForm">
                            @csrf
                            <div class="form-group mt-3">
                                <input type="file" class="form-control-file" name="file">
                            </div>
                            <div class="p-2 float-right">
                                <button type="submit" name="type" value="excel" class="btn btn-primary">Import</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script type="text/javascript"
            src="{!! asset('vendor/mediapress/js/plugins/jsgrid/jsgrid.min.js') !!}"></script>
    <script type="text/javascript"
            src="{!! asset('vendor/mediapress/js/plugins/jsgrid/i18n/tr.js') !!}"></script>
    <script>

        @if(\App::getLocale() != 'en')
        jsGrid.locale("tr");
        @endif

        $(function () {


            $("#jsGrid").jsGrid({
                width: "100%",

                filtering: true,
                inserting: false,
                editing: true,
                sorting: true,
                paging: true,
                data: "{}",
                pageButtonCount: 5,
                autoload: true,
                pageLoading: true,
                pageSize: 20,
                pageIndex: 1,

                fields: [
                    {
                        name: "id",
                        title: "#",
                        type: "text",
                        width: 30,
                        editing: false,
                        filtering: false,
                        sorting: false
                    },
                    {
                        name: "url",
                        title: "{!! trans('ContentPanel::image_tags.url') !!}",
                        type: "text",
                        width: 200,
                        editing: false,
                        sorting: false
                    },
                    {
                        name: "image",
                        title: "{!! trans('ContentPanel::image_tags.image') !!}",
                        type: "text",
                        width: 75,
                        editing: false,
                        filtering: false,
                        sorting: false
                    },
                    {
                        name: "title",
                        title: "{!! trans('ContentPanel::image_tags.title') !!}",
                        type: "text",
                        width: 100,
                        editing: true,
                        sorting: false,
                        itemTemplate: function (value, item) {
                            if (item.title) {
                                return value;
                            } else {
                                return "<div style='color:gray'>" + item.default_text + "</div>"
                            }
                        }
                    },
                    {
                        name: "alt_tag",
                        title: "{!! trans('ContentPanel::image_tags.alt_tag') !!}",
                        type: "text",
                        width: 100,
                        editing: true,
                        sorting: false,
                        itemTemplate: function (value, item) {
                            if (item.alt_tag) {
                                return value;
                            } else {
                                return "<div style='color:gray'>" + item.default_text + "</div>"
                            }
                        }
                    },
                    {
                        name: "metas",
                        title: "{!! trans('ContentPanel::image_tags.detail') !!}",
                        width: 50,
                        align: "center",
                        editing: false,
                        filtering: false,
                        inserting: false,
                        sorting: false,
                        itemTemplate: function (value, item) {
                            var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                            var data_ = {};
                            data_['id'] = item.id;
                            var $customButton = $("<a href='javascript:void(0);' class='modalBtn' onclick=\"getImageTagDetails(" + item.id + ",'" + item.url + "');\"><i class='fa fa-external-link-alt modal-trigger'></i></a>");
                            return $result.add($customButton);
                        },
                    },
                    {type: "control", editButton: true, modeSwitchButton: false, deleteButton: false}
                ],
                onItemUpdated: function (args) {

                    var item = args.item;
                    var data_ = {};

                    data_['_token'] = '{!! csrf_token() !!}';
                    data_['item'] = item;
                    return $.ajax({
                        type: "POST",
                        url: "{!! route("Content.image_tags.updateImageTag") !!}",
                        data: data_,
                        async: false,
                        dataType: "json",
                        success: function () {
                            $("#jsGrid").jsGrid('loadData');
                        }
                    });
                },
                rowClick: function (args) {
                    var $target = $(args.event.target);

                    if ($target.closest("a.modalBtn").length) {
                        return;
                    }

                    if (this.editing) {
                        this.editItem($target.closest("tr"));
                    }
                },
                controller: {
                    loadData: function (filter) {
                        const deferred = $.Deferred();
                        var data_ = {};
                        data_["filter"] = filter;
                        data_['_token'] = '{!! csrf_token() !!}';
                        $.ajax({
                            type: "POST",
                            url: "{!! route("Content.image_tags.getImageTags") !!}",
                            data: data_,
                            dataType: "json",
                            success: function (response) {
                                deferred.resolve({
                                    data: response.list,
                                    itemsCount: response.count
                                });
                            },
                        });
                        return deferred.promise();
                    },

                }
            });
        });

        function getImageTagDetails(id, url) {
            $.ajax({
                type: "GET",
                url: "{!! route("Content.image_tags.getImageTagDetails") !!}",
                data: {id: id, 'url': url},
                success: function (response) {
                    $('#imageTagDetailModal .modal-content').html(response);
                    $('#imageTagDetailModal').modal('show');
                },
            });
        }

        $('body').delegate('#imageTagDetailFormBtn', 'click', function () {
            var data = $('#imageTagDetailForm').serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('Content.image_tags.saveImageTagDetails') }}",
                data: data,
                success: function (response) {
                    $('#imageTagDetailModal').modal('hide');
                    $("#jsGrid").jsGrid('loadData');
                },
            });
        })

        $(window).scroll(function () {
            if ($(window).scrollTop()  > $("#jsGrid").offset()['top'] - 72) {
                $("#tempTable").css('display','block');
            }else{
                $("#tempTable").css('display','none');
            }

        });
    </script>
@endpush


@push('styles')
    <style>
        td.jsgrid-cell.jsgrid-control-field.jsgrid-align-center input {
            float: left;
            margin: 0 !important;
        }

        .jsgrid-grid-header {
            background: none;
        }

        .jsgrid-table {
            margin: 0;
        }

        .jsgrid-filter-row > .jsgrid-cell {
            background: #fff !important;
        }

        .jsgrid-header-cell {
            font-size: 15px;
            white-space: nowrap;
            font-weight: 500;
            letter-spacing: -0.3px;
            overflow: hidden;
        }

        .modal-dialog {
            min-width: 60% !important;
        }

        tr td {
            text-align: center;
        }

        .jsgrid-pager-current-page {
            font-weight: 100 !important;
        }

        .jsgrid-header-row > .jsgrid-header-cell {
            background: #f3f4f9 !important;
        }

        .jsgrid-pager-container {
            margin-top: 10px !important;
        }

        .jsgrid-pager-current-page {
            font: 13px 'Poppins', sans-serif;
            color: #272727 !important;
            border: 1px solid #d2d2d2 !important;
            border-radius: 4px !important;
            padding: 4px 10px !important;
        }

        .jsgrid-pager-page a {
            font: 13px 'Poppins', sans-serif;
            color: #272727 !important;
        }

        .jsgrid-pager-page a:hover {
            color: #2196f3 !important;
        }

        .jsgrid-pager-page {
            padding: 4px 12px !important;
        }

        .jsgrid-pager-nav-button a {
            font: 13px Comfortaa-Regular;
            color: #272727 !important;
        }

        .jsgrid-pager {
            font-size: 13px;
        }

        .page-content table tbody tr td:last-child {
            text-align: center !important;
        }

        #tempTable {
            position: sticky;
            top: 75px;
            z-index: 9;
        }

        .jsgrid {
            overflow: inherit !important;
        }
    </style>
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/plugins/jsgrid/jsgrid.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/plugins/jsgrid/jsgrid-theme.min.css') !!}"/>
@endpush
