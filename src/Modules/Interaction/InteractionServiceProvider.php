<?php

namespace Mediapress\Modules\Interaction;


use Mediapress\Modules\Interaction\Facades\Interaction;
use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class InteractionServiceProvider extends ServiceProvider
{
    public const INTERACTION = "Interaction";
    public const DATABASE = 'database';
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = self::INTERACTION;
    protected $namespace = 'Mediapress\Modules\Interaction';

    public function boot()
    {
        Parent::boot();
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' , 'InteractionPanel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database/Interaction.sqlite' => storage_path('app'. DIRECTORY_SEPARATOR.  self::DATABASE . "/Interaction.sqlite")], $this->module_name.'Database');
        $this->publishActions(__DIR__);
        $files = $this->app['files']->files(__DIR__ . '/Config');

        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        if(config('mediapress.interaction.driver', 'sqlite') == 'sqlite') {
            $this->app['config']
                ->set("database.connections.mediapress_interaction", [
                    'driver' => config('mediapress.interaction.driver', 'sqlite'),
                    'database' => config('mediapress.interaction.database', storage_path('app/database/Interaction.sqlite')),
                    'prefix' => ''
                ]);
        } else {
            $this->app['config']
                ->set("database.connections.mediapress_interaction", [
                    'driver' => config('mediapress.interaction.driver'),
                    'database' => config('mediapress.interaction.database'),
                    'host' => env('DB_HOST', '127.0.0.1'),
                    'port' => env('DB_PORT', '3306'),
                    'username' => env('DB_USERNAME', 'forge'),
                    'password' => env('DB_PASSWORD', ''),
                    'prefix' => '',
                    'strict' => false
                ]);
        }

        $loader = AliasLoader::getInstance();
        $loader->alias(self::INTERACTION, Interaction::class);

        app()->bind(self::INTERACTION, function () {
            return new \Mediapress\Modules\Interaction\Interaction;
        });
    }


    protected function mergeConfig($path, $key)
    {

        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }
}
