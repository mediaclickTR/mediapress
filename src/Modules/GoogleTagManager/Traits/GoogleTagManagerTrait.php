<?php


namespace Mediapress\Modules\GoogleTagManager\Traits;

use Mediapress\Modules\MPCore\Models\SettingSun;
use Cache;

trait GoogleTagManagerTrait
{
    public function tagManagerInit()
    {
        $this->middleware(function ($request, $next) {
            $account_id = SettingSun::where("key", "google.tagmanager.account.id")->where("website_id", session('panel.website')->id)->first();
            $access_token = SettingSun::where("key", "google.tagmanager.access.token")->where("website_id", session('panel.website')->id)->first();
            if ($account_id && $access_token) {
                $this->account_id = $account_id->value;
                $this->access_token = $access_token->value;
                $expire_date = SettingSun::where("key", "google.tagmanager.expire.date")->where("website_id", session('panel.website')->id)->first();
                if (time() > $expire_date->value) {
                    $refresh_token = SettingSun::where("key", "google.tagmanager.refresh.token")->where("website_id", session('panel.website')->id)->first();

                    if (isset($refresh_token) && $refresh_token->value) {
                        $this->refreshOrRegisterAuth($refresh_token->value);
                    }
                }
            } else {
                $this->account_id = 0;
                $this->access_token = 0;
            }
            return $next($request);
        });
    }

    public function refreshOrRegisterAuth($refresh_token)
    {
        $data = refresh_token(config('google_analytics.client_id', "238284199418-701prjoophrmeimkm6ptp4uck6ecbv9a.apps.googleusercontent.com"), config('google_analytics.client_secret', "ne_BBDBvFEphQJO3i7Dm-uLZ"), $refresh_token);

        $content = (
        new \GuzzleHttp\Client())
            ->request('GET', 'https://www.googleapis.com/tagmanager/v1/accounts?access_token=' . $data->access_token)
            ->getBody()->getContents();

        $contents = json_decode($content, true);

        $expire_date = time() + $data->expires_in;
        $access_token = $data->access_token;

        SettingSun::updateOrCreate(['key' => "google.tagmanager.expire.date", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Refresh Token Yenileme Zamanı",
            "value" => $expire_date
        ]);

        SettingSun::updateOrCreate(['key' => "google.tagmanager.access.token", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Access Token",
            "value" => $access_token
        ]);

        SettingSun::updateOrCreate(['key' => "google.tagmanager.refresh.token", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Refresh Token",
            "value" => $refresh_token
        ]);

        return $contents;
    }

    public function connectTagManager($account_id)
    {

        $access_token = $this->access_token = settingSun('google.tagmanager.access.token');
        $refresh_token = settingSun('google.tagmanager.refresh.token');
        $expires_in = settingSun('google.tagmanager.expire.date');

        $expire_date = time() + $expires_in;

        $content = $this->getFromAPI('accounts/' . $account_id . '/containers');

        $public_id = $content['containers'][0]['publicId'] ?? null;

        SettingSun::updateOrCreate(['key' => "google.tagmanager.expire.date", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Refresh Token Yenileme Zamanı",
            "value" => $expire_date
        ]);

        SettingSun::updateOrCreate(['key' => "google.tagmanager.access.token", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Access Token",
            "value" => $access_token
        ]);

        SettingSun::updateOrCreate(['key' => "google.tagmanager.refresh.token", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Refresh Token",
            "value" => $refresh_token
        ]);

        SettingSun::updateOrCreate(['key' => "google.tagmanager.account.id", "website_id" => session("panel.website")->id], [
            "group" => "TagManager",
            "title" => "Google Account ID",
            "value" => $account_id
        ]);

        if (isset($public_id)) {
            SettingSun::updateOrCreate(['key' => "google.tagmanager.container.publicId", "website_id" => session("panel.website")->id], [
                "group" => "TagManager",
                "title" => "Google Container Public ID",
                "value" => $public_id
            ]);
        }


        Cache::flush();

        return redirect()->back();
    }

    public function refreshOrRegisterAuth2($refresh_token)
    {
        $data = refresh_token(config('google_analytics.client_id', "238284199418-701prjoophrmeimkm6ptp4uck6ecbv9a.apps.googleusercontent.com"), config('google_analytics.client_secret', "ne_BBDBvFEphQJO3i7Dm-uLZ"), $refresh_token);


        $content = (
        new \GuzzleHttp\Client())
            ->request('GET', 'https://www.googleapis.com/tagmanager/v1/accounts?access_token=' . $data->access_token)
            ->getBody()->getContents();

        $contents = json_decode($content, true);

        if ($contents) {
            foreach ($contents['accounts'] as $item) {

                if (isset($item['accountId'])) {
                    $content = (
                    new \GuzzleHttp\Client())
                        ->request('GET', 'https://www.googleapis.com/tagmanager/v1/accounts/' . $item['accountId'] . '/containers?access_token=' . $data->access_token)
                        ->getBody()->getContents();
                    $contents = json_decode($content, true);

                    $url = str_replace(['https://', 'http://', 'www.'], '', getHttpSchemaWithHost());

                    foreach ($contents['containers'] as $webProperty) {

                        if (isset($webProperty['name'])) {
                            if (strpos($webProperty['name'], $url) !== false) {
                                $account_id = $item['accountId'];
                                $public_id =  $webProperty['publicId'];
                            }
                        }

                    }
                }


            }
        }
        if (isset($account_id)) {

            $expire_date = time() + $data->expires_in;
            $access_token = $data->access_token;

            SettingSun::updateOrCreate(['key' => "google.tagmanager.expire.date", "website_id" => session("panel.website")->id], [
                "group" => "TagManager",
                "title" => "Google Refresh Token Yenileme Zamanı",
                "value" => $expire_date
            ]);

            SettingSun::updateOrCreate(['key' => "google.tagmanager.access.token", "website_id" => session("panel.website")->id], [
                "group" => "TagManager",
                "title" => "Google Access Token",
                "value" => $access_token
            ]);

            SettingSun::updateOrCreate(['key' => "google.tagmanager.refresh.token", "website_id" => session("panel.website")->id], [
                "group" => "TagManager",
                "title" => "Google Refresh Token",
                "value" => $refresh_token
            ]);

            SettingSun::updateOrCreate(['key' => "google.tagmanager.account.id", "website_id" => session("panel.website")->id], [
                "group" => "TagManager",
                "title" => "Google Account ID",
                "value" => $account_id
            ]);

            if (isset($public_id)) {
                SettingSun::updateOrCreate(['key' => "google.tagmanager.container.publicId", "website_id" => session("panel.website")->id], [
                    "group" => "TagManager",
                    "title" => "Google Container Public ID",
                    "value" => $public_id
                ]);
            }


            Cache::flush();

            return true;
        } else {
            return false;
        }
    }

    private function getContainerWithTags()
    {
        $content = $this->getFromAPI('accounts/' . $this->account_id . '/containers');

        foreach ($content['containers'] as $container){
            $contents = $container;
        }

        $data = ['container' => $contents];

        $this->containerId = $contents['containerId'];

        $tags = $this->getTags();

        if (isset($tags['tags'])) {
            $data['tags'] = $tags['tags'];
        }

        $triggers = $this->getTriggers();
        $data['triggers'] = [];
        if (isset($triggers['triggers'])) {

            foreach ($triggers['triggers'] as $trigger){
                $data['triggers'][$trigger['triggerId']] = $trigger;
            }
        }
        return $data;
    }


    private function getTags()
    {

        return $this->getFromAPI('accounts/' . $this->account_id . '/containers/' . $this->containerId . '/tags');

    }

    private function getTriggers()
    {
        return $this->getFromAPI('accounts/' . $this->account_id . '/containers/' . $this->containerId . '/triggers');
    }


    private function getFromAPI($string)
    {
        $content = (new \GuzzleHttp\Client())
            ->request('GET', 'https://www.googleapis.com/tagmanager/v1/' . $string . '?access_token=' . $this->access_token)
            ->getBody()->getContents();

        return json_decode($content, true);
    }

}
