<?php

use Illuminate\Support\Facades\Route;



Route::group(['middleware' => 'panel.auth'], function () {
    Route::group(['prefix' => 'Content', 'as' =>"Content."], function () {
        /*
        * Content modules
        * Include websites, sitemaps
        */

        Route::post('/checkUrl', ['as' => 'checkUrl', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@checkUrl']);
        Route::post('deleteDetail', ['as' =>'deleteDetail', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@deleteDetail']);
        Route::post('restoreDetail', ['as' =>'restoreDetail', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@restoreDetail']);
        Route::post('updateFieldFromFront', ['as' =>'updateFieldFromFront', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@updateFieldFromFront']);
        Route::post('updateMetaFromFront', ['as' =>'updateMetaFromFront', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@updateMetaFromFront']);
        Route::group(['prefix' => 'Formbuilder', 'as' => 'formbuilder.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'FormbuilderController@index']);
            Route::post('/store', ['as' => 'store', 'uses' => 'FormbuilderController@store']);
            Route::get('/store', ['as' => 'store', 'uses' => 'FormbuilderController@store']);
        });

        Route::group(['prefix' => 'Websites', 'as' => 'websites.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@edit']);
            Route::post('/update' , ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@delete']);
            Route::post('/removeLanguageVariation', ['as' => 'removeLanguageVariation', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@removeLanguageVariation']);

            Route::get('/{website}/variationsTemplates', ['as' => 'variation_templates', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@variation_templates']);
            Route::get('/{website}/detailTabsMarkup', ['as' => 'detail_tabs_markup', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@getDetailTabs']);

            Route::post('/websiteGroupLanguages', ['as' => 'websiteGroupLanguages', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@websiteGroupLanguages']);
            Route::post('/countryGroups', ['as' => 'countryGroups', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteController@countryGroups']);
            Route::group(['prefix' => "{id}/CountryGroups", 'as' => "country_groups."], function () {
                Route::get('/manage', ['as' => 'manage', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteCountryGroupsController@manage']);
                Route::get('/groupsLanguages', ['as' => 'groups_languages', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteCountryGroupsController@getCountryGroupsLanguages']);
                Route::get('/groupsCountriesAndLanguages', ['as' => 'groupsCountriesAndLanguages', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteCountryGroupsController@getGroupsCountriesAndLanguages']);
//                Route::get('/ownersGroups',['as' => 'ownersGroups', 'uses' => 'WebsiteCountryGroupsController@getOwnersCountryGroups']);
//                Route::get('/ungroupedCountries',['as' => 'ungroupedCountries', 'uses' => 'WebsiteCountryGroupsController@getUngroupedCountries']);
                Route::post('/save', ['as' => 'save', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteCountryGroupsController@saveCountryGroup']);
                Route::post('/remove', ['as' => 'remove', 'uses' => '\Mediapress\Modules\Content\Controllers\WebsiteCountryGroupsController@removeCountryGroup']);
            });
        });

        Route::group(['prefix' => 'Sitemaps', 'as' => 'sitemaps.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@index']);

            Route::post('/export-json', ['as' => "export_json", 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@exportJson']);
            Route::post('/import-json', ['as' => "import_json", 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@importJson']);

            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@create']);
            Route::get('popupStatic/{sitemap_id?}', ['as' => "popup_static", 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@popupStatic']);
            Route::post('popupStaticStore/{sitemap_id?}', ['as' => "popup_static_store", 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@popupStaticStore']);
            //Route::get('popupCreateStatic', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@popupCreateStaticSitemap']);
            //Route::get('/{sitemap}/popupUpdateStatic', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@popupCreateStaticSitemap']);

            Route::get('/{sitemap}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@edit']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@store']);

            Route::group(['prefix' => "{sitemap}/websites", 'as' => "websites."], function () {
                Route::get('/variationsTemplates', ['as' => 'variations', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@variations']);
            });

            Route::get('{sitemap}/content/{website}', ['as' => 'getContent', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@getContent']);

            Route::get('/{sitemap}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@delete']);

            Route::post('/checkUrl', ['as' => 'checkUrl', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@checkUrl']);
            Route::post('/checkSubKey', ['as' => 'checkSubKey', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@checkSubKey']);

            Route::get('/{sitemap}/mainEdit', ['as' => 'mainEdit', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@mainEdit']);
            Route::post('/{sitemap}/mainUpdate', ['as' => 'mainUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapController@mainUpdate']);
        });


        Route::group(['prefix' => 'Sliders', 'as' => 'slider.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@edit']);
            Route::post('/{id}/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\SliderController@delete']);

            Route::group(['prefix' => '{slider_id}/Scenes', 'as' => 'scenes.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@create']);
                Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@store']);
                Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@edit']);
                Route::post('/{id}/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@update']);
                Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\SceneController@delete']);
            });
        });



        Route::group(['prefix' => 'Popup', 'as' => 'popup.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@edit']);
            Route::post('/{id}/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@delete']);

            Route::get('/clone', ['as' => 'clone', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@clonePopup']);
            Route::get('/getLanguages', ['as' => 'getLanguages', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@getLanguages']);

            Route::get('search-url', ['as' => 'search_url', 'uses' => '\Mediapress\Modules\Content\Controllers\PopupController@searchUrl']);
        });

        Route::group(['prefix' => 'SocialMedia', 'as' => 'socialmedia.'], function () {

            Route::get('/ajax', ['as'  => 'ajax', 'uses' => '\Mediapress\Modules\Content\Controllers\SocialMediaController@ajax']);
            Route::get('/', ['as'  => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\SocialMediaController@index']);
            Route::get('/create', ['as'  => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SocialMediaController@create']);
            Route::post('/store', ['as'  => 'store', 'uses' =>  '\Mediapress\Modules\Content\Controllers\SocialMediaController@store']);
            Route::get('{id}/edit', ['as'  => 'edit', 'uses' =>  '\Mediapress\Modules\Content\Controllers\SocialMediaController@edit']);
            Route::post('/{id}/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\SocialMediaController@update']);
            Route::get('{id}/delete', ['as'  => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\SocialMediaController@delete']);
        });


        Route::get('/pageCreateSuccessPopupType', ['as' => 'pageCreateSuccessPopupType', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@pageCreateSuccessPopupType']);

        Route::group(['prefix' => 'Pages', 'as' => 'pages.'], function () {

            Route::get('/{sitemap_id}', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@index']);

            Route::get('/{sitemap_id}/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@create']);

            Route::get('{page}/variationStatus', ['as' => 'variationStatus', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@variationStatus']);

            Route::get('/{sitemap_id}/edit/{id}', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@edit']);

            Route::post('/{sitemap_id}/update/{id}', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@update']);

            Route::get('/{sitemap_id}/delete/{id}', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@delete']);


            Route::get('/{sitemap_id}/batchUpdate', ['as' => 'batchUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@batchUpdate']);
            Route::get('/{sitemap_id}/getPagesForBatchUpdate', ['as' => 'getPagesForBatchUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@getPagesForBatchUpdate']);
            Route::post('/{sitemap_id}/updatePageForBatchUpdate', ['as' => 'updatePageForBatchUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@updatePageForBatchUpdate']);


            Route::post('/checkUrl', ['as' => 'checkUrl', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@checkUrl']);

            // Ajax
            Route::get('/ajax/{website_id}/{sitemap_id?}', ['as' => 'ajax', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@ajax']);
            Route::post('/getPageCriteriaValues', ['as' => 'page_criteria_values', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@getPageCriteriaValues']);
            Route::post('/getPagePropertyValues', ['as' => 'page_property_values', 'uses' => '\Mediapress\Modules\Content\Controllers\PageController@getPagePropertyValues']);
        });

        Route::group(['prefix' => 'Categories', 'as' => 'categories.'], function () {

            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@index']);
            Route::get('/list/{type}', ['as' => 'getList', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@getList']);

            Route::get('/list/{sitemap_id}/{type}/getListItems', ['as' => 'getListItems', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@getListItems']);

            // Category
            Route::get('/categoryCreate/{sitemap_id}', ['as' => 'category.create', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@create']);
            Route::post('/categoryStore/{category}', ['as' => 'category.store', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@store']);
            Route::post('/categoryOrderSave/{sitemap_id}', ['as' => 'category.orderSave', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@orderSave']);
            Route::post('/categoryPropertySave/{sitemap_id}', ['as' => 'category.propertySave', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@propertySave']);
            Route::post('/categoryCriteriaSave/{sitemap_id}', ['as' => 'category.criteriaSave', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@criteriaSave']);
            Route::post('/categoryDelete', ['as' => 'category.delete', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@delete']);

            // Criteria

            Route::get('/Criteria/Create/{sitemap}', ['as' => 'criteria.create', 'uses' => '\Mediapress\Modules\Content\Controllers\CriteriaController@index']);
            Route::post('/Criteria/Store/{criteria}', ['as' => 'criteria.store', 'uses' => '\Mediapress\Modules\Content\Controllers\CriteriaController@store']);
            Route::post('/Criteria/Order', ['as' => 'criteria.orderSave', 'uses' => '\Mediapress\Modules\Content\Controllers\CriteriaController@orderSave']);
            Route::post('/Criteria/Delete', ['as' => 'criteria.delete', 'uses' => '\Mediapress\Modules\Content\Controllers\CriteriaController@delete']);

            // Property
            Route::get('/Property/Create/{sitemap}', ['as' => 'property.create', 'uses' => '\Mediapress\Modules\Content\Controllers\PropertyController@index']);
            Route::post('/Property/Store/{property}', ['as' => 'property.store', 'uses' => '\Mediapress\Modules\Content\Controllers\PropertyController@store']);
            Route::post('/Property/Order', ['as' => 'property.orderSave', 'uses' => '\Mediapress\Modules\Content\Controllers\PropertyController@orderSave']);
            Route::post('/Property/Delete', ['as' => 'property.delete', 'uses' => '\Mediapress\Modules\Content\Controllers\PropertyController@delete']);

            // Ajax
            Route::get('/list/{sitemap_id}/{type}/ajax', ['as' => 'ajax', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@ajax']);
            Route::post('/getRelatedCriteriasOfCategories', ['as' => 'related_criterias', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@getRelatedCriteriasOfCategories']);
            Route::post('/getRelatedCriteriasOfCategoriesFull', ['as' => 'related_criterias_full', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@getRelatedCriteriasOfCategoriesFull']);
            Route::post('/getRelatedPropertiesOfCategories', ['as' => 'related_properties', 'uses' => '\Mediapress\Modules\Content\Controllers\CategoryController@getRelatedPropertiesOfCategories']);

        });

        Route::group(['prefix' => 'Ajax', 'as' => 'ajax.'], function () {
            Route::post('/popup/{process}', ['as' => 'popup', 'uses' => '\Mediapress\Modules\Content\Controllers\AjaxController@popup']);
        });


        Route::group(['prefix' => 'Menus', 'as' => 'menus.'], function () {

            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@delete']);

            Route::get('/{id}/selectLanguage', ['as' => 'selectLanguage', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@selectDetailLanguage']);
            Route::get('/{country_group_id}/{language_id}/{id}/details', ['as' => 'details', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@detailsCreate']);
            Route::post('/detailsStore', ['as' => 'details.store', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@detailsStore']);
            Route::post('/updateList', ['as' => 'updateList', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@updateList']);
            Route::get('/{menu_id}/edit-detail/{id}', ['as' => 'edit.details', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@detailsEdit']);
            Route::post('/{id}/update-detail', ['as' => 'update.details', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@detailsUpdate']);
            Route::get('/detailsDelete/{id}', ['as' => 'detailsDelete', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@detailsDelete']);

            Route::get('/copy/{id}', ['as' => 'copy', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@copy']);
            Route::post('/copy-ajax', ['as' => 'copyAjax', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@copyAjax']);
            Route::post('/copy', ['as' => 'copy', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuController@postCopy']);
        });

        Route::group(['prefix' => 'PanelMenus', 'as' => 'panelmenus.'], function () {

            Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@delete']);

            Route::get('/{id}/details', ['as' => 'details', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@details']);
            Route::post('/detailsPost', ['as' => 'detailsPost', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@detailsPost']);
            Route::post('/updateList', ['as' => 'updateList', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@updateList']);
            Route::get('/{id}/edit-detail', ['as' => 'edit.details', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@detailsEdit']);
            Route::post('/detailsUpdate', ['as' =>  'detailsUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@detailsUpdate']);
            Route::post('/detailsEditPost/{id}', ['as' => 'detailsEditPost', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@detailsEditPost']);
            Route::get('/detailsDelete/{id}', ['as' => 'detailsDelete', 'uses' => '\Mediapress\Modules\Content\Controllers\MenuPanelController@detailsDelete']);
        });

        Route::group(['prefix' => 'Seo', 'as' => 'seo.'], function () {
            Route::get('/Metas', ['as' => 'metas', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@listMetas']);
            Route::get('/Robot', ['as' => 'robot', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@robotIndex']);
            Route::post('/Robot', ['as' => 'robot', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@robotUpdate']);
            Route::post('/storeMeta', ['as' => 'storeMeta', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@storeMeta']);
            Route::post('/listUpdate', ['as' => 'listUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@listUpdate']);
            Route::post('/metasQuickUpdate', ['as' => 'metasQuickUpdate', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@metasQuickUpdate']);
            Route::post('/listFilter', ['as' => 'listFilter', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@listFilter']);
            Route::post('/listDelete', ['as' => 'listDelete', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@listDelete']);
            Route::post('/getUrlMetas', ['as' => 'getUrlMetas', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@getUrlMetas']);
            Route::post('/getExcelImport', ['as' => 'getExcelImport', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@getExcelImport']);
            Route::get('/excelExport', ['as' => 'excelExport', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@excelExport']);
            Route::post('/excelImport', ['as' => 'excelImport', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@excelImport']);
            Route::get('/ajax', ['as' => 'ajax', 'uses' => '\Mediapress\Modules\Content\Controllers\SeoController@ajax']);

            Route::group(['prefix' => 'SitemapXMLs', 'as' => 'sitemap_xmls.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@create']);
                Route::get('/{id}/edit', ['as' => 'edit', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@edit']);
                Route::post('/store', ['as' => 'store', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@store']);
                Route::post('/{id}/delete', ['as' => 'delete', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@delete']);
                Route::post('/{id}/enable', ['as' => 'enable', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@enable']);
                Route::post('/{id}/disable', ['as' => 'disable', 'uses' => '\Mediapress\Modules\Content\Controllers\SitemapXmlsController@disable']);
            });
        });



        Route::group(['prefix' => 'Meta', 'as' => 'meta.'], function () {
            Route::post('/getMetaVariablesFor', ['as' => 'variables', 'uses' => '\Mediapress\Modules\Content\Controllers\ContentController@getMetaVariablesFor']);
        });

        Route::group(['prefix' => 'Instagram', 'as' => 'instagram.'], function () {
            Route::get('/', "InstagramController@index")->name("index");
            Route::get('/sync', "InstagramController@sync")->name("sync");
            Route::get('/album', "InstagramController@album")->name("album");
            Route::get('/allposts', "InstagramController@allPosts")->name("allposts");
            Route::get('/edit-album/{id}', "InstagramController@editAlbum")->name("edit-album");
            Route::post('syncHashtag', "InstagramController@syncWithHashtag")->name("syncHashtag");
            Route::post('syncUser', "InstagramController@syncWithUser")->name("syncUser");
            Route::post('albumSyncHashtag', "InstagramController@albumSyncHashtag")->name("albumSyncHashtag");
            Route::post('albumSyncUser', "InstagramController@albumSyncUser")->name("albumSyncUser");
            Route::post('savePost', "InstagramController@savePost")->name("savePost");
            Route::post('saveAlbum', "InstagramController@saveAlbum")->name("saveAlbum");
            Route::post('ajaxEditAlbum', "InstagramController@ajaxEditAlbum")->name("ajaxEditAlbum");
            Route::post('deletePrioritiy', "InstagramController@deletePrioritiy")->name("deletePrioritiy");
            Route::post('deleteCache', "InstagramController@deleteCache")->name("deleteCache");
            Route::post('deleteAlbum', "InstagramController@deleteAlbum")->name("deleteAlbum");
        });

        Route::group(['prefix' => 'NewInstagram', 'as' => 'newinstagram.'], function () {
            Route::get('/', "NewInstagramController@index")->name("index");
            Route::get('/login', "NewInstagramController@login")->name("login");
            Route::get('/auth', "NewInstagramController@auth")->name("auth");
            Route::get('/deauth', "NewInstagramController@deauth")->name("deauth");
            Route::get('/user-timeline', "NewInstagramController@userTimelineRoute")->name("user-timeline");
        });


        Route::group(['prefix' => 'ExportPages', 'as' => 'export_pages.'], function () {
            Route::get('/selectLanguage', "ExportPagesController@selectLanguage")->name("selectLanguage");
            Route::get('/', "ExportPagesController@index")->name("index");
            Route::post('/export', "ExportPagesController@export")->name("export");
        });

        Route::group(['prefix' => 'ImportPages', 'as' => 'import_pages.'], function () {
            Route::get('/selectLanguage', "ImportPagesController@selectLanguage")->name("selectLanguage");
            Route::get('/', "ImportPagesController@index")->name("index");
            Route::post('/import', "ImportPagesController@import")->name("import");
        });


        Route::group(['prefix' => 'MetaTemplates', 'as' => 'meta_templates.'], function () {
            Route::get('/', "MetaTemplateController@index")->name("index");
            Route::get('/{sitemap_id}/edit', "MetaTemplateController@edit")->name("edit");
            Route::post('/update', "MetaTemplateController@update")->name("update");
        });

        Route::group(['prefix' => 'ImageTags', 'as' => 'image_tags.'], function () {
            Route::get('/', "ImageController@index")->name("index");
            Route::post('/getImageTags', "ImageController@getImageTags")->name("getImageTags");
            Route::post('/updateImageTag', "ImageController@updateImageTag")->name("updateImageTag");
            Route::get('/getImageTagDetails', "ImageController@getImageTagDetails")->name("getImageTagDetails");
            Route::post('/saveImageTagDetails', "ImageController@saveImageTagDetails")->name("saveImageTagDetails");

            Route::get('/excelExport', "ImageController@excelExport")->name("excelExport");
            Route::post('/excelImport', "ImageController@excelImport")->name("excelImport");
        });

    });
});
