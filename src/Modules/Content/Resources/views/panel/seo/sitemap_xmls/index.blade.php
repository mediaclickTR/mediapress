@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::sitemap_xmls.sitemap_xml_files") !!}</div>
            </div>
            <div class="float-right">
                @if(count($sitemapxmls)>0)
                    @if(!empty($queries))
                        <a href="{!! route("Content.seo.sitemap_xmls.index") !!}" class="filter-block">
                            <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                        </a>
                    @endif
                @endif
                    <a href="{!! route('Content.seo.sitemap_xmls.create') !!}" class="btn btn-primary btn-sm"> <i
                            class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
        </div>

       <div class="p-30">
           <table>
               <thead>
               <tr>
                   <th>{!! trans("MPCorePanel::general.type") !!}</th>
                   <th>{!! trans("MPCorePanel::general.status") !!}</th>
                   <th>{!! trans("MPCorePanel::general.title") !!}</th>
                   <th>{!! trans("MPCorePanel::general.filename") !!}</th>
                   {{--<th>{!! trans("ContentPanel::sitemap_xmls.selected_sitemap_count") !!}</th>--}}
                   <th>{!! trans("MPCorePanel::general.actions") !!}</th>
               </tr>
               </thead>
               @foreach($sitemapxmls as $key=>$sitemapxml)
                   <tr>
                       <td>{!! ($sitemapxml->type=="default") ? 'Genel' : 'Özel' !!}</td>
                       <td>{!! ($sitemapxml->status==1) ? '<i class="status_circle active"></i>' : '<i class="status_circle passive"></i>' !!}</td>
                       <td>{!! $sitemapxml->title !!}</td>
                       <td>{!! $sitemapxml->filename !!}<small class="text-muted">.xml</small> </td>
                       {{--<td>{!! dump($sitemapxml->sitemaps) ?? "-" !!} </td>--}}
                       <td>
                           <a href="{!! route('Content.seo.sitemap_xmls.edit',$sitemapxml->id) !!}" class="m-1" title="{{trans("MPCorePanel::general.edit")}}">
                               <span class="fa fa-pen"></span>
                           </a>
                           @if($sitemapxml->type != "default")
                               <a href="{!! route('Content.seo.sitemap_xmls.delete',$sitemapxml->id ) !!}"
                                  class="m-1 needs-confirmation"
                                  title="  {{trans("MPCorePanel::general.delete")}}"
                                  data-dialog-text="{{trans("MPCorePanel::general.action_cannot_undone")}}"
                                  data-dialog-cancellable="1"
                                  data-dialog-type="warning"><span
                                       class="fa fa-trash"></span></a>
                           @endif
                       </td>
                   </tr>
               @endforeach
           </table>
           <div class="float-left">
               {!! $sitemapxmls->links() !!}
           </div>
           <div class="float-right">
               <select onchange="locationOnChange(this.value);" class="fs-14">
                   <option value="#">{!! trans('MPCorePanel::general.select.list.size') !!}</option>
                   <option
                       {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("Content.seo.sitemap_xmls.index", array_merge($queries, ["list_size"=>"15"])) !!}">
                       15
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("Content.seo.sitemap_xmls.index", array_merge($queries, ["list_size"=>"25"])) !!}">
                       25
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("Content.seo.sitemap_xmls.index", array_merge($queries, ["list_size"=>"50"])) !!}">
                       50
                   </option>
                   <option
                       {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("Content.seo.sitemap_xmls.index", array_merge($queries, ["list_size"=>"100"])) !!}">
                       100
                   </option>
               </select>
           </div>
       </div>

    </div>
@endsection
@push("styles")
    <style>
        td,th{
            text-align:center;
        }
    </style>
@endpush
@push("scripts")
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
    </script>
@endpush
