<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramPosts extends Model
{
    protected $table = "instagram_posts";
    protected $guarded = ["created_at", "updated_at", "deleted_at"];

    public function albums()
    {
        return $this->belongsToMany("App\Album", "instagram_post_album", "post_id", "album_id");
    }
}
