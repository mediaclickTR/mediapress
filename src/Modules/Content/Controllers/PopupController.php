<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\Auth;
use Mediapress\Http\Controllers\PanelController as Controller;

use Mediapress\Modules\Content\Models\Popup;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\PopupCustomUrl;
use Mediapress\Modules\Content\Models\PopupLanguage;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Foundation\Validation\ValidatesRequests;

class PopupController extends Controller
{
    use ValidatesRequests;

    public const REQUIRED = 'required';
    public const ACCESSDENIED = 'accessdenied';
    public const CONTENT_POPUP_INDEX = 'Content.popup.index';
    private $rules = [
        'order' => self::REQUIRED,
        'status' => self::REQUIRED,
        'devices' => self::REQUIRED,
        'languages' => self::REQUIRED,
        'display_date_type' => self::REQUIRED,
        'start_date' => 'required_if:display_date_type, 2|nullable',
        'finish_date' => 'required_if:display_date_type, 2|nullable',
        'display_time_type' => self::REQUIRED,
        'display_time' => 'required_if:display_time_type, 1|nullable',
        'type' => self::REQUIRED,
        'name' => self::REQUIRED,
        'content' => self::REQUIRED,
        'impressions' => self::REQUIRED,
        'impression_count' => 'required_if:impressions, 1|required_if:impressions, 2|nullable',
        'show_page' => self::REQUIRED,
        'url_ids' => 'required_if:show_page, 1|nullable',
        'conditions' => self::REQUIRED,
        'condition_time' => 'required_if:conditions, 0|nullable',
        'condition_pixel' => 'required_if:conditions, 1|nullable',
    ];

    private $labels = [
        'website_id' => 'Web Sitesi',
        'order' => 'Sıra',
        'status' => 'Durum',
        'devices' => 'Cihazlar',
        'display_date_type' => 'Ne zaman',
        'start_date' => 'Başlangıç Tarihi',
        'finish_date' => 'Bitiş Tarihi',
        'display_time_type' => 'Kaç Saniye',
        'display_time' => 'Saniye',
        'type' => 'Etkileşim Türü',
        'name' => 'Popup Adı',
        'content' => 'Popup İçeriği',
        'impressions' => 'Kaç Kez Görecek',
        'impression_count' => 'Kaç kez',
        'show_page' => 'Gösterilecek Sayfalar',
        'url_ids' => 'Url\'ler',
        'conditions' => 'Şartlar',
        'condition_time' => 'Süre',
        'condition_pixel' => 'Pixel',
    ];

    public function index()
    {

        $websiteId = session("panel.website.id");
        if (!activeUserCan([
            "content.websites.website$websiteId.popups.index",
            "content.websites.popups.index"
        ])) {
            return rejectResponse();
        }

        $popups = Popup::where('website_id', $websiteId)->orderBy('order')->get();
        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");

        $country_groups = CountryGroup::where('owner_id', $this->getWebsiteSession()->id)
            ->get();

        return view('ContentPanel::popups.index', compact('popups', 'websites', 'country_groups'));
    }


    public function create()
    {

        $websiteId = request()->get('website_id') ?? session("panel.website.id");
        if (!activeUserCan([
            "content.websites.website$websiteId.popups.create",
            "content.websites.popups.create"
        ])) {
            return rejectResponse();
        }

        $website = session()->get('panel.website');
        $countryGroups = $website->countryGroups()->with('languages')->get();


        $fonts = [];

        if (file_exists(public_path('assets/fonts'))) {
            $fonts = array_diff(scandir(public_path('assets/fonts')), array('.', '..'));

            $fonts = array_filter($fonts, function ($item) {
                return strstr($item, '.ttf') && !strstr($item, 'slick') && !strstr($item, 'fontawesome');
            });
        }


        return view('ContentPanel::popups.create', compact('countryGroups', 'fonts'));
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $data['admin_id'] = Auth::guard('admin')->user()->id;
        $data['website_id'] = $websiteId = session()->get('panel.website')->id;
        if (!activeUserCan([
            "content.websites.website$websiteId.popups.create",
            "content.websites.popups.create"
        ])) {
            return rejectResponse();
        }
        $this->validate($request, $this->rules, [], $this->labels);

        if (isset($data['start_date']) && isset($data['finish_date'])) {
            $data['start_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $data['start_date']))->format('Y-m-d');
            $data['finish_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $data['finish_date']))->format('Y-m-d');
        }


        $urls = isset($data['url_ids']) && $data['show_page'] == 1 ? $data['url_ids'] : [];
        $customUrls = isset($data['custom_url_ids']) && $data['show_page'] == 3 ? $data['custom_url_ids'] : [];


        $languages = $data['languages'];
        unset($data['_token'], $data['languages'], $data['url_ids'], $data['custom_url_ids']);

        $popup = Popup::firstOrCreate($data);

        foreach ($languages as $lang) {
            $hold = explode('-', $lang);
            PopupLanguage::firstOrCreate([
                'popup_id' => $popup->id,
                'country_group_id' => array_shift($hold),
                'language_id' => array_shift($hold)
            ]);
        }

        if (count($urls) > 0) {
            $popup->urls()->sync($urls);
        }
        PopupCustomUrl::where('popup_id', $popup->id)->delete();
        if (count($customUrls) > 0) {
            foreach ($customUrls as $customUrl) {
                PopupCustomUrl::firstOrCreate([
                    'popup_id' => $popup->id,
                    'url' => $customUrl
                ]);
            }
        }

        \Cache::flush();

        return redirect(route(self::CONTENT_POPUP_INDEX));
    }

    public function edit($id)
    {
        $website = session()->get('panel.website');

        $websiteId = request()->get('website_id') ?? session("panel.website.id");
        if (!activeUserCan([
            "content.websites.website$websiteId.popups.update",
            "content.websites.popups.update"
        ])) {
            return rejectResponse();
        }
        $countryGroups = $website->countryGroups()->with('languages')->get();


        $fonts = [];
        if (file_exists(public_path('assets/fonts'))) {
            $fonts = array_diff(scandir(public_path('assets/fonts')), array('.', '..'));

            $fonts = array_filter($fonts, function ($item) {
                return strstr($item, '.ttf') && !strstr($item, 'slick') && !strstr($item, 'fontawesome');
            });
        }

        $popup = Popup::find($id);

        $selectedUrls = Url::whereIn('type', ['original', 'category'])
                ->whereIn('id', $popup->urls->pluck('id')->toArray())
                ->where('website_id', $website->id)
                ->whereHasMorph('model', '*')
                ->with([
                    'model' => function ($query) {
                        $query->select('id', 'name');
                    }
                ])
                ->get(['id', 'model_id', 'model_type']);

        $customUrls = $popup->customUrls;

        return view('ContentPanel::popups.edit', compact('countryGroups', 'popup', 'fonts', 'selectedUrls', 'customUrls'));
    }

    public function update($id, Request $request)
    {

        $this->validate($request, $this->rules, [], $this->labels);
        $data = $request->all();

        if (isset($data['start_date']) && isset($data['finish_date'])) {
            $data['start_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $data['start_date']))->format('Y-m-d');
            $data['finish_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $data['finish_date']))->format('Y-m-d');
        }

        $languages = $data['languages'];
        $urls = isset($data['url_ids']) && $data['show_page'] == 1 ? $data['url_ids'] : [];
        $customUrls = isset($data['custom_url_ids']) && $data['show_page'] == 3 ? $data['custom_url_ids'] : [];

        unset($data['_token'], $data['languages'], $data['url_ids'], $data['custom_url_ids']);

        // TODO: $popup = Popup::find($id) bu satır, diğer tüm eylemlerden önceye alınır.
        // Yük getiren koda girilmeden önce birincil basit kontroller ve atamalar yapılır.
        // Hele önyüzden erişilen yerlerde bu tarz yazımlar DoS saldırılarına izin verir.

        $popup = Popup::find($id);

        if (!activeUserCan([
            "content.websites.website" . $popup->website_id . ".popups.update",
            "content.websites.popups.update"
        ])) {
            return rejectResponse();
        }

        foreach ($data as $key => $value) {
            if (mb_substr($key, 0, 1) != '_') {
                $popup->{$key} = $value;
            }
        }

        $popup->save();

        PopupLanguage::where('popup_id', $popup->id)->delete();
        foreach ($languages as $lang) {
            $hold = explode('-', $lang);
            PopupLanguage::firstOrCreate([
                'popup_id' => $popup->id,
                'country_group_id' => array_shift($hold),
                'language_id' => array_shift($hold)
            ]);
        }

        if (count($urls) > 0) {
            $popup->urls()->sync($urls);
        }

        PopupCustomUrl::where('popup_id', $popup->id)->delete();
        if (count($customUrls) > 0) {
            foreach ($customUrls as $customUrl) {
                PopupCustomUrl::firstOrCreate([
                   'popup_id' => $popup->id,
                   'url' => $customUrl
                ]);
            }
        }

        \Cache::flush();
        return redirect(route(self::CONTENT_POPUP_INDEX));
    }

    public function delete($id)
    {

        $popup = Popup::find($id);

        if (!activeUserCan([
            "content.websites.website" . $popup->website_id . ".popups.delete",
            "content.websites.popups.delete"
        ])) {
            return rejectResponse();
        }

        PopupLanguage::where('popup_id', $popup->id)->delete();
        $popup->delete();
        \Cache::flush();
        return redirect(route(self::CONTENT_POPUP_INDEX));
    }

    public function searchUrl()
    {
        $key = request()->get('search');
        $langs = request()->get('langs');

        $countryGroupArray = [];
        $languageArray = [];

        if($langs) {
            foreach ($langs as $lang) {
                $temp = explode('-', $lang);

                $countryGroupArray[] = $temp[0];
                $languageArray[] = $temp[1];
            }

            $countryGroupArray = array_unique($countryGroupArray);
            $languageArray = array_unique($languageArray);
        }

        $website = session()->get('panel.website');

        $urls = Url::where('type', 'original')
            ->where('website_id', $website->id)
            ->whereHasMorph('model', '*', function ($q) use($countryGroupArray, $languageArray) {
                $q->whereIn('country_group_id', $countryGroupArray)
                    ->whereIn('language_id', $languageArray);
            })
            ->with([
                'model' => function ($query) {
                    $query->select('id', 'name');
                }
            ]);

        if ($key) {
            $urls = $urls->where('url', 'like', '%' . $key . '%');
        }
        $urls = $urls->get(['id', 'model_id', 'model_type'])
            ->take(20);


        $return = array();
        foreach ($urls as $url) {
            $return[] = [
                'text' => $url->model->name,
                'value' => $url->id
            ];
        }

        return $return;
    }

    public function getLanguages()
    {
        $data = request()->all();

        $popup = Popup::find($data['popup_id']);

        $languages = CountryGroup::where('owner_id', $this->getWebsiteSession()->id)
            ->find($data['country_group_id'])->languages
            ->pluck('name', 'id');

        return $languages;
    }

    public function clonePopup()
    {

        $data = request()->except('_token');

        $popup = Popup::find($data['popup_id']);

        if($popup) {
            $newPopup = $popup->replicate();
            $newPopup->admin_id = session('panel.user.id');
            $newPopup->save();

            PopupLanguage::firstOrCreate([
                'popup_id' => $newPopup->id,
                'country_group_id' => $data['country_group_id'],
                'language_id' => $data['language_id']
            ]);
        }
        \Cache::flush();
        return redirect()->back();
    }

    public function setPopupImpressions()
    {
        $popupId = request()->get('popupId');
        $popup = Popup::find($popupId);
        popupImpressions($popup);
    }
}
