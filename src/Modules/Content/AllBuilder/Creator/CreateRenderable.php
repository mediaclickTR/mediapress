<?php

namespace Mediapress\Modules\Content\AllBuilder\Creator;


use Exception;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Exceptions\PageRenderableNotFoundException;
use Mediapress\Modules\Content\Exceptions\SitemapRenderableNotFoundException;
use Mediapress\Modules\Content\Exceptions\CategoryRenderableNotFoundException;
use Mediapress\Modules\Content\Exceptions\CriteriaRenderableNotFoundException;
use Mediapress\Modules\Content\Exceptions\PropertyRenderableNotFoundException;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use \ReflectionClass;

class CreateRenderable
{

    public const APP = '\App\\';
    public $sitemap;

    public function __construct(Sitemap $sitemap)
    {
        $this->sitemap = $sitemap;
    }

    public function createSitemapRenderable()
    {
        try {
            //if ($this->local()) {
                \Mediapress\Modules\Content\Facades\Content::getSitemapRenderableClassPath($this->sitemap);
            //}
        } catch (SitemapRenderableNotFoundException $e) {
            if ($e instanceof SitemapRenderableNotFoundException) {
                $path = $e->path;
            }

            if (isset($path[0])) {
                $path = $path[0];
            }

            if (strstr($path, self::APP)) {
                $app_path = app_path(str_replace([self::APP, '\\'], ['', DIRECTORY_SEPARATOR], $path) . '.php');
                if (!file_exists($app_path)) {
                    $this->createFileOfType("Sitemap",$app_path, $path);
                }
            }

        }

        return 1;
    }


    public function createPagesRenderable()
    {
        try {
            if ($this->local()) {
                \Mediapress\Modules\Content\Facades\Content::getPageRenderableClassPath(null,false,$this->sitemap->sitemapType->name);
            }
        } catch (Exception $e) {
            if ($e instanceof PageRenderableNotFoundException) {
                $path = $e->path;
                if (is_array($path)) {
                    $path = $path[0];
                }

                if (strstr($path, self::APP)) {
                    $app_path = app_path(str_replace([self::APP, '\\'], ['', DIRECTORY_SEPARATOR], $path) . '.php');
                    if (!file_exists($app_path)) {
                        $this->createFileOfType("Pages",$app_path, $path);
                    }
                }
            }

        }

        return 1;
    }

    public function createCategoriesRenderable()
    {

        try {
            if ($this->local()) {
                \Mediapress\Modules\Content\Facades\Content::getCategoryRenderableClassPath(null,false,$this->sitemap->sitemapType->name);
            }
        } catch (Exception $e) {

            if ($e instanceof CategoryRenderableNotFoundException) {
                $path = $e->path;
                if (is_array($path)) {
                    $path = $path[0];
                }

                if (strstr($path, self::APP)) {
                    $app_path = app_path(str_replace([self::APP, '\\'], ['', DIRECTORY_SEPARATOR], $path) . '.php');
                    if (!file_exists($app_path)) {
                        $this->createFileOfType("Categories",$app_path, $path);
                    }
                }
            }

        }

        return 1;
    }

    public function createCriteriasRenderable()
    {
        try {
            if ($this->local()) {
                \Mediapress\Modules\Content\Facades\Content::getCriteriaRenderableClassPath(null,false,$this->sitemap->sitemapType->name);
            }
        } catch (Exception $e) {
            if ($e instanceof CriteriaRenderableNotFoundException) {
                $path = $e->path;
                if (is_array($path)) {
                    $path = $path[0];
                }

                if (strstr($path, self::APP)) {
                    $app_path = app_path(str_replace([self::APP, '\\'], ['', DIRECTORY_SEPARATOR], $path) . '.php');
                    if (!file_exists($app_path)) {
                        $this->createFileOfType("Criterias",$app_path, $path);
                    }
                }
            }

        }

        return 1;
    }

    public function createPropertiesRenderable()
    {
        try {
            if ($this->local()) {
                \Mediapress\Modules\Content\Facades\Content::getPropertyRenderableClassPath(null,false,$this->sitemap->sitemapType->name);
            }
        } catch (Exception $e) {
            if ($e instanceof PropertyRenderableNotFoundException) {
                $path = $e->path;
                if (is_array($path)) {
                    $path = $path[0];
                }

                if (strstr($path, self::APP)) {
                    $app_path = app_path(str_replace([self::APP, '\\'], ['', DIRECTORY_SEPARATOR], $path) . '.php');
                    if (!file_exists($app_path)) {
                        $this->createFileOfType("Properties",$app_path, $path);
                    }
                }
            }

        }

        return 1;
    }

    private function local()
    {
        return config('app.env') == 'local';
    }


    private function createFileOfType(string $type,string $app_path, $path)
    {
        $this->createFile($type, $app_path, $path);

    }

    private function createFile(string $type, string $app_path, $path)
    {

        $data = file_get_contents(base_path('vendor/mediapress/mediapress/src/Modules/Content/AllBuilder/Creator/' . $type . '.renderable'));
        $classname = substr($path, strrpos($path, '\\') + 1);
        $namespace = ltrim(mb_substr($path, 0, -1 * (strlen($classname) + 1)), '\\');
        $renderable = str_replace(['{%namespace%}', '{%class%}'], [$namespace, $classname], $data);

        $fileName = substr($app_path, strrpos($app_path, DIRECTORY_SEPARATOR) + 1);
        $folder = mb_substr($app_path, 0, -1 * (strlen($fileName) + 1));

        if (!file_exists($folder)) {
            $oldmask = umask(0);
            mkdir($folder, 0777,true);
            umask($oldmask);
        }
        file_put_contents($app_path, $renderable);
    }
}
