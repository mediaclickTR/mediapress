<?php

namespace Mediapress\Foundation\ThirdParty;

use Illuminate\Support\Facades\Cache;

class Youtube
{

    public $key = null;
    public $title;
    public $image;
    public $embed;

    public function __construct($url_or_id)
    {

        //TODO youtube id urlden ayrılacak

        $this->key = trim($url_or_id);
        $this->getParams();
    }

    private function getParams()
    {
        $array = Cache::remember('Youtube'.$this->key,60*60, function(){
            $content = file_get_contents('https://youtube.com/get_video_info?video_id=' . $this->key);
            parse_str($content, $ytarr);
            $playerResponse = json_decode($ytarr['player_response'], 1);
            $array = [];
            if (isset($playerResponse['videoDetails'])) {

                foreach ($playerResponse['videoDetails'] as $key => $value) {
                    if ($key == 'thumbnail') {
                        $array['image'] = end($value['thumbnails'])['url'];
                    } else {
                        $array[$key]= $value;
                    }
                }
                $array['embed']= 'https://www.youtube.com/embed/'. $array['videoId'];
            }
            return $array;
        });

        foreach ($array as $key=>$value){
            $this->{$key} = $value;
        }
    }

    public function __get($name)
    {
        if(isset($this->$name)){
            return $this->$name;
        }
        return null;

    }
}
