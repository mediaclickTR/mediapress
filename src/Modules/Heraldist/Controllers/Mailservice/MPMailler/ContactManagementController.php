<?php
namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\MPMailler;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mediapress\Facades\Slots\EntitySlot;
use Mediapress\Facades\Slots\MessagingSlot;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Entity\Models\Entities;
use Mediapress\Modules\Heraldist\Models\EmailSender;
use Mediapress\Modules\Heraldist\Models\MailTemplate;


class ContactManagementController extends Controller
{
    public const NAME = "name";
    public const TOTAL = "total";
    public const SOURCE_URL = "source_url";
    public const SUB = "sub";
    public const ENTITYLISTS = "entitylists";
    public const FORMS = "forms";
    public const EMAIL = "email";

    public function index()
    {
        $users = EntitySlot::getUsers();
        $entities = EntitySlot::getEntities();
        $entitylists = EntitySlot::getEntitylists();
        $forms = MessagingSlot::getForms();
        $ebulletins = MessagingSlot::getEbulletins();

        $data = [
            "users"=>[
                self::NAME =>"Üyeler",
                self::TOTAL =>$users->count(),
                self::SOURCE_URL =>route("Entity.users.index"),
                self::SUB =>[]
            ],
            "entities"=>[
                self::NAME =>"Etkileşim Hedefleri",
                self::TOTAL =>$entities->count(),
                self::SOURCE_URL =>route("Entity.entities.index"),
                self::SUB =>[]
            ],
            self::ENTITYLISTS =>[
                self::NAME =>"Üye Listeleri",
                self::TOTAL =>$entitylists->count(),
                self::SOURCE_URL =>"",
                self::SUB =>[]
             ],
            self::FORMS =>[
                self::NAME =>"Formlar",
                self::SOURCE_URL =>"",
                self::TOTAL =>$forms->count(),
                self::SUB =>[]
            ],
            "ebulletins"=>[
                self::NAME =>"Bültenler",
                self::SOURCE_URL => route("Heraldist.ebulletin.index"),
                self::TOTAL =>$ebulletins->count(),
                self::SUB =>[]
            ]
        ];

        foreach ($entitylists as $entitylist) {
            $data[self::ENTITYLISTS][self::SUB][] = [
                        "id"=>$entitylist->id,
                        self::NAME => $entitylist->name,
                        self::TOTAL => $entitylist->records->count(),
                        self::SOURCE_URL => route('Entity.entitylist.listdetail.index',$entitylist->id)];
        }

        foreach ($forms as $form) {
            $data[self::FORMS][self::SUB][] = [
                        "id"=>$form->id,
                        self::NAME => $form->name,
                        self::TOTAL => $form->messages->count(),
                        self::SOURCE_URL =>  route("Heraldist.message.show", $form->id)];
        }

        return view("HeraldistPanel::mail_service.mpmailler.contact_management.index", compact('data'));
    }

    public function send(Request $request)
    {
        $type = $request->type;
        $template = $request->template;
        $subject = $request->subject;
        $detail = $request->detail;

        $selected = unserialize($request->selected);

        $data = [];

        foreach ($selected as $select){
            $parse = explode("-", $select);
            $this->determineDataSource($parse[0], $parse[1], $data);
        }
        // tekrar eden verileri temizle
        $data = array_map("unserialize", array_unique(array_map("serialize", $data)));
        if ($type == self::EMAIL){
            $this->sendEmail($data,$template,$subject,$detail);
        }elseif($type == "sms"){
            $this->sendSMS($data,$subject,$detail);
        }
    }

    public function sendEmail($data,$template,$subject,$detail)
    {
        $fromEmail = config('mediapress.default_email_address');
        $mailtemplate = MailTemplate::find($template);
        if ($mailtemplate){
            foreach ($data as $value)
            {
                $search  = ['%username%','%mail_subject%', '%mail_detail%'];
                $replace = [$value[self::NAME], $subject, $detail];
                $mail_body = str_replace($search,$replace,$mailtemplate->body);
                $mail_body = ['mail_body' => $mail_body];
                MessagingSlot::mailSender($mail_body,$value[self::EMAIL],$subject,$fromEmail);
            }
            $send_data = ['maillist_id' => 1, 'mailtemplate_id' => $mailtemplate->id];
            $insert = EmailSender::insertGetId($send_data);
            if($insert){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function sendSMS($data,$subject,$detail)
    {
        // TODO :: SMS yapılacak
    }

    public function import(Request $request)
    {
        $this->validate($request,
            ['excel' => 'required'],
            ['excel.required' => trans("MPCorePanel::validation.filled", ['filled', trans("ContentPanel::seo.excel.select")])],
            ['excel' => trans("ContentPanel::seo.excel.select")]
        );

        $file = Excel::load($request->excel, function ($reader)
        {
            $results = $reader->get()->toArray();
            foreach ($results as $result)
            {
                $str = 'email';
                if (isset($result['name']) && isset($result[$str]))
                {
                    $isHas = Entities::where($str,$result[$str])->first();
                    $str1 = 'phone';
                    if (!$isHas){
                        Entities::create([
                            'name'=> $result['name'],
                            $str =>  $result[$str],
                            $str1 =>  (isset($result[$str1])) ? $result[$str1] : null,
                        ]);
                    }else{
                        Entities::where($str, $result[$str])->update([
                            'name'=> $result['name'],
                            $str1 => (isset($result[$str1])) ? $result[$str1] : null,
                        ]);
                    }
                }
                else{
                    return false;
                }
            }
        });
        if ($file){
            return redirect(route('Heraldist.mpmailler.contact_management.index'))->with('message', trans('HeraldistPanel::contact_management.success.import'));
        }
    }

    public function export(Request $request)
    {
        $selected = $request->selected;
        $data = [];
        foreach ($selected as $select){
            $parse = explode("-", $select);
            $this->determineDataSource($parse[0], $parse[1], $data);
        }
        // tekrar eden verileri temizle
        $data = array_map("unserialize", array_unique(array_map("serialize", $data)));
        Excel::create("İletişim Yönetimi ". time(), function ($excel) use ($data) {
            $excel->sheet('Lister', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export('xls');
    }

    public function determineDataSource($table, $id,&$data)
    {
        $str = "phone";
        switch ($table){
            case "users":
                $users = EntitySlot::getUsers();
                foreach ($users as $user){
                    $data[] = [self::NAME =>$user->name, self::EMAIL =>$user->email, $str =>$user->phone];
                }
            break;
            case "entities":
                $entities = EntitySlot::getEntities();
                foreach ($entities as $entity){
                    $data[] = [self::NAME =>$entity->name, self::EMAIL =>$entity->email, $str =>$entity->phone];
                }
                break;
            case "ebulletins":
                $ebulletins = MessagingSlot::getEbulletins();
                foreach ($ebulletins as $ebulletin){
                    $data[] = [self::NAME =>$ebulletin->name, self::EMAIL =>$ebulletin->email, $str =>$ebulletin->phone];
                }
                break;
            case self::ENTITYLISTS:
                $entitylist = EntitySlot::getEntitylist($id);
                foreach ($entitylist->records as $record){
                    if ($record->model){
                        $data[] = [self::NAME =>$record->model->name, self::EMAIL =>$record->model->email, $str =>$record->model->phone];
                    }
                }
                break;
            case self::FORMS:
                $form = MessagingSlot::getForm($id);
                foreach ($form->messages as $message){
                    $data[] = [self::NAME =>$message->name, self::EMAIL =>$message->email, $str =>$message->phone];
                }
                break;
                break;
            default:
                break;
        }
        return $data;
    }
}
