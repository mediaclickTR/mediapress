<?php

return [
        'status' => 'Status',
        'upload' => 'Upload',
        'save' => 'Save',
        'new_add' => 'Add New',
        'add' => 'Add',
        'edit' => 'Edit',
'show' => 'View',
'change' => 'Change',
'remove' => 'Remove',
'delete' => 'Delete',
'preview' => 'Preview' ,
'widget' => 'Plugins',
    'back' => 'Back',
    'yes' => 'Yes',
    'no' => 'No',
    'cancel' => 'Cancel',
    'transactions' => 'Transactions',
    'actions' => 'Actions',
    'all_website_records' => 'All Website Records',
    'clear_filters' => 'Clear Filters',
    'selection' => 'Select from',
    'list_selection' => "Select from list",
    'general' => 'General',
    'create-title' => 'Add name',
    'edit-title' => 'Edit name',
    'selected-delete' => 'Delete Selected',

    'next' => 'Next',
    'previous' => 'Previous',

    'id' => 'ID',
    'name' => 'Name',
    'title' => 'Title',
    'type' => 'Type',
    'image' => 'Image',
    'name_entry' => 'Enter name',

    'order' => 'Order',
    'order_entry' => 'Enter order',

    'users' => 'Members',

    'slug' => 'Slug',
    'slug_entry' => 'Enter Slug',


    'detail' => 'Detail',
    'detail_entry' => 'Enter detail',

    'note' => 'Note',
    'note_entry' => 'Enter note',

    'key' => 'Key',
    'key_entry' => 'Enter key',


    'created_at' => 'Record Date',
    'updated_at' => 'Updated Date',
    'deleted_at' => 'Deleted Date',

    'reads' => 'Read',
    'read' => 'Read',
    'unread' => 'Unread',
    'subject' => 'Subject',
    'email' => 'Email',
    'nodata' => 'No saved data found.',
    'select_service' => 'Select Service',
    'select_processing' => 'Select Action',

    'mpmailler' => 'MPMailler',
    'mailchimp' => 'Mailchimp',
    'eligible' => 'Selectable',
    'selected' => 'Selected',
];
