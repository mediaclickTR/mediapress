@php
    $breadcrumb = $mediapress->breadcrumb();
@endphp
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach($breadcrumb as $bread)
            <li class="breadcrumb-item">
                <a href="{!! $bread['url'] !!}">{!! $bread['name'] !!}</a>
            </li>
        @endforeach
    </ol>
</nav>
