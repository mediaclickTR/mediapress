<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Facades\Slots\CoreSlot;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\MPModule;
use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
//use Mediapress\Foundation\CustomBladeCompiler;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Auth\Models\Role;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Facades\DataSourceEngine;
use View;

class PlaygroundController extends Controller
{
    //public $bladeTests;
    public const ITEMS = "items";

    public function index()
    {

        $ignored_list = [
            "index",
            "runAction",
            "__construct",
            "callAction",
            "getWebsiteSession",
            "setWebsiteSession",
            "getPanelLanguagesSession",
            "setPanelLanguagesSession",
            "getActiveLanguageSession",
            "setPanelActiveLanguageSession",
            "getUserSession",
            "setUserSession",
            "middleware",
            "getMiddleware",
            "__call",
            "authorize",
            "authorizeForUser",
            "parseAbilityAndArguments",
            "normalizeGuessedAbilityName",
            "authorizeResource",
            "resourceAbilityMap",
            "resourceMethodsWithoutModels",
            "dispatch",
            "dispatchNow",
            "validateWith",
            "validate",
            "extractInputFromRules",
            "validateWithBag",
            "getValidationFactory",
        ];

        $all_methods = get_class_methods($this);

        $urls = [];

        $play_with = array_diff($all_methods, $ignored_list);
        $tests = [];
        foreach ($play_with as $pw) {
            $tests[] = [
                "action" => $pw,
                "route" => url(route("MPCore.playground.runner", ["action" => $pw]))
            ];
        }
        return view("MPCorePanel::playground.index", compact("tests"));
    }

    public function test_country_province_index()
    {
        return view("MPCorePanel::playground.test");

    }

    public function testDistinct($ids){
        $website_ids = Ebulletin::select("website_id")->whereIn('id',$ids)->distinct("website_id")->get()->pluck("website_id")->toArray();
        return dd($website_ids);
    }

    public function testGetSingleField($params){
        $superMCAdminRoleId = Role::where("name","SuperMCAdmin")->select("id")->first();
        $superMCAdminRoleId = $superMCAdminRoleId ? $superMCAdminRoleId->id : 0;
        return dd($superMCAdminRoleId);
    }

    public function test_country_province(Request $request)   {


        return   DataSourceEngine::getDataSource('MPCore', 'ProvincesDS')->setParams($request->country_id)->getData();

    }

    public function bouncerTrys($params){
        $roles = Role::where("name","<>", "SuperMCAdmin")->select("name")->get()->pluck("name")->toArray();
        echo Admin::whereIs($roles[1])->orWhere(function($query)use($roles){return $query->whereIs($roles[0]);})->toSql();
    }


    public function runAction($actionname, $params = "")
    {
        if (is_callable([$this, $actionname])) {
            $params = preg_split("/\//", $params);
            return $this->$actionname($params);
        } else {
            return "İşlev bulunamadı: $actionname";
        }
    }

    public function permissions()
    {
        $modules = ModulesEngine::getMPModules();
        foreach ($modules as $key => $module) {
            $ns = $module . "\\" . $key;
            dump(new $ns);
            dump((new $ns)->getActions());
        }
        return dd(CoreSlot::getMPModuleNames());
    }

    public function renderables()
    {
        $root_class = new BuilderRenderable();
        $renderables = $root_class->options["renderable_types"];

        $first_count = count($renderables);
        $filtered_count = count(array_filter($renderables));
        dump("İlk sayım: $first_count   -   Filtre sonrası sayım: $filtered_count");


        foreach ($renderables as $renderable) {
            dump($renderable);
            $rn = $renderable;


            /** @var BuilderRenderable $renderable */
            $renderable = new $renderable;
            $info = $renderable->getInfo();
            if (isset($info[self::ITEMS]["options"][self::ITEMS]["html"][self::ITEMS]["attributes"][self::ITEMS]["tag"])) {
                echo "$rn nesnesinde yanlış yerde tag var \n <br/>";
            }


        }

        return dd($root_class);
    }

    public function metasDeneme($params)
    {
        $sd = SitemapDetail::find(6);
        $sd->url->metas->deneme2->value = 10;
        return dd($sd->url->metas->toArray());
        //$sd->save();
        return dd(1);// = "olmayan url meta deneme";
        $sd->slug = "denemeeeee";
        //$a = $sd->url->metas()->get();
        $sd->save();
        /*$sd->url->metas->deneme="deneme";
        $sd->save();*/
        return dd($sd->url->metas);
    }

    public function metakvc($params)
    {
        $pdid = $params[0];
        $pd = PageDetail::find($pdid);
        $pd->url->metas->render();
        return dd($pd->url->metas->getListForDevice());
    }

    public function urlWorksPageDetail($params)
    {
        list($page_detail_id, $new_slug) = $params;
        dump($page_detail_id, $new_slug);
        $pd = PageDetail::find($page_detail_id);
        $pd->slug = $new_slug;
        $pd->save();
    }

    public function urlEngineUpdate($params)
    {

        list($page_detail_id, $new_slug, $type) = $params;
        $pd = PageDetail::find($page_detail_id);
        $website = $pd->parent->sitemap->website;

        $pd->slug = $new_slug;

        $pd->save();

        //URLEngine::update($website,$pd->url->url, )

        //$check_existing = URLEngine::checkUrl($website,"/en/services-1/gs5avzvag6z2lycr"); //

        //$check_new = URLEngine::checkUrl($website,$new_slug/*,$pd,$type*/);
        //dump($check_existing);

        //return dd($check_existing,$check_new);

    }
    /*
        public function bladeTests($params){
            //$a = View::composers("agsdhdh");
            //return View::make('<div></div>');
            //return dd($params);
            /*$first_name = 'Joe';
            $dbString = '<p>Welcome {{ $first_name }},</p>';

            return CustomBladeCompiler::render($dbString, ['first_name' => $first_name]);*/
    /*}*/

    public function urlEngineTest($params)
    {
        $a = $params;
        $pd = PageDetail::find($params[0] * 1);
        $a = URLEngine::generateUrlString($pd, 1, 760, "turkish-pagedetail-tr-yeni");
        $b = URLEngine::checkPattern(1, $a["pattern"], $a["slug"]);
        return dd($a, $b);
    }

    public function urlEngineTest2($params)
    {
        $a = $params;
        //$pd = SitemapDetail::find($params[0]*1);
        //$a=URLEngine::generateUrlString($pd,2,616,"deneme");
        $a = URLEngine::getSitemapUrlBase(17, 1, 760);
        dump($a);
    }

    public function check_url_test($params)
    {
        $website_id = 1;
        $detail_model_cls = 'Mediapress\Modules\Content\Models\PageDetail';
        $country_group_model = CountryGroup::find(1);
        $language_id = 760;
        $slug = "Türkişh";

        $detail_model = $detail_model_cls::find(63);

        $pattern = URLEngine::generateUrlString($detail_model, $country_group_model, $language_id, $slug);
        $result = URLEngine::checkPattern($website_id, $pattern["pattern"], $pattern["slug"]);
        return dd($pattern, $result);
    }

    public function classExists($params)
    {
        return dd(class_exists('App\Modules\Content\AllBuilder\Renderables\Dynamicforcloud\Criterias'));
    }

    public function equateWebsiteDetailsAndVariations($params)
    {
        (new \Mediapress\Modules\Content\Content())->equateWebsiteDetailsAndVariations($params[0]);
    }

    public function restorePageDetail($params)
    {
        //$pdid = $params[0];

        foreach ($params as $pdid) {
            $model = PageDetail::withTrashed()->find($pdid);
            if ($model) {
                $model->restore();
                /*$sitemap_id = $model->parent()->withTrashed()->first()->sitemap_id;

                $website_id = SitemapWebsite::where("sitemap_id",$sitemap_id)->first()->website_id;
                dump(URLEngine::checkUrl($website_id, $model->url, $model));*/
            }

        }

        return "-------";

    }

    public function getPageDetailPattern($params)
    {
        $pd = PageDetail::find($params[0]);
        return dd(URLEngine::getPatternForPageDetail($pd, $pd->countryGroup, $pd->language, $pd->slug));
    }

    public function getPattern($params){
        $sitemap_id = isset($params[0]) && $params[0] ? $params[0] : 2;
        $pattern = URLEngine::getPattern(1,760, CountryGroup::find(1)->code);
        return dd($pattern);
    }

    public function getSimpleRelationData()
    {
        //return dd($params);
        return \getSimpleRelationData(PageDetail::class, "Detail");
    }

    public function fixUrls($params)
    {
        $page_id = $params[0];

        $page = Page::where('id', $page_id)->with(['details', 'details.url'])->first();
        if (!$page) {
            return dd('Page Not Found');
        }

        return dd($page->details->pluck('url.url'));


    }

    public function mediapress($params)
    {
        mediapress()->init();
        mediapress("sitemap_detail", SitemapDetail::find(5));
        $mediapress = mediapress();
        dump($mediapress);
    }

    public function variations($params)
    {
        $website = Website::find($params[0]);
        return dd(VariationTemplates::variations($website));
    }

    public function criteriasOfCategory($params)
    {
        $ds1 = DataSourceEngine::getDataSource("Content", "CriteriasOfCategories", ["category_ids" => [98]])->getData();
        dump($ds1);
        /*$ds2 = DataSourceEngine::getDataSource("Content","CategoriesWithParentTree", ["category_ids"=>[98]])->getData();
        dump($ds2);*/
    }

    public function UrlMetaWorkerWithoutUrl($params)
    {
        //$metaworker = new UrlMetaWorker();
        dump(mediapress()->metaworker);
    }
    /*

    public function testMakeSurePathExists($params){
            $fe = new FilesEngine();
            $path = implode("/",$params);
            //return dd($params);
            $test = $fe->makeSurePathExists("local",$path);
            dump($test);
            dump($test["data"]["MFolder"]->getPath());
    }

    public function AllBuilderGetBuilderClassPath($params){

        try{
            list($renderable, $module, $scope, $section, $altpri) = $params;

        }catch (\Exception $e){
            dump($e->getMessage());
        }finally{
            dump("Örnek parametre str: /Pagess/Content/App/SearchPage/a");
        }
        $usage = '$renderable, $module (Content, MPCore ...), $scope (App, Mediapress), $section (Folder), $altpri (alternating priority)';
        echo "<h1>Params order:</h1>";
        echo $usage."\n<hr/>";
        echo "<h1>Called:</h1>";
        echo "<code><pre>".highlight_string("<?php AllBuilderEngine::getBuilderClassPath([\"$renderable\", \"Page_1\"], \"Content\", \"App\", \"$section\") ?>")."</pre></code>";

        $do = AllBuilderEngine::getBuilderClassPath([$renderable, "Page_1"], "Content", "App", $section);
        echo ("Bulundu mu? ". ($do[0] ? "Evet.":"Hayır."));
        return dd($do[1]);
    }
    public function AllBuilderGetStorerClassPath($params){
        try{
            list($storer, $module, $scope, $section, $altpri) = $params;

        }catch (\Exception $e){
            dump($e->getMessage());
        }finally{
            dump("Örnek parametre str: /PagesStorer/Content/App/Section/a");
        }
        $usage = '$storer, $module (Content, MPCore ...), $scope (App, Mediapress), $section (Folder), $altpri (alternating priority)';
        echo "<h1>Params order:</h1>";
        echo $usage."\n<hr/>";
        echo "<h1>Called:</h1>";
        echo "<code><pre>".highlight_string("<?php AllBuilderEngine::getStorerClassPath([$storer"."_11,\"$storer\"], \"Content\", [\"App\",\"Mediapress\"], null) ?>")."</pre></code>";

        $do = AllBuilderEngine::getStorerClassPath([$storer."_11", $storer], "Content", ["App","Mediapress"], null);
        echo ("Bulundu mu? ". ($do[0] ? "Evet.":"Hayır."));
        return dd($do[1]);
    }

    public function ContentGetPageStorer($params){
        $page_specific = "Page_11";
        $sitemap_specific = "SearchPage";

        $storer_class = AllBuilderEngine::getStorerClassPath([$page_specific."Deneme", "PagesDeneme"], "Content", "App", $sitemap_specific);
        $tried=$storer_class[1];
        if (!$storer_class[0]) {
            $storer_class_2 = AllBuilderEngine::getStorerClassPath(["PagesStorerDeneme"], "Content", ["App","Mediapress"], null);
            $tried = array_merge($storer_class[1], $storer_class_2[1]);
        }

        return dd($tried);
    }

    public function ContentGetPageRenderable($params){
        $page_specific = "Page_11Deneme";
        $sitemap_specific = "SearchPage";

        $renderable_class = AllBuilderEngine::getBuilderClassPath(array_filter([$page_specific, "PagesDeneme"]), "Content", "App", $sitemap_specific);
        $tried = $renderable_class[1];
        return dd($tried);
    }*/

    /*public function ContentGetSitemapDetailsStorer($params){
        $sitemap_specific = "SearchPage";
        $storer_class = AllBuilderEngine::getStorerClassPath(["SitemapDetails"], "Content", "App", $sitemap_specific);
        $tried=$storer_class[1];
        if (!$storer_class[0]) {
            $storer_class_2 = AllBuilderEngine::getStorerClassPath(["SitemapDetailsStorer"], "Content", ["App", "Mediapress"]);
            $tried = array_merge($storer_class[1], $storer_class_2[1]);
        }

        return dd($tried);
    }
    public function ContentGetSitemapDetailsRenderable($params){
        $sitemap_specific = "SearchPage";

        $renderable_class = AllBuilderEngine::getBuilderClassPath(["SitemapDetails"], "Content", "App", $sitemap_specific);
        return dd($renderable_class[1]);
    }

    public function elq($params){
        $x = Page::where("id",917);
        return dd($x->getRelation("detail"));
    }


    public function MFile($params){
        $mfile_id = $params[0];
        $mfile = MFile::find($mfile_id);
        dump($mfile->getUrl());
        dump($mfile->path());
        return dd($mfile);
    }



    public function MFileGeneral($params){
        $mfile_id = $params[0];
        $page_id = $params[1];

        $page = Page::find($page_id);

        $mfile = MFile::find($mfile_id);
        dump($page->mfiles()->toSql());
        $page->syncMFiles=["cover"=>$mfile_id];
        return dd($page);
    }*/
    /*
        public function slugUrlTest($params){
            $pd = PageDetail::find(41);

            $pd->slug = "";
            $pd->save();
            return dd($pd->getDirty());
        }

        public function websiteModel($params){
            $website_id = $params[0];
            $website = Website::find($website_id);
            dump($website);
            dump($website->variations()->get(["country_group_id","language_id"])->toArray());
            $do = Content::equateWebsiteDetailsAndVariations($website_id);
             return dd($do);
        }

        public function metaTrials($params){
            //$sitemap_id = $params[0];
            //return dd(Content::getStandardMetaVariablesOfSitemap($sitemap_id));

            return Content::getMetaVariablesFor(PageDetail::find(2070));
        }

        public function metaTemplates($params){
            $sitemap = Sitemap::where("id",$params[0])->with("details")->first();
            return dd($sitemap->detail->meta_templates);
        }

        public function sitemapXmlsSitemaps($params){
            $sitemap_xml = SitemapXml::where("id",$params[0])->with("sitemaps")->first();
            return dd($sitemap_xml);
        }*/


}
