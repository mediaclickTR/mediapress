<?php

return [
    "index" => [
        "title" => 'Sliders',
        "scene_status" => 'Scene Statuses',
        "scene_count" => 'Number of Scenes',
        "added_by" => 'Added By',
        "scenes" => 'Scenes'
    ],

    "create" => [
        'title' => 'Add Slider',
        'select_website' => 'Choose website',
        'name' => 'Slider Name',
        'type' => [
            'label' => 'Slider Type',
            'image' => 'Image',
            'video' => 'Video'
        ],
        'screen' => 'Slider screen',
        'width' => 'Width',
        'height' => 'Height',
        'text' => 'Number of Text Fields',
        'button' => 'Number of Buttons',
    ],

    "edit" => [
        "title" => 'Edit Slider',
        "go_scenes" => 'Show Scenes',
    ],
];
