<?php

return [
    "edit" => "Edit Role",
    "slug" => "Role Slug",
    "title" => "Role Title",
    "save_and_edit_perm" => "Save and Edit Permissions",
    "edit_perm" => "Edit Permissions",
    "add_new" => "Add New",
    'ability' => [
        'allow' => 'Grant all permissions',
        'no_permission' => 'Don\'t Even Let',
        'special' => 'Special'
    ],
    "header" => [
        "Listele" => "List",
        "Oluştur" => "Create",
        "Düzenle" => "Edit",
        "Sil" => "Delete",
        "Görüntüle" => "Show",
        "Okunmadı Olarak İşaretle" => "Mark As Unread",
        "Excel'e Aktar" => "Export to Excel",
        "Excele Aktar" => "Import to Excel",
        "Ekle" => "Add",
        "Güncelle" => "Update",
        "Formu Güncelle" => "Update Form",
        "Sırala" => "Sort",
    ],
    "app" => [
        "sub_module" => "Sub Module",
        "sub_modules" => "Sub Modules",
        "module" => "Module",
        "variation" => "Variation",
        "recorded_items" => "Saved Items"
    ],
    "titles" => [
        "index" => "Roles",
        "Ortak Yetkiler" => "Common Powers",
        "Sayfa Yapısı" => "Page Structure",
        "Sitemap Detayı" => "Page Structure Details",
        "Sayfa" => "Page",
        "Kategori" => "Category",
        "Kriter" => "Criteria",
        "Özellik" => "Property",
        "Mesajlar" => "Messages",
        "SEO" => "SEO",
        "Rol" => "Role",
        "Menüler" => "Menus",
    ],
    "authorization" => [
        'optionAllow' => "Allow",
        'optionDeny' => "Deny",
        'optionUseDefault' => "Default",
    ],
    "loading" => "Loading",
];
