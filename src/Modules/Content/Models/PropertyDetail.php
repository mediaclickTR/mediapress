<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\HasMFiles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Traits\IsDetail;

class PropertyDetail extends BaseModel
{
    use HasMFiles;
    use SoftDeletes;
    use IsDetail;

    protected $table = 'property_details';
    protected $dates = ['deleted_at'];

    public $timestamps = false;

    protected $fillable = [
        'language_id',
        'property_id',
        'country_group_id',
        'name',
        'slug',
        'detail',
        'status'
    ];


    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function pages()
    {
        return $this->belongsToMany(PageDetail::class)->withPivot("value");
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function parent(){
        return $this->belongsTo(Property::class);
    }

    public function getParentIdAttribute(){
        return $this->property_id;
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'model')->withTrashed()->withDefault(['url'=>'/'])->whereIn('type',['original','reserved'])->orderBy('type','asc');
    }
}
