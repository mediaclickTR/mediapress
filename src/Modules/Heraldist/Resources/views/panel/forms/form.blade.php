@if(!isset($form))
    {{ Form::open(['route' => 'Heraldist.forms.store']) }}
@else
    {{ Form::model($form, ['route' => ['Heraldist.forms.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$form->id) !!}
@endif
@csrf

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
@endpush

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="tit">{!! trans("HeraldistPanel::forms.select_websites") !!}</div>
            {!!Form::select('website_id[]', $websites,isset($form) ? $form->websites->pluck("id") : session()->get('panel.website')->id, ['class' => 'multiple languages','multiple'])!!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group rel">
            <div class="tit">{!! trans("HeraldistPanel::forms.send_user") !!}</div>
            <select name="send_user" class="w-25">
                <option value="1" {{ old('send_user', (isset($form) ? $form->send_user : 2)) == 1 ? 'selected' : '' }}>{!! trans("HeraldistPanel::forms.send_user_1") !!}</option>
                <option value="2" {{ old('send_user', (isset($form) ? $form->send_user : 2)) == 2 ? 'selected' : '' }}>{!! trans("HeraldistPanel::forms.send_user_2") !!}</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group rel">
            <div class="tit">{!! trans("MPCorePanel::general.name") !!}</div>
            {!!Form::text('name', null)!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel">
            <div class="tit">{!! trans("MPCorePanel::general.slug") !!}</div>
            {!!Form::text('slug', null)!!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group rel">
            <div class="tit">{!! trans("HeraldistPanel::forms.success_message") !!}</div>
            {!!Form::text('success', null)!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel">
            <div class="tit">{!! trans("HeraldistPanel::forms.error_message") !!}</div>
            {!!Form::text('error', null)!!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group rel focus">
            <div class="tit">{!! trans("HeraldistPanel::forms.buton_name") !!}</div>
            {!!Form::text('button', null)!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus" id="receiver">
            <div class="tit">{!! trans("HeraldistPanel::forms.receiver_email") !!}</div>
            {!!Form::text('receiver', null)!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group rel focus">
            <div class="tit">{!! trans("HeraldistPanel::forms.recaptcha_key") !!}</div>
            {!!Form::text('captcha_site_key', null)!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group rel focus" id="receiver">
            <div class="tit">{!! trans("HeraldistPanel::forms.recaptcha_secret") !!}</div>
            {!!Form::text('captcha_secret', null)!!}
        </div>
    </div>
</div>

<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! $submitButtonText !!}</button>
</div>


@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function() {
            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
        });
    </script>
@endpush
@push("styles")
    <style>
        .selectator_element{margin:  0 !important;}
    </style>
@endpush
