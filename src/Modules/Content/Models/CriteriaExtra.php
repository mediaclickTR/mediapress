<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class CriteriaExtra extends Model
{

    protected $table = 'criteria_extras';

    public $timestamps = false;

    protected $fillable = ["criteria_id", "key", "value"];

    public function criteria()
    {
        return $this->belongsTo(Criteria::class);
    }
}
