<?php

return [
    'save' => 'Kaydet',

    'labels' => [
        'name' => 'Panel Adı',
        'key' => 'Key',
        'status' => 'Durum',
        'show_time' => 'Yorumun Gösterilme Zamanı',
        'condition' => 'Yorum Gönderme Şartı'
    ],

    'status' => [
        'active' => 'Aktif',
        'passive' => 'Pasif'
    ],

    'show_time' => [
        'after' => 'Hemen Göster',
        'confirmed' => 'Onaylandıktan Sonra'
    ],

    'condition' => [
        'all' => 'Herkes Kullanılabilir',
        'login' => 'Login Olduktan Sonra kullanılabilir'
    ]
];
