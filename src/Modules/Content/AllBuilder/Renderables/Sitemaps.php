<?php

namespace Mediapress\Modules\Content\AllBuilder\Renderables;

use Mediapress\Facades\DataSourceEngine;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Modules\Content\Models\Criteria;

class Sitemaps extends BuilderRenderable
{

    public const OPTIONS = "options";
    public const CONTENTS = "contents";
    public const ATTRIBUTES = "attributes";
    public const TITLE = "title";
    public const VALUE = "value";
    public const INPUTWITHLABEL = "inputwithlabel";

    public function defaultContents()
    {
        extract($this->params);
        return
            [
                [
                    "type" => "form",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                "class" => "form-inline",
                                "method" => "POST",
                                "action" => url(route("Content.categories.criteria.store", $criteria->id))
                            ]
                        ],
                        "collectable_as" => ["form", "pagesform"]
                    ],
                    self::CONTENTS => [
                        [
                            "type" => self::INPUTWITHLABEL,
                            self::OPTIONS => [
                                self::TITLE => "Status",
                                "html" => [
                                    self::ATTRIBUTES => [
                                        "name" => "criteria-><print>criteria->id</print>->status",
                                        self::VALUE => "<print>criteria->status</print>"
                                    ],
                                ]
                            ]
                        ],
                        [
                            "type" => "detailtabs",
                            "params" => [
                                "details" => "<var>criteria->details</var>",
                            ],
                            self::CONTENTS => [
                                "tab" => [
                                    "type" => "tab",
                                    self::OPTIONS => [
                                        self::TITLE => "<print>detail->language->name</print>"
                                    ],
                                    self::CONTENTS => [
                                        [
                                            "type" => self::INPUTWITHLABEL,
                                            self::OPTIONS => [
                                                self::TITLE => "Başlık",
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        "name" => "detail->name",
                                                        self::VALUE => "<print>detail->name</print>"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            "type" => self::INPUTWITHLABEL,
                                            self::OPTIONS => [
                                                self::TITLE => "Slug",
                                                "html" => [
                                                    self::ATTRIBUTES => [
                                                        "name" => "detail->slug",
                                                        self::VALUE => "<print>detail->slug</print>"
                                                    ]
                                                ]
                                            ]
                                        ],
                                        /* [
                                             "type" => "inputwithlabel",
                                             "options" => [
                                                 "title" => "Image",
                                                 "html" => [
                                                     "attributes" => [
                                                         "name" => "detail->extras->image",
                                                         "value" => "<print>detail->extras->image</print>"
                                                     ]
                                                 ]
                                             ]
                                         ],*/
                                        [
                                            "type" => "clearfix"
                                        ]
                                    ]
                                ]

                            ]
                        ],
                        [
                            "type" => "div",
                            self::OPTIONS => [
                                "html" => [
                                    self::ATTRIBUTES => [
                                        "class" => "submit"
                                    ]
                                ]
                            ],
                            self::CONTENTS => [
                                [
                                    "type" => "button",
                                    self::OPTIONS => [
                                        "html" => [
                                            "tag" => "button",
                                            self::ATTRIBUTES => [
                                                "type" => "submit"
                                            ]
                                        ]
                                    ],
                                    self::CONTENTS => [
                                        "Kaydet"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
    }
}
