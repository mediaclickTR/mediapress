<?php

return [


    "auth" => [
        'name' => 'AuthPanel::auth.sections.auth',
        "name_affix" => "",
        'node_type' => "grouper",
        "item_model" => "",
        "item_id" => "*",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        "actions" => [],
        "subs" => [
            "admins" => [
                'name' => 'AuthPanel::auth.sections.admins',
                'item_model' => null,
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update"
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'settings'=>[
                    "auth_view_collapse" => "in"
                ]
            ],
            "roles" => [
                'name' => 'AuthPanel::auth.sections.roles',
                'item_model' => null,
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update"
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],

                "subs" => [
                    "abilities" => [
                        'name' => 'AuthPanel::auth.sections.abilities',
                        'item_model' => null,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => [],
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update"
                            ]
                        ],
                    ],
                ]

            ],
        ]
    ]
];
