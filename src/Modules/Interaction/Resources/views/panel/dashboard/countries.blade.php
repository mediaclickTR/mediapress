<div class="bd-example p-4 white mb-4">
    <div class="dashboardMaps">
        <div class="title color">{{ __("InteractionPanel::dashboard.countries") }}</div>
        <div id="container">
            <div class="loading">
                <i class="icon-spinner icon-spin icon-large"></i>
            </div>
        </div>
    </div>
</div>

<script>
    window.graphData.countries = @json($data)
</script>

<script src="{!! asset('vendor/mediapress/js/dashboardMaps.js') !!}"></script>
