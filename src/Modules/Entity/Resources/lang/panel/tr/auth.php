<?php

return [
    "sections"=>[
        "users"=>"Kullanıcılar",
        "user_lists"=>"Kullanıcı Listeleri",
        "entities"=>"Kurum/Kuruluş",
        "entity_lists"=>"Kurum/Kuruluş Listeleri",
    ],
    "users"=>"Kullanıcılar",
    "corporate"=>"Tüzel Kayıtlar",
];
