<?php

Route::get('login', 'PanelLoginController@showLoginForm')->name("panel.login");

if(class_exists(\Mediapress\VeronLogin\Http\Controllers\Panel\PanelLoginController::class)){

    Route::post('login', '\Mediapress\VeronLogin\Http\Controllers\Panel\PanelLoginController@login');
}else{
    Route::post('login', 'PanelLoginController@login');

}
Route::post('logout', 'PanelLoginController@logout')->name('panel.logout');

// Registration Routes...
//Route::get('register', 'PanelRegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'PanelRegisterController@register');

//// Password Reset Routes...
//Route::get('password/reset', 'PanelForgotPasswordController@showLinkRequestForm');
//Route::post('password/email', 'PanelForgotPasswordController@sendResetLinkEmail');
//Route::get('password/reset/{token}', 'PanelResetPasswordController@showResetForm');
//Route::post('password/reset', 'PanelResetPasswordController@reset');


Route::group(['middleware' => 'panel.auth'], function () {
    Route::group(['prefix' => 'Auth', 'as'=>'Auth.'], function () {

        Route::group(['prefix' => 'Admins', 'as' => 'admins.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PanelAdminController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'PanelAdminController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'PanelAdminController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'PanelAdminController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => 'PanelAdminController@update']);
            Route::get('/ajax/{website_id?}', ['as' => 'ajax', 'uses' => 'PanelAdminController@ajax']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'PanelAdminController@delete']);
        });

        Route::group(['prefix' => 'Roles', 'as' => 'roles.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PanelRoleController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'PanelRoleController@formCreate']);
            Route::get('/{role}/edit', ['as' => 'edit', 'uses' => 'PanelRoleController@edit']);
            Route::post('/{role}/edit', ['as' => 'update', 'uses' => 'PanelRoleController@update']);
            Route::get('/{id}/abilities', ['as' => 'abilities', 'uses' => 'PanelRoleController@abilities2']);
            Route::post('/{id}/storeAbilities', ['as' => 'store_abilities', 'uses' => 'PanelRoleController@storeAbilities']);
            Route::post('/{id}/moduleActions', ['as' => 'module_actions', 'uses' => 'PanelRoleController@ajaxModuleActions']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'PanelRoleController@delete']);
            Route::get('/ajax', ['as' => 'ajax', 'uses' => 'PanelRoleController@ajax']);
        });

        Route::group(['prefix' => 'Profile', 'as' => 'profile.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'ProfileController@index']);
            Route::post('/update', ['as' => 'update', 'uses' => 'ProfileController@update']);
        });




    });
});

Route::group(['prefix' => 'Google2FA', 'as' => 'Google2fa.'], function () {
    Route::post('/verifyAuth', ['as' => 'verifyAuth', 'uses' => 'Google2FaController@verifyAuth']);
    Route::get('/activate2FA', ['as' => 'activate2FA', 'uses' => 'Google2FaController@activate2FA']);
    Route::get('/deactivate2FA', ['as' => 'deactivate2FA', 'uses' => 'Google2FaController@deactivate2FA']);
    Route::post('/verifyKey', ['as' => 'verifyKey', 'uses' => 'Google2FaController@verifyKey']);
});


