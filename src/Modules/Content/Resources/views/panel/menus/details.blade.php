@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')

    <div class="page-content pt-4">
        @include("MPCorePanel::inc.errors")
        <div class="p-30 pl-0 pr-0 pb-3" style="border-bottom: 1px solid #e6e6e6;">
            <a href="{!! route('Content.menus.edit', $id) !!}" class="btn btn-sm float-right no-bg">{{ __('ContentPanel::menu.settings') }}</a>
            <ul class="nav nav-tabs" id="myTab" role="tablist" style="border:none;">
                @php
                    if( auth()->user()->country_group ){
                        $country_groups = $country_groups->where('id', auth()->user()->country_group)->all();
                    }
                @endphp
                @php($website_id = session()->get('panel')['website']->id ?: 1)
                @foreach($country_groups as $group)
                    @foreach($group->languages as $lang)
                        <li class="nav-item">
                            <a href="javascript:void(0)"
                               data-url="{!! url("mp-admin/Content/Menus/{$group->id}/{$lang->id}/{$id}/details") !!}"
                               class="lang-btn {{ ($lang->id == $language->id && $group->id == $country_group_id) ? 'active show' : '' }}">
                                <img src="{{ asset("/vendor/mediapress/images/flags/". $lang->flag) }}"
                                     height="18"> {!! $lang->name . '(' . $group->title . ')'  !!}
                            </a>
                        </li>
                    @endforeach
                @endforeach
            </ul>
        </div>

        <div class="topPage pl-0 pr-0">
            <div class="float-left">
                <div class="title m-0" data-language-id="{!! $language->id !!}" data-country-group-id="{!! $country_group_id !!}">{!! $language->name !!}</div>
            </div>
        </div>

        <div class="p-30 pl-0 pr-0">
            <div class="row">
                <div class="col-md-6">
                    <div class="cat-list">
                        @if($menu != "[]")
                            <div class="dd" id="nestable">
                                <div class="panel panel-warning">
                                    <div class="dd span11" id="nestable-menu">
                                        <ol class="dd-list p-0">
                                        </ol>
                                    </div>
                                    {{ Form::open(['route' => 'Content.menus.updateList']) }}
                                    {!! Form::hidden('menu_json',$menu) !!}
                                    <div class="float-right">
                                        <button class="btn btn-primary">{!! __("ContentPanel::general.save") !!}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @else
                            <div class="alert alert-warning">{!! __("ContentPanel::menu.empty_menu_detail", ['language'=>$language->name]) !!}</div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="tab-list tabSelectBox">
                        <a href="{{ route("Content.menus.copy").'/'.$id. '?source='.$language->id}}"
                           class="float-right yellow-button btn-sm mt-2">
                            <i class="fa fa-copy"></i> {!! __("ContentPanel::menu.copy_menu") !!}
                        </a>
                        <div class="clearfix"></div>
                        <div class="title">
                            {!! __("ContentPanel::general.add_new") !!}
                        </div>
                        <div class="form hasFileUploader">

                            {{ Form::open(['route' => 'Content.menus.details.store']) }}
                            @csrf
                            {!!Form::hidden('menu_id', $id)!!}
                            {!!Form::hidden('language_id', $language->id)!!}
                            {!!Form::hidden('country_group_id', $country_group_id)!!}


                            <div class="col-12 p-0">
                                <?php
                                $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"2048","max_file_count":"1","additional_rules":""}', 1);
                                $options['key'] = $language->id . "_menu";
                                ?>
                                <div class="file-box file-manager" id="{!! $language->id !!}_menu"
                                     data-options='{!! json_encode($options) !!}'>
                                    <input name="file_id" type="hidden"
                                           value="[]">
                                    <a href="javascript:void(0);"
                                       onclick="javascript:addImage($(this))">
                                        <span class="file-icon"></span>
                                        <i class="mx">{!! trans('FileManagerPanel::general.option_rule.max_filesize',['q' => '2']) !!}</i>
                                    </a>
                                </div>
                            </div>

                            <div class="form-group focus">
                                <div class="tit">{!! __("ContentPanel::menu.name") !!}</div>
                                {!!Form::text('name', null, ["class"=>"validate[required]", "required"=>"required"])!!}
                            </div>

                            <div class="form-group rel" id="types">
                                <div class="tit">{!! __("ContentPanel::menu.link_types") !!}</div>
                                {!!Form::select('type', $form_data['types'],null, ['placeholder' => __("MPCorePanel::general.selection"),'class' => ''])!!}
                            </div>

                            <div class="form-group rel" id="inlink" style="display:none;">
                                <div class="tit">{!! __("ContentPanel::menu.select_url") !!}</div>
                                <div class="row">
                                    <div class="col-8">
                                        <select name="url_id" class="selectpicker"
                                                data-width="100%"
                                                data-size="8"
                                                data-live-search="true">
                                            <option value="">{{ __("ContentPanel::general.selection") }}</option>
                                            @foreach($urls as $url)
                                                <option value="{{ $url->id }}">{!! $url->url !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4 lastText">
                                        <span>{{ __("ContentPanel::menu.new_page_1") }} </span>
                                        <a href="javascript:void(0);"  data-toggle="modal" data-target="#staticSitemapEditor" class="open-static-sitemap-editor" data-sitemap-id="" data-callback="newSitemapInMenuDetails">{{ __("ContentPanel::menu.new_page_2") }} <i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group rel" id="outlink" style="display:none;">
                                <div class="tit">{!! __("ContentPanel::menu.outlink") !!}</div>
                                {!!Form::text('out_link', "", ["class"=>"validate[required]"])!!}
                            </div>

                            <div class="form-group rel" id="">
                                <div class="tit">{!! __("ContentPanel::menu.view") !!}</div>
                                {!!Form::select('type_view', $form_data['view'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => ''])!!}
                            </div>

                            <div class="form-group rel" id="targets">
                                <div class="tit">{!! __("ContentPanel::menu.targets") !!}</div>
                                {!!Form::select('target', $form_data['targets'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => ''])!!}
                            </div>

                            <div class="form-group rel">
                                <div class="tit">{!! __("ContentPanel::menu.status") !!}</div>
                                {!!Form::select('status', $form_data['status'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => ''])!!}
                            </div>

                            <div class="float-right">
                                <button class="btn btn-primary"><i
                                            class="fa fa-plus"></i> {!! __("ContentPanel::general.add_new") !!}</button>
                            </div>

                            {{ Form::close() }}

                            <div class="clearfix"></div>
                            <br/>
                            <div class="note">
                                <p>{!! __("ContentPanel::menu.info") !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")
    <link rel="stylesheet" href="{!! asset('/vendor/mediapress/css/bootstrap-select.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dropify.css') !!}">
    <style>
        select {
            width: 100% !important;
        }


        .bootstrap-select .btn {
            -webkit-appearance: none;
            background: url("/vendor/mediapress/images/path.png") right 10px center #fff no-repeat;
            font: 17px Comfortaa-Regular;
            color: #3c4858 !important;
            border: 1px solid #d2d2d2;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }

        .bootstrap-select .btn:after {
            content: none;
        }

        .bootstrap-select .dropdown-menu {
            width: 100% !important;

        }

        .bootstrap-select .bs-searchbox > input {
            height: 30px;
            padding: 12px 12px;
            margin: 8px 0;
            display: inline-block;
        }

    </style>

    <style>
        .hasFileUploader .images img {
            max-height: 183px;
            border-radius: 4px;
        }

        .post-file.has-file {
            border: none;
        }

        hr {
            margin-top: -30px;
            margin-bottom: 30px;
            clear: left;

        }
        .right-area{
            float:right;
            position: absolute;
            right: -120px;
            top: -60px;
        }

        .hasFileUploader .right-area input{
            width: 70px !important;
            float: left !important;
        }
        .hasFileUploader .right-area label{
            float: left !important;
            margin: 6px 3px 6px 20px !important;
        }
    </style>
@endpush
@push('scripts')
    <script src="{!! asset("/vendor/mediapress/js/bootstrap-select.min.js") !!}"></script>
    <script>
        $(document).ready(function () {
            $("select[name='type']").change(function () {

                if ($(this).val() == 0) {
                    $("#inlink").show();
                    $("#outlink").hide();
                } else if ($(this).val() == 1) {
                    $("#outlink").show();
                    $("#inlink").hide();
                } else if ($(this).val() == 2) {
                    $("#outlink").hide();
                    $("#inlink").hide();
                    $("#targets").hide();
                } else {
                    $("#outlink").hide();
                    $("#inlink").hide();
                }
            });

            $("select[name='type']").trigger("change");



            $('.lang-btn').on('click', function () {
                var url = $(this).data('url');
                Swal.fire({
                    title: '{{ __("ContentPanel::general.swal.are_you_sure") }}',
                    text: '{!! __("ContentPanel::menu.draft.info") !!}',
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: '{!! __("ContentPanel::general.yes") !!}',
                    cancelButtonText: '{!! __("ContentPanel::general.no") !!}',
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function (dismiss) {
                    if (dismiss.value) {
                        window.location = url;
                    }
                });
            });

            $('.dd-list').delegate('.delete', 'click', function () {
                var id = $(this).find('i').data('id');
                var sibling = $(this).siblings('ol.dd-list');
                Swal.fire({
                    title: '{{ __("ContentPanel::general.swal.are_you_sure") }}',
                    text: '{{ __("ContentPanel::menu.delete.info") }}',
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: '{!! __("ContentPanel::general.delete") !!}',
                    cancelButtonText: '{!! __("ContentPanel::general.cancel") !!}',
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function (dismiss) {
                    if (dismiss.value) {
                        if(sibling[0] != undefined) {
                            Swal.fire({
                                title: '{{ __("ContentPanel::general.swal.are_you_sure") }}',
                                text: '{{ __("ContentPanel::menu.delete.child_info") }}',
                                type: "info",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: '{!! __("ContentPanel::general.yes") !!}',
                                cancelButtonText: '{!! __("ContentPanel::general.no") !!}',
                                closeOnConfirm: false,
                                closeOnCancel: false
                            }).then(function (dismiss) {
                                if (dismiss.value) {
                                    window.location = '{{ url('mp-admin/Content/Menus/detailsDelete') }}/' + id + '?child=1';
                                } else {
                                    window.location = '{{ url('mp-admin/Content/Menus/detailsDelete') }}/' + id + '';
                                }
                            });
                        } else {
                            window.location = '{{ url('mp-admin/Content/Menus/detailsDelete') }}/' + id + '';
                        }
                    }
                });
            });

            $('.dd-list').delegate('.edit', 'click', function () {
                var id = $(this).find('i').data('id');
                Swal.fire({
                    title: '{{ __("MPCorePanel::general.are_you_sure") }}',
                    text: '{!! __("ContentPanel::menu.draft.info") !!}',
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: '{!! __("MPCorePanel::general.yes") !!}',
                    cancelButtonText: '{!! __("MPCorePanel::general.no") !!}',
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function (dismiss) {
                    if (dismiss.value) {
                        window.location = '{{ route('Content.menus.index') }}/{!! $id !!}/edit-detail/' + id;
                    }
                });
            });

            $(function () {
                var obj = '{!! $menu !!}';
                var output = '';
                $.each(JSON.parse(obj), function (index, item) {
                    output += buildItem(item);
                });
                $('#nestable-menu').nestable().on('change', function () {
                    $('[name=menu_json]').val(JSON.stringify($('#nestable-menu').nestable('serialize')))
                })
                $(".dd-list").append(output);
                $("#nestable-output").val(obj);
            });

            function buildItem(item) {
                var html = "<li class='dd-item' data-id='" + item.id + "' >";
                html += "<div class='AllLeft'><span class='delete '><i class='fa fa-trash' data-id='" + item.id + "'></i></span>"
                    + "<span class='edit'><i class='fa fa-edit' data-id='" + item.id + "'></i></span></div>"
                    + "<div class='dd-handle'>" + item.name + "</div>";
                if (item.status == 2) {
                    html += "<b style='float:right;color:#a74242;position:absolute;top:20px;right:20px'>{{ trans('MPCorePanel::general.status-titles.passive') }}</b>";
                }
                if (item.children) {
                    html += "<ol class='dd-list'>";
                    $.each(item.children, function (index, sub) {
                        html += buildItem(sub);
                    });
                    html += "</ol>";
                }
                html += "</li>";
                return html;
            }


        });

        function newSitemapInMenuDetails(event){
            var model = event.model;
            var page_lang_id, page_cg_id;

            page_lang_id = $('.title').data('language-id');
            page_cg_id =   $('.title').data('country-group-id');

            var  url_id, url, toselect;
            $.each(model.details, function(ind, el){
                url_id = el.url.id;
                url = el.url.url;
                let option = '<option value="'+url_id+'">'+url+'</option>';
                $('select.selectpicker[name=url_id]').append(option);
                if(el.language_id == page_lang_id && el.country_group_id == page_cg_id){
                    toselect = url_id;
                }
            });
            $('select.selectpicker[name=url_id]').selectpicker('refresh').selectpicker('val',toselect);
            //$('select.selectpicker[name=url_id]');
        }

    </script>


@endpush


@if(function_exists('customFilemanager'))
    @push('scripts')
        {!! customFilemanager() !!}
    @endpush
@endif
@push('styles')
    <style>
        .dd-item span {
            color: #333 !important;
        }

        .page .page-content .content .cat-list ol li span {
            background: none;
        }
    </style>

    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
    <style>
        .file-manager {
            border: 2px solid #E5E5E5;
            height: 181px;
            width: 230px;
            float:left;
            margin-bottom: 10px;
        }

        .file-manager-image {
            height: 100%;
        }
        .file-manager-image img {
            height: 100%;
        }
        i.file-manager-rule {
            position: absolute;
            left: 0;
            bottom: 48px;
            text-align: center;
            display: block;
            width: 100%;
            font: 12px "Poppins", sans-serif;
            color: #393939;
        }

        ul.file-manager-options {
            position: initial !important;
        }
        ul.file-manager-options li {
            width: 100% !important;
            padding: 0 10px;
            float: none;
        }
        ul.file-manager-options li u {
            text-align: left !important;
        }
    </style>
@endpush
