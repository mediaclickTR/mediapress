<?php

Route::get("/urlAuth", ["as" => "web.url.auth", "uses" => "PasswordRequiredController@index"]);
Route::post("/urlAuth", ["uses" => "PasswordRequiredController@check"]);
Route::fallback("WebUrlController@index");

Route::get('/assets/css/{names}.css', "AssetController@css");
Route::get('/assets/js/{names}.js', "AssetController@js");

Route::get('/mediapressAssets/css/{names}.css', "AssetController@mediapressCSS");
Route::get('/mediapressAssets/js/{names}.js', "AssetController@mediapressJS");

/*Route::get('/gtmetrix-test', "GTMetrixController@index");*/
