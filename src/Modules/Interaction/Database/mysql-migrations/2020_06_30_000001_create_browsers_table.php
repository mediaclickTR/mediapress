<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrowsersTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('browsers'))
            return;

		Schema::connection('mediapress_interaction')->create('browsers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('browsers');
	}
}
