<?php

namespace Mediapress\Modules\Interaction\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;

class Visit extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'visits';

    protected $fillable = [
        "id",
        "cookie_id",
        "session_id",
        "url_id",
        "ip",
        "query_string",
        "referer",
        "hour",
        "week"
    ];
    public $timestamps = false;


    public static function boot()
    {

        parent::boot();


        static::creating(function ($model) {

            $model->created_at = Carbon::now();

        });
    }


    public function cookie(){
        return $this->belongsTo(Cookie::class);
    }
    public function session(){
        return $this->belongsTo(Session::class);
    }
}
