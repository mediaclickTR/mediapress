<?php

namespace Mediapress\Modules\Auth\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Auth\Models\Admin;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use ValidatesRequests;

    public const ROLE_ID = 'role_id';
    public const LANGUAGE_ID = 'language_id';
    public const FIRST_NAME = 'first_name';
    public const LAST_NAME = 'last_name';
    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASS_WORD = 'password';
    public const AUTH_PANEL_ADMIN_EMAIL_ADMIN = "AuthPanel::admin.email-admin";
    public const AUTH_PANEL_ADMIN_PASS_WORD_ADMIN = "AuthPanel::admin.password-admin";
    public const AUTH_PANEL_ADMIN_USERNAME_ADMIN = "AuthPanel::admin.username-admin";
    public const AUTH_PANEL_ADMIN_LAST_NAME_ADMIN = "AuthPanel::admin.last-name-admin";
    public const AUTH_PANEL_ADMIN_NAME_ADMIN = "AuthPanel::admin.name-admin";
    public const AUTH_PANEL_ADMIN_ROLE_ADMIN = "AuthPanel::admin.role-admin";
    public const REQUIRED = 'required';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';

    public function index()
    {
        $user = auth()->user();

        if($user->role_id == 1) {
            return redirect()->route('panel.dashboard');
        }

        return view("AuthPanel::profile", compact("user"));
    }

    public function update()
    {
        $data = request()->except('_token');

        $admin = Admin::find($data['id']);

        $data['google2fa_warning'] = isset($data['google2fa_warning']) ? 1 : 0;

        $fields = [
            self::FIRST_NAME => trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN),
            self::LAST_NAME => trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN),
            'phone' => trans("AuthPanel::admin.phone-admin"),
            self::EMAIL => trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN),
            self::PASS_WORD => trans(self::AUTH_PANEL_ADMIN_PASS_WORD_ADMIN),
        ];

        $rules = [
            self::FIRST_NAME => self::REQUIRED,
            self::LAST_NAME => self::REQUIRED,
            self::EMAIL => 'unique:admins,email,' . $admin->id,
            self::PASS_WORD => 'nullable|min:8|regex:/[a-zA-Z]/|regex:/[0-9]/|regex:/[@$!%*#?.&]/',
        ];

        $messages = [
            'first_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans(self::AUTH_PANEL_ADMIN_NAME_ADMIN)]),
            'last_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans(self::AUTH_PANEL_ADMIN_LAST_NAME_ADMIN)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
            'email.unique' => trans("MPCorePanel::validation.unique", ['unique', trans(self::AUTH_PANEL_ADMIN_EMAIL_ADMIN)]),
        ];

        $this->validate(request(), $rules, $messages, $fields);

        if (is_null($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }
        unset($data['id']);

        $data['username'] = trim($data['first_name']) . ' ' . trim($data['last_name']);

        $update = $admin->update($data);

        if($update) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            if($schema->hasColumn('admins', 'api_token')) {
                if(is_null($admin->api_token)) {
                    $admin->api_token = uniqid();
                    $admin->save();
                }
            }
        }

        return redirect()->route('Auth.profile.index')->with(['message' => "Profil bilgileri başarıyla güncellenmiştir."]);
    }
}
