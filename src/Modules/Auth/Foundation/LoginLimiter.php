<?php

namespace Mediapress\Modules\Auth\Foundation;

use Illuminate\Http\Request;
use Mediapress\Modules\Auth\Models\LoginAttempt;

class LoginLimiter
{
    private $ip;
    private $maxAttempt;
    private $expireTime;
    private $whiteList;

    public $attempt;

    function __construct()
    {
        $this->setAttributes();
    }

    public function init()
    {
        if($this->ipInWhiteList()) {
            $this->deleteLoginAttempt();
        } else {
            if(is_null($this->attempt)) {
                $this->createLoginAttempt();
            }

            $this->increaseLoginAttempt();
        }
    }

    public function refresh()
    {
        if($this->attempt) {
            $this->attempt->update(['count' => 0, 'expire_time' => null]);
        }
    }

    public function checkExpireTime()
    {
        if($this->attempt) {
            return is_null($this->attempt->expire_time) || ($this->attempt->expire_time && $this->attempt->expire_time <= time());
        }
        return true;
    }

    public function getErrorMessage() {
        $errorMsg = trans('AuthPanel::auth.failed');

        if($this->attempt) {
            if(is_null($this->attempt->expire_time)) {
                $errorMsg .= '</br>' . ($this->maxAttempt - $this->attempt->count) . " kere yanlış girme hakkınız kalmıştır.";
            } else {
                $time = $this->attempt->expire_time - time();
                $errorMsg = "Çok fazla hatalı giriş yaptınız! </br> " . "<span id='expireTime'>" . $time . "</span>" . " saniye sonra tekrar deneyiniz.";
            }
        }

        return $errorMsg;
    }

    private function ipInWhiteList()
    {
        return in_array($this->ip, $this->whiteList);
    }

    private function createLoginAttempt()
    {
        $this->attempt = LoginAttempt::create(
            [
                'ip' => $this->ip,
                'count' => 0
            ]
        );
    }

    private function increaseLoginAttempt() {

        if($this->attempt->expire_time && $this->attempt->expire_time <= time()) {
            $this->refresh();
        }
        $this->attempt->increment('count');

        if($this->attempt->count >= $this->maxAttempt) {
            $expireTime = time() + ($this->expireTime * 60);
            $this->attempt->update(['expire_time' => $expireTime]);
        }
    }

    private function deleteLoginAttempt() {
        if($this->attempt) {
            $this->attempt->delete();
            $this->attempt = null;
        }
    }

    private function setAttributes()
    {
        $request = request();
        $this->ip = $request->server('HTTP_CF_CONNECTING_IP') ?: $request->server('REMOTE_ADDR');

        $this->maxAttempt = settingSun('login_limiter.max_attempt');
        $this->expireTime = settingSun('login_limiter.expire_time');
        $tempWhiteList = settingSun('login_limiter.white_list');

        $this->whiteList = $tempWhiteList ? json_decode(settingSun('login_limiter.white_list'), 1) : array();

        $this->attempt = LoginAttempt::where('ip', $this->ip)->first();
    }

}
