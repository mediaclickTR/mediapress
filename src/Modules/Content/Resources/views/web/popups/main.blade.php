@php
    $hold = new \Mediapress\Modules\Content\Foundation\Popup($mediapress);
    $popups = $hold->popups;
@endphp

@if($popups->isNotEmpty())
    <!-- Popup Start -->
    <link rel="stylesheet" href="{!! mp_asset('mediapressAssets/css/mcpopup_v04.css') !!}"/>

    @if(file_exists(resource_path('assets/css/customPopup.css')))
        <link rel="stylesheet" href="{!! mp_asset('assets/css/customPopup.css') !!}"/>
    @endif

    @foreach($popups as $key => $popup)
        @if(popupImpressions($popup, $popup->button_type != 2))
            @continue
        @endif

        @if($popup->custom_css)
            <style>
                {!! $popup->custom_css !!}
            </style>
        @endif

        @if($popup->type == 1)
            @include("Popup::types.lightbox")
        @elseif($popup->type == 2)
            @include("Popup::types.tooltip")
        @elseif($popup->type == 3)
            @include("Popup::types.bar")
        @elseif($popup->type == 4)
            @include("Popup::types.curtain")
        @endif

        @if(($popup->button_type == 2 || $popup->type == 4))
            <script>
                $('[id^=mcTopTooltipClose], .biggnerContentClose').click(function () {
                    $.ajax({
                        'url': ' {{ route('setPopupImpressions') }}',
                        'method': 'GET',
                        'data': {'popupId' : '{{ $popup->id }}'}
                    });
                });
            </script>
        @endif

    @endforeach
    <!-- Popup Finish -->
@endif
