@if($repoName == 'GoogleRepository')
    <div class="bd-example d-inline-block w-100 ">
        <div class="dashboardBoxes w-100">
            <div class="row">
                <div class="col">
                    <div class="item">
                        <div class="icon border-right online"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.online") }}
                            <span class="green">{{ $data['online'] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <div class="icon border-right speed"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.load_time") }}
                            <span class="yellow">{{ $data['loadTime'] }} (s)</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <div class="icon border-right times"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.visit_time") }}
                            <span class="purple">{{ $data['visitTime'] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <div class="icon border-right traffic"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.bounces") }}
                            <span class="orange">% {{ $data['bounces'] }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="bd-example d-inline-block w-100">
        <div class="dashboardBoxes w-100">
            <div class="row">
                <div class="col">
                    <div class="item">
                        <div class="icon border-right online"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.online") }}
                            <span class="green">{{ $data['online'] }}</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <div class="icon border-right speed"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.avg_visit") }}
                            <span class="yellow">{!! $data['avgVisit'] !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <div class="icon border-right traffic"></div>
                        <div class="text">{{ __("InteractionPanel::dashboard.bounces") }}
                            <span class="orange">% {{ $data['bounces'] }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
