<?php

namespace Mediapress\Modules\MPCore\Models;

use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Models\Module;
use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class UserActionLogs extends Model
{
    use CacheQueryBuilder;

    protected $table = 'useraction_logs';

    public $timestamps = true;

    protected $fillable = [
        "website_id",
        "user_id",
        "controller",
        "action",
        "module_id",
        "model_id",
        "model_type",
        "context_type",
        "context_icon",
        "content",
        "priority",
        "parent_id",
        "scope_1",
        "scope_2",
        "scope_3",
        "ip"
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function website()
    {
        return $this->belongsTo(Website::class, "website_id");
    }

    public function user()
    {
        return $this->belongsTo(Admin::class, "user_id");
    }

    public function module()
    {
        return $this->belongsTo(Module::class, "module_id");
    }


}
