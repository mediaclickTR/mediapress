<?php


return [
    'categories' => 'Kategoriler',
    'criterias' => 'Kriterler',
    'menus' => 'Menüler',
    'social_media' => 'Sosyal Medya',
    'slider' => 'Slider Yönetimi',
    'popup' => 'Etkileşim Yönetimi',
    'users' => 'Üyeler / Üye Listesi',
    'modules' => 'Modüller',

    'structure' => 'Yapı',
    'websites' => 'Site Yapısı',
    'sitemaps' => 'Sayfa Yapıları',
    'seo' => "Meta Yönetimi",
    'meta_templates' => "Meta Formülleri",
    'robots' => "Robots.txt",
    'image_tags' => "Görsel Tag Yönetimi",
    'sitemap_xml_editor' => "Sitemap Xml",
    'seo_virtual' => "Sanal Sayfalar",
    'panel_menus' => 'Mediapress Menü',
    'export_pages' => 'Dışarı Aktar',

    'comments' => 'Yorumlar',
];
