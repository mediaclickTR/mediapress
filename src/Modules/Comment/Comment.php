<?php

namespace Mediapress\Modules\Comment;

use Mediapress\Models\MPModule;

class Comment extends MPModule
{

    public const SUBMENU = "submenu";
    public const TYPE = "type";
    public const TITLE = "title";
    public const URL = "url";
    public $name = "Comment";
    public $url = "mp-admin/Comments";
    public $description = "Comment module of Mediapress";
    public $author = "";
    public $menus = [];
    private $plugins = [];


    public static function fillMenu($menu)
    {
        data_set($menu, 'header_menu.content.cols.modules.name', trans("ContentPanel::menu_titles.modules"));
        $content_cols_modules_rows_set = [
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.comments"),
                self::URL => route("CommentStructure.index"),
            ],
        ];

        $menu = dataGetAndMerge($menu, 'header_menu.content.cols.modules.rows', $content_cols_modules_rows_set);

        return $menu;
    }
}
