<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class Extra extends Model
{
    use CacheQueryBuilder;
    protected $table = 'extras';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['model_type', 'model_id  ', 'key','value'];

    public function model()
    {
        return $this->morphTo();
    }
}
