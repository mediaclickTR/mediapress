<div class="modal fade bd-example-modal-lg" id="imageQuality" tabindex="-1" role="dialog"
     aria-labelledby="imageQualityLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageQualityLabel">{!! __('SettingPanel::image_quality.modal_title') !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <input type="range" class="custom-range" id="image_quality_input"
                               value="{{ config('mediapress.image_quality') }}" min="1"
                               style="width: 100%; margin-bottom: 5%"
                               max="100" oninput="image_quality_output.value = image_quality_input.value"
                               onchange="updateImage(this.value)">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <img src="{{ asset('vendor/mediapress/images/image-quality.jpg') }}"
                             style="max-height: 100%; max-width: 100%"
                             id="image-test">
                    </div>
                    <div class="col-md-4">
                        <ul>
                            <li>
                                <span>
                                    {!! __('SettingPanel::image_quality.quality') !!}
                                    <output id="image_quality_output">{{ config('mediapress.image_quality') }}</output>
                                </span>
                            </li>
                            <li>
                                <span>
                                    {!! __('SettingPanel::image_quality.original_size') !!}
                                    <small>{{ formatBytes(filesize(public_path('vendor/mediapress/images/image-quality.jpg'))) }}</small>
                                </span>
                            </li>
                            <li>
                                <span>
                                    {!! __('SettingPanel::image_quality.new_size') !!}
                                    <small id="image-size"></small>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row form-group">
                    <div class="col-md-1 mr-3">
                        <label class="btn">Webp: </label>
                    </div>
                    <div class="col-md-8">
                        <div id="webp_status" class="btn-group" data-toggle="buttons">
                            <label
                                class="btn {!! config('mediapress.support_webp') == true ? 'btn-primary active' : '' !!}"
                                data-toggle-passive-class="btn-default">
                                <input type="radio" name="webp_status"
                                       value="true" {!! config('mediapress.support_webp') == true ? 'checked' : '' !!}>
                                {!! __('SettingPanel::general.active') !!}
                            </label>

                            <label
                                class="btn {!! config('mediapress.support_webp') != true ? 'btn-primary active' : '' !!}"
                                data-toggle-passive-class="btn-default">
                                <input type="radio" name="webp_status"
                                       value="false" {!! config('mediapress.support_webp') != true ? 'checked' : '' !!}>
                                {!! __('SettingPanel::general.passive') !!}
                            </label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{!! __("SettingPanel::general.close") !!}</button>
                <button type="button" class="btn btn-primary"
                        onclick="changeEnv()">{!! __("SettingPanel::general.save") !!}</button>
            </div>
        </div>
    </div>
</div>
