@extends('AuthPanel::inc.module_main')

@section("content")
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {{trans("AuthPanel::roles.titles.index")}}
                </div>
            </div>
            <div class="float-right">
                @if(userAction('role.create',true,false))
                    <a href="{{route("Auth.roles.create")}}" class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-plus-circle"></i> {{trans('AuthPanel::roles.add_new')}}
                    </a>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="p-30">
            <div class="table-field">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
@endsection
