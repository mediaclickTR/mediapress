<?php

namespace Mediapress\Modules\Interaction\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;

class Cookie extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'cookies';
    protected $fillable = [
        "hash",
        'user_id',
        'browser_id',
        'device_id',
        'operating_system_id',
    ];
    public $timestamps = false;


    public static function boot()
    {

        parent::boot();


        static::creating(function ($model) {

            $model->created_at = Carbon::now();

        });
    }

    public function sessions(){
        return $this->hasMany(Session::class);
    }
    public function visits(){
        return $this->hasMany(Visit::class);
    }
}
