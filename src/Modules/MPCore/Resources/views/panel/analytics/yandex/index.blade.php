@extends('MPCorePanel::inc.app')
@section('content')

    @push("styles")
        <!-- page specific resources-->
        <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dashboard.css') !!}" />
        <!-- page specific resources-->
    @endpush

    <div class="container-fluid">
        @include("MPCorePanel::inc.errors")
        <div class="page">
            <div class="title float-left">{!! trans("MPCorePanel::analytics.yandex") !!}</div>
            @if($conn == true)
                <div class="float-right">
                    <select onchange="locationOnChange(this.value);">
                        @php($request = request())
                        <option @if($request->days)  @if($request->days=='0') selected @endif @endif value="{!! url()->current() !!}?days='0'">{!! trans("MPCorePanel::analytics.today") !!}</option>
                        <option @if($request->days) @if($request->days==1) selected @endif @endif value="{!! url()->current() !!}?days=1">{!! trans("MPCorePanel::analytics.yesterday") !!}</option>
                        <option @if($request->days==7 || !$request->days) selected @endif value="{!! url()->current() !!}?days=7">{!! trans("MPCorePanel::analytics.week") !!}</option>
                        <option @if($request->days) @if($request->days==30) selected @endif @endif value="{!! url()->current() !!}?days=30">{!! trans("MPCorePanel::analytics.month") !!}</option>
                        <option @if($request->days) @if($request->days==90) selected @endif @endif value="{!! url()->current() !!}?days=90">{!! trans("MPCorePanel::analytics.three-month") !!}</option>
                        <option @if($request->days) @if($request->days==365) selected @endif @endif value="{!! url()->current() !!}?days=365">{!! trans("MPCorePanel::analytics.year") !!}</option>
                    </select>
                </div>
            @endif
            <div class="page-content">
                @if($conn==true)
                    <div class="analytics-detail">
                        <a class="yellow-button" href="{!! route('Interaction.analytics.google.detailedAnalytics') !!}">
                            <i class="fa fa-bars"></i> {!! trans("MPCorePanel::analytics.detailedAnalytics") !!}
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="timer-box row">
                        <div class="col-md-3">
                            <div class="item">
                                <span class="timer" data-from="{!! $data['totals']['pageviews']+100 !!}" id="duration" data-to={!! $data['totals']['pageviews'] !!} data-speed="1000"></span>
                                <i>{!! trans("MPCorePanel::analytics.total-views") !!}</i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="item">
                                <span class="timer" data-from="{!! $data['totals']['users']+100 !!}" id="duration" data-to="{!! $data['totals']['users'] !!}" data-speed="1000"></span>
                                <i>{!! trans("MPCorePanel::analytics.users") !!}</i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="item">
                                <span class="timer" data-from="{!! $totalDuration+100 !!}" data-to="{!! (int)$totalDuration; !!}" data-speed="1000"></span>
                                <i>{!! trans("MPCorePanel::analytics.average-session-time") !!}</i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="item">
                                <span class="timer" data-from="{!! $organicData['totals']['organicVisits']+100 !!}" id="duration" data-to={!! $organicData['totals']['organicVisits'] !!} data-speed="1000"></span>
                                <i>{!! trans("MPCorePanel::analytics.organic-users") !!}</i>
                            </div>
                        </div>

                    </div>
                    <div class="name">{!! trans("MPCorePanel::analytics.general-analytics") !!}</div>
                    <div id='myChart'></div>
                @else
                    <div class="alert alert-light" style="border:#ccc 1px solid">
                        {!! trans("MPCorePanel::analytics.connection_warning", ['type'=>'Yandex']) !!}
                        <a id="google_connect" style="color:#6c757d" class="float-right" href="#">
                            {!! trans("MPCorePanel::analytics.google_connect") !!} <i class="fa fa-key"></i>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                @endif
            </div>
        </div>

    </div>

@endsection

@push("scripts")
    <script src="{!! asset('vendor/mediapress/js/timer.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/zingchart.min.js') !!}"></script>
    @if($conn==true)
    <script>
        var myConfig = {
            "type": "area",
            "plotarea": {
                "margin": "dynamic"
            },
            "scale-x": {
                "values": [
                    '',
                    @if(isset($data))
                            @foreach($data['visitorAndPageViews'] as $day)
                        '{!! translateDate("j F Y",$day['ga:date']) !!}',
                    @endforeach
                    @endif
                ],
                "line-style": "solid",
                "line-width": "1px",
                "line-color": "#5b5b5b",
                "guide": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#606060"
                },
                "tick": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#606060"
                }
            },
            "scale-y": {
                "min-value": 0,
                "max-value":{!! (isset($max)) ? $max : '' !!}, //max
                "step": 1,
                "format": "",
                "label": {
                    "font-color": "#5b5b5b"
                },
                "line-style": "solid",
                "line-width": "2px",
                "line-color": "#606060",
                "guide": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#e2e2e2"
                },
                "tick": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#5b5b5b"
                }
            },
            "legend": {
                "layout": "float",
                "toggle-action": "remove",
                "shadow": 0,
                "adjust-layout": true,
                "align": "center",
                "vertical-align": "bottom",
                "marker": {
                    "type": "match",
                    "show-line": true,
                    "line-width": 4,
                    "shadow": "none"
                }
            },
            "series": [{
                "values": [
                    0,
                    @if(isset($data))
                    @foreach($data['visitorAndPageViews'] as $visitor)
                    {!! $visitor['ga:users'] !!},
                    @endforeach
                    @endif

                ],
                "text": '{!! trans("MPCorePanel::analytics.user") !!}',
                "line-color": "#2196f3",
                "marker": {
                    "background-color": "#fff",
                    "border-color": "#2196f3",
                    "border-width": "1px",
                    "shadow": false
                },
                "background-color": "#2196f3"
            }, {
                "values": [
                    0,
                    @if(isset($data))
                    @foreach($data['visitorAndPageViews'] as $pageView)
                    {!! $pageView['ga:pageviews'] !!},
                    @endforeach
                    @endif
                ],
                "text": '{!! trans("MPCorePanel::analytics.views") !!}',
                "line-color": "#12574b",
                "marker": {
                    "background-color": "#fff",
                    "border-color": "#1d8b80",
                    "border-width": "1px",
                    "shadow": false
                },
                "background-color": "blue"
            }],

        };


        zingchart.render({
            id: 'myChart',
            data: myConfig,
            height: '100%',
            width: '100%'
        });
    </script>
    @endif
@endpush
