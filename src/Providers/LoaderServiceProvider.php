<?php

namespace Mediapress\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class LoaderServiceProvider extends ServiceProvider
{
    public const HELPERS_PHP = 'helpers.php';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if(file_exists(app_path('Http'.DIRECTORY_SEPARATOR. self::HELPERS_PHP))){
            include_once app_path('Http'.DIRECTORY_SEPARATOR. self::HELPERS_PHP);
        }
        if(file_exists(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR. self::HELPERS_PHP)){
            include_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR. self::HELPERS_PHP;
        }
        /*$modulesdir = base_path('vendor/mediapress/mediapress/src/Modules');
        if(is_dir($modulesdir)){
            $found = File::directories($modulesdir);
            foreach($found as $module){
                $filename = pathinfo($module, PATHINFO_FILENAME);
                $modpath = 'Mediapress\\Modules\\'.$filename;
                if(class_exists($modpath)){
                    $modpath::register();
                }
            }
        }*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function loadModule(){

    }
}
