<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateWebsiteExtrasTable extends Migration
    {

        public const WEBSITE_EXTRAS = "website_extras";

        public function up()
        {
            if (!Schema::hasTable(self::WEBSITE_EXTRAS)) {
                Schema::create(self::WEBSITE_EXTRAS, function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('website_id')->index();
                    $table->string('key');
                    $table->text('value');
                });
            }
        }

        public function down()
        {
            if (Schema::hasTable(self::WEBSITE_EXTRAS)) {
                Schema::drop(self::WEBSITE_EXTRAS);
            }
        }
    }
