<?php

namespace Mediapress\Modules\MPCore\Controllers;

use App\Http\Controllers\Controller;
use Mediapress\FileManager\Models\MDisk;
use Mediapress\FileManager\Models\MFile;
use Mediapress\FileManager\Models\MFolder;
use Illuminate\Support\Facades\Storage;

class FileManagerControllerDraft extends Controller
{

    public const REQ_STATE = "req_state";
    public const STATUS = "status";
    public const DISKKEY = 'diskkey';
    public const ERRORS = "errors";
    public const STATUS1 = self::STATUS;
    public const FILES = "files";
    public const ITEMS = "items";
    public const BREADCRUMB = "breadcrumb";
    public const NAV_DISK = "nav_disk";
    public const NAV_FOLDER = "nav_folder";
    public const ACTION = "action";
    public const FOLDERS = "folders";
    public const MFOLDER_ID = "mfolder_id";
    public const CREATE_FOLDER_NAME = "create_folder_name";

    public function index($diskkey = "", $folder_id = null)
    {

        $params = request()->all();

        $resultset = $this->resultset();
        $resultset[self::REQ_STATE] = $params;

        // true till error occurs
        $resultset[self::STATUS1] = true;

        if ($diskkey) {
            $disk = MDisk::where(self::DISKKEY, $diskkey)->first();
            if (!$disk) {
                $resultset[self::STATUS1] = false;
                $resultset[self::ERRORS][] = "Requested disk not found";
                //$resultset["items"]["disks"]=MDisk::all();
            } else {
                if ($folder_id) {
                    $folder = MFolder::find($folder_id);
                    if ($folder) {
                        $resultset[self::NAV_DISK] = $disk;
                        $resultset[self::NAV_FOLDER] = $folder;
                        $resultset[self::ITEMS][self::FOLDERS] = $folder->folders;
                        $resultset[self::ITEMS][self::FILES] = $folder->files;
                    } else {
                        $resultset[self::STATUS1] = false;
                        $resultset[self::ERRORS][] = "Requested folder not found";
                    }

                    while (isset($folder)) {
                        $resultset[self::BREADCRUMB][] = ["type" => "folder", "name" => $folder->path, "id" => $folder->id];
                        $folder = $folder->parent;
                    }

                } else {

                    $resultset[self::NAV_DISK] = $disk;
                    $resultset[self::ITEMS][self::FOLDERS] = $disk->folders()->with(self::FILES)->get();
                    $resultset[self::ITEMS][self::FILES] = $disk->files()->get();
                }
            }
            $resultset[self::BREADCRUMB][] = ["type" => "disk", "name" => $disk->name, "id" => $disk->diskkey];
        } else {
            $resultset[self::ITEMS]["disks"] = MDisk::all();
        }


        return response()->json($resultset);


        /*$dataset = ['disks' => MDisk::all(),
            'ac_disk' => $diskkey,
            'asked_folder' => $folder_id,
            'folders' => MFolder::where('disk_name')];

        return response()->json($dataset);*/
    }

    public function download($diskkey, $folder_id = -1, $file_id)
    {
        $file = MFile::find($file_id);
        $header = [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' . basename($file->path()) . '"',
        ];

        if (MDisk::where(self::DISKKEY, $diskkey)->first()->drive == "azure") {
            $url = 'https://' . config('filesystems.disks.azure.name') . '.file.core.windows.net/' . config('filesystems.disks.azure.container') . '/' . $file->path();
            return redirect($url);
        } else {
            return \Response::make(Storage::disk($diskkey)->get($file->path()), 200, $header);
        }
    }

    public function info($diskkey, $folder_id = -1, $file_id = -1)
    {
        if ($file_id != -1) {
            $info = Storage::disk($diskkey)->getMetadata(MFile::find($file_id)->path());
            $info["size"] = Storage::disk($diskkey)->getSize(MFile::find($file_id)->path());
        } else {
            $info = Storage::disk($diskkey)->getMetadata(MFolder::find($folder_id)->getPath());
            $info["size"] = Storage::disk($diskkey)->getSize(MFolder::find($folder_id)->getPath());
        }

        $resultset = $this->resultset();
        $resultset[self::REQ_STATE] = request()->all();
        $resultset[self::STATUS1] = true;
        $resultset["info"] = $info;
        $resultset[self::ACTION] = __FUNCTION__;

        return response()->json($resultset);
    }

    public function select($diskkey, $folder_id = -1, $file_id = -1)
    {
        $result = $this->resultset();
        $file = MFile::find($file_id);
        $result["nav_file"] = \Arr::except($file->toArray(), ['folder', 'disk']);
        $result[self::NAV_FOLDER] = $file->folder;
        $result[self::NAV_DISK] = $file->disk;
        return response()->json($result);
    }

    public function remove($diskkey, $folder_id, $file_id = -1)
    {

        $resultset = $this->resultset();

        $resultset[self::ACTION] = __FUNCTION__;
        $resultset[self::REQ_STATE] = request()->all() + [self::DISKKEY => $diskkey, 'folder_id' => $folder_id, 'file_id' => $file_id];

        $fileRes = false;


        if ($file_id != -1) {
            $file = MFile::find($file_id);
            if (!$file) {
                $resultset[self::STATUS1] = false;
                $resultset[self::ERRORS][] = "Requested file not found";
                return response()->json($resultset);
            }
            $fileRes = Storage::disk($diskkey)->delete($file->path());

            $resultset["deleted_file_id"] = $file_id;

            if ($fileRes) {
                $file->delete();
            }
        } else {
            $folder = MFolder::find($folder_id);
            if (!$folder) {
                $resultset[self::STATUS1] = false;
                $resultset[self::ERRORS][] = "Requested folder not found";
                return response()->json($resultset);
            } else if (count($folder->children) + count($folder->files) > 0) {
                $resultset[self::STATUS1] = false;
                $resultset[self::ERRORS][] = "Requested folder is not empty.";
                return response()->json($resultset);
            }
            $fileRes = Storage::disk($diskkey)->deleteDirectory($folder->getPath());
            if ($fileRes){
                $folder->delete();
            }
        }
        $resultset[self::STATUS1] = $fileRes;
        return response()->json($resultset);
    }

    public function crop($diskkey, $folder_id, $file_id)
    {
    }

    public function upload($diskkey, $folder_id = -1)
    {
        /** @var \Illuminate\Http\UploadedFile $files */
        $file = \request("file", null);
        $folder = MFolder::find($folder_id);

        if ($folder){
            $folder = $folder->getPath();
        }
        else{
            $folder = "";
        }

        $resultset = $this->resultset();

        $name = $file->storeAs($folder, uniqid(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . "-") . '.' . $file->getClientOriginalExtension(), ["disk" => $diskkey]);
        //TODO:file exist
        if (Storage::disk($diskkey)->exists($folder . '/' . $name)) {
            MFile::create([self::MFOLDER_ID => $folder_id > 0 ? $folder_id : null, "name" => $name]);
        } else {
            $resultset[self::ERRORS][] = $name . " dosyası eklenemedi";
        }
        $resultset[self::ITEMS][self::FILES][] = $name;


        $resultset[self::STATUS1] = true;
        $resultset[self::ACTION] = __FUNCTION__;
        return response()->json($resultset);
    }

    public function upload2()
    {

        $req = request()->all();
        $diskkey = $req["active_disk_key"];
        $folder_id = $req["active_folder_id"];
        $file = \request("file", null);


        $folder = MFolder::find($folder_id);

        if ($folder) {
            $folder = substr($folder->getPath(), 0, -1);
            //return var_dump($folder);
        } else {
            $folder = "";
        }

        $resultset = $this->resultset();
        $newfname = uniqid(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . "-") . '.' . $file->getClientOriginalExtension();


        //return var_dump($newfname);
        //return var_dump($newfname);

        $name = $file->storeAs($folder, $newfname, ["disk" => $diskkey]);
        //TODO:file exist
        if (Storage::disk($diskkey)->exists($name)) {
            MFile::create([self::MFOLDER_ID => $folder_id > 0 ? $folder_id : null, "name" => $newfname]);
        } else {
            $resultset[self::ERRORS][] = $name . " dosyası eklenemedi";
        }
        $resultset[self::ITEMS][self::FILES][] = $newfname;


        $resultset[self::STATUS1] = true;
        $resultset[self::ACTION] = __FUNCTION__;
        return response()->json($resultset);
    }

    public function createFolder($diskkey, $folder_id = -1)
    {
        $request = request()->only(self::CREATE_FOLDER_NAME);
        $request["mdisk_id"] = MDisk::where(self::DISKKEY, $diskkey)->first(["id"])->id;
        $request["path"] = $request[self::CREATE_FOLDER_NAME];
        unset($request[self::CREATE_FOLDER_NAME]);
        $path = $request["path"];
        $parent = MFolder::find($folder_id);
        if ($parent) {
            $path = $parent->getPath() . "/" . $path;
            $request[self::MFOLDER_ID] = $parent->id;
        }

        if (!Storage::disk($diskkey)->exists($path)) {
            Storage::disk($diskkey)->makeDirectory($path);
            $folder = MFolder::create($request);
        } else {
            $folder = MFolder::where([self::MFOLDER_ID => $folder_id, "path" => $request["path"]])->first();
        }

        $resultset = $this->resultset();
        $resultset[self::ACTION] = __FUNCTION__;
        $resultset[self::REQ_STATE] = $request;
        $resultset[self::STATUS1] = true;
        $resultset[self::ITEMS][self::FOLDERS][] = $folder->path;
        return response()->json($resultset);
    }

    private function resultset()
    {
        return [
            self::ACTION => null,
            self::BREADCRUMB => [],
            "mode" => null,
            self::STATUS1 => null,
            self::ERRORS => [],
            self::NAV_DISK => null,
            self::NAV_FOLDER => null,
            "nav_file" => null,
            "selected" => null,
            self::REQ_STATE => [

            ],
            self::ITEMS => [
                "disks" => [],
                self::FOLDERS => [],
                self::FILES => []
            ]
        ];
    }

}
