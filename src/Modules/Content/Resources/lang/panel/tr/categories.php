<?php

return [
    'list_page_title'=>'Kategori, Kriter, Özellik Seçimi',
    'categories'=>'Kategoriler',
    'classification'=>'Sınıflandırma',
    'category'=>'Kategori',
    'property'=>'Özellik',
    'properties'=>'Özellikler',
    'criteria'=>'Kriter',
    'criterias'=>'Kriterler',
    'name_category'=>'Kategori Adı',
    'create_category'=>'Kategori oluştur',
    'edit_category'=>'Kategori Düzenle',
    'add_main_category'=> 'Ana Kategori Ekle',
    'description_category'=>'Kategori Açıklaması',
    'not_found_sitemap' => 'Sayfa Yapısı bulunamadı.',
    'category_name'=>'Kategori Adı',
    'category.not.permission'=>' adlı sayfa yapısına category(kategori) özelliği kapatılmış. Kategori eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    'criteria.not.permission'=>' adlı sayfa yapısına criteria(kriter) özelliği kapatılmış. Kriter eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    'property.not.permission'=>' adlı site Haritasına property(özellik) özelliği kapatılmış. Özellik eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    /*
     * Criteria
     */
    'create_criteria'=>'Kriter oluştur',
    'edit_criteria'=>'Kriter Düzenle',
    'name_criteria'=>'Kriter Adı',

    /*
     * Property
     */
    'create_property'=>'Özellik oluştur',
    'edit_property'=>'Özellik Düzenle',
    'name_property'=>'Özellik Adı',
    'not_found_data'=>':type bulunamadı. Sağ üstteki "Yeni oluştur" butonundan :type ekleme işlemini yapabilirsiniz.'
];
