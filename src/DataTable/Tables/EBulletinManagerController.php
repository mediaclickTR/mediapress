<?php

namespace Mediapress\DataTable\Tables;

use Html;
use Mediapress\DataTable\DataTableInterface;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class EBulletinManagerController implements DataTableInterface
{
    const TITLE = 'title';
    const DATA = 'data';
    const FOOTER = 'footer';
    const WEBSITE_ID = 'website_id';

    public function columns(Builder $builder)
    {
        return $builder->columns([
            [self::DATA => 'id', 'name' => 'id', self::TITLE => 'ID', self::FOOTER => 'ID'],
            [self::DATA => 'name', 'name' => 'name', self::TITLE => trans("MPCorePanel::general.name"), self::FOOTER => trans("MPCorePanel::general.name")],
            [self::DATA => 'email', 'name' => 'email', self::TITLE => trans("MPCorePanel::general.email"), self::FOOTER => trans("MPCorePanel::general.email")],
            [self::DATA => self::WEBSITE_ID, 'name' => self::WEBSITE_ID, self::TITLE => trans("MPCorePanel::general.website"), self::FOOTER => trans("MPCorePanel::general.website")],
            [self::DATA => 'created_at', 'name' => 'created_at', self::TITLE => trans("MPCorePanel::general.created_at"), self::FOOTER => trans("MPCorePanel::general.created_at")]
        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);
    }

    public function ajax($website_id)
    {

        if ($website_id != null) {
            if( !activeUserCan(["heraldist.website".$website_id.".ebulletin.index"])){
                $ebulletins = Ebulletin::query()->where("id","<",0);
            }else{
                $ebulletins = Ebulletin::query()->where(self::WEBSITE_ID, $website_id);
            }
        } else {
            $ebulletins = Ebulletin::query();
            $allSiteIDs = Website::select("id")->get()->pluck("id")->toArray();
            foreach($allSiteIDs as $siteID){
                if( ! activeUserCan([
                    "heraldist.website".$siteID.".ebulletin.index"
                ])){
                    $ebulletins = $ebulletins->where(self::WEBSITE_ID,"<>", $siteID);
                }
            }
        }

        return DataTables::eloquent($ebulletins)
            ->addColumn(self::WEBSITE_ID, function ($ebulletin) {
                return $ebulletin->website->slug;
            })
            ->addColumn('action', function ($ebulletin) {
                return
                    '<select class="nice" onchange="locationOnChange(this.value);">
                    <option value="">' . trans("MPCorePanel::general.selection") . '</option>
                    <option value="' . route('Heraldist.ebulletin.delete', $ebulletin->id) . '">' . trans("MPCorePanel::general.delete") . '</option>
                </select>';
            })
            ->setRowId('tbl-{{$id}}')->make(true);
    }
}
