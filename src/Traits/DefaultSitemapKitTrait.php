<?php

namespace Mediapress\Traits;

use Mediapress\Exceptions\NotFoundException;
use Mediapress\Foundation\Mediapress;
use Mediapress\Models\CategoryDetail;
use Mediapress\Models\Language;
use Mediapress\Models\PageDetail;
use Mediapress\Modules\Content\Foundation\Slide;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Slider;

use Illuminate\View\View;
use Illuminate\Support\Collection;

/**
 * Trait DefaultSitemapKitTrait
 * @package Mediapress\Traits
 */
trait  DefaultSitemapKitTrait
{
    /**
     * @var Sitemap
     */
    private $sitemap;
    /**
     * @var string
     */
    private $sitemapType;
    /**
     * @var string
     */
    private $sitemapTypeName;
    /**
     * @var array
     */
    private $parameters;
    /**
     * @var Mediapress
     */
    private $mediapress;
    /**
     * @var int
     */
    private $category = 0;
    /**
     * @var int
     */
    private $criteria = 0;

    /**
     * @param array $parameters
     * @return void
     */
    private function set(array $parameters = []): void
    {
        $this->mediapress = mediapress();
        $this->parameters = $this->setParams($parameters);

        $this->sitemap = $this->mediapress->data['sitemap'] = $this->getSitemap();

        $this->sitemapCategory = (int) $this->sitemap->category;
        $this->sitemapCriteria = (int) $this->sitemap->criteria;
        $this->sitemapType = $this->sitemap->sitemapType->sitemap_type_type;
        $this->sitemapTypeName = $this->sitemap->sitemapType->name;
    }


    //region mainFunctions
    /**
     * @param array $parameters
     * @return View
     */
    protected function sitemapDetailFunc(array $parameters = []): View
    {
        $this->set($parameters);

        if ($this->sitemapType == 'dynamic') {
            if ($this->sitemapCategory == 1) {
                $this->mediapress->data['categories'] = $this->getSitemapCategories();
            }
            $this->mediapress->data['pages'] = $this->getSitemapPages();
        } else {
            if ($this->sitemap->feature_tag == 'homepage') {
                $this->mediapress->data['scenes'] = $this->getScenes();
            }
        }

        if(file_exists(resource_path('views/web/pages/' . mb_strtolower($this->sitemapTypeName)) . "/sitemap.blade.php")) {
            return view('web.pages.' . mb_strtolower($this->sitemapTypeName) . '.sitemap');
        }
        return view('web.' . mb_strtolower($this->sitemapTypeName) . '.sitemap');
    }

    /**
     * @param array $parameters
     * @return View
     */
    protected function pageDetailFunc(array $parameters = []): View
    {
        $this->set($parameters);

        $this->mediapress->data['page'] = $this->getPage();

        $this->mediapress->data['subPages'] = $this->getPageSubPages();

        if ($this->sitemapCategory == 1) {
            $this->mediapress->data['category'] = $this->getPageCategory();
            if ($this->sitemapCriteria == 1) {
                $this->mediapress->data['criterias'] = $this->getPageCriterias();
            }
        }

        if(file_exists(resource_path('views/web/pages/' . mb_strtolower($this->sitemapTypeName)) . "/page.blade.php")) {
            return view('web.pages.' . mb_strtolower($this->sitemapTypeName) . '.page');
        }
        return view('web.' . mb_strtolower($this->sitemapTypeName) . '.page');
    }

    /**
     * @param array $parameters
     * @return View
     */
    protected function categoryDetailFunc(array $parameters = []): View
    {
        $this->set($parameters);

        $this->mediapress->data['category'] = $this->getCategory();
        $this->mediapress->data['children'] = $this->getCategoryChildren();
        $this->mediapress->data['pages'] = $this->getCategoryPages();

        if ($this->sitemap->criteria == 1) {
            $this->mediapress->data['criterias'] = $this->getCategoryCriterias();
        }

        if ($this->mediapress->data['children']->count() > 0) {
            if(file_exists(resource_path('views/web/pages/' . mb_strtolower($this->sitemapTypeName)) . "/category_children.blade.php")) {
                return view('web.pages.' . mb_strtolower($this->sitemapTypeName) . '.category_children');
            }
            return view('web.' . mb_strtolower($this->sitemapTypeName) . '.category_children');
        }


        if(file_exists(resource_path('views/web/pages/' . mb_strtolower($this->sitemapTypeName)) . "/category.blade.php")) {
            return view('web.pages.' . mb_strtolower($this->sitemapTypeName) . '.category');
        }
        return view('web.' . mb_strtolower($this->sitemapTypeName) . '.category');
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    protected function filterCriteriaFunc(array $parameters = [])
    {

        $category_id = request()->get('category_id');
        $selected_ids = request()->get('selected_ids') ?: [];

        $category = Category::find($category_id);
        $sitemap = $category->sitemap;
        $pages = $category->pages()
            ->whereHas('detail')
            ->where('status', 1)
            ->with('detail.url');

        foreach ($selected_ids as $criteria) {
            $pages = $pages->whereHas('criterias', function ($q) use ($criteria) {
                $q->whereIn('id', $criteria);
            });
        }

        if (isset($parameters['paginate'])) {
            $pages = $pages->paginate($parameters['paginate']);
        } else {
            $pages = $pages->get();
        }


        if(file_exists(resource_path('views/web/pages/' . mb_strtolower($sitemap->sitemapType->name)) . "/category_ajax.blade.php")) {
            return view('web.pages.' . mb_strtolower($sitemap->sitemapType->name) . '.category_ajax', compact('pages'))->render();
        }
        return view('web.' . mb_strtolower($sitemap->sitemapType->name) . '.category_ajax', compact('pages'))->render();
    }
    //endregion

    //region sitemapDetailFunc
    /**
     * @return Sitemap
     */
    private function getSitemap(): Sitemap
    {

        $sitemap = $this->mediapress->sitemap();
        $params = $this->parameters['sitemap'] ?? [];

        if (isset($params['main'])) {
            $sitemap = $sitemap->select($params['main']);
        }

        if (isset($params['detail'])) {
            $sitemap = $sitemap->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        return $sitemap->first();
    }

    /**
     * @return mixed
     */
    private function getSitemapPages()
    {

        if (isset($this->parameters['pages'])) {
            $params = $this->parameters['pages'];
        } else {
            return collect();
        }

        $pages = $this->sitemap->pages()
            ->whereHas('detail')
            ->where('status', 1);

        if (isset($params['main'])) {
            $pages = $pages->select($params['main']);
        }

        if (isset($params['detail'])) {
            $pages = $pages->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['image'])) {
            $pages = $pages->with(['mfiles' => function ($q) use ($params) {
                $q->whereIn('file_key', $params['image']);
            }]);
        }

        if (isset($params['order']) && isset($params['order']['by'])) {
            $pages = $pages->orderBy($params['order']['by'], $params['order']['direction'] ?? 'asc');
        }

        if (isset($params['paginate'])) {
            return $pages->paginate($params['paginate']);
        }

        return $pages->get();
    }

    /**
     * @return Collection
     */
    private function getSitemapCategories(): Collection
    {
        if (isset($this->parameters['categories'])) {
            $params = $this->parameters['categories'];
        } else {
            return collect();
        }

        $categories = $this->sitemap->categories()
            ->where('status', 1)
            ->whereHas('detail')
            ->where('category_id', 0)
            ->orderBy('lft');

        if (isset($params['main'])) {
            $categories = $categories->select($params['main']);
        }

        if (isset($params['detail'])) {
            $categories = $categories->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['image'])) {
            $categories = $categories->with(['mfiles' => function ($q) use ($params) {
                $q->whereIn('file_key', $params['image']);
            }]);
        }

        if (isset($params['categoryPages']) && $params['categoryPages'] === true) {
            $categories = $categories->with(['pages' => function ($q) {
                $q->where('status', 1);
            }]);
        }

        if (isset($params['categoryChildren']) && $params['categoryChildren'] === true) {
            $categories = $categories->with(['children' => function ($q) {
                $q->where('status', 1)->orderBy('lft');
            }]);
        }

        return $categories->get();
    }
    //endregion


    //region pageDetailFunc
    /**
     * @return Page
     */
    private function getPage(): Page
    {
        $page = $this->mediapress->parent();
        $params = $this->parameters['page'] ?? [];

        if (isset($params['main'])) {
            $page = $page->select($params['main']);
        }

        if (isset($params['detail'])) {
            $page = $page->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        return $page->first();
    }

    /**
     * @return mixed
     */
    private function getPageSubPages()
    {
        $currentPage = $this->mediapress->data['page'];
        $params = $this->parameters['sub_pages'] ?? [];

        $pages = Page::where('page_id', $currentPage->id);

        if (isset($params['main'])) {
            $pages = $pages->select($params['main']);
        }

        if (isset($params['detail'])) {
            $pages = $pages->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['image'])) {
            $pages = $pages->with(['mfiles' => function ($q) use ($params) {
                $q->whereIn('file_key', $params['image']);
            }]);
        }

        if (isset($params['order']) && isset($params['order']['by'])) {
            $pages = $pages->orderBy($params['order']['by'], $params['order']['direction'] ?? 'asc');
        }

        if (isset($params['paginate'])) {
            return $pages->paginate($params['paginate']);
        }

        return $pages->get();
    }

    /**
     * @return Category|null
     */
    private function getPageCategory(): ?Category
    {
        if (isset($this->parameters['category'])) {
            $params = $this->parameters['category'];
        } else {
            return null;
        }

        $category = $this->mediapress->parent->category();

        if (isset($params['main'])) {
            $category = $category->select($params['main']);
        }

        if (isset($params['detail'])) {
            $category = $category->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['categoryPages']) && $params['categoryPages'] === true) {
            $category = $category->with(['pages' => function ($q) {
                $q->where('status', 1);
            }]);
        }

        return $category->first();
    }

    /**
     * @return array
     */
    private function getPageCriterias(): array
    {
        if (!isset($this->parameters['criterias'])) {
            return [];
        }
        $page = $this->mediapress->parent;
        $criterias = $page->criterias()
            ->whereHas('detail')
            ->whereHas('parent.detail')
            ->with(['parent', 'detail', 'children' => function ($q) {
                $q->with('detail');
            }])
            ->get();

        $hold = array();
        foreach ($criterias as $criteria) {
            $hold[$criteria->parent->detail->name] = $criteria->detail->name;
        }

        return $hold;
    }
    //endregion


    //region categoryDetailFunc
    /**
     * @return Category
     */
    private function getCategory(): Category
    {
        $category = $this->mediapress->parent();
        $params = $this->parameters['category'] ?? [];

        if (isset($params['main'])) {
            $category = $category->select($params['main']);
        }

        if (isset($params['detail'])) {
            $category = $category->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['categoryPages']) && $params['categoryPages'] === true) {
            $category = $category->with(['pages' => function ($q) {
                $q->where('status', 1);
            }]);
        }

        return $category->first();
    }

    /**
     * @return Collection
     */
    private function getCategoryChildren(): Collection
    {
        if (isset($this->parameters['category'])) {
            $params = $this->parameters['category'];
        } else {
            return collect();
        }

        $children = $this->mediapress->parent->children()
            ->where('status', 1)
            ->whereHas('detail')
            ->orderBy('lft');

        if (isset($params['main'])) {
            $children = $children->select($params['main']);
        }

        if (isset($params['detail'])) {
            $children = $children->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['categoryPages']) && $params['categoryPages'] === true) {
            $children = $children->with(['pages' => function ($q) {
                $q->where('status', 1);
            }]);
        }

        return $children->get();
    }

    /**
     * @return mixed
     */
    private function getCategoryPages()
    {
        if (isset($this->parameters['pages'])) {
            $params = $this->parameters['pages'];
        } else {
            return collect();
        }

        $pages = $this->mediapress->parent->pages()
            ->whereHas('detail')
            ->where('status', 1);

        if (isset($params['main'])) {
            $pages = $pages->select($params['main']);
        }

        if (isset($params['detail'])) {
            $pages = $pages->with(['detail' => function ($q) use ($params) {
                $q = $q->select($params['detail']);

                if (isset($params['url'])) {
                    $q = $q->with(['url' => function ($q) use ($params) {
                        $q->select($params['url']);
                    }]);
                }
                return $q;
            }]);
        }

        if (isset($params['image'])) {
            $pages = $pages->with(['mfiles' => function ($q) use ($params) {
                $q->whereIn('file_key', $params['image']);
            }]);
        }

        if (isset($params['order']) && isset($params['order']['by'])) {
            $pages = $pages->orderBy($params['order']['by'], $params['order']['direction'] ?? 'asc');
        }

        if (isset($params['paginate'])) {
            return $pages->paginate($params['paginate']);
        }


        return $pages->get();
    }

    /**
     * @return Collection
     */
    private function getCategoryCriterias(): Collection
    {
        if (!isset($this->parameters['criterias'])) {
            return collect();
        }

        $criterias = $this->mediapress->parent->criterias()
            ->whereHas('detail')
            ->with(['detail', 'children' => function ($q) {
                $q->with('detail');
            }])
            ->get();

        if ($criterias->count() == 0 && $this->mediapress->parent->parent) {
            $criterias = $this->mediapress->parent->parent->criterias()
                ->whereHas('detail')
                ->with(['detail', 'children' => function ($q) {
                    $q->with('detail');
                }])
                ->get();
        }

        return $criterias;
    }

    //endregion



    /**
     * @return array
     */
    private function getScenes(): array
    {
        $slider = Slider::first();

        $holder = [];
        if ($slider) {
            $scenes = $slider->scenes()
                ->orderBy('order')
                ->with('detail')
                ->get();

            foreach ($scenes as $scene) {
                $tempScene = new Slide([
                    'time' => $scene->detail->time,
                    'texts' => $scene->detail->texts,
                    'buttons' => $scene->detail->buttons,
                    'files' => $scene->detail->files,
                    'url' => $scene->detail->url,
                ]);

                if (is_null($tempScene->image()->id)) {
                    continue;
                }
                $temp = [
                    'image' => null,
                    'texts' => [],
                    'buttons' => [],
                    'urls' => [],
                ];
                $temp['image'] = $tempScene->image();

                for ($i = 1; $i <= $slider->text; $i++) {
                    $temp["texts"][] = $tempScene->text($i);
                }
                for ($j = 1; $j <= $slider->button; $j++) {
                    $temp["buttons"][] = $tempScene->button($j);
                    $temp["urls"][] = $tempScene->url('button', $j);
                }

                $holder[] = $temp;
            }
        }

        return $holder;
    }

    /**
     * @param $params
     * @return array
     */
    private function setParams($params): array
    {
        $hold = [];
        foreach ($params as $key => $param) {

            if (isset($param['select']) && count($param['select']) > 0) {
                foreach ($param['select'] as $ke => $para) {
                    if (strpos($para, 'detail') !== false) {
                        if (isset(explode('.', $para)[1])) {
                            $hold[$key]['detail'][] = explode('.', $para)[1];
                        }
                    } elseif (strpos($para, 'url') !== false) {
                        if (isset(explode('.', $para)[1])) {
                            $hold[$key]['url'][] = explode('.', $para)[1];
                        }
                    } else {
                        $hold[$key]['main'][] = $para;
                    }
                }

                if (isset($param['image'])) {
                    $hold[$key]['image'][] = $param['image'];
                }

                if (isset($param['paginate'])) {
                    $hold[$key]['paginate'] = $param['paginate'];
                }

                if (isset($param['categoryPages'])) {
                    $hold[$key]['categoryPages'] = $param['categoryPages'];
                }

                if (isset($param['order'])) {
                    $hold[$key]['order'] = $param['order'];
                }
            }
        }
        return $hold;
    }
}
