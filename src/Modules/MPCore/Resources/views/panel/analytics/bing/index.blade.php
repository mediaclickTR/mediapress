@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="dashboard">
        @include("MPCorePanel::inc.errors")
        <div class="page-top">
            <div class="title">
                {!! trans("MPCorePanel::general.google-analytics") !!}
            </div>
            <div class="filter">
                <select class="nice" onchange="locationOnChange(this.value);">
                    @php($request = request())
                    <option @if($request->days)  @if($request->days=='0') selected @endif @endif value="{!! url()->current() !!}?days='0'">{!! trans("MPCorePanel::analytics.today") !!}</option>
                    <option @if($request->days) @if($request->days==1) selected @endif @endif value="{!! url()->current() !!}?days=1">{!! trans("MPCorePanel::analytics.yesterday") !!}</option>
                    <option @if($request->days==7 || !$request->days) selected @endif value="{!! url()->current() !!}?days=7">{!! trans("MPCorePanel::analytics.week") !!}</option>
                    <option @if($request->days) @if($request->days==30) selected @endif @endif value="{!! url()->current() !!}?days=30">{!! trans("MPCorePanel::analytics.month") !!}</option>
                    <option @if($request->days) @if($request->days==90) selected @endif @endif value="{!! url()->current() !!}?days=90">{!! trans("MPCorePanel::analytics.three-month") !!}</option>
                    <option @if($request->days) @if($request->days==365) selected @endif @endif value="{!! url()->current() !!}?days=365">{!! trans("MPCorePanel::analytics.year") !!}</option>
                </select>
            </div>
        </div>
        <div class="content">
            <div class="pro-lists">
                <div class="top-filter">
                    <div class="right">
                            <a class="yellow-button" href="{!! route('Interaction.analytics.google.detailedAnalytics') !!}">
                               <i class="fa fa-bar-chart-o"></i> {!! trans("MPCorePanel::analytics.detailedAnalytics") !!}
                            </a>
                    </div>
                </div>
            </div>
            <div class="step-number">

                <div class="mc-md-3">
                    <div class="item">
                        <span class="timer" data-from="{!! $data['totalSessions']+100 !!}" id="duration" data-to="{!! $data['totalSessions'] !!}" data-speed="1000"></span>
                        <i>{!! trans("MPCorePanel::analytics.total-views") !!}</i>
                    </div>
                </div>

                <div class="mc-md-3">
                    <div class="item">
                        <span class="timer" data-from="{!! $data['users']+100 !!}" data-to="{!! $data['users'] !!}" data-speed="1000"></span>
                        <i>{!! trans("MPCorePanel::analytics.users") !!}</i>
                    </div>
                </div>

                <div class="mc-md-3">
                    <div class="item">
                        <span class="timer" data-from="{!! $data['averageTimeOnSite']+100 !!}" id="duration" data-to="{!! $data['averageTimeOnSite'] !!}" data-speed="1000"></span>
                        <i>{!! trans("MPCorePanel::analytics.average-session-time") !!}</i>
                    </div>
                </div>

                <div class="mc-md-3">
                    <div class="item">
                        <span class="timer" data-from="{!! $data['organicSearch']+100 !!}" data-to="{!! $data['organicSearch'] !!}" data-speed="1000"></span>
                        <i>{!! trans("MPCorePanel::analytics.organic-users") !!}</i>
                    </div>
                </div>
            </div>
            <div class="statics">
                <h3>{!! trans("MPCorePanel::analytics.general-analytics") !!}</h3>
                <div id='myChart'></div>
            </div>
        </div>

    </div>
@endsection

@push('styles')
    <style>
        .nice ul {
            z-index:999 !important;
        }
    </style>
@endpush

@push('styles')
    <style>
        .yellow-button
        {
            background: #f3dc19;
            color:#000 !important;
            font-size:20px;
            padding:10px 20px 10px 20px;
            border: 1px solid #f2f2f2;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            font-weight:bold;
        }
    </style>
@endpush

@push("scripts")
    <script src="{!! asset('vendor/mediapress/js/zingchart.min.js') !!}"></script>
    <script>
        /*
        var duration_data = $('#duration').data("to");
        $("#duration").text(duration_data);
        */
        var myConfig = {
            "type": "area",
            "plotarea": {
                "margin": "dynamic"
            },
            "scale-x": {
                "values": [
                    '',
                    @foreach($visitorAndPageViews as $day)
                        '{!! substr($day['date'],0,10) !!}',
                    @endforeach
                ],
                "line-style": "solid",
                "line-width": "1px",
                "line-color": "#5b5b5b",
                "guide": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#606060"
                },
                "tick": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#606060"
                }
            },
            "scale-y": {
                "min-value": 0,
                "max-value":{!! $max !!}, //max
                "step": 1,
                "format": "",
                "label": {
                    "font-color": "#5b5b5b"
                },
                "line-style": "solid",
                "line-width": "2px",
                "line-color": "#606060",
                "guide": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#e2e2e2"
                },
                "tick": {
                    "line-style": "solid",
                    "line-width": "1px",
                    "line-color": "#5b5b5b"
                }
            },
            "legend": {
                "layout": "float",
                "toggle-action": "remove",
                "shadow": 0,
                "adjust-layout": true,
                "align": "center",
                "vertical-align": "bottom",
                "marker": {
                    "type": "match",
                    "show-line": true,
                    "line-width": 4,
                    "shadow": "none"
                }
            },
            "series": [{
                "values": [
                    0,
                    @foreach($visitorAndPageViews as $visitor)
                        {!! $visitor['visitors'] !!},
                    @endforeach

                ],
                "text": '{!! trans("MPCorePanel::analytics.user") !!}',
                "line-color": "#2196f3",
                "marker": {
                    "background-color": "#fff",
                    "border-color": "#2196f3",
                    "border-width": "1px",
                    "shadow": false
                },
                "background-color": "#2196f3"
            }, {
                "values": [
                    0,
                    @foreach($visitorAndPageViews as $pageView)
                        {!! $pageView['pageViews'] !!},
                    @endforeach
                ],
                "text": '{!! trans("MPCorePanel::analytics.views") !!}',
                "line-color": "#12574b",
                "marker": {
                    "background-color": "#fff",
                    "border-color": "#1d8b80",
                    "border-width": "1px",
                    "shadow": false
                },
                "background-color": "blue"
            }],

        };


        zingchart.render({
            id: 'myChart',
            data: myConfig,
            height: '100%',
            width: '100%'
        });
    </script>
@endpush
