<?php

return [
    "sitemap_xml_files" => "Sitemap XML Files",
    "selected_sitemap_count" => "Selected Sitemap Count",
    "create_sitemap_xml" => "Create New Sitemap XML File",
    "update_sitemap_xml" => "Edit Sitemap XML File"
];
