@if(isset($field))

    <{!! $field['tag'] !!} {!! $field->attributes() !!} id="{!! $field['id'] !!}" >
    {!! langPart($field['content']) !!}
    @include('HeraldistWeb::forms.main',['fields'=>$field->children])
    </{!! $field['tag'] !!}>
@endif