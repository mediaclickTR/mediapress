<?php

return [
    'list_page_title'=>'Category, Criteria, Property Selection',
    'categories'=>'Categories',
    'classification'=>'Classification',
    'category'=>'Category',
    'property'=>'Property',
    'properties'=>'Properties',
    'criteria'=>'Criteria',
    'criterias'=>'Criterias',
    'name_category'=>'Category Name',
    'create_category'=>'Create Category',
    'edit_category'=>'Edit Category',
    'add_main_category'=> 'Add Main Category',
    'description_category'=>'Category Description',
    'not_found_sitemap' => 'Sitemap Not Found',
    'category_name'=>'Category Name',

    // TODO: [Seçkin] find the usage and re-factor the call of the translation with the context.
    'category.not.permission'=>' adlı sayfa yapısına category(kategori) özelliği kapatılmış. Kategori eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    'criteria.not.permission'=>' adlı sayfa yapısına criteria(kriter) özelliği kapatılmış. Kriter eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    'property.not.permission'=>' adlı site Haritasına property(özellik) özelliği kapatılmış. Özellik eklemek için lütfen sayfa yapıları bölümünden ilgili alanı düzenleyiniz.',
    // To-Do-End

    /*
     * Criteria
     */
    'create_criteria'=>'Create Criteria',
    'edit_criteria'=>'Edit Criteria',
    'name_criteria'=>'Criteria Name',

    /*
     * Property
     */
    'create_property'=>'Create Property',
    'edit_property'=>'Edit Property',
    'name_property'=>'Property Name',

    // TODO: [Seçkin] find the usage and re-factor the call of the translation with the context.
    'not_found_data'=>':type bulunamadı. Sağ üstteki "Yeni oluştur" butonundan :type ekleme işlemini yapabilirsiniz.'
    // To-Do-End
];
