@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="dashboard">
        @include("MPCorePanel::inc.errors")
        <div class="page-top">
            <div class="title">
                {!! trans("MPCorePanel::analytics.detailedAnalytics") !!}
            </div>
            <div class="filter">
                <select class="nice" onchange="locationOnChange(this.value);">
                    @php($request = request())
                    <option @if($request->days)  @if($request->days=='0') selected @endif @endif value="{!! url()->current() !!}?days='0'">{!! trans("MPCorePanel::analytics.today") !!}</option>
                    <option @if($request->days) @if($request->days==1) selected @endif @endif value="{!! url()->current() !!}?days=1">{!! trans("MPCorePanel::analytics.yesterday") !!}</option>
                    <option @if($request->days==7 || !$request->days) selected @endif value="{!! url()->current() !!}?days=7">{!! trans("MPCorePanel::analytics.week") !!}</option>
                    <option @if($request->days) @if($request->days==30) selected @endif @endif value="{!! url()->current() !!}?days=30">{!! trans("MPCorePanel::analytics.month") !!}</option>
                    <option @if($request->days) @if($request->days==90) selected @endif @endif value="{!! url()->current() !!}?days=90">{!! trans("MPCorePanel::analytics.three-month") !!}</option>
                    <option @if($request->days) @if($request->days==365) selected @endif @endif value="{!! url()->current() !!}?days=365">{!! trans("MPCorePanel::analytics.year") !!}</option>
                </select>
            </div>
        </div>
        <div class="content">
            @if(!$status)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.notfound_to_date") !!}</div>
            @endif
            <div class="pro-lists">
                <div class="col-md-12 pa">
                    <div class="lang-box">
                        <form action="#">
                            <div class="item" style="background: #f6f6f6 !important;" >
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.pageAnalytics") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="pageAnalytics"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.pageUrl") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pageTitle") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.views") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($pageAnalytics as $page)
                                                <tr>
                                                    <td>{!! $page['url'] !!}</td>
                                                    <td>{!! $page['title'] !!}</td>
                                                    <td>{!! $page['pageviews'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.traffic-sources") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="trafficSources"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.source") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sourceEngine") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.exits") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pageDepth") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.duration") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($trafficSources as $source)
                                                <tr>
                                                    <td>{!! $source['trafficSource'] !!}</td>
                                                    <td>{!! $source['sourceEngine'] !!}</td>
                                                    <td>{!! $source['visits'] !!}</td>
                                                    <td>{!! $source['bounceRate'] !!}</td>
                                                    <td>{!! $source['pageDepth'] !!}</td>
                                                    <td>{!! $source['avgVisitDurationSeconds'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.countries") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="countries"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.country") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.click") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($countries as $country)
                                                <tr>
                                                    <td>{!! $country['name'] !!}</td>
                                                    <td>{!! $country['y'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="mc-md-10 pa">
                                    <span>{!! trans("MPCorePanel::analytics.browsers") !!}</span>
                                </div>
                                <div class="mc-md-2 pa">
                                    <a href="#">{!! trans("MPCorePanel::general.detail") !!}</a>
                                </div>
                                <div class="mc-md-12 pa">
                                    <div class="edit">
                                        <div id="browsers"></div>
                                        <table>
                                            <tr>
                                                <th>{!! trans("MPCorePanel::analytics.browser") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.exits") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.pageDepth") !!}</th>
                                                <th>{!! trans("MPCorePanel::analytics.duration") !!}</th>
                                            </tr>
                                            <tr>
                                            </tr>
                                            @foreach($browsersAndSystems as $browser)
                                                <tr>
                                                    <td>{!! $browser['browser'] !!}</td>
                                                    <td>{!! $source['visits'] !!}</td>
                                                    <td>{!! $source['bounceRate'] !!}</td>
                                                    <td>{!! $source['pageDepth'] !!}</td>
                                                    <td>{!! $source['avgVisitDurationSeconds'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .nice ul {
            z-index:999 !important;
        }
        tr td {
            text-align:center;
        }

    </style>
@endpush

@push('scripts')
    <script src="{!! asset('vendor/mediapress/js/zingchart.min.js') !!}"></script>
    <script>
        $('a').click(function (e) {
            e.preventDefault();
        });

        var pageAnalytics = {
            "type": "line",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($pageAnalytics as $page)
                        "{!! $page['url'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($pageAnalytics as $page)
                            {!! $page['pageviews'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.views") !!}"
                }
            ]
        };
        zingchart.render({
            id: "pageAnalytics",
            data: pageAnalytics,
            height: "300",
            width: "100%"
        });

        var trafficSources = {
            "type": "bar",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($trafficSources as $source)
                        "{!! $source['sourceEngine'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($trafficSources as $source)
                        {!! $source['visits'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.pageviews") !!}"
                },
                {
                    "values": [
                        @foreach($trafficSources as $source)
                        {!! $source['bounceRate'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.exits") !!}"
                }
            ]
        };
        zingchart.render({
            id: "trafficSources",
            data: trafficSources,
            height: "300",
            width: "100%"
        });

    var countries = {
        "type": "heatmap",
        "plot": {
            "value-box": {
                "text": "%v"
            },
            "tooltip": {
                "text": "%v"
            }
        },
        "legend": {
            "toggle-action": "hide",
            "header": {
                "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
            },
            "item": {
                "cursor": "pointer"
            },
            "draggable": true,
            "drag-handler": "icon"
        },
        "scale-x": {
            "values": [
                @foreach($countries as $country)
                    "{!! $country['name'] !!}",
                @endforeach
            ]
        },
        "series": [
            {
                "values": [
                    @foreach($countries as $country)
                        {!! $country['y'] !!},
                    @endforeach
                ],
                "text": "{!! trans("MPCorePanel::analytics.click") !!}"
            }
        ]
    };
    zingchart.render({
        id: "countries",
        data: countries,
        height: "300",
        width: "100%"
    });

    var browsers = {
        "type": "area",
        "plot": {
            "value-box": {
                "text": "%v"
            },
            "tooltip": {
                "text": "%v"
            }
        },
        "legend": {
            "toggle-action": "hide",
            "header": {
                "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
            },
            "item": {
                "cursor": "pointer"
            },
            "draggable": true,
            "drag-handler": "icon"
        },
        "scale-x": {
            "values": [
                @foreach($browsersAndSystems as $browser)
                    "{!! $browser['browser'] !!}",
                @endforeach
            ]
        },
        "series": [
            {
                "values": [
                    @foreach($browsersAndSystems as $browser)
                    {!! $browser['visits'] !!},
                    @endforeach
                ],
                "text": "{!! trans("MPCorePanel::analytics.sessions") !!}"
            }
        ]
    };
    zingchart.render({
        id: "browsers",
        data: browsers,
        height: "300",
        width: "100%"
    });
    </script>
@endpush
