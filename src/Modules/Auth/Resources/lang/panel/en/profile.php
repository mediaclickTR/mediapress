<?php

return [
    "page_title" => "Profile Page",
    "information" => "My Information",
    "security" => "Security",
    "google_authenticator" => "Google Authenticator (2FA)",
    "active" => "Active",
    "passive" => "Passive",
    "activate" => "Activate",
    "deactivate" => "Cancel",
    "google2fa_warning" => "Show Google 2FA Alert"
];
