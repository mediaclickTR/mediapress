<?php

namespace Mediapress\Modules\MPCore\Foundation;

use Maatwebsite\Excel\Facades\Excel;
use Mediapress\Modules\Content\Imports\MetaImport;
use Mediapress\Modules\Content\Models\Meta;
use Mediapress\Modules\Content\Exports\MetaExport;
use Mediapress\Modules\MPCore\Models\Url;

final class MetaEngine
{
    const BASE_META_TITLE = 'title';
    const BASE_META_DESCRIPTION = 'description';
    const BASE_META_KEYWORDS = 'keywords';
    const STATIC_URL = "Mediapress\Modules\MPCore\Models\Url";
    const DYNAMIC_URL = "Mediapress\Modules\MPCore\Models\DynamicUrl";
    public const MODEL_TYPE = 'model_type';
    public const MODEL_ID = 'model_id';
    public const URL = 'url';
    public const ID = 'id';
    public const TYPE = 'type';
    public const MODEL = "model";
    public const ORIGINAL = 'original';
    public const CATEGORY = 'category';
    public const URL_ID = 'url_id';
    public const URL_TYPE = 'url_type';
    public const KEY = 'key';
    public const VALUE = 'value';
    public const DESKTOP_VALUE = 'desktop_value';
    public const MOBILE_VALUE = 'mobile_value';
    public const TEMPLATE = 'template';
    public const INDEXED = 'indexed';
    public const LANGUAGE = 'language';
    public const WEBSITE = 'website';
    public const URLS = 'urls';
    public const ORDER = 'order';
    public const INDEX_FOLLOW = 'index,follow';

    public const DESKTOP_TITLE = 'desktop_title';
    public const DESKTOP_KEYWORDS = 'desktop_keywords';
    public const DESKTOP_DESCRIPTION = "desktop_description";

    public const MOBILE_TITLE = 'mobile_title';
    public const MOBILE_KEYWORDS = 'mobile_keywords';
    public const MOBILE_DESCRIPTION = "mobile_description";
    public const VALUES = "values";

    public function renderMeta($url)
    {
        //dinamic url sorgulanıyor dinamik metalar için eğer sayfaya ait dinamik meta var ise onlar bulunuyor
        $dynamicUrl = null;
        if($url->dynamicUrl){
            foreach ($url->dynamicUrl as $dynUrl) {
                if (preg_match("/{$dynUrl->regex}/m", $this->request->fullUrl()) !== false) {
                    $dynamicUrl = $dynUrl;
                    break;
                }
            }
        }

        //normal metalar ezilebilmek için key ile guruplanıyor
        $metas = $url->metas->groupBy(self::KEY);
        if ($dynamicUrl != null) {
            //dinamik metalar var ise normal metalar eziliyor
            $metas = $metas->merge($dynamicUrl->metas->groupBy(self::KEY));
        }
        //metalar key grubundan geri döndürülüyor ve data daki meta bölümüne ekleniyor
        $metas = $metas->flatten();
        $metaBody = '';
        foreach ($metas as $meta){
            $metaBody .= $meta->render();
        }
        return $metaBody;
    }

    public function getUrls($website_id = null)
    {
        $data = [];

        $data ['index_types'] = [
            [
                self::KEY => "",
                self::VALUE => trans("MPCorePanel::general.selection"),
            ],
            [
                self::KEY => self::INDEX_FOLLOW,
                self::VALUE => trans("ContentPanel::seo.index.and.follow"),
            ],
            [
                self::KEY => "noindex",
                self::VALUE => trans("ContentPanel::seo.nofollow"),
            ],
            [
                self::KEY => "follow",
                self::VALUE => trans("ContentPanel::seo.follow"),
            ],
            [
                self::KEY => "nofollow",
                self::VALUE => trans("ContentPanel::seo.nofollow"),
            ],
            [
                self::KEY => "noarchive",
                self::VALUE => trans("ContentPanel::seo.noarchive"),
            ]
        ];

        if (count($data) > 0) {
            return [true, $data];
        } else {
            return [false,"Veri bulunamadı."];
        }
    }

    public function getUrlMetas($url_id)
    {
        $url = Url::with(self::MODEL)
                    ->where(self::ID, $url_id)
                    ->whereIn(self::TYPE, [self::ORIGINAL, self::CATEGORY])
                    ->groupBy(self::URL)
                    ->orderBy(self::URL)
                    ->select([self::ID, self::URL, self::MODEL_ID, self::MODEL_TYPE])
                    ->first();

        if ($url->model) {

            $urlMetas = $this->getUrlMetasWithId($url->id);

            if (!$urlMetas) {
                // Eğer meta kaydı yoksa default[title,description,keywords] ekle
                $newMeta = [
                    [
                        self::URL_ID => $url->id,
                        self::URL_TYPE => self::STATIC_URL,
                        self::KEY => self::BASE_META_TITLE,
                        self::DESKTOP_VALUE => $url->model->name,
                        self::TYPE => self::DESKTOP_DESCRIPTION,
                        self::TEMPLATE => $this->determineTemplate(self::BASE_META_TITLE)
                    ],
                    [
                        self::URL_ID => $url->id,
                        self::URL_TYPE => self::STATIC_URL,
                        self::KEY => self::BASE_META_DESCRIPTION,
                        self::DESKTOP_VALUE => $url->model->name,
                        self::TYPE => self::DESKTOP_DESCRIPTION,
                        self::TEMPLATE => $this->determineTemplate(self::BASE_META_DESCRIPTION)
                    ],
                    [
                        self::URL_ID => $url->id,
                        self::URL_TYPE => self::STATIC_URL,
                        self::KEY => self::BASE_META_KEYWORDS,
                        self::DESKTOP_VALUE => strtolower($url->model->name),
                        self::TYPE => self::DESKTOP_DESCRIPTION,
                        self::TEMPLATE => $this->determineTemplate(self::BASE_META_KEYWORDS)
                    ]
                ];

                $insertMetas = Meta::insert($newMeta);
                if ($insertMetas) {
                    // refresh data
                    $urlMetas = $this->getUrlMetasWithId($url->id);
                }

            } else {
                // default[title,description,keywords] keys check
                $checkTitleKey = Meta::where(self::URL_ID, $url->id)->where(self::KEY, self::BASE_META_TITLE)->first();
                if (!$checkTitleKey) {
                    Meta::insert([self::URL_ID => $url->id, self::URL_TYPE => self::STATIC_URL, self::KEY => self::BASE_META_TITLE, self::DESKTOP_VALUE => $url->model->name]);
                }
                $checkDescriptionKey = Meta::where(self::URL_ID, $url->id)->where(self::KEY, self::BASE_META_DESCRIPTION)->first();
                if (!$checkDescriptionKey) {
                    Meta::insert([self::URL_ID => $url->id, self::URL_TYPE => self::STATIC_URL, self::KEY => self::BASE_META_DESCRIPTION, self::DESKTOP_VALUE => $url->model->name]);
                }
                $checkKeywordsKey = Meta::where(self::URL_ID, $url->id)->where(self::KEY, self::BASE_META_KEYWORDS)->first();
                if (!$checkKeywordsKey) {
                    Meta::insert([self::URL_ID => $url->id, self::URL_TYPE => self::STATIC_URL, self::KEY => self::BASE_META_KEYWORDS, self::DESKTOP_VALUE => $url->model->name]);
                }
                // refresh data
                $data['metas'] = $this->getUrlMetasWithId($url->id);
            }

            if ($urlMetas) {
                return [true, $urlMetas];
            } else {
                return [false, "Veri alınamadı"];
            }

        } else {
            return [false, $url->model_type . ' adlı modele ait veri bulunamadı'];
        }
    }

    public function getUrlMetasWithId($url_id)
    {
        $urlMetas = [];
        $metas = Meta::with(self::URL)->where(self::URL_ID, $url_id)->get();
        if (count($metas) > 0) {
            foreach ($metas as $key => $meta) {
                $urlMetas[$key][self::ID] = $meta->id;
                $urlMetas[$key][self::KEY] = $meta->key;
                $urlMetas[$key][self::DESKTOP_VALUE] = $meta->desktop_value;
                $urlMetas[$key][self::INDEXED] = $meta->indexed;
            }
            return $urlMetas;
        } else {
            return false;
        }
    }

    public function getMetaTypes()
    {
        return [
            [
                self::KEY =>"",
                self::VALUE =>trans("MPCorePanel::general.selection")
            ],
            [
                self::KEY => self::BASE_META_TITLE,
                self::VALUE => trans("ContentPanel::seo.title")
            ],
            [
                self::KEY => self::BASE_META_DESCRIPTION,
                self::VALUE => trans("ContentPanel::seo.description")
            ],
            [
                self::KEY => self::BASE_META_KEYWORDS,
                self::VALUE => trans("ContentPanel::seo.keywords")
            ],
            [
                self::KEY =>"og:title",
                self::VALUE => trans("ContentPanel::seo.og.title")
            ],
            [
                self::KEY =>"og:description",
                self::VALUE => trans("ContentPanel::seo.og.description")
            ],
            [
                self::KEY =>"og:keywords",
                self::VALUE => trans("ContentPanel::seo.og.keywords")
            ]
        ];
        //return config("meta_types");
    }

    public function storeMeta($data)
    {
        $checkMetaKey = Meta::where(self::KEY, $data[self::KEY])->where(self::URL_ID, $data[self::URL_ID])->first();
        if(!$checkMetaKey){

            $template = $this->determineTemplate($data[self::KEY]);

            $meta = [
                self::URL_ID => $data[self::URL_ID],
                self::URL_TYPE => self::STATIC_URL,
                self::KEY => $data[self::KEY],
                self::TYPE => self::DESKTOP_DESCRIPTION,
                self::TEMPLATE => $template,
            ];
            foreach ($data['values'] as $key=>$val){
                $meta[$key] = $val;
            }

            $store = Meta::insert($meta);
            if ($store) {
                return [true, $store];
            } else {
                return [false, "Veri eklenemedi"];
            }

        }else{
            return [false, "Böyle bir key kayıtlı olduğu için veri eklenemedi."];
        }

    }

    public function determineTemplate($key)
    {
        if ($key == self::DESKTOP_TITLE || $key==self::MOBILE_TITLE){
            $template = '<title>$1</title>';
        }else{
            $template = '<meta name="$0" content="$1">';
        }

        return $template;
    }

    public function determineUrlType($type)
    {
        $data = [];
        if ($type == self::STATIC_URL) {
            $data = [
                self::TYPE => "Static",
                self::MODEL_TYPE => self::STATIC_URL,
            ];
        } else if ($type == self::DYNAMIC_URL) {
            $data = [
                self::TYPE => "Dynamic",
                self::MODEL_TYPE => self::DYNAMIC_URL,
            ];
        }
        return $data;
    }

    public function excelExport($website_id = null)
    {
        $urls = new Url();

        if ($website_id) {
            $urls = $urls->where("website_id", $website_id);
        }

        $metas = Meta::get()->groupBy(self::KEY);

        foreach ($metas as $key=>$value){
            if ($key =="parent") continue;
            $metaGroups [] = $key;
        }

        $urls = $urls->whereIn(self::TYPE, [self::ORIGINAL, self::CATEGORY])->with(self::MODEL)->get();
        $data = [];
        foreach ($urls as $urlKey=>$url) {

            if ($url->metas){
                    $data[$urlKey] = [
                        self::ID => $url->id,
                        self::URL => $url->url,
                        self::LANGUAGE => (isset($url->model->language) ? $url->model->language->code : '-')
                    ];

                    // urle ait metaların kolonlara eklenmesi
                    $addedColumns = [];
                    foreach ($url->metas as $meta){
                        if (in_array($meta->key, $metaGroups)){
                            $added [] = $meta->key;
                            $data[$urlKey] += [$meta->key=>$meta->desktop_value];
                        }

                    }


                    // eklenmeyen kolonların listeye eklenip boş bırakılması
                    $notAddedColoums = array_diff($metaGroups, $addedColumns);
                    foreach ($notAddedColoums as $notAddedColoum) {
                        // Eğer title description ve keywords alanları yok ise model adına göre dahil et
                        if (
                            $notAddedColoum == self::DESKTOP_TITLE || $notAddedColoum == self::DESKTOP_DESCRIPTION || $notAddedColoum == self::DESKTOP_KEYWORDS ||
                            $notAddedColoum == self::MOBILE_TITLE || $notAddedColoum == self::MOBILE_DESCRIPTION || $notAddedColoum == self::MOBILE_KEYWORDS
                        ){
                            $modelName = (isset($url->model)) ? $url->model->name : '';
                            $data[$urlKey] += [$notAddedColoum=> $modelName];
                        }else{
                            $data[$urlKey] += [$notAddedColoum => ''];
                        }
                    }
            }
        }

        $data = collect($data);
        $date =  \Carbon\Carbon::parse(date("d-m-Y"))->formatLocalized("%Y-%m-%d");

        return Excel::download(new MetaExport($data),  trans("ContentPanel::seo.metas").' '.$date.'.xlsx');
    }


    public function excelImport($data)
    {
        Excel::import(new MetaImport,$data['excel']);

        return true;
    }

    #region List

    public function metasQuickUpdate($data)
    {
        //dd($data);
        foreach ($data as $urlId => $metas) {
            foreach ($metas as $key => $value) {
                if ($key != self::INDEXED && $key != self::ORDER) {
                    $update = Meta::where(self::URL_ID, $urlId)->where(self::KEY, $key)->first();
                    if ($update) {
                        foreach ($value as $device_key=>$device_val){
                            $update->update([$device_key => $device_val]);
                        }
                    }
                    else{
                        $data = [
                            self::URL_ID =>$urlId,
                            self::KEY =>$key,
                            self::VALUES =>$value
                        ];
                        $this->storeMeta($data);
                    }
                }else{
                    // indexed update
                    $update = Meta::where(self::URL_ID,$urlId)->first();
                    if ($update){
                        $update->update(["indexed"=>$value]);
                    }
                }
            }
        }
        return [true, 'İşlem başarıyla yapıldı'];
    }

    public function listFilter($data)
    {

        $website_id = $data['website'];
        $language_code = $data['language'];
        $url = $data['url'];
        $desktop_title = $data['desktop_title'];
        $desktop_description = $data['desktop_description'];
        $indexed = $data['indexed'];


        $urls = Url::whereIn(self::TYPE, [self::ORIGINAL, self::CATEGORY])
            ->whereHasMorph('model', "*")
            ->with('model', 'website', 'metas');

        if ($website_id) {
            $urls = $urls->where("website_id", $website_id);
        }

        if ($language_code) {
            $urls = $urls->whereHasMorph("model", "*", function($query) use($language_code) {
                $query->whereHas('language', function($q) use($language_code) {
                    $q->where('code', $language_code);
                });
            });
        }

        if ($url) {
            $urls = $urls->where("url", 'like', '%'.$url.'%');
        }

        if ($desktop_title) {
            $urls = $urls->whereHas("metas", function($query) use($desktop_title) {
                $query->where('desktop_enabled', 1)
                    ->where('key', 'title')
                    ->where('desktop_value', 'like', '%'.$desktop_title.'%');

            });
        }

        if ($desktop_description) {
            $urls = $urls->whereHas("metas", function($query) use($desktop_description) {
                $query->where('desktop_enabled', 1)
                    ->where('key', 'description')
                    ->where('desktop_value', 'like', '%'.$desktop_description.'%');

            });
        }

        if ($indexed) {
            $urls = $urls->whereHas("metas", function($query) use($indexed) {
                $query->where('indexed', $indexed);

            });
        }


        $urlCount = $urls->count();
        $pageSize = request()->get('filter.pageSize') ?: 20;
        $urls = $urls->paginate($pageSize, ["*"], 'filter.pageIndex');

        $data = [
            self::URLS => []
        ];

        foreach ($urls->groupBy('model.language_id') as $lang_code=>$groups) {
            foreach ($groups as $url){
                $data [self::URLS][] = [
                    self::ID => $url->id,
                    self::WEBSITE => $url->website->slug,
                    self::URL => $url->url,

                    self::DESKTOP_TITLE => (is_null($url->metas->title->desktop_value)) ? (isset($url->model) && $url->model->name) ? $url->model->name  : '-' :  $url->metas->title->desktop_value,
                    self::DESKTOP_DESCRIPTION =>(is_null($url->metas->description->desktop_value)) ? (isset($url->model) && $url->model->name)  ? $url->model->name : '-' :  $url->metas->description->desktop_value,

                    self::LANGUAGE => (isset($url->model->language) ? $url->model->language->code : '-'),
                    self::INDEXED =>  ($url->meta) ? $url->meta->indexed : self::INDEX_FOLLOW,
                ];
            }
        }

        return ['list' => $data['urls'], 'count' => $urlCount];
    }

    public function listUpdate($data)
    {
        foreach ($data as $key => $value) {
            $update = Meta::where(self::ID, $key)->update($value);
            if ($update) {
                return [true, $update];
            } else {
                return [false, "Veri düzenlenemedi"];
            }
        }
    }

    public function listDelete($item_id)
    {
        $delete = Meta::find($item_id)->delete();

        if ($delete) {
            return [true, $delete];
        } else {
            return [false, "Veri Silinemedi"];
        }
    }

    #endregion

}
