@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                Yorum Yapıları
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('CommentStructure.create') !!}" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    {!! trans("ContentPanel::general.add_new") !!}
                </a>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Durum</th>
                <th>Yorum Yapısı Adı</th>
                <th>Yorum Yapısı Key</th>
                <th>Ekleme Tarihi</th>
                <th>Ekleyen</th>
                <th>İşlemler</th>
            </tr>
            </thead>
            <tbody>
                @foreach($commentStructures as $commentStructure)
                    <tr>
                        <td>{!! $commentStructure->id !!}</td>
                        <td class="status">
                            @if($commentStructure->status == 1)
                                <i class="active" style="background: #7fcc46"></i> {{ "Yayında" }}
                            @elseif($commentStructure->status == 4)
                                <i class="passive" style="background: #dc3545"></i> {{ "İleri Tarihli" }}
                            @else
                                <i class="passive" style="background: #dc3545"></i> {{ "Pasif" }}
                            @endif
                        </td>
                        <td>{!! $commentStructure->name !!}</td>
                        <td>{!! $commentStructure->key !!}</td>
                        <td>{!! $commentStructure->created_at !!}</td>
                        <td>{!! optional($commentStructure->admin)->username !!}</td>
                        <td>
                            <a href="{!! route('CommentStructure.comments.index', ['commentStructureId' => $commentStructure->id]) !!}" class="mr-2" title="Yorumlar">
                                <i class="fa fa-comments"></i>
                            </a>
                            <a href="{!! route('CommentStructure.edit', ['id' => $commentStructure->id]) !!}" class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('CommentStructure.delete', ['id' => $commentStructure->id]) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! trans("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
