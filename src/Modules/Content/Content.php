<?php

namespace Mediapress\Modules\Content;

use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Mediapress\Contracts\ContentInterface;
use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;
use Mediapress\Models\MPModule;
use Mediapress\Modules\Content\Exceptions\PageRenderableNotFoundException;
use Mediapress\Modules\Content\Exceptions\SitemapRenderableNotFoundException;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\CriteriaDetail;
use Mediapress\Modules\Content\Models\LanguageWebsite;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\PanelMenu;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\PropertyDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Website;
use Illuminate\Support\Facades\File;
use Mediapress\Modules\Content\Models\WebsiteDetail;
use Illuminate\Support\Str;
use App;
use DB;

class Content extends MPModule //implements ContentInterface
{
    const UNKNOWN_STR = "unknown";
    public const SUBMENU = "submenu";
    public const TITLE = "title";
    public const STANDARD = "standard";
    public const BELONGS_TO = "belongs_to";
    public const TYPE = "type";
    public const INTERPRETATION = "interpretation";
    public const VALUE = "value";
    public const DESCRIPTION = "description";
    public const STATUS = "status";
    public const COUNTRY_GROUP_ID = "country_group_id";
    public const VARIABLES = "variables";
    public const KEY = "key";
    public const MODEL = "model";
    public const SEO_DESCRIPTION_LENGTH_FOR_DESKTOP = "seo_description_length_for_desktop";
    public const DIRECT_VALUE = "direct_value";
    public const MEDIAPRESS = "Mediapress";
    public const APP = "App";
    public const SITEMAP = "sitemap";
    public const LANGUAGE_ID = "language_id";
    public const CONTENT = "Content";

    public $name = self::CONTENT;
    public $url = "mp-admin/Content";
    public $description = "Core content manager of Mediapress";
    public $author = "";
    public $menus = [];
    private $plugins = [];

    public function vowelStringReplace($string)
    {
        $vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U", " "];
        return strtolower(substr(str_replace($vowels, "", $string), 0, 3));
    }

    public function fillMenu($menu)
    {
        data_set($menu, 'header_menu.content.cols.modules.name', trans("ContentPanel::menu_titles.modules"));
        $content_cols_modules_rows_set = [
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.categories"),
                "url" => route("Content.categories.index"),
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.menus"),
                "url" => route("Content.menus.index"),
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.social_media"),
                "url" => route("Content.socialmedia.index"),
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.slider"),
                "url" => route("Content.slider.index"),
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.popup"),
                "url" => route("Content.popup.index"),
            ]
        ];
        $menu = dataGetAndMerge($menu, 'header_menu.content.cols.modules.rows', $content_cols_modules_rows_set);



        #region ShowCase Sitemap
        $showcase_sitemap = Sitemap::whereHas('websites',
            function ($query) {
                $query->where('id', session("panel.website.id"));
            })
            ->with('detail', 'sitemapType')
            ->whereHas('detail')
            ->where("featured_in_panel_menu", 1)
            ->first();

        if ($showcase_sitemap) {
            data_set($menu, 'header_menu.content.cols.show_case.name', $showcase_sitemap->detail->name);
            $content_cols_show_case_rows_set[] = [
                self::TYPE => self::SUBMENU,
                self::TITLE => $showcase_sitemap->detail->name,
                "url" => $showcase_sitemap->sitemapType->sitemap_type_type == "static" ? route("Content.sitemaps.mainEdit", $showcase_sitemap->id) : route("Content.pages.index", $showcase_sitemap->id)
            ];

            if ($showcase_sitemap->sitemapType->sitemap_type_type == "static") {

                $subSitemaps = Sitemap::whereHas('websites',
                    function ($query) {
                        $query->where('id', session("panel.website.id"));
                    })
                    ->where("sitemaps.sitemap_id", $showcase_sitemap->id)
                    ->with('detail', 'sitemapType')
                    ->whereHas('detail')
                    ->get();

                if ($subSitemaps->count() > 0) {
                    foreach ($subSitemaps as $subSitemap) {
                        $content_cols_show_case_rows_set[] = [
                            self::TYPE => self::SUBMENU,
                            self::TITLE => $subSitemap->detail->name,
                            "url" => $subSitemap->sitemapType->sitemap_type_type == "static" ? route("Content.sitemaps.mainEdit", $subSitemap->id) : route("Content.pages.index", $subSitemap->id)
                        ];
                    }
                }
            }

            if ($showcase_sitemap->category == 1) {
                $content_cols_show_case_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans("ContentPanel::menu_titles.categories"),
                    "url" => route("Content.categories.category.create", $showcase_sitemap->id)
                ];
            }

            if ($showcase_sitemap->criteria == 1) {
                $content_cols_show_case_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans("ContentPanel::menu_titles.criterias"),
                    "url" => route("Content.categories.criteria.create", $showcase_sitemap->id)
                ];
            }

            if ($showcase_sitemap->property == 1) {
                $content_cols_show_case_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans("ContentPanel::menu_titles.properties"),
                    "url" => route("Content.categories.property.create", $showcase_sitemap->id)
                ];
            }

            $menu = dataGetAndMerge($menu, 'header_menu.content.cols.show_case.rows', $content_cols_show_case_rows_set);

        }
        #endregion


        #region Sitemaps
        $sitemaps = Sitemap::whereHas('websites',
            function ($query) {
                $query->where('id', session("panel.website.id"));
            })
            ->with('detail', 'sitemapType')
            ->whereHas('detail')
            ->where("show_in_panel_menu", 1)
            ->where("featured_in_panel_menu", 0)
            ->get();

        $parentPanelMenus = PanelMenu::where('website_id', session('panel.website')->id)->get();

        data_set($menu, 'header_menu.content.cols.sitemaps.name', trans("ContentPanel::menu_titles.sitemaps"));
        $content_cols_sitemaps_rows_set = [];
        $panelSitemapIds = [];


        foreach ($parentPanelMenus as $panelMenu) {
            $panelMenus = $panelMenu->details()
                ->whereHas('sitemap')
                ->with(['sitemap', 'children' => function ($q) {
                    $q->orderBy('lft')->whereHas('sitemap');
                }])->orderBy('lft')->get();

            $hold = [
                self::TYPE => self::SUBMENU,
                self::TITLE => Str::title($panelMenu->name),
                "url" => "javascript:void(0);"
            ];

            foreach ($panelMenus as $panelm) {
                $panelSitemapIds[] = $panelm->sitemap->id;
                $hold['children'][] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => Str::title($panelm->sitemap->detail->name),
                    "url" => $panelm->sitemap->sitemapType->sitemap_type_type == "static" ? route("Content.sitemaps.mainEdit", $panelm->sitemap->id) : route("Content.pages.index", $panelm->sitemap->id)
                ];
            }
            $content_cols_sitemaps_rows_set[] = $hold;
        }

        $panelSitemapIds = array_unique($panelSitemapIds);


        foreach ($sitemaps as $sitemap) {
            if (in_array($sitemap->id, $panelSitemapIds)) {
                continue;
            }
            $content_cols_sitemaps_rows_set[] =
                [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => Str::title($sitemap->detail->name),
                    "url" => $sitemap->sitemapType->sitemap_type_type == "static" ? route("Content.sitemaps.mainEdit", $sitemap->id) : route("Content.pages.index", $sitemap->id)
                ];
        }

        $menu = dataGetAndMerge($menu, 'header_menu.content.cols.sitemaps.rows', $content_cols_sitemaps_rows_set);
        #endregion




        #region Structure
        data_set($menu, 'header_menu.settings.cols.structure.name', trans("ContentPanel::menu_titles.structure"));
        $modules_cols_structure_rows_set = [
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.websites"),
                "url" => route("Content.websites.index"),
            ],
        ];

        if(request()->server->get('REMOTE_ADDR') == "127.0.0.1" || \Str::startsWith(request()->server->get('REMOTE_ADDR'), "192.168.1.") || isMCAdmin()) {
            $modules_cols_structure_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => trans("ContentPanel::menu_titles.sitemaps"),
                    "url" => route("Content.sitemaps.index"),
                ];
        }
        $menu = dataGetAndMerge($menu, 'header_menu.settings.cols.structure.rows', $modules_cols_structure_rows_set);
        #endregion Structure


        #region SEO
        $modules_cols_seo_rows_set = [
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.seo"),
                "url" => route("Content.seo.metas")
            ],
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.meta_templates"),
                "url" => route("Content.meta_templates.index")
            ],
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.image_tags"),
                "url" => route("Content.image_tags.index")
            ],
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.sitemap_xml_editor"),
                "url" => route("Content.seo.sitemap_xmls.index"),
            ],
/*
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.seo_virtual"),
                "url" => 'javascript:void(0);',
            ],
*/
            [
                "type" => self::SUBMENU,
                self::TITLE => trans("ContentPanel::menu_titles.robots"),
                "url" => route("Content.seo.robot")
            ],
        ];
        $menu = dataGetAndMerge($menu, 'header_menu.settings.cols.seo.rows', $modules_cols_seo_rows_set);
        #endregion SEO



        $settings_cols_system_rows_set = [
            [
                self::TYPE => self::SUBMENU,
                self::TITLE => trans('ContentPanel::menu_titles.panel_menus'),
                "url" => route('Content.panelmenus.index'),
            ]
        ];
        $menu = dataGetAndMerge($menu, 'header_menu.settings.cols.system.rows', $settings_cols_system_rows_set);


        #region Header Menu > Sites > Websites lists > Set
        // Tab Name Set

        $current_website = session()->get('panel.website');

        $websites = Website::where("id", "!=", $current_website->id)
            ->where(self::TYPE, "!=", "alias")
            ->where(self::STATUS, "!=", 0)
            ->where(self::STATUS, Website::ACTIVE)
            ->orderBy("sort", "ASC")->get();


        if ($websites->count() > 0) {
            data_set($menu, 'header_menu.sites.cols.website_lists.name', $current_website->name);
            $sites_websites_lists_set = [];
            foreach ($websites as $website) {
                $domain = ($website->ssl == 1) ? "https://$website->slug" : "http://$website->slug";
                $sites_websites_lists_set [] =
                    [
                        self::TYPE => self::SUBMENU,
                        self::TITLE => $website->name,
                        "url" => $domain,
                        'target' => $website->target,
                        'id' => $website->id
                    ];
            }

            return dataGetAndMerge($menu, 'header_menu.sites.cols.website_lists.rows', $sites_websites_lists_set);
        }

        return $menu;
        #endregion
    }

    public function getPanelMenus()
    {
        return PanelMenu::get();
    }

    public function websites()
    {
        return Website::get();
    }

    public function internalWebsites()
    {
        return Website::where("target", "internal")->where(self::STATUS, 1)->get();
    }


    public function getPageRenderableClassPath($page_or_page_id, $for_existing_page = true, $inexistent_page_sitemap_type = self::UNKNOWN_STR)
    {

        if ($for_existing_page) {
            $page = is_a($page_or_page_id, Page::class) ? $page_or_page_id : Page::find($page_or_page_id);
            if (!$page) {
                throw new InvalidArgumentException("Page bulunamadı: $page_or_page_id");
            }
            $page_specific = "Page_" . $page->id;
            $sitemap_specific = $page->sitemap->sitemapType->name;
        } else {
            $page_specific = null;
            $sitemap_specific = $inexistent_page_sitemap_type;
        }

        $renderable_class = AllBuilderEngine::getBuilderClassPath(array_filter([$page_specific, "Pages"]), self::CONTENT . "\\Website" . session("panel.website.id"), self::APP, $sitemap_specific);
        if (!isset($renderable_class[0]) || !$renderable_class[0]) {
            throw new PageRenderableNotFoundException(/*"<strong><code>" . */
                implode("\n"/*."<br/>"*/, $renderable_class[1]) . /*"</code></strong>\n<br/>n<br/>".*/
                " denemeleri  başarısız oldu.", $renderable_class[1][0]);
        } else {
            $renderable_class = $renderable_class[1];
        }
        return $renderable_class;

    }

    public function getPageStorerClassPath($page_or_page_id)
    {

        /*$page = is_a($page_or_page_id, Page::class) ? $page_or_page_id : Page::find($page_or_page_id);
        if (!$page) {
            throw new \Exception("Page bulunamadı: $page_or_page_id");
        }
        $page_specific = "Page_" . $page->id;
        $sitemap_specific = $page->sitemap->sitemapType->name;


        $storer_class = AllBuilderEngine::getStorerClassPath([$page_specific, "Pages"], self::CONTENT, self::APP, $sitemap_specific);
        if (!$storer_class[0]) {
            $storer_class_2 = AllBuilderEngine::getStorerClassPath(["PagesStorer"], self::CONTENT, [self::APP, self::MEDIAPRESS], null);
            if (!$storer_class_2[0]) {
                return "<strong> <code>" . implode("\n <br/>", array_merge($storer_class[1], $storer_class_2[1])) . "</code> </strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
            } else {
                $storer_class = $storer_class_2[1];
            }
        } else {
            $storer_class = $storer_class[1];
        }

        return $storer_class;*/
        return $this->getSitemapChildObjectStorerClassPath(Page::class, "Page", $page_or_page_id);

    }

    public function getSitemapRenderableClassPath($sitemap_or_sitemap_id)
    {

        $sitemap = is_a($sitemap_or_sitemap_id, Sitemap::class) ? $sitemap_or_sitemap_id : Sitemap::find($sitemap_or_sitemap_id);
        if (!$sitemap) {
            throw new \Exception("Sitemap bulunamadı: $sitemap_or_sitemap_id");
        }

        $sitemap_specific = $sitemap->sitemapType->name;

        $renderable_class = AllBuilderEngine::getBuilderClassPath(["Sitemap"], [self::CONTENT . "\\Website" . $sitemap->website->id, self::CONTENT], [self::APP, "Mediapress"], $sitemap_specific);
        if (!isset($renderable_class[0]) || !$renderable_class[0]) {
            throw new SitemapRenderableNotFoundException(/*"<strong><code>" . */
                implode("\n"/*."<br/>"*/, $renderable_class[1]) . /*"</code></strong>\n<br/>n<br/>".*/
                " denemeleri başarısız oldu.", $renderable_class[1]);
        } else {
            $renderable_class = $renderable_class[1];
        }
        return $renderable_class;

    }

    public function getSitemapStorerClassPath($sitemap_or_sitemap_id)
    {
        $sitemap = is_a($sitemap_or_sitemap_id, Sitemap::class) ? $sitemap_or_sitemap_id : Sitemap::find($sitemap_or_sitemap_id);
        if (!$sitemap) {
            throw new \Exception("Sitemap bulunamadı: $sitemap_or_sitemap_id");
        }

        $sitemap_specific = $sitemap->sitemapType->name;


        $storer_class = AllBuilderEngine::getStorerClassPath(["SitemapStorer"], self::CONTENT . "\\Website" . $sitemap->website->id, self::APP, $sitemap_specific, null);
        if (!$storer_class[0]) {
            $storer_class_2 = AllBuilderEngine::getStorerClassPath(["SitemapStorer"], self::CONTENT, [self::APP, self::MEDIAPRESS], null);
            if (!$storer_class_2[0]) {
                return "<strong><code>" . implode("\n<br/>", array_merge($storer_class[1], $storer_class_2[1])) . "</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
            } else {
                $storer_class = $storer_class_2[1];
            }
        } else {
            $storer_class = $storer_class[1];
        }

        return $storer_class;

    }

    public function getCategoryRenderableClassPath($cat_or_cat_id, $for_existing_cat = true, $inexistent_cat_sitemap_type = self::UNKNOWN_STR)
    {
        return $this->getSitemapChildObjectRenderableClassPath(Category::class, "Category", $cat_or_cat_id, $for_existing_cat, $inexistent_cat_sitemap_type);
    }

    public function getCategoryStorerClassPath($cat_or_cat_id)
    {
        return $this->getSitemapChildObjectStorerClassPath(Category::class, "Category", $cat_or_cat_id);
    }

    public function getCriteriaRenderableClassPath($cri_or_cri_id, $for_existing_cri = true, $inexistent_cri_sitemap_type = self::UNKNOWN_STR)
    {
        return $this->getSitemapChildObjectRenderableClassPath(Criteria::class, "Criteria", $cri_or_cri_id, $for_existing_cri, $inexistent_cri_sitemap_type);
    }

    public function getCriteriaStorerClassPath($cri_or_cri_id)
    {
        return $this->getSitemapChildObjectStorerClassPath(Criteria::class, "Criteria", $cri_or_cri_id);
    }

    public function getPropertyRenderableClassPath($prop_or_prop_id, $for_existing_prop = true, $inexistent_prop_sitemap_type = self::UNKNOWN_STR)
    {
        return $this->getSitemapChildObjectRenderableClassPath(Property::class, "Property", $prop_or_prop_id, $for_existing_prop, $inexistent_prop_sitemap_type);
    }

    public function getPropertyStorerClassPath($prop_or_prop_id)
    {
        return $this->getSitemapChildObjectStorerClassPath(Property::class, "Property", $prop_or_prop_id);
    }


    private function getSitemapChildObjectRenderableClassPath($obj_classname, $obj_short_name, $obj_or_obj_id, $for_existing_obj = true, $inexistent_obj_sitemap_type = self::UNKNOWN_STR)
    {

        if ($for_existing_obj) {
            $obj = is_a($obj_or_obj_id, $obj_classname) ? $obj_or_obj_id : (new $obj_classname)::find($obj_or_obj_id);
            if (!$obj) {
                throw new \InvalidArgumentException("$obj_short_name bulunamadı: $obj_or_obj_id");
            }
            $obj_specific = $obj_short_name . "_" . $obj->id;
            $sitemap_specific = $obj->sitemap->sitemapType->name;
        } else {
            $obj_specific = null;
            $sitemap_specific = $inexistent_obj_sitemap_type;
        }

        $renderable_class = AllBuilderEngine::getBuilderClassPath(array_filter([$obj_specific, \Str::plural($obj_short_name)]), self::CONTENT . "\\Website" . session("panel.website.id"), self::APP, $sitemap_specific);

        if (!isset($renderable_class[0]) || !$renderable_class[0]) {
            $exception_class = "Mediapress\\Modules\\Content\\Exceptions\\" . $obj_short_name . "RenderableNotFoundException";
            throw new $exception_class(implode("\n", $renderable_class[1]) . " denemeleri başarısız oldu.", $renderable_class[1][0]);
        } else {
            $renderable_class = $renderable_class[1];
        }

        return $renderable_class;

    }

    private function getSitemapChildObjectStorerClassPath($obj_classname, $obj_short_name, $obj_or_obj_id)
    {

        $obj = is_a($obj_or_obj_id, $obj_classname) ? $obj_or_obj_id : Page::find($obj_or_obj_id);
        if (!$obj) {
            throw new \InvalidArgumentException("$obj_short_name bulunamadı: $obj_or_obj_id");
        }
        $obj_specific = $obj_short_name . "_" . $obj->id;
        $sitemap_specific = $obj->sitemap->sitemapType->name;
        $plural_short_name = \Str::plural($obj_short_name);

        $storer_class = AllBuilderEngine::getStorerClassPath([$obj_specific, $plural_short_name], self::CONTENT . "\\Website" . session("panel.website.id"), self::APP, $sitemap_specific);
        if (!$storer_class[0]) {
            $storer_class_2 = AllBuilderEngine::getStorerClassPath([$plural_short_name . "Storer"], self::CONTENT, [self::APP, self::MEDIAPRESS], null);
            if (!$storer_class_2[0]) {
                return "<strong><code>" . implode("\n<br/>", array_merge($storer_class[1], $storer_class_2[1])) . "</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
            } else {
                $storer_class = $storer_class_2[1];
            }
        } else {
            $storer_class = $storer_class[1];
        }

        return $storer_class;

    }

    // region Tamamlanmadı

    public function getMetaVariablesListForDetail($object_class, $country_group_id, $language_id)
    {

    }

    public function getMetaVariablesFor($model_or_type_and_id, $values_to_override = [])
    {
        $model = objectify($model_or_type_and_id);
        $model_cls = null;
        if ($model){
            $model_cls = get_class($model);
        }


        $sitemap = null;
        $metavariables = [];
        $modelvariables = [];
        switch ($model_cls) {
            case PageDetail::class:
            case CategoryDetail::class:
                $modelvariables = array_merge($metavariables, $this->getStandardMetaVariablesOfParent($model->parent, $model->country_group_id, $model->language_id));
                $sitemap = $model->parent->sitemap;

                if ($model_cls == PageDetail::class) {
                    $metavariables["page_variables"] = $modelvariables;
                } else {
                    $metavariables["category_variables"] = $modelvariables;
                }


                break;
            case SitemapDetail::class:
                $sitemap = $model->sitemap;
                break;
            default:
                $sitemap = null;
                break;
        }

        if ($sitemap) {


            $metavariables["sitemap_variables"] = $this->getStandardMetaVariablesOfSitemap($sitemap, $model->country_group_id, $model->language_id);
            $metavariables["website_variables"] = $this->getStandardMetaVariablesOfWebsite($sitemap->website, $model->country_group_id, $model->language_id);
        }

        return $metavariables;


    }

    public function getStandardMetaVariablesOfWebsite($model_or_id, $country_group_id, $language_id)
    {


        if (is_integer($model_or_id) || is_string($model_or_id)) {
            $website_model = Website::where("id", $model_or_id)->first();
        } else {
            $website_model = $model_or_id;
        }

        $website_detail = $website_model->details()->where(self::COUNTRY_GROUP_ID, $country_group_id)->where(self::LANGUAGE_ID, $language_id)->first();
        $website_title = $website_detail->name ?? $website_model->name;

        $variables = self::getMetaVariablesList("website_variables");

        $variables[self::MODEL]["id"] = $website_model->id;
        $variables[self::VARIABLES]["website_title"][self::VALUE] = strip_tags($website_title);
        $variables[self::VARIABLES]["sitemaps_count"][self::VALUE] = \DB::table('sitemap_website')->where('website_id', $website_model->id)->count();
        return $variables;

    }

    public function getStandardMetaVariablesOfSitemap($sitemap_model_or_id, $country_group_id, $language_id)
    {

        if (is_integer($sitemap_model_or_id) || is_string($sitemap_model_or_id)) {
            $sitemap_model = Sitemap::where("id", $sitemap_model_or_id)->first();
        } else {
            $sitemap_model = $sitemap_model_or_id;
        }


        $sitemap_detail = $sitemap_model->details()->where(self::COUNTRY_GROUP_ID, $country_group_id)->where(self::LANGUAGE_ID, $language_id)->first();
        $sitemap_title = $sitemap_detail->name ?? "";
        $detail_text = \Str::limit(str_replace(array("\r", "\n"), '', strip_tags($sitemap_detail->detail)), config(self::SEO_DESCRIPTION_LENGTH_FOR_DESKTOP, 158)) ?? "";

        unset($sitemap_model_or_id);

        $variables = self::getMetaVariablesList("sitemap_variables");

        $variables[self::MODEL]["id"] = $sitemap_model->id;
        $variables[self::VARIABLES]["sitemap_title"][self::VALUE] = strip_tags($sitemap_title);
        $variables[self::VARIABLES]["sitemap_detail"][self::VALUE] = strip_tags($detail_text);
        $variables[self::VARIABLES]["pages_count"][self::VALUE] = $sitemap_model->pages()->count();
        $variables[self::VARIABLES]["categories_count"][self::VALUE] = $sitemap_model->categories()->count();

        return $variables;


    }

    public function getStandardMetaVariablesOfParent($parent_model_or_type_id_arr, $country_group_id, $language_id)
    {

        $parent_model = objectify($parent_model_or_type_id_arr);

        unset($parent_model_or_type_id_arr);

        $parent_class = get_class($parent_model);

        switch ($parent_class) {
            case Page::class:
                return $this->getStandardMetaVariablesOfPage($parent_model, $country_group_id, $language_id);
                break;
            case Category::class:
                return $this->getStandardMetaVariablesOfCategory($parent_model, $country_group_id, $language_id);
                break;
            default:
                return [];
                break;
        }


    }

    public function getStandardMetaVariablesOfPage($page_model_or_id, $country_group_id, $language_id)
    {
        if (is_integer($page_model_or_id) || is_string($page_model_or_id)) {
            $page_model = Page::where("id", $page_model_or_id)->first();
        } else {
            $page_model = $page_model_or_id;
        }

        $page_detail = $page_model->details()->where(self::COUNTRY_GROUP_ID, $country_group_id)->where(self::LANGUAGE_ID, $language_id)->first();
        $page_title = $page_detail->name ?? "";
        $page_detail = \Str::limit(str_replace(array("\r", "\n"), '', strip_tags($page_detail->detail)), config(self::SEO_DESCRIPTION_LENGTH_FOR_DESKTOP, 158)) ?? "";
        //info:https://blog.spotibo.com/meta-description-length/


        unset($page_model_or_id);

        $variables = self::getMetaVariablesList("page_variables");

        $variables[self::MODEL]["id"] = $page_model->id;
        $variables[self::VARIABLES]["page_title"][self::VALUE] = strip_tags($page_title);
        $variables[self::VARIABLES]["page_detail"][self::VALUE] = strip_tags($page_detail);
        $cat = $page_model->category();
        if ($cat) {
            $variables[self::VARIABLES]["page_category_title"][self::VALUE] = strip_tags($page_model->category()->detail->name ?? "");
        }

        return $variables;

    }

    public function getStandardMetaVariablesOfCategory($model_or_id, $country_group_id, $language_id)
    {

        if (is_integer($model_or_id) || is_string($model_or_id)) {
            $model = Category::where("id", $model_or_id)->first();
        } else {
            $model = $model_or_id;
        }

        $detail = $model->details()->where(self::COUNTRY_GROUP_ID, $country_group_id)->where(self::LANGUAGE_ID, $language_id)->first();
        $title = $detail->name ?? "";
        $cat_detail = \Str::limit(str_replace(array("\r", "\n"), '', strip_tags($detail->detail)), config(self::SEO_DESCRIPTION_LENGTH_FOR_DESKTOP, 158)) ?? "";

        unset($model_or_id);

        $variables = self::getMetaVariablesList("category_variables");

        $variables[self::MODEL]["id"] = $model->id;
        $variables[self::VARIABLES]["category_title"][self::VALUE] = strip_tags($title);
        $variables[self::VARIABLES]["category_detail"][self::VALUE] = strip_tags($cat_detail);

        return $variables;

    }

    public function getMetaTemplatesOf($object_or_type_and_id_arr, $sitemap_detail_or_id = null)
    {


        $object = \objectify($object_or_type_and_id_arr);


        $cls = get_class($object);
        $cls_flat_name = \getSimpleRelationData($cls, "Detail");
        $cls_flat_name = $cls_flat_name["child_flatname"];

        $template = [
            "category_detail" => [
                self::TITLE => [
                    "desktop_value" => "%category_title%"
                ],
                self::DESCRIPTION => [
                    "desktop_value" => "%category_detail%"
                ],
                "keywords" => [
                    "desktop_value" => null
                ],
            ],
            "page_detail" => [
                self::TITLE => [
                    "desktop_value" => "%page_title%"
                ],
                self::DESCRIPTION => [
                    "desktop_value" => "%page_detail%"
                ],
                "keywords" => [
                    "desktop_value" => null
                ],
            ],
            "sitemap_detail" => [
                self::TITLE => [
                    "desktop_value" => "%sitemap_title%"
                ],
                self::DESCRIPTION => [
                    "desktop_value" => "%sitemap_detail%"
                ],
                "keywords" => [
                    "desktop_value" => null
                ],
            ]
        ];
        $_template = $template;
        $sm_detail = null;

        if ($sitemap_detail_or_id) {
            if (is_object($sitemap_detail_or_id)) {
                $sm_detail = $sitemap_detail_or_id;
            } else {
                $sm_detail = objectify([SitemapDetail::class, $sitemap_detail_or_id]);
            }
        }

        if ($cls === 'Mediapress\Modules\Content\Models\PageDetail' ||
            $cls === 'Mediapress\Modules\Content\Models\CategoryDetail') {
            if (!$sm_detail) {
                $sm_detail = SitemapDetail::where("sitemap_id", $object->parent->sitemap_id)
                    ->where(self::COUNTRY_GROUP_ID, $object->country_group_id)
                    ->where(self::LANGUAGE_ID, $object->language_id)->first();
            }
            if ($sm_detail) {
                $_template = array_key_exists($cls_flat_name, $sm_detail->meta_templates) ?
                    $sm_detail->meta_templates[$cls_flat_name] :
                    (array_key_exists($cls_flat_name, $template) ? $template[$cls_flat_name] : $template);
                //$template;
            }

        } elseif ($cls === 'Mediapress\Modules\Content\Models\SitemapDetail') {
            $_template = /*$object->meta_templates ? $object->meta_templates :
                    $template;*/
                in_array($cls_flat_name, array_keys($object->meta_templates)) ?
                    $object->meta_templates[$cls_flat_name] :
                    (array_key_exists($cls_flat_name, $template) ? $template[$cls_flat_name] : $template);/**/
        } elseif ($cls === "Mediapress\Modules\Content\Models\Sitemap") {
            if ($sm_detail) {
                $_template = array_merge($_template, $sm_detail->meta_templates);
            }
        }


        return $_template;
    }

    public function getMetaVariablesList(string $part = "all", array $fill = []): array
    {
        $variables = [
            "website_variables" => [
                self::KEY => "website_variables",
                self::TITLE => trans("ContentPanel::seo.meta.group.website_variables"),
                self::MODEL => [
                    self::TYPE => Website::class,
                    "id" => null,
                ],
                self::VARIABLES => [
                    "website_title" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "website",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.website_title"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.website_title.description")
                    ],
                    "sitemaps_count" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "website",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.sitemaps_count"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.sitemaps_count.description")
                    ],
                ]
            ],
            "sitemap_variables" => [
                self::KEY => "sitemap_variables",
                self::TITLE => trans("ContentPanel::seo.meta.group.sitemap_variables"),
                self::MODEL => [
                    self::TYPE => Sitemap::class,
                    "id" => null,
                ],
                self::VARIABLES => [
                    "sitemap_title" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => self::SITEMAP,
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.sitemap_title"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.sitemap_title.description")
                    ],
                    "sitemap_detail" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "page",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.sitemap_detail"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.sitemap_detail.description")
                    ],
                    "pages_count" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => self::SITEMAP,
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.pages_count"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.pages_count.description")
                    ],
                    "categories_count" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => self::SITEMAP,
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.categories_count"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.categories_count.description")
                    ],

                ]
            ],
            "page_variables" => [
                self::KEY => "page_variables",
                self::TITLE => trans("ContentPanel::seo.meta.group.page_variables"),
                self::MODEL => [
                    self::TYPE => Page::class,
                    "id" => null,
                ],
                self::VARIABLES => [
                    "page_title" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "page",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.page_title"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.page_title.description")
                    ],
                    "page_detail" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "page",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.page_detail"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.page_detail.description")
                    ],
                    "page_category_title" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "page",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.page_category_title"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.page_category_title.description")
                    ],
                ]
            ],
            "category_variables" => [
                self::KEY => "category_variables",
                self::TITLE => trans("ContentPanel::seo.meta.group.category_variables"),
                self::MODEL => [
                    self::TYPE => Category::class,
                    "id" => null,
                ],
                self::VARIABLES => [
                    "category_title" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "category",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.category_title"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.category_title.description")
                    ],
                    "category_detail" => [
                        self::TYPE => self::STANDARD,
                        self::BELONGS_TO => "category",
                        self::INTERPRETATION => self::DIRECT_VALUE,
                        self::VALUE => null,
                        self::TITLE => trans("ContentPanel::seo.meta.category_detail"),
                        self::DESCRIPTION => trans("ContentPanel::seo.meta.category_detail.description")
                    ],
                ]
            ]

        ];

        if (array_key_exists($part, $variables)) {
            $toreturn = $variables[$part];
        } else {
            $toreturn = $variables;
        }

        if (count($fill)) {
            $toreturn = array_merge_recursive($toreturn, $fill);
        }

        return $toreturn;

    }

    // endregion

    public function equateWebsiteDetailsAndVariations($website_id)
    {

        $website = Website::where("id", $website_id)->with(["details" => function ($q) {
            $q->withTrashed();
        }])->first();

        if (is_null($website) || $website->target == "external") {
            return false;
        }

        $website_variations = $website->variations(true);


        if (!count($website_variations) && count($website->details)) {
            $website->details->delete();
        }

        //silinmiş olanları geri al, oluşturulması gerekenleri oluştur (fazlalıklar için öncesinde/sonrasında işlem yap)
        $valid_website_detail_ids = [];
        foreach ($website_variations as $wv) {
            /** @var LanguageWebsite $wv */
            $detail = WebsiteDetail::where("website_id", $website_id)
                ->where(self::COUNTRY_GROUP_ID, $wv[self::COUNTRY_GROUP_ID])
                ->where(self::LANGUAGE_ID, $wv[self::LANGUAGE_ID])->withTrashed()->first();
            if ($detail) {
                if (!$detail->name) {
                    $detail->name = $website->name . "";
                }
                if ($detail->deleted_at) {
                    $detail->deleted_at = null;
                }
                if ($detail->isDirty()) {
                    $detail->update();
                }

            } else {
                $detail = new WebsiteDetail;
                $detail->name = $website->name . "";
                $detail->website_id = $website_id;
                $detail->country_group_id = $wv[self::COUNTRY_GROUP_ID];
                $detail->language_id = $wv[self::LANGUAGE_ID];
                $detail->save();
            }
            $valid_website_detail_ids[] = $detail->id;
        }

        WebsiteDetail::where("website_id", $website_id)->whereNotIn("id", array_filter($valid_website_detail_ids))->delete();

        return true;

    }

    function equatePageDetailsToSitemaps($sitemap_or_sitemap_id)
    {

        return $this->equateDetailsToSitemaps($sitemap_or_sitemap_id, "pages", "page_id", PageDetail::class);

    }

    function equateCategoryDetailsToSitemaps($sitemap_or_sitemap_id)
    {

        return $this->equateDetailsToSitemaps($sitemap_or_sitemap_id, "categories", "category_id", CategoryDetail::class);

    }

    function equateCriteriaDetailsToSitemaps($sitemap_or_sitemap_id)
    {

        return $this->equateDetailsToSitemaps($sitemap_or_sitemap_id, "criterias", "criteria_id", CriteriaDetail::class);

    }

    function equatePropertyDetailsToSitemaps($sitemap_or_sitemap_id)
    {

        return $this->equateDetailsToSitemaps($sitemap_or_sitemap_id, "properties", "property_id", PropertyDetail::class);

    }

    public function equateDetailsToSitemaps($sitemap_or_sitemap_id, $parent_table_name, $parent_key, $detail_class)
    {

        // Details of parents are processed one by one on their single instances
        // for the reason observers do not work on mass processes


        // Regulate $sitemap param
        $sitemap = objectifyOneParam("Mediapress\\Modules\\Content\\Models\\Sitemap", $sitemap_or_sitemap_id);

        // Pick ids of parents of the sitemap
        $parent_ids = DB::table($parent_table_name)
            ->select("id")
            ->where('sitemap_id', $sitemap->id)
            ->where('deleted_at', null)
            ->get()
            ->pluck("id")
            ->toArray();


        // Gather detail unique params
        $details_of_sitemap = $sitemap->details()->select(["country_group_id", "language_id"])->get();

        // Create query to get all existing details in one query  (tail in block below)
        $query = $detail_class::whereIn($parent_key, $parent_ids);

        // Create query to get all details for deletion in one query  (tail in block below)
        $delete_query = $detail_class::whereIn($parent_key, $parent_ids);

        // Loop for every detail of sitemap
        foreach ($details_of_sitemap as $detail) {
            // Create variables for "use" of closures
            $country_group_id = $detail->country_group_id;
            $language_id = $detail->language_id;

            // Tail details to delete query
            $delete_query->where(function ($query) use ($country_group_id, $language_id) {
                $query->where('country_group_id', "<>", $country_group_id)->where('language_id', "<>", $language_id);
            });

        }

        // Fetch details to delete
        $delete_details = $delete_query->get();
        foreach ($delete_details as $delete_detail) {
            // Delete them one by one
            $delete_detail->delete();
        }

    }


    public function createUnexistentDetailsOfParent($parent_object_or_type_and_id_arr)
    {

        $parent_object = objectify($parent_object_or_type_and_id_arr);

        $rel_data = getSimpleRelationData(get_class($parent_object), "Detail");

        $sitemap = $parent_object->sitemap()->with('details')->first();
        $details = $parent_object->details()->withTrashed()->get();

        $objects_affected = 0;
        foreach ($sitemap->details as $sm_detail) {
            $country_group_id = $sm_detail->country_group_id;
            $language_id = $sm_detail->language_id;
            $matching_detail = $details->where("country_group_id", $country_group_id)->where("language_id", $language_id)->first();
            if (!$matching_detail) {
                $dataset = [
                    $rel_data["childs_foreign_key"] => $parent_object->id,
                    'country_group_id' => $country_group_id,
                    'language_id' => $language_id,
                ];
                $detail_full_class = $rel_data["child_class"];
                $new_detail = new $detail_full_class($dataset);
                $new_detail->save();
                $new_detail->delete();
                $objects_affected++;
            }
        }

        return $objects_affected;
    }


}
