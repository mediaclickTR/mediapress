@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{$sitemap->detail->name}}</div>
            </div>
            <div class="float-right">
                @if($sitemap->category == "1")
                    <div class="actions float-left mr-2">
                        <select name="category" id="category" class="simple">
                            <option value="all">{{trans('ContentPanel::page.all_categories')}}</option>
                            @include('ContentPanel::pages.options',['categories'=>cluster($categories,['parent_id'=>'category_id'])])
                        </select>
                    </div>
                @endif
                @if(isMCAdmin())
                    <a href="{!! route("Content.sitemaps.edit",$sitemap_id) !!}"
                       class="filter-block btn-light btn-sm btn"
                       title="{!! trans("ContentPanel::sitemap.edit_sitemap") !!}">
                        <i class="fa fa-cog"></i>
                    </a>
                @endif
                @if($sitemap->detailPage == "1")
                    <a href="{!! route("Content.sitemaps.mainEdit",$sitemap_id) !!}"
                       class="filter-block btn-light btn-sm btn"
                       title="{!! trans("ContentPanel::sitemap.edit_sitemap_detail") !!}" >
                        <i class="fa fa-edit"></i> {!! trans("ContentPanel::sitemap.edit_sitemap_detail") !!}
                    </a>
                @endif
                @if($sitemap->category == "1")
                    <a href="{!! route("Content.categories.category.create",$sitemap_id) !!}"
                       class="filter-block btn-light btn-sm btn"
                       title="{!! trans("ContentPanel::sitemap.edit_categories") !!}">
                        <i class="fa fa-list-alt"></i> {!! trans("ContentPanel::sitemap.edit_categories") !!}
                    </a>
                @endif
                @if($sitemap->criteria == "1")
                    <a href="{!! route("Content.categories.criteria.create",$sitemap_id) !!}"
                       class="filter-block btn-light btn-sm btn"
                       title="{!! trans("ContentPanel::sitemap.edit_criterias") !!}">
                        <i class="fa fa-tasks"></i> {!! trans("ContentPanel::sitemap.edit_criterias") !!}
                    </a>
                @endif
                @if($sitemap->property == "1")
                    <a href="{!! route("Content.categories.property.create",$sitemap_id) !!}"
                       class="filter-block btn-light btn-sm btn"
                       title="{!! trans("ContentPanel::sitemap.edit_properties") !!}">
                        <i class="fa fa-list"></i> {!! trans("ContentPanel::sitemap.edit_properties") !!}
                    </a>
                @endif

                <a href="{!! route("Content.pages.batchUpdate", ['sitemap_id' => $sitemap_id]) !!}"
                   class="filter-block btn-light btn-sm btn"
                   title="{!! trans("ContentPanel::page.edit_order") !!}" >
                    <i class="fa fa-grip-horizontal"></i> {!! trans("ContentPanel::page.edit_order") !!}
                </a>
                @if(function_exists($sitemap->feature_tag.'_button'))
                    @php($func = $sitemap->feature_tag.'_button')
                     {!! $func($sitemap) !!}
                @endif
                    <a href="{!! $createRoute !!}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> {!! trans("ContentPanel::sitemap.add_page") !!}</a>
            </div>
        </div>
       <div class="p-30">
           <div class="table-field">
               {!! $dataTable->table() !!}
           </div>
       </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            var query = parseQuery(window.location.search);
            if (query['category']) {
                $('#category option:selected').prop('selected', false);
                $('#category option[value=' + query['category'] + ']').prop('selected', true);
            }
        });

        $('#category').change(function () {
            let e = $(this);
            let url = window.location.protocol + '//' + window.location.hostname + window.location.pathname;

            if (e.val() !== 'all')
                url = `${url}?category=${e.val()}`;

            location.href = url;
        });

        function parseQuery(search) {
            var args = search.substring(1).split('&');
            var argsParsed = {};
            var i, arg, kvp, key, value;
            for (i = 0; i < args.length; i++) {
                arg = args[i];
                if (-1 === arg.indexOf('=')) {
                    argsParsed[decodeURIComponent(arg).trim()] = true;
                } else {
                    kvp = arg.split('=');
                    key = decodeURIComponent(kvp[0]).trim();
                    value = decodeURIComponent(kvp[1]).trim();
                    argsParsed[key] = value;
                }
            }

            return argsParsed;
        }
    </script>
@endpush
