<?php

return [
    'created_at' => 'Oluşturma Tarihi',
    'updated_at' => 'Son Güncelleme Tarihi',
    'deleted_at' => 'Silinme Tarihi',






    //-------
    'other' => "Diğer",
    'homepage' => "Ana Sayfa",
    'search' => "Arama",
    'abilities' => 'Yetkileri Düzenle',
    'dashboard' => 'Dashboard',
    'website' => 'Web Site',
    'status' => 'Durum',
    'draft' => 'Taslak',
    'published' => 'Yayında',
    'login' => 'Giriş Yap',
    'logout' => 'Çıkış Yap',
    'profile' => 'Profil',
    'preview_site' => 'Siteyi Görüntüle',
    'my_account' => 'Hesabım',
    'check' => 'Seç',
    'upload' => 'Yükle',
    'save' => 'Kaydet',
    'new_add' => 'Yeni Ekle',
    'faker' => 'Fake Veri',
    'create' => ':content Oluştur',
    'add' => 'Ekle',
    'edit' => 'Düzenle',
    'show' => 'Görüntüle',
    'change' => 'Değiştir',
    'remove' => 'Kaldır',
    'delete' => 'Sil',
    'preview' => 'Ön izle',
    'widget' => 'Eklentiler',
    'back' => 'Geri',
    'send' => 'Gönder',
    'yes' => 'Evet',
    'no' => 'Hayır',
    'ok' => 'Tamam',
    'cancel' => 'Vazgeç',
    'actions' => 'İşlemler',
    'selection' => 'Seçim Yapınız',
    'list_selection' => 'Listeden Seç',
    'general' => 'Genel',
    'create-title' => ':name Ekle',
    'edit-title' => ':name Düzenle',
    'selected-delete' => 'Seçilenleri Sil',
    'list_all' => 'Tümünü',
    'show_all' => "Tümünü Göster",
    'all_website_records' => 'Tüm Site Kayıtları',
    'clear_filters' => 'Filtreleri Temizle',
    'model_create' => ':filled Ekle',
    'model_edit' => ':filled Düzenle',
    'settings' => 'Ayarlar',
    'select.language' => 'Dil Seçiniz',
    'filename'=>'Dosya Adı',
    'general_settings' => "Genel Ayarlar",
    'panel_menus' => "Panel Menü",

    'id' => 'ID',
    'row_num' => '<abbr title="Satır">S.</abbr>',
    'name' => 'Ad',
    'surname' => 'Soyad',
    'title' => 'Başlık',
    'type' => 'Tür',
    'image' => 'Resim',
    'name_entry' => 'Ad giriniz',
    'role' => 'Rol',
    'next' => 'İleri',
    'previous' => 'Geri',

    'send_user' => 'Formu doldurulan kullanıcıya e-posta gönderilsin mi?',

    'order' => 'Sıra',
    'order_entry' => 'Sıra giriniz',

    'users' => 'Üyeler',
    'entities' => 'Entity',

    'slug' => 'Slug',
    'slug_entry' => 'Slug giriniz',

    'detail' => 'Detay',
    'detail_entry' => 'Detay giriniz',

    'note' => 'Not',
    'note_entry' => 'Not giriniz',

    'key' => 'Anahtar',
    'key_entry' => 'Anahtar giriniz',

    "system" => "Sistem",

    'created_at_sm' => 'Oluşturma',
    'updated_at_sm' => 'Güncelleme',
    'deleted_at_sm' => 'Silinme',

    'reads' => 'Okunma',
    'read' => 'Okundu',
    'unread' => 'Okunmadı',
    'subject' => 'Konu',
    'email' => 'E-Posta',
    'phone' => 'Telefon',

    'nodata' => 'Kayıtlı veri bulunamadı.',
    'select_service' => 'Servis Seçiniz',
    'select_processing' => 'İşlem Seçiniz',

    'mpmailler' => 'MPMailler',
    'mailchimp' => 'Mailchimp',
    'eligible' => 'Seçilebilir',
    'selected' => 'Seçildi',

    'success_title' => 'Tebrikler!',
    'success_message' => 'İşlem başarıyla yapıldı',
    'danger_title' => 'Opss!',
    'danger_message' => 'İşlem sırasında hata oluştu',
    'notfound_to_date' => 'Bu tarih aralığında veri bulunamadı.',
    'not_found' => 'Kayıt bulunamadı.',
    'choose_below_options' => 'Aşağıdaki işlemlerden birini seçiniz.',
    'back_to_list' => 'Listeye Dön',
    'edit_again' => 'Tekrar Düzenle',
    'add_new' => 'Yeni Ekle',
    'preview_page' => 'Sayfayı Gör',
    'set_this_option_while_session' => 'Bu oturum boyunca bu seçeneği kullan.',

    'select-analytics-type' => 'Analitik Seçimi',
    'google-analytics' => 'Google Analytics',
    'yandex-analytics' => 'Yandex Metrika',

    'multiple_transactions' => 'İşlemler',
    'delete_selected' => 'Seçilenleri Sil',
    'log_content' => '":user" adlı kullanıcı ":processing" işlemi yaptı.',
    'log_transcation_create' => 'Ekleme',
    'log_transcation_edit' => 'Düzenleme',
    'log_transcation_remove' => 'Silme',
    'log_transcation_preview' => 'Görüntüleme',
    'log_transcation_not_defined' => 'Tanımlanmamış İşlem',

    'select.list.size' => 'Liste sayısını seçiniz',

    'status_active' => 'Yayında',
    'status_passive' => 'Yayında Değil / Taslak',

    'sure_deletion' => 'Kaydı silmek üzeresiniz. Emin misiniz?',

    /*
     * Alert
     */

    'are_you_sure' => 'Emin misiniz?',
    'action_cannot_undone' => 'Dikkat bu işlem geri alınamaz!',
    'are_you_sure_sub_desc' => 'Alt sayfaları da silmek istiyor musunuz?',


    /*
     * Status
     */

    'passive' => 'Pasif',
    'active' => 'Aktif',
    'draft' => 'Taslak',
    'predraft' => 'Ön Taslak',

    'email.address' => 'E-Posta Adresi',
    'password' => 'Şifre',
    'login' => 'Giriş Yap',
    'select.default.language'=>'Siteye ait varsayılan dil bulunamadı. Lütfen varsayılan bir dil seçiniz.',


    /*
     * Genel Hata Mesajları
     */

    'status_messages'=>[
        'missing_parameters'=>'Eksik değiştirge(ler)',
        'at_least_one_should_be_provided'=>'En azından biri sağlanmalı: :mass',
    ],
    'sorting'=>[
        'save'=>'Sıralamayı Kaydet',
        'save-now' => 'Şimdi Kaydet',
        'change-text' => 'Sıralamayı değiştirdiniz. Kaydetmeyi unutmayın.',
        'alert' => 'Uyarı!',
    ],

    "status-titles" =>  [
      "active" => 'Aktif',
      "passive" => 'Pasif',
      "draft" => 'Taslak',
    ],

    "category-icon-title" =>  [
      "criteria" => 'Kriter Seç',
      "property" => 'Özellik Seç',
      "delete" => 'Sil',
      "edit" => 'Düzenle',
      "sub-category" => 'Alt Kategori Ekle',
    ],

    'login_type' => "Giriş Sayfası",
    'register_type' => "Kayıt Sayfası",
    'password_email_type' => "Şifremi Unuttum Sayfası",
    'email_verify_type' => "E-posta Etkinleştirme Sayfası",
    'export_json' => "Json Çıktı Ver",
    'import_json' => "Json Girdi Al",

];
