@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.edit_user") !!}</h1>
        <div class="row">
            <div class="col-md-12">
                <form action="{!! route('Heraldist.mailchimp.maillists.members.update',['id'=>$maillistid,'memberid'=>$member->id]) !!}" novalidate method="POST">
                    @include("HeraldistPanel::mail_service.mailchimp.maillists.members.form")
                </form>
            </div>
        </div>
    </div>
@endsection
