<?php

    namespace Mediapress\Modules\Content\Controllers;

    use Mediapress\Http\Controllers\PanelController as Controller;
    use Mediapress\Modules\Content\AllBuilder\Renderables\WebsiteDetailTabs;
    use Mediapress\Modules\Content\Facades\VariationTemplates;
    use Mediapress\Modules\MPCore\Facades\FilterEngine;
    use Mediapress\Modules\MPCore\Models\Country;
    use Mediapress\Modules\Content\Models\LanguageWebsite;
    use Mediapress\Modules\MPCore\Models\CountryGroup;
    use Mediapress\Modules\MPCore\Models\CountryGroupLanguage;
    use Mediapress\Modules\MPCore\Models\Language;
    use Illuminate\Http\Request;
    use Mediapress\Modules\Content\Models\Website;
    use Mediapress\Modules\MPCore\Facades\UserActionLog;
    use Mediapress\Modules\Content\Facades\Auth;

    class WebsiteController extends Controller
    {
        const ACCESSDENIED = 'accessdenied';
        const TITLE = "title";
        const OWNER_TYPE = "owner_type";
        const OWNER_ID = "owner_id";
        const TARGET = "target";
        const LANGUAGES = 'languages';
        const CONTENT_PANEL_WEBSITE_SORT = "ContentPanel::website.sort";
        const REQUIRED = 'required';
        const FILLED = 'filled';
        const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
        const FIELD = "field";
        const RULES = "rules";
        const PANEL_WEBSITE = 'panel.website';

        /**
         * @return mixed
         */
        public function index()
        {

            if(! activeUserCan([
                "content.websites.index",
            ])){
                return rejectResponse();
            }

            $websites = Website::where("status", "!=", Website::PREDRAFT);
            $queries = FilterEngine::filter($websites, false)['queries'];
            $websites = FilterEngine::filter($websites, false)['model'];
            $crumb = [
                "key"=>"websites_index",
                "text"=>trans('ContentPanel::website.websites'),
                "icon"=>"globe",
                "href"=>"javascript:void(0);"
            ];
            $breadcrumb = \Mediapress\Modules\Content\Facades\Content::getBreadcrumb([$crumb]);
            return view('ContentPanel::websites.index', compact('websites', 'queries','breadcrumb'));
        }

        public function create()
        {

            if(! activeUserCan([
                "content.websites.create",
            ])){
                return rejectResponse();
            }
            // draft website varsa edite gönder
            $create = Website::preCreate();
            if ($create) {
                return redirect(route("Content.websites.edit", $create->id));
            } else {
                throw new Exception("Hata oluştu", json_encode($create));
            }
        }

        public function edit($id)
        {
            if (!userAction('message.websites.edit', true, false)) {
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }

            $website = Website::findOrFail($id);
            // Sitenin ülke grupları boş ise, siteye global ülke grubu ata ve içini tüm ülkelerle doldur
            if (!$website->countryGroup) {
                $insertCountryGroup = CountryGroup::updateOrCreate(["code" => "gl", self::TITLE => "Global", self::OWNER_TYPE => Website::class, self::OWNER_ID => $website->id]);
                if ($insertCountryGroup) {
                    $countries = Country::get();
                    $insertCountryGroup->countries()->sync($countries->pluck("code"));
                }
                return redirect(route('Content.websites.edit', $website->id));
            }

            $targets = ['internal' => trans("ContentPanel::website.internal"), 'external' => trans("ContentPanel::website.external")];
            $website_types = ['domain' => trans("ContentPanel::website.domain"), 'folder' => trans("ContentPanel::website.folder"), 'alias' => trans("ContentPanel::website.alias")];

            $websites = Website::where(self::TARGET, "internal")->get()->pluck("name", "id");
            $lang_codes = Language::get()->pluck("code", "id");
            $languages = Language::orderBy("sort")->get()->pluck("name", "id");

            $countryGroups = ($website->countryGroups) ? $website->countryGroups->pluck(self::TITLE, "id") : [];

            $crumbs = [
                [
                    "key"=>"websites_index",
                    "text"=>trans('ContentPanel::website.websites'),
                    "icon"=>"globe",
                    "href"=>route('Content.websites.index')
                ],
                [
                    "key"=>"website_edit",
                    "text"=>$website->name ? : "<yeni website>",
                    "icon"=>"globe",
                    "href"=>"javascript:void(0);"
                ]
            ];

            $breadcrumb = \Mediapress\Modules\Content\Facades\Content::getBreadcrumb($crumbs);
            return view('ContentPanel::websites.edit', compact(
                'countryGroups',
                'website',
                'websites',
                'id',
                'targets',
                'lang_codes',
                self::LANGUAGES,
                'website_types',
                'breadcrumb'
            ));
        }

        public function update(Request $request)
        {
            $id = $request->id;

            if(! activeUserCan([
                "content.websites.website$id.update",
                "content.websites.update",
            ])){
                return rejectResponse();
            }

            $website = Website::find($id);

            $fields = [
                self::TARGET => trans("ContentPanel::website.internal_or_external"),
                'type' => trans("ContentPanel::website.website_type"),
                'name' => trans("ContentPanel::website.name"),
                'slug' => trans("ContentPanel::website.domain"),
                'website_id' => trans("ContentPanel::website.parent_website"),
                'sort' => trans(self::CONTENT_PANEL_WEBSITE_SORT),
                'ssl' => trans("ContentPanel::website.ssl"),
                'use_deflng_code' => trans("ContentPanel::website.use_deflng_code"),
            ];

            $rules = [
                self::TARGET => self::REQUIRED,
                'name' => 'required|max:255',
                'slug' => 'unique:websites,slug,' . $website->id,
                'sort' => 'required|integer',
                'ssl' => self::REQUIRED,
                'use_deflng_code' => self::REQUIRED,
            ];

            $messages = [
                'target.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.internal_or_external")]),
                'type.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.type")]),
                'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.name")]),
                'slug.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.slug")]),
                'slug.unique' => trans("MPCorePanel::validation.unique", [self::FILLED, trans("ContentPanel::website.slug")]),
                'sort.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans(self::CONTENT_PANEL_WEBSITE_SORT)]),
                'sort.integer' => trans("MPCorePanel::validation.integer", [self::FILLED, trans(self::CONTENT_PANEL_WEBSITE_SORT)]),
                'ssl.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.ssl")]),
                'use_deflng_code.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.use_deflng_code")]),
            ];

            //region details validation

            $detail_ids = $website->details->pluck("id");

            $fields_for_details = [
                [
                    self::FIELD => "name",
                    self::RULES => "required|max:255"
                ],
            ];

            $generated_names=[];

            foreach ($fields_for_details as $ffd) {
                foreach ($detail_ids as $di) {
                    $field_generated_name = "website_detail->" . $di . "->" . $ffd[self::FIELD];
                    $generated_names[]=$field_generated_name;
                    $fields[$field_generated_name] = trans("ContentPanel::website.detail.fields." . $ffd[self::FIELD]);
                    $rules[$field_generated_name] = $ffd[self::RULES];
                    if (\Str::contains($ffd[self::RULES], self::REQUIRED)) {
                        $messages[$field_generated_name . ".required"] = trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED, trans("ContentPanel::website.detail.fields.name")]);
                    }
                }
            }

            //endregion

            $this->validate($request, $rules, $messages, $fields);

            $except = $generated_names;
            $except[]='_token';

            $data = request()->except($except);

            if($data['regions'] == 1) {
                $data['variation_template'] = "{cg}_{lng}";
            }

            $data += ["status" => 1];


            $update = Website::updateOrCreate(['id' => $id], $data);


            $dataSourceFolder = app_path('Modules' . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . 'Website' . $id . '/DataSources');
            if(!file_exists($dataSourceFolder)){
                $oldmask = umask(0);
                mkdir($dataSourceFolder, 0777,true);
                umask($oldmask);
            }

            // Log
            if ($update) {
                if (session()->get(self::PANEL_WEBSITE)->id == $update->id) {
                    session()->forget(self::PANEL_WEBSITE);
                    session()->put(self::PANEL_WEBSITE, $update);
                }

                if($request->get('default_language')) {
                    if (isset($update->defaultLanguage()->id) && !in_array($update->defaultLanguage()->id, $request->languages)) {
                        //$update->defaultLanguage()->id
                        return redirect()->back()->with('error', trans('ContentPanel::website.not_remove_default_language'));
                    } else {
                        $update_languages = $update->languages()->sync($request->languages);

                        if ($update_languages) {
                            CountryGroupLanguage::where("language_id", $request->default_language)->where("country_group_id", $update->globalCountryGroup()->id)->update(["default" => "1"]);
                        }
                    }
                }

                //region details save
                foreach($website->details as $detail){
                    foreach($fields_for_details as $ffd){
                        $fieldname = $ffd[self::FIELD];
                        $field_generated_name = "website_detail->" . $detail->id . "->" . $fieldname;
                        if($request->has($field_generated_name)){
                            $detail->$fieldname = $request->$field_generated_name;
                        }
                    }
                    if($detail->isDirty()){
                        $detail->update();
                    }
                }
                //endregion

            }

            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $update);

            return redirect(route('Content.websites.index'))->with('message', trans('MPCorePanel::general.success_message'));
        }

        public function delete($id)
        {


            if(! activeUserCan([
                "content.websites.website$id.delete",
                "content.websites.delete",
            ])){
                return rejectResponse();
            }

            $website = Website::find($id);

            if ($website) {
                $countryGroup = CountryGroup::where(self::OWNER_ID, $website->id)->where(self::OWNER_TYPE, Website::class)->first();
                if ($countryGroup) {
                    $countryGroup->countries()->detach();
                    $countryGroup->delete();
                }

                $website->delete();
                $website->languages()->detach();

                UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $website);
            }

            return redirect(route('Content.websites.index'))->with('message', trans('MPCorePanel::general.success_message'));
        }

        public function removeLanguageVariation(Request $request)
        {
            $variations = LanguageWebsite::where("website_id", $request->website_id)->where("country_group_id", $request->country_group_id)->delete();
            if ($variations) {
                return 1;
            } else {
                return 0;
            }
        }

        public function websiteGroupLanguages(Request $request)
        {
            $countryGroup = CountryGroup::find($request->group_id);
            if ($countryGroup) {
                $countries = $countryGroup->countries->pluck("native", "id");
                return response()->json($countries);
            } else {
                return false;
            }
        }

        public function countryGroups(Request $request)
        {
            $countryGroup = CountryGroup::where(self::OWNER_TYPE, $request->owner_type)->where(self::OWNER_ID, $request->owner_id)->get()->pluck(self::TITLE, "id");
            return response()->json($countryGroup);
        }

        public function variation_templates(Website $website)
        {
            return response()->json(VariationTemplates::variations($website));
        }

        public function getDetailTabs(Website $website)
        {
            $renderable = new WebsiteDetailTabs(["details" => $website->details]);
            return $renderable->render(false);
        }
    }
