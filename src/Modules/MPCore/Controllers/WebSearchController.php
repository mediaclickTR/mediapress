<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Mediapress\Foundation\JsonSearch;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Models\Url;

class WebSearchController extends BaseController
{
    public const RESULTS = 'results';
    public $mediapress;
    public $default_language;
    public $active_language;
    public $website;

    public function __construct(Mediapress $mediapress)
    {
        $mediapress->init();
        $this->mediapress = $mediapress;
        $this->default_language = $mediapress->defaultLanguage;
        $this->active_language = $mediapress->activeLanguage;
        $this->website = $mediapress->website;
    }

    public function getResult(Request $request)
    {
        $query = htmlspecialchars(strip_tags($request->get('q')));
        $query = trim(preg_replace('/\s\s+/', ' ', $query));

        $json_search = new JsonSearch();
        if ($json_search->active) {
            $json_search->index();
            $results = $json_search->search($this->mediapress, $query);
            if ($results) {
                // TODO :: düzenlenecek
                return view("web.search.results", compact(self::RESULTS));
            }
        }

        return $this->search($query);
    }

    public function search($query)
    {
        $results_per_page = config("app.site_search.results_per_page", 10);

        $results=$this->mediapress->search($query);
        $search = $this->paginate($results, $results_per_page, null, ['path' => url(langPart('search.url','ara'))]);

        $home_page_url = new Url(['']);
        $set_url_att = ($this->active_language->id != $this->default_language->id) ? "/" . $this->active_language : null;
        $home_page_url->setUrlAttribute($set_url_att);

        $this->mediapress->breadcrumb = [
            ['name' => langPart('mediapress.homePage', 'Ana Sayfa', [], $this->active_language->id), 'url' => $home_page_url],
            ['name' => langPart('mediapress.searchResults', 'Arama Sonuçları', [], $this->active_language->id), 'url' => url('')]
        ];

        // Metas
        $this->mediapress->metas->paginate($search);

        $this->mediapress->search = [
            'query'=>$query,
            'pagination'=>$search,
            self::RESULTS =>$results,
            'total'=>$search->total(),
        ];

        $results = $this->mediapress;
        // TODO:düzenlenecek
        return view("web.search.results", compact(self::RESULTS));
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
