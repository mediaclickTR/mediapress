<?php

namespace Mediapress\Modules\Locale\Models;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    protected $connection = 'mediapress_locale';
    protected $table = 'mahalle';
    protected $fillable = ["name"];

    public function village()
    {
        return $this->belongsTo(Village::class, 'koy_id');
    }
}
