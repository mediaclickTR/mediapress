<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\AllBuilder\Foundation\FormStorer;
use Mediapress\Foundation\BackupData;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\AllBuilder\Creator\CreateRenderable;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Foundation\DefaultSitemapKit;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\SitemapDetailExtra;
use Mediapress\Modules\Content\Models\SitemapExtra;
use Mediapress\Modules\Content\Models\SitemapType;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\FileManager\Models\MFolder;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Models\Website;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SitemapController extends Controller
{
    public const WEBSITE = 'website';
    public const SITEMAP = "sitemap";
    public const SUB_KEY = 'sub_key';
    public const ACCESSDENIED = 'accessdenied';
    public const WEBSITE_ID = "website_id";
    public const FILESYSTEM = 'filesystem';
    public const SFILESYSTEM = 'sfilesystem';
    public const TYPE = 'type';
    public const SEARCHABLE = 'searchable';
    public const URL_STATUS = 'url_status';
    public const HAS_DETAIL = 'has_detail';
    public const AUTO_ORDER = 'auto_order';
    public const SUB_PAGE_ORDER = 'sub_page_order';
    public const RESERVED_URL = 'has_reserved_url';
    public const HAS_CATEGORY = 'has_category';
    public const HAS_CRITERIA = 'has_criteria';
    public const SHOW_IN_PANEL_MENU = 'show_in_panel_menu';
    public const HAS_PROPERTY = 'has_property';
    public const FEATURED_IN_PANEL_MENU = 'featured_in_panel_menu';
    public const DETAILS = 'details';
    public const C = "[0-9]+";

    /**
     * @return mixed
     */


    // region CRUD
    public function index()
    {

        $website = session("panel.website");
        if(! activeUserCan([
            "content.websites.website".$website->id.".sitemaps.index",
            "content.websites.sitemaps.index",
        ])){
            return rejectResponse();
        }

        $types = SitemapType::get()->pluck("name", "id")->map(function ($name) {
            return trans("ContentPanel::sitemap.type.$name.name") === "ContentPanel::sitemap.type.$name.name" ? $name : trans("ContentPanel::sitemap.type.$name.name");
        });

        $sitemaps = Sitemap::
        whereHas('websites', function ($q) {
            $q->where('id', session('panel.website')->id);
        })->
        status([Sitemap::ACTIVE, Sitemap::PASSIVE, Sitemap::DRAFT])->
        with(['sitemapType', self::DETAILS => function ($query) {
            //TODO checkout
            //  $query->active("active")->with("url");
        }])->
        orderBy('id')->
        paginate(25);

        $website = session()->get('panel.website', function () {
            return Website::where("target", "internal")->where("type", "!=", "alias")->whereHas("variations")->orderByRaw('FIELD(slug,"' . request()->getHost() . '") DESC')->first();
        });

        $crumb = [
            "key" => "sitemaps_index",
            "text" => trans('ContentPanel::sitemap.sitemaps'),
            "icon" => "sitemap",
            "href" => "javascript:void(0);"
        ];
        $breadcrumb = \Mediapress\Modules\Content\Facades\Content::getBreadcrumb([$crumb]);

        return view('ContentPanel::sitemaps.index', compact('sitemaps', 'types', self::WEBSITE, 'breadcrumb'));
    }

    public function create()
    {

        $website = session("panel.website");

        if(! activeUserCan([
            "content.websites.website".$website->id.".sitemaps.create",
            "content.websites.sitemaps.create",
        ])){
            return rejectResponse(null, null, "redirect_back");
        }

        $website_id = session("panel.website.id");


        $type = \request(self::TYPE);
        $type = $type == "null" ? null : $type;
        $create = Sitemap::preCreate(["feature_tag" => $type]);
        if ($create->websites()->sync([session("panel.website.id")])) {
            UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $create);
            /*if(activeUserCan([
                "content.websites.website".$website_id.".sitemaps.sitemap".$create->id.".update",
                "content.websites.website".$website_id.".sitemaps.update",
                "content.websites.sitemaps.update",
            ])){*/
                return redirect(route("Content.sitemaps.edit", ['sitemap'=>$create->id,'isNew'=>'yes']));
            /*}else{
                return redirect(route("Content.sitemaps.index"))->with("messages",["Sayfa yapısı başarıyla oluşturuldu. Düzenleme yetkiniz olmadığı için liste sayfasına yönlendirildiniz."]);
            }*/

        } else {
            throw new \Exception("Sitemap Modeli Oluşturulamadı : " . json_encode($create));
        }
    }

    public function edit(Sitemap $sitemap, Website $website)
    {
        $create_permissions = [
            "content.websites.website".$sitemap->website->id.".sitemaps.create",
            "content.websites.sitemaps.create",
        ];

        $update_permissions = [
            "content.websites.website".$sitemap->website->id.".sitemaps.sitemap".$sitemap->id.".update",
            "content.websites.website".$sitemap->website->id.".sitemaps.update",
            "content.websites.sitemaps.update",
        ];

        if(request()->has("isNew") && request()->isNew=="yes" && $sitemap->status == 3){
            if( ! activeUserCan($create_permissions)){
                return rejectResponse();
            }
        }else{
            if( ! activeUserCan($update_permissions)){
                return rejectResponse();
            }
        }

        $websites = Website::where("target", "internal")->where(self::TYPE, "!=", "alias")->orderByRaw('FIELD(slug,"' . request()->getHost() . '") DESC')->get()->pluck("name", "id");
        $types = SitemapType::get();

        $sitemap->load([
            self::DETAILS => function ($q) use ($websites) {
                $q->where("website_id", session("panel.website.id"))->withTrashed()->with("urls");
            },
            "categories.details" => function ($q) use ($websites) {
//                $q->where("website_id", $websites->keys()->first())->with("url");
            },
            "sitemapType"]);
        $olds = [
            //'websites[]' => $sitemap->websites->pluck("id"),
            self::FILESYSTEM => config('mediapress.filesystem.public_uploads_default'),
            self::SFILESYSTEM => config('mediapress.filesystem.secure_uploads_default'),
            self::TYPE => isset($sitemap->sitemapType) ? $sitemap->sitemapType->sitemap_type_type : null,
            self::SUB_KEY => isset($sitemap->sitemapType) ? $sitemap->sitemapType->name : null,
            self::SEARCHABLE => $sitemap->searchable,
            self::URL_STATUS => $sitemap->urlStatus,
            self::HAS_DETAIL => $sitemap->detailPage,
            self::AUTO_ORDER => $sitemap->autoOrder,
            self::SUB_PAGE_ORDER => $sitemap->subPageOrder,
            self::RESERVED_URL => $sitemap->reservedUrl,
            self::HAS_CATEGORY => $sitemap->category,
            self::HAS_CRITERIA => $sitemap->criteria,
            self::HAS_PROPERTY => $sitemap->property,
            self::SHOW_IN_PANEL_MENU => $sitemap->show_in_panel_menu,
            self::FEATURED_IN_PANEL_MENU => $sitemap->featured_in_panel_menu,
        ];

        session(array_combine(array_map(function ($k) {
            return "_old_input." . $k;
        }, array_keys($olds)), array_values($olds)));
        $sub_active = !isset($sitemap->sitemapType);

        $variation_templates = VariationTemplates::variations($sitemap);

        $crumbs = [
            [
                "key" => "sitemaps_index",
                "text" => trans('ContentPanel::sitemap.sitemaps'),
                "icon" => "sitemap",
                "href" => route("Content.sitemaps.index")
            ],
            [
                "key" => "sitemap_edit",
                "text" => isset($sitemap->detail->name) && $sitemap->detail->name ? $sitemap->detail->name . " - Ayarlar" : "<yeni sayfa yapısı>",
                "icon" => "sitemap",
                "href" => "javascript:void(0);"
            ]
        ];
        $breadcrumb = \Mediapress\Modules\Content\Facades\Content::getBreadcrumb($crumbs);

        $variables = Content::getMetaVariablesList();
        $flatten_variables = Arr::collapse(Arr::pluck($variables, "variables"));

        $websiter = session('panel.website');
        $sitemaps = $websiter->sitemaps()->with(['detail' => function ($q) {
            $q->select('sitemap_id', 'name');
        }])->where('id', '!=', $sitemap->id)->status(1)->get(['sitemaps.id'])->pluck('detail.name', 'id')->toArray();
        $sitemaps = ['' => 'Boş yada Seçiniz'] + $sitemaps;
        return view('ContentPanel::sitemaps.edit', compact("websites", "types", self::SITEMAP, "sub_active", 'variation_templates', 'breadcrumb', 'sitemaps', 'flatten_variables'));
    }

    public function mainEdit($id)
    {


        $object = Sitemap::find($id);
        if (!$object) {
            throw new \Exception("Sitemap kaydı bulunamadı:" . $object->id);
        }
        $website = $object->website;

        if ($website->id != session("panel.website.id")) {

            session(["panel.website" => $website]);

            return redirect()->route('panel.dashboard');

            //throw new \UnexpectedValueException("Yönetim panelinde website oturumu değiştirilmiş. Bir sayfa ya da sayfa yapısı, sadece oturumda seçilen websiteye aitse düzenlenebilir.");
        }

        if(! activeUserCan([
            "content.websites.website".$website->id.".sitemaps.sitemap".$object->id.".update",
            "content.websites.website".$website->id.".sitemaps.update",
            "content.websites.sitemaps.update",
        ])){
            return rejectResponse();
        }


        $renderable_class = \Mediapress\Modules\Content\Facades\Content::getSitemapRenderableClassPath($object);
        $renderable = new $renderable_class([self::SITEMAP => &$object, self::WEBSITE => $website, "request_params" => request()->all()]);

        $crumb = [
            "key" => "sitemap_main_edit",
            "text" => $object->detail->name . " - İçerik",
            "icon" => "sitemap",
            "href" => "javascript:void(0);"
        ];
        $breadcrumb = \Mediapress\Modules\Content\Facades\Content::getBreadcrumb([$crumb]);
        return view('ContentPanel::sitemaps.mainedit', compact('id', 'object', 'renderable', 'breadcrumb'));

    }

    public function mainUpdate($id){

        $object = Sitemap::find($id);
        if (!$object) {
            throw new \Exception("Sitemap kaydı bulunamadı:" . $object->id);
        }

        $request = Request::capture();
        $website = $object->website;

        if ($website->id != session("panel.website.id")) {
            session(["panel.website" => $website]);

            return redirect()->route('panel.dashboard');
            //throw new \UnexpectedValueException("Yönetim panelinde website oturumu değiştirilmiş. Bir sayfa ya da sayfa yapısı, sadece oturumda seçilen websiteye aitse düzenlenebilir.");
        }
        if(! activeUserCan([
            "content.websites.website".$website->id.".sitemaps.sitemap".$object->id.".update",
            "content.websites.website".$website->id.".sitemaps.update",
            "content.websites.sitemaps.update",
        ])){
            return rejectResponse();
        }
        $renderable_class = \Mediapress\Modules\Content\Facades\Content::getSitemapRenderableClassPath($object);

        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class([self::SITEMAP => &$object, self::WEBSITE => $website, "request_params" => request()->all()]);

        $storer_class = \Mediapress\Modules\Content\Facades\Content::getSitemapStorerClassPath($object);

        //return dd($storer_class);

        /** @var FormStorer $storer */
        $storer = new $storer_class($renderable, $request);

        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $storer->setMainObjectData($object);

        $store = $storer->store();


        if (!$store) {
            return redirect()->back()->withErrors($storer->getErrors());
        }

        $backup = new BackupData();
        $backup->store($storer);

        $sitemap_index_route = $object->sitemapType->sitemap_type_type == "static" ? route("Content.sitemaps.mainEdit", $object->id) : route("Content.pages.index", $object->id);
        return redirect()->to($sitemap_index_route)->with('message', trans('MPCorePanel::general.success_message'));

    }

    public function store()
    {
        $website_id = session('panel.website.id');

        \DB::beginTransaction();
        try {
            $data = request()->all();

            $sitemap = Sitemap::findOrNew($data["id"] * 1);

            if(! $sitemap->exists || (request()->has("isNew") && request()->isNew=="yes" && $sitemap->status == 3)){
                \DB::rollback();
                $create_permissions = [
                    "content.websites.website".$website_id.".sitemaps.create",
                    "content.websites.sitemaps.create",
                ];
                if( ! activeUserCan($create_permissions)){
                    \DB::rollback();
                    return rejectResponse();
                }
            }else{
                \DB::rollback();
                $update_permissions = [
                    "content.websites.website".$sitemap->website->id.".sitemaps.sitemap".$sitemap->id.".update",
                    "content.websites.website".$sitemap->website->id.".sitemaps.update",
                    "content.websites.sitemaps.update",
                ];
                if( ! activeUserCan($update_permissions)){
                    \DB::rollback();
                    return rejectResponse();
                }
            }


            $existing_details = [];

            if (!$sitemap->exists) {
                $sitemap->admin_id = auth('admin')->id();
            }else{
                $details = $sitemap->details()->withTrashed()->get();
                foreach ($details as $detail) {
                    $key = $detail->country_group_id . ":" . $detail->language_id;
                    $existing_details[$key] = $detail;
                }
            }
            $sitemap->searchable = isset($data[self::SEARCHABLE]) && filter_var($data[self::SEARCHABLE], FILTER_VALIDATE_BOOLEAN);
            $sitemap->urlStatus = isset($data[self::URL_STATUS]) && filter_var($data[self::URL_STATUS], FILTER_VALIDATE_BOOLEAN);
            $sitemap->detailPage = isset($data[self::HAS_DETAIL]) && filter_var($data[self::HAS_DETAIL], FILTER_VALIDATE_BOOLEAN);
            $sitemap->autoOrder = isset($data[self::AUTO_ORDER]) && filter_var($data[self::AUTO_ORDER], FILTER_VALIDATE_BOOLEAN);
            $sitemap->subPageOrder = isset($data[self::SUB_PAGE_ORDER]) && filter_var($data[self::SUB_PAGE_ORDER], FILTER_VALIDATE_BOOLEAN);
            $sitemap->reservedUrl = isset($data[self::RESERVED_URL]) && filter_var($data[self::RESERVED_URL], FILTER_VALIDATE_BOOLEAN);
            $sitemap->category = isset($data[self::HAS_CATEGORY]) && filter_var($data[self::HAS_CATEGORY], FILTER_VALIDATE_BOOLEAN);
            $sitemap->criteria = isset($data[self::HAS_CRITERIA]) && filter_var($data[self::HAS_CRITERIA], FILTER_VALIDATE_BOOLEAN);
            $sitemap->property = isset($data[self::HAS_PROPERTY]) && filter_var($data[self::HAS_PROPERTY], FILTER_VALIDATE_BOOLEAN);
            $sitemap->filesystem = isset($data[self::FILESYSTEM]) ? $data[self::FILESYSTEM] : config('mediapress.filesystem.public_uploads_default');
            $sitemap->sfilesystem = isset($data[self::SFILESYSTEM]) ? $data[self::SFILESYSTEM] : config('mediapress.filesystem.secure_uploads_default');
            $sitemap->sitemap_id = $data['parent_sitemap_id'];
            $sitemap->show_in_panel_menu = isset($data[self::SHOW_IN_PANEL_MENU]) && filter_var($data[self::SHOW_IN_PANEL_MENU], FILTER_VALIDATE_BOOLEAN);
            $sitemap->featured_in_panel_menu = isset($data[self::FEATURED_IN_PANEL_MENU]) && filter_var($data[self::FEATURED_IN_PANEL_MENU], FILTER_VALIDATE_BOOLEAN);
            if ($sitemap->featured_in_panel_menu) {
                Sitemap::where(self::FEATURED_IN_PANEL_MENU, true)->whereHas('websites', function ($q) {
                    $q->where('id', session('panel.website.id'));
                })->update([self::FEATURED_IN_PANEL_MENU => false]);
            }
            $sitemap->status = Sitemap::ACTIVE;

            /** @var Model $sitemapType */
//            $sitemap->sitemap_type_id = $data["type"] * 1;
            if (isset($data[self::SUB_KEY])) {
                $sitemapType = SitemapType::findOrNew($sitemap->sitemap_type_id);
                if (!$sitemapType->exists) {
                    if (SitemapType::where([
                            'name' => $data[self::SUB_KEY],
                            'website_id' => session('panel.website.id')
                        ])->count() > 0) {
                        throw new \Exception($data[self::SUB_KEY] . " tip adı daha önce kullanılmış. Lütfen özgün bir ad belirtin.");
                    }
                    $sitemapType->exists = false;
                    $sitemapType->id = null;
                    $sitemapType->sitemap_type_type = $data[self::TYPE];
                    $sitemapType->website_id = session('panel.website.id');
                    $sitemapType->fill(["name" => Str::studly(Str::slug($data[self::SUB_KEY], '_'))]);
                    $sitemapType->save();
                    $sitemap->sitemapType()->associate($sitemapType);
                } else {
                    $sitemapType->sitemap_type_type = $data[self::TYPE];
                    $sitemapType->name = $data[self::SUB_KEY];
                    $sitemapType->update();
                }
            }
            $sitemap->save();

            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $sitemap);

            /*\DB::rollback();
            dump($sitemap);
            dump($sitemap->details()->with("countryGroups.language")->get()->pluck("language","id")->toArray());
            return dd($data["sitemap_active"]);*/


            $sent_details = [];

            if (isset($data["sitemap_detail_active"])) {
                foreach ($data["sitemap_detail_active"] as $country_group_id => $country_group_languages) {
                    foreach ($country_group_languages as $language_id => $vdata) {

                        $meta_templates = $data["meta_templates"][$country_group_id][$language_id] ?? [];

                        $key = $country_group_id . ":" . $language_id;
                        $sent_details[] = $key;

                        $name = $data["sitemap_name"][$country_group_id][$language_id];
                        $pattern = $data["sitemap_pattern"][$country_group_id][$language_id];
                        $slug = $data["sitemap_slug"][$country_group_id][$language_id];
                        $category_slug = !$sitemap->category ? $sitemap->category_slug : $data["sitemap_catslug"][$country_group_id][$language_id];

                        /** @var SitemapDetail $sitemapDetail */
                        $sitemap->websites()->sync($website_id);
                        if ($sitemap->exists && array_key_exists($key, $existing_details)) {
                            $detail = $existing_details[$key];
                            $detail->update(
                                [
                                    "name" => $name,
                                    "slug" => $slug,
                                    "category_slug" => $category_slug,
                                    "active" => 1,
                                    'status' => 1,
                                    'meta_templates' => $meta_templates
                                ]
                            );
                            if ($detail->trashed()) {
                                $detail->restore();
                            }
                        } else {
                            $sitemapDetail = $sitemap->details()->updateOrCreate(
                                [
                                    "language_id" => $language_id,
                                    "country_group_id" => $country_group_id,
                                    self::WEBSITE_ID => $website_id
                                ],
                                [
                                    "name" => $name,
                                    "slug" => $slug,
                                    "category_slug" => $category_slug,
                                    "active" => 1,
                                    'status' => 1,
                                    'meta_templates' => $meta_templates
                                ]
                            );
                        }
                    }
                }
            }

            $details_to_delete = array_diff(array_keys($existing_details), $sent_details);

            if (count($details_to_delete)) {

                foreach ($details_to_delete as $dtd) {

                    $separated = preg_split('/:/', $dtd);

                    if (!(strlen($separated[0]) && strlen($separated[1]))) {
                        continue;
                    }

                    $_cgid = $separated[0];
                    $_lngid = $separated[1];

                    $detail_model = $sitemap->details()->where("country_group_id", $_cgid)->where("language_id", $_lngid)->first();
                    if ($detail_model) {
                        $detail_model->delete();
                    }

                }
            }


            \Mediapress\Modules\Content\Facades\Content::equatePageDetailsToSitemaps($sitemap);
            \Mediapress\Modules\Content\Facades\Content::equateCategoryDetailsToSitemaps($sitemap);
            \Mediapress\Modules\Content\Facades\Content::equateCriteriaDetailsToSitemaps($sitemap);
            \Mediapress\Modules\Content\Facades\Content::equatePropertyDetailsToSitemaps($sitemap);


            \DB::commit();
            if(config('app.env') == 'local') {
                $this->createFiles($sitemap);
            }
            return response()->redirectToRoute("Content.sitemaps.index");
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->back()->withInput()->withErrors($e->getMessage() . " in " . $e->getFile() . " at line " . $e->getLine() . " stack");
        }
    }

    public function delete($id)
    {



        $sitemap = Sitemap::find($id);

        if(! activeUserCan([
            "content.websites.website".$sitemap->website->id.".sitemaps.sitemap".$sitemap->id.".delete",
            "content.websites.website".$sitemap->website->id.".sitemaps.delete",
            "content.websites.sitemaps.delete",
        ])){
            return rejectResponse();
        }
        if ($sitemap) {
            \DB::beginTransaction();
            try {
                $sitemap->details()->delete();
                $sitemap->urls()->delete();
                $sitemap->delete();
                UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $sitemap);

            }catch(\Exception $e){
                \DB::rollback();
            }
            \DB::commit();
        }

        return redirect(route('Content.sitemaps.index'))->with('message', trans('MPCorePanel::general.success_message'));
    }
    // endregion

    //region Ajax Functions
    public function variations(Sitemap $sitemap)
    {
        return response()->json(VariationTemplates::variations($sitemap));
    }

    public function getContent(Sitemap $sitemap, Website $website)
    {
        $sitemap->load(self::DETAILS, "rootCategory.details");
        $data = [];
        foreach ($sitemap->details as $detail) {
            $data[":regex(name,^sitemap_name.[0-9]+.." . $detail->language_id . ".." . ($detail->country_group_id ?? self::C) . ".)"] = $detail->name;
            $pattern = URLEngine::generateUrlString($detail, $detail->countryGroup, $detail->language, $detail->slug);
            $data[":regex(name,^sitemap_pattern.[0-9]+.." . $detail->language_id . ".." . ($detail->country_group_id ?? self::C) . ".)"] = $pattern["pattern"];
            $data[":regex(name,^sitemap_link.[0-9]+.." . $detail->language_id . ".." . ($detail->country_group_id ?? self::C) . ".)"] = slug(Str::ascii($detail->name)) == $pattern["slug"];
            $data[":regex(name,^sitemap_slug.[0-9]+.." . $detail->language_id . ".." . ($detail->country_group_id ?? self::C) . ".)"] = $detail->slug;
            $data[":regex(name,^category_slug.[0-9]+.." . $detail->language_id . ".." . ($detail->country_group_id ?? self::C) . ".)"] = $detail->category_slug;
        }

        return response()->json($data);
    }
    //endregion

    #region Helpers

    public function checkUrl(Request $request)
    {
        $website_id = $request->get(self::WEBSITE_ID) * 1;
        $sitemap_id = $request->get("sitemap_id") * 1;
        $language = $request->get("language") * 1;
        $country = $request->has("country") ? $request->get("country") : null;

        $slug = $request->get("slug");
        $cg = CountryGroup::find($country);
        $smd = SitemapDetail::where("language_id", $language)->where("country_group_id", $cg->id)->first();
        if ($smd == null) {
            $smd = [
                self::TYPE => SitemapDetail::class,
                self::WEBSITE => Website::find($website_id),
                self::SITEMAP => Sitemap::find($sitemap_id)
            ];
        }
        $pattern = URLEngine::generateUrlString($smd, $cg, $language, $slug);
        return response()->json(URLEngine::checkPattern($website_id, $pattern["pattern"], $pattern["slug"]));
    }


    public function checkSubKey()
    {
        return response()->json(SitemapType::where("name", \request(self::SUB_KEY))->count() == 0);
    }

    private function checkUrlRecursive($websiteId, $language = null, $country = null, $url = null, $i = 1)
    {
        $pattern = '';
        $website = Website::find($websiteId);
        $website_default_lang = $website->defaultLanguage();

        if ($website->use_deflng_code == 0) {
            if ($website_default_lang->code == "$language" && $country == null) {
                $pattern = '';
            } else {
                if ($country == null) {
                    $pattern = $language;
                } else {
                    $pattern = "/" . $language . "_" . $country;
                }
            }
        } else {
            if ($country == null) {
                $pattern = $language;
            } else {
                $pattern = "/" . $language . "_" . $country;
            }
        }

        if ($url == "null") {
            return $pattern;
        }
        $query = $i > 1 ? $pattern . $url . "-$i" : $pattern . $url;

        $urlModel = Url::where(['url' => $query, self::WEBSITE_ID => $websiteId])->first();

        if ($urlModel != null) {
            return $this->checkUrlRecursive($websiteId, $language, $country, $url, ++$i);
        } else {
            if ($i > 1) {
                return $pattern . $url . "-$i";
            } else {
                return $pattern . $url;
            }
        }
    }

    private function createRenderables($sitemap)
    {
        /** @var Sitemap $sitemap */

        $creator = new CreateRenderable($sitemap);

        $creator->createSitemapRenderable();
        $sitemap->sitemapType->sitemap_type_type != "static" && $creator->createPagesRenderable();
        $sitemap->property && $creator->createPropertiesRenderable() ;
        $sitemap->category && $creator->createCategoriesRenderable();
        $sitemap->criteria && $creator->createCriteriasRenderable() ;
        unset($creator);
    }

    private function createController($sitemap){
        $name = $sitemap->sitemapType->name;
        $Name = ucfirst($name);

        $folder = app_path(implode(DIRECTORY_SEPARATOR, ["Modules", "Content", "Website" . session("panel.website.id"), "Http", "Controllers", "Web"]));


        $fileCreatePath = $folder . DIRECTORY_SEPARATOR . $Name . "Controller.php";

        if (is_file($fileCreatePath)) {
            return true;
        }

        $class_namespace = "App\Modules\Content\\Website" . session("panel.website.id") . "\Http\Controllers\Web";
        $class_name = $Name . "Controller";

        $data = file_get_contents(base_path('vendor/mediapress/mediapress/src/Modules/Content/AllBuilder/Creator/Controller.template'));

        $replaceKeys = ['{%namespace%}', '{%class%}'];
        $replaceValues = [$class_namespace, $class_name];


        //Default Controller Functions && Blades
        $defaultsKit = new DefaultSitemapKit($sitemap);
        $defaultsKit->init($replaceKeys, $replaceValues);


        if ($sitemap->feature_tag == 'search') {
            $data = file_get_contents(base_path('vendor/mediapress/mediapress/src/Modules/Content/AllBuilder/Creator/SearchController.template'));

            $replaceKeys = ['{%namespace%}', '{%class%}'];
            $replaceValues = [$class_namespace, $class_name];

        }

        $replacedData = str_replace($replaceKeys, $replaceValues, $data);

        if (!file_exists($folder)) {

            $oldmask = umask(0);
            mkdir($folder, 0777, true);
            umask($oldmask);
        }
        file_put_contents($fileCreatePath, $replacedData);

        unset($name, $Name, $folder, $file2create_path, $class_name, $class_namespace, $data, $replaceKeys, $replaceValues, $replacedData);

        return true;

    }

    private function createFiles($sitemap){

        $this->createRenderables($sitemap);
        $this->createController($sitemap);

    }


    public function exportJson()
    {

        $sitemaps = Sitemap::with(['details' => function ($q) {
            $q->with('extras', 'url');
        }, 'extras', 'sitemapType'])->get()->toJson();

        Storage::disk('public')->put('sitemap.json', $sitemaps);


        return response()->download(storage_path('app/public/sitemap.json'))->deleteFileAfterSend();
    }

    public function importJson()
    {

        $sitemap_holds = file_get_contents(request()->file('sitemap_json'));


        foreach (json_decode($sitemap_holds, 1) as $hold) {
            if ($this->sitemapExists($hold) === false) {

                $details = $hold['details'];
                $extras = $hold['extras'];
                $sitemap_type = $hold['sitemap_type'];


                //Sitemap Type
                unset($sitemap_type['id']);
                $sitemap_type_model = SitemapType::firstOrCreate($sitemap_type);

                //Sitemap
                unset($hold['id'], $hold['details'], $hold['urls'], $hold['extras'], $hold['sitemap_type']);
                $sitemap_model = Sitemap::preCreate(["feature_tag" => $hold['feature_tag']]);
                if (!$sitemap_model->websites()->sync([$this->website->id])) {
                    throw new \Exception("Sitemap Modeli Oluşturulamadı : " . json_encode($sitemap_model));
                }

                foreach ($hold as $key => $value) {
                    $sitemap_model->$key = $value;
                }
                $sitemap_model->save();


                //Sitemap Detail & Sitemap Detail Extra & Url
                foreach ($details as $detail) {

                    $detail_extras = $detail['extras'];
                    $url = $detail['url'];

                    unset($detail['id'], $detail['sitemap_id'], $detail['extras'], $detail['url']);
                    $detail['sitemap_id'] = $sitemap_model->id;
                    $sitemap_detail_model = SitemapDetail::firstOrCreate($detail);

                    unset($url['id'], $url['website']);
                    Url::firstOrCreate($url);


                    if (count($detail_extras) > 0) {
                        foreach ($detail_extras as $detail_extra) {
                            unset($detail_extra['id']);
                            $detail_extra['sitemap_detail_id'] = $sitemap_detail_model->id;
                            SitemapDetailExtra::firstOrCreate($detail_extra);
                        }
                    }
                }

                //Sitemap Extras
                if (count($extras) > 0) {
                    foreach ($extras as $extra) {
                        unset($extra['id']);
                        $extra['sitemap_id'] = $sitemap_model->id;
                        SitemapExtra::firstOrCreate($extra);
                    }
                }

            }
        }

        return response()->route('Content.sitemaps.index');

    }

    private function sitemapExists($sitemap)
    {

        unset($sitemap['extras'], $sitemap['sitemap_type']);
        $old_sitemap = Sitemap::with(['details' => function ($q) {
            $q->with('extras', 'url');
        }])->find($sitemap['id']);

        if ($old_sitemap) {
            $old_sitemap = $old_sitemap->toArray();
            $differences = $this->diff_recursive($sitemap, $old_sitemap);
            if (count($differences) == 0) {
                return true;
            }
        }
        return false;

    }

    private function diff_recursive($array1, $array2)
    {
        $difference = array();
        foreach ($array1 as $key => $value) {
            if (is_array($value) && isset($array2[$key])) { // it's an array and both have the key
                $new_diff = $this->diff_recursive($value, $array2[$key]);
                if (!empty($new_diff))
                    $difference[$key] = $new_diff;
            } else if (is_string($value) && !in_array($value, $array2)) { // the value is a string and it's not in array B
                $difference[$key] = $value . " is missing from the second array";
            } else if (!is_numeric($key) && !array_key_exists($key, $array2)) { // the key is not numberic and is missing from array B
                $difference[$key] = "Missing from the second array";
            }
        }
        return $difference;
    }

#endregion


    // region Popup Create & Edit Static Sitemap

    public function popupStatic($sitemap_id=null){

        /*if (!userAction('message.sitemaps.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }*/

        $website_id = session("panel.website.id");


        DB::beginTransaction();
        $feature = \request("feature_tag");
        $feature = $feature == "null" ? null : $feature;
        $isNew = false;

        $variation_templates = null;
        if( ! $sitemap_id){
            if(! activeUserCan([
                "content.websites.website".$website_id.".sitemaps.create",
                "content.websites.sitemaps.create",
            ])){
                return rejectResponse();
            }
            $user_static_smt = SitemapType::firstOrCreate(["name"=>"UserStatic","sitemap_type_type"=>"static","website_id"=>$website_id],[]);
            $sitemap = Sitemap::preCreate(["feature_tag" => $feature, "sitemap_type_id"=>$user_static_smt->id]);
            $isNew = true;
            if ($sitemap) {
                $variation_templates = VariationTemplates::variations(session("panel.website"));
                $sitemap->load('details');
                $sitemap->websites()->sync([$website_id]);
            }
        }else{
            if(! activeUserCan([
                "content.websites.website".$website_id.".sitemaps.sitemap".$sitemap_id.".update",
                "content.websites.website".$website_id.".sitemaps.update",
                "content.websites.sitemaps.update",
            ])){
                return rejectResponse();
            }
            $sitemap = Sitemap::with('details')->whereId($sitemap_id)->first();
        }

        /*return dd(
            $variation_templates = VariationTemplates::variations(session("panel.website")));*/
        if (!$sitemap) {
            DB::rollback();
            return response("Sitemap could not be created/found");
        }

        DB::commit();

        return view('ContentPanel::sitemaps.popupformcontent', compact('variation_templates', 'sitemap', 'isNew'));

    }


    public function popupEditStaticSitemap(Sitemap $sitemap, Website $website){
        /*if (!userAction('message.sitemaps.edit', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }*/


        $sitemap->load([
            self::DETAILS => function ($q) {
                $q->where("website_id", session("panel.website.id"))->withTrashed()->with("urls");
            },
            "sitemapType"]);

        $variation_templates = VariationTemplates::variations($sitemap);

        return view("ContentPanel::sitemaps.popupform", compact("sitemap", "variation_templates"));


    }


    public function popupStaticStore(Request $request){


        //return response()->json($request->all());

        if (!userAction('message.sitemaps.update', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }


        $result_set = [
            'status' => null,
            'model' => null,
            'message' => null
        ];

        //TODO: url check inside
        \DB::beginTransaction();
        try {
            $data = request()->all();

            $sitemap = Sitemap::findOrNew(($data["id"] ?? 0) * 1);
            $existing_details = [];

            if (!$sitemap->exists) {
                $sitemap->admin_id = auth('admin')->id();
                $website_id = isset($data['website_id']) && $data['website_id'] ? $data['website_id'] * 1 : session('panel.website.id');
            } else {
                $details = $sitemap->details()->withTrashed()->get();
                $website_id = $sitemap->website->id;
                foreach ($details as $detail) {
                    $key = $detail->country_group_id . ":" . $detail->language_id;
                    $existing_details[$key] = $detail;
                }
            }


            $sitemap->searchable = isset($data[self::SEARCHABLE]) && filter_var($data[self::SEARCHABLE], FILTER_VALIDATE_BOOLEAN);
            $sitemap->urlStatus = isset($data[self::URL_STATUS]) && filter_var($data[self::URL_STATUS], FILTER_VALIDATE_BOOLEAN);
            $sitemap->detailPage = isset($data[self::HAS_DETAIL]) && filter_var($data[self::HAS_DETAIL], FILTER_VALIDATE_BOOLEAN);
            $sitemap->autoOrder = isset($data[self::AUTO_ORDER]) && filter_var($data[self::AUTO_ORDER], FILTER_VALIDATE_BOOLEAN);
            $sitemap->subPageOrder = isset($data[self::SUB_PAGE_ORDER]) && filter_var($data[self::SUB_PAGE_ORDER], FILTER_VALIDATE_BOOLEAN);
            $sitemap->reservedUrl = isset($data[self::RESERVED_URL]) && filter_var($data[self::RESERVED_URL], FILTER_VALIDATE_BOOLEAN);
            $sitemap->category = isset($data[self::HAS_CATEGORY]) && filter_var($data[self::HAS_CATEGORY], FILTER_VALIDATE_BOOLEAN);
            $sitemap->criteria = isset($data[self::HAS_CRITERIA]) && filter_var($data[self::HAS_CRITERIA], FILTER_VALIDATE_BOOLEAN);
            $sitemap->property = isset($data[self::HAS_PROPERTY]) && filter_var($data[self::HAS_PROPERTY], FILTER_VALIDATE_BOOLEAN);
            $sitemap->filesystem = isset($data[self::FILESYSTEM]) ? $data[self::FILESYSTEM] : config('mediapress.filesystem.public_uploads_default');
            $sitemap->sfilesystem = isset($data[self::SFILESYSTEM]) ? $data[self::SFILESYSTEM] : config('mediapress.filesystem.secure_uploads_default');
            $sitemap->sitemap_id = $data['parent_sitemap_id'] ?? null;
            $sitemap->show_in_panel_menu = isset($data[self::SHOW_IN_PANEL_MENU]) && filter_var($data[self::SHOW_IN_PANEL_MENU], FILTER_VALIDATE_BOOLEAN);
            $sitemap->featured_in_panel_menu = isset($data[self::FEATURED_IN_PANEL_MENU]) && filter_var($data[self::FEATURED_IN_PANEL_MENU], FILTER_VALIDATE_BOOLEAN);
            if ($sitemap->featured_in_panel_menu) {
                Sitemap::where(self::FEATURED_IN_PANEL_MENU, true)->whereHas('websites', function ($q) {
                    $q->where('id', session('panel.website.id'));
                })->update([self::FEATURED_IN_PANEL_MENU => false]);
            }
            $sitemap->status = Sitemap::ACTIVE;

            $sitemapType = SitemapType::firstOrCreate(["name" => "UserStatic", "sitemap_type_type" => "static", "website_id" => $website_id * 1], []);
            $sitemap->sitemap_type_id = $sitemapType->id;
            $sitemap->save();


            UserActionLog::update(__CLASS__ . "@" . __FUNCTION__, $sitemap);

            /*\DB::rollback();
            dump($sitemap);
            dump($sitemap->details()->with("countryGroups.language")->get()->pluck("language","id")->toArray());
            return dd($data["sitemap_active"]);*/

            $sitemap->websites()->sync($website_id);

            $sent_details = [];

            if (isset($data["sitemap_detail_active"])) {
                foreach ($data["sitemap_detail_active"] as $country_group_id => $country_group_languages) {
                    foreach ($country_group_languages as $language_id => $vdata) {

                        $meta_templates = $data["meta_templates"][$country_group_id][$language_id] ?? [];

                        $key = $country_group_id . ":" . $language_id;
                        $sent_details[] = $key;

                        $name = $data["sitemap_name"][$country_group_id][$language_id];
                        $slug = $data["sitemap_slug"][$country_group_id][$language_id];
                        $category_slug = !$sitemap->category ? $sitemap->category_slug : $data["sitemap_catslug"][$country_group_id][$language_id];

                        /** @var SitemapDetail $sitemapDetail */
                        if ($sitemap->exists && array_key_exists($key, $existing_details)) {
                            $detail = $existing_details[$key];
                            $detail->update(
                                [
                                    "name" => $name,
                                    "slug" => $slug,
                                    "category_slug" => $category_slug,
                                    "active" => 1,
                                    'status' => 1,
                                    'meta_templates' => $meta_templates
                                ]
                            );
                            if ($detail->trashed()) {
                                $detail->restore();
                            }
                        } else {
                            $sitemapDetail = $sitemap->details()->updateOrCreate(
                                [
                                    "language_id" => $language_id,
                                    "country_group_id" => $country_group_id,
                                    self::WEBSITE_ID => $website_id
                                ],
                                [
                                    "name" => $name,
                                    "slug" => $slug,
                                    "category_slug" => $category_slug,
                                    "active" => 1,
                                    'status' => 1,
                                    'meta_templates' => $meta_templates
                                ]
                            );
                        }
                    }
                }
            }

            $details_to_delete = array_diff(array_keys($existing_details), $sent_details);

            if (count($details_to_delete)) {

                foreach ($details_to_delete as $dtd) {

                    $separated = preg_split('/:/', $dtd);

                    if (!(strlen($separated[0]) && strlen($separated[1]))) {
                        continue;
                    }

                    $_cgid = $separated[0];
                    $_lngid = $separated[1];

                    $detail_model = $sitemap->details()->where("country_group_id", $_cgid)->where("language_id", $_lngid)->first();
                    if ($detail_model) {
                        $detail_model->delete();
                    }

                }
            }

            $sitemap->load(["details.url"]);

            $result_set["model"] = $sitemap;
            $result_set['status'] = 1;
            $result_set['message'] = 'Sayfa yapısı başarıyla kaydedildi';

            \DB::commit();

            return response()->json($result_set);

        } catch (\Exception $e) {

            \DB::rollBack();

            $err_str = $e->getMessage() . " in " . $e->getFile() . " at line " . $e->getLine() . " stack";
            $result_set['status'] = 0;
            $result_set['message'] = $err_str;

            return response()->json($result_set);

        }




    }
    public function popupRemoveStaticSitemap(){

    }

    // endregion



}

