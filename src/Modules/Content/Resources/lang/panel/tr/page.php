<?php

return [
    'page' => 'Sayfa',
    'pages' => 'Sayfalar',
    'create_page' => 'Sayfa oluştur',
    'edit_page' => 'Sayfa Düzenle',
    'new_page' => 'Yeni Sayfa',
    'name_page' => 'Sayfa Adı',
    'name' => 'Sayfa Adı',
    'sort' => 'Sıra',
    'password_invalid' => 'Geçersiz Şifre',

    'edit_order' => "Sıralama Güncelle",

    'status' => [
        'PASSIVE' => 'PASİF',
        'ACTIVE' => 'AKTİF',
        'DRAFT' => 'TASLAK',
        'PREDRAFT' => 'ÖN TASLAK',
        'PENDING' => 'BEKLEMEDE',
        'POSTDATE' => 'İLERİ TARİHLİ'
    ],
    'goto_page' => 'Sayfaya Git',
    'page_structure_slug' => 'Sayfa Y. Slug',
    'page_content' => 'Sayfa İçeriği',
    'all_categories' => 'Kategori Seçiniz',
    'page_not_found'=>'Sayfa bulunamadı',
    'page_not_found_by_id'=>'Sayfa Bulunamadı (id::id)',

    'preview_box_text' => 'Kaydetmeden önce arama motoru sonucu için herhangi bir anlık görüntü yok.',

    'order_page' => [
      'info' => 'Sıralama güncellendikten sonra diğer sayfaların sıralamaları otomatik güncellenir.',
        'status' => 'Durum',
        'order' => 'Sıra',
        'category_name' => 'Kategori Adı',
        'page_name' => 'Sayfa Adı',
        'active' => 'Aktif',
        'passive' => 'Pasif',
    ],
];
