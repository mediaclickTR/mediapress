<div class="bd-example">
    <div class="row">
        <div class="col">
            <div class="card p-4">
                <div class="card-title color m-0">{{ __("InteractionPanel::dashboard.browser") }}</div>
                <div class="card-body p-0">
                    <div id="browser-pie" class="pieChartAll"></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card p-4">
                <div class="card-title color m-0">{{ __("InteractionPanel::dashboard.os") }}</div>
                <div class="card-body p-0">
                    <div class="center-list">
                        <ul class="m-0 p-0 text-center">
                            @foreach($data['os'] as $key=>$value)
                                <li class="d-inline-block ml-4 mr-4 {!! strtolower($value['name']) !!}">{!! $value['name'] !!}
                                    <span style="color:{!! $value['color'] !!}">%{!! $value['y'] !!}</span>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.graphData.browser = @json($data['browser']);
</script>

<script src="{!! asset('vendor/mediapress/js/dashboardBrowser.js') !!}"></script>
