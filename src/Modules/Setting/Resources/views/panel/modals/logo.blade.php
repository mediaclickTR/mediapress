<div class="modal fade bd-example-modal-lg" id="logo" tabindex="-1" role="dialog"
     aria-labelledby="imageQualityLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageQualityLabel">{!! __('SettingPanel::logo.modal_title') !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('Settings.logo') }}" method="post" id="logo-form" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <img src="{{ settingSun("logo.desktop_logo") ? image(settingSun("logo.desktop_logo")) : asset('vendor/mediapress/images/default.jpg') }}"
                                 class="desktop_logo"
                                 onclick="addLogo(this)"
                                 width="161" height="161">
                            <label class="mt-2">{!! __('SettingPanel::logo.desktop_logo') !!}</label>
                            <input type="file" name="desktop_logo" id="desktop_logo" style="display: none;"/>
                            <i class="fa fa-check" style="display: none"></i>
                        </div>
                        <div class="col-md-3 text-center">
                            <img src="{{ settingSun("logo.mobile_logo") ? image(settingSun("logo.mobile_logo")) : asset('vendor/mediapress/images/default.jpg') }}"
                                 class="mobile_logo"
                                 onclick="addLogo(this)"
                                 width="161" height="161">
                            <label class="mt-2">{!! __('SettingPanel::logo.mobile_logo') !!}</label>
                            <input type="file" name="mobile_logo" id="mobile_logo" style="display: none;"/>
                            <i class="fa fa-check" style="display: none"></i>
                        </div>
                        <div class="col-md-3 text-center">
                            <img src="{{ settingSun("logo.email_logo") ? image(settingSun("logo.email_logo")) : asset('vendor/mediapress/images/default.jpg') }}"
                                 class="email_logo"
                                 onclick="addLogo(this)"
                                 width="161" height="161">
                            <label class="mt-2">{!! __('SettingPanel::logo.email_logo') !!}</label>
                            <input type="file" name="email_logo" id="email_logo" style="display: none;"/>
                            <i class="fa fa-check" style="display: none"></i>
                        </div>
                        <div class="col-md-3 text-center">
                            <img src="{{ settingSun("logo.favicon") ? image(settingSun("logo.favicon")) : asset('vendor/mediapress/images/default.jpg') }}"
                                 class="favicon"
                                 width="161" height="161"
                                 onclick="addLogo(this)">
                            <label class="mt-2">{!! __('SettingPanel::logo.favicon') !!}</label>
                            <input type="file" name="favicon" id="favicon" style="display: none;"/>
                            <i class="fa fa-check" style="display: none"></i>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! __('SettingPanel::general.close') !!}</button>
                <button type="button" class="btn btn-primary" onclick="saveLogo()">{!! __('SettingPanel::general.save') !!}</button>
            </div>
        </div>
    </div>
</div>
