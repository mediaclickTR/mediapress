@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="name">Çalıştırmak istediğiniz testi seçin</h1>
        <div class="pro-lists">
{{--            <div class="top-filter">
                <div class="right">
                    <div class="add">
                        <a href="#"> <i class="fa fa-plus"></i> Link</a>
                    </div>
                </div>
            </div>--}}
            <table class="table">
                <tr>
                    <th>Eylem</th>
                    <th>Parametreler</th>
                    <th>Çalıştır</th>
                </tr>
                <tr>
                </tr>
                @foreach($tests as $test)
                    <tr data-route="{{ $test["route"] }}">
                        <td><strong class="name" style="font-size:1.25em">{{ $test["action"] }}</strong><br/> <small><em>{{$test["route"]}}</em></small></td>
                        <td>
                            <div class="form-group"><input type="text" value="" class="form-control test-params" placeholder="/param1/param2"></div>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary run-test">Çalıştır</button>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
@push("scripts")
    <script>
        $(document).ready(function(e){
            $(".run-test").click(function(e){
                var tr = $(this).closest("tr");

                var url = $(tr).data("route");

                var params = $(".test-params",tr).val();

                if(params!=""){
                    url+=params;
                }

                document.location.href = url;

            });
        });
    </script>
@endpush