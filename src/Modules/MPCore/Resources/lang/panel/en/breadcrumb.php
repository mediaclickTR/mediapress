<?php

return [
    "modules" => [
        "mpcore" => "Mediapress",
        "content" => "Content",
        "heraldist" => "Communication",
        "entity" => "Entity",
        "auth" => "Membership",
    ],
    "sub_modules" => [
        "menus" => "Menus",
        "sitemaps" => "Site Structures",
        "websites" => "Web Sites",
        "categories" => "Categories",
        "panelmenus" => "Panel Menus",
        "ebulletins" => "E-Bulletin",
        "forms" => "Forms",
        "users" => "Members",
        "entitylist" => "Member Lists",
        "translate" => "Language Variables",
        "useractionlogs" => "User Actions",
        "category" => "Category",
        "criteria" => "Criteria",
        "property" => "Properties",
        "admins" => "Administrators",
        "messages" => "Messages"
    ],
    "actions" => [
        "create" => "Add New",
        "store" => "Save",
        "edit" => "Edit",
        "delete" => "Delete",
        "update" => "Update",
        "details" => "Detail",
        "show" => "List",
        "edit-detail" => "Edit Detail",
        "listdetail" => "Detail",
    ],
    "additions" => [
        "mp-admin" => "Dashboard",
        "list" => "List",
        "seo" => "SEO",
        "robot" => "Robot",
        "metas" => "Metas",
        "categorycreate" => "Manage Category",
        "details" => "Detail",
        "show" => "List",
        "edit-detail" => "Edit Detail",
        "listdetail" => "Detail",
    ]
];
