<?php

return [
    'settings'=>'Ayarlar',
    'group-title'=>'Grup Adı',
    'title'=>'Ayar Adı',
    'key'=>'Anahtar Kelime',
    'value'=>'Değer',
    'type'=>'Veri Türü',
    'params'=>'Parametreler',
    'create-setting' => 'Ayar Ekle',
    'edit-setting' => 'Ayar Düzenle',
];