<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Foundation\MPCache;
use Mediapress\Foundation\DataSource;

class DataSourceController extends Controller
{

    public function get($module_name,$ds_name,$params = [])
    {
        if(!userAction('message.datasource.getdatasource',true,false)){
            return redirect()->to(url(route('accessdenied')));
        }

        $array = [
            "App\\DataSources\\$module_name\\$ds_name",
            "Mediapress\\Modules\\$module_name\\DataSources\\$ds_name"
        ];

        foreach ($array as $class)
        {
            if (class_exists($class))
            {
                if ($params){
                    return $class::getData($params);
                }
                else{
                    return $class::getData();
                }
            }
        }

        throw new \InvalidArgumentException ("DataSource not found: $module_name/DataSources/$ds_name");
    }

    public function post(Request $request)
    {
        if(!userAction('message.datasource.postdatasource',true,false)){
            return redirect()->to(url(route('accessdenied')));
        }

        $array = [
            "App\\DataSources\\$request->module_name\\$request->ds_name",
            "Mediapress\\Modules\\$request->module_name\\DataSources\\$request->ds_name"
        ];

        foreach ($array as $class)
        {
            if (class_exists($class))
            {
                if ($request->params){
                    return $class::getData($request->params);
                }
                else{
                    return $class::getData();
                }
            }
        }

        throw new \InvalidArgumentException("DataSource not found: $request->module_name/DataSources/$request->ds_name");
    }
}