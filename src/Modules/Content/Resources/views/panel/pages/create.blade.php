@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! trans("ContentPanel::page.create_page") !!}</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    <div class="asd">
                        {{--@include("ContentPanel::pages.form")--}}
                        @if( isset($renderable) && is_a($renderable, \Mediapress\AllBuilder\Foundation\BuilderRenderable::class))
                            @php $renderable->render(); @endphp
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.mp-checkbox-wrapper ul li:last-child').click(function () {
                $('.mp-checkbox-wrapper ul .switch-toggle').addClass('on');
                $('.mp-checkbox-wrapper ul .switch-toggle').removeClass('off');
            });
            $('.mp-checkbox-wrapper ul li:first-child span').click(function () {
                $('.mp-checkbox-wrapper ul .switch-toggle').addClass('off');
                $('.mp-checkbox-wrapper ul .switch-toggle').removeClass('on');
            });
        });
    </script>
@endsection
