<?php

namespace Mediapress\Modules\Content\Models;
use Illuminate\Database\Eloquent\Model;


class SitemapWebsite extends Model
{
    protected $table = 'sitemap_website';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['website_id', 'sitemap_id', 'model_id', 'file_key', 'ordernum', 'detail'];
    
    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }
    
    public function website()
    {
        return $this->belongsTo(Website::class);
    }
    
}
