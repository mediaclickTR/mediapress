@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.edit_maillist") !!}</h1>
        <div class="col-md-12 pa">
            <form action="{!! route('Heraldist.mpmailler.maillists.update') !!}" novalidate method="POST">
                @include("HeraldistPanel::mail_service.mpmailler.maillists.form")
            </form>
        </div>
    </div>
@endsection
