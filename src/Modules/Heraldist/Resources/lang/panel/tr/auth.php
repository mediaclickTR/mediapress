<?php

return [
    "sections"=>[
        "message"=>"İleti",
        "messages"=>"İletiler",
        "form"=>"Form",
        "forms"=>"Formlar",
        "ebulletin_list"=>"E-Bülten Listesi",
        "ebulletin_lists"=>"E-Bülten Listeleri",
        "heraldist_module_abilities"=>"Heraldist Modülü Yetkileri"
    ],
    "actions"=>[
        "list_inbox"=>"Gelen K. Göster",
        "watch_flow_record"=>"Kaydı İzle",
        "read_message"=>"İletiyi Oku",
        "mark_as_unread"=>"Okunmadı O. İşaretle",
        "download_attachments"=>"Ekleri İndir"
    ]
];
