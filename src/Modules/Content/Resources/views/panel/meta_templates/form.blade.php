{!! Form::open(['route' => ['Content.meta_templates.update'],'method' => 'POST'])  !!}
{!! Form::hidden("id", $sitemap->id) !!}

@php
    $website_variation_data = $variation_templates[session("panel.website.id")];
@endphp

<div id="meta_templates_main_row" class="row">
    <div class="container">
        <div class="title mb-4 mt-5">{!! trans('ContentPanel::seo.meta_defaults') !!}</div>

        <div class="alert alert-info alert-dismissible in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Kapat</span>
            </button>
            {!! trans('ContentPanel::seo.info') !!}
        </div>

        @php
            $scopes = [
                "sitemap_detail" => trans('ContentPanel::seo.scopes.page_structure'),
                "category_detail" => trans('ContentPanel::seo.scopes.categories'),
                "page_detail" => trans('ContentPanel::seo.scopes.pages'),
            ];
        @endphp

        <div>
            <ul class="nav nav-tabs">
                @foreach($website_variation_data["country_groups"] as $cgid => $cgvd)
                    @foreach($cgvd["languages"] as $lng_code=>$lng)
                        <li
                            data-country-group-id="{{$cgid}}"
                            data-language-id="{{$lng["id"]}}">
                            <a class="@if($loop->parent->first && $loop->first)show active @endif"
                               data-toggle="tab"
                               href="#{!! $cgvd["code"].$lng["code"] !!}">
                                {!! getVariationLocalTitle($cgvd["title"], $lng["code"]) !!}
                            </a>
                        </li>
                    @endforeach

                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($website_variation_data["country_groups"] as $cg_id => $cgvd)
                    @foreach($cgvd["languages"] as $lng_id=>$lng)
                        <div id="{!! $cgvd["code"].$lng["code"] !!}"
                             class="tab-pane fade in @if($loop->parent->first && $loop->first)active show @endif"
                             data-country-group-id="{{$cgid}}"
                             data-language-id="{{$lng["id"]}}">

                            @php
                                $detail = $sitemap->details->where("country_group_id",$cg_id)->where("language_id",$lng_id)->first();
                                $templates = \Mediapress\Modules\Content\Facades\Content::getMetaTemplatesOf($sitemap, $detail);
                                /*if($detail){
                                    $details_templates = \Mediapress\Modules\Content\Facades\Content::getMetaTemplatesOf($detail, $detail);
                                    $templates["sitemap_detail"] = array_merge($templates["sitemap_detail"], $details_templates );
                                }*/

                            @endphp

                            <div class="accordion row">
                                @foreach($scopes as $scope=> $scope_title)

                                    <div class="card mbActive @if($scope=="category_detail"){!! "category-field" !!} @endif">
                                        <div class="card-header">
                                            <h5 class="mb-0">
                                                <button type="button" class="btn btn-link collapsed"
                                                        data-toggle="collapse"
                                                        data-target="#{!! $scope !!}Templates{!! $cgvd["code"].$lng["code"] !!}"
                                                        aria-expanded="true"
                                                        aria-controls="collapseOne">{{ $scope_title }}</button>
                                            </h5>
                                        </div>
                                        <div id="{!! $scope !!}Templates{!! $cgvd["code"].$lng["code"] !!}"
                                             class="collapse" data-parent=".accordion">
                                            <div class="card-body">

                                                <div class="col-12 detail-meta-templates-editor"
                                                     data-language-id="{!! $lng_id !!}"
                                                     data-country-group-id="{!! $cg_id !!}"
                                                     data-object-scope="{!! $scope !!}">
                                                    <div class="clearfix"></div>
                                                    <div class="add-meta-key">
                                                        <button class="float-right btn" type="button">
                                                            <i class="fa-plus fa"></i>
                                                            {{ __('ContentPanel::seo.add.meta.key') }}
                                                        </button>
                                                    </div>
                                                    <div class="box-list desktop p-0">
                                                        <div class="head">
                                                            <div class="clearfix"></div>
                                                            <img src="/vendor/mediapress/images/web-site-icon.png">
                                                            {{ __('ContentPanel::seo.desktop') }}
                                                        </div>
                                                        <div class="select-item">
                                                            @foreach($templates[$scope] as $template_key => $template)
                                                                <div class="lists"
                                                                     data-meta-name="{{$template_key}}"
                                                                     data-meta-device="desktop">
                                                                    <div class="float-none w-auto row">
                                                                        <div class="col-sm-2">
                                                                            <small class="sCounter"
                                                                                   data-limit-low="55"
                                                                                   data-limit-high="65">
                                                                                {{ $template_key }}
                                                                                <em style="color: rgb(0, 128, 0);">0</em>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-8"
                                                                             style="padding-left: 0">
                                                                            <div class="rich-meta-input"
                                                                                 spellcheck="false"
                                                                                 contenteditable="true">{{ $template["desktop_value"] }}</div>
                                                                        </div>
                                                                        <div class="col-sm-2"
                                                                             style="padding-left: 0">
                                                                            <div class="add-variable"
                                                                                 style="padding-left: 0;">
                                                                                <span>{{ __('ContentPanel::seo.add.variable') }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                    <div class="meta-variables-modal">
                                                        <div class="centered">
                                                            <div class="clse">×</div>
                                                            <input type="text" placeholder="Ara...">
                                                            <ul>
                                                                @foreach($flatten_variables as $variable_key => $variable)
                                                                    <li data-meta-key="{{ $variable_key }}"
                                                                        data-meta-value=""
                                                                        title="{{ $variable["description"] }}">{{ $variable["title"] }}</li>
                                                                @endforeach
                                                            </ul>
                                                            <button class="btn" type="button">
                                                                {{ __('ContentPanel::seo.add.variable') }}
                                                            </button>
                                                        </div>
                                                    </div>

                                                    @foreach($templates[$scope] as $template_key => $template)
                                                        <input class="meta_base_input" type="hidden"
                                                               value="{!! $template["desktop_value"] !!}"
                                                               name="meta_templates[{!! $cg_id !!}][{!! $lng_id !!}][{!! $scope !!}][{!! $template_key !!}][desktop_value]"
                                                               data-meta-name="{!! $template_key !!}"
                                                               data-meta-device="desktop">
                                                    @endforeach
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>

    </div>
</div>

<div
    class="submit mt-5">{!! Form::button(trans("MPCorePanel::general.save"),["type"=>"submit", "class"=>"btn btn-primary float-right"]) !!}</div>

{!! Form::close() !!}

@push("scripts")
    <script>
        var xhr_requests = [];

        var extracted_meta_variables = JSON.parse('{!! json_encode($flatten_variables) !!}');

        var convertVariablesToBoxes = function (el) {
            var value = rhtmlspecialchars($(el).html());
            $.each(extracted_meta_variables, function (meta_key, meta_data) {
                var re = new RegExp('%' + meta_key + '%', "g");
                if (re.test(value)) {
                    var box = createVariableBox(meta_key, meta_data.value, meta_data.title, meta_data.description);
                    value = value.replace(re, box);
                }
            });
            $(el).html(value);
        };

        var createVariableBox = function (meta_key, meta_value, meta_title, meta_desc) {
            return '<span contenteditable="false" data-meta-key="' + meta_key + '" data-meta-value="' + meta_value + '" title="' + meta_desc + '">' + meta_title + '<i>x</i></span><em contenteditable="false">&nbsp;</em>';
        };

        $(document).ready(function () {

            $('.rich-meta-input').each(function (ind, el) {
                convertVariablesToBoxes(el);
            });

            $("select[name=type]").change(function () {
                var selection = $(this).val();
                if (selection == "dynamic") {
                    $("#meta_templates_main_row").removeClass("d-none");
                } else {
                    $("#meta_templates_main_row").addClass("d-none");
                }
            });
        });
    </script>
@endpush
