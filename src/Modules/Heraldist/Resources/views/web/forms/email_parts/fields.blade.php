@if(isset($field))

    @if(in_array($field['tag'],['h1','h2','h3','h4','h5','h6']))
        @include('HeraldistWeb::forms.email_parts.h',['field'=>$field])
    @else
        @include('HeraldistWeb::forms.email_parts.'.$field['tag'],['field'=>$field])
    @endif

@endif