@php
    $sitemap = $mediapress->data['sitemap'];
    $page = $mediapress->data['page'];
    $criterias = $mediapress->data['criterias'] ?? [];
@endphp
<div class="container">
    <h1 class="my-4">{!! $sitemap->detail->name !!}</h1>

    @include("ContentWeb::default.layouts.breadcrumb")

    <div class="row">
        <div class="col-md-4">
            @if($page->f_)
                @if(is_a($page->f_, 'Illuminate\Database\Eloquent\Collection'))
                    <img class="img-fluid" src="{!! $page->f_->first() !!}" alt="">
                @else
                    <img class="card-img-top img-fluid" src="{!! $page->f_ !!}" alt="">
                @endif
            @else
                <img class="card-img-top img-fluid" src="http://placehold.it/380x230" alt="">
            @endif

            <ul>
                @foreach($criterias as $criteria => $value)
                    <li>
                        {!! $criteria !!}: <span>{!! $value !!}</span>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-md-8">

            <h3 class="my-3">{!! $page->detail->name !!}</h3>
            {!! $page->detail->detail !!}


            <div class='row'>
                @if(is_a($page->f_gallery, 'Illuminate\Database\Eloquent\Collection'))
                    @foreach($page->f_gallery as $gallery)
                        <a href="{!! $gallery !!}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
                            <img src="{!! image($gallery->id)->resize(['w' => 250, 'h' => 250]) !!}"
                                 class="img-fluid rounded">
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet">
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

    <script>
        $(document).on("click", '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
@endpush
