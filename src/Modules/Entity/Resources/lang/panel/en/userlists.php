<?php

return [
    'userlists' => 'Member List',
    'list-members' => 'List Members',
    'list-members-create' => 'Add Members to List',
    'edit-userlist' => 'Update List',
    'name-userlist' => 'List Name',
    'description-userlist' => 'List Description',
    'create-userlist' => 'Create List',
];
