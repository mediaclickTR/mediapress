@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("EntityPanel::entitylistdetail.user-detail") !!}</div>
        <form action="#">
            <div class="row">
                <div class="col-md-4 ">
                    <span style="font-weight: bold;">{!! trans("MPCorePanel::general.name") !!}</span>
                </div>
                <div class="col-md-4 ">
                    <span style="font-weight: bold;">{!! trans("MPCorePanel::general.surname") !!}</span>
                </div>
                <div class="col-md-4 ">
                    <span style="font-weight: bold;">{!! trans("MPCorePanel::general.email") !!}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 ">
                    <span>{!! $moduluser != null ? $moduluser->name : '-' !!}</span>
                </div>
                <div class="col-md-4 ">
                    <span>{!! $moduluser != null ? $moduluser->surname : '-' !!}</span>
                </div>
                <div class="col-md-4 ">
                    <span>{!! $moduluser != null ? $moduluser->email : '-' !!}</span>
                </div>
            </div>
        </form>
    </div>
@endsection
