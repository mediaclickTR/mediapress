<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelLanguagePartsTable extends Migration {

	public function up()
	{
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->create('panel_language_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id');
            $table->integer('language_id');
            $table->string('key');
            $table->text('value');
        });
	}

	public function down()
	{
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->dropIfExists('panel_language_parts');
	}
}
