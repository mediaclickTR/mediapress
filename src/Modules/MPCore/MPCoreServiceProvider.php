<?php

namespace Mediapress\Modules\MPCore;

use Mediapress\Modules\Module as ServiceProvider;
use Mediapress\Modules\MPCore\Facades\MetaEngine;
use Mediapress\Modules\MPCore\Foundation\FilterEngine;
use Mediapress\Modules\MPCore\Foundation\TranslateEngine;
use Mediapress\Modules\MPCore\Foundation\URLEngine;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Barryvdh\Debugbar\Facade as DebugbarFacade;
use Illuminate\Support\Facades\Blade;
use Cache;

class MPCoreServiceProvider extends ServiceProvider
{
    public const RESOURCES = 'Resources';
    public const DATABASE = 'database';
    /**
     * Bootstrap services.
     *
     * @return void
     */
    protected $module_name = "MPCore";
    protected $namespace = 'Mediapress\Modules\MPCore';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->customBladeDirectives();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'web', 'mediapress');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        //$this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name . 'Database');
        //$this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Public' => public_path('vendor/mediapress')], $this->module_name . 'Public');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Publishes' . DIRECTORY_SEPARATOR . 'config' => config_path()], $this->module_name . 'Publishes');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Storage' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'analytics' => storage_path('app/analytics')], $this->module_name . 'Storage');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' . DIRECTORY_SEPARATOR . 'sqlite' => storage_path('app' . DIRECTORY_SEPARATOR . self::DATABASE)], $this->module_name . 'BackupDatabase');
        $this->publishActions(__DIR__);

        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);

            $this->mergeConfig($file, $filename);
        }

        if (!$this->app->runningInConsole()) {
            Cache::rememberForever('settingSun', function () {
                return SettingSun::get();
            });
        }
    }

    public function register()
    {
        $this->setCacheBuilder();
        $this->setCacheFileNotDeleted();
        $this->app['config']->set("database.connections.mediapress_backup", ['driver' => 'sqlite', self::DATABASE => storage_path('app' . DIRECTORY_SEPARATOR . self::DATABASE . DIRECTORY_SEPARATOR . 'Backup.sqlite'), 'prefix' => '']);
        config(['debugbar.enabled' => true]);
        if ($this->app->request->get('debugbar') == 1) {
            DebugbarFacade::enable();
            \Config::set('app.debug', true);
        }


        $this->app->bind('MPCore', \Mediapress\Modules\MPCore\MPCore::class);
        $this->app->bind('UserActionLog', \Mediapress\Modules\MPCore\Foundation\UserActionLog::class);
        $this->app->bind('MetaEngine', \Mediapress\Modules\MPCore\Foundation\MetaEngine::class);
        $this->app->bind('TranslateEngine', \Mediapress\Modules\MPCore\Foundation\TranslateEngine::class);
        $this->app->bind('FilterEngine', \Mediapress\Modules\MPCore\Foundation\FilterEngine::class);

        $this->app->singleton(MPCore::class);
        $this->app->singleton(UserActionLogs::class);
        $this->app->singleton("URLEngine", URLEngine::class);
        $this->app->singleton(MetaEngine::class);
        $this->app->singleton(TranslateEngine::class);
        $this->app->singleton(FilterEngine::class);

    }

    private function setCacheBuilder()
    {

        $cachebuilder = [
            'driver' => 'file',
            'path' => storage_path('framework/cache/sql')
        ];
        \Config::set('cache.stores.request', $cachebuilder);
    }

    private function setCacheFileNotDeleted()
    {

        $cachebuilder = [
            'driver' => 'file',
            'path' => public_path('vendor/cache'),
        ];
        \Config::set('cache.stores.file_not_deleted', $cachebuilder);
    }


    protected function mergeConfig($path, $key)
    {

        $config = config($key);

        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key => $config]);
    }

    protected function customBladeDirectives()
    {
        Blade::directive('pushonce', function ($expression) {
            $domain = explode(':', trim(substr($expression, 1, -1)));
            $push_name = $domain[0];
            $push_sub = $domain[1];
            $isDisplayed = '__pushonce_' . $push_name . '_' . $push_sub;
            return "<?php if(!isset(\$__env->{$isDisplayed})): \$__env->{$isDisplayed} = true; \$__env->startPush('{$push_name}'); ?>";
        });
        Blade::directive('endpushonce', function ($expression) {
            return '<?php $__env->stopPush(); endif; ?>';
        });
    }
}
