<?php
return [

    "googletagmanager" => [
        'name' => 'GoogleTagManagerPanel::auth.sections.googletagmanager',
        'node_type' => "grouper",
        "name_affix" => "",
        "item_model" => "",
        "item_id" => "*",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        "actions" => [
            'auth' => [
                'name' => "MPCorePanel::auth.actions.auth",
                "icon" =>"fa-shield-alt",
            ],
            'index' => [
                'name' => "MPCorePanel::auth.actions.view",
            ],
        ],
        "subs"=>[
        ]

    ]
];
