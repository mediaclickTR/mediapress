<?php

namespace Mediapress\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use View;

class AuthenticateMiddleware
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string[] ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if(request()->segment(1) == 'mp-admin'){


        if (!$guards) {
            $guards = ['admin'];
        }


        $this->authenticate($guards);

        $websiteCheck = Website::count();

        if (!$websiteCheck) {
            $this->installMediapress();
        }

        // Session check
        if (!session("panel.website") || !session("panel.user")) {

            return redirect(panel('login').'?next='.urlencode($request->url()));
        }
        // Set App Locale (Session active language)
        app()->setLocale(session("panel.active_language")->code);

        // General Panel Men� Share
        $general_menus = MPCore::buildMenu();
        View::share('general_menus', $general_menus);

        return $next($request);
        }else{
            if(!Auth::check()){
             return redirect(route('login').'?next='.urlencode($request->url()));
            }
            return $next($request);
        }
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  array $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate(array $guards)
    {

        if (empty($guards)) {
            return $this->auth->authenticate();
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        return 1;
        throw new AuthenticationException('Unauthenticated.', $guards);
    }

    private function installMediapress()
    {
        $request = Request::capture();
        $name = $request->getHost();

        $website = new Website(
            [
                'type' => 'domain',
                'target' => 'internal',
                'sort' => 1,
                'name' => $name,
                'slug' => $name,
                'ssl' => 0,
                'default' => 0,
                'use_deflng_code' => 0,
                'regions' => 1,
                'variation_template' => '{lng}',
                'admin_id' => 0,
                'status' => 1,
                'order' => 1,
            ]
        );
        $website->save();
        session(["panel.website" =>$website]);


    }
}
