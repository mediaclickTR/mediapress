<?php

namespace Mediapress\Http\Controllers;

use Illuminate\Support\Facades\App;
use Mediapress\Http\Controllers\Controller as BaseController;
use Mediapress\Modules\Content\Models\Website;
use Illuminate\Support\Facades\Auth;
use Mediapress\Modules\Auth\Models\Role;
use Silber\Bouncer\BouncerFacade as Bouncer;

class PanelController extends BaseController {

    public const PANEL_WEBSITE = "panel.website";
    public const PANEL_LANGUAGES = "panel.languages";
    public const PANEL_ACTIVE_LANGUAGE = "panel.active_language";
    public $admin;
    public $website;
    public $websites;

    public function __construct()
    {
        Bouncer::useRoleModel(Role::class);
    }
    public function callAction($method, $parameters)
    {
        $this->website = $this->getWebsiteSession();

    if (Auth::guard('admin')->check())
    {
        $this->admin = Auth::guard('admin')->getUser();
    }
    Bouncer::useRoleModel(Role::class);

    return parent::callAction($method, $parameters);
    }

    public function getWebsiteSession()
    {
        if (session(self::PANEL_WEBSITE) == null)
        {
            $this->setWebsiteSession();
        }
        return session(self::PANEL_WEBSITE);
    }

    public function setWebsiteSession()
    {
        $default_website = Website::where('default', "1")->first();
        if (isset($default_website)){
            session([self::PANEL_WEBSITE =>$default_website]);
        }else{
            // Default site yoksa ilk eklenmiş siteyi ekle
            session([self::PANEL_WEBSITE => Website::orderBy("id", "ASC")->first()]);
        }
    }

    public function getPanelLanguagesSession()
    {
        if (session(self::PANEL_LANGUAGES) == null) {
            $this->setPanelLanguagesSession();
        }
        return session(self::PANEL_LANGUAGES);
    }

    public function setPanelLanguagesSession($language_id = null)
    {
        if ($language_id) {
            session(
                [self::PANEL_LANGUAGES => [
                    "default"=> $language_id,
                    "languages"=> config("defaultPanelLanguages.languages")
                ]]
            );
        }else {
            // Eğer ön yüzde panelLanguages config dosyası yoksa mediapress tarafında default language config dosyasını oku.
            if (config("panelLanguages") == null){
                $languages = config("defaultPanelLanguages");
            }else{
                $languages = config("panelLanguages");
            }
            session([self::PANEL_LANGUAGES => $languages]);
        }

    }

    public function getActiveLanguageSession()
    {
        if (session(self::PANEL_ACTIVE_LANGUAGE) == null)
        {
            $this->setPanelActiveLanguageSession();
        }
        return session(self::PANEL_ACTIVE_LANGUAGE);
    }

    public function setPanelActiveLanguageSession()
    {
        $user = auth('admin')->user();

        if($user && $user->language_id) {
            $langId = $user->language_id;
        } else {
            $langId = session("panel.languages.default");
        }

        $language = \Mediapress\Modules\MPCore\Models\Language::find($langId);
        if ($language){
            session([self::PANEL_ACTIVE_LANGUAGE => $language]);
        }else{
            session([self::PANEL_ACTIVE_LANGUAGE => null]);
        }
    }

    public function getUserSession()
    {
        if (session("user_id") == null)
        {
            $this->setUserSession();
        }

        return session("user_id");
    }

    public function setUserSession($user)
    {
        session(["panel.user" => $user]);
    }
}
