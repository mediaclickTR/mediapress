<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Support\Database\CacheQueryBuilder;

class PageDetailExtra extends BaseModel
{

    use CacheQueryBuilder;
    protected $table = 'page_detail_extras';

    protected $fillable = ["page_id", "page_detail_id", "key", "value"];

    public $timestamps = true;

    public function pageDetail()
    {
        return $this->belongsTo(PageDetail::class);
    }

    public function parent()
    {
        return $this->pageDetail->parent();
    }

    public function getValueAttribute($value)
    {
        return supFront($value, 'value', $this);
    }
}
