<?php
/**
 * Created by PhpStorm.
 * User: Saban Koparal
 * E-Mail: saban.koparal@mediaclick.com.tr
 */

Route::group(['middleware' => 'panel.auth'], function()
{
    Route::group(['prefix' => 'Entity', 'as'=>'Entity.'], function () {


        /**
         * Base Modules
         * Include entites, users, userlists
         */

        Route::group(['prefix' => 'Entities', 'as' => 'entities.'], function()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'EntityController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'EntityController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'EntityController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'EntityController@edit']);
            Route::post( '/update', ['as' => 'update', 'uses' => 'EntityController@update']);
        });

        Route::group(['prefix' => 'Users', 'as' => 'users.'], function()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'UserController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'UserController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'UserController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'UserController@edit']);
            Route::post( '/update', ['as' => 'update', 'uses' => 'UserController@update']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'UserController@delete']);
        });

        Route::group(['prefix' => 'Entities', 'as' => 'entities.'], function()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'EntityController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'EntityController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'EntityController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'EntityController@edit']);
            Route::post( '/update', ['as' => 'update', 'uses' => 'EntityController@update']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'EntityController@delete']);
        });

        Route::group(['prefix' => 'Entitylist', 'as' => 'entitylist.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'EntitylistController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'EntitylistController@create']);
            Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'EntitylistController@edit']);
            Route::post('/store', ['as' => 'store', 'uses' => 'EntitylistController@store']);
            Route::post( '/update', ['as' => 'update', 'uses' => 'EntitylistController@update']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'EntitylistController@delete']);


            Route::group(['prefix' => 'ListDetail/{listid}', 'as' => 'listdetail.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'EntitylistDetailController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'EntitylistDetailController@create']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'EntitylistDetailController@edit']);
                Route::get('/detail/{id}', ['as' => 'detail', 'uses' => 'EntitylistDetailController@detail']);
                Route::post('/store', ['as' => 'store', 'uses' => 'EntitylistDetailController@store']);
                Route::post( '/update', ['as' => 'update', 'uses' => 'EntitylistDetailController@update']);
                Route::get('/delete/{id}', ['as' => 'delete', 'uses' => 'EntitylistDetailController@delete']);
                Route::post('/selectedRemove', ['as' => 'selectedRemove', 'uses' => 'EntitylistDetailController@selectedRemove']);
            });

        });

    });
});
