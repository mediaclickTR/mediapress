<?php

return [
    'panelmenus'=>'Panel Menus',
    'name'=>'Menu Name',
    'create_menu'=>'Create Menu',
    'edit_menu'=>'Edit Menu',
    'sitemap'=>'Page Structure',
    'empty_menu_detail'=>'Details of the <b>:menu</b> menu could not be found. You can add it from the right side.',
    'info' => 'The new title you will add, will be added to the bottom on the left side. Drag this tittle to the place you want and press the save button.',
    'select_sitemap' => 'Select Sitemap.',
];
