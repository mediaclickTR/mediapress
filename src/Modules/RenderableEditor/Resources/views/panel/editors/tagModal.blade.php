<div class="modal fade" id="tagModal" tabindex="10" role="dialog" aria-labelledby="tagmodal2"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="javascript:updateTags();" class="form-holder-ajax-forms">
                <div class="modal-header">
                    <h5 class="modal-title" id="#MPFile">MPFile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="id">
                    <input type="hidden" name="holder">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">File Type</label>
                            </div>
                            <div class="col-md-2">
                                <div class="custom-control custom-radio">
                                    <input onclick="javascript:changeType(1)" type="radio"
                                           class="custom-control-input" id="r2" value="image"
                                           name="file_type">
                                    <label onclick="javascript:changeType(1)" class="custom-control-label" for="r2">Image</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="custom-control custom-radio">
                                    <input onclick="javascript:changeType(2)" type="radio"
                                           class="custom-control-input" id="r3" value="file"
                                           name="file_type">
                                    <label onclick="javascript:changeType(2)" class="custom-control-label"
                                           for="r3">File</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Required ?</label>
                            </div>
                            <div class="col-md-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="r4" value="required"
                                           name="required">
                                    <label class="custom-control-label" for="r4">Yes</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="r5" value=""
                                           name="required">
                                    <label class="custom-control-label" for="r5">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">File key</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="key" class="form-control" required
                                       placeholder="File Upload Key ">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Friendly Name</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="title" class="form-control" required
                                       placeholder="Upload Title Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Extentions</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="extensions" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="imageoptions" style="display: none">

                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Min Width</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="min_width" class="form-control"
                                           placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Max Width</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="max_width" class="form-control"
                                           placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Min Height</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="min_height" class="form-control"
                                           placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Max Height</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="max_height" class="form-control"
                                           placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Min File Size (Kb)</label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="min_filesize" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Max File Size (Kb)</label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="max_filesize" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Additional Rules</label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="additional_rules" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Max File For This Section</label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="max_file_count" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
