@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.emailtemplates") !!}</h1>

            @if(count($mailtemplates)==0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>

                    <div class="float-right">
                        <a href="{!! route('Heraldist.mpmailler.mail_templates.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                    </div>
            @else
                <div class="float-left">
                    <select class="nice" id="multiple-processings" onchange="locationOnChange(this.value);">
                        <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                        <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!}  value="?list=all">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                    </select>
                    @if(request()->get('list') && request()->get('list')=="all")
                        <a href="{!! route("Heraldist.mpmailler.mail_templates.index") !!}" class="filter-block">
                            <i class="fa fa-remove"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                        </a>
                    @endif
                </div>
                <div class="float-right">
                    <a href="{!! route('Heraldist.mpmailler.mail_templates.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
            <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.website") !!}</th>
                    <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                <tr>
                </tr>
                @foreach($mailtemplates as $template)
                    <tr>
                        <td>{!! $template->name !!}</td>
                        <td>
                            @if($template->websites())
                                @foreach($template->websites as $site)
                                    {!! $site->domain !!}
                                @endforeach
                            @endif
                        </td>
                        <td>{!! $template->created_at !!}</td>
                        <td>
                            <select onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.mail_templates.edit',$template->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.mail_templates.show',$template->id) !!}">{!! trans("MPCorePanel::general.preview") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.mail_templates.delete',$template->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="float-left">
                {!! $mailtemplates->links() !!}
            </div>
            <div class="float-right">
                <select class="nice">
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
        @endif
    </div>
@endsection
