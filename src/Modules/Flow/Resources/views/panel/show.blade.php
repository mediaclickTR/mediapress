<!DOCTYPE html>
<html id="ng-app" lang="tr" ng-app="piwikApp">
<head>
    <meta charset="utf-8">
    <title>Replay Session Recording - Matomo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
    <meta name="viewport" content="initial-scale=1.0"/>
    <meta name="generator" content="Matomo - free/libre analytics platform"/>
    <meta name="description" content=""/>

    <meta name="google" content="notranslate">
    <meta name="robots" content="noindex,nofollow">

    <link rel="shortcut icon" href="plugins/CoreHome/images/favicon.png"/>
    <link rel="icon" type='image/png' sizes='256x256' href="plugins/CoreHome/images/applogo_256.png"/>
    <link rel="mask-icon" href="plugins/CoreHome/images/applePinnedTab.svg" color="#3450A3">
    <meta name="theme-color" content="#3450A3">
    <script type="text/javascript">
        var piwik = {};
        piwik.token_auth = "d7f380402c48e75fc6af0a4c97edf5ea";
        piwik.piwik_url = "https://3renktest.matomo.cloud/";
        piwik.cacheBuster = "be675af471ec1879e35281e14742d8af";
        piwik.timezoneOffset = 3600;
        piwik.numbers = {
            patternNumber: "#,##0.###",
            patternPercent: "%#,##0",
            patternCurrency: "¤#,##0.00",
            symbolPlus: "+",
            symbolMinus: "-",
            symbolPercent: "%",
            symbolGroup: ".",
            symbolDecimal: ","
        };

        piwik.userLogin = "gorkem.duymaz";
        piwik.idSite = "1";
        piwik.siteName = "3renk.test";
        piwik.siteMainUrl = "http\u003A\/\/3renk.test";
        piwik.period = "year";
        piwik.currentDateString = "2019-04-08";
        piwik.startDateString = "2019-04-07";
        piwik.endDateString = "2019-04-09";
        piwik.minDateYear = 2019;
        piwik.minDateMonth = parseInt("04", 10);
        piwik.minDateDay = parseInt("07", 10);
        piwik.maxDateYear = 2019;
        piwik.maxDateMonth = parseInt("04", 10);
        piwik.maxDateDay = parseInt("09", 10);
        piwik.language = "tr";
        piwik.hasSuperUserAccess = 1;
        piwik.userCapabilities = ["tagmanager_write", "tagmanager_publish_live_container", "tagmanager_use_custom_templates"];
        piwik.config = {};
        piwik.config = {
            "action_url_category_delimiter": "\/",
            "action_title_category_delimiter": "",
            "autocomplete_min_sites": 5,
            "datatable_export_range_as_day": "rss",
            "datatable_row_limits": ["5", "10", "25", "50", "100", "250", "500", "-1"],
            "are_ads_enabled": true
        };
        piwik.shouldPropagateTokenAuth = false;
        var piwikExposeAbTestingTarget = true;
        piwik.heatmapWriteAccess = true;
        piwik.hasServerDetectedHttps = true;
        piwik.languageName = 'Türkçe';
        var cloudAllowedFeatures = {};
        piwik.isReadOnlyProxySite = true;
        piwik.proxySiteOverlayLinkSuffix = "__enterprise_demo=3renktest.matomo.cloud&__enterprise_demo_idsite=1";
        piwik.whiteLabelRemoveLinks = 0;
    </script>
    <link rel="stylesheet" type="text/css" href="{!! asset('vendor/mediapress/flow/style.css') !!}"/>

    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/translations.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/core.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/nonCore.js') !!}"></script>


</head>
<body id="" ng-app="app" class="">


<script type="text/javascript">
    $(function () {
        $('body').css("display", "none");
        if (self == top) {
            var theBody = document.getElementsByTagName('body')[0];
            theBody.style.display = 'block';
        } else {
            top.location = self.location;
        }
    });
</script>
<noscript>
    <div id="javascriptDisabled">Matomo uygulamasını standart görünümü ile kullanabilmek için JavaScript
        etkinleştirilmiş olmalıdır. Bununla birlikte JavaScript devre dışı bırakılmış ya da web tarayıcınız tarafından
        desteklenmiyor gibi görünüyor.<br/>Standart görünümü kullanabilmek için web tarayıcı ayarlarınızdan JavaScript
        desteğini etkinleştirip <a href="">yeniden deneyin</a>.<br/></div>
</noscript>

<div id="root">
    <style type="text/css">
        #root {
            margin: 0 !important;
        }
    </style>
    <div class="sessionRecording">
        <div class="sessionRecordingHead">
            <span class="recordingTime">{!! $recordingData['time'] !!}</span>

            <span class="recordingUrl"><a title="{!! $recordingData['url'] !!}" href="{!! $recordingData['url'] !!}"
                                          target="_blank" rel="noreferrer">{!! $recordingData['url'] !!}</a></span>
            <span class="recordingResolution" title="Viewport Resolution (width x height)"><span
                        class="recordingWidth">{!! $recordingData['viewport_w_px'] !!}</span>x<span
                        class="recordingHeight">{!! $recordingData['viewport_h_px'] !!}</span></span>
            <span class="recordingLogos">
                <img alt="test" class="countryFlag" title="Türkiye, Denizli, Denizli" src="/vendor/mediapress/images/flags/TR.png" />
                <img alt="test" title="Masaüstü, generic desktop" src="{!! '/vendor/mediapress/flow/plugins/Morpheus/icons/dist/devices/desktop.png' !!}" />
                <img alt="test" title="GNU/Linux" src="{!! '/vendor/mediapress/flow/plugins/Morpheus/icons/dist/os/LIN.png' !!}" />
                <img alt="test" title="Chrome" src="{!! '/vendor/mediapress/flow/plugins/Morpheus/icons/dist/browsers/CH.png' !!}" />
               </span>
        </div>

        <script type="text/javascript">
            window.sessionRecordingData = {!! json_encode($recordingData) !!};
        </script>

        <div piwik-session-recording-vis
             scroll-accuracy="1000" offset-accuracy="2000"
             replay-speed="1"
             skip-pauses-enabled="false"
             auto-play-enabled="false">

        </div>

    </div>

    <div piwik-popover-handler></div>

    <div class="ui-confirm" id="shortcuthelp">
        <h2>Kullanılabilecek kısa yollar</h2>
        <dl></dl>
    </div>

    <div id="pageFooter">

    </div>

    <div id="bottomAd" style="font-size: 2px;">&nbsp;</div>
    <script type="text/javascript">
        if ('undefined' === (typeof hasBlockedContent) || hasBlockedContent !== false) {
            (function () {
                var body = document.getElementsByTagName('body');

                if (!body || !body[0]) {
                    return;
                }

                var bottomAd = document.getElementById('bottomAd');
                var wasMostLikelyCausedByAdblock = false;

                if (!bottomAd) {
                    wasMostLikelyCausedByAdblock = true;
                } else if (bottomAd.style && bottomAd.style.display === 'none') {
                    wasMostLikelyCausedByAdblock = true;
                } else if ('undefined' !== (typeof bottomAd.clientHeight) && bottomAd.clientHeight === 0) {
                    wasMostLikelyCausedByAdblock = true;
                }

                if (wasMostLikelyCausedByAdblock) {
                    var shouldGetHiddenElement = document.getElementById("should-get-hidden");
                    var warning = document.createElement('p');
                    warning.innerText = 'Bir\u0020reklam\u0020engelleyici\u0020kullan\u0131yorsan\u0131z,\u0020Matomo\u0020uygulamas\u0131n\u0131n\u0020sorunsuz\u0020\u00E7al\u0131\u015Fmas\u0131n\u0131\u0020sa\u011Flamak\u0020i\u00E7in\u0020devre\u0020d\u0131\u015F\u0131\u0020b\u0131rak\u0131n.';

                    if (shouldGetHiddenElement) {
                        shouldGetHiddenElement.appendChild(warning);
                    } else {
                        body[0].insertBefore(warning, body[0].firstChild);
                        warning.style.color = 'red';
                        warning.style.fontWeight = 'bold';
                        warning.style.marginLeft = '16px';
                        warning.style.marginBottom = '16px';
                        warning.style.fontSize = '20px';
                    }
                }
            })();
        }
    </script>
</body>
</html>
