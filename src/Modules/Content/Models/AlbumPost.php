<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class AlbumPost extends Model
{
    protected $table = "instagram_post_album";
    public $timestamps = false;

}
