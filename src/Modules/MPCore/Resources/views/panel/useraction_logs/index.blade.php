@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! __('MPCorePanel::useraction_logs.title') !!}</div>
                @if(count($actions)>0)
                    @if(!empty($queries))
                        <a href="{!! route("MPCore.logs.index") !!}" class="filter-block">
                            <i class="fa fa-trash"></i> {!! __('MPCorePanel::useraction_logs.clear_filters') !!}
                        </a>
                    @endif
                @endif
            </div>
        </div>

        @include("MPCorePanel::inc.errors")
        <div class="p-30">
            <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{!! __('MPCorePanel::useraction_logs.website') !!}</th>
                    <th>IP</th>
                    <th>{!! __('MPCorePanel::useraction_logs.user') !!}</th>
                    <th>{!! __('MPCorePanel::useraction_logs.content') !!}</th>
                    <th>{!! __('MPCorePanel::general.created_at') !!}</th>
                </tr>
                </thead>
                @foreach($actions as $action)
                    <tr>
                        <td>{!! $action->id !!}</td>
                        <td>{!! optional($action->website)->name !!}</td>
                        <td>{!! $action->ip ?: "N/A" !!}</td>
                        <td>{!! optional($action->user)->username !!}</td>
                        <td class="text text-{!! $action->context_type !!}"> <i class="fa {!! $action->context_icon !!}" style="margin-right:10px"></i> {!! $action->content !!} </td>
                        <td>{!! $action->created_at	 !!} </td>
                    </tr>
                @endforeach
            </table>

            <div class="float-left">
                {!! $actions->links() !!}
            </div>
            <div class="float-right">
                <select onchange="locationOnChange(this.value);" class="fs-13">
                    <option value="#">{!! __('MPCorePanel::useraction_logs.list_size') !!}</option>
                    <option  {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("MPCore.logs.index", array_merge($queries, ["list_size"=>"15"])) !!}">15</option>
                    <option  {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("MPCore.logs.index", array_merge($queries, ["list_size"=>"25"])) !!}">25</option>
                    <option  {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("MPCore.logs.index", array_merge($queries, ["list_size"=>"50"])) !!}">50</option>
                    <option  {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("MPCore.logs.index", array_merge($queries, ["list_size"=>"100"])) !!}">100</option>
                </select>
            </div>

        </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush
