<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class UpdateMetasTableDivideMobileValues extends Migration
    {

        public function up()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('metas')) {
                $schema->table('metas', function (Blueprint $table) {
                    /** @var Blueprint\ $table */
                    $table->dropColumn("value");
                    $table->boolean("mobile_enabled")->default(true);
                    $table->boolean("desktop_enabled")->default(true);
                    $table->string("mobile_value")->nullable();
                    $table->string("desktop_value")->nullable();
                });
            }
        }

        public function down()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('metas')) {
                $schema->table('metas', function (Blueprint $table) {
                    $table->string("value")->nullable();
                    $table->dropColumn("mobile_enabled");
                    $table->dropColumn("desktop_enabled");
                    $table->dropColumn("mobile_value");
                    $table->dropColumn("desktop_value");
                });
            }
        }
    }
