@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("EntityPanel::entity.create-entity") !!}</div>
            </div>
        </div>
        <div class="p-30">
            <form action="{!! route('Entity.entities.store') !!}" novalidate method="POST">
                @include("EntityPanel::entities.form")
            </form>
        </div>
    </div>
@endsection
