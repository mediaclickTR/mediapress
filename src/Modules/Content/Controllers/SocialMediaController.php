<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\Content\Models\SocialMedia;
use Mediapress\Modules\MPCore\Facades\MPCore;



class SocialMediaController extends Controller
{

    public const CONTENT_SOCIALMEDIA_INDEX = 'Content.socialmedia.index';
    public const REQUIRED = 'required';
    public const ACCESSDENIED = 'accessdenied';

    private $rules = [
        'name' => self::REQUIRED,
        'status' => self::REQUIRED,
        'order' => self::REQUIRED,


    ];
    private $labels = [
        'name' => '"Sosyal Medya Adı"',
        'status' => '"Aktiflik"',
        'order' => '"Sıra No"',


    ];


    use TableBuilderTrait;

    public function index()
    {
        $websiteId = session("panel.website.id");
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.index",
            "content.websites.social_media.index",
        ])){
            return rejectResponse();
        }

        if (!userAction('Content.social_media.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }


        $website = session("panel.website");

        $socialmedia = SocialMedia::where('website_id',$website->id)->get();

        $groups = $socialmedia->groupBy("group_id");

        return view('ContentPanel::socialmedia.index', compact('groups','website'));

    }

    public function create()
    {

        $website = session("panel.website");
        $websiteId = $website->id;
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.create",
            "content.websites.social_media.create",
        ])){
            return rejectResponse();
        }


        $country_groups= $website->countryGroups;
        $languages=$website->languages;

        $data_options = json_decode('{"key":"","file_type":"image","required":"required","title":"","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","pixels_x_min":"","pixels_x_max":"","pixels_y_min":"","pixels_y_max":"","pixels_x":"","pixels_y":"","filesize_min":"","filesize_max":"","files_max":"1","additional_rules":""}', 1);

        return view('ContentPanel::socialmedia.create', compact(  'data_options','country_groups','languages'));

    }

    public function store(Request $request)
    {

        $website = session("panel.website");
        $websiteId = $website->id;
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.create",
            "content.websites.social_media.create",
        ])){
            return rejectResponse();
        }

        $this->validate($request, $this->rules, [], $this->labels);



        $website = session("panel.website");
        $user_id= session("panel.user")->id;

        $website_id=$website->id;
        $default_lang_id=$website->defaultLanguage()->id;


        $data = [];
        $data1=[];
        $data['admin_id'] =$user_id;
        $data['website_id']=$website_id;
        $data['name']=$request->name;
        $data['order']=$request->order;
        $data['status']=$request->status;
        $data['group_id']=$request->group_id;
        $data['icon'] = $request->icon == "[]" ? null : $request->icon;
        $data['lang_id'] =$default_lang_id;

        foreach($request->all() as $key =>$value){

            if (is_numeric($key)) {
                foreach( $value as $newkey => $newval ){
                    $data1[$key][$newkey]['link'] = $newval['link'];
                }
            }
        }

        $data['link']=$data1;

        SocialMedia::create($data);

        return redirect(route(self::CONTENT_SOCIALMEDIA_INDEX));
    }

    public function update($id, Request $request)
    {

        $website = session("panel.website");
        $websiteId = $website->id;
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.update",
            "content.websites.social_media.update",
        ])){
            return rejectResponse();
        }

        $this->validate($request, $this->rules, [], $this->labels);




        $user_id=session("panel.user")->id;


        $data = $request->all();
        $data1=[];
        $socialmedia = SocialMedia::find($id);


        foreach($request->all() as $key =>$value){

            if (is_numeric($key)) {
                foreach( $value as $newkey => $newval ){
                    $data1[$key][$newkey]['link'] = $newval['link'];
                }
            }
        }

        $socialmedia->link=$data1;
        $socialmedia->order = $request->order;
        $socialmedia->save();
        return redirect(route(self::CONTENT_SOCIALMEDIA_INDEX));

    }

    public function edit($id)
    {

        $website = session("panel.website");
        $websiteId = $website->id;
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.update",
            "content.websites.social_media.update",
        ])){
            return rejectResponse();
        }

        $socialmedia = SocialMedia::find($id);

        $languages=$website->languages;
        $country_groups= $website->countryGroups;

        $data_options = json_decode('{"key":"","file_type":"image","required":"required","title":"","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","pixels_x_min":"","pixels_x_max":"","pixels_y_min":"","pixels_y_max":"","pixels_x":"","pixels_y":"","filesize_min":"","filesize_max":"","files_max":"1","additional_rules":""}', 1);

        return view('ContentPanel::socialmedia.edit', compact('website','socialmedia','data_options','languages','country_groups'));


    }


    public function delete($id)
    {


        $website = session("panel.website");
        $websiteId = $website->id;
        if(!activeUserCan([
            "content.websites.website$websiteId.social_media.delete",
            "content.websites.social_media.delete",
        ])){
            return rejectResponse();
        }

        $socialmedia = SocialMedia::find($id);
        $socialmedia->delete();

        return redirect(route(self::CONTENT_SOCIALMEDIA_INDEX));

    }
}
