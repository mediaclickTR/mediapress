<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class Maillist extends Model
{
    protected $table = 'maillists';

    public $timestamps = true;

    protected $primaryKey = 'id';
    protected $fillable = ['name','usernames', 'emails'];

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
    public function websites()
    {
        return $this->belongsToMany(Website::class)->withPivot('website_id');
    }
}
