<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGroupCountryTable extends Migration
{
    public const COUNTRY_GROUP_COUNTRY = 'country_group_country';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::COUNTRY_GROUP_COUNTRY)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::COUNTRY_GROUP_COUNTRY, function (Blueprint $table) {
                $table->string('country_code',20);
                $table->unsignedInteger('country_group_id');
//                $table->foreign('country_code')->references('code')->on('countries')->onDelete('cascade')
//                $table->foreign('country_group_id')->references('id')->on('country_groups')->onDelete('cascade')
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::COUNTRY_GROUP_COUNTRY);
    }
}
