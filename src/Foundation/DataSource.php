<?php

namespace Mediapress\Foundation;

class DataSource
{
    public $params=[];

    public function __construct($params=[]){
        $this->setParams($params);
        return $this;
    }

    public function getData(){
        return null;
    }

    public function setParams($params=[]){
        $this->params = $params;
        return $this;
    }

    public function setParam($key, $value){
        $this->params[$key] = $value;
        return $this;
    }

    public function getParam($key){
        return $this->params[$key] ?? null;
    }

}
