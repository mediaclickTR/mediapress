<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MDiskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mdisks')->delete();
        DB::table('mdisks')->insert([
            [
                "id" => 1,
                "diskkey" => "local",
                "name" => "uploads",
                "driver" => "local",
                "root" => "uploads/",
            ],
        ]);
    }
}
