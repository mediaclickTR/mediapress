<?php


return [
    'dashboard' => 'Dashboard',
    'content' => 'Update',
    'modules' => 'Modules',
    'settings' => 'Settings',

    'general_settings' => 'General Settings',
    'tools' => 'Developer Tools',
    'translate_center' => 'Translate Center',
    'user_actions' => 'Usage History',
    'file_manager' => 'File Manager',

    'survey' => 'Survey',
    'panel_language_parts' => 'Panel Language Parts',
];
