<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class RedirectUrl extends Url
{
    use CacheQueryBuilder;
    use SoftDeletes;

    protected $table = 'redirect_urls';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $fillable = ['website_id','url','model_type','model_id'];
}
