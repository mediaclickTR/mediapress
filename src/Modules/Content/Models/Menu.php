<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends BaseModel
{
    use SoftDeletes;
    protected $table = 'menus';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = ["admin_id","website_id", "slug", "name", "type", "sections","status"];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function details()
    {
        return $this->hasMany(MenuDetail::class);
    }
    public function log()
    {
        return $this->morphMany(UserActionLogs::class,"model");
    }
    
    // TODO: apply country group parameter to detail function and migration
}
