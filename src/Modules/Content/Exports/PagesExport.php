<?php

namespace Mediapress\Modules\Content\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\MPCore\Models\LanguagePart;

class PagesExport implements FromArray
{

    public function __construct(int $countryGroupId, int $languageId, array $sitemaps)
    {
        $this->countryGroupId = $countryGroupId;
        $this->languageId = $languageId;
        $this->sitemaps = $sitemaps;
    }

    public function array(): array
    {
        $hold = [];

        $headingRow = ['page_id', 'name', 'detail'];
        $hold[] = $headingRow;

        $details = PageDetail::where('country_group_id', $this->countryGroupId)
            ->where('language_id', $this->languageId)
            ->select('page_id', 'name', 'detail')
            ->whereHas('page', function ($q) {
                $q->where('status', 1)
                    ->whereIn('sitemap_id', $this->sitemaps);
            })
            ->get()
            ->toArray();

        $hold = array_merge($hold, $details);

        return $hold;
    }
}
