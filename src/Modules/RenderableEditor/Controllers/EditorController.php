<?php

namespace Mediapress\Modules\RenderableEditor\Controllers;


use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Page;
use Symfony\Component\Finder\SplFileInfo;

class EditorController extends Controller
{

    public const INFO = '_info';
    public $api = 'https://editor.mclck.com/';
    public const BREAKER = '/*EDITOR*/';
    public const CONTENTS = 'contents';
    public const QUOT = '&quot;';
    public const OPTIONS = 'options';
    public const PARAMS = 'params';


    public function index()
    {

        if(!activeUserCan([
            "renderableeditor.index",
        ])){
            return rejectResponse();
        }

        $ds = DIRECTORY_SEPARATOR;
        $path = base_path('app' . $ds . 'Modules' . $ds . 'Content' . $ds . "Website" . session("panel.website.id") . $ds .'AllBuilder' . $ds . 'Renderables');

        $files = (new Filesystem())->allFiles($path);
        /** @var SplFileInfo $file */
        return view("RenderableEditorPanel::editors.index", compact('files'));
    }

    public function show($filePath)
    {
        if(!activeUserCan([
            "renderableeditor.update",
        ])){
            return rejectResponse();
        }
        $path = decrypt($filePath);
        $file = File::get($path);
        if (strstr($file, '//NONEDITABLE')) {
            dd('This Renderable non Editable');
        }
        $class = $this->getClassFullNameFromFile($path);

        $class = str_replace('App\Modules\Content\Website'. session("panel.website.id")  .'\AllBuilder', 'App\...', $class);

        if (!strstr($file, self::BREAKER)) {
            dd(self::BREAKER . ' not Found');
        }
        $explode = explode(self::BREAKER, $file);

        $contents = eval('return ' . $explode[1] . ';');

        $list = $this->getRenderable($contents);
        $renderable = new BuilderRenderable();
        $object_tags = $renderable->getObjectTags();
        $types = $renderable->getOption('renderable_types', []);
        $types = array_map([$this, "mapRenderableType"], $types);
        $names = $this->renderableNames($types);

        $templates = $this->templates();

        return view("RenderableEditorPanel::editors.show", compact('list', 'class', 'filePath', 'types', 'names', 'object_tags','templates'));
    }

    public  function templates()
    {
        $templates = [

            "order"=>
                [
                    "name"=>"Sıralama",
                    "icon"=>"fa-list-ol",
                    "data"=>'[{ "_info": { "options": { "html": { "tag": "input", "attributes": { "class": "", "name": "page-><print>page->id</print>->order", "type": "number", "value":"<print>page->order</print>" } }, "title": "Sıralama", "rules": "" } }, "type": "inputwithlabel", "id": "5d1dba0823a01" }]'
                ],
            [
                "name"=>"Tarih Seçimi",
                "icon"=>"fa-calendar",
                "data"=>'[{ "_info": { "options": { "html": { "tag": "input", "attributes": { "class": "", "name": "page-><print>page->id</print>->date", "type": "date", "value":"<print>page->date</print>" } }, "title": "Tarih", "rules": "" } }, "type": "inputwithlabel", "id": "5d1dba0823a46" }]'
            ],
            [
                "name"=>"Öne Çıkar/Çıkarma",
                "icon"=>"fa-check-circle",
                "data"=>'[ { "_info": { "options": { "html": { "tag": "input", "attributes": { "class": "form-check-input", "name": "page-><print>page->id</print>->cvar_1", "type": "checkbox" } }, "title": "Öne Çıkar/Çıkarma", "rules": "", "value": "<print>page->cvar_1</print>" }, "data": { "on_value": "1", "on_text": "<u>Öne Çıkar</u>", "off_value": "0", "off_text": "<u>Öne Çıkarma</u>" } }, "type": "toggleswitch", "id": "5d1dba0423a95" } ]'
            ],
            [
                "name"=>"Ülke Seçiçi",
                "icon"=>"fa-caret-square-down",
                "data"=> '[ { "_info": { "options": { "html": { "tag": "select", "attributes": { "class": "", "name": "page-><print>page->id</print>->cint_1", "data-live-search": "true" } }, "rules" : "", "primary_key" : "id", "country_name_type": "en", "title": "Ülke Seçin", "multiple": "0", "value": "<print>page->cint_1</print>", "show_default_option": "0", "additional_content": "merge", "default_value": "", "default_text": "---Seçiniz---" } }, "type": "countryselect", "id": "5e26f19fe088d" } ]'
            ],
            [
                "name"=>"Şehir Seçiçi",
                "icon"=>"fa-caret-square-down",
                "data"=> '[ { "_info": { "options": { "html": { "tag": "select", "attributes": { "class": "", "name": "page-><print>page->id</print>->cint_2", "data-live-search": "true" } }, "rules" : "", "primary_key" : "id", "country_name_type": "en", "title": "Şehir Seçin", "multiple": "0", "value": "<print>page->cint_2</print>", "show_default_option": "0", "additional_content": "merge", "default_value": "", "default_text": "---Seçiniz---" } }, "type": "provinceselect", "id": "5e26f19fe08d0" } ]'
            ],
            [
                "name"=>"Aptal Arama",
                "icon"=>"fa-search",
                "data"=>'[{ "_info": { "options": { "html": { "tag": "textarea", "attributes": { "class": "form-control", "name": "detail->search_text" } }, "title": "Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>", "value": "<print>detail->search_text</print>", "rules": "" } }, "type": "textareawithlabel", "id": "60002d30cf613" }]'
            ],
        ];

        return $templates;
    }

    private function mapRenderableType($classname)
    {
        $brokens_message = [];
        try {
            $model = new $classname;
            $new_data = collect($model->getInfo())->except("items")->toArray();
        } catch (\Exception $e) {
            $brokens_message[] = "Hatalı!  " . $classname . " nesnesindeki sorun şu: ".$e->getMessage();
        }
        if(count($brokens_message)){
            return dd(implode("\n\n",$brokens_message));
        }
        return $new_data;
    }


    public function update(Request $request)
    {

        if(!activeUserCan([
            "renderableeditor.update",
        ])){
            return rejectResponse();
        }
        $filePath = $request->filePath;
        $path = decrypt($filePath);
        $file = File::get($path);
        $explode = explode(self::BREAKER, $file);
        $start = $explode[0] . self::BREAKER . "\n";
        $finish = "\n" . self::BREAKER . "\n" . $explode[2];

        $output = str_replace(['"children"'], ['"contents"'], $request->output);


        $output = preg_replace("/\"id\":\"5+[a-zA-Z0-9]{12}+\"/i", '', $output);
        $output = str_replace(',,', ',', $output);
        $output = str_replace(',}', '}', $output);
        $output = str_replace(',]', ']', $output);
        $output = str_replace('{,', '{', $output);
        $output = str_replace('[,', '[', $output);
        $output = json_decode($output, 1);
        $output = $this->removeInfo($output);


        file_put_contents($path, $start . $this->varexport($output, true) . "\n" . $finish);
        return redirect()->back()->with("success", "Başarıyla kaydedildi");//(route('admin.editors.index'));

    }

    private function varexport($expression, $return = FALSE)
    {
        $export = var_export($expression, TRUE);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [NULL, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));
        if ((bool)$return) {
            return $export;
        } else {
            echo $export;
        }
    }

    public function cloud(Request $request)
    {

        if(!activeUserCan([
            "renderableeditor.savetocloud",
        ])){
            return rejectResponse();
        }

        $array = $request->all();
        unset($array['_token']);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->api . "api/save");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
        Cache::forget('RenderableEditor.cloudLayouts');
        return response($server_output);
    }

    public function layouts()
    {
        $layouts = Cache::remember('RenderableEditor.cloudLayouts', 1, function () {
            return json_decode(file_get_contents($this->api . "api/get"), 1);
        });

        return response()->json($layouts);
    }

    private function getRenderable(array $contents)
    {
        $list = [];
        foreach ($contents as $content) {
            $list[] = $this->makePart($content);
        }

        return json_encode($list);
    }

    private function makePart($contents)
    {
        $array = ['id' => uniqid(), self::INFO => []];

        if (isset($contents['type'])) {

            $array['type'] = $contents['type'];
        } else if (is_array($contents) && count($contents)) {

            foreach ($contents as $content) {
                $array['children'][] = $this->makePart($content);
            }
        } else {
            $array[self::CONTENTS] = str_replace("\"", self::QUOT, json_encode($contents));
        }
        foreach ($contents as $key => $value) {
            if ($key == 'type') {
                continue;
            } elseif ($key == self::CONTENTS) {
                foreach ($contents[self::CONTENTS] as $content) {
                    $array['children'][] = $this->makePart($content);
                }
            } else {

                $array[self::INFO][$key] = $contents[$key];
            }

        }
        $items = str_replace("\"", self::QUOT, json_encode($array[self::INFO]));
        $array[self::INFO] = str_replace('\&quot;', '%quot%', $items);


        return $array;
    }

    /**
     * get the full name (name \ namespace) of a class from its file path
     * result example: (string) "I\Am\The\Namespace\Of\This\Class"
     *
     * @param $filePathName
     *
     * @return  string
     */
    public function getClassFullNameFromFile($filePathName)
    {
        return $this->getClassNamespaceFromFile($filePathName) . '\\' . $this->getClassNameFromFile($filePathName);
    }


    /**
     * build and return an object of a class from its file path
     *
     * @param $filePathName
     *
     * @return  mixed
     */
    public function getClassObjectFromFile($filePathName)
    {
        $classString = $this->getClassFullNameFromFile($filePathName);

        return new $classString;
    }


    /**
     * get the class namespace form file path using token
     *
     * @param $filePathName
     *
     * @return  null|string
     */
    protected function getClassNamespaceFromFile($filePathName)
    {
        $src = file_get_contents($filePathName);

        $tokens = token_get_all($src);
        $count = count($tokens);
        $i = 0;
        $namespace = '';
        while ($i < $count) {
            $token = $tokens[$i];
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                // Found namespace declaration
                while (++$i < $count) {
                    if ($tokens[$i] === ';') {
                        $namespace = trim($namespace);
                        break;
                    }
                    $namespace .= is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i];
                }
                break;
            }
            $i++;
        }
        return $namespace ?? null;

    }

    /**
     * get the class name form file path using token
     *
     * @param $filePathName
     *
     * @return  mixed
     */
    protected function getClassNameFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);

        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {

                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }

        return $classes[0];
    }

    private function renderableNames(array $types)
    {
        $list = [];
        foreach ($types as $key => $value) {
            $list[$key] = $value['name'];
        }
        return $list;

    }

    private function removeInfo($output)
    {
        foreach ($output as $key => $value) {
            if ($key === self::INFO) {

                foreach ($value as $k => $v) {
                    $output[$k] = $v;
                }
            }
            unset($output[self::INFO]);
            if (is_array($value)) {
                $value = $this->removeInfo($value);
            }

            $output[$key] = $value;
        }
        return array_map(function ($tmp) {
            if(isset($tmp['_token'])){
                unset($tmp['_token']);
            }
            return $tmp;
        }, $output);


    }

}
