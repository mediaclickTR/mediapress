<?php

namespace Mediapress\Modules\Content\Controllers;


use Mediapress\Modules\Content\Models\Category;
use Mediapress\Traits\DefaultSitemapKitTrait;

class WebContentController
{

    public function ajaxCriteriaFilter() {

        $category_id = request()->get('category_id');

        $category = Category::find($category_id);
        $sitemap = $category->sitemap;
        $sitemap_type = $sitemap->sitemapType->name;

        $controller = "App\Modules\Content\Website".$sitemap->website->id."\Http\Controllers\Web\\". $sitemap_type . "Controller";


        return app($controller)->ajaxCriteriaFilter();
    }

}
