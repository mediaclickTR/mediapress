<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Traits\HasDetails;
use Mediapress\Modules\MPCore\Models\BaseModel;

class Scene extends BaseModel
{
    use SoftDeletes;
    use HasDetails;

    protected $table = "scenes";

    public $timestamps = true;

    protected $dates = ["deleted_at"];

    public $fillable = [
        "slider_id",
        "order",
        "time",
        "admin_id"
    ];

    public function slider(){
        return $this->belongsTo(Slider::class);
    }

    public function details(){
        return $this->hasMany(SceneDetail::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
}
