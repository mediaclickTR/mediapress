@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! trans("MPCorePanel::sitemap_type.create") !!}</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    <div class="asd">
                        @include("MPCorePanel::sitemap_types.form")
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection