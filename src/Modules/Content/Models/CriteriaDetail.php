<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\HasMFiles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Traits\IsDetail;

class CriteriaDetail extends BaseModel
{

    use HasMFiles;
    use SoftDeletes;
    use IsDetail;

    protected $dates = ['deleted_at'];
    public const MODEL = 'model';
    protected $table = 'criteria_details';

    public $timestamps = false;

    protected $fillable = ["language_id", 'criteria_id','country_group_id', "name", "slug", "detail",'status'];

    public function criteria()
    {
        return $this->belongsTo(Criteria::class);
    }

    public function parent(){
        return $this->belongsTo(Criteria::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'model')->withTrashed()->withDefault(['url'=>'/'])->whereIn('type',['original','reserved'])->orderBy('type','asc');
    }

    public function medias()
    {
        return $this->morphMany(Media::class, self::MODEL);
    }

    public function extras()
    {
        return $this->hasMany(CriteriaDetailExtra::class);
    }


    public function getParentIdAttribute(){
        return $this->criteria_id;
    }

//    public function setSlugAttribute($value)
//    {
//        if (mb_substr($value, 0, 1) != '/') {
//            $this->attributes['slug'] = '/' . $value;
//        } else {
//            $this->attributes['slug'] = $value;
//        }
//
//    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

}
