@extends('MPCorePanel::inc.app')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="p-30 mt-4">
            <div class="sitemapList">
                <h2>{!! __('ContentPanel::export_pages.select_pages') !!}</h2>
                <div class="pt-3">
                    @include("MPCorePanel::inc.errors")
                    <form action="{{ route('Content.export_pages.export') }}" method="post" class="former" id="exportForm">
                        @csrf
                        <input type="hidden" name="language" value="{{request()->language}}">
                        @foreach($sitemaps as $sitemap)
                            <div class="form-check checkbox">
                                <input class="form-check-input" type="checkbox" name="sitemaps[]" value="{{$sitemap->id}}"
                                       id="check{{$sitemap->id}}">
                                <label class="form-check-label" for="check{{$sitemap->id}}">
                                    {!! $sitemap->detail->name !!}
                                </label>
                            </div>
                        @endforeach
                        <div class="btn-group float-right">
                            <button type="button" class="btn btn-primary dropdown-toggle float-right" id="dropdownBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {!! __('ContentPanel::export_pages.export') !!}
                            </button>
                            <div class="dropdown-menu">
                                <button type="submit" name="type" value="word" class="dropdown-item">Word</button>
                                <button type="submit" name="type" value="excel" class="dropdown-item">Excel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="successMessage d-none text-center">
                <h3></h3>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var checkedInput = 0;

        $('.form-check-input').on('ifChecked', function(event){
            checkedInput += 1;
        });
        $('.form-check-input').on('ifUnchecked', function(event){
            checkedInput -= 1;
        });

        $('#exportForm').submit(function(e) {

            $('#dropdownBtn').html('Exporting...').addClass('disabled');

            setTimeout(function(){

                $('#dropdownBtn').html("{!! __('ContentPanel::export_pages.export') !!}").removeClass('disabled');

                $('.successMessage > h3').html(checkedInput + " sayfa yapısı dışarı aktarıldı.");
                $('.sitemapList').addClass('d-none');
                $('.successMessage').removeClass('d-none');
                $('.form-check-input').iCheck('uncheck');

                setTimeout(function () {
                    $('.successMessage').addClass('d-none');
                    $('.sitemapList').removeClass('d-none');
                }, 2000)
            }, 3000)
        })

    </script>
@endpush

