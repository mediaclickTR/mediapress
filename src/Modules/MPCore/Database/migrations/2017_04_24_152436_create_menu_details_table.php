<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('menu_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('language_id')->unsigned()->nullable();
            $table->integer('url_id')->unsigned()->nullable();
            $table->tinyInteger('type')->unsigned()->nullable();
            $table->string('target')->nullable();
            $table->string('name');
            $table->integer('parent')->nullable()->unsigned();
            $table->integer('type_view')->nullable()->unsigned();
            $table->string('out_link')->nullable();
            $table->integer("lft")->unsigned()->nullable();
            $table->integer("rgt")->unsigned()->nullable();
            $table->integer("depth")->unsigned()->nullable();
            $table->tinyInteger("draft")->default(1);
            $table->tinyInteger("status")->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_details');
    }
}
