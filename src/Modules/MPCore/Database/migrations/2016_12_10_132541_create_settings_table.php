<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

    public const SETTINGS = 'settings';

    public function up()
    {

        if (!Schema::hasTable(self::SETTINGS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::SETTINGS, function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('website_id')->nullable()->unsigned();
                $table->string('name');
                $table->string('value');
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::SETTINGS);
        Schema::enableForeignKeyConstraints();
    }
}
