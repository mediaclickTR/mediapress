<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\MPCore\Exports\LanguagePartExport;
use Mediapress\Modules\MPCore\Imports\LanguagePartImport;
use Maatwebsite\Excel\Facades\Excel;
use Mediapress\Modules\MPCore\Models\PanelLanguagePart;
use Illuminate\Support\Facades\Cache;

class PanelTranslateController extends Controller
{
    public const PANEL_LANGUAGES = "panel.languages";

    public function index()
    {
        $website = session('panel.website');
        $languageParts = PanelLanguagePart::where('website_id', $website->id)->get()->groupBy('key');
        $adminLanguages = Admin::all()->pluck('language_id')->unique()->toArray();


        $data = Cache::rememberForever('panelLanguageParts' . $website->id, function() use($languageParts, $adminLanguages) {
            $data = [];
            foreach ($languageParts as $key => $parts) {

                $keyArr = explode('->', $key);

                $sitemap = Sitemap::find($keyArr[1]);

                if($sitemap) {
                    $tempKey = $sitemap->detail->name . ' (' . $key .')';
                } else {
                    $tempKey = $key;
                }

                $defaultLangPart = PanelLanguagePart::where('language_id', session('panel.user.language_id'))->where('key', $key)->first();
                $defaultValue = $defaultLangPart ? $defaultLangPart->value : '';

                $temp = [];
                foreach ($parts as $part) {
                    $temp[$part->language->id] = $part->value;
                }
                $data[$tempKey] = [
                    'parts' => $temp,
                    'key' => $key,
                    'default_value' => $defaultValue,
                ];

                foreach ($adminLanguages as $adminLanguage) {
                    if(isset($data[$tempKey]['parts'][$adminLanguage])) {
                        continue;
                    }
                    $data[$tempKey]['parts'][$adminLanguage] = null;
                }
            }
            return $data;
        });

        return view("MPCorePanel::panel_translate.index", compact('data'));
    }

    public function search()
    {

        $searchKey = request()->get('search_key');

        $website = session('panel.website');
        $langParts = PanelLanguagePart::where('website_id', $website->id)
            ->where(function ($q) use ($searchKey) {
                $q->where('key', 'like', '%'.$searchKey.'%')
                    ->orWhere('value', 'like', '%'.$searchKey.'%');
                if($q->first()) {
                    return $q->orWhere('key', $q->first()->key);
                }
            })->get();

        $searchPartKeys = $langParts->pluck('key')->toArray();
        $languageParts = PanelLanguagePart::whereIn('key', $searchPartKeys)->get()->groupBy('key');

        $adminLanguages = Admin::all()->pluck('language_id')->unique()->toArray();

        $data = [];

        foreach ($languageParts as $key => $parts) {

            $keyArr = explode('->', $key);

            $sitemap = Sitemap::find($keyArr[1]);

            if($sitemap) {
                $tempKey = $sitemap->detail->name . ' (' . $key .')';
            } else {
                $tempKey = $key;
            }

            $defaultLangPart = PanelLanguagePart::where('language_id', session('panel.active_language.id'))->where('key', $key)->first();
            $defaultValue = $defaultLangPart ? $defaultLangPart->value : '';

            $temp = [];
            foreach ($parts as $part) {
                $temp[$part->language->id] = $part->value;
            }
            $data[$tempKey] = [
                'parts' => $temp,
                'key' => $key,
                'default_value' => $defaultValue,
            ];

            foreach ($adminLanguages as $adminLanguage) {
                if(isset($data[$tempKey]['parts'][$adminLanguage])) {
                    continue;
                }
                $data[$tempKey]['parts'][$adminLanguage] = null;
            }
        }


        return view("MPCorePanel::panel_translate.ajax", compact('data'))->render();

    }

    public function save(Request $request)
    {
        $data = $request->except('_token');

        foreach ($data as $key => $langs) {
            foreach ($langs as $language_id => $value) {
                PanelLanguagePart::updateOrCreate(
                    [
                        'website_id' => session('panel.website.id'),
                        'language_id' => $language_id,
                        'key' => $key
                    ],
                    [
                        'value' => $value
                    ]
                );
            }
        }
        Cache::flush();
    }

    public function export() {
        return Excel::download(new LanguagePartExport, 'language_parts.xlsx');
    }

    public function import(Request $request) {

        $file = $request->file('excelFile');

        if($file) {
            $fileName = $file->store(null, 'public');
            $fileName = storage_path('app/public/'. $fileName);


            Excel::import(new LanguagePartImport, $fileName);

            @unlink($fileName);

            return redirect()->route('MPCore.translate.index');
        }
    }
}
