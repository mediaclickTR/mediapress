@extends('ContentPanel::inc.module_main')

@php($devices = ["desktop","tablet","mobile"])

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                {!! $slider->name !!} {{ __('ContentPanel::scene.title') }}
            </div>
            <div class="float-right">
                <a href="{!! route('Content.slider.edit',$slider->id) !!}" class="btn no-bg">{{ __('ContentPanel::scene.settings') }}</a>
                <a href="{!! route('Content.slider.scenes.create',$slider->id) !!}" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    {{ __('ContentPanel::general.add_new') }}
                </a>
            </div>
        </div>
        <table>
            <thead>
            <tr>

                <th>{{ __('ContentPanel::general.order') }}</th>
                <th>{{ __('ContentPanel::general.status') }}</th>
                <th style="width:190px;">{{ __('ContentPanel::scene.video_link') }}</th>
                <th>{{ __('ContentPanel::general.created_at') }}</th>
                <th>{{ __('ContentPanel::general.added_by') }}</th>
                <th>{{ __('ContentPanel::general.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($scenes as $scene)
                <tr>
                    <td>
                        {!! $scene->order !!}
                    </td>
                    <td class="status">
                        @if($scene->status == 1)
                            <i class="active" style="background: #7fcc46"></i>{{ __('ContentPanel::general.status_title.active') }}
                        @else
                            <i class="passive" style="background: #dc3545"></i>{{ __('ContentPanel::general.status_title.passive') }}
                        @endif
                    </td>
                    <td>{!! optional($scene->detail)->video_url !!}</td>
                    <td>{!! $scene->created_at !!}</td>
                    <td>{!! $scene->admin->short_name !!}</td>
                    <td>
                        <a href="{!! route('Content.slider.scenes.edit',[$slider->id, $scene->id]) !!}" class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('Content.slider.scenes.delete',[$slider->id, $scene->id]) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                            <i class="fa fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
        <div class="float-right">
            {!! $scenes->links() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! trans("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
