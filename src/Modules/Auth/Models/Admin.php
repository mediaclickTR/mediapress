<?php

namespace Mediapress\Modules\Auth\Models;


use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mediapress\Support\Database\CacheQueryBuilder;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class Admin extends Authenticatable
{
    use CacheQueryBuilder;
    use HasRolesAndAbilities;

    protected $connection = 'mysql';
    protected $table = 'admins';
    public $timestamps = true;
    protected $fillable = ["role_id", "language_id", "country_group", "first_name", "last_name", "username", "email", "phone", "password", "api_token", "google2fa", "google2fa_warning"];
    protected $hidden = ["password", "remember_token"];

    public function getShortNameAttribute(){
        return $this->attributes['first_name'].' '.mb_substr($this->attributes['last_name'],0,1).'.';
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }


}
