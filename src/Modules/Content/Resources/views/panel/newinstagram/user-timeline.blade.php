@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }
        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title">
            <div class="float-left">
                Instagram
            </div>
        </div>
        <div class="container">
            @php($postCounter = 0)
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-6 post-{!! $post["id"] !!}">
                        @if($post["media_type"] == "ddasd")
                            <div id="carouselExampleControls{!! $postCounter !!}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($post["carousel_media"] as $child)
                                        <div class="carousel-item {!! $loop->iteration == 1 ? "active": "" !!}">
                                            <img src="{!! $child["media_url"] !!}" class="d-block w-100" alt="...">
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls{!! $postCounter !!}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @elseif($post["media_type"] == "VIDEO")
                            <video width="250" controls>
                                <source src="{!! $post["media_url"] !!}" type="video/mp4">
                                Lütfen "Chrome" Tarayıcı İle Deneyiniz
                            </video>
                        @else
                            <img class="post-photo" src="{!! $post["media_url"] !!}" alt="" style="max-height: 250px    ">
                        @endif

                        </div>
                    @php($postCounter ++)
                @endforeach
            </div>


        </div>
    </div>

@endsection