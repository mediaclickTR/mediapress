<?php
namespace Mediapress\Modules\MPCore\DataSources;

use Mediapress\Foundation\DataSource;
use Mediapress\Modules\MPCore\Models\Website;

class WebsiteDS extends DataSource
{
    public function getData()
    {
        return Website::get();
    }
}