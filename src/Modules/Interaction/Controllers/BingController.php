<?php
namespace Mediapress\Modules\Interaction\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Cache;
use Request;
use Mediapress\Modules\MPCore\Traits\BingAnalyticsTrait;

class BingController extends Controller
{
    use BingAnalyticsTrait;

    protected $apikey;
    protected $apiUrl;
    protected $siteUrl;
    protected $data;
    protected $cache;
    protected $days;

    // constructor
    public function __construct()
    {
        $this->apikey = settingSunFindValue('mpcore.analytics.bing.apikey');
        $this->siteUrl = settingSunFindValue('mpcore.analytics.bing.api_website_url');
        $this->apiUrl = "https://bing.com/webmaster/api.svc/json/";

        if(request('days') || request('days')=='0'){
            $this->days = (int)str_replace(array(' ', ','), '', request('days'));
        }
        else{
            $this->days = 7;
        }
    }

    public function index()
    {
        $avgClickAndViews = $this->getAvgClickAndViews();
        $getUserWebSites = $this->getUserWebSites();
        $trafficStats = $this->getTrafficStats();
        $urlTrafficInfo = $this->getUrlTrafficeInfo();
        $pageStats = $this->getPageStats();
        $rankAndTrafficStats = $this->getRankAndTrafficStats();
        $keywordStats = $this->getKeywordStats();

        dd($getUserWebSites);
        dd("Veriler gelince view kısmı yapılacak.");
        return view("MPCorePanel::analytics.bing.index", compact("avgClickAndViews"));
    }
    public function detailedAnalytics()
    {

    }
}
