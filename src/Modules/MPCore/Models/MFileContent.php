<?php
namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class MFileContent extends Model
{
    use CacheQueryBuilder;
    protected $table = 'mfile_content';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['mfile_id','model_type','model_id','file_key'];

    public function model()
    {
        return $this->morphTo();
    }

    public function mfiles()
    {
        return $this->morphMany(MFile::class, 'model');
    }

    public function mfile($key = null)
    {
        $result = $this->morphOne(MPFile::class, 'model');
        if($key != null){
            $result = $result->where("file_key",$key);
        }
        return $result;
    }
}
