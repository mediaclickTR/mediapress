<?php
return [


    "flow" => [
        'name' => 'FlowPanel::auth.sections.flow_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [
            "form_filling_records" => [
                'name' => 'FlowPanel::auth.sections.form_filling_records',
                'name_affix' => "",
                'item_model' => \Mediapress\Modules\MPCore\Models\UserActionLogs::class,
                'item_id' => "*",
                "actions" => [
                    "view" => [
                        "name" => "MPCorePanel::auth.actions.view"
                    ]
                ],
                'settings' => [
                    "auth_view_collapse" => "in"
                ],
            ],

        ]
    ]
];
