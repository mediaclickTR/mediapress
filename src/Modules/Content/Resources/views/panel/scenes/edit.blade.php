@extends('ContentPanel::inc.module_main')

@php($devices = ["desktop","tablet","mobile"])

@push("styles")
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
    <style>
        [class^="col-"] {
            float: left;
        }
    </style>


    <style>
        .file-manager {
            border: 2px solid #E5E5E5;
            height: 181px;
            width: 230px;
        }

        .file-manager-image {
            height: 100%;
        }
        .file-manager-image img {
            height: 100% !important;
        }
        i.file-manager-rule {
            position: absolute;
            left: 0;
            bottom: 48px;
            text-align: center;
            display: block;
            width: 100%;
            font: 12px "Poppins", sans-serif;
            color: #393939;
        }

        ul.file-manager-options {
            position: initial !important;
        }
        ul.file-manager-options li {
            width: 100% !important;
            padding: 0 10px;
            float: none;
        }
        ul.file-manager-options li u {
            text-align: left !important;
        }
    </style>
@endpush

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
@endpush

@section('content')
    <div class="float-left">
        <a href="{{ route('Content.slider.scenes.index', ['slider_id' => $slider->id]) }}" class="btn btn-secondary mb-4">
            <i class="fa fa-chevron-left"></i>
            {!! __('ContentPanel::general.go_back_list') !!}
        </a>
    </div>
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content slidepage">
        <div class="title">
            {!! __('ContentPanel::scene.edit_scene') !!}
        </div>
        <div class="col">
            <div class="row">
                <form action="{!! route('Content.slider.scenes.update',[$slider->id,$scene->id]) !!}" method="post">
                    {!! csrf_field() !!}
                    <div class="col-6 right-area">
                        <div class="row">

                            <div class="status">
                                <div class="form-group">
                                    <div class="checkbox" style="margin: 0">
                                        <label class="active p-0">
                                            {!! __("ContentPanel::general.status_title.active") !!}
                                            <input type="radio" name="status"
                                                   value="1" {!! old('status', $scene->status) == 1 ? 'checked' : '' !!}>
                                        </label>
                                        <label class="passive p-0">
                                            {!! __("ContentPanel::general.status_title.passive") !!}
                                            <input type="radio" name="status"
                                                   value="0" {!! old('status', $scene->status) == '0' ? 'checked' : '' !!}>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="timer">
                                <label for="time">{!! __('ContentPanel::scene.time') !!}</label>
                                <input type="text" name="time" id="time" class="form-control" value="{!! old('time',$scene->time) !!}">
                            </div>

                            <div class="order">
                                <label for="order">{!! __('ContentPanel::general.order') !!}</label>
                                <input type="text" name="order" id="order" class="form-control" value="{!! old('order',$scene->order) !!}">
                            </div>
                        </div>
                    </div>


                    <ul class="nav nav-tabs col-12">
                        @foreach($countryGroups as $countryGroup)
                            @foreach($countryGroup->languages as $language)
                                <li>
                                    <a {!! $loop->first && $loop->parent->first ? 'class="show active"' :'' !!} data-toggle="tab"
                                       href="#{!! $countryGroup->code . $language->code !!}">
                                        <img src="{!! asset('vendor/mediapress/images/flags/'.$language->flag) !!}"
                                             height="13" alt=""> {!! strtoupper($language->code) . " (" . $countryGroup->title . ")" !!}
                                    </a>
                                </li>
                            @endforeach
                        @endforeach

                    </ul>
                    <div class="tab-content">
                        @foreach($countryGroups as $countryGroup)
                            @foreach($countryGroup->languages as $language)
                                <div id="{!! $countryGroup->code . $language->code !!}"
                                     class="tab-pane fade in  {!! $loop->first && $loop->parent->first ? 'show active' :'' !!}">

                                <div class="item row mb0">
                                    <div class="col-3 pl">
                                        <div class="head">&nbsp;</div>
                                    </div>
                                    <div class="col-9 pr">
                                        <div class="cont row mb0">
                                            <div class="col-4">
                                                <b><em>*</em> {!! __('ContentPanel::scene.desktop') !!}</b>
                                            </div>
                                            <div class="col-4">
                                                <b>{!! __('ContentPanel::scene.tablet') !!}</b>
                                            </div>
                                            <div class="col-4">
                                                <b>{!! __('ContentPanel::scene.mobile') !!}</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item row mb0">
                                    <div class="col-3 pl">
                                        <div class="head">
                                            <span>{!! __('ContentPanel::scene.images') !!}</span>
                                            <small><em>*</em>{!! __('ContentPanel::scene.blank_fields_text') !!}
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-9 pr">
                                        <div class="cont row mb0">
                                            <div class="col-4">
                                                <?php
                                                $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}', 1);
                                                $options['key'] = $countryGroup->code . $language->code . '_scene_desktop';
                                                $detail = $scene->details()->where('country_group_id', $countryGroup->id)->where('language_id',$language->id)->first();
                                                ?>
                                                <div class="file-box file-manager"
                                                     id="{!! $countryGroup->code . $language->code !!}_desktop"
                                                     data-options='{!! json_encode($options) !!}'>


                                                    <input name="{{$countryGroup->id}}[{!! $language->id !!}][files][desktop]"
                                                           id="file-manager-input" type="hidden"
                                                           value="{!! $detail ? $detail->files['desktop'] : '[]' !!}">

                                                    <a href="javascript:void(0);"
                                                       onclick="javascript:addImage($(this))">

                                                        <span class="file-icon"></span>
                                                        <i class="file-manager-rule">{!! __('ContentPanel::general.max_file_size', ['size' => '5']) !!}</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <?php
                                                $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"2048","max_file_count":"1","additional_rules":""}', 1);
                                                $options['key'] = $countryGroup->code . $language->code . '_scene_tablet';
                                                ?>
                                                <div class="file-box file-manager" id="{!! $countryGroup->code . $language->code !!}_tablet"
                                                     data-options='{!! json_encode($options) !!}'>
                                                    <input name="{{$countryGroup->id}}[{!! $language->id !!}][files][tablet]" type="hidden"
                                                           value="{!! $detail ? $detail->files['tablet'] : '[]' !!}">

                                                    <a href="javascript:void(0);"
                                                       onclick="javascript:addImage($(this))">
                                                        <span class="file-icon"></span>
                                                        <i class="file-manager-rule">{!! __('ContentPanel::general.max_file_size', ['size' => '2']) !!}</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <?php
                                                $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"1024","max_file_count":"1","additional_rules":""}', 1);
                                                $options['key'] = $countryGroup->code . $language->code . '_scene_mobile';
                                                ?>
                                                <div class="file-box file-manager" id="{!! $countryGroup->code . $language->code !!}_mobile"
                                                     data-options='{!! json_encode($options) !!}'>
                                                    <input name="{{$countryGroup->id}}[{!! $language->id !!}][files][mobile]" type="hidden"
                                                           value="{!! $detail ? $detail->files['mobile'] : '[]' !!}">
                                                    <a href="javascript:void(0);"
                                                       onclick="javascript:addImage($(this))">
                                                        <span class="file-icon"></span>
                                                        <i class="file-manager-rule">{!! __('ContentPanel::general.max_file_size', ['size' => '1']) !!}</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                @for($t=1;$t<=$slider->text;$t++)
                                    <div class="item row mb0">
                                        <div class="col-3 pl">
                                            <div class="head">
                                                <i>{{$t}}. {!! __('ContentPanel::scene.text_field') !!}</i>
                                            </div>
                                        </div>
                                        <div class="col-9 pr">
                                            <div class="cont row mb0">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <textarea
                                                                name="{{$countryGroup->id}}[{!! $language->id !!}][texts][{{$t}}][desktop]">{!! $detail->texts[$t]['desktop'] ?? '' !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0);">{!! __('ContentPanel::scene.copy_desktop_text') !!}</a>
                                                        <textarea
                                                                name="{{$countryGroup->id}}[{!! $language->id !!}][texts][{{$t}}][tablet]">{!! $detail->texts[$t]['tablet'] ?? '' !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0);">{!! __('ContentPanel::scene.copy_desktop_text') !!}</a>
                                                        <textarea
                                                                name="{{$countryGroup->id}}[{!! $language->id !!}][texts][{{$t}}][mobile]">{!! $detail->texts[$t]['mobile'] ?? '' !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endfor

                                @for($t=1;$t<=$slider->button;$t++)
                                    <div class="item row mb0">
                                        <div class="col-3 pl">
                                            <div class="head">
                                                <i>{{$t}}. {!! __('ContentPanel::scene.button_text') !!}</i>
                                                <small style="padding: 0;">{!! __('ContentPanel::scene.if_you_dont_enter_button_text_p1') !!} <br>{!! __('ContentPanel::scene.if_you_dont_enter_button_text_p2') !!}
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-9 pr">
                                            <div class="cont row mb0">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text"
                                                               name="{{$countryGroup->id}}[{!! $language->id !!}][buttons][{{$t}}][desktop]" value="{!! $detail->buttons[$t]['desktop'] ?? '' !!}">
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0);">{!! __('ContentPanel::scene.copy_desktop_text') !!}</a>
                                                        <input type="text"
                                                               name="{{$countryGroup->id}}[{!! $language->id !!}][buttons][{{$t}}][tablet]" value="{!! $detail->buttons[$t]['tablet'] ?? '' !!}">
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0);">{!! __('ContentPanel::scene.copy_desktop_text') !!}</a>
                                                        <input type="text"
                                                               name="{{$countryGroup->id}}[{!! $language->id !!}][buttons][{{$t}}][mobile]" value="{!! $detail->buttons[$t]['mobile'] ?? '' !!}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item row mb0">
                                        <div class="col-3 pl">
                                            <div class="head">
                                                <i>{!! __('ContentPanel::scene.link') !!}</i>
                                            </div>
                                        </div>
                                        <div class="col-9 pr">
                                            <div class="cont row mb0">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="{{$countryGroup->id}}[{!! $language->id !!}][url][button][{{$t}}][type]">
                                                            <option value="_blank" {!! isset($detail->url['button'][$t]['type']) && $detail->url['button'][$t]['type'] == '_blank' ? 'selected' :'' !!}>Dış Link</option>
                                                            <option value="_self" {!! isset($detail->url['button'][$t]['type']) && $detail->url['button'][$t]['type'] == '_self' ? 'selected' :'' !!}>Aynı Sayfa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-8">
                                                    <div class="form-group">
                                                        <input type="text"
                                                               name="{{$countryGroup->id}}[{!! $language->id !!}][url][button][{{$t}}][text]" value="{!! $detail->url['button'][$t]['text'] ?? '' !!}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endfor
                                @if($slider->button<1)
                                    <div class="item row mb0">
                                        <div class="col-3 pl">
                                            <div class="head">
                                                <i>{!! __('ContentPanel::scene.link') !!}</i>
                                            </div>
                                        </div>
                                        <div class="col-9 pr">
                                            <div class="cont row mb0">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="{{$countryGroup->id}}[{!! $language->id !!}][url][text][type]">
                                                            <option value="_blank">{!! __('ContentPanel::scene.external_link') !!}</option>
                                                            <option value="_self">{!! __('ContentPanel::scene.same_page') !!}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-8">
                                                    <div class="form-group">
                                                        <input type="text"
                                                               name="{{$countryGroup->id}}[{!! $language->id !!}][url][text][text]"
                                                               value="{!! $detail->url['text']['text'] ?? '' !!}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item row mb0">
                                        <div class="col-3 pl">
                                            <div class="head">
                                                <i>{!! __('ContentPanel::scene.link_scope') !!}</i>
                                            </div>
                                        </div>
                                        <div class="col-9 pr">
                                            <div class="cont row mb0">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="">
                                                                <div class="iradio_square-blue" style="position: relative;">
                                                                    <input name="{{$countryGroup->id}}[{!! $language->id !!}][url][holder]" value="text"
                                                                           {!! isset($detail->url['holder']) && $detail->url['holder'] == 'text' ? 'checked' :'' !!}
                                                                           type="radio" style="position: absolute; opacity: 0;">
                                                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                                </div>
                                                                {!! __('ContentPanel::scene.text_area') !!}
                                                            </label>
                                                            <label>
                                                                <div class="iradio_square-blue" style="position: relative;">
                                                                    <input name="{{$countryGroup->id}}[{!! $language->id !!}][url][holder]" value="image"
                                                                           {!! isset($detail->url['holder']) && $detail->url['holder'] == 'image' ? 'checked' :'' !!}
                                                                           type="radio" style="position: absolute; opacity: 0;">
                                                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                                </div>
                                                                {!! __('ContentPanel::scene.image') !!}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <hr>
                                <div class="submit">
                                    <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                                </div>

                            </div>
                            @endforeach
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dropify.css') !!}">
    <style>

        hr {
            margin-top: -30px;
            margin-bottom: 30px;
            clear: left;

        }
        .right-area{
            float:right;
            position: absolute;
            right: -120px;
            top: -60px;
        }

        .slidepage .right-area input{
            width: 70px !important;
            float: left !important;
        }
        .slidepage .right-area label{
            float: left !important;
            margin: 6px 3px 6px 20px !important;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $('.item .cont a').click(function () {
            var copy = $(this).parent().parent().parent().find('.col-4:nth-child(1) textarea,input').val();
            $(this).parent().find('textarea,input').val(copy);
        });
    </script>

    @if(function_exists('customFilemanager'))
        {!! customFilemanager() !!}
    @endif
@endpush
