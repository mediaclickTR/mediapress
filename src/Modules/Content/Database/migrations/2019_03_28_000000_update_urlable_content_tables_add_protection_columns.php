<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class UpdateUrlableContentTablesAddProtectionColumns extends Migration
{
    public const SITEMAPS = 'sitemaps';
    public const PAGES = 'pages';
    public const CATEGORIES = 'categories';
    public const PASS_WORD = "password";
    public const ALLOWED_ROLE_ID = 'allowed_role_id';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(self::SITEMAPS)) {
            Schema::table(self::SITEMAPS, function (Blueprint $table) {
                $table->string(self::PASS_WORD, 191)->nullable();
                $table->integer(self::ALLOWED_ROLE_ID)->default(null)->nullable();
            });
        }
        if (Schema::hasTable(self::PAGES)) {
            Schema::table(self::PAGES, function (Blueprint $table) {
                $table->string(self::PASS_WORD)->nullable();
                $table->integer(self::ALLOWED_ROLE_ID)->default(null)->nullable();
            });
        }
        if (Schema::hasTable(self::CATEGORIES)) {
            Schema::table(self::CATEGORIES, function (Blueprint $table) {
                $table->string(self::PASS_WORD)->nullable();
                $table->integer(self::ALLOWED_ROLE_ID)->default(null)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable(self::SITEMAPS)) {
            Schema::table(self::SITEMAPS, function (Blueprint $table) {
                $table->dropColumn(self::PASS_WORD);
                $table->dropColumn(self::ALLOWED_ROLE_ID);
            });
        }
        if (Schema::hasTable(self::PAGES)) {
            Schema::table(self::PAGES, function (Blueprint $table) {
                $table->dropColumn(self::PASS_WORD);
                $table->dropColumn(self::ALLOWED_ROLE_ID);
            });
        }
        if (Schema::hasTable(self::CATEGORIES)) {
            Schema::table(self::CATEGORIES, function (Blueprint $table) {
                $table->dropColumn(self::PASS_WORD);
                $table->dropColumn(self::ALLOWED_ROLE_ID);
            });
        }
    }
}
