<?php

namespace Mediapress\Facades;

use Illuminate\Support\Facades\Facade;

class URLEngine extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\MPCore\Foundation\URLEngine::class;
    }
}
