<?php

namespace Mediapress\Modules\MPCore\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\LanguagePart;
use Illuminate\Support\Collection;

class LanguagePartImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $rows = $rows->toArray();

        $heading = $rows[0];

        $lang = [];
        foreach ($heading as $key => $head) {
            if($head != null) {
                $head = explode('-', $head);
                $countryGroupCode = $head[0];
                $languageCode = $head[1];

                $lang[$key] = [
                    'languageId' => Language::where('code', $languageCode)->first()->id,
                    'countryGroupId' => CountryGroup::where('code', $countryGroupCode)->first()->id,
                ];
            }
        }


        foreach ($rows as $mainKey => $row)
        {
            if($mainKey == 0) {
                continue;
            }

            foreach ($row as $key => $r) {

                if($r == null) {
                    continue;
                }

                if($key == 0) {
                    $langPartKey = $r;
                    continue;
                }

                LanguagePart::updateOrCreate(
                    [
                        'key' => $langPartKey,
                        'country_group_id' => $lang[$key]['countryGroupId'],
                        'language_id' => $lang[$key]['languageId'],
                        'website_id' => session('panel.website.id'),

                    ],
                    [
                        'value' => $r
                    ]
                );

            }
        }
    }
}
