@if(!isset($panelmenu))
    {{ Form::open(['route' => 'Content.panelmenus.store']) }}
@else
    {{ Form::model($panelmenu, ['route' => ['Content.panelmenus.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$panelmenu->id) !!}
@endif
@csrf

<input type="hidden" value="{!! session("panel.website")->id !!}" name="website_id">

<div class="contents">
    <div class="mc-md-12">
        <div class="form-group rel rel">
            <div class="tit">{!! trans("ContentPanel::panelmenu.name") !!}</div>
            {!!Form::text('name', null, ["placeholder"=>trans("ContentPanel::panelmenu.name"), "class"=>"validate[required]"])!!}
        </div>
    </div>
</div>


<div class="float-right">
    <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

{{ Form::close() }}
