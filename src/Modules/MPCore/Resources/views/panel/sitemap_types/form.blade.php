@if(!isset($sitemap_type))
    {{ Form::open(['route' => 'MPCore.sitemap_types.store']) }}
@else
    {{ Form::model($sitemap_type, ['route' => ['MPCore.sitemap_types.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$sitemap_type->id) !!}
@endif
@csrf

{{ Form::close() }}