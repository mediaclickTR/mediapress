<?php

return [
    'menus' => 'Menüler',
    'menu_detail' => 'Menü Detayı',
    'edit_menu' => 'Menü Düzenle',
    'settings' => 'Ayarlar',

    'status' => 'Yayın Durumu',
    'name' => 'Menü Adı',
    'slug' => 'Menü URL',
    'type' => 'Menü Türü',
    'sections' => 'Menu Bölümü',

    'select_url' => 'Url Seçimi',
    'link_types' => 'Menü Linkleri',
    'self' => 'Kendi içinde (_self)',
    'blank' => 'Yeni sekmede (_blank)',

    'view' => 'Menü Görünüm',
    'dropdown_menu' => 'Dropdown Menü',
    'mega_menu' => 'Mega Menü',
    'single_menu' => 'Tek Menü',

    'targets' => 'Hedef Türü (Target)',
    'inlink' => 'İç Link',
    'outlink' => 'Dış Link',
    'emptylink' => 'Boş Link',

    'header_menu' => 'Header Menü',
    'footer_menu' => 'Footer Menü',
    'sidebar_menu' => 'Sidebar Menü',
    'other_menu' => 'Diğer Menü',

    'empty_menu_detail' => ':language diline henüz menü eklenmedi. Sağ taraftaki "Yeni Ekle" bölümünden ekleyebilirsiniz.',
    'info' => 'Yeni ekleyeceğiniz başlık, sol tarafa eklenecektir.Bu başlığı istediğiniz yere sürükleyerek, kaydet butonuna basınız.',


    'copy_menu' => 'Menüyü kopyala',
    'source_country_group' => 'Kaynak Ülke Grubu',
    'target_country_group' => 'Hedef Ülke Grubu',
    'source_language' => 'Kaynak Dili',
    'target_language' => 'Hedef Dili',

    'new_page_1' => "Ya da yeni",
    'new_page_2' => "sayfa oluşturun",


    'delete.info' => 'Sildiğiniz bilgiler kalıcı olarak sistemden kaldırılacaktır.',
    'delete.child_info' => 'Sildiğiniz menünün alt menüleri silinsin mi?',
    'draft.info' => 'Taslağınızı kaydettiniz mi ?',
    'copy.info' => 'Hedef dile ait tüm kayıtlar silinecektir. Onaylıyor musunuz?',
    'no_data_source_lang' => 'Kaynak dile ait menü bilgisi bulunamadı. Lütfen başka bir dil seçiniz.',
];
