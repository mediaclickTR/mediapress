{{--@php dump($actions) @endphp--}}
@php

global $_first_action, $dates, $icons;
$icons = [
    "index"             =>  "list",
    "create"            =>  "plus",
    "update"            =>  "pen",
    "edit_update"       =>  "pen",
    "delete"            =>  "minus",
    "export"            =>  "share",
    "reorder"           =>  "sort",
    "view"              =>  "eye",
    "mark_as_unread"   =>  "envelope",
    "auth" =>"shield-alt"
 ];
$_first_action = true;
$dates=[];

    function renderActions($actionset, $entity, $abilities, $forbidden_abilities, $options=[]) {

        global $_first_action,$dates, $icons;
        if($_first_action){
            $dates["first"]=date("H:i:s", time());
        }
        $dates["last"]=date("H:i:s", time());

        $title_undefined = trans('AuthPanel::roles.authorization.optionUndefined');
        $title_default = trans('AuthPanel::roles.authorization.optionUseDefault');
        $title_allow = trans('AuthPanel::roles.authorization.optionAllow');
        $title_deny = trans('AuthPanel::roles.authorization.optionDeny');
        $forbidden = null; $allowed = null;
        $text= '<div id="'.$options["accordion_id"].'" class="_accordion">';
        foreach($actionset as $setkey => $set){

            $name_affix = trans($set["name_affix"] ?? "");
            $node_type = $set["node_type"] ?? "";
            $title_undefined = \Arr::get($set,'data.descendant_of_variation',false) || \Arr::get($set,'data.is_variation',false) ?  $title_default : $title_undefined;

            $set["name"] =  trans($set["name"]);
            $set["slug"] = uniqid();
            $is_open = isset($set["settings"]["auth_view_collapse"]) && $set["settings"]["auth_view_collapse"]=="in" ? true : false;
            if($node_type=="grouper"){
                $text.= "<div class=\"grouper\">";
            }else{
            $text.= '
                    <div class="card">
                        <div class="card-header" id="heading_'.$set["slug"].'">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link'.( $_first_action || $is_open ?"": " collapsed").'" data-toggle="collapse" data-target="#collapse_'.$set["slug"].'" aria-expanded="false" aria-controls="collapse_'.$set["slug"].'">'
                                        . $set["name"].($name_affix ? " - " . $name_affix : "").
                                '</button>
                            </h5>
                        </div>
                        <div id="collapse_'.$set["slug"].'" class="collapse'.( $_first_action || $is_open ?" show": "").'" aria-labelledby="heading_'.$set["slug"].'" data-parent="#'. $options["accordion_id"] .'">
                            <div class="card-body">';

            }

            if(count($set["actions"])){
                    foreach ($set["actions"] as $actkey => $action){
                        $barekey = last(explode('.',$actkey));
                        $icon = isset($action["icon"]) && $action["icon"] ? $action["icon"] : ("fa-". (array_key_exists($barekey,$icons) ? $icons[$barekey]: "circle"));
                        $description=isset($action["description"]) && $action["description"] ? $action["description"] : "";
                        $forbidden = null; $allowed = null;
                        $forbidden = in_array($actkey, $forbidden_abilities) ? true : false;
                        if( ! $forbidden){
                            $allowed = in_array($actkey, $abilities) ? true : false;
                        }
                        //<i class="fa '. $icon .'"></i>
                        $text.='<div class="form-group"><span><i class="fa '. $icon .' mr-2"></i> '.trans($action['name']).'</span><div class="checkbox">';
                        $default_is_forbidden=false;
                        if(!($allowed || $forbidden)){
                            $default_is_forbidden=true;
                        }
/*                        if($actkey=="content.websites.website1.sitemaps.details.edit_update"){
                            dump($set,545);
                        }*/
                        if(\Arr::get($set,'data.descendant_of_variation',false) || \Arr::get($set,'data.is_variation',false)){
                            $default_is_forbidden = false;
                            $text.='<label for="'.$actkey.'-null'.'">'.$title_undefined.'<input type="radio"  id="'.$actkey.'-null'.'"  name="actions['.$actkey.']" value="" '.(!($allowed || $forbidden) ? "checked" : "").'></label>';
                        }


                        $text .= '<label for="'.$actkey.'-allow'.'" class="active">'.$title_allow.'<input type="radio" id="'.$actkey.'-allow'.'"  name="actions['.$actkey.']" value="allow" '.($allowed ? "checked" : "").'></label>
                          <label for="'.$actkey.'-deny'.'" class="passive">'.$title_deny.'<input type="radio" id="'.$actkey.'-deny'.'"  name="actions['.$actkey.']" value="deny" '.($forbidden || $default_is_forbidden ? "checked" : "").'></label>';
                        $text .= '</div>';
                        if($description){
                            $text .= '<p class="permission_description">'.$description.'</p>';
                        }
                        $text .= '</div>';
                    }
            }

            $_first_action = false;

            if(count($set['subs'])){
                $text.='<div class="clearfix"></div>';
                $id = uniqid("accordion_");
                $text.= /*'<h3 class="name">'. trans("AuthPanel::".'roles.app.sub_modules') .'</h3>'.*/renderActions($set['subs'],$entity,$abilities,$forbidden_abilities,["accordion_id"=>$id]);
            }

            if(count($set['variations'])){
                $text.='<div class="clearfix"></div>';
                $id = uniqid("accordion_");
                $title = trans($set["variations_title"] ?? "MPCorePanel::auth.general.recorded_items");//isset($set["variations_title"]) ? trans($set["variations_title"]) : trans("AuthPanel::".'roles.app.recorded_items') .' - '. $set["name"];
                $text.= '<h3 class="name">'. $title.'</h3>'.renderActions($set['variations'], $entity, $abilities, $forbidden_abilities,["accordion_id"=>$id]);
            }

            if($node_type=="grouper"){
                $text .= "</div>";
            }else{
                $text .= "</div></div></div>";
            }
        }
        return $text.'</div>';
    }

    $forbidden_abilities = $role->getForbiddenAbilities()->pluck("name")->toArray();
    $abilities = $role->getAbilities(false)->pluck("name")->toArray();

    $forbidden = null; $allowed = null;
    $forbidden = in_array("*", $forbidden_abilities) ? true : false;

    if( ! $forbidden){
        $allowed = in_array("*", $abilities) || $role->can("*") ? true : false;
    }
@endphp
{!! renderActions($actions["actions"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{!! renderActions($actions["subs"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{!! renderActions($actions["variations"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{{--@php dd($actions) @endphp--}}
{{--@dump($dates)--}}
