<?php

namespace Mediapress\Foundation;


use Illuminate\Database\Eloquent\Collection as BaseCollection;

class Cluster extends BaseCollection
{
    public const CHILDREN = 'children';
    public $lft = 'lft';
    public $rgt = 'rgt';
    public $parent_id = 'parent_id';
    public $dpt = 'depth';
    public $collection = [];

    public function __construct($items = [], $parameters = [])
    {

        foreach ($parameters as $key => $parameter) {
            $this->{$key} = $parameter;
        }

        $this->items = $items->sortBy($this->lft);
    }
    public function unBuild()
    {
       return (new BaseCollection($this->unBuildTree($this->toTree())))->sortBy('lft');
    }
    public function unBuildTree($elements, $branch = [])
    {
        foreach ($elements as $element)
        {

            if (isset($element[self::CHILDREN]))
            {
                $branch = $this->unBuildTree($element[self::CHILDREN], $branch);
                unset($element[self::CHILDREN]);
            }
            $branch[] = $element;
        }
        return $branch;
    }
    public function toTree($root = false)
    {

        if ($this->isEmpty()) {
            return new static;
        }

        $this->linkNodes();
        $items = [];

        $root = $this->getRootNodeId($root);
        /** @var BaseCollection $node */
        foreach ($this->items as $node) {

            if ($this->getParentId($node) == $root) {
                $items[] = $node;
            }
        }

        return new BaseCollection($items);
    }

    public function linkNodes()
    {

        if ($this->isEmpty()) {
            return $this;
        }

        $groupedNodes = $this->items->groupBy($this->getParentIdName());

        /** @var BaseCollection $node */
        foreach ($this->items as $node) {
            if (!$this->getParentId($node)) {
                $node->setRelation('parent', null);
            }

            $children = $groupedNodes->get($node->getKey(), []);
            /** @var BaseCollection $child */
            foreach ($children as $child) {
                $child->setRelation('parent', $node);
            }
            $node->setRelation(self::CHILDREN, BaseCollection::make($children));
        }
        return $this;
    }

    public function getDefaultColumns()
    {
        return [$this->lft, $this->rgt, $this->parentId, $this->dpt];
    }

    /**
     * @return string
     */
    public function getLftName()
    {
        return $this->lft;
    }

    /**
     * @param string $lft
     */
    public function setLftName($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return string
     */
    public function getRgtName()
    {
        return $this->rgt;
    }

    /**
     * @param string $rgt
     */
    public function setRgtName($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return string
     */
    public function getDptName()
    {
        return $this->dpt;
    }

    /**
     * @param string $dpt
     */
    public function setDptName($dpt)
    {
        $this->dpt = $dpt;
    }

    /**
     * @return string
     */
    public function getParentIdName()
    {
        return $this->parent_id;
    }

    /**
     * @param string $parentIdName
     */
    public function setParentIdName($parentIdName)
    {
        $this->parent_id = $parentIdName;
    }

    private function getParentId($node)
    {
        $parentName = $this->getParentIdName();
        return $node->{$parentName};
    }

    public function getLft($node)
    {
        $lftName = $this->getLftName();
        return $node->{$lftName};
    }

    public function getRgt($node)
    {
        $rgtName = $this->getRgtName();
        return $node->{$rgtName};
    }

    private function getRootNodeId($root = false)
    {

        if (Cluster::isNode($root)) {
            return $root->getKey();
        }
        if ($root !== false) {
            return $root;
        }
        // If root node is not specified we take parent id of node with
        // least lft value as root node id.
        $leastValue = null;
        /** @var BaseCollection $node */
        foreach ($this->items as $node) {

            if ($leastValue === null || $this->getLft($node) < $leastValue) {
                $leastValue = $this->getLft($node);
                $root = $this->getParentId($node);
            }
        }
        return $root;
    }

    public static function isNode($node)
    {
        return is_object($node) && in_array(BaseCollection::class, (array)$node);
    }
}