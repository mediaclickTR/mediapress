<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\MPMailler;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Heraldist;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Mediapress\Modules\Heraldist\Models\Maillist;
use Mediapress\Modules\Heraldist\Models\MailTemplate;
use Mediapress\Modules\Entity\Facades\Entity;

class PanelMaillistController extends Controller
{

    public const HERALDIST_MPMAILLER_MAILLISTS_INDEX = 'Heraldist.mpmailler.maillists.index';
    public const HERALDIST_PANEL_MAIL_SERVICE_MAILLIST_NAME = "HeraldistPanel::mail_service.maillist_name";
    public const HERALDIST_PANEL_MAIL_SERVICE_EMAIL_SELECT = "HeraldistPanel::mail_service.email_select";
    public const HERALDIST_PANEL_MAIL_SERVICE_WEBSITE = "HeraldistPanel::mail_service.website";
    public const NAME = 'name';
    public const EMAILS = 'emails';
    public const WEBSITE_ID = 'website_id';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const MESSAGE = 'message';
    public const MESSAGE1 = self::MESSAGE;
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const WEBSITES = 'websites';
    public const REQUIRED = 'required';
    public const FILLED = 'filled';
    public const ACCESSDENIED = 'accessdenied';

    public function index()
    {
        if(!userAction('message.maillists.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $website_id = session("panel.website")->id;
        $maillists = Maillist::where('id','<>',0);

        if (request()->get('list') && request()->get('list')=="all") {
            $maillists = $maillists->paginate(25);
        }
        else {
            $maillists=$maillists->whereHas(self::WEBSITES, function($query) use ($website_id) {
                $query->where('id', $website_id);
            })->paginate(25);
        }

        return view('HeraldistPanel::mail_service.mpmailler.maillists.index', compact('maillists'));
    }

    public function create()
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = Entity::getMaillist();
        $mailtemplates = MailTemplate::all();

        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck(self::NAME, "id");

        return view("HeraldistPanel::mail_service.mpmailler.maillists.create", compact("mailtemplates","data", self::WEBSITES));
    }

    public function edit($id)
    {
        $data = Entity::getMaillist();
        $maillist = Maillist::find($id);

        $emails = explode(',',$maillist->emails);

        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck(self::NAME, "id");

        return view('HeraldistPanel::mail_service.mpmailler.maillists.edit', compact("maillist","mailtemplates","data", self::EMAILS, self::WEBSITES));
    }

    public function store(Request $request)
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $fields = [
            self::NAME => trans(self::HERALDIST_PANEL_MAIL_SERVICE_MAILLIST_NAME),
            self::EMAILS => trans(self::HERALDIST_PANEL_MAIL_SERVICE_EMAIL_SELECT),
            self::WEBSITE_ID => trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE),
        ];
        $rules = [
            self::NAME => 'required|max:255',
            self::EMAILS => self::REQUIRED,
            self::WEBSITE_ID => self::REQUIRED,
        ];
        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_MAILLIST_NAME)]),
            'emails.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_EMAIL_SELECT)]),
            'website_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE)]),
        ];

        $emails ='';
        $usernames = '';


        if($request->emails) {
            foreach ($request->emails as $email)
            {
                $data = explode("-",$email);
                $emails .= $data[0] . ',';
                if ($data[1]){
                    $usernames .= $data[1] . ',';
                }
                else{
                    $usernames .= "-" . ',';
                }
            }
        }

        $emails = trim($emails,",");
        $usernames = trim($usernames,",");

        $datas = [
            self::NAME => $request->name,
            self::EMAILS => $emails,
            'usernames' => $usernames,
        ];

        //Son karekteri temizle
        $emails = rtrim($emails,",");
        $request->emails = $emails;
        $this->validate($request, $rules, $messages, $fields);
        $insert=Maillist::firstOrCreate($datas);

        // Log
        if ($insert){
            foreach ($request->website_id as $website) {
                $insert->websites()->attach([$website]);
            }
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAILLISTS_INDEX))->with(self::MESSAGE1, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        $fields = [
            self::NAME => trans(self::HERALDIST_PANEL_MAIL_SERVICE_MAILLIST_NAME),
            self::EMAILS => trans(self::HERALDIST_PANEL_MAIL_SERVICE_EMAIL_SELECT),
            self::WEBSITE_ID => trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE),
        ];
        $rules = [
            self::NAME => 'required|max:255',
            self::EMAILS => self::REQUIRED,
            self::WEBSITE_ID => self::REQUIRED,
        ];
        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_MAILLIST_NAME)]),
            'emails.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_EMAIL_SELECT)]),
            'website_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE)]),
        ];

        $emails ='';
        $usernames = '';

        if($request->emails) {
            foreach ($request->emails as $email)
            {
                $data = explode("-",$email);
                $emails .= $data[0] . ',';
                if ($data[1]) {
                    $usernames .= $data[1] . ',';
                }
                else {
                    $usernames .= "-" . ',';
                }
            }
        }

        $emails = trim($emails,",");
        $usernames = trim($usernames,",");

        $datas = [
            self::NAME => $request->name,
            self::EMAILS => $emails,
            'usernames' => $usernames
        ];

        $this->validate($request, $rules, $messages, $fields);

        $update=Maillist::updateOrCreate(['id'=>$request->id],$datas);
        // Log
        if ($update){
            // Pivottan sil
            $update->websites()->detach();
            foreach($request->website_id as $website){
                // Pivota ekle
                $update->websites()->attach([$website]);
            }
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAILLISTS_INDEX))->with(self::MESSAGE1, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(!userAction('message.maillists.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $maillist = Maillist::find($id);

        if($maillist)
        {
            $maillist->delete();
            // Pivottan sil
            $maillist->websites()->detach();
            // Log
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$maillist);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAILLISTS_INDEX))->with(self::MESSAGE1, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

}