<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchContentView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW search_content AS SELECT    
                s.id AS model_id,
                'Mediapress\\\\Modules\\\\Content\\\\Models\\\\Sitemap' AS model_type,
                sd.language_id AS language_id,
                sd.name AS detail_name,
                sd.detail AS detail_detail,
                su.id AS url_id,
                s.cvar_1,
                s.cvar_2,
                s.ctex_1,
                s.ctex_2,
                s.cint_1,
                s.cint_2,
                s.cdat_1,
                s.cdat_2,
                s.cdec_1,
                s.cdec_2,
                sde.key AS 'detail_key',
                sde.value AS 'detail_value',
                se.key AS 'key',
                se.value AS 'value'
            FROM
                sitemaps AS s
            LEFT JOIN sitemap_details AS sd
            ON
                s.id = sd.sitemap_id
            LEFT JOIN sitemap_detail_extras AS sde
            ON
                sd.id = sde.sitemap_detail_id
            LEFT JOIN sitemap_extras AS se
            ON
                s.id = se.sitemap_id
            LEFT JOIN urls AS su
            ON
                sd.id = su.model_id
            WHERE
                su.type = 'original' AND su.model_type = 'Mediapress\\\\Modules\\\\Content\\\\Models\\\\SitemapDetail' AND s.searchable = 1
            UNION
            SELECT
                p.id AS model_id,
                'Mediapress\\\\Modules\\\\Content\\\\Models\\\\Page' AS model_type,
                pd.language_id AS language_id,
                pd.name AS detail_name,
                pd.detail AS detail_detail,
                pu.id AS url_id,
                p.cvar_1,
                p.cvar_2,
                p.ctex_1,
                p.ctex_2,
                p.cint_1,
                p.cint_2,
                p.cdat_1,
                p.cdat_2,
                p.cdec_1,
                p.cdec_2,
                pde.key AS 'detail_key',
                pde.value AS 'detail_value',
                pe.key AS 'key',
                pe.value AS 'value'
            FROM
                pages AS p
            LEFT JOIN page_details AS pd
            ON
                p.id = pd.page_id
            LEFT JOIN page_detail_extras AS pde
            ON
                pd.id = pde.page_detail_id
            LEFT JOIN page_extras AS pe
            ON
                p.id = pe.page_id
            LEFT JOIN urls AS pu
            ON
                pd.id = pu.model_id
            WHERE
                pu.type = 'original' AND pu.model_type = 'Mediapress\\\\Modules\\\\Content\\\\Models\\\\PageDetail'
            UNION
            SELECT
                c.id AS model_id,
                'Mediapress\\\\Modules\\\\Content\\\\Models\\\\Category' AS model_type,
                cd.language_id AS language_id,
                cd.name AS detail_name,
                cd.detail AS detail_detail,
                cu.id AS url_id,
                c.cvar_1,
                c.cvar_2,
                c.ctex_1,
                c.ctex_2,
                c.cint_1,
                c.cint_2,
                c.cdat_1,
                c.cdat_2,
                c.cdec_1,
                c.cdec_2,
                cde.key AS 'detail_key',
                cde.value AS 'detail_value',
                ce.key AS 'key',
                ce.value AS 'value'
            FROM
                categories AS c
            LEFT JOIN category_details AS cd
            ON
                c.id = cd.category_id
            LEFT JOIN category_detail_extras AS cde
            ON
                cd.id = cde.category_detail_id
            LEFT JOIN category_extras AS ce
            ON
                c.id = ce.category_id
            LEFT JOIN urls AS cu
            ON
                cd.id = cu.model_id
            WHERE
                cu.type = 'original' AND cu.model_type = 'Mediapress\\\\Modules\\\\Content\\\\Models\\\\CategoryDetail'
    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS search_content');
    }
}
