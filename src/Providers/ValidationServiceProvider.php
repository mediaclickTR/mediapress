<?php

namespace Mediapress\Providers;

use Mediapress\Foundation\CreditCard;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('tc', function ($attribute, $value, $parameters, $validator) {
            if(strlen($value) != 11){
                return false;
            }
            $array = ['11111111110','22222222220','33333333330','44444444440','55555555550','66666666660','7777777770','88888888880','99999999990'];
            $valid1=((7*($value[0]+$value[2]+$value[4]+$value[6]+$value[8])-($value[1]+$value[3]+$value[5]+$value[7]))%10)==$value[9];
            $valid2=(($value[0]+$value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]+$value[8]+$value[9])%10)==$value[10];
            return $valid1 && $valid2 && !in_array($value,$array);
        });
        Validator::extend('ccn', function($attribute, $value, $parameters, $validator) {
            return CreditCard::validCreditCard($value)['valid'];
        });
        Validator::extend('ccd', function($attribute, $value, $parameters, $validator) {
            try {
                $value = explode('/', $value);
                return CreditCard::validDate(strlen($value[1]) == 2 ? (2000+$value[1]) : $value[1], $value[0]);
            } catch(\Exception $e) {
                return false;
            }
        });
        Validator::extend('cvc', function($attribute, $value, $parameters, $validator) {
            return ctype_digit($value) && (strlen($value) == 3 || strlen($value) == 4);
        });

        Validator::extend('captcha', 'Mediapress\\Modules\\Heraldist\\Validators\\Recaptcha@validate');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
