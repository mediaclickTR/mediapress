<?php

namespace Mediapress\Modules\RenderableEditor;

use Illuminate\Http\Request;
use Mediapress\Models\MPModule;

class RenderableEditor extends MPModule
{
    public $name = "RenderableEditor";
    public $url = "mp-admin/RenderableEditor";
    public $menus;
    public $description = "Renderable Editor of Mediapress";
    public $author = "";

    public function fillMenu($menu)
    {

        #region Header Menu > Datasets > Users > Set
        if (config('app.env') == 'local') {
            data_set($menu, 'header_menu.settings.cols.editor.name', "İçerik Araçları");

            $datasets_cols_users_rows_set = [
                [
                    "type" => "submenu",
                    "title" => "Renderable Editör",
                    "url" => route("admin.editors.index")
                ]
            ];

            return dataGetAndMerge($menu, 'header_menu.settings.cols.editor.rows', $datasets_cols_users_rows_set);
        } else {
            return $menu;
        }
        #endregion

    }

}
