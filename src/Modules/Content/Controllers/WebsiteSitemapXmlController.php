<?php

    namespace Mediapress\Modules\Content\Controllers;

    use Illuminate\Http\Request;
    use Mediapress\Facades\DataSourceEngine;
    use Mediapress\Http\Controllers\Controller;

    use Illuminate\Support\Facades\DB;
    use Mediapress\Modules\Content\Models\Sitemap;
    use Mediapress\Modules\Content\Models\SitemapXml;
    use Mediapress\Modules\Content\Models\SitemapXmlBlock;
    use Mediapress\Modules\Content\Models\Website;
    use Mediapress\Modules\MPCore\Models\BaseModel;
    use Mediapress\Modules\MPCore\Models\CountryGroup;
    use Mediapress\Modules\MPCore\Models\CountryGroupLanguage;

    class WebsiteSitemapXmlController extends Controller
    {
        public const LANGUAGE_ID = "language_id";
        public const CONTENT_TYPE = 'Content-Type';
        public const APPLICATION_XHTML_XML = 'application/xhtml+xml';
        public const HOMEPAGE = "homepage";
        public const HOMEPAGE1 = self::HOMEPAGE;
        public const CONTENT = "Content";
        public const DEFAULT_SITEMAP_XML_BLOCK_DATA = "DefaultSitemapXmlBlockData";
        public const OTHER = "other";
        private $defaultLimit = 1200;

        public $cg_id = null;
        public $lng_id = null;
        public $website = null;
        public $website_id = null;
        public $entries_per_page = null;
        public $page_number = null;
        public $other_languages = [];
        public $sitemaps_to_index_pages_of = [];
        public $sitemaps_to_index_categories_of = [];
        public $sitemaps_to_index_detail_of = [];
        public $priorities_table = null;
        public $change_frequencies_table = null;
        public $xml_model = null;
        public $count = null;


        public function index(Request $request, $param1 = null, $param2=null, $param3=null)
        {

            $cnt = count(array_filter([$param1,$param2,$param3]));
            switch ($cnt){
                case 3:
                    $xml = $param3;
                    break;
                case 2:
                    $xml = $param2;
                    break;
                case 1:
                    $xml = $param1;
                    break;
            }



            $this->website = mediapress()->website();
            if(!$this->website){
                 $this->website = Website::first();
            }
            $this->website_id = $this->website->id;
            $xml = SitemapXml::where("filename", $xml)->where("website_id", $this->website_id)->with("blocks")->first()/*->first()*/;

            if (!$xml) {
                notFound();
            }

            $this->xml_model = $xml;

            // TODO: Compute entries per page by an helper function. It's no longer set by model itself.
            $this->entries_per_page = getRecommendedSplitSizeForSitemapXmls($this->website);

            if ($request->has("cg")) {
                $this->cg_id = $request->cg * 1;
            }
            if ($request->has("lng")) {
                $this->lng_id = $request->lng * 1;
            }
            if ($request->has("page")) {
                $this->page_number = $request->page * 1;
            }

            if (!($this->cg_id && $this->lng_id)) {
                return $this->serveBaseXml();
            } elseif (!$this->page_number) {
                return $this->serveIntermediateXml();
            } else {
                return $this->serveXml();
            }


        }

        public function serveBaseXml()
        {

            $variations = $this->website->variations(true);
            $files = [];

            foreach ($variations as $variation) {
                $files[] = getPathOfFileUrl() . "/" . $this->xml_model->filename . ".xml?cg=" . $variation["country_group_id"] . "&lng=" . $variation[self::LANGUAGE_ID];
            }
            return response()->view('ContentPanel::xml.main', compact('files'))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);

        }

        public function serveIntermediateXml()
        {


            $counter = $this->getCount();
            if ($counter > $this->entries_per_page) {
                $pages = ceil($counter / $this->entries_per_page);
                for ($i = 1; $i <= $pages; $i++) {
                    $files[] = request()->root() . "/" . $this->xml_model->filename . ".xml?cg=" . $this->cg_id . "&lng=" . $this->lng_id . "&page=" . $i;
                }
                return response()->view('ContentPanel::xml.main', compact('files'))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);
            }
            return $this->serveXml();

        }

        private function getCount()
        {
            $query = $this->getQuery(true);
            //$q= "".($query);
            //echo $q; die();
            $this->count = DB::select($query)[0]->cnt * 1;
            return $this->count;
        }


        public function serveXml()
        {

            $query = $this->getQuery(false);

            $_rows = collect(DB::select($query));

            $rows=[];
            $root = request()->root();
            foreach($_rows as $row){
                $alternates = [];
                if($row->alternates){
                    $alternates_ = preg_split('/;/',$row->alternates);
                    foreach($alternates_ as $alternate){
                        $alternate = preg_split('/:/',$alternate);
                        $alternates[$alternate[0]]=$root . $alternate[1];
                    }
                }
                $rows[]=[
                    "link"=> $root . $row->url,
                    "lang"=> $row->lng_code,
                    "priority" => $row->priority,
                    "change_frequency" => $row->change_frequency,
                    "alternates"=>$alternates
                ];
            }

            return response()->view('ContentPanel::xml.xml', compact('rows'))->header(self::CONTENT_TYPE, self::APPLICATION_XHTML_XML);

        }

        private function getQuery($for_count = false)
        {
            $limit_str = "";
            $limit = null;
            $offset = null;
            $paginate = false;

            if(!$for_count){
                $counter = is_null($this->count) ? $this->getCount() : $this->count;
                if ($counter > $this->entries_per_page){
                    $limit=$this->entries_per_page;

                    $page = $this->page_number ?? 1;
                    $paginate = true;
                    if($page > 1){
                        $offset = ($this->entries_per_page * $page)-$this->entries_per_page;
                    }

                    $limit_str=" LIMIT ". ($offset ? $offset.",":"").$limit;
                }

            }




            //$xml = SitemapXml::where("filename", $xml)->where("website_id", $this->website_id)->with("blocks")->first();

            $blocks = $this->xml_model->blocks;//SitemapXmlBlock::where("sitemap_xml_id", $this->xml_model->id)->get()

            $this->other_languages = CountryGroupLanguage::where("country_group_id", $this->cg_id)->where(self::LANGUAGE_ID, "<>", $this->lng_id)->get()->pluck(self::LANGUAGE_ID)->toArray();


            $sitemaps = Sitemap::whereHas('websites', function ($q) {
                $q->where('id', $this->website_id);
            })->where("status", BaseModel::ACTIVE)->get();


            $this->sitemaps_to_index_pages_of = [];
            $this->sitemaps_to_index_categories_of = [];
            $this->sitemaps_to_index_detail_of = [];

            $default_settings_for_homepage = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, ["sitemap_feature" => self::HOMEPAGE]);
            $default_settings_for_others = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, [""]);
            $default_settings = [
                self::HOMEPAGE => $default_settings_for_homepage->getData(),
                self::OTHER => $default_settings_for_others->getData()
            ];

            $this->priorities_table = $this->derivePrioritiesTable($default_settings, $blocks, $sitemaps);
            $this->change_frequencies_table = $this->deriveChangeFrequenciesTable($default_settings, $blocks, $sitemaps);

            foreach ($sitemaps as $sitemap) {
                $dataset = count($blocks) ? ($blocks->where("sitemap_id", $sitemap->id)->first() ?? null) : null;
                if (is_null($dataset)) {
                    $dataset = $sitemap->feature_tag == self::HOMEPAGE1 ? $default_settings[self::HOMEPAGE] : $default_settings[self::OTHER];
                } else {
                    $dataset = $dataset->toArray();
                }
                $has_categories = $sitemap->category ? true : false;
                $has_pages = $sitemap->sitemapType->sitemap_type_type == "dynamic" ? true : false;

                if ($dataset["include_pages"] == true) {
                    $this->sitemaps_to_index_pages_of[] = $sitemap->id;
                }
                if ($has_categories && $dataset["include_categories"] == true) {
                    $this->sitemaps_to_index_categories_of[] = $sitemap->id;
                }
                if (/*$sitemap->detail &&*/ $dataset["include_detail"] == true) {
                    $this->sitemaps_to_index_detail_of[] = $sitemap->id;
                }

            }


            $queries = [];


            if (count($this->sitemaps_to_index_pages_of)) {
                $queries[] = $this->getPagesQuery();
            }

            if (count($this->sitemaps_to_index_categories_of)) {
                $queries[] = $this->getCategoriesQuery();
            }

            if (count($this->sitemaps_to_index_detail_of)) {
                $queries[] = $this->getDetailQuery();
            }


            $query = implode(" UNION ", array_filter($queries));
            if ($for_count && count($queries)) {
                $query = "SELECT COUNT(united.model_id) as cnt FROM (" . $query . ") united";
            } else{
                $query .= $limit_str;
            }

            return $query;

        }


        private function derivePrioritiesTable($rules, $blocks, $sitemaps)
        {

            $homepage_rule = $rules[self::HOMEPAGE1];
            $others_rule = $rules["other"];

            $rows = [];
            foreach ($sitemaps as $sitemap) {
                $customized = false;
                $fields = [];
                $default_settings= $sitemap->feature_tag == self::HOMEPAGE1 ? $homepage_rule : $others_rule;
                $dataset = count($blocks) ? ($blocks->where("sitemap_id", $sitemap->id)->first() ?? null) : null;
                if (is_null($dataset)) {
                    $dataset = $default_settings;
                } else {
                    $customized = true;
                    $dataset = $dataset->toArray();
                }

                $has_categories = $sitemap->category ? true : false;
                $has_pages = $sitemap->sitemapType->sitemap_type_type == "dynamic" ? true : false;

                $detail_priority = ( ! is_null($dataset["detail_priority"]) && $dataset["detail_priority"]!=$default_settings["detail_priority"]) ? $dataset["detail_priority"] : $default_settings["detail_priority"];
                $categories_priority = ( ! is_null($dataset["categories_priority"]) && $dataset["categories_priority"]!=$default_settings["categories_priority"]) ? $dataset["categories_priority"] : $default_settings["categories_priority"];
                $pages_priority = ( ! is_null($dataset["pages_priority"]) && $dataset["pages_priority"]!=$default_settings["pages_priority"]) ? $dataset["pages_priority"] : $default_settings["pages_priority"];


                $fields[] = "$sitemap->id AS sitemap_id";
                $fields[] = $detail_priority . " AS detail_priority";
                $fields[] = $categories_priority . " AS categories_priority";
                $fields[] = $pages_priority . " AS pages_priority";

                $fields = "SELECT " . implode(", ", $fields);
                $rows[] = $fields;
            }

            return "(" . implode(" UNION ", $rows) . ") AS priorities";


        }
        private function deriveChangeFrequenciesTable($rules, $blocks, $sitemaps)
        {

            $homepage_rule = $rules[self::HOMEPAGE1];
            $others_rule = $rules["other"];

            $rows = [];
            foreach ($sitemaps as $sitemap) {
                $customized = false;
                $fields = [];
                $default_settings= $sitemap->feature_tag == self::HOMEPAGE1 ? $homepage_rule : $others_rule;
                $dataset = count($blocks) ? ($blocks->where("sitemap_id", $sitemap->id)->first() ?? null) : null;
                if (is_null($dataset)) {
                    $dataset = $default_settings;
                } else {
                    $customized = true;
                    $dataset = $dataset->toArray();
                }

                $has_categories = $sitemap->category ? true : false;
                $has_pages = $sitemap->sitemapType->sitemap_type_type == "dynamic" ? true : false;

                $detail_cf = ( ! is_null($dataset["detail_change_frequency"]) && $dataset["detail_change_frequency"]!=$default_settings["detail_change_frequency"]) ? $dataset["detail_change_frequency"] : $default_settings["detail_change_frequency"];
                $categories_cf = ( ! is_null($dataset["categories_change_frequency"]) && $dataset["categories_change_frequency"]!=$default_settings["categories_change_frequency"]) ? $dataset["categories_change_frequency"] : $default_settings["categories_change_frequency"];
                $pages_cf = ( ! is_null($dataset["pages_change_frequency"]) && $dataset["pages_change_frequency"]!=$default_settings["pages_change_frequency"]) ? $dataset["pages_change_frequency"] : $default_settings["pages_change_frequency"];


                $fields[] = "$sitemap->id AS sitemap_id";
                $fields[] = "\"".$detail_cf . "\"". " AS detail_change_frequency";
                $fields[] =  "\"".$categories_cf . "\"". " AS categories_change_frequency";
                $fields[] =  "\"".$pages_cf . "\"". " AS pages_change_frequency";

                $fields = "SELECT " . implode(", ", $fields);
                $rows[] = $fields;
            }

            return "(" . implode(" UNION ", $rows) . ") AS change_frequencies";


        }


        private function getPagesQuery($active_alternates = true)
        {

            if (!count($this->sitemaps_to_index_pages_of)) {
                return "";
            }

            $alternates_query = $active_alternates && count($this->other_languages) ? $this->getPagesAlternatesQuery() : "(SELECT NULL)";
            $sitemaps = "(" . implode(",", $this->sitemaps_to_index_pages_of) . ")";
            return <<<"EOT"
SELECT
    urls.model_type,
    pd.id as model_id,
    lng.code AS lng_code,
    pd.page_id as parent_id,
    urls.id AS url_id,
    urls.url,
    pages.sitemap_id,
    priorities.pages_priority as priority,
    change_frequencies.pages_change_frequency as change_frequency,
    $alternates_query AS alternates
FROM page_details pd
         LEFT JOIN pages ON
        pages.id = pd.page_id
         LEFT JOIN urls ON
            urls.model_type="Mediapress\\\Modules\\\Content\\\Models\\\PageDetail"
        AND urls.model_id=pd.id
        AND urls.type <> "redirect"
        AND urls.type <> "reserved"
        AND urls.deleted_at IS NULL
         LEFT JOIN metas ON metas.url_id=urls.id
        LEFT JOIN languages lng ON pd.language_id=lng.id
        LEFT JOIN $this->priorities_table ON priorities.sitemap_id = pages.sitemap_id
        LEFT JOIN $this->change_frequencies_table ON change_frequencies.sitemap_id = pages.sitemap_id
WHERE
        pd.country_group_id=$this->cg_id
        AND pd.language_id=$this->lng_id
        AND pd.deleted_at IS NULL
        AND url_id IS NOT NULL
        AND pages.status=1
        AND pages.deleted_at IS NULL
        AND pages.sitemap_id IN $sitemaps
EOT;

        }

        private function getPagesAlternatesQuery()
        {
            $query = "";
            if (count($this->other_languages)) {
                $other_lngs = implode(",", $this->other_languages);
                // AQ => [A]lternates [Q]uery
                $query = <<<"AQ"
                (
                    SELECT  GROUP_CONCAT( DISTINCT CONCAT(lng2.code,":" ,urls2.url) ORDER BY pd2.id SEPARATOR ";")
                    FROM page_details pd2
                    LEFT JOIN urls urls2 ON urls2.model_type="Mediapress\\\Modules\\\Content\\\Models\\\PageDetail" AND urls2.model_id = pd2.id
                    LEFT JOIN languages lng2 ON lng2.id = pd2.language_id
                    WHERE pd2.language_id IN ($other_lngs)
                      AND pd2.country_group_id=pd.country_group_id
                      AND pd2.page_id=pd.page_id
                )
AQ;
            }

            return $query;
        }


        private function getCategoriesQuery($active_alternates = true)
        {

            if (!count($this->sitemaps_to_index_categories_of)) {
                return "";
            }
            $alternates_query = $active_alternates && count($this->other_languages) ? $this->getCategoriesAlternatesQuery() : "(SELECT NULL)";
            $sitemaps = "(" . implode(",", $this->sitemaps_to_index_categories_of) . ")";
            return <<<"EOT"
SELECT
    urls.model_type,
    cd.id as model_id,
    lng.code AS lng_code,
    cd.category_id as parent_id,
    urls.id AS url_id,
    urls.url,
    categories.sitemap_id,
    priorities.categories_priority as priority,
    change_frequencies.categories_change_frequency as change_frequency,
    $alternates_query AS alternates
FROM category_details cd
         LEFT JOIN categories ON
        categories.id = cd.category_id
         LEFT JOIN urls ON
            urls.model_type="Mediapress\\\Modules\\\Content\\\Models\\\CategoryDetail"
        AND urls.model_id=cd.id
        AND urls.type <> "redirect"
        AND urls.type <> "reserved"
        AND urls.deleted_at IS NULL
         LEFT JOIN metas ON metas.url_id=urls.id
        LEFT JOIN languages lng ON cd.language_id=lng.id
        LEFT JOIN $this->priorities_table ON priorities.sitemap_id = categories.sitemap_id
        LEFT JOIN $this->change_frequencies_table ON change_frequencies.sitemap_id = categories.sitemap_id

WHERE
        cd.country_group_id=$this->cg_id
        AND cd.language_id=$this->lng_id
        AND cd.deleted_at IS NULL
        AND url_id IS NOT NULL
        AND categories.status=1
        AND categories.deleted_at IS NULL
        AND categories.sitemap_id IN $sitemaps
EOT;
        }


        private function getCategoriesAlternatesQuery()
        {
            $query = "";
            if (count($this->other_languages)) {
                $other_lngs = implode(",", $this->other_languages);
                $query = <<<"AQ"
(SELECT
            GROUP_CONCAT( DISTINCT CONCAT(lng2.code,":" ,urls2.url) ORDER BY cd2.id SEPARATOR ";")
    FROM category_details cd2
        LEFT JOIN urls urls2 ON urls2.model_type ="Mediapress\\\Modules\\\Content\\\Models\\\CategoryDetail" AND urls2.model_id = cd2.id
        LEFT JOIN languages lng2 ON lng2.id = cd2.language_id
        WHERE cd2.language_id IN ($other_lngs)
            AND cd2.country_group_id = cd.country_group_id
            AND cd2.category_id = cd.category_id
    )
AQ;
            }

            return $query;
        }

        private function getDetailQuery($active_alternates = true)
        {

            if (!count($this->sitemaps_to_index_detail_of)) {
                return "";
            }
            $alternates_query = $active_alternates && count($this->other_languages) ? $this->getDetailAlternatesQuery() : "(SELECT NULL)";
            $sitemaps = "(" . implode(",", $this->sitemaps_to_index_detail_of) . ")";
            return <<<"EOT"
SELECT
    urls.model_type,
    sd.id as model_id,
    lng.code AS lng_code,
    sd.sitemap_id as parent_id,
    urls.id AS url_id,
    urls.url,
    sd.sitemap_id,
    priorities.detail_priority as priority,
    change_frequencies.detail_change_frequency as change_frequency,
    $alternates_query AS alternates
FROM sitemap_details sd
         LEFT JOIN sitemaps ON
        sitemaps.id = sd.sitemap_id
         LEFT JOIN urls ON
            urls.model_type="Mediapress\\\Modules\\\Content\\\Models\\\SitemapDetail"
        AND urls.model_id=sd.id
        AND urls.type <> "redirect"
        AND urls.type <> "reserved"
        AND urls.deleted_at IS NULL
         LEFT JOIN metas ON metas.url_id=urls.id
        LEFT JOIN languages lng ON sd.language_id=lng.id
        LEFT JOIN $this->priorities_table ON priorities.sitemap_id = sd.sitemap_id
        LEFT JOIN $this->change_frequencies_table ON change_frequencies.sitemap_id = sd.sitemap_id

WHERE
        sd.country_group_id=$this->cg_id
        AND sd.language_id=$this->lng_id
        AND sd.deleted_at IS NULL
        AND url_id IS NOT NULL
        AND sitemaps.status=1
        AND sitemaps.deleted_at IS NULL
        AND sd.sitemap_id IN $sitemaps
EOT;
        }

        private function getDetailAlternatesQuery()
        {
            $query = "";
            if (count($this->other_languages)) {
                $other_lngs = implode(",", $this->other_languages);
                $query = <<<"AQ"
(SELECT
            GROUP_CONCAT( DISTINCT CONCAT(lng2.code,":" ,urls2.url) ORDER BY sd2.id SEPARATOR ";")
    FROM sitemap_details sd2
        LEFT JOIN urls urls2 ON urls2.model_type ="Mediapress\\\Modules\\\Content\\\Models\\\SitemapDetail" AND urls2.model_id = sd2.id
        LEFT JOIN languages lng2 ON lng2.id = sd2.language_id
        WHERE sd2.language_id IN ($other_lngs)
            AND sd2.country_group_id = sd.country_group_id
            AND sd2.sitemap_id = sd.sitemap_id
    )
AQ;
            }

            return $query;
        }


    }
