<?php

namespace Mediapress\Facades\Slots;

use Illuminate\Support\Facades\Facade;

class BaseSlot extends Facade
{

    public static function __callStatic($method, $args)
    {
        $path = storage_path('app/slots.json');
        if(!file_exists($path)){
            file_put_contents($path,'{}');
        }
        $slots = json_decode(file_get_contents($path), 1);
        $fallback_slots = json_decode(file_get_contents(__DIR__.'/../../Storage/slots.json'), 1);
        $className = class_basename(get_called_class());
        if (isset($slots[$className]) && isset($slots[$className][$method])) {
            $slotData = $slots[$className][$method];
            if (method_exists($slotData['class'], $slotData['method'])) {
                return self::callFunc($slotData, $args);
            }
        }

        if (isset($fallback_slots[$className]) && isset($fallback_slots[$className][$method])) {
            $slotData = $fallback_slots[$className][$method];
            if (method_exists($slotData['class'], $slotData['method'])) {
                return self::callFunc($slotData, $args);
            }
            throw new \Exception('Fallback method is not Found');
        }
        throw new \Exception('Slot is not Found');
    }

    public static function callFunc($methodArray, $args)
    {
        if (isset($methodArray['type']) && $methodArray['type'] == 'static') {
            return call_user_func_array(
                array($methodArray['class'], $methodArray['method']),
                $args
            );
        } else {
            return call_user_func_array([new $methodArray['class'](), $methodArray['method']], $args);
        }
    }

}
