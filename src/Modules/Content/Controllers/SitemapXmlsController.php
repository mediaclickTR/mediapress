<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Modules\Content\Requests\SitemapXmlStore;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mediapress\Facades\DataSourceEngine;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Requests\SitemapXmlPost;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\Content\Models\SitemapXml;
use Mediapress\Modules\Content\Models\SitemapXmlBlock;

class SitemapXmlsController extends Controller
{

    /*private $rules = [
        'website_id' => 'required',
        'name' => 'required',
        'type' => 'required',
        'text' => 'required',
        'button' => 'required',

    ];

    private $labels = [
        'website_id' => 'Web Sitesi',
        'name' => 'Slider adı',
        'type' => 'Slider tipi',
        'text' => 'Metin Sayısı',
        'button' => 'Buton Sayısı',
    ];*/

    #region Actions

    public const WEBSITE_ID = "website_id";
    public const PANEL_WEBSITE_ID = "panel" . ".website.id";
    public const MODEL = 'model';
    public const SITEMAP_XML_ID = "sitemap_xml_id";
    public const SITEMAP_ID = "sitemap_id";
    public const CONTENT = "Content";
    public const DEFAULT_SITEMAP_XML_BLOCK_DATA = "DefaultSitemapXmlBlockData";
    public const HOMEPAGE = "homepage";
    public const OTHER = "other";
    public const BLOCKS = 'blocks';
    public const DEFAULT1 = "default";
    const INCLUDE_DETAIL = 'include_detail';
    const INCLUDE_CATEGORIES = 'include_categories';
    const INCLUDE_PAGES = 'include_pages';
    const DETAIL_PRIORITY = 'detail_priority';
    const CATEGORIES_PRIORITY = 'categories_priority';
    const PAGES_PRIORITY = 'pages_priority';
    const DETAIL_CHANGE_FREQUENCY = 'detail_change_frequency';
    const CATEGORIES_CHANGE_FREQUENCY = 'categories_change_frequency';
    const PAGES_CHANGE_FREQUENCY = 'pages_change_frequency';

    public function index()
    {

        $websiteId = session(self::PANEL_WEBSITE_ID);
        if(!activeUserCan([
            "content.websites.website$websiteId.seo.sitemap_xmls.index",
            "content.websites.seo.sitemap_xmls.index"
        ])){
            return rejectResponse();
        }

        $sitemapxmls = SitemapXML::where("status", "!=", SitemapXML::DRAFT)->where(self::WEBSITE_ID, $websiteId);
        $queries = FilterEngine::filter($sitemapxmls, false)['queries'];
        $sitemapxmls = FilterEngine::filter($sitemapxmls, false)[self::MODEL];


        return view('ContentPanel::seo.sitemap_xmls.index', compact('sitemapxmls', 'queries'));
    }

    public function create(Request $request)
    {

        $websiteId = session(self::PANEL_WEBSITE_ID);
        if(!activeUserCan([
            "content.websites.website$websiteId.seo.sitemap_xmls.create",
            "content.websites.seo.sitemap_xmls.create"
        ])){
            return rejectResponse();
        }

        $recommended_split_size = 120;
        try {
            // detect possible url count for one entry (1 default + n alternate urls)
            $variations = 0;
            $cgs = session("panel.website")->countryGroups()->with("languages")->get();
            foreach ($cgs as $cg) {
                $variations += count($cg->languages);
            }
            // compute how many entries fill projected quota (1200)
            if ($variations) {
                $recommended_split_size = (1200 - (1200 % $variations)) / $variations;
            }
        } catch (FatalErrorException $e) {
            $recommended_split_size = 120;
        }

        $has_default = SitemapXml::where("type", self::DEFAULT1)->where(self::WEBSITE_ID, session(self::PANEL_WEBSITE_ID))->first();


        $model = new SitemapXml();
        $model->type = $has_default ? "custom" : self::DEFAULT1;
        $blocks = collect();
        $blocks_sitemap_ids = [];
        $sitemaps_not_included = [];
        $active_website_id = session()->get(self::PANEL_WEBSITE_ID);
        $sitemaps = Sitemap::
        whereHas('websites', function ($q) {
            $q->where('id', session('panel.website.id'));
        })->status(1)->with("detail")->get();

        if ($model->exists) {
            $blocks = SitemapXmlBlock::where(self::SITEMAP_XML_ID, $model->id)->with("sitemap.detail")->all();
            $blocks_sitemap_ids = $blocks->pluck(self::SITEMAP_ID)->toArray();
        }

        $default_block_template_for_homepage = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, ["sitemap_feature" => self::HOMEPAGE]);
        $default_block_template = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, [""]);
        $block_templates = [
            self::HOMEPAGE => $default_block_template_for_homepage->getData(),
            self::OTHER => $default_block_template->getData()
        ];

        $sitemaps_not_included = array_diff($sitemaps->pluck("id")->toArray(), $blocks_sitemap_ids);

        return view('ContentPanel::seo.sitemap_xmls.edit_create', compact(self::MODEL, self::BLOCKS, 'sitemaps', 'sitemaps_not_included', "block_templates", "recommended_split_size"));
    }



    public function edit($id = null)
    {

        $model = SitemapXml::where("id", $id)->first();

        $websiteId = $model->website_id;
        if(!activeUserCan([
            "content.websites.website$websiteId.seo.sitemap_xmls.update",
            "content.websites.seo.sitemap_xmls.update"
        ])){
            return rejectResponse();
        }

        if (!$model) {
            return redirect()->back()->withErrors(["Sitemap XML Kaydı bulunamadı."]);
        }

        $blocks = collect();
        $blocks_sitemap_ids = [];
        $sitemaps_not_included = [];
        $active_website_id = session()->get(self::PANEL_WEBSITE_ID);
        $sitemaps = Sitemap::
        whereHas('websites', function ($q) {
            $q->where('id', session('panel.website.id'));
        })->where('status',1)->with("detail")->get();

        $blocks = SitemapXmlBlock::where(self::SITEMAP_XML_ID, $model->id)->with("sitemap.detail")->get();
        $blocks_sitemap_ids = $blocks->pluck(self::SITEMAP_ID)->toArray();

        $default_block_template_for_homepage = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, ["sitemap_feature" => self::HOMEPAGE]);
        $default_block_template = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, [""]);
        $block_templates = [
            self::HOMEPAGE => $default_block_template_for_homepage->getData(),
            self::OTHER => $default_block_template->getData()
        ];

        $sitemaps_not_included = array_diff($sitemaps->pluck("id")->toArray(), $blocks_sitemap_ids);

        return view('ContentPanel::seo.sitemap_xmls.edit_create', compact(self::MODEL, self::BLOCKS, 'sitemaps', 'sitemaps_not_included', "block_templates"));
    }

    public function store(SitemapXmlStore $request, $id = null)
    {

        // pre-assign existence flag to false
        $exists = false;

        //regulate parameter
        $sitemap_xml_id = $request->id ?? null;

        if ($sitemap_xml_id) {

            //whilst we have a param, this object should be existing, so it will get updated
            /** @var SitemapXML $sitemap_xml_model */
            $sitemap_xml_model = SitemapXml::where("id", $sitemap_xml_id)->with(self::BLOCKS)->first();
            if (!$sitemap_xml_model) {
                return redirect()->back()->withErrors(["Belirtilen SitemapXml kaydı bulunamadı. ID:" . $sitemap_xml_id]);
            }
            $websiteId = $sitemap_xml_model->website_id;
            if(!activeUserCan([
                "content.websites.website$websiteId.seo.sitemap_xmls.update",
                "content.websites.seo.sitemap_xmls.update"
            ])){
                return rejectResponse();
            }
            //made sure model exists, set existence flag to true
            $exists = true;
        } else {
            // no identifier param means model will be created.
            $sitemap_xml_model = new SitemapXml();
        }

        //check if there is a default SitemapXML model for this website.
        $has_default = SitemapXml::where("type", self::DEFAULT1)->where(self::WEBSITE_ID, session(self::PANEL_WEBSITE_ID))->first();
        $sitemap_xml_model->website_id = $websiteId = $has_default ? $request->website_id : session(self::PANEL_WEBSITE_ID);

        if(!activeUserCan([
            "content.websites.website$websiteId.seo.sitemap_xmls.create",
            "content.websites.seo.sitemap_xmls.create"
        ])){
            return rejectResponse();
        }

        // If there is, all others' type will be custom.
        // Otherwise, there should be one asap.
        // Set type of THIS SITEMAPXML MODEL to CUSTOM or DEFAULT
        $sitemap_xml_model->type = $has_default ? "custom" : self::DEFAULT1;
        // Fill model's attributes
        $sitemap_xml_model->status = 1;
        $sitemap_xml_model->title = $request->title;
        $sitemap_xml_model->filename = $request->filename;

        // Get default values for SitemapXMLBlock models
        $default_settings_for_homepage = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, ["sitemap_feature" => self::HOMEPAGE])->getData();
        $default_settings_for_others = DataSourceEngine::getDataSource(self::CONTENT, self::DEFAULT_SITEMAP_XML_BLOCK_DATA, [""])->getData();

        //return dd($default_settings_for_others);
        // get sitemaps
        //$sitemaps = Sitemap::where(self::WEBSITE_ID, session(self::PANEL_WEBSITE_ID))->get();
        $sitemaps = Sitemap::
        whereHas('websites', function ($q) {
            $q->where('id', session(self::PANEL_WEBSITE_ID));
        })->where("status",1)->with("detail")->get();



        try {
            DB::beginTransaction();

            // save it first and be sure to have an SitemapXML model id
            $sitemap_xml_model->save();

            // if this is an update for an existing SitemapXML Model
            if ($exists) {
                // kill all current SitemapXMLBlocks which belongs to this model,
                SitemapXmlBlock::where(self::SITEMAP_XML_ID, $sitemap_xml_id)->delete();
            }

            //will check each sitemap for customized values
            //dump($request->all(),655);

            foreach ($sitemaps as $sm) {
                //dump($sm); continue;

                $default_settings = $sm->feature_tag == "homepage" ? $default_settings_for_homepage : $default_settings_for_others;


                /** @var array $sitemap_xml_block_data Will be used to create a new SitemapXmlBlock model */
                $sitemap_xml_block_data = [
                    self::INCLUDE_DETAIL => null,
                    self::INCLUDE_CATEGORIES => null,
                    self::INCLUDE_PAGES => null,
                    self::DETAIL_PRIORITY => null,
                    self::CATEGORIES_PRIORITY => null,
                    self::PAGES_PRIORITY => null,
                    self::DETAIL_CHANGE_FREQUENCY => null,
                    self::CATEGORIES_CHANGE_FREQUENCY => null,
                    self::PAGES_CHANGE_FREQUENCY => null,
                ];

                $req_include_detail = $request->has('include_detail') ? $request->include_detail : [];
                $req_detail_priority = $request->has('detail_priority') ? $request->detail_priority : [];
                $req_detail_change_frequency = $request->has('detail_change_frequency') ? $request->detail_change_frequency : [];

                $req_include_categories = $request->has('include_categories') ? $request->include_categories : [];
                $req_categories_priority = $request->has('categories_priority') ? $request->categories_priority : [];
                $req_categories_change_frequency = $request->has('categories_change_frequency') ? $request->categories_change_frequency : [];

                $req_include_pages = $request->has('include_pages') ? $request->include_pages : [];
                $req_pages_priority = $request->has('pages_priority') ? $request->pages_priority : [];
                $req_pages_change_frequency = $request->has('pages_change_frequency') ? $request->pages_change_frequency : [];



                // assign requested values to the data array if
                // parameters exists for this sitemap
                if(array_key_exists($sm->id, $req_include_detail) ){
                    $include_detail = boolval($req_include_detail[$sm->id]);
                    $sitemap_xml_block_data[self::INCLUDE_DETAIL] = $include_detail;
                }

                if(array_key_exists($sm->id, $req_detail_priority) && $req_detail_priority[$sm->id]){
                    $detail_priority = $req_detail_priority[$sm->id]+0;
                    if($detail_priority != $default_settings[self::DETAIL_PRIORITY]){
                        $sitemap_xml_block_data[self::DETAIL_PRIORITY] = $detail_priority;
                    }
                }
                if(array_key_exists($sm->id, $req_detail_change_frequency) && $req_detail_change_frequency[$sm->id]){
                    $detail_change_frequency = $req_detail_change_frequency[$sm->id];
                    if($detail_change_frequency != $default_settings[self::DETAIL_CHANGE_FREQUENCY]){
                        $sitemap_xml_block_data[self::DETAIL_CHANGE_FREQUENCY] = $detail_change_frequency;
                    }
                }

                // if sitemap has no categories, running the block below has no sense
                if ($sm->category) {
                    // assign requested values to the data array if
                    // parameters exists for this sitemap
                    if(array_key_exists($sm->id, $req_include_categories) ){
                        $include_categories = boolval($req_include_categories[$sm->id]);
                        $sitemap_xml_block_data[self::INCLUDE_CATEGORIES] = $include_categories;
                    }
                    if(array_key_exists($sm->id, $req_categories_priority) && $req_categories_priority[$sm->id]){
                        $categories_priority = $req_categories_priority[$sm->id]+0 ;
                        if($categories_priority != $default_settings[self::CATEGORIES_PRIORITY]){
                            $sitemap_xml_block_data[self::CATEGORIES_PRIORITY] = $categories_priority;
                        }
                    }
                    if(array_key_exists($sm->id, $req_categories_change_frequency) && $req_categories_change_frequency[$sm->id]){
                        $categories_change_frequency = $req_categories_change_frequency[$sm->id];
                        if($categories_change_frequency != $default_settings[self::CATEGORIES_CHANGE_FREQUENCY]){
                            $sitemap_xml_block_data[self::CATEGORIES_CHANGE_FREQUENCY] = $categories_change_frequency;
                        }
                    }
                }

                // if sitemap has no pages, running the block below has no sense
                if ($sm->sitemapType->sitemap_type_type=="dynamic") {
                    // assign requested values to the data array if
                    // parameters exists for this sitemap
                    if(array_key_exists($sm->id, $req_include_pages)){
                        $include_pages = boolval($req_include_pages[$sm->id]);
                        $sitemap_xml_block_data[self::INCLUDE_PAGES] = $include_pages;
                    }
                    if(array_key_exists($sm->id, $req_pages_priority) && $req_pages_priority[$sm->id]){
                        $pages_priority = $req_pages_priority[$sm->id]+0;
                        if($pages_priority != $default_settings[self::PAGES_PRIORITY]){
                            $sitemap_xml_block_data[self::PAGES_PRIORITY] = $pages_priority;
                        }
                    }
                    if(array_key_exists($sm->id, $req_pages_change_frequency) && $req_pages_change_frequency[$sm->id]){
                        $pages_change_frequency = $req_pages_change_frequency[$sm->id];
                        if($pages_change_frequency != $default_settings[self::PAGES_CHANGE_FREQUENCY]){
                            $sitemap_xml_block_data[self::PAGES_CHANGE_FREQUENCY] = $pages_change_frequency;
                        }
                    }
                }

                // if every value in data array is null still, this means there is no customization.
                // thus, no future process needed.
                if(!count(array_filter($sitemap_xml_block_data, function($var){return !is_null($var);}))){
                    //dump("Will continue");
                    continue;
                }


                // last and critical parameters for data array
                $sitemap_xml_block_data[self::SITEMAP_XML_ID] = $sitemap_xml_model->id;
                $sitemap_xml_block_data[self::SITEMAP_ID] = $sm->id;


                // save the created model.
                $block_model = new SitemapXmlBlock($sitemap_xml_block_data);
                $block_model->save();

            }
            /*DB::rollBack();
            return dd("bitti");*/
            DB::commit();
        } catch (\Exception $e) {
            //dump("Buraya Düştü");
            DB::rollBack();
            $trace = $e->getTrace();
            foreach ($trace as $trace_) {
                if (isset($trace_["file"]) && $trace_["file"] == __FILE__) {
                    dump("Hata " . __FILE__ . " dosyasında " . __LINE__ . ". satırda tetiklendi.");
                    break;
                }
            }
            dump("Exception " . __FILE__ . " dosyasında " . (__LINE__ + 1) . ". satırda fırlatıldı.");
            throw $e;
        }

        //return dd("Bitti");

        return redirect(route("Content.seo.sitemap_xmls.edit", $sitemap_xml_model->id))->with("message", trans('MPCorePanel::general.success_message'));

    }

    public function enable()
    {

    }

    public function disable()
    {

    }

    public function delete()
    {

        $websiteId = session(self::PANEL_WEBSITE_ID);
        if(!activeUserCan([
            "content.websites.website$websiteId.seo.sitemap_xmls.delete",
            "content.websites.seo.sitemap_xmls.delete"
        ])){
            return rejectResponse();
        }

    }

    #endregion

    #region Helpers
    function getDefaultBlockArray()
    {

    }
    #endregion
}
