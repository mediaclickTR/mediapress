<?php

namespace Mediapress\Modules\Auth;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\Auth\Middleware\RedirectIfAuthenticated;
use Mediapress\Modules\Auth\Models\Role;
use Silber\Bouncer\Database\Models;

class AuthServiceProvider extends ServiceProvider
{
    public const CONFIG = 'Config';
    public const ACTIONS_PHP = 'actions.php';
    /**
     * The policy mappings for the application.
     *
     * @var array
     */

    protected $module_name = "Auth";
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];
    protected $namespace= 'Mediapress\Modules\Auth';

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {


        $this->registerPolicies();
        $this->map();
        $this->publishActions(__DIR__);
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->loadMigrationsFrom(__DIR__.DIRECTORY_SEPARATOR . 'Database'. DIRECTORY_SEPARATOR . 'migrations');

        Models::setRolesModel(Role::class);
    }

    protected function publishActions($dir){
        $actions_config = $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP;
        if(is_file($actions_config)){
            $this->publishes([
                $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP => config_path(strtolower($this->module_name).'_module_actions.php')
            ]);
//        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name . 'Database');
            $this->mergeConfigFrom($dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR .strtolower($this->module_name).'_module_actions.php', strtolower($this->module_name).'_module_actions');
        }
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('Auth', \Mediapress\Modules\Auth\Facades\Auth::class);
        app()->bind('Auth', function () {
            return new \Mediapress\Modules\Auth\Auth;
        });

        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
        /** @var Router $router */
        $router = $this->app['router'];
        $router->aliasMiddleware('panel.guest', RedirectIfAuthenticated::class);

    }


    private function getConfigBasename($file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    protected function mergeConfig($path, $key)
    {
        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }


    public function map()
    {
        $this->mapPanelRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace . '\Controllers',
        ], function ($router) {

            $web_routes=__DIR__.DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'WebRoutes.php';
            if(is_file($web_routes)){
                include_once $web_routes;
            }
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapPanelRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace . '\Controllers',
            'prefix' => 'mp-admin',
        ], function ($router) {

            $adm_routes=__DIR__.DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'PanelRoutes.php';
            if(is_file($adm_routes)){
                include_once $adm_routes;
            }
        });
    }


}
