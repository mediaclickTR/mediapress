@if($data['parts'])
    <div class="lang-box">
        <form action="javascript:void(0);">
            <div class="item">
                <div class="row">
                    <div class="col-4">
                        <span>{!! __("MPCorePanel::translate.variable") !!}</span>
                    </div>
                    <div class="col-6">
                        <span>{!! __("MPCorePanel::translate.default_lang_value") !!}</span>
                    </div>
                    <div class="col-2">
                        <span>{!! __("MPCorePanel::translate.detail") !!}</span>
                    </div>
                </div>
            </div>

            @foreach($data['parts'] as $key=>$part)
                <div class="item" id="{{ $key }}">
                    <div class="row">
                        <div class="col-4">
                            <span>{{ $key }}</span>
                        </div>
                        <div class="col-6">
                            <span>
                                @php
                                    $defaultCountryGroupId = $variations[$website->id]['default_country_group_id'];
                                    $defaultLanguageId = $variations[$website->id]['default_language'] ?? "";
                                @endphp
                                {!!
                                    $part[session('panel.active_language.id')][$defaultCountryGroupId] ?? ($part[$defaultLanguageId][$defaultCountryGroupId] ?? "")
                                !!}
                            </span>
                        </div>
                        <div class="col-2">
                            <button>{!!  __("MPCorePanel::translate.view") !!}</button>
                        </div>
                        <div class="col-12 edit lang_area">
                            <div class="row">
                                @if(isset($variations[$website->id]['country_groups']))

                                    <div class="col-11">
                                        <span class="head">{!! __("MPCorePanel::translate.languages") !!}</span>
                                    </div>
                                    <div class="col-1 head">
                                        <a href="{{ route("MPCore.translate.delete", ['key' => $key]) }}" class="btn btn-sm btn-danger d-block">{!! __("MPCorePanel::general.delete") !!}</a>
                                    </div>
                                    <!-- Country group variations -->
                                    @foreach($variations[$website->id]['country_groups'] as $group)
                                        @foreach($group['languages'] as $language)
                                            <div class="col-6">
                                                <div class="{{ $language['code'] }}_{{ $group['code'] }}">
                                                    <div class="form-group">
                                                        <div class="language-header">
                                                            <div class="language-flag">
                                                                <img src="{{ getFlag($language['code']) }}" alt="{!! $language['code'] !!}">
                                                            </div>
                                                            <div class="language-title">
                                                                {!! $language['name'] !!} @if($group['code'] != "gl") / {!! $group['title'] !!} ({!! strtoupper($group['code']) !!}) @endif
                                                            </div>
                                                        </div>
                                                        {!! Form::text('key['.$key.']['.$language['id'].']['.$group['id'].']',((isset($part[$language['id']][$group['id']])) ? $part[$language['id']][$group['id']] : null),["class"=>"lang_part"]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </form>
    </div>
@else
    <div class="clearfix"></div>
    <div class="alert alert-warning">{!! __("MPCorePanel::translate.no_data",['website'=>$website->name]) !!}</div>
@endif
