@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("ContentPanel::website.create_website") !!}</h1>
        <div class="col-md-12 pa">
            @include("ContentPanel::websites.form")
        </div>
    </div>
@endsection
