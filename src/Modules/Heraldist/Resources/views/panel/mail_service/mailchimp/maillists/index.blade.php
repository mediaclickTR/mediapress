@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.mailchimp_maillists") !!} ({!! $total !!})</h1>
            @if($total==0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                <div class="float-right">
                    <a href="{!! route('Heraldist.mailchimp.maillists.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
                </div>
            @else
            <table>
                <thead>
                <tr>
                    <th>{!! trans("HeraldistPanel::mail_service.mailchimp_listid") !!}</th>
                    <th>{!! trans("HeraldistPanel::mail_service.maillist_name") !!}</th>
                    <th>{!! trans("HeraldistPanel::mail_service.sender_name") !!}</th>
                    <th>{!! trans("HeraldistPanel::mail_service.sender_email") !!}</th>
                    <th>{!! trans("MPCorePanel::general.subject") !!}</th>
                    <th>{!! trans("MPCorePanel::general.users") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($maillists as $maillist)
                <tr>
                    <td>{!! $maillist->id !!}</td>
                    <td><a class="{!! route('Heraldist.mailchimp.members.index',$maillist->id) !!}" href="">{!! $maillist->name !!}</a></td>
                    <td>{!! $maillist->campaign_defaults->from_name !!}</td>
                    <td>{!! $maillist->campaign_defaults->from_email !!}</td>
                    <td>{!! $maillist->campaign_defaults->subject; !!}</td>
                    <td>
                        @if($maillist->stats->member_count==0)
                            <a href="{!! route('Heraldist.mailchimp.members.index',$maillist->id) !!}" class="text-success"><i class="fa fa-plus" style="display:inline"></i> Üye Ekle</a>
                        @else
                        <a href="{!! route('Heraldist.mailchimp.members.index',$maillist->id) !!}" class="text-info"><i class="fa fa-users" style="display:inline;"></i> {!! $maillist->stats->member_count !!}</a>
                        @endif
                    </td>
                    <td>
                        <select onchange="locationOnChange(this.value);">
                            <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                            <option value="{!! route('Heraldist.mailchimp.members.index',$maillist->id) !!}">{!! trans("MPCorePanel::general.users") !!}</option>
                        </select>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        @endif
@endsection
