<?php

return [
    "general"=>[
        "shared_abilities"=>"Ortak Yetkiler",
        "recorded_items"=>"Kayıtlı Öğeler",
    ],
    "sections"=>[
        "userlogs"=>"Kullanıcı Eylemleri",
        "translations"=>"Çeviriler",
        "modules"=>"Modüller",
        "module"=>"Modül",
    ],
    "actions"=>[
        "view"=>"Gör / Aç",
        "list"=>"Listele",
        "create"=>"Oluştur",
        "update"=>"Güncelle",
        "delete"=>"Sil",
        "reorder"=>"Sırala",
        "import"=>"İçe Aktar",
        "export"=>"Dışa Aktar",
        "auth"=>"Yetkilendir",
        "export2excel"=>"Excel'e Aktar",
        "update_level1_settings"=>"1. Düzey Ayarları Değiştir",
        "update_level2_settings"=>"2. Düzey Ayarları Değiştir",
        "update_level3_settings"=>"3. Düzey Ayarları Değiştir",
    ]
];
