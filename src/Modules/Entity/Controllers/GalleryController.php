<?php

namespace Mediapress\Modules\Entity\Controllers;


use Carbon\Carbon;
use Html;
use http\Env\Response;
use Mediapress\Foundation\MPCache;
use Mediapress\Models\Gallery;
use Mediapress\Models\PageGallery;
use Mediapress\Models\PageGalleryImage;
use Mediapress\Models\Url;
use Mediapress\Models\Image;
use Yajra\Datatables\Facades\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Models\Menu;
use Mediapress\Models\MenuDetail;
use Mediapress\Models\LanguageWebsite;
use File;
use Validator;
use Illuminate\Support\Facades\Storage;
use DB;
use Mediapress\Http\Controllers\Controller;


class GalleryController extends Controller
{
    #region crud
    public const PHOTOGALLERY_DETAIL_INDEX = 'photogallery.detail.index';
    public const ACCESSDENIED = 'accessdenied';
    public const TITLE = 'title';
    public const DATA = 'data';
    public const FOOTER = 'footer';
    public const PAGE_GALLERY_DETAILS_NAME = 'page_gallery_details.name';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const LANGUAGES = "languages";
    public const PHOTOGALLERY_DETAIL_CREATE = 'photogallery.detail.create';
    public const GALLERY = "gallery";
    public const DEFAULT1 = "default";
    public const PARAMETERS = "parameters";
    public const PANEL_GENERAL_AJAX_ACCESS_ERROR = "panel::general.ajax.access_error";
    public const PANEL_GENERAL_AJAX_CREATE_SUCCESS = "panel::general.ajax.create_success";
    public const PANEL_GENERAL_AJAX_CREATE_ERROR = "panel::general.ajax.create_error";
    public const PAGE_GALLERIES_ID = 'page_galleries.id';
    public const TITLES = 'titles';
    public const ID = 'id';
    public const SRC = 'src';
    public const URL = 'url';
    public const STATUS = 'status';
    public const IMAGE_ID = 'image_id';
    public const PAGE_GALLERY_ID = 'page_gallery_id';
    public const EXTRAS = 'extras';
    public const NAME = "name";
    public const IMAGES = 'images';
    public const IMG_TITLE = 'img_title';

    public function index(Builder $builder)
    {
        if(!userAction(self::PHOTOGALLERY_DETAIL_INDEX,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $dataTable = $this->columns($builder)->ajax(route('admin.gallery.ajax'));

        return $this->view("pages.galleries.index", compact("sitemap", 'dataTable'));
    }


    public function columns(Builder $builder)
    {
        return $builder->columns([

            [
                self::DATA => self::ID, self::NAME => self::ID, self::TITLE => 'ID', self::FOOTER => 'ID',
            ],
            [
                self::DATA => self::PAGE_GALLERY_DETAILS_NAME, self::NAME => self::PAGE_GALLERY_DETAILS_NAME, self::TITLE => trans("panel::general.name"), self::FOOTER => trans("panel::general.name")
            ], [
                self::DATA => self::CREATED_AT, self::NAME => self::CREATED_AT, self::TITLE => trans("panel::general.created_at"), self::FOOTER => trans("panel::general.created_at")
            ], [
                self::DATA => self::UPDATED_AT, self::NAME => self::UPDATED_AT, self::TITLE => trans("panel::general.updated_at"), self::FOOTER => trans("panel::general.updated_at")
            ]
        ])->addAction([self::TITLE => trans("panel::general.transactions")]);
    }

    public function ajax()
    {
        $galleries = PageGallery::join('page_gallery_details', self::PAGE_GALLERIES_ID,'page_gallery_details.page_gallery_id')->select(self::PAGE_GALLERIES_ID, self::PAGE_GALLERY_DETAILS_NAME,'page_galleries.created_at','page_galleries.updated_at')->groupBy(self::PAGE_GALLERIES_ID);
        Carbon::setLocale('tr');

        return Datatables::eloquent($galleries)->addColumn('action', function ($gallery) {
            return Html::link(route("admin.gallery.edit", $gallery->id), '<i class="fa fa-pencil-square-o"></i>', ["style" => "margin-left:5px", "class" => "btn btn-primary", "tit"."le" => trans("panel::general.btnEditTitle")], false, false) .
                Html::link(route("admin.gallery.images", $gallery->id), '<i class="fa fa-file-image-o"></i> Resimler', ["style" => "margin-left:5px", "class" => "btn btn-primary", "title" => 'Gallery Resim Ekle'], false, false) . '
                <button name="delete" onclick="rowDelete(' . $gallery->id . ')" class="btn btn-danger" title="' . trans("panel::general.btnDeleteTitle") . '"><i class="fa fa-trash-o"></i></button>';
        })->editColumn(self::PAGE_GALLERY_DETAILS_NAME, function (PageGallery $gallery) {
            return $gallery->detail->name;
        })
            ->editColumn(self::CREATED_AT, function (PageGallery $gallery) {
                return $gallery->created_at->diffForHumans();
            })->editColumn(self::UPDATED_AT, function (PageGallery $gallery) {
                return $gallery->updated_at->diffForHumans();
            })->setRowId('tbl-{{$id}}')->make(true);
    }

    public function formCreate(FormBuilder $formBuilder, $id = 0)
    {


        $languages = $this->getWebsiteSession()->languages->pluck("code", self::ID);

        if ($id) {
            if(!userAction('photogallery.detail.edit_update',true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.edit-title", [self::NAME => 'Galeri']);
            $button = trans("panel::general.edit");

            $data = [
                "find" => [
                    self::ID, "=", $id
                ], self::LANGUAGES => $languages
            ];
        } else {
            if(!userAction(self::PHOTOGALLERY_DETAIL_CREATE,true,false)){
                return redirect()->to(url(route(self::ACCESSDENIED)));
            }
            $title = trans("panel::general.create-title", [self::NAME => 'Galeri']);
            $button = trans("panel::general.save");

            $data = [self::LANGUAGES => $languages];
        }

        $form = PanelFormBuilder::create(self::GALLERY, self::DEFAULT1, $formBuilder, [], $data);

        return $this->view("pages.galleries.form", compact(self::ID, self::LANGUAGES, "form", self::TITLE, "button"));
    }

    public function save(FormBuilder $formBuilder, Collection $data)
    {


        $id = $data->pull("parameters.id");

        $gallery = PageGallery::with('detail')->find($id);

        if ($gallery) {
            if(!userAction('photogallery.detail.edit_update',true,false)){
                return [
                    false, trans(self::PANEL_GENERAL_AJAX_ACCESS_ERROR)
                ];
            }
            $gallery = \Arr::first(FormStore::store($formBuilder, self::GALLERY, self::DEFAULT1, [], [
                "find" => [self::ID, "=", $id], self::DATA => $data->except(self::PARAMETERS)->toArray()
            ]));
        } else {
            if(!userAction(self::PHOTOGALLERY_DETAIL_CREATE,true,false)){
                return [
                    false, trans(self::PANEL_GENERAL_AJAX_ACCESS_ERROR)
                ];
            }
            $gallery = \Arr::first(FormStore::store($formBuilder, self::GALLERY, self::DEFAULT1, [], [
                self::DATA => $data->except(self::PARAMETERS)->toArray()
            ]));
        }

        if ($gallery) {


            return [
                true, trans(self::PANEL_GENERAL_AJAX_CREATE_SUCCESS, [self::NAME => $gallery->detail->name])
            ];
        } else{
            return [
                false, trans(self::PANEL_GENERAL_AJAX_CREATE_ERROR, [self::NAME => $gallery->detail->name])
            ];
        }
    }

    public function delete(Collection $data)
    {
        if(!userAction('photogallery.detail.delete',true,false)){
            return [
                false, trans(self::PANEL_GENERAL_AJAX_ACCESS_ERROR)
            ];
        }

        $id = $data->pull("parameters.id");
        if (!$id){
            return [false, trans("panel::sliders.general.not_record")];
        }

        $gallery = PageGallery::with('detail')->find($id);

        if (!$gallery){
            return [false, trans("panel::sliders.general.not_record")];
        }
        if ($gallery->delete()){
            return [true, trans("panel::general.ajax.delete_success", [self::NAME => $gallery->detail->name])];
        }

        return [false, trans("panel::general.ajax.delete_error", [self::NAME => $gallery->detail->name])];
    }

    public function images($id)
    {
        if(!userAction(self::PHOTOGALLERY_DETAIL_INDEX,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $gallery = PageGallery::find($id);
        $languages = LanguageWebsite::orderByDesc('language_website.default')->leftJoin(self::LANGUAGES,'language_id', self::ID)->select('language_id','code','flag','native')->get();
        $flagPath=url("vendor/mediapress/img/flags/");
        return $this->view('pages.galleries.images', compact(self::GALLERY, self::LANGUAGES,'flagPath'));
    }

    public function addImage($data)
    {

        $image = [];
        $image[self::IMAGE_ID] = \Arr::get($data[self::PARAMETERS], self::IMAGE_ID);
        $image[self::PAGE_GALLERY_ID] = \Arr::get($data[self::PARAMETERS], self::PAGE_GALLERY_ID);
        $image[self::EXTRAS] = \Arr::get($data[self::PARAMETERS], self::EXTRAS);
        $model = PageGalleryImage::create($image);
        if ($model){
            return [true, trans(self::PANEL_GENERAL_AJAX_CREATE_SUCCESS, [self::NAME => 'Resim'])];
        }

        return [false, trans(self::PANEL_GENERAL_AJAX_CREATE_ERROR, [self::NAME => 'Resim'])];
    }

    public function quickUpload($id,Request $request)
    {
        if(!userAction(self::PHOTOGALLERY_DETAIL_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $pageGallery = PageGallery::find($id);

        $rules = [
            self::IMAGES => 'required|array|max:5',
            'images.*' => 'mimes:jpg,jpeg,png|max:900'
        ];

        $fields = [
            'images.required' => 'Lütfen yüklenecek resimleri seçiniz.',
            'images.max' => 'Tek seferde en fazla 5 resim yükleyebilirsiniz.',
            'images.*.mimes' => 'Sadece JPG, JPEG ve PNG uzantılı resimler yükleyebilirsiniz.',
            'images.*.max' => 'İzin verilen resim boyutunu aştınız, maksimum 900kb boyutunda resim yükleyebilirsiniz.'
        ];

        $messages = [
            self::IMAGES => 'Galeri resim'
        ];

        $validator = Validator::make($request->all(), $rules, $fields, $messages);
        if ( $validator->fails() ) {
            return response()->json($validator->messages(),422);
        }

        $folder = $id.'-'.\Str::slug($pageGallery->detail->name,"-");
        $directory = 'galleries/photos';

        $gallery = Gallery::where('folder_name','=',$folder)
            ->where('path','=',$directory)
            ->first();

        if(!$gallery){
            Storage::makeDirectory($directory);
            $gallery = Gallery::create([
                self::NAME =>$pageGallery->detail->name,
                'folder_name'=>$folder,
                'disk'=>'Local',
                'path'=>$directory
            ]);
        }

        $image_page_gallery_create_rows = [];

        foreach($request->file(self::IMAGES) as $file)
        {
            $file_name = uniqid().'-'.\Str::slug($file->getClientOriginalName(),"-").'.'.$file->getClientOriginalExtension();
            $file->storeAs($directory.'/'.$folder,$file_name,'public');

            $image = Image::create([
                'gallery_id'=>$gallery->id,
                'file_name'=>$file_name
            ]);

            $image_page_gallery_create_rows[] = [
                self::IMAGE_ID =>$image->id,
                self::PAGE_GALLERY_ID =>$pageGallery->id,
            ];

        }

        DB::table('image_page_gallery')->insert($image_page_gallery_create_rows);

        return response()->json([
            'message'=>'Resimler başarıyla yüklendi, düzenlemenizi yaparak KAYDET butonunu tıklamayı unutmayınız.'
        ], 200);
    }

    public function imageList($id)
    {
        if(!userAction(self::PHOTOGALLERY_DETAIL_INDEX,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $gallery = PageGallery::find($id);
        $imageList = $gallery->imageList;
        $datas = [];
        foreach ($imageList as $image) {
            $model = image($image->image_id);
            if(is_file($model->originalPath)){
                $extras=$image->extras;
                if(file_exists($model->originalPath)){
                    $file = File::mimeType($model->originalPath);
                }
                if($file == 'image/jpeg' || $file == 'image/png' || $file == 'image/gif'){
                    $resize = $model->resize(['size' => '130x130']);
                    $data = [
                        self::ID => $image->id,
                        self::SRC => (isset($resize->url) ? $resize->url : $resize),
                        self::URL => $model->originalUrl,
                        self::STATUS => ($image->status == 1 ? 'E1F8E6' : 'bfbfbf'),
                        self::TITLES =>isset($extras[self::IMG_TITLE])?$extras[self::IMG_TITLE]:[]
                    ];
                }else{
                    $data = [
                        self::ID => $image->id,
                        self::SRC => url('vendor/mediapress/img/icons/pdf-icon.png'),
                        self::URL => $model->url,
                        self::STATUS => ($image->status == 1 ? 'E1F8E6' : 'bfbfbf'),
                        self::TITLES =>isset($extras[self::IMG_TITLE])?$extras[self::IMG_TITLE]:[]
                    ];
                }

                $datas[] = $data;
            }
        }
        return response()->json($datas);
    }

    public function saveGalleryImageList($data)
    {
        if(!userAction(self::PHOTOGALLERY_DETAIL_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $galleryOrder = json_decode(\Arr::get($data[self::PARAMETERS], 'galleryOrder'), 1);
        $galleryDelete = explode(',', rtrim(\Arr::get($data[self::PARAMETERS], 'galleryDelete'), ','));
        $imageTitles = json_decode(\Arr::get($data[self::PARAMETERS], 'imageTitles'),1);
        $order = [];
        $delete =[];
        foreach ($galleryOrder as $list){
            if(isset($list[self::ID]) && $list[self::ID]){
                if(!in_array($list[self::ID],$galleryDelete)){
                    $order[] = $list[self::ID];
                }else{
                    $delete[] = $list[self::ID];
                }
            }
        }
        $i = 1;

        foreach ($order as $or){
            $image = PageGalleryImage::find($or);
            $extras=$image->extras;
            foreach ($imageTitles as $imgid => $imgdata){
                if(isset($imgdata[self::ID]) && $imgdata[self::ID] == $or){
                    $extras[self::IMG_TITLE]=$imgdata[self::TITLES];
                }
            }
            $image->extras=$extras;
            $image->order = $i;
            $image->status = '1';
            $image->save();
            $i++;
        }
        foreach ($delete as $de){
            $image = PageGalleryImage::find($de);
            $image->delete();
        }
        return [true, trans(self::PANEL_GENERAL_AJAX_CREATE_SUCCESS, [self::NAME => 'Sıralama'])];
    }
}
