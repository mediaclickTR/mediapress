<?php

return [
    'previous' => 'Önceki',
    'next' => 'Sonraki',
    'please_try_with_chrome' => 'Lütfen "Chrome" Tarayıcı İle Deneyiniz',
    'likes' => 'Beğenme',
    'comment' => 'Yorum',
    'goto_post' => 'Gönderiye Git',
    'add_to_album' => 'Albüme Ekle'
];
