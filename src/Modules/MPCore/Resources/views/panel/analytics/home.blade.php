@extends("MPCorePanel::inc.app")
@section('content')
    <div class="dashboard">
        <div class="page-top">
            <div class="title">
                {!! trans("MPCorePanel::general.select-analytics-type") !!}
            </div>
        </div>
        <div class="content">
            <div class="step-number">
                <div class="mc-md-6">
                    <div class="item">
                        <div class="form-group">
                            <div class="submit">
                                <a href="{!! route('Interaction.analytics.google.index') !!}"><button>{!! trans("MPCorePanel::general.google-analytics") !!}</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                @if($data['yandex'])
                    <div class="mc-md-6">
                        <div class="item">
                            <div class="submit">
                                <a href="{!! route('Interaction.analytics.yandex.index') !!}"><button>{!! trans("MPCorePanel::general.yandex-analytics") !!}</button></a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
