<?php

namespace Mediapress\Modules\Heraldist\Validators;

use GuzzleHttp\Client;

class Recaptcha
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $client = new Client();

        $secret = $parameters[0] ?? config('mediapress.recaptcha.secret_key');

        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params' =>
                [
                    'secret' => $secret,
                    'response' => $value
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());
        return $body->success;
    }
}
