@csrf
@if(isset($entity))
    <input type="hidden" name="id" value="{!! $entity->id !!}">
@endif
<div class="row">
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::entity.name-entity") !!}</label>
            <input type="text" name="name" value="{{ old('name',  isset($entity->name) ? $entity->name : null) }}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::entity.surname-entity") !!}</label>
            <input type="text" name="surname" value="{{ old('name',  isset($entity->surname) ? $entity->surname : null) }}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::entity.email-entity") !!}</label>
            <input type="text" name="email" value="{{ old('email',  isset($entity->email) ? $entity->email : null) }}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group rel focus">
            <label>{!! trans("EntityPanel::entity.description-entity") !!}</label>
            <input type="text" name="description" value="{{ old('description',  isset($entity->description) ? $entity->description : null) }}">
        </div>
    </div>
</div>

<div class="float-right">
    <button class="btn btn-primary" type="submit">{!! trans("MPCorePanel::general.save") !!}</button>
</div>
