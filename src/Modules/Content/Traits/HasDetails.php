<?php

namespace Mediapress\Modules\Content\Traits;

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use App;

trait HasDetails{


    public function detail()
    {
        $simple_rel = getSimpleRelationData(get_class($this), "detail");

        $detail_class_name = $simple_rel["child_class"];
        $details_table_name = $simple_rel["child_flatname"]."s";

        if (request()->segment(1) == 'mp-admin' || App::runningInConsole()) {

            $languages_ordered = [];
            $languages_order_values = [];
            $language_id = 760;
            if(session("panel.active_language")){
                $languages_ordered[]=session("panel.active_language.id");
            }
            if(session("panel.website") && session("panel.website")->defaultLanguage()) {
                $language_id = session("panel.website")->defaultLanguage()->id;
            }
            $languages_ordered[]=$language_id;

            //if the defaultLanguage of the panel.website is not English,
            // be sure the second preferred language will be assumed as English.
            if($language_id!=760){
                $languages_ordered[]=760;
            }
            // $languages_ordered will always have one value at least.
            $order_by_str = "case";
            $counter=0;
            foreach($languages_ordered as $lo){
                $order_by_str.=" when `$details_table_name`.`language_id`=? then ".++$counter;
                $languages_order_values[]=$lo;
            }
            $order_by_str.=" else ".++$counter." end";

            return $this->hasOne($detail_class_name)->orderByRaw($order_by_str,$languages_order_values);
        } else {

            /** @var Mediapress $mediapress */
            $mediapress = mediapress();

            $activeLanguage = $mediapress->activeLanguage();
            $activeCountryGroup = $mediapress->activeCountryGroup();
            //dump($activeCountryGroup);

            if ($activeLanguage instanceof Language) {
                $language_id = $activeLanguage->id;
            } else {
                $language_id = $activeLanguage->first()->id;
            }

            if ($activeCountryGroup instanceof CountryGroup) {
                $cg_id = $activeCountryGroup->id;
            } else {
                $cg_id = $activeCountryGroup->first()->id;
            }

            return $this->hasOne($detail_class_name)->where('language_id', $language_id)->where('country_group_id', $cg_id);
        }
    }
}
