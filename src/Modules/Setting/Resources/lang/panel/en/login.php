<?php

return [
    'modal_title' => "Login Settings",
    'google2fa_title' => "Google2fa Settings",
    'login_title' => "Login Restriction Settings",
];
