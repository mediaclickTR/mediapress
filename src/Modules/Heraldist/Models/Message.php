<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Modules\Interaction\Models\Visit;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    public const VISIT_ID = 'visit_id';
    public const ARRAY = 'array';
    protected $table = 'messages';

    public $timestamps = true;

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'subject',
        'email',
        'phone',
        'ip',
        'data',
        'read',
        'file',
        'deleted',
        'ctex_1',
        'ctex_2',
        'cvar_1',
        'cvar_2',
        'cvar_3',
        'cvar_4',
        'cvar_5',
        'cint_1',
        'cint_2',
        'cint_3',
        'cint_4',
        'cint_5',
        'cdec_1',
        'cdec_2',
        'cdat_1',
        'cdat_2',
        'source',
        'utm',
        self::VISIT_ID,
        'agent',
        'track_id'
    ];
    protected $casts = ['data' => self::ARRAY, 'utm' => self::ARRAY, 'agent' => self::ARRAY];

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }


    public function getFirstAttribute()
    {
        $data = collect($this->data)->first();

        return $data['value'];
    }

    public function getLastVisitAttribute()
    {
        $visit = Visit::find($this->attributes[self::VISIT_ID]);
        $list = [];
        if ($visit) {
            $cookie = $visit->cookie;

            if($cookie) {
                $lastVisits = $cookie->visits()->where('id', '<', $this->attributes[self::VISIT_ID])
                    ->orderBy('created_at', 'desc')
                    ->select('url_id')
                    ->groupBy('url_id')
                    ->take(12)
                    ->get();

                foreach ($lastVisits as $data) {
                    $url = Url::find($data->url_id);
                    if(is_null($url)) {
                        continue;
                    }
                    $list[] = url($url->url);
                }
            }
        }
        return $list;
    }
}
