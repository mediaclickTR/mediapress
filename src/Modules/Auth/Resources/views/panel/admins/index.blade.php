@extends('AuthPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("AuthPanel::admin.admins") !!}</div>
            </div>
            <div class="float-right">
                <a class="btn btn-primary btn-sm" href="{!! route('Auth.admins.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
        </div>
            <div class="p-30">
                <div class="table-field">
                    {!! $dataTable->table() !!}
                </div>
            </div>
    </div>
@endsection
