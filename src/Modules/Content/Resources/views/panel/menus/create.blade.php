@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! __("ContentPanel::menu.create_menu") !!}
                </div>
            </div>
        </div>
    <div class="p-30">
        @include("ContentPanel::menus.form")
    </div>
@endsection
