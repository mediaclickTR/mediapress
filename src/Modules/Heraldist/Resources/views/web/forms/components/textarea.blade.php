@if(isset($field))
    <div class="form-group">
        @include('HeraldistWeb::forms.components.label',['field'=>$field])
        <{!! $field['tag'] !!} {!! $field->attributes(['class'=>'form-control']) !!} id
        ="{!! $field['id'] !!}" >{!! old($field->name,$field['content']) !!}</{!! $field['tag'] !!}>
    </div>
@endif
