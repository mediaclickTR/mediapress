<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    public const PAGES = 'pages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable(self::PAGES)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::PAGES, function (Blueprint $table) {

                $table->increments('id')->unsigned();
                $table->integer("sitemap_id")->unsigned();
                $table->integer("page_id")->unsigned();
                $table->integer("admin_id")->unsigned();
                $table->string("fb_template", 100)->nullable()->comment("AllBuilder template differer key");
                $table->integer("order")->unsigned();
                $table->date("date")->nullable();
                $table->tinyInteger("status")->unsigned()->default(\Mediapress\Modules\Content\Models\Page::PREDRAFT);
                $table->timestamp("published_at");

                $table->text("ctex_1")->nullable();
                $table->text("ctex_2")->nullable();

                $table->string("cvar_1", 300)->nullable();
                $table->string("cvar_2", 300)->nullable();
                $table->string("cvar_3", 300)->nullable();
                $table->string("cvar_4", 300)->nullable();
                $table->string("cvar_5", 300)->nullable();

                $table->integer("cint_1")->nullable();
                $table->string("cint_2")->nullable();
                $table->string("cint_3")->nullable();
                $table->string("cint_4")->nullable();
                $table->string("cint_5")->nullable();

                $table->date("cdat_1")->nullable();
                $table->date("cdat_2")->nullable();

                $table->decimal("cdec_1", 20, 4)->nullable();
                $table->decimal("cdec_2", 20, 4)->nullable();
                $table->decimal("cdec_3", 20, 4)->nullable();
                $table->decimal("cdec_4", 20, 4)->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::PAGES);
    }
}
