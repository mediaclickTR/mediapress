<?php

namespace Mediapress\Modules\MPCore\Traits;

use Carbon\Carbon;
use Cache;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Log;

trait YandexTrait
{
    // index
    public function getGeneralAnalytics($days = 7, $maxResult = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('general-analytics' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResult));
        $urlParams = [
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'metrics' => 'ym:s:visits,ym:s:pageviews,ym:s:users',
            'dimensions' => 'ym:s:date',
            'sort' => 'ym:s:date',
            'ids' => $this->counter_id,
            'oauth_token' => $this->token
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));
        $this->data = $this->request($requestUrl, $cacheName);
        return $this->adaptGeneralAnalytics();
    }

    public function getOrganicData($days = 7, $maxResult = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('organic-data' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResult));

        $urlParams = [
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'metrics' => 'ym:s:visits,ym:s:users',
            'filters' => "trafficSource=='organic'",
            'dimensions' => 'ym:s:searchEngine',
            'ids' => $this->counter_id,
            'oauth_token' => $this->token
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));
        $this->data = $this->request($requestUrl, $cacheName);
        return $this->adaptOrganicData();
    }

    public function getDurationData($days = 7, $maxResult = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('duration-data' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResult));

        $urlParams = [
            'ids' => $this->counter_id,
            'oauth_token' => $this->token,
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'preset' => 'sources_summary'

        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));

        $this->data = $this->request($requestUrl, $cacheName);

        return $this->adaptDurationData();
    }

    public function adaptGeneralAnalytics()
    {
        $itemArray = [];

        if (isset($this->data) && $this->data['data']) {
            foreach ($this->data['data'] as $item) {
                $itemArray['date'][] = Carbon::createFromFormat('Y-m-d', $item['dimensions'][0]['name'])->formatLocalized('%d.%m.%Y');
                $itemArray['visits'][] = $item['metrics'][0];
                $itemArray['pageviews'][] = $item['metrics'][1];
                $itemArray['users'][] = $item['metrics'][2];
            }
        } else {
            $itemArray['date'][] = "-";
            $itemArray['visits'][] = 0;
            $itemArray['pageviews'][] = 0;
            $itemArray['users'][] = 0;
        }
        if (isset($this->data)) {
            $itemArray['totals'] = [
                'visits' => $this->data['totals'][0],
                'pageviews' => $this->data['totals'][1],
                'users' => $this->data['totals'][2],
            ];
            // Total visit değeri null dönerse bağlantı verilerinde hata olabilir.
            if ($itemArray['totals']['visits'] == null || $itemArray['totals']['pageviews'] == null || $itemArray['totals']['users'] == null) {
                return false;
            }
        }




        return $itemArray;
    }

    public function adaptOrganicData()
    {
        $itemArray = [];

        $itemArray['totals'] = [
            'organicVisits' => $this->data['totals'][0],
            'organicUsers' => $this->data['totals'][1],
        ];
        return $itemArray;
    }

    public function adaptDurationData()
    {
        $dataArray = [];
        if ($this->data['data']) {
            foreach ($this->data['data'] as $item) {
                $dataArray['data'][] = [
                    'trafficSource' => $item['dimensions'][0]['name'],
                    'sourceEngine' => $item['dimensions'][1]['name'],
                    'visits' => $item['metrics'][0],
                    'bounceRate' => $item['metrics'][1],
                    'pageDepth' => $item['metrics'][2],
                    'avgVisitDurationSeconds' => $item['metrics'][3]
                ];
            }
        } else {
            $dataArray['data'][] = [
                'trafficSource' => "-",
                'sourceEngine' => "-",
                'visits' => "-",
                'bounceRate' => 0,
                'pageDepth' => 0,
                'avgVisitDurationSeconds' => 0
            ];
        }


        $dataArray['totals'] = [
            'visits' => $this->data['totals'][0],
            'bounceRate' => $this->data['totals'][1],
            'pageDepth' => $this->data['totals'][2],
            'avgVisitDurationSeconds' => $this->data['totals'][3],
        ];

        return $dataArray;
    }

    // Detailed
    public function getPageAnalytics($days = 7, $maxResults = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('page-analytics' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResults));

        $urlParams = [
            'ids' => $this->counter_id,
            'oauth_token' => $this->token,
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'metrics' => 'ym:pv:pageviews',
            'dimensions' => 'ym:pv:URLPathFull,ym:pv:title',
            'sort' => '-ym:pv:pageviews',
            'limit' => $maxResults

        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));

        $this->data = $this->request($requestUrl, $cacheName);

        return $this->adaptPageAnalytics();
    }

    public function adaptPageAnalytics()
    {
        $dataArray = [];

        if ($this->data['data']) {
            foreach ($this->data['data'] as $item) {
                $dataArray[] = [
                    'url' => $item['dimensions'][0]['name'],
                    'title' => $item['dimensions'][1]['name'],
                    'pageviews' => $item['metrics'][0]
                ];
            }
        } else {
            $dataArray[] = [
                'url' => "-",
                'title' => "-",
                'pageviews' => 0
            ];
        }


        return $dataArray;
    }

    public function getCountries($days = 7, $maxResults = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('countries' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResults));

        $urlParams = [
            'ids' => $this->counter_id,
            'oauth_token' => $this->token,
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'dimensions' => 'ym:s:regionCountry,ym:s:regionArea',
            'metrics' => 'ym:s:visits',
            'sort' => '-ym:s:visits',
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));

        $this->data = $this->request($requestUrl, $cacheName);
        return $this->adaptCountries();
    }

    public function adaptCountries()
    {
        $key_array = [];
        $idArray = [];

        if ($this->data['data']) {
            foreach ($this->data['data'] as $value) {
                if (!in_array($value['dimensions'][0]['id'], $key_array)) {
                    $key_array[] = $value['dimensions'][0]['id'];
                    $idArray[] = $value['dimensions'][0];
                }
            }
        } else {
            $key_array[] = 0;
            $idArray[] = 0;
        }


        $cnt = count($idArray);
        $dataArray = [];
        $drilldownArray = [];

        if ($cnt != 0) {
            for ($i = 0; $i < $cnt; $i++) {
                $dataArray[$i] = ['name' => $idArray[$i]['name'], 'y' => 0, 'drilldown' => $idArray[$i]['name']];
                $drilldownArray[$i] = ['name' => $idArray[$i]['name'], 'id' => $idArray[$i]['name'], 'data' => []];

                if ($this->data['data']) {
                    foreach ($this->data['data'] as $item) {

                        if ($item['dimensions'][0]['id'] == $idArray[$i]['id']) {
                            $dataArray[$i]['y'] += $item['metrics'][0];

                            if ($item['dimensions'][1]['name']) {
                                $region = $item['dimensions'][1]['name'];
                            } else {
                                $region = 'Не определено';
                            }

                            $drilldownArray[$i]['data'][] = [$region, $item['metrics'][0]];
                        }
                    }
                }

            }
        }

        return $dataArray;
    }

    public function getBrowserAndSystems($days = 7, $maxResults = 365)
    {
        list($startDate, $endDate) = $this->calculateDays($days);
        $cacheName = md5(serialize('browser-and-systems' . $startDate->format('Y-m-d') . $endDate->format('Y-m-d') . $maxResults));

        $urlParams = [
            'ids' => $this->counter_id,
            'oauth_token' => $this->token,
            'date1' => $startDate->format('Y-m-d'),
            'date2' => $endDate->format('Y-m-d'),
            'preset' => 'tech_platforms',
            'dimensions' => 'ym:s:browser',
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . urldecode(http_build_query($urlParams));

        $this->data = $this->request($requestUrl, $cacheName);

        return $this->adaptBrowserAndSystems();
    }

    public function adaptBrowserAndSystems()
    {
        $dataArray = [];

        if ($this->data['data']) {
            foreach ($this->data['data'] as $item) {
                $dataArray[] = [
                    'browser' => $item['dimensions'][0]['name'],
                    'visits' => $item['metrics'][0],
                    'bounceRate' => $item['metrics'][1],
                    'pageDepth' => $item['metrics'][2],
                    'avgVisitDurationSeconds' => date("i:s", $item['metrics'][3])
                ];
            }
        } else {
            $dataArray[] = [
                'browser' => "-",
                'visits' => 0,
                'bounceRate' => 0,
                'pageDepth' => 0,
                'avgVisitDurationSeconds' => 0
            ];
        }
        return $dataArray;
    }

    // Extras methods
    public function calculateDays($numberOfDays)
    {
        $endDate = Carbon::today();
        $startDate = Carbon::today()->subDays($numberOfDays);
        return [$startDate, $endDate];
    }

    public function request($url, $cacheName)
    {
        return Cache::remember($cacheName, $this->cache, function () use ($url) {
            try {
                $client = new GuzzleClient();
                $response = $client->request('GET', $url);

                $result = json_decode($response->getBody(), true);

            } catch (ClientException $e) {
                Log::error('Yandex Metrika: ' . $e->getMessage());

                $result = null;
            }
            return $result;
        });
    }
}
