<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitemapXmlsTable extends Migration
{
    public const SITEMAP_XMLS = 'sitemap_xmls';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable(self::SITEMAP_XMLS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::SITEMAP_XMLS, function (Blueprint $table) {
                
                $table->increments('id')->unsigned();
                $table->string("title", 191)->comment("Title to display");
                $table->string("filename", 30)->comment("Base filename for this sitemap_xml entry");
                $table->tinyInteger("status")->unsigned()->default(\Mediapress\Modules\Content\Models\SitemapXml::DRAFT);
                $table->string("type", 20)->default("custom");
                $table->tinyInteger("split")->default(1);
                $table->integer("urls_per_page")->unsigned()->nullable()->default(10000);
                $table->integer("website_id");
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::SITEMAP_XMLS);
    }
}
