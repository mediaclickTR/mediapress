<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PanelMenu extends Model
{

    use SoftDeletes;
    protected $table = 'panel_menus';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = ["website_id","name"];

    public function details()
    {
        return $this->hasMany(PanelMenuDetail::class, "menu_id");
    }

    public function menu()
    {
        return $this->hasOne(PanelMenuDetail::class);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }
    // TODO: apply country group parameter to detail function and migration

}
