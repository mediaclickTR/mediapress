@if(isset($field))

    @if($field->attrs['type'] == 'hidden')
        <{!! $field['tag'] !!} {!! $field->attributes(['class'=>'form-control']) !!} id
        ="{!! $field['id'] !!}" />
    @elseif($field->attrs['type'] == 'country')
        @php
            $lang_code = in_array($mediapress->activeLanguage->code, ['tr', 'en']) ? $mediapress->activeLanguage->code : 'en';
            $countries = Cache::remember(cacheKey(["FORM", "COUNTRIES"]), 10, function() use($lang_code) {
                return Mediapress\Modules\MPCore\Models\Country::all()->pluck($lang_code, 'id');
            });
        @endphp

        <div class="form-group">
            @include('HeraldistWeb::forms.components.label',['field'=>$field])
            <select {!! $field->attributes() !!} id="{!! $field['id'] !!}">
                {!! $field['content'] !!}
                <option value="">{!! strip_tags(langPart('form.select_option')) !!}</option>
                @foreach($countries as $id => $name)
                    <option value="{!! $name !!}" data-id="{!! $id !!}">{!! $name !!}</option>
                @endforeach
            </select>
        </div>
    @elseif($field->attrs['type'] == 'province')
        @if($field->attrs['data-connect-country'] == false)
            @php
                $lang_code = in_array($mediapress->activeLanguage->code, ['tr', 'en']) ? $mediapress->activeLanguage->code : 'en';
                $provinces = Cache::remember(cacheKey(["FORM", "PROVINCE"]), 10, function() use($lang_code, $field) {
                    $country =  Mediapress\Modules\MPCore\Models\Country::where('code', strtoupper($field->attrs['data-country-code']))
                        ->first();
                    $cities = array();
                    if($country) {
                        $temp_cities = $country->provinces;
                        $name = $temp_cities->pluck('name', 'id')->toArray();
                        $plate = $temp_cities->pluck('id', 'name')->toArray();
                        $collator = new \Collator("tr_TR");
                        $collator->sort($name);
                        foreach ($name as $c) {
                            $cities[$plate[$c]] = $c;
                        }
                    }
                    return $cities;
                });
            @endphp
        @else
            @pushonce('scripts:country')
                <script>
                    var country_id = "";
                    var holdCountry = [108,231,38,57];
                    $('select[type="country"]').on('change', function () {
                        var obj = $(this);
                        country_id = obj.children("option:selected").attr('data-id');
                        $.ajax({
                            'method': 'post',
                            'data': {'country_id': country_id, '_token': "{{csrf_token()}}"},
                            'url': "{{route('form.changeCountry')}}",
                            success: function (result) {
                                if(result.length > 0) {
                                    var content = '<option value="">{!! strip_tags(langPart("form.select_option")) !!}</option>';
                                    $.each(result, function (k, v) {
                                        content += '<option value="' + v.name + '" data-id="' + v.id + '">' + v.name + '</option>'
                                    });
                                    $('select[type="province"]').html(content);
                                    $('select[type="province"]').parent().show();
                                } else {
                                    $('select[type="province"]').parent().hide();
                                }
                            }
                        });
                    });


                    $('select[type="province"]').on('change', function () {
                        var obj = $(this);

                        if(holdCountry.includes(country_id * 1)) {
                            $.ajax({
                                'method': 'post',
                                'data': {'province_id': obj.children("option:selected").attr('data-id'), '_token': "{{csrf_token()}}"},
                                'url': "{{route('form.changeProvince')}}",
                                success: function (result) {
                                    if(result.length > 0) {
                                        var content = '<option value="">{!! strip_tags(langPart("form.select_option")) !!}</option>';
                                        $.each(result, function (k, v) {
                                            content += '<option value="' + v.name + '" data-id="' + v.id + '">' + v.name + '</option>'
                                        });
                                        $('#city').html(content);
                                        $('#city').parent().show();
                                    } else {
                                        $('#city').parent().hide();
                                    }
                                }
                            });
                        }
                    })
                </script>
            @endpushonce
        @endif



        <div class="form-group" {!! $field->attrs['data-connect-country'] == true ? 'style="display: none"' : '' !!} >
            @include('HeraldistWeb::forms.components.label',['field'=>$field])
            <select {!! $field->attributes() !!} id="{!! $field['id'] !!}">
                {!! $field['content'] !!}
                <option value="">{!! strip_tags(langPart('form.select_option')) !!}</option>
                @isset($provinces)
                    @foreach($provinces as $id => $name)
                        <option value="{!! $name !!}" data-id="{{$id}}" >{!! $name !!}</option>
                    @endforeach
                @endisset
            </select>
        </div>


        <div class="form-group" style="display: none">
            <select class="city" id="city" name="city">
            </select>
        </div>

    @else
        <div class="form-group">
            @include('HeraldistWeb::forms.components.label',['field'=>$field])
            @if(in_array($field->attrs['type'],['radio','checkbox']))
                @foreach($field->options as  $key => $option)
                    <div class="{!! $field->attrs['type'] !!}">
                        @if($field->attrs['type'] == 'checkbox')

                            @php($option['selected'] = $option['checked'])
                        @endif
                        <input id="{!! $field['id'].'_'.$key !!}"
                               {!! $field->attributes() !!}
                               value="{!! strip_tags($option['value']) !!}"
                            {!! isset($option['selected']) && $option['selected'] ? 'checked' :''  !!}>
                        <label for="{!! $field['id'].'_'.$key !!}">{!! $option['label'] !!}</label>
                    </div>
                @endforeach
            @else
                <{!! $field['tag'] !!} {!! $field->attributes(['class'=>'form-control']) !!} id
                ="{!! $field['id'] !!}" />

            @endif

        </div>
    @endif

@endif
