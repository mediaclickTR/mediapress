@if(!isset($page))
    {{ Form::open(array('route' => array('Content.pages.store', $sitemap_id))) }}
@else
    {{ Form::model($page,array('route' => array('Content.pages.update', $sitemap_id))) }}
    {!! Form::hidden("id",$page->id) !!}
@endif
@csrf

@if( isset($renderable) && is_a($renderable, \Mediapress\AllBuilder\Foundation\BuilderRenderable::class))
    @php $renderable->render(); @endphp
    @php
        $push = $renderable->getStacks();
    @endphp
    @foreach($push as $stack => $p)
        @push($stack) {!! implode("\n",$p) !!} @endpush
    @endforeach
@endif


<div class="submit">
    <button type="submit">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

{{ Form::close() }}
