<?php

namespace Mediapress\Traits;

use Mediapress\Exceptions\NotFoundException;
use Mediapress\Foundation\Mediapress;
use Mediapress\Models\CategoryDetail;
use Mediapress\Models\Language;
use Mediapress\Models\PageDetail;

trait StaticTrait
{
    public function decide(Mediapress $mediapress)
    {
        $sitemap_id =  $mediapress->sitemap->id;
        if(isset($this->list[$sitemap_id])){
            $sitemap_id = $this->list[$sitemap_id];
        }
        $class = 'App\Http\Controllers\Web\Statics\Static' . $sitemap_id . 'Controller';
        if (class_exists($class)) {
            return app($class)->mainPage($mediapress);
        }
        if ($mediapress->relation instanceof PageDetail) {
            if ((isset($this->pass) && $this->pass != 1) || !isset($this->pass)) {
                if (!isset($mediapress->relation->page->id)) {
                    throw new NotFoundException('Not Found');
                }
            }
            $this->method = 'pageDetail';
        } elseif ($mediapress->relation instanceof CategoryDetail) {
            if ((isset($this->pass) && $this->pass != 1) || !isset($this->pass)) {
                if (!isset($mediapress->relation->category->id)) {
                    throw new NotFoundException('Not Found');
                }
            }
            $this->method = 'category';
        }
        return $this->{$this->method}($mediapress);
    }

}