<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCookiesTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('cookies'))
            return;

		Schema::connection('mediapress_interaction')->create('cookies', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('hash');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('device_id');
            $table->unsignedInteger('operating_system_id');
            $table->unsignedInteger('browser_id');
            $table->timestamp('created_at')->index('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('cookies');
	}
}
