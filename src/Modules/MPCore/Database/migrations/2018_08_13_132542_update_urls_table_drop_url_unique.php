<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;


class UpdateUrlsTableDropUrlUnique extends Migration
{

    public function up()
    {


        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });

        $schema->table('urls', function(Blueprint $table) {
            $table->dropUnique('url_unique');
        });

        $schema->table('dynamic_urls', function(Blueprint $table) {
            $table->dropUnique('dynamic_url_unique');
        });

        $schema->table('redirect_urls', function(Blueprint $table) {
            $table->dropUnique('redirect_url_unique');
        });




    }

    public function down()
    {

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->disableForeignKeyConstraints();
        $schema->table('urls', function (Blueprint $table) {
            $table->unique([WEBSITE_ID, "url"], "url_unique");
        });
        $schema->table('dynamic_urls', function (Blueprint $table) {
            $table->unique([WEBSITE_ID, "url_id"], "dynamic_url_unique");
        });
        $schema->table('redirect_urls', function (Blueprint $table) {
            $table->unique([WEBSITE_ID, "url"], "redirect_url_unique");
        });

        $schema->enableForeignKeyConstraints();
    }
}
