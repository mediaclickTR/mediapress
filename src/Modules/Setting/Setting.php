<?php

namespace Mediapress\Modules\Setting;

use Mediapress\Models\MPModule;

class Setting extends MPModule
{

    public $name = "Setting";
    public $url = "mp-admin/Setting";
    public $description = "Settings module of Mediapress";
    public $author = "";
    private $plugins = [];
}
