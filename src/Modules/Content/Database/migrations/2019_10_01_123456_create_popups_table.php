<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('popups'))
            return;

        Schema::create('popups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger("admin_id")->nullable();
            $table->integer("website_id")->unsigned();
            $table->integer('order')->unsigned()->nullable();
            $table->tinyInteger('status')->unsigned();
            $table->string('devices', 50);
            $table->tinyInteger('display_date_type')->unsigned();
            $table->date('start_date')->nullable();
            $table->date('finish_date')->nullable();
            $table->tinyInteger('display_time_type')->unsigned();
            $table->smallInteger('display_time')->unsigned()->nullable();
            $table->tinyInteger('type')->unsigned();
            $table->string('name');
            $table->tinyInteger('name_display')->unsigned();
            $table->text('content')->nullable();
            $table->tinyInteger('impressions')->nullable();
            $table->integer('impression_count')->unsigned()->nullable();
            $table->tinyInteger('show_page')->unsigned();
            $table->tinyInteger('conditions')->unsigned();
            $table->smallInteger('condition_time')->unsigned()->nullable();
            $table->smallInteger('condition_pixel')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popups');
    }
}
