<?php

namespace Mediapress\Modules\Entity\Models;

use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    protected $table = 'admins';
    public $timestamps = true;
    protected $fillable = ["language_id","first_name", "last_name", "name", "email", "phone", "password"];
    protected $hidden = ["password", "remember_token"];

    /*public function group()
    {
        return $this->belongsTo(Groups::class);
    }*/

    /*public function role()
    {
        return $this->belongsTo(Role::class);
    }*/

    public function userable()
    {
        return $this->morphTo();
    }


}
