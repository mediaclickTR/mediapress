<?php

return [
    'select_websites' => "Liste'nin Bağlanacağı Website(leri) Seç",
    'forms' => 'Formlar',
    'form_create' => 'Form Oluştur',
    'form_edit' => 'Form Düzenle',
    'success_message' => 'Başarı Mesajı',
    'error_message' => 'Hata Mesajı',
    'buton_name' => 'Buton Adı',
    'receiver_email' => 'Alıcı E-Posta (Boş Bırakılabilir)',
    'recaptcha_key' => 'Google ReCaptcha Key',
    'recaptcha_secret' => 'Google ReCaptcha Secret',


    'send_user' => 'Formu doldurulan kullanıcıya e-posta gönderilsin mi?',
    'send_user_1' => 'Evet, gönderilsin',
    'send_user_2' => 'Hayır, gönderilmesin',
    /*
     * Forms/details
     */
    'form_detail' => 'Form Detay',
    'inbox' => 'Gelen Kutusu',
    'form.inputs' => 'Form Elemanlarını Yönet',
    'lang.part.save.info'=>'Dikkat kaydetme işlemi otomatik yapılır. İlgili alanları doldurmanız yeterlidir.',
    'notes'=>'Notlar'
];
