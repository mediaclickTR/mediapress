@extends('ContentPanel::inc.module_main')

@php($devices = ["desktop","tablet","mobile"])

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! __("ContentPanel::slider.index.title") !!}
                </div>
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Content.slider.create') !!}" class="btn btn-primary btn-sm">
                    <i class="fas fa-plus"></i>
                    {!! __("ContentPanel::general.add_new") !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>{!! __("ContentPanel::general.status") !!}</th>
                    <th>{!! __("ContentPanel::general.name") !!}</th>
                    <th>{!! __("ContentPanel::slider.index.scene_count") !!}</th>
                    <th>{!! __("ContentPanel::slider.index.added_by") !!}</th>
                    <th>{!! __("ContentPanel::general.created_at") !!}</th>
                    <th>{!! __("ContentPanel::general.actions") !!}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)
                    <?php
                        $sceneStatuses = $slider->scenes->pluck('status')->toArray();
                    ?>
                    <tr>
                        <td>
                            {{ $slider->id }}
                        </td>
                        <td class="status">
                            @if(in_array(1, $sceneStatuses))
                                <i class="active" style="background: #7fcc46"></i>{{ __('ContentPanel::general.status_title.active') }}
                            @else
                                <i class="passive" style="background: #dc3545"></i>{{ __('ContentPanel::general.status_title.passive') }}
                            @endif
                        </td>
                        <td>
                            {{ $slider->name }}
                        </td>
                        <td>{!! $slider->scenes()->count() !!}</td>
                        <td>{!! ($slider->admin) ? $slider->admin->short_name : '' !!}</td>
                        <td>
                            {{ \Carbon\Carbon::parse($slider->created_at)->format('d.m.Y H:i') }}
                        </td>
                        <td>
                            <a href="{!! route('Content.slider.scenes.index',$slider->id) !!}" class="mr-2" title="{!! __("ContentPanel::slider.index.scenes") !!}">
                                <i class="far fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('Content.slider.delete',$slider->id) !!}" title="{!! __("ContentPanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        td.status > i {
            content: "";
            float: left;
            display: block;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            margin: 5px 8px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var sliderDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("ContentPanel::general.swal.are_you_sure") }}',
                text: "{!! trans("ContentPanel::slider.slider_delete") !!}",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("ContentPanel::general.yes") !!}',
                cancelButtonText: '{!! trans("ContentPanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = sliderDeleteUrl;
                }
            });
        })
    </script>
@endpush
