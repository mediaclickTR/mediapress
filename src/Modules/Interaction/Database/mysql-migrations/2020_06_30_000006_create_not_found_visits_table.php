<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotFoundVisitsTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('not_found_visits'))
            return;

		Schema::connection('mediapress_interaction')->create('not_found_visits', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cookie_id');
            $table->unsignedBigInteger('session_id');
            $table->string('ip');
            $table->text('url');
            $table->text('query_string')->nullable();
            $table->text('referer')->nullable();
            $table->timestamp('created_at')->index('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('not_found_visits');
	}
}
