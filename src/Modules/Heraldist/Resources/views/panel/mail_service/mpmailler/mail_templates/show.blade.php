@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.template_detail") !!}</h1>
        {!! $form->body !!}
    </div>
@endsection
