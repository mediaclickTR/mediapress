<?php

$str = "Masaüstü Seçenekleri";
$str1 = "Tablet Seçenekleri";
$str2 = "Mobil Seçenekleri";
return [

    "index" => [
        'title' => 'Sliderlar',
        "scene_status" => 'Sahne Durumları',
        "scene_count" => 'Sahne Sayısı',
        "added_by" => 'Ekleyen',
        "scenes" => 'Sahneler'
    ],

    "create" => [
        'title' => 'Slider Ekle',
        'select_website' => 'Website seçiniz',
        'name' => 'Slider Adı',
        'type' => [
            'label' => 'Slider Tipi',
            'image' => 'Görsel',
            'video' => 'Video'
        ],
        'screen' => 'Slider ekranı',
        'width' => 'Genişlik',
        'height' => 'Yükseklik',
        'text' => 'Metin Alanı Sayısı',
        'button' => 'Buton Sayısı',
    ],

    "edit" => [
        "title" => 'Slider Düzenle',
        "go_scenes" => 'Sahneleri Gör',
    ],



    "add_title" => "Slider Ekle",
    "devices" => "Cihazlar",
    "count" => "Adet",
    "slider_delete" => "Bu Slider'a Ait Tüm Sahneler Silinecek!",
    "device" => [
        "desktop" => "Masaüstü",
        "desktop_input_tooltip" => "Masaüstü",
        "tablet" => "Tablet",
        "tablet_input_tooltip" => "Tablet",
        "mobile" => "Mobil",
        "mobile_input_tooltip" => "Mobil",

        "auto" => "Otomatik",
        "new_load" => "Yeni Yükle",

        "desktop_info" => $str,
        "desktop_options" => $str,
        "desktop_options_info" => $str,
        "desktop_language_options" => "Masaüstü Dil",
        "desktop_language_options_info" => "Masaüstü Dil",

        "tablet_info" => $str1,
        "tablet_options" => $str1,
        "tablet_options_info" => $str1,
        "tablet_language_options" => "Tablet Dil",
        "tablet_language_options_info" => "Tablet Dil",

        "mobile_info" => $str2,
        "mobile_options" => $str2,
        "mobile_options_info" => $str2,
        "mobile_language_options" => "Mobil Dil",
        "mobile_language_options_info" => "Mobil Dil",


        "image" => "Görsel",
        "image_info" => "Görsel",
        "text" => "Yazı",
        "text_info" => "Yazı",
        "button" => "Button",
        "button_info" => "Button",
    ],

    "type" => [
        "image" => "Resim",
        "video" => "Video"
    ],

    "has_language" => [
        "yes" => "Evet",
        "no" => "Hayır",
    ]
];
