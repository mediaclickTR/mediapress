<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramPostsTable extends Migration {

	public function up()
	{
	    // neden mediapress blueprint kullnılıyor öğren
        Schema::create('instagram_posts', function(Blueprint $table) {

            $table->bigIncrements("id");
            $table->text("instagram_id");
            $table->json("post_datas");
            $table->timestamps();
		});

	}

	public function down()
	{
		Schema::drop('instagram_posts');
	}
}
