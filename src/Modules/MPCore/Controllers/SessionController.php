<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\Language;

class SessionController
{
    public function setWebsiteSession($website_id)
    {
        $website = Website::find($website_id);
        if ($website){
            session()->forget('panel.website');
            session()->put('panel.website', $website);
            return redirect()->route('panel.dashboard');
        }else{
            dd("Website bulunamadı.");
        }
    }
    public function setLanguageSession($language_id)
    {
        $language = Language::find($language_id);
        if ($language){
            session()->forget('panel.active_language');
            session()->put('panel.active_language', $language);
            return redirect()->route('panel.dashboard');
        }else{
            dd("Dil bulunamadı.");
        }
    }

}
