@if(isset($fields))
    @foreach($fields as $field)
        @include('HeraldistWeb::forms.email_parts.'.$field['key'],['field'=>new \Mediapress\Modules\Heraldist\Foundation\Field($field)])
    @endforeach
@endif