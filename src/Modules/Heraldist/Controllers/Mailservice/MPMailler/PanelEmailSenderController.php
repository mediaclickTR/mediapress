<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\MPMailler;

use Mediapress\Http\Controllers\PanelController as Controller;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Entity\Facades\Entity;
use Mediapress\Modules\Heraldist\Models\Maillist;
use Mediapress\Modules\Heraldist\Models\MailTemplate;
use Mediapress\Modules\Heraldist\Models\EmailSender;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\Heraldist\Facades\Heraldist;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Cache;
use Mediapress\Modules\MPCore\Facades\MPCore;

class PanelEmailSenderController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const ERROR = 'error';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    protected $emailFromAddress;
    protected $emailFromName;

    public function __construct()
    {
        $this->emailFromAddress = MPCore::settingSunFindValue('heraldist.mpmailler.fromemailaddress');
        $this->emailFromName = MPCore::settingSunFindValue('heraldist.mpmailler.fromname');
    }

    public function index()
    {
        if(!userAction('message.email_sender.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        return view('HeraldistPanel::mail_service.mpmailler.email_sender.index', compact('emailsenders'));
    }

    public function create()
    {
        if(!userAction('message.email_sender.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = Entity::getMaillist();

        $maillist_all = Maillist::get();
        $mailtemplates = MailTemplate::get();
        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck("name", "id");

        return view('HeraldistPanel::mail_service.mpmailler.email_sender.create',compact('websites','maillist_all','mailtemplates','data'));
    }

    public function store(Request $request)
    {
        // Yeni e-posta listesi oluşturursa
        if(!$request->maillist_id)
        {
            if (!$request->emails) {
                return redirect()->back()->with(self::ERROR, trans("HeraldistPanel::mail_service.please_select_email"));
            }

            $emails ='';
            $usernames = '';

            if($request->emails) {
                foreach ($request->emails as $email)
                {
                    $data = explode("-",$email);
                    $emails .= $data[0] . ',';
                    if ($data[1]){
                        $usernames .= $data[1] . ',';
                    }
                    else{
                        $usernames .= "-" . ',';
                    }
                }
            }

            $emails = trim($emails,",");
            $usernames = trim($usernames,",");

            $data = new Maillist;
            $data->name = trans('HeraldistPanel::mail_service.quick_create').' ('.Carbon::now().')';
            $data->emails = $emails;
            $data->usernames = $usernames;
            $data->save();
            $maillist_id = $data->id;
        }
        else{
            $maillist_id = $request->maillist_id;
        }

        // Yeni şablon oluşturursa
        if(!$request->mailtemplate_id){
            if(!$request->name || !$request->mail_title || !$request->mail_subject || !$request->mail_detail || !$request->body) {
                return redirect()->back()->with(self::ERROR, trans("HeraldistPanel::mail_service.email_template_empty_fields"));
            }
            $data = new MailTemplate();
            $data->name = $request->name;
            $data->mail_title = $request->mail_title;
            $data->mail_subject = $request->mail_subject;
            $data->mail_detail = $request->mail_detail;
            $data->body = $request->body;
            $data->save();
            $email_template = $data->id;
        }
        else{
            $email_template = $request->mailtemplate_id;
        }


        $maillist = Maillist::find($maillist_id);
        $mailtemplate = MailTemplate::find($email_template);

        $title = $mailtemplate->mail_title;
        $subject = $mailtemplate->mail_subject;
        $body = $mailtemplate->mail_detail;

        // eşleşen şablon veya maillist yoksa uyarı ver
        if(!$mailtemplate) {
            return redirect(route('Heraldist.mpmailler.mail_templates.index'))->with(self::ERROR, trans("HeraldistPanel::mail_service.not_found_email_template"));
        }
        if(!$maillist){
            return redirect(route('Heraldist.mpmailler.mail_templates.index'))->with(self::ERROR, trans("HeraldistPanel::mail_service.not_found_email_list"));
        }

        // Mail listesini array yap
        $emails = explode(',', $maillist->emails);
        $usernames = explode(',', $maillist->usernames);

        $fromEmail = $this->emailFromAddress;

        foreach ($emails as $key => $email)
        {
            // Mail şablonu içinde arama yap, eşleşen verileri replace et
            $search  = array('%username%','%mail_title%', '%mail_subject%', '%mail_detail%');
            $replace = array($usernames[$key],$title, $subject, $body);
            $mail_body = str_replace($search,$replace,$mailtemplate->body);
            $data = ['mail_body' => $mail_body];

            // Mail Send
            Heraldist::mailSender($data,$email,$title,$subject,$fromEmail);
        }

        // İşlem sonrası Maillist ve template'i kaydet
        $send_data = ['maillist_id' => $maillist->id, 'mailtemplate_id' => $mailtemplate->id];
        $sended = EmailSender::insertGetId($send_data);

        //Log
        if($sended) {
            $lastInsert = EmailSender::find($sended);
            UserActionLog::emailSended(__CLASS__."@".__FUNCTION__,$lastInsert);
        }

        return redirect(route("Heraldist.mpmailler.email_senders.sendeds"))->with('success', trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(!userAction('message.email_sender.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $emailsender = EmailSender::find($id);

        if ($emailsender)
        {
            $emailsender->delete();
            //Log
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$emailsender);
        }

        return redirect(route('Heraldist.mpmailler.email_senders.sendeds'))->with('message', trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function selectedRemove(Request $request)
    {
        if($request->checked) {
            foreach ($request->checked as $check){
                EmailSender::destroy($check);
            }
        }
        else{
            return redirect()->back()->withErrors([trans('MPCorePanel::general.danger_message')]);
        }

        return redirect()->back()->with('message', trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function sendeds()
    {
        if(!userAction('message.email_sender.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $emailsenders = EmailSender::paginate(25);

        return view('HeraldistPanel::mail_service.mpmailler.email_sender.sendeds', compact('emailsenders'));
    }

    public function show($id)
    {
        $sended = EmailSender::find($id);

        $search  = array('%mail_title%', '%mail_subject%', '%mail_detail%');
        $replace = array( $sended->email_template->mail_title, $sended->email_template->mail_subject, $sended->email_template->mail_detail);
        $mail_body = str_replace($search,$replace,$sended->email_template->body);

        return view('HeraldistPanel::mail_service.mpmailler.email_sender.show', compact('sended','mail_body'));
    }

}