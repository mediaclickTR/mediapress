@stack("styles")
<div class="modal-header">
    <h4 class="modal-title">{!! $title ?? null !!}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    {!! $renderable->render() !!}
</div>
<script>
    let list = $('.mfile');


    $.each(list, function (i, index) {
        let input = $(list[i]);
        let editor = new mFile(input);

        //editor.build()
    })
</script>
@stack("scripts")
