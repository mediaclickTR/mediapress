<?php

return [
    'social_media' => 'Social Media',
    'link' => 'Social Media Link',
    'add_social_media' => 'Add Social Media',
    'select_group' => 'Select Group',
    'group' => 'Group' ,
    'name' => 'Social Media Name',
    'icon' => 'Social Media Icon',
    'target_area' => 'Target Area'
];
