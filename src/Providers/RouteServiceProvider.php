<?php

namespace Mediapress\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Mediapress\Http\Controllers';

    protected $files = [];
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {

        $modules = \Mediapress\Facades\ModulesEngine::getMPModules();

        foreach ($modules as $module => $namespace) {
            $modulesdir = base_path('vendor/mediapress/mediapress/src/Modules');
            $webRoutes = $modulesdir.'/'.$module.'/Routes/WebRoutes.php';
            $webRoutes = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $webRoutes);
            if (!in_array($webRoutes,$this->files) && file_exists($webRoutes)) {
                $this->files[] = $webRoutes;
                Route::middleware('web', 'mediapress.after')
                    ->namespace($namespace.'\Controllers')
                    ->group($webRoutes);
            }
            $panelRoutes = $modulesdir.'/'.$module.'/Routes/PanelRoutes.php';
            $panelRoutes = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $panelRoutes);
            if (!in_array($panelRoutes,$this->files) && file_exists($panelRoutes)) {
                $this->files[] = $panelRoutes;
                Route::middleware('web')
                    ->namespace($namespace.'\Controllers')
                    ->prefix('mp-admin')
                    ->group($panelRoutes);
            }

        }
    }


}
