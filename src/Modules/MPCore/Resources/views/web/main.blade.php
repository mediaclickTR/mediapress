<!doctype html>
<html dir="{!! isset($mediapress->activeLanguage) ? $mediapress->activeLanguage->direction : 'ltr' !!}" lang="{!! isset($mediapress->activeLanguage) ? $mediapress->activeLanguage->code : 'en' !!}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! $mediapress->metaworker !!}
    @if(!isset($mediapress->metaworker->getFinalList()["image"]) && file_exists(public_path(). '/images/contentlogo.svg'))
    <meta name="image" content="{!! asset("images/contentlogo.svg") !!}">
    @endif
    {!! $mediapress->tagManager('head') !!}
    @stack("styles")
</head>
<body class=@stack('bodyClass')>

{!! $mediapress->tagManager('body') !!}

@stack('prependBody')

@yield('header')

@yield('content')

@yield('footer')

@stack('appendBody')

@stack("scripts")

@stack("scripts_after")

@include('Popup::main')

@include('mediapress::debugBar')

@include('QuickAccessView::seo_content')

@if(adminAuth() && !isMobile())
    @include('QuickAccessView::admin-bar', ['admin'=>\Illuminate\Support\Facades\Auth::guard('admin')->user()])
@endif
</body>
</html>
