<div class="modal fade p-0 mcPopup" id="popupModal{{ $popup->id }}" tabindex="-1" role="dialog"
     aria-labelledby="popupModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"
             data-border-size="{{ $popup->border_size }}"
             data-border-color="{{ $popup->border_color }}"
             data-border-radius="{{ $popup->border_radius }}"
             data-padding-size="{{ $popup->padding_size }}">

            @if($popup->name_display == 1)
                <div class="modal-header">
                    <h5 class="modal-title" data-title-size="{{ $popup->title_size }}"
                        class="mcPopupTitle">{{$popup->name}}</h5>

                    <button type="button" class="mcPopupClose" data-dismiss="modal" aria-label="Close"
                            data-button-bg="{{ $popup->button_bg }}"
                            data-button-color="{{ $popup->button_color }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" data-a-color="{{ $popup->a_color }}"
                     style="{{ $popup->font ? "font-family: '$popup->font, sans-serif !important;'" : '' }}">
                    {!! $popup->content !!}
                </div>
            @else
                <div class="modal-body" data-a-color="red"
                     style="{{ $popup->font ? "font-family: '$popup->font'" : '' }}">
                    <button type="button" class="mcPopupClose" data-dismiss="modal" aria-label="Close"
                            data-button-bg="{{ $popup->button_bg }}"
                            data-button-color="{{ $popup->button_color }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {!! $popup->content !!}
                </div>
            @endif
        </div>
    </div>
</div>

<script>
    $('.mcPopup .modal-content').each(function () {
        var $borderSize = $(this).attr("data-border-size");
        var $borderColor = $(this).attr("data-border-color");
        var $buttonBg = $(this).find('.mcPopupClose').attr("data-button-bg");
        var $buttonColor = $(this).find('.mcPopupClose').attr("data-button-color");
        var $titleSize = $(this).find('.mcPopupTitle').attr("data-title-size");
        var $paddingSize = $(this).attr("data-padding-size");
        var $aTagColor = $(this).find('.modal-body').attr("data-a-color");
        var $borderRadius = $(this).attr("data-border-radius");

        $(this).css({
            'borderWidth': '' + $borderSize + '',
            'borderColor': '' + $borderColor + '',
            'border-radius': $borderRadius + 'px'
        });
        $(this).find('.modal-header,.modal-body').css({
            'padding': '' + $paddingSize + ''
        });
        $(this).find('.mcPopupClose').css({
            'backgroundColor': '' + $buttonBg + '',
            'color': '' + $buttonColor + ''
        });
        $(this).find('.mcPopupTitle').css({
            'font-size': '' + $titleSize + 'px'
        });
        $(this).find('#mctoolTipText a').css(
            'cssText', "color: " + $aTagColor + " !important"
        );
    });

    @if($popup->conditions == 1)
    $(window).scroll(function () {
        if ($(document).scrollTop() > {{ $popup->condition_pixel }}) {
            $('#popupModal{{ $popup->id }}').modal('show');
        }
    });
    @else
    @if($popup->condition_time > 0)
    setTimeout(function () {
        $('#popupModal{{ $popup->id }}').modal('show');
    }, {{$popup->condition_time*1000}});
    @else
    setTimeout(function () {
        $('#popupModal{{ $popup->id }}').modal('show');
    }, 100);
    @endif
    @endif

    @if($popup->display_time_type == 1)
    setTimeout(function () {
        $('#popupModal{{ $popup->id }}').modal('hide');
    }, {{ ($popup->display_time + $popup->condition_time) *1000}});
    @endif
</script>
