<?php
return [


    "locale" => [
        'name' => 'LocalePanel::auth.sections.locale_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [

        ]
    ]
];
