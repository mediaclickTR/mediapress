<?php

namespace Mediapress\Modules\Entity\Controllers;

use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Models\Page;
use Mediapress\Models\Sitemap;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Html\Builder;


class MemberController extends Controller
{
    use TableBuilderTrait;
    public const MEMBERS = "members";
    public const DEFAULT1 = "default";
    public const DEFAULT_PASS_WORD = "default___password";

    /** Admins List
     * @param Builder $builder
     * @return mixed
     */
    public function index(Builder $builder)
    {
        $dataTable = $this->columns($builder)->ajax(route('admin.members.ajax'));

        return $this->view("pages.members.index", compact('dataTable'));
    }

    public function formCreate(FormBuilder $formBuilder, $id = 0)
    {
        if ($id)
        {
            $title = trans("panel::general.edit-title", ["name" => "Üye"]);
            $button = trans("panel::general.edit");
            $data = [
                "find" => [
                    "id", "=", $id
                ]
            ];
        }
        else
        {
            $title = trans("panel::general.create-title", ["name" => "Üye"]);
            $button = trans("panel::general.save");
            $data = [];
        }

        $form = PanelFormBuilder::create(self::MEMBERS, self::DEFAULT1, $formBuilder, [], $data);

        return $this->view("pages.members.form", compact("id", "form", "title", "button"));
    }

    public function save(FormBuilder $formBuilder, Collection $data)
    {
        $id = $data->pull("parameters.id");
        if($data->get(self::DEFAULT_PASS_WORD)){
        $data->put(self::DEFAULT_PASS_WORD, bcrypt($data->get(self::DEFAULT_PASS_WORD)));
    }else {
            $data->forget(self::DEFAULT_PASS_WORD);
    }


        if ($id)
        {
            $admin = \Arr::first(FormStore::store($formBuilder, self::MEMBERS, self::DEFAULT1, [], [
                "find" => ["id", "=", $id], "data" => $data->except("parameters")->toArray()
            ]));

            if ($admin)
            {
                return [
                    true, trans("panel::general.ajax.edit_success", ["name" => $admin->name])
                ];
            }

            return [
                false, trans("panel::general.ajax.edit_error", ["name" => $admin->name])
            ];
        }
        else
        {
            $email = $data['default___email'];

            $unique = User::where('email',$email)->first();
            if(!is_null($unique)){
                return [
                    false, trans("panel::general.ajax.email_not_unique", ["name" => $email])
                ];
            }
            $admin = \Arr::first(FormStore::store($formBuilder, self::MEMBERS, self::DEFAULT1, [], [
                "data" => $data->except("parameters")->toArray()
            ]));

            if ($admin)
            {
                return [
                    true, trans("panel::general.ajax.edit_success", ["name" => $admin->name])
                ];
            }

            return [
                false, trans("panel::general.ajax.edit_error", ["name" => $admin->name])
            ];
        }
    }

    public function delete(Collection $data)
    {
        $id = $data->pull("parameters.id");
        if (! $id){
            return [false, "Geçersiz Id"];
        }

        $admin = User::find($id);

        if (! $admin){
            return [false, "Üye Kaydı Bulunamadı!"];
        }

        if ($admin->delete())
        {
            return [true, trans("panel::general.ajax.delete_success", ["name" => $admin->name])];
        }

        return [false, trans("panel::general.ajax.delete_error", ["name" => $admin->name])];
    }
}
