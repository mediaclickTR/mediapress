<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Heraldist\Models\EmailSender;
use Mediapress\Modules\MPCore\Facades\MPCore;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class PanelEmailSenderController extends Controller
{
    public const MESSAGE = 'message';
    public const HERALDIST_MAILCHIMP_EMAIL_SENDERS_CAMPAIGNS = 'Heraldist.mailchimp.email_senders.campaigns';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const ACCESSDENIED = 'accessdenied';
    public const CAMPAIGNS = "campaigns/";
    protected $apiKey;
    protected  $apiUrl;

    public function __construct()
    {
        $mailChimpApiKey = settingSun('heraldist.mailchimp.apikey');
        // API KEY KONTROL
        if($mailChimpApiKey!=''){
            $this->apiKey = $mailChimpApiKey;
        }
        else{
            dd("Mailchimp api key bulunamadı.1");
        }

        // APi URL
        $dataCenter = substr($this->apiKey,strpos($this->apiKey,'-')+1);
        $this->apiUrl = "https://".$dataCenter.".api.mailchimp.com/3.0/";
    }

    public function index()
    {
        if(!userAction('message.email_sender.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        return view('HeraldistPanel::mail_service.mailchimp.email_sender.index', compact('emailsenders'));
    }

    public function create()
    {
        if(!userAction('message.email_sender.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $maillist_all = curlGenerator($this->apiUrl."lists",$this->apiKey,"GET")->lists;
        $mailtemplates = curlGenerator($this->apiUrl."templates",$this->apiKey,"GET")->templates;
        return view('HeraldistPanel::mail_service.mailchimp.email_sender.create',compact('maillist_all','mailtemplates'));
    }

    public function store(Request $request)
    {
        $maillist_id = $request->maillist_id;
        $template_id = intval($request->template_id);
        $json = json_encode([
            'recipients'  => [
                'list_id' => $maillist_id,
            ],
            'type' => 'regular',
            'content_type' => 'html',
            'settings'  => [
                'subject_line' => $request->subject_line,
                'reply_to' => $request->reply_to,
                'from_name' => $request->from_name,
                'template_id' => $template_id
            ]
        ]);
        curlGenerator($this->apiUrl.     self::CAMPAIGNS,$this->apiKey,null,$json,"POST");

        return redirect(route(self::HERALDIST_MAILCHIMP_EMAIL_SENDERS_CAMPAIGNS))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));

    }

    public function campaigns()
    {
        if(!userAction('message.email_sender.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $campaigns = curlGenerator($this->apiUrl."campaigns",$this->apiKey,"GET");
        $campaigns=$campaigns->campaigns;
        return view('HeraldistPanel::mail_service.mailchimp.email_sender.campaigns', compact('campaigns'));
    }

    public function edit($id)
    {
        if(!userAction('message.email_sender.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $maillist_all = curlGenerator($this->apiUrl."lists",$this->apiKey,"GET")->lists;
        $mailtemplates = curlGenerator($this->apiUrl."templates",$this->apiKey,"GET")->templates;
        //curl edit
        $campaign = curlGenerator($this->apiUrl. self::CAMPAIGNS .$id,$this->apiKey,"GET");
        return view('HeraldistPanel::mail_service.mailchimp.email_sender.edit', compact('maillist_all','mailtemplates','campaign'));
    }

    public function update(Request $request)
    {
        $maillist_id = $request->maillist_id;
        $template_id = intval($request->template_id);
        $json = json_encode([
            'recipients'  => [
                'list_id' => $maillist_id,
            ],
            'type' => 'regular',
            'content_type' => 'html',
            'settings'  => [
                'subject_line' => $request->subject_line,
                'reply_to' => $request->reply_to,
                'from_name' => $request->from_name,
                'template_id' => $template_id
            ]
        ]);
        curlGenerator($this->apiUrl. self::CAMPAIGNS .$request->id,$this->apiKey,"PATCH",$json);
        return redirect(route(self::HERALDIST_MAILCHIMP_EMAIL_SENDERS_CAMPAIGNS))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function show($id)
    {
        $sended = EmailSender::join('maillists', 'maillists.id', '=', 'email_sender.maillist_id')
            ->join('mail_templates', 'mail_templates.id', '=', 'email_sender.mailtemplate_id')
            ->select('email_sender.*','email_sender.id as email_senderid', 'mail_templates.*','maillists.*')
            ->where('email_sender.id',$id)->first();
        return view('HeraldistPanel::mail_service.mailchimp.email_sender.show', compact('sended'));
    }

    public function delete($id)
    {
        if(!userAction('message.email_sender.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = EmailSender::find($id);

        if ($data){
            $data->delete();
        }

        return redirect(route('Heraldist.email_senders.sendeds'))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function campaignContent($id)
    {
        $campaingid = $id;
        $campaign = curlGenerator($this->apiUrl. self::CAMPAIGNS .$id."/content",$this->apiKey,"GET");
        return view('HeraldistPanel::mail_service.mailchimp.email_sender.campaigncontent', compact('campaign','campaingid'));
    }

    public function campaignSend($id)
    {
        //curlGenerator($this->apiUrl."templates/".$id."/content",$this->apiKey,"PUT", $json);
        curlGenerator($this->apiUrl. self::CAMPAIGNS .$id."/actions/send",$this->apiKey,"POST");
        return redirect(route(self::HERALDIST_MAILCHIMP_EMAIL_SENDERS_CAMPAIGNS))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

}
