<?php

return [
    'modal_title' => "BreadCrumb Setting",
    'add_homepage' => "Add Homepage to the breadcrumb links? (<b>Home > Corporate</b>)",
    'add_domain' => "Do you get a domain name at the beginning of breadcrumb urls? (<b>domain.com/history</b> | <b>/history</b>)",
    'menu_slug' => "Contents from which menu to read?",
    'category_depth' => "In categorized structures, how many level categories should be seen from left to right? (Except pre-category levels. When 0 is written, the top category is entered, and when 1 is written, the top and one below.)",

    'string_type' => [
        'title' => "How should the contents look? <br><b>Corporate > Who We Are</b><br><b>Corporate > Who we are</b><br><b>CORPORATE > WHO WE ARE</b><br><b>corporate > who we are</b>",
        'all_word_first_char' => "Only the first letters are uppercase",
        'first_word_first_char' => "Only the first letter of the first words are uppercase",
        'all_word_all_char' => "All letters are uppercase",
        'all_word_no_char' => "All letters are lowercase",
    ]
];
