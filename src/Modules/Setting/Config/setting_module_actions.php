<?php

return [
    "setting" => [
        'name' => 'SettingPanel::auth.sections.setting_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update",
            ],
        ],
        'subs' => [
        ]
    ]
];
