<?php


namespace Mediapress\Modules\Interaction\Controllers;


use Illuminate\Http\Request;
use Mediapress\Modules\Interaction\Repositories\GoogleRepository;
use Mediapress\Modules\MPCore\Traits\GoogleAnalyticsTrait;
use Illuminate\View\View;

class DashboardController
{

    use GoogleAnalyticsTrait;

    /**
     * @param Request $request
     * @return object
     */
    public function index(Request $request): View
    {

        /* if (!permissionCheck(session("panel.user"), ["dashboard.index"])) {
             return rejectResponse();
         }*/

        $time = 30;
        if ($request->days || $request->days === '0') {
            $time = (int)$request->days;
        }

        //  $data = DashboardSlot::data($time);

        return view("InteractionPanel::dashboard.index", compact('time'));
    }


    /**
     * @param Request $request
     * @return object|null
     */
    public function ajax(Request $request): ?View
    {
        $whitelist = [
            'alert',
            'stats',
            'online',
            'countries',
            'platforms',
            'ages',
            'browsers',
            'urls'
        ];

        $func = $request->get('func');

        if ($func && in_array($func, $whitelist)) {

            $repoName = $this->checkRepository();

            $time = (int)$request->get('time') ?? 30;

            $class = '\\Mediapress\\Modules\\Interaction\\Repositories\\' . $repoName;

            /** @var GoogleRepository $class */
            $repo = new $class($time, $this);

            $data = $repo->$func();

            if ($data) {
                return view("InteractionPanel::dashboard." . $func, compact('data', 'repoName'));
            }
        }
        return null;
    }


    /**
     * @return string
     */
    private function checkRepository() : string
    {
        $google = $this->analyticsInit();

        if ($google) {
            $repo = 'GoogleRepository';
        } else {
            $repo = 'InteractionRepository';
        }

        return $repo;


    }

}
