<?php

return [
    'user-name' => 'Member Name',
    'model-name' => 'Model',
    'user-selection' => 'Member Selection',
    'user-update' => 'Update Member',
    'user-detail' => 'Member Detail',
    'userlist-add' => 'Add Member to List',
];
