<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class CountryGroupCountry extends Model
{
    use CacheQueryBuilder;
    protected $table = "country_group_country";

    public $timestamps = false;

    protected $fillable = ["country_code", "country_group_id"];

    //
}
