# Geliştirici Notları

* Migrasyonlarda `admin_id` ve `status` alanlarının otomatik eklenmesi için Blueprint class'ı `\Modules\MPCore\Foundation\Blueprint` yolunda genişletildi. Extend eden etmeyen migrasyonlara dikkat edilecek, elden geçirilecek.
* Uygulama tarafında aranacak dizin/dosya yapısı standartlaştırılacak.

* MODÜLER YAPIYI SAĞLAMAK İÇİN: URL Tablosunun MPCore yönetimine bırakılıp, module_name diye alan eklenip, istek geldiğinde ilgili modüle işlenmek üzere URL modelinin gönderilmesi sağlanır. Website ID vb. alanlar, modüllerin kendi oluşturacakları pivot tablolarda tutulur.
 
* Metaları controller tarafında değiştirmek için `mediapress("metas")->setTemp("og:fast", "test");` Paginate metalarını eklemek için ise `mediapress("metas")->paginate(Meta::paginate());`
 
* mediapress'e `mediapress()` ile erişebilir. `mediapress("relation")` veya `mediapress()->relation` diyerek çağırabilir. `mediapress("veri","veri")` veya `mediapress()->veri = "veri"` diyerek veri tutabilirsiniz

* Contorllerlar için Contollerlar `Mediapress\Http\Controllers\BaseController` türetilmelidir.

* Controllerlar ön yüzde `app/Modules/Content/Http/Controllers/Web/{SitemapType}` şeklinde oluşturulmalıdır. 

* Panel tarafında sitemap'e özgü controller yazmak için `app/Modules/Content/Http/Controllers/Panel/{PageController|SitemapController|...}` kullanılmalıdır

`APP_URL` env içinden düzenlenmesi gerekir ilk seeder website ı buna göre oluşturuyor ve sitedeki uploads linki de buna göre oluşuyor
`FILESYSTEM_PUBLIC_UPLOADS_DEFAULT` env den dosyaların yüklenecek filesystem keyini vererek seçebilirsiniz normali uploads dır mediapress tarafından dinamik olarak kullanılır,
arama motorlarında çıkmaması gereken dosyalar `FILESYSTEM_SECURE_UPLOADS_DEFAULT` ile belirlenen diskte tutulacaktır.
    
* `user_modify_panelmenu(array &panel_menu)` Panel menusünün basılmadan önce kullanıcı tarafından düzenlenmesini sağlar `app/Http/helpers.php` içinden
* Ön yüzden panel view 'i ezebilmemiz için gerekli view bileşenleri
```
@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
     ...
@endsection

@push("styles")
    ...
@endpush

@push("scripts")
    ...
@endpush

@push("specific.styles")
    ...
@endpush

@push("specific.scripts")
    ...
@endpush
```

* `Mediapress\Modules\Heraldist\Models\Form` modelinden `$form->source` olarak veri çekildiğinde DataSource ları array içine gömüyor

* Datasourcelar `App\DataSources\{isim}` adında sınıf oluşturup kullanılabiliyor. `Mediapress\Foundation\DataSource` dan extend edilmiş olması gerekiyor ve `getData` methodu ile geriye veri döndürülüyor.

* Şifrelenmiş sayfalar için ön yüzde `inc.urlAuth` blade'i gerekiyor ve blade içeriği aşağıdadır
```$xslt
{{ Form::open(['route' => 'web.url.auth']) }}
{!!Form::password('password')!!}
{!!Form::hidden('next')!!}
{!!Form::submit('Gir')!!}
{{ Form::close() }}
```


* Status Kodları -> `PASSIVE = 0, ACTIVE = 1, DRAFT = 2, PREDRAFT = 3, PENDING = 4, POSTDATE = 5`

* `ALLOW_POSTDATE_CONTENT=true` İleri tarihte yayınlanacak içeriğe izin verir varsayılan olarak açıktır, kapatmak için false yapılmalı

* Pages için 
  * Storer bakılacak yollar:
    ```   
          0 => "\App\Modules\Content\AllBuilder\Storers\<sitemap_type_name>\Page_<page_id>"
          1 => "\App\Modules\Content\AllBuilder\Storers\<sitemap_type_name>\Pages"
          2 => "\App\Modules\Content\AllBuilder\Storers\PagesStorer"
          3 => "\Mediapress\Modules\Content\AllBuilder\Storers\PagesStorer"
    ```
    ```
            $page_specific = "Page_" . $page->id;
            $sitemap_specific = $page->sitemap->sitemapType->name;
    
    
            $storer_class = AllBuilderEngine::getStorerClassPath([$page_specific, "Pages"], "Content", "App", $sitemap_specific);
            if (!$storer_class[0]) {
                $storer_class_2 = AllBuilderEngine::getStorerClassPath(["PagesStorer"], "Content", ["App","Mediapress"], null);
                if (!$storer_class_2[0]) {
                    return "<strong><code>" . implode("\n<br/>", array_merge($storer_class[1], $storer_class_2[1])) . "</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
                } else {
                    $storer_class = $storer_class_2[1];
                }
            } else {
                $storer_class = $storer_class[1];
            }
    
            return $storer_class;
    ```
  * Renderable bakılacak yollar
    ```
      0 => "\App\Modules\Content\AllBuilder\Renderables\<sitemap_type_name>\Page_<page_id>"
      1 => "\App\Modules\Content\AllBuilder\Renderables\<sitemap_type_name>\Pages"
    ```
    ```
    
        if($for_existing_page){
            $page = is_a($page_or_page_id, Page::class) ? $page_or_page_id : Page::find($page_or_page_id);
            if (!$page) {
                throw new InvalidArgumentException("Page bulunamadı: $page_or_page_id");
            }
            $page_specific = "Page_" . $page->id;
            $sitemap_specific = $page->sitemap->sitemapType->name;
        }else{
            $page_specific = null;
            $sitemap_specific = $inexistent_page_sitemap_type;
        }

        $renderable_class = AllBuilderEngine::getBuilderClassPath(array_filter([$page_specific, "Pages"]), "Content", "App", $sitemap_specific);
        if (!isset($renderable_class[0]) || !$renderable_class[0]) {
            throw new PageRenderableNotFoundException(/*"<strong><code>" . */
                implode("\n"/*."<br/>"*/, $renderable_class[1]) . /*"</code></strong>\n<br/>n<br/>".*/
                " denemeleri başarısız oldu.",$renderable_class[1][0]);
        } else {
            $renderable_class = $renderable_class[1];
        }
        return $renderable_class;
    ```
* Sitemap içeriği için
    * Storer bakılacak yollar
    ```
      0 => "\App\Modules\Content\AllBuilder\Storers\<sitemap_type_name>\SitemapDetails"
      1 => "\App\Modules\Content\AllBuilder\Storers\SitemapDetailsStorer"
      2 => "\Mediapress\Modules\Content\AllBuilder\Storers\SitemapDetailsStorer"
    ```
    ```
            $sitemap_specific = $sitemap->sitemapType->name;
    
    
            $storer_class = AllBuilderEngine::getStorerClassPath(["SitemapDetails"], "Content", "App", $sitemap_specific, null);
            if (!$storer_class[0]) {
                $storer_class_2 = AllBuilderEngine::getStorerClassPath(["SitemapDetailsStorer"], "Content", ["App", "Mediapress"], null);
                if (!$storer_class_2[0]) {
                    return "<strong><code>" . implode("\n<br/>", array_merge($storer_class[1], $storer_class_2[1])) . "</code></strong>\n<br/>\n<br/> denemeleri başarısız oldu.";
                } else {
                    $storer_class = $storer_class_2[1];
                }
            } else {
                $storer_class = $storer_class[1];
            }
    
            return $storer_class;
    ```
    * Renderable bakılacak yollar
    ```
    0 => "\App\Modules\Content\AllBuilder\Renderables\SearchPage\SitemapDetails"
    ```
    ```
    
        $sitemap_specific = $sitemap->sitemapType->name;

        $renderable_class = AllBuilderEngine::getBuilderClassPath(["SitemapDetails"], "Content", "App", $sitemap_specific);
        if (!isset($renderable_class[0]) || !$renderable_class[0]) {
            throw new SitemapRenderableNotFoundException(/*"<strong><code>" . */
                implode("\n"/*."<br/>"*/, $renderable_class[1]) . /*"</code></strong>\n<br/>n<br/>".*/
                " denemeleri başarısız oldu.", $renderable_class[1]);
        } else {
            $renderable_class = $renderable_class[1];
        }
        return $renderable_class;
    ```