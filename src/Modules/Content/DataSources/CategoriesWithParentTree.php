<?php
namespace Mediapress\Modules\Content\DataSources;

use Mediapress\Foundation\DataSource;
use Mediapress\Modules\Content\Models\Category;

class CategoriesWithParentTree extends DataSource
{
    public function getData()
    {
        $category_ids = $this->params["category_ids"] ?? [];
        $results = [];
        foreach($category_ids as $category_id){
            $category  = objectifyOneParam(Category::class,$category_id);//is_a($category_id,Category::class) ? $category_id : Category::find($category_id);
            if(!$category){
                continue;
            }
            $parent_ids = Category::select("id")->where('lft','<',$category->lft)->where('rgt','>',$category->rgt)->get()->pluck('id')->toArray();
            $results[$category->id]=$parent_ids;
        }
        
        return $results;
       
    }
}
