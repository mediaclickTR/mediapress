@if(isset($field))

    <table>
        <tr>


            <td width="40%">
                @include('HeraldistWeb::forms.email_parts.label',['field'=>$field])
            </td>
            <td width="60%" {!! $field->attributes(['class'=>'form-control']) !!}>
                @if(isset($sended_message->data[$field->name]))
                {!! $sended_message->data[$field->name]['value'] !!}
                @endif
            </td>
            {{--  @if(in_array($field->attrs['type'],['radio','checkbox']))
                  <div id="{!! $field->id !!}" {!! $field->attributes(['class'=>'form-control']) !!}>{!! $message->data[$field->name]['value'] !!}</div>
              @else
                  <div id="{!! $field->id !!}" {!! $field->attributes(['class'=>'form-control']) !!}>{!! $message->data[$field->name]['value'] !!}</div>

              @endif
              --}}


        </tr>
    </table>


@endif