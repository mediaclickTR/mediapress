<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSlidersTable extends Migration
{

    public const SLIDERS = 'sliders';
    public const SCENES = 'scenes';
    public const SCENE_DETAILS = 'scene_details';

    public function up()
    {
        Schema::dropIfExists(self::SLIDERS);
        Schema::dropIfExists(self::SCENES);
        Schema::dropIfExists(self::SCENE_DETAILS);
        if (!Schema::hasTable(self::SLIDERS)) {
            Schema::create(self::SLIDERS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer("website_id")->unsigned();
                $table->string('name')->nullable();
                $table->string('type')->default('image');
                $table->integer('text')->default(0);
                $table->integer('button')->default(0);
                $table->integer('screen')->default(1);
                $table->integer('width')->nullable();
                $table->integer('height')->nullable();
                $table->integer('admin_id')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable(self::SCENES)) {
            Schema::create(self::SCENES, function (Blueprint $table) {
                $table->increments('id');
                $table->integer("slider_id")->unsigned();
                $table->integer("time")->nullable();
                $table->integer("order")->default(1);
                $table->tinyInteger("status")->default(1);
                $table->tinyInteger("admin_id")->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable(self::SCENE_DETAILS)) {
            Schema::create(self::SCENE_DETAILS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer("scene_id")->unsigned();
                $table->integer("language_id")->nullable();
                $table->text("texts")->nullable();
                $table->text("buttons")->nullable();
                $table->text("files")->nullable();
                $table->text("url")->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists(self::SLIDERS);
        Schema::dropIfExists(self::SCENES);
        Schema::dropIfExists(self::SCENE_DETAILS);
    }
}
