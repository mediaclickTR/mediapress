<?php

namespace Mediapress\Modules\Content\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\LanguagePart;
use Illuminate\Support\Collection;

class PagesImport implements ToCollection
{

    public function __construct(int $countryGroupId, int $languageId)
    {
        $this->countryGroupId = $countryGroupId;
        $this->languageId = $languageId;
    }

    public function collection(Collection $rows)
    {
        $rows = $rows->toArray();

        unset($rows[0]);

        foreach ($rows as $row) {
            PageDetail::updateOrCreate(
                [
                    'page_id' => $row[0],
                    'country_group_id' => $this->countryGroupId,
                    'language_id' => $this->languageId,
                ],
                [
                    'name' => $row[1],
                    'detail' => $row[2]
                ]
            );
        }
    }
}
