<?php

namespace Mediapress\Foundation;

use Config;
use Cache;
use Cviebrock\LaravelElasticsearch\Facade as Search;
use Illuminate\Database\Eloquent\Collection;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Models\Url;


class JsonSearch
{
    public const INDEX = 'index';
    public const SCHEME = 'scheme';
    public const TOTAL = 'total';

    public $active = false;

    public function __construct()
    {
        if (config('mediapress.elasticsearch_host')) {
            $this->active = true;
        }
    }

    public function index()
    {
        Cache::remember(cache_key('search.index'), 24 * 60, function () {

            $urls = Url::with('model.extras', 'website')
                    ->where('model_type', PageDetail::class)
                    ->where('type', 'original')
                    ->groupBy('model_type')
                    ->groupBy('model_id')
                    ->get();

            $params = [
                self::INDEX => $urls[0]->website->slug
            ];

            $config = Config::get('elasticsearch')['connections']['default']['hosts'][0];

            $scheme = 'http';
            if ($config[SCHEME]) {
                $scheme = $config[SCHEME];
            }

            $get = @file_get_contents($scheme . '://' . $config['host'] . ':' . $config['port'] . '/' . $urls[0]->website->slug);

            if ($get) {
                Search::indices()->delete($params);
            }
            foreach ($urls as $url)
            {
                if ($url->website && $url->model) {
                    $model = get_class($url->model);
                    $data = [
                        self::INDEX => $url->website->slug,
                        'type' => 'url',
                        'id' => $url->id,
                        'body' => [
                            'language' => $url->model->language_id,
                            'content' => json_encode($url->model->toArray()),
                            'id' => $url->model->id,
                            'model' => $model
                        ]
                    ];
                    Search::index($data);
                }

            }
            return true;
        });

    }

    public function search(Mediapress $mediapress, $query)
    {
        $active_language = $mediapress->activeLanguage;
        $website = $mediapress->website;

        $config = Config::get('elasticsearch')['connections']['default']['hosts'][0];

        $scheme = 'http';

        if ($config[self::SCHEME]) {
            $scheme = $config[self::SCHEME];
        }

        $get = @file_get_contents($scheme . '://' . $config['host'] . ':' . $config['port'] . '/' . $website->slug);

        if(!$get){
            return false;
        }
        $data = [
            'body' => [
                'min_score' => 1.01,
                'from' => 0,
                'size' => 50,
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => [
                                'language' => $active_language->id
                            ],
                        ],
                        'should' => [
                            'fuzzy' => [
                                'content' => strtolower($query),
                            ],
                        ]
                    ]

                ]
            ],
            self::INDEX => $website->slug,
            'type' => 'url',

        ];

        $results = Search::search($data);
        $total = $results['hits'][self::TOTAL];
        $hits = $results['hits']['hits'];
        $collection = [];

        foreach ($hits as $hit) {
            $model = $hit['_source']['model']::with('parent.extras', 'extras')->find($hit['_source']['id']);
            if ($model instanceof PageDetail) {
                $collection[] = $model->page;
            } elseif ($model instanceof CategoryDetail) {
                $collection[] = $model->category;
            } elseif ($model instanceof SitemapDetail) {
                $collection[] = $model->sitemap;
            }

        }

        $mediapress->data['results'] = new Collection([self::TOTAL => $total, 'items' => $collection]);

        // Metas
        $this->mediapress->search = new Collection([self::TOTAL => $total, 'items' => $collection]);
        $results = $this->mediapress;

        // TODO :: düzenlenecek
        return view("web.search.results", compact('results'));
    }

}
