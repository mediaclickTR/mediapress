<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 15.11.2018
     * Time: 09:59
     */

    namespace Mediapress\Modules\MPCore\DataSources;


    use Mediapress\Foundation\DataSource;
    use Mediapress\Modules\MPCore\Facades\CountryGroups as CGF;

    class CountryGroupOwnerTypesDistinctDS extends DataSource
    {
        public  function getData(){
            $owner_types_only = $this->params["ownerTypesOnly"] ?? [];
                return CGF::getDistinctOwnerTypes($owner_types_only);
        }
    }