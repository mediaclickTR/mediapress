<div class="modal fade" id="google2FaRegisterModal" tabindex="-1" role="dialog" data-backdrop="static"
     aria-labelledby="google2FaRegisterModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ __('MPCorePanel::google2fa.register.title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12 mb-5 mt-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="allNewBox">
                                <span>1</span>
                                <p>
                                    {{ __('MPCorePanel::google2fa.register.description_1') }}
                                </p>
                                <div class="miniListing">
                                    <a href="https://apps.apple.com/tr/app/google-authenticator/id388497605"
                                       target="_blank">
                                        <img src="{{ asset('vendor/mediapress/images/app-store.png') }}" width="125"
                                             alt="App Store">
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=tr"
                                       target="_blank">
                                        <img src="{{ asset('vendor/mediapress/images/play-store.png') }}"
                                             width="125"
                                             alt="Play Store">
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="allNewBox">
                                <span>2</span>
                                <p>
                                    {{ __('MPCorePanel::google2fa.register.description_2') }}
                                </p>
                                <img src="" id="qrCode">
                                <div class="secret">
                                    <small>{{ __('MPCorePanel::google2fa.register.secret_key') }}</small>
                                    <div class="fBox">
                                        <input type="text" id="secretKey" class="form-control">
                                        <button class="copy" id="txtDownload"><i class="fal fa-download"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="allNewBox">
                                <span>3</span>
                                <p>
                                    {{ __('MPCorePanel::google2fa.register.description_3') }}
                                </p>
                                <div class="formBox">
                                    <div class="form-group">
                                        <input type="text" name="code" class="form-control">
                                        <small class="d-none text-danger">{{ __('MPCorePanel::google2fa.code_success') }}</small>
                                        <small class="d-none text-success">{{ __('MPCorePanel::google2fa.code_error') }}</small>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn-primary verifyBtn">{{ __('MPCorePanel::google2fa.verify_btn') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="google2FaVerifyModal" tabindex="-1" role="dialog" data-backdrop="static"
     aria-labelledby="google2FaVerifyModal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="close absoluteClose" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="col-md-12 mb-5 mt-5">
                    <div class="phoneModalPanel">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="pImg text-center">
                                    <img src="{{ asset('vendor/mediapress/images/modalPhone.png') }}"
                                         alt="Google Mobile">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center h-100">
                                    <div class="all pl-2 pr-2">
                                        <div class="text-center mTitle">
                                            {{ __('MPCorePanel::google2fa.verify.title') }}
                                        </div>
                                        <div class="form-group">
                                            <div class="row mRow">
                                                <input type="text" class="col" maxlength="1">
                                                <input type="text" class="col" maxlength="1">
                                                <input type="text" class="col" maxlength="1">
                                                <input type="text" class="col" maxlength="1">
                                                <input type="text" class="col" maxlength="1">
                                                <input type="text" class="col" maxlength="1">
                                            </div>
                                            <input type="hidden" name="code">
                                            <div class="text-center mt-1">
                                                <small class="d-none text-danger">{{ __('MPCorePanel::google2fa.code_success') }}</small>
                                                <small class="d-none text-success">{{ __('MPCorePanel::google2fa.code_error') }}</small>
                                            </div>
                                        </div>
                                        <div class="form-group submit">
                                            <button type="button" class="btn-primary verifyBtn">
                                                {{ __('MPCorePanel::google2fa.verify.login') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        @if(session('panel.user') && is_null(auth('admin')->user()->google2fa) && auth('admin')->user()->google2fa_warning == 1)
        var isPanel = true;
        $(document).ready(function () {
            setTimeout(function () {
                $('body .container-fluid > .page').prepend('' +
                    '<div class="alert alert-warning" id="google2faAlert" role="alert">\n' +
                    '    <p class="d-inline-block mt-1">\n' +
                    '         {!! __('MPCorePanel::google2fa.warning.title') !!}\n' +
                    '    </p>\n' +
                    '    <button class="btn btn-outline-warning float-right" id="google2faActivateBtn" style="padding: .275rem .75rem; color: #856404">\n' +
                    '        <strong>{{ __('MPCorePanel::google2fa.verify_btn') }}</strong>\n' +
                    '    </button>\n' +
                    '</div>'
                )
            }, 1500);
        });
        @else
        var isPanel = false;
        @endif
    </script>

    <script>
        var secretKey = "";

        $('body').delegate('#google2faActivateBtn', 'click', function () {
            $.ajax({
                'url': "{{ route('Google2fa.activate2FA') }}",
                'method': 'GET',
                success: function (response) {
                    if (response.status == true) {

                        if (response.registerStatus == true) {
                            $('#qrCode').attr('src', response.qrCodeUrl);
                            secretKey = response.secretKey;
                            $('#secretKey').val(secretKey);
                            $('#google2FaRegisterModal').modal('show');
                        }
                    } else {
                        $('#loginForm').submit();
                    }
                }
            });
        });

        $('#loginBtn').on('click', function (e) {
            e.preventDefault();

            var email = $('input[name="email"]').val();
            var password = $('input[name="password"]').val();

            checkAuth(email, password);
        })

        function checkAuth(email, password) {

            $.ajax({
                'url': "{{ route('Google2fa.verifyAuth') }}",
                'data': {'email': email, 'password': password, '_token': "{{ csrf_token() }}"},
                'method': 'POST',
                success: function (response) {

                    if (response.status == true && response.google2faRequired == true) {

                        if (response.isMC == true) {
                            $('#loginForm').submit();
                        } else {
                            secretKey = response.secretKey;

                            if (response.registerStatus == true) {
                                $('#qrCode').attr('src', response.qrCodeUrl);
                                $('#secretKey').val(secretKey);
                                $('#google2FaRegisterModal').modal('show');
                            } else {
                                $('#google2FaVerifyModal').modal('show');
                            }

                        }

                    } else {
                        $('#loginForm').submit();
                    }
                }
            });
        }

        $('.verifyBtn').on('click', function () {
            var code = $('input[name="code"]').val();

            if (code) {
                $.ajax({
                    'url': "{{ route('Google2fa.verifyKey') }}",
                    'data': {'code': code, 'secretKey': secretKey, '_token': "{{ csrf_token() }}"},
                    'method': 'POST',
                    success: function (response) {

                        if (response.status == true) {
                            $('small.text-danger').addClass('d-none');
                            $('small.text-success').removeClass('d-none');

                            setTimeout(function () {
                                $('[id^="google2Fa"]').modal('hide');
                            }, 750)

                            if (isPanel == true) {
                                $('#google2faAlert').remove();
                            } else {
                                $('#loginForm').submit();
                            }

                        } else {
                            $('small.text-danger').removeClass('d-none');
                            $('.mRow input').val('');
                        }

                    }
                })
            }
        });

        $('#txtDownload').on('click', function () {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(secretKey));
            element.setAttribute('download', 'google2fa-secretKey.txt');

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        });

        $('.mRow input').keypress(function (e) {
            if ($(this).val().length == 1) {

                if($(this).next().val() != undefined && $(this).next().val().length == 0) {
                    $(this).next().val(e.key);
                }
                $(this).next().focus();
            }
        });

        $('.mRow input').keyup(function (e) {
            var tempCode = "";
            $.each($('.mRow input'), function (k, v) {
                tempCode += $(v).val()
            });
            $('input[name="code"]').val(tempCode);
        });
    </script>
@endpush
