<?php

namespace Mediapress\Modules\RenderableEditor\Facades;

use Illuminate\Support\Facades\Facade;

class RenderableEditor extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\RenderableEditor\RenderableEditor::class;
    }
}