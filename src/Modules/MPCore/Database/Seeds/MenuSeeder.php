<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert(array(
            array('website_id' => 1, 'slug' => 'header-menu', 'name' => 'Header Menu', 'status' => 1, 'admin_id' => 1),
            array('website_id' => 1, 'slug' => 'footer-menu', 'name' => 'Footer Menu', 'status' => 1, 'admin_id' => 1)
        ));
    }
}
