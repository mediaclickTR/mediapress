<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 14.11.2018
     * Time: 11:07
     */

    namespace Mediapress\Modules\MPCore\Foundation;


    use Mediapress\Modules\MPCore\Models\CountryGroupCountry;
    use Mediapress\Modules\MPCore\Models\CountryGroup;
    use Mediapress\Modules\MPCore\Models\Country;
    use Illuminate\Database\Eloquent\Collection as EloqCollection;
    use Illuminate\Support\Facades\DB;

    class CountryGroupsEngine
    {

        const OWNER_TYPE_STR = "owner_type";
        const OWNER_ID_STR = "owner_id";
        const COUNTRY_CODE_STR = "country_code";


        public function getGroupsOf($object_cls_and_id_arr_or_object, $limit = null)
        {
            $cls = null;
            $id = null;
            if (is_array($object_cls_and_id_arr_or_object)) {
                list($cls, $id) = $object_cls_and_id_arr_or_object;
            } elseif (is_object($object_cls_and_id_arr_or_object)) {
                $cls = get_class($object_cls_and_id_arr_or_object);
                $id = isset($object_cls_and_id_arr_or_object->id) ? $object_cls_and_id_arr_or_object->id : null;
            }
            $results = new EloqCollection();
            if ($cls) {
                $results = CountryGroup::where(self::OWNER_TYPE_STR, $cls);
                if ($id) {
                    $results = $results->where(self::OWNER_ID_STR, $id);
                }
                if ($limit) {
                    $results = $results->take($limit);
                } else {
                    $results = $results->get();
                }
            }
            return $results;
        }

        public function getUniqueCountriesOfGroups(){

        }

        public function getUngroupedCountriesOfOwner($object_cls_and_id_arr_or_object){
            $cls = null;
            $id = null;
            if (is_array($object_cls_and_id_arr_or_object)) {
                list($cls, $id) = $object_cls_and_id_arr_or_object;
            } elseif (is_object($object_cls_and_id_arr_or_object)) {
                $cls = get_class($object_cls_and_id_arr_or_object);
                $id = isset($object_cls_and_id_arr_or_object->id) ? $object_cls_and_id_arr_or_object->id : null;
            }

            $results = new EloqCollection();
            if ($cls && $id) {
                $groups_ids = CountryGroup::where(self::OWNER_TYPE_STR,$cls)->where(self::OWNER_ID_STR,$id)->select("id")->get()->pluck("id")->toArray();
                $intersect = DB::table("country_group_country")->whereIn('country_group_id',$groups_ids)->distinct()->select(self::COUNTRY_CODE_STR)->pluck(self::COUNTRY_CODE_STR)->toArray();
                $results = Country::whereNotIn("code",$intersect)->get();

            }
            return $results;
        }

        public function getCountriesOfGroup($group_or_id){

            $group_or_id_is_obj = is_object($group_or_id) ? $group_or_id : null;

            $group = is_numeric($group_or_id) ? CountryGroup::whereId($group_or_id)->with("countries")->first() : $group_or_id_is_obj;
            unset($group_or_id);
            $results = new EloqCollection();
            if ($group) {
                $results=$group->countries()->get();
            }

            return $results;
        }

        public function setCountriesOfGroup($group_or_id, array $country_ids, $syncWithoutDetaching = false)
        {

            $group_or_id_is_obj = is_object($group_or_id) ? $group_or_id : null;
            $group = is_numeric($group_or_id) ? CountryGroup::find($group_or_id) : $group_or_id_is_obj;
            unset($group_or_id);
            if (is_null($group)) {
                return false;
            }
            if ($syncWithoutDetaching) {
                $result = $group->countries()->syncWithoutDetaching($country_ids);
            } else {
                $result = $group->countries()->sync($country_ids);
            }

            return $result;
        }


        public function emptyGroups($groups_andor_ids)
        {
            $groups_andor_ids = is_countable($groups_andor_ids) ? $groups_andor_ids : [$groups_andor_ids];

            foreach ($groups_andor_ids as $item) {
                if(is_numeric($item)){
                    $this->emptyGroupById($item);
                }elseif(is_object($item)){
                    $this->emptyGroupByObject($item);
                }
            }
        }

        public function detachCountriesFromAllGroups(array $countryCodes)
        {
            return CountryGroupCountry::whereIn(self::COUNTRY_CODE_STR, $countryCodes)->delete();
        }

        public function detachCountriesFromGroups(array $countryCodes, array $groupIds)
        {
            return CountryGroupCountry::whereIn(self::COUNTRY_CODE_STR, $countryCodes)->whereIn("country_group_id", $groupIds)->delete();
        }

        public function getDistinctOwnerTypes(array $allowed_types=[]){
            return DB::table('country_groups')
                ->distinct()
                ->select(self::OWNER_TYPE_STR)
                ->whereNotIn(self::OWNER_TYPE_STR,$allowed_types)
                ->get();
        }

        public function getOwnersList(array $only_owner_types=[]){

            return DB::table('country_groups')
                ->distinct()
                ->select(self::OWNER_TYPE_STR, self::OWNER_ID_STR)
                ->whereNotIn(self::OWNER_TYPE_STR,$only_owner_types)
                ->get();
        }

        public function getGroupsByOwnerType(array $owner_types=[]){
            return CountryGroup::whereIn(self::OWNER_TYPE_STR,$owner_types)->get();
        }




        //region Private Methods
        private function emptyGroupById($group_id)
        {
            return CountryGroupCountry::whereCountryGroupId($group_id)->delete();
        }

        private function emptyGroupByObject(CountryGroup $group)
        {
            return $group->sync([]);
        }
        //endregion

        public function getCountryGroup($id)
        {
            return CountryGroup::find($id)->whereHas('websites', function($query) use ($id) {
                    $query->where('id', $id);
            });
        }
    }