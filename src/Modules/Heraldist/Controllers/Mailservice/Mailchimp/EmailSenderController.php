<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Cache;
use Mediapress\Modules\MPCore\Facades\MPCore;

class EmailSenderController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const HERALDIST_MAILCHIMP_EMAILSENDER_INDEX = 'Heraldist.mailchimp.emailsender.index';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const CAMPAIGNS = "campaigns/";
    public const TITLE = 'title';
    public const MAILLIST_ID = 'maillist_id';
    public const TEMPLATE_ID = 'template_id';
    protected $apiKey;
    protected  $apiUrl;

    public function __construct()
    {

        $mailChimpApiKey = settingSun('heraldist.mailchimp.apikey');
        // API KEY KONTROL
        if($mailChimpApiKey){
            $this->apiKey = $mailChimpApiKey;
        }
        else {
            //dd("Mailchimp api key bulunamadı.2");
            return null;
        }

        // APi URL
        $dataCenter = substr($this->apiKey,strpos($this->apiKey,'-')+1);
        $this->apiUrl = "https://".$dataCenter.".api.mailchimp.com/3.0/";
    }

    public function index()
    {
        if(!userAction('message.emailsender.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $emailsenders = curlGenerator($this->apiUrl."campaigns",$this->apiKey,"GET");
        $total=$emailsenders->total_items;
        $emailsenders=$emailsenders->campaigns;
        return view('HeraldistPanel::mail_service.mailchimp.emailsender.index', compact('emailsenders','total'));
    }

    public function show($id)
    {
        return view('HeraldistPanel::mail_service.mailchimp.emailsender.show', compact('sended'));
    }

    public function create()
    {
        if(!userAction('message.emailsender.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }


        $maillist_all = curlGenerator($this->apiUrl."lists",$this->apiKey,"GET")->lists;
        $mailtemplates = curlGenerator($this->apiUrl."templates",$this->apiKey,"GET")->templates;
        return view('HeraldistPanel::mail_service.mailchimp.emailsender.create',compact('maillist_all','mailtemplates'));
    }

    public function edit($id)
    {
        if(!userAction('message.emailsender.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $maillist_all = curlGenerator($this->apiUrl."lists",$this->apiKey,"GET")->lists;
        $mailtemplates = curlGenerator($this->apiUrl."templates",$this->apiKey,"GET")->templates;
        //curl edit
        $emailsender = curlGenerator($this->apiUrl. self::CAMPAIGNS .$id,$this->apiKey,"GET");
        return view('HeraldistPanel::mail_service.mailchimp.emailsender.edit', compact('maillist_all','mailtemplates','emailsender'));
    }


    public function delete($id)
    {
        if(!userAction('message.emailsender.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        //Curl delete
        curlGenerator($this->apiUrl. self::CAMPAIGNS .$id,$this->apiKey,"DELETE");

        return redirect(route(self::HERALDIST_MAILCHIMP_EMAILSENDER_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function store(Request $request)
    {

        $fields = [
            self::TITLE => trans("HeraldistPanel::mail_service.emailsender_name"),
            self::MAILLIST_ID => trans("HeraldistPanel::mail_service.email_select"),
            self::TEMPLATE_ID => trans("HeraldistPanel::mail_service.template_select"),
        ];

        $rules = [
            self::TITLE => 'required|max:255|regex:/^[A-Za-z\şŞıİçÇöÖüÜĞğ\s]+$/',
            self::MAILLIST_ID => 'required',
            self::TEMPLATE_ID => 'required',
        ];

        $messages = [
            'title.required' => 'E-Posta gönderim adı boş bırakılmamalıdır.',
            'maillist_id.required' => 'E-Posta Liste adı boş bırakılmamalıdır.',
            'template_id.required' => 'Şablon boş bırakılmamalıdır.',
        ];

        $this->validate($request, $rules, $messages, $fields);

        $maillist_id = $request->maillist_id;
        $template_id = intval($request->template_id);

        $maillist = curlGenerator($this->apiUrl."lists/".$maillist_id,$this->apiKey,"GET")->campaign_defaults;

        $json = json_encode([
            'recipients'  => [
                'list_id' => $maillist_id,
            ],
            'type' => 'regular',
            'content_type' => 'html',
            'settings'  => [
                self::TITLE => $request->title,
                'subject_line' => $maillist->subject,
                'reply_to' =>$maillist->from_email,
                'from_name' => $maillist->from_name,
                self::TEMPLATE_ID => $template_id
            ]
        ]);

        curlGenerator($this->apiUrl. self::CAMPAIGNS,$this->apiKey,null,$json,"POST");

        return redirect(route(self::HERALDIST_MAILCHIMP_EMAILSENDER_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));

    }

    public function emailContent($id)
    {
        $campaingid = $id;
        $campaign = curlGenerator($this->apiUrl. self::CAMPAIGNS .$id."/content",$this->apiKey,"GET");
        return view('HeraldistPanel::mail_service.mailchimp.emailsender.campaigncontent', compact('campaign','campaingid'));
    }

    public function emailSend($id,Request $request)
    {
        curlGenerator($this->apiUrl. self::CAMPAIGNS .$id."/actions/send",$this->apiKey,"POST");
        return redirect(route(self::HERALDIST_MAILCHIMP_EMAILSENDER_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
