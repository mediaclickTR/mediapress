<?php

namespace Mediapress\Modules\Interaction\Repositories;

/**
 * Interface RepositoryInterface
 * @package Mediapress\Modules\Interaction\Repositories
 */
interface RepositoryInterface
{
    /**
     * @return bool
     */
    public function alert(): bool;

    /**
     * @return array
     */
    public function stats(): array;

    /**
     * @return array
     */
    public function online(): array;

    /**
     * @return mixed
     */
    public function countries();

    /**
     * @return array
     */
    public function platforms(): array;

    /**
     * @return mixed
     */
    public function ages();

    /**
     * @return array
     */
    public function browsers(): array;

    /**
     * @return array
     */
    public function urls(): array;
}
