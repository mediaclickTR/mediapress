<?php

namespace Mediapress\Modules\Flow\Controllers;


use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Flow\Models\Config;
use Mediapress\Modules\Flow\Models\Hsr;
use Mediapress\Modules\Flow\Models\Track;
use Symfony\Component\Finder\SplFileInfo;

class FlowWebController extends Controller
{

    public const CONTENT_TYPE = 'Content-Type';
    public $api = 'https://editor.mclck.com/';
    public const BREAKER = '/*EDITOR*/';
    public const CONTENTS = 'contents';
    public const QUOT = '&quot;';
    public const OPTIONS = 'options';
    public const PARAMS = 'params';

    public function matomo(Request $request)
    {
        if ($request->hsr_vid) {
            return $this->record($request);
        }
        Track::create($request->all());
        $file = file_get_contents(__DIR__ . '/../Files/matomo.gif');
        return response($file)->header(self::CONTENT_TYPE, 'image/gif');
    }

    public function recording(Request $request)
    {
        Config::create($request->all());
        return response('Piwik.HeatmapSessionRecording.configuration.assign({"heatmaps":[],"sessions":[{"id":1,"sample_rate":"10.0","min_time":0,"activity":true,"keystrokes":true}],"idsite":"1","trackerid":"' . $request->trackerid . '"});')->header(self::CONTENT_TYPE, 'application/javascript');
    }

    public function post(Request $request)
    {
        Hsr::create($request->all());
        return response()->json($request->all());
    }

    private function record(Request $request)
    {
        Hsr::create($request->all());
        $file = file_get_contents(__DIR__ . '/../Files/matomo.gif');
        return response($file)->header(self::CONTENT_TYPE, 'image/gif');
    }

}