@extends('GoogleTagManagerPanel::inc.module_main')
@section('content')
    <div class="page-content">

        @if(isset($data['error']))
            <div class="alert alert-light" style="border:#ccc 1px solid">
                {!! trans("GoogleTagManagerPanel::tagmanager.nodata") !!}

                <div class="clearfix"></div>
            </div>
        @else
            @if(!isset($data['container']))
                <div class="alert alert-light" style="border:#ccc 1px solid">
                    {!! trans("GoogleTagManagerPanel::tagmanager.connection_warning", ['type'=>'Google']) !!}
                    <a id="google_connect" style="color:#6c757d" class="float-right" href="#">
                        {!! trans("GoogleTagManagerPanel::tagmanager.google_connect") !!} <i class="fa fa-key"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="mt-5" id="accountList" style="display: none">
                    <h4>Kayıtlı Hesaplar</h4>
                    <table class="table mt-3">
                    </table>
                </div>
            @else
                <div class="title">{!! $data['container']['publicId'] !!}
                    <a href="https://tagmanager.google.com/#/container/accounts/{!! $data['container']['accountId'] !!}/containers/{!! $data['container']['containerId'] !!}/"
                       target="_blank"> <i class="fa fa-edit"></i></a>

                    <a href="{{ route('admin.GoogleTagManager.unconnect') }}" class="btn btn-sm btn-danger text-white float-right">Bağlantıyı Kopar</a>

                    <small class="text-muted d-block float-right mr-4">{!! $data['container']['name'] !!}</small>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <td>Etiket Adı</td>
                        <td>Etiket Türü</td>
                        <td>Etiketi Tetikleyen Tetikleyiciler</td>
                        <td>Son Düzenleme</td>
                        <td>Veriler</td>

                    </tr>
                    </thead>
                    @foreach($data['tags'] as $tag)

                        <tr>
                            <td> {!! $tag['name'] !!}</td>
                            <td> {!! $tag['type'] !!}</td>
                            <td>
                                {!! $tag['triggers'] !!}
                            </td>
                            <td> {!! $tag['time'] !!}</td>
                            <td>
                                <button type="button" class="btn poper btn-default" data-container="body"
                                        data-toggle="popover" data-placement="left"
                                        data-html="true"
                                        data-title="{!! $tag['name'] !!}"
                                        data-content="
<table class='table'>@foreach($tag['parameters'] as $key=>$param)
                                            <tr>
                                             <td>{!! $key !!}</td>
 <td>{!! htmlentities(str_replace(['<','>'],['&lt;','&gt;'],$param)) !!}</td>
 </tr>
@endforeach</table>


                                            ">
                                    Parametreler
                                </button>
                            </td>

                        </tr>
                    @endforeach
                </table>
            @endif

        @endif
    </div>
@endsection
@push ('styles')
    <style>
        table tbody tr td:nth-child(1) {
            width: inherit !important;
        }
        .popover{
            max-width: 800px;
        }
        .popover,.popover-body,.popover-body .table{
            background: #eee;
        }
    </style>
@endpush
@push('scripts')
    <script>

        $.oauthpopup = function (options) {
            options.windowName = options.windowName || 'ConnectWithOAuth'; // should not include space for IE
            var w = 450;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            options.windowOptions = options.windowOptions || 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left;
            options.callback = options.callback || function () {
                window.location.reload();
            };
            var that = this;
            that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
            that._oauthInterval = window.setInterval(function () {
                if (that._oauthWindow.data) {
                    responseDataPopup(that._oauthWindow.data);
                }
                if (that._oauthWindow.closed) {
                    window.clearInterval(that._oauthInterval);
                    options.callback();
                }
            }, 100);
        };

        $('#google_connect').on("click", function () {
            $.oauthpopup({
                windowName: "Mediapress Google Bağlantı Aracı",
                path: "{!! route("admin.GoogleTagManager.google.login") !!}",
                callback: function () {
                }
            });
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });

       /* $('.poper').click(function(){
            $('.popover').popover('hide');
        });*/

        function responseDataPopup(data) {
            if(data.accounts) {
                var content = '' +
                    '<thead>' +
                        '<tr>'+
                            '<td>Tag Manager Adı</td>'+
                            '<td></td>'+
                        '</tr>'
                    '</thead>';


                $.each(data.accounts, function (k,v ) {

                    content += '<tr>' +
                        '<td>' + v.name + '</td>' +
                        '<td><a href="{{ route('admin.GoogleTagManager.connect') }}?id='+v.accountId+'" class="btn btn-primary">Bağla</a></td>'+
                        '</tr>';
                });
                $('#accountList > .table').html(content);
                $('#accountList').show();
            }
        }
    </script>
@endpush

