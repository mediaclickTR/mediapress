<?php

namespace Mediapress\Modules\RenderableEditor\Controllers;


use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\AllBuilder\Foundation\Renderables\Div;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Content\Models\Page;
use Symfony\Component\Finder\SplFileInfo;

class AjaxEditorController extends Controller
{

    public const ITEMS = 'items';
    public const DEFAULT_VALUE = 'default_value';
    public const DESCRIPTION = 'description';
    private $tabs = [];

    public function ajaxGet($tag,Request $request)
    {
        $info = $request->all();
        unset($info['_token']);
        $this->info = $info;
        $renderable = new BuilderRenderable();
        $types = $renderable->getOption('renderable_types', []);

        if (!isset($types[$tag])) {
            return response()->json([]);
        }

        /** @var BuilderRenderable $builder */
        $builder = new $types[$tag]();

        $schema = $builder->getInfo();
        $this->mergeInfo($schema);

        $response = ['view' => $this->renderable($this->schema), 'json' => $this->schema];

        return response()->json($response);
    }

    public function renderable(array $item, $name = 'info', $render = null, $jsonItem = 'info')
    {

        $pool = '';
        if (isset($item[self::ITEMS])) {
            $parent = $name;

            foreach ($item[self::ITEMS] as $key => $value) {
                if ($name) {
                    $sendName = $parent . '[' . $key . ']';
                } else {
                    $sendName = $parent . $key;
                }
                $pool = $this->renderable($value, $sendName, $pool, $jsonItem);
            }
        }

        return $render.$this->compileRenderableItem($item, $name, $pool, $jsonItem);
    }

    public function compileRenderableItem(array $item, $name, $render, $jsonItem)
    {
        if ($item != [] && isset($item['type'])) {
            $render = $this->{'compile_' . $item['type']}($item, $name, $render, $jsonItem);
        } elseif ($item != [] && !isset($item['type']) && isset($this->tabs[md5($name)])) {
            $render .= $this->renderTabs($this->tabs[md5($name)],md5($name));
        }

        return $render;
    }

    public function compile_input_text($item, $name, $render, $jsonItem)
    {

        $tags = '';
        $token =  uniqid();
        if(strpos($name,'[options][tags]') !== false){
            $item[self::DEFAULT_VALUE] = str_replace('%quot%','"', $item[self::DEFAULT_VALUE]);
            $tags = '<a href="javascript:void(0);" onclick="javascript:editTag(\''.$token.'\')"><i class="fa fa-edit"></i></a> &nbsp;<a href="javascript:void(0);" onclick="javascript:remove($(this))"><i class="fa fa-trash"></i></a>';
        }
        return $render . '<div class="form-group row">
                <label class="col-sm-4 col-form-label">' . $item['key'] . '   <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button> </label>
                <div class="col-sm-6">
                  <input type="text" '.($tags ? 'readonly' :'').' name="' . $name . '" class="form-control" id="' . $token . '" value=\'' . $item[self::DEFAULT_VALUE] . '\'>
                </div>
                <div class="col-sm-2">
                '.$tags.'
</div>
              </div>';
    }

    public function compile_input_textarea($item, $name, $render, $jsonItem)
    {

        return $render . '<div class="form-group row">
                <label class="col-sm-4 col-form-label">' . $item['key'] . '    <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button> </label>
                <div class="col-sm-8">
                  <textarea  name="' . $name . '"  class="form-control" id="' . uniqid() . '" > ' . $item[self::DEFAULT_VALUE] . ' </textarea>
                </div>
              </div>';
    }

    public function compile_group($item, $name, $render, $jsonItem)
    {
        $token = uniqid();
        return '<div class="row html-group" data-name="'.$name.'"  id="' . $token . '"><div class="col-sm-12">' .
            '<div class="row group-title"><div class="col-sm-12"><h6>' . $item['key'] . '      <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button><br><small>' . $item[self::DESCRIPTION] . '</small></h6>
<a class="new-button" href="javascript:void(0);" onclick="javascript:addnewInput(\''.$token.'\')"><i class="fa fa-plus"></i></a>
</div></div>' .
            '<div class="row"><div class="col-sm-12 tag-content"> ' . $render . '</div></div>' .
            '</div>   </div>';
    }

    public function compile_readonly_text($item, $name, $render, $jsonItem)
    {

        return $render . '<div class="form-group row">
                <label class="col-sm-4 col-form-label">' . $item['key'] . '  <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button> </label>
                <div class="col-sm-8">
                  <input type="text" readonly name="' . $name . '" class="form-control" id="' . uniqid() . '" value="' . $item[self::DEFAULT_VALUE] . '">
                </div>
              </div>';

    }

    public function compile_mpfile_tag($item, $name, $render, $jsonItem)
    {

        $token = uniqid();
        $html =  '<div class="row html-group" id="' . $token . '"><div class="col-sm-12">' .
            '<div class="row group-title"><div class="col-sm-12"><h6>' . $item['key'] . '      <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button><br><small>' . $item[self::DESCRIPTION] . '</small></h6>

<a class="new-button" href="javascript:void(0);" onclick="javascript:addnewMpfile(\''.$token.'\')"><i class="fa fa-plus"></i></a>
</div></div>' .
            '<div class="row"><div class="col-sm-12 tag-content">';

        return $html . $render.'</div> </div>' .
            '</div>  </div>';
    }

    public function compile_radio($item, $name, $render, $jsonItem)
    {

        $render .= '  <div class="form-group row">' .
            '    <label class="col-sm-4 col-form-label">' . $item['key'] . '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
                <i class="fa fa-info"></i>
            </button></label>' .
            '    <div class="col-sm-8">';
        foreach ($item['values'] as $key => $value) {
            $token = uniqid();
            $render .= '<div class="form-check form-check-inline">' .
                '  <input class="form-check-input" type="radio" name="' . $name . '"  id="' . $token . '" value="' . $key . '" '.($item[self::DEFAULT_VALUE] == $key ? 'checked' :'').'>' .
                '  <label class="form-check-label" for="' . $token . '">' . $value . '</label>' .
                '</div> ';
        }
        return $render. '    </div>' .
            '  </div>';
    }

    public function compile_tab($item, $name, $render, $jsonItem)
    {
        $this->tab_arrays($item, $name, $render, $jsonItem);
        return '';
    }
    public function compile_select($item, $name, $render, $jsonItem){
        $token = uniqid();
        $html = '<div class="form-group row">
  <label for="'.$token.'" class="col-sm-4 col-form-label">' . $item['key'] . '     <button type="button" class="btn btn-primary btn-sm btn-mn" data-toggle="tooltip" data-placement="top" title="' . $item['name'] . '">
  <i class="fa fa-info"></i>
</button> </label>
                <div class="col-sm-8">
  <select class="custom-select" name="' . $name . '"   id="'.$token.'">';
        foreach ($item['values'] as $key=>$value){
             $html .='<option value="'.$key.'" '.($item[self::DEFAULT_VALUE] == $key ? 'selected' :'').'>'.$value.'</option>';
        }
        return $html . '</select>
            </div></div> ';
    }

    public function compile_input_array($item, $name, $render, $jsonItem)
    {
        //TODO
        return $this->compile_input_text($item, $name, $render, $jsonItem);
    }

    public function compile_object_selector($item, $name, $render, $jsonItem)
    {
        //TODO
        return $this->compile_input_text($item, $name, $render, $jsonItem);
    }

    public function compile_condition($item, $name, $render, $jsonItem)
    {
        //TODO
        return $this->compile_input_text($item, $name, $render, $jsonItem);
    }
    public function compile_collection($item, $name, $render, $jsonItem)
    {
        //TODO
        return $this->compile_input_text($item, $name, $render, $jsonItem);
    }
    public function tab_arrays($item, $name, $render, $jsonItem)
    {
        $this->tabs[md5($jsonItem)][$item['key']] = [
            'item' => $item,
            'render' => $render
        ];
    }

    public function renderTabs($item,$md5)
    {
        $navs = '<ul class="nav nav-tabs" id="'.$md5.'" role="tablist">';
        $contents = ' <div class="tab-content" id="'.$md5.'Content">';

        $i=0;
        foreach ($item as $value){

            $keyName = $md5.'-'.$value['item']['key'];
            $navs .= '<li class="nav-item">
                <a class="nav-link '.($i==0 ? 'active' :'') .'" id="'.$keyName.'-tab" data-toggle="tab" href="#'.$keyName.'" role="tab"
                   aria-controls="'.$keyName.'" aria-selected="false">'.$value['item']['name'].'</a>
                   <a class="new-button2" href="javascript:void(0);" onclick="javascript:addnewTabInput(\''.$keyName.'\')"><i class="fa fa-plus"></i></a>
           </li>';
            $contents .='<div class="tab-pane fade '.($i==0 ? 'show active' :'') .'" id="'.$keyName.'" role="tabpanel"
                                 aria-labelledby="'.$keyName.'-tab">
                '.$value['render'].'
                            </div>';
            $i++;
        }

        return $navs.'</ul>'.$contents.'</div>';
    }

    public function mergeInfo(array $schema)
    {
        $this->schema =$schema;

        $this->mergeItem($this->info,'');


    }

    public function mergeItem( $info,$name)
    {


        foreach ($info as $key=>$value){

            $send = ltrim($name.'.items.'.$key,'.');
            if(is_array($value)){
                $find = data_get( $this->schema,$send);
                if(!$find){
                    $set = [
                        "key" => $key,
                        "name" => $key,
                        self::DESCRIPTION => "",
                        "type" => "group",
                        "custom_template" => "",
                        self::ITEMS => []
                    ];

                    data_set( $this->schema,$send,$set);
                }
                $this->mergeItem( $value,$send);
            }else{
                $find = data_get( $this->schema,$send);
                if(!$find){
                    $set = [
                        "key" => $key,
                        "name" => $key,
                        self::DESCRIPTION => "",
                        "type" => "input_text",
                        "custom_template" => "",
                        self::DEFAULT_VALUE => $value
                    ];
                    data_set( $this->schema,$send,$set);
                }else{
                    data_set( $this->schema,$send.'.default_value',$value);
                }

            }
        }
    }


}
