<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;

class CriteriaDetailExtra extends BaseModel
{

    protected $table = 'criteria_detail_extras';

    public $timestamps = false;

    protected $fillable = ["criteria_detail_id", "key", "value"];

    public function criteriaDetail()
    {
        return $this->belongsTo(CriteriaDetail::class);
    }
}
