<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyDetailExtrasTable extends Migration
{

    public const PROPERTY_DETAIL_EXTRAS = 'property_detail_extras';

    public function up()
    {
        if (!Schema::hasTable(self::PROPERTY_DETAIL_EXTRAS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::PROPERTY_DETAIL_EXTRAS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('property_detail_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop(self::PROPERTY_DETAIL_EXTRAS);
    }
}
