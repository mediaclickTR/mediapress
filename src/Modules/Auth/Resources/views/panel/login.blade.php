@extends('AuthPanel::inc.module_main')
@section('content')
    <div class="d-block loginPage">
        <div class="col-12 h-100">
            <div class="row h-100">
                <div class="col-lg-5 h-100 d-table p-0">
                    <div class="form align-middle d-table-cell">
                        <div class="col-7 mx-auto">
                            @include("MPCorePanel::inc.errors")
                            <form action="" method="post" autocomplete="off" id="loginForm">
                                @csrf

                                @if(auth('admin')->check())
                                    <div class="form-group">

                                        <img class="rounded-circle" src="{{get_gravatar(auth('admin')->user()->email)}}"
                                             alt="">
                                        <label>
                                            <h5> {!! auth('admin')->user()->username !!}</h5>
                                            <small>{{ auth('admin')->user()->email}} <span class="badge badge-secondary float-right ml-3" style="cursor: pointer;" onclick="refreshAuth(this)">x</span></small>
                                        </label>
                                        <input type="hidden" name="email" value="{{ auth('admin')->user()->email}}">
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label>{!! trans("MPCorePanel::general.email.address") !!}</label>
                                        {!! Form::text('email',null,['placeholder'=>trans("MPCorePanel::general.email.address"), 'autocomplete' => 'on']) !!}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>{!! trans("MPCorePanel::general.password") !!}</label>
                                    {!! Form::password('password',['placeholder'=>trans("MPCorePanel::general.password")]) !!}
                                </div>
                                <div class="form-group">
                                    <button class="btn submit w-100 btn-primary light-blue" id="loginBtn">{!! trans("MPCorePanel::general.login") !!}</button>
                                </div>
                            </form>
                            <!-- <a href="#" class="f-pass color text-center d-block text-decoration-none">Şifremi unuttum</a>-->
                        </div>
                    </div>
                    <div class="bottomLogo position-absolute w-100 text-center mb-4 pb-4">
                        <img src="{!! asset("vendor/mediapress/images/logo.png") !!}" alt="">
                    </div>
                </div>
                <div class="col-lg-7 h-100 p-0">
                    <h1 class="position-absolute m-5 p-5">Bir web site <br>yönetim panelinden <br>çok daha fazlası.</h1>
                    <div class="images" data-img="{!! asset("vendor/mediapress/images/login.jpg") !!}"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push("styles")
    <style>
        body {
            background: none; }

        header {
            display: none; }

        .wrapper {
            display: none; }

        .loginPage {
            height: 100vh; }
        .loginPage h1 {
            z-index: 1;
            font-size: 30px;
            color: #fff;
            line-height: 40px;
            top: 50px; }

        .images {
            height: 100%;
            background-size: cover;
            background-position: center;
            position: relative; }
        .images:after {
            content: "";
            background: #101010;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            opacity: 0.15; }

        .f-pass {
            font-size: 14px; }

        .bottomLogo {
            bottom: 0;
            left: 0; }
    </style>
@endpush

@push('scripts')
    <script>
        var $source1 = $('.loginPage .images').attr("data-img");
        $('.loginPage .images').css({
            'backgroundImage': 'url(' + $source1 +')'
        });

        function refreshAuth(el) {
            $(el).closest('div.form-group').find('img').remove()
            $(el).closest('div.form-group').find('input:hidden').remove();
            $(el).closest('div.form-group').append('{!! Form::text('email',null,['placeholder'=>trans("MPCorePanel::general.email.address"), 'autocomplete' => 'on']) !!}');
            $(el).closest('div.form-group').find('label').html('{!! trans("MPCorePanel::general.email.address") !!}');
        }

        expireTimer();
        function expireTimer() {
            setTimeout(function () {
                var $time = $('#expireTime').html();

                if($time <= 0) {
                    $('.alert.alert-danger a').trigger('click');
                    return;
                } else {
                    $('#expireTime').html($time - 1);
                    expireTimer();
                }
            }, 1000)
        }
    </script>
@endpush
