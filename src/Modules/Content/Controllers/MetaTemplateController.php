<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;

use Illuminate\Http\Request;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Support\Arr;

class MetaTemplateController extends Controller
{
    public function index()
    {
        $website = session("panel.website");

        if(! activeUserCan([
            "content.websites.website".$website->id.".seo.meta_templates.index",
            "content.websites.seo.meta_templates.index",
        ])){
            return rejectResponse();
        }


        $sitemaps = Sitemap::whereHas('websites', function ($q) use($website) {
            $q->where('id', $website->id);
        })
            ->whereHas('sitemapType', function ($q) use($website) {
                $q->where('sitemap_type_type', 'dynamic');
            })
            ->status([Sitemap::ACTIVE, Sitemap::PASSIVE, Sitemap::DRAFT])
            ->with('sitemapType')
            ->orderBy('sitemap_type_id')
            ->paginate(25);


        return view('ContentPanel::meta_templates.index', compact('sitemaps', 'website'));
    }


    public function edit($sitemap_id)
    {
        $website = session("panel.website");

        if(! activeUserCan([
            "content.websites.website".$website->id.".seo.meta_templates.update",
            "content.websites.seo.meta_templates.update",
        ])){
            return rejectResponse();
        }

        $sitemap = Sitemap::find($sitemap_id);

        if($sitemap->sitemapType->sitemap_type_type != 'dynamic') {
            abort(404);
        }

        $variation_templates = VariationTemplates::variations($sitemap);

        $variables = Content::getMetaVariablesList();
        $flatten_variables = Arr::collapse(Arr::pluck($variables, "variables"));

        return view('ContentPanel::meta_templates.edit', compact('sitemap', 'variation_templates', 'flatten_variables'));
    }

    public function update(Request $request) {

        $website = session("panel.website");

        if(! activeUserCan([
            "content.websites.website".$website->id.".seo.meta_templates.update",
            "content.websites.seo.meta_templates.update",
        ])){
            return rejectResponse();
        }

        $data = $request->except('_token');

        $sitemapId = $data['id'] ?? null;

        $sitemap = Sitemap::find($sitemapId);

        if($sitemap) {
            foreach ($data['meta_templates'] as $countryGroupId => $templates) {
                foreach ($templates as $languageId => $template) {

                    $detail = $sitemap->details()->where('country_group_id', $countryGroupId)
                        ->where('language_id', $languageId)
                        ->first();

                    if($detail) {
                        $detail->update(['meta_templates' => $template]);
                    }
                }
            }
        }

        return redirect()->back();
    }
}
