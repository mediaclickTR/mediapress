<?php
/**
 * Created by PhpStorm.
 * User: Saban Koparal
 * E-Mail: saban.koparal@mediaclick.com.tr
 */

Route::group(['middleware' => 'panel.auth'], function()
{
    Route::group(["prefix" => 'Settings', 'as'=>'Settings.'], function () {

        Route::get('/', ['as' => "index", 'uses' => 'SettingsController@index']);
        Route::post('/image', ['as' => "imageAjax", 'uses' => 'SettingsController@imageAjax']);
        Route::post('/env', ['as' => "envAjax", 'uses' => 'SettingsController@envAjax']);
        Route::post('/breadcrumb', ['as' => "breadcrumb", 'uses' => 'SettingsController@breadcrumb']);
        Route::post('/logo', ['as' => "logo", 'uses' => 'SettingsController@logo']);
        Route::post('/googleMap', ['as' => "googleMap", 'uses' => 'SettingsController@googleMap']);
        Route::post('/login', ['as' => "login", 'uses' => 'SettingsController@login']);


    });
});
