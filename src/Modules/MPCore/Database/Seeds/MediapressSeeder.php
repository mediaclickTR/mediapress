<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;

class MediapressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        @unlink(public_path("robots.txt"));
    }
}
