<?php

namespace Mediapress\Modules\Content\Facades;

use Illuminate\Support\Facades\Facade;

class VariationTemplates extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\Content\Foundation\VariationTemplates::class;
    }

}