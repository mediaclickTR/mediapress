<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Foundation\KeyValueCollection;
use Mediapress\Modules\Content\Controllers\InstagramController;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;
    protected $table = "instagram_albums";
    protected $guarded = ["created_at", "updated_at", "deleted_at"];

    public function posts($function = null)
    {
        $controller = new InstagramController();
        return $controller->getPosts($this->id, $function);
    }

    public function getPosts(){
        return $this->belongsToMany(InstagramPosts::class, "instagram_post_album", "album_id", "post_id");
    }

}
