<?php

return [
    'code_success' => 'Your Password is Correct !!',
    'code_error' => 'Your Password Is Invalid !!',
    'verify_btn' => 'Verify',

    'register' => ['title' => "Enable Google Authenticator",
        'description_1' => "Download and install Google Authenticator application from App Store or Google Play on your mobile ..",
        'description_2' => "Add the application by scanning the following QR code or entering the setup key.",
        'description_3' => "Enter the 6-digit code in the mobile application in the box below.",
        'secret_key' => "Setup Key:",
    ],

    'verify' => [
        'title' => 'Google Authentication Password',
        'login' => 'Sign In',
    ],

    'warning' => [
        'title' => "Your <strong>Google 2FA</strong> verification account is not active!",
    ]
];
