<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSitemapXmlBlocksTableAddCFColumns extends Migration
{
    public const SITEMAP_XML_BLOCKS = 'sitemap_xml_blocks';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable(self::SITEMAP_XML_BLOCKS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable(self::SITEMAP_XML_BLOCKS)) {
                $schema->table(self::SITEMAP_XML_BLOCKS, function (Blueprint $table) {
                    $table->varchar('detail_change_frequency',10)->nullable()->default(null);
                    $table->varchar('categories_change_frequency',10)->nullable()->default(null);
                    $table->varchar('pages_change_frequency',10)->nullable()->default(null);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_XML_BLOCKS)) {
            $schema->table(self::SITEMAP_XML_BLOCKS, function (Blueprint $table) {
                $table->dropColumn("detail_change_frequency");
                $table->dropColumn("categories_change_frequency");
                $table->dropColumn("pages_change_frequency");
            });
        }
    }
}
