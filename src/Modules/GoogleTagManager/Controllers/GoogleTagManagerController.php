<?php

namespace Mediapress\Modules\GoogleTagManager\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Mediapress\Modules\GoogleTagManager\Traits\GoogleTagManagerTrait;
use Carbon\Carbon;
use Cache;

class GoogleTagManagerController extends Controller
{
    use GoogleTagManagerTrait;


    public function __construct()
    {
        $this->tagManagerInit();
    }

    public function index()
    {

        if(! activeUserCan(["googletagmanager.view",])){return rejectResponse();}

        $setting = SettingSun::where('key', 'google.tagmanager.account.id')->first();
        $data = [];
        try{
            if ($setting) {
                $data = $this->getContainerWithTags();
                $data = $this->renderData($data);
            }
        }catch (\Exception $e){
            $data = ['error'=>true];
        }

        return view('GoogleTagManagerPanel::tagmanager.index', compact('data'));
    }


    public function connect()
    {
        $id = request()->get('id');

        return $this->connectTagManager($id);
    }


    public function unconnect()
    {
        SettingSun::where("key", 'like', "google.tagmanager.%")
            ->where('group', 'TagManager')
            ->where("website_id",session("panel.website.id"))->delete();

        Cache::flush();

        return redirect()->back();
    }

    private function renderData(array $data)
    {
        $dataTable = ['container' => $data['container'], 'tags' => []];
        if(!isset($data['tags'])){
            return $dataTable;
        }
        foreach ($data['tags'] as $tag) {
            $item = [];

            $item['id'] = $tag['tagId'];
            $item['name'] = $tag['name'];
            $item['type'] = $this->tagmanagerType($tag['type']);
            $tagTriggers = [];
            foreach ($tag['firingTriggerId'] as $firingTriggerId) {
                if (isset($data['triggers'][$firingTriggerId])) {
                    $tagTriggers[] = '<span class="badge badge-warning">'.$data['triggers'][$firingTriggerId]['name'].'</span>';
                } else {
                    $tagTriggers[] = '<span class="badge badge-primary">Tüm Sayfalar  </span>';
                }

            }
            $item['triggers'] = @implode(' ', $tagTriggers);
            $item['parameters'] = [];
            foreach ($tag['parameter'] as $parameter){
                if(isset($parameter['key']) && isset($parameter['value'])){
                    $item['parameters'][$parameter['key']] = $parameter['value'];
                }
            }
            $item['time'] = Carbon::createFromTimestamp($tag['fingerprint'] / 1000)->diffForHumans();
            $dataTable['tags'][] = $item;
        }
        return $dataTable;

    }


    private function tagmanagerType($type)
    {
        $list = [
            'ua' => 'Google Analytics: Universal Analytics',
            'html' => 'Özel HTML',
            'awct' => 'Google Ads Dönüşüm İzleme',
            'gcs' => 'Google Anketler Web Sitesi Memnuniyeti',
        ];

        if (isset($list[$type])) {
            return $list[$type];
        }
        return $type;
    }


}
