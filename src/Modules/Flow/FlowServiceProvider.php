<?php

namespace Mediapress\Modules\Flow;


use Illuminate\Support\Facades\Route;
use Mediapress\Modules\Flow\Facades\Flow;
use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class FlowServiceProvider extends ServiceProvider
{
    public const DATABASE = 'database';
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = "Flow";
    protected $namespace = 'Mediapress\Modules\Flow';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => storage_path('app' . DIRECTORY_SEPARATOR . self::DATABASE)], $this->module_name . 'Database');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->publishActions(__DIR__);
    
        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $this->app['config']->set("database.connections.mediapress_flow", ['driver' => 'sqlite', self::DATABASE => storage_path('app' . DIRECTORY_SEPARATOR . self::DATABASE . DIRECTORY_SEPARATOR . 'Flow.sqlite'), 'prefix' => '']);
        $loader = AliasLoader::getInstance();
        $loader->alias('Flow', Flow::class);

        app()->bind('Flow', function () {
            return new \Mediapress\Modules\Flow\Flow;
        });
    }
    
    
    protected function mergeConfig($path, $key)
    {
    
        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }
            
            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }
    
    protected function mapWebRoutes()
    {

        $routes_file = __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'WebRoutes.php';

        Route::group([
            'middleware' => 'flow',
            'namespace' => $this->namespace . '\Controllers',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }
}
