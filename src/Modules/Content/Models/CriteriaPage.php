<?php

namespace Mediapress\Modules\Content\Models;
use Illuminate\Database\Eloquent\Model;

class CriteriaPage extends Model {

	protected $table = 'criteria_page';
	public $timestamps = false;

	protected $fillable = ['criteria_id', 'page_id'];

}
