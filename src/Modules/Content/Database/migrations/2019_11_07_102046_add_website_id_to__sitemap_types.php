<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebsiteIdToSitemapTypes extends Migration
{
    public const SITEMAP_TYPES = 'sitemap_types';

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_TYPES)) {
            $schema->table(self::SITEMAP_TYPES, function (Blueprint $table) {
                $table->integer('website_id')->default(1);
            });
        }
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable(self::SITEMAP_TYPES)) {
            $schema->table(self::SITEMAP_TYPES, function (Blueprint $table) {
                $table->dropColumn("website_id");
            });
        }
    }
}
