<?php

namespace Mediapress\Modules\Interaction\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;

class Session extends Model
{
    use CacheQueryBuilder;
    protected $connection = 'mediapress_interaction';
    protected $table = 'sessions';
    protected $fillable = [
        "id",
        "cookie_id",
        'hash'
    ];

    public $timestamps = false;


    public static function boot()
    {

        parent::boot();


        static::creating(function ($model) {

            $model->created_at = Carbon::now();

        });
    }

    public function cookie(){
        return $this->belongsTo(Cookie::class);
    }
    public function visits(){
        return $this->hasMany(Visit::class);
    }
}
