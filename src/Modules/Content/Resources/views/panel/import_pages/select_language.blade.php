@extends('MPCorePanel::inc.app')

@section('content')
    <div class="page-content p-0">

        <div class="p-30 mt-4 pb-0">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a href="{!! route('MPCore.translate.index') !!}"
                       class="lang-btn">
                        <i class="far fa-2x fa-language"></i>
                        {!! __('MPCorePanel::translate.tab.langPart') !!}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('Content.export_pages.selectLanguage') !!}"
                       class="lang-btn">
                        <i class="fas fa-file-export"></i>
                        {!! __('MPCorePanel::translate.tab.exportContent') !!}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{!! route('Content.import_pages.selectLanguage') !!}"
                       class="lang-btn active show">
                        <i class="fas fa-file-import"></i>
                        {!! __('MPCorePanel::translate.tab.importContent') !!}
                    </a>
                </li>
            </ul>
        </div>

        <div class="p-30 mt-4">
            <h2>{!! __('ContentPanel::import_pages.select_language') !!}</h2>
            <div class="pt-3">
                <div class="form-group">
                    <select class="form-control" id="language">
                        @foreach($countryGroups as $countryGroup)
                            @foreach($countryGroup->languages as $language)
                                <option value="{{ $countryGroup->id.','.$language->id }}">{!! $language->name . " ( " . $countryGroup->title . " )" !!}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
                <a href="javascript:void(0)" onclick="importPage($('#language'))" class="btn btn-primary float-right">{!! __('ContentPanel::import_pages.next') !!}</a>
            </div>
        </div>
    </div>
@endsection

<script>
    function importPage(el) {
        window.location = "{{ route('Content.import_pages.index') }}?language=" + el.val();
    }
</script>
