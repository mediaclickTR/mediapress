{{ Form::open(['route' => 'Content.sitemaps.popup_static_store']) }}
<div>
    <style>

        .form-control:focus {
            box-shadow: none;
        }

        input.sitemap_name {
            height: 33px !important;
            padding: .375rem .75rem !important;
            font-size: 1.25rem !important;
            color: #007bff !important;
        }

        table#sitemap_slugs_table td {
            vertical-align: middle;
        }

    </style>
        <div class="form-group d-none">
            <div class="row p-1">
                <div class="col-md-4">
                    <label for="feature_tag">{!! __('ContentPanel::sitemap.sitemap_name') !!}</label>
                </div>
                <div class="col-md-8">
                    <input type="text" name="sitemap_title" id="sitemap_title" class="form-control"
                           placeholder="{!! __('ContentPanel::sitemap.ie_about') !!}"
                           aria-describedby="sitemapTitleHelp"
                           value=""
                    >
                    <small id="sitemapTitleHelp" class="text-muted">{!! __('ContentPanel::sitemap.give_name_to_remember_your_page') !!}</small>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                @foreach(["searchable","show_in_panel_menu","featured_in_panel_menu"] as $key)
                    <div class="@if($loop->first) col-md-3 col-md-offset-1 @elseif($loop->last)  col-md-5 @else col-md-4 @endif">
                        <div class="checkbox">
                            <label title="{!! __("ContentPanel::sitemap.".$key.".input.tooltip") !!}">
                                {!! Form::checkbox($key, 'on', old($key),["class"=>"form-check-input"]) !!}
                                {!! __("ContentPanel::sitemap.".$key) !!}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="form-group d-none">
            <div class="row p-1">
                <div class="col-md-4">
                    <label for="feature_tag" style="right: 20px;">{!! __('ContentPanel::sitemap.property_tag') !!}</label>
                </div>
                <div class="col-md-8">
                    <input type="text" name="feature_tag" id="feature_tag" class="form-control"
                           placeholder=""
                           aria-describedby="featureTagHelp">
                    <small id="featureTagHelp" class="text-muted">{!! __('ContentPanel::sitemap.feature_tag_help') !!}</small>
                </div>
            </div>
        </div>
        @php
            $website_variation_data = $variation_templates[session("panel.website.id")];
        @endphp

        @foreach($website_variation_data["country_groups"] as $cgid => $cgvd)
            <div>
                @if(count($website_variation_data["country_groups"])>1)
                    <h6 class="text-black-50 mb-0" style="font-size:21px;">{!! __('ContentPanel::sitemap.target_area') !!} {{ $cgvd["title"] }}
                        ({{ $cgvd["code"] }}) </h6>
                @endif
                <table class="table table-hover" id="sitemap_slugs_table" data-sitemap-id="{{$sitemap->id}}">
                    <thead>
                    <tr>
                        <th width="7%">{!! __('ContentPanel::sitemap.on_off') !!}</th>
                        <th width="13%">{!! __('ContentPanel::sitemap.language') !!}</th>
                        <th width="80%">{!! __('ContentPanel::sitemap.title_and_slug') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cgvd["languages"] as $lngid => $lng)
                        @php
                            $detail = $sitemap->details->where("country_group_id",$cgid)->where("language_id",$lngid)->first();

                        @endphp
                        @php  $prefix = getWebsiteDetailUrl(session("panel.website.id"), $cgid, $lngid)[0]; @endphp
                        <tr data-cgid="{{$cgid}}" data-lngid="{{$lngid}}">
                            <td class="">
                                <div class="checkbox">
                                    <label title="">
                                        <input class="form-check-input chk_sitemap_detail_active" checked="checked"
                                               name="sitemap_detail_active[{{ $cgid }}][{{ $lngid }}]"
                                               type="checkbox"
                                               value="on"/>
                                    </label>
                                </div>
                                {{--<button class="btn btn-danger btn-sm deactivate-detail @if( ! $detail) d-none @endif" type="button" title="Bu ülke grubu/dil çiftini kapat ve ona ait içeriği sil"><i class="fa fa-times"></i></button>
                                <button class="btn btn-success btn-sm activate-detail @if($detail) d-none @endif" type="button" title="Bu ülke grubu/dil çiftini etkinleştir"><i class="fa fa-check"></i></button>
                                <input type="hidden" name="sitemap_detail_active[{{ $cgid }}][{{ $lngid }}]" id="sitemap_detail_active_{{ $cgid }}_{{ $lngid }}">--}}
                            </td>
                            <td class="">
                                <img
                                    src="{{ mp_asset('vendor/mediapress/images/flags/'. strtoupper($lng["code"]).'.png') }}"
                                    class="rounded"
                                    alt=""> {{ $lng["name"] }} ({{ $lng["code"] }}) {{--<strong>*</strong>--}}
                            </td>
                            <td class="">

                                <div class="row mb-1">
                                    <div class="col-md-2 col-lg-2 align-middle">
                                        <span class="">{!! __('ContentPanel::sitemap.title') !!}</span>
                                    </div>

                                    <div class="col-md-10 col-lg-10">
                                        <div class="form-group">
                                            <label for="sitemap_name[{{ $lng["code"] }}]"></label>
                                            <input type="text" class="form-control sitemap_name"
                                                   name="sitemap_name[{{ $cgid }}][{{ $lngid }}]"
                                                   id="sitemap_name_{{ $cgid }}_{{ $lngid }}"
                                                   value="{{ $detail->name ?? "" }}"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-md-2 col-lg-2 align-middle">
                                        <span class="">Slug</span>
                                    </div>
                                    <div class="col-md-10 col-lg-10">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                        <span class="input-group-text" title="{!! __('ContentPanel::sitemap.update_automatically_from_title') !!}">
                                            <label for="check" class="m-0">
                                                <i class="fa fa-link mr-1"></i>
                                                <input type="checkbox" class="align-middle ml-1 chk_auto_sync_slug"
                                                       id="auto_sync_slug_{{ $cgid }}_{{ $lngid }}" {{ $detail ? "" : "checked" }}>
                                            </label>
                                        </span>
                                                <span class="input-group-text" style="font-size:14px;">
                                    {{ getWebsiteRootUrl(session("panel.website.id"))}}@if($prefix){!! '<strong class="text-primary">/'.$prefix.'</strong>' !!}@endif
                                </span>
                                            </div>
                                            <input class="form-control  slug-input" type="text"
                                                   value="{{$detail->slug ?? ""}}"
                                                   name="sitemap_slug[{{ $cgid }}][{{ $lngid }}]"
                                                   id="sitemap_slug_{{ $cgid }}_{{ $lngid }}"/>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach

</div>
<div class="float-right">
</div>
<div class="clearfix"></div>
{!! Form::close() !!}

<script>

    var dialog_title='@if(isset($isNew) && $isNew){{'Sayfa Yapısı Oluştur'}}@else{{'Sayfa Yapısı Düzenle'}}@endif';
    $(document).ready(function(){
        $('input.chk_auto_sync_slug').change();
        $('#staticSitemapEditor .checkbox').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $('#staticSitemapEditorTitle').html(dialog_title);
        $('#staticSitemapEditor #saveStaticSitemapBtn').click(function(){
            var form = $('#staticSitemapEditor form');
            var _url ='{{ route('Content.sitemaps.popup_static_store') }}';
            var callback = $('#staticSitemapEditor').data('callback');
            $.ajax({
                url: _url,
                data: form.serialize(),
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    console.log(data);
                    window[callback](data);
                    $('#staticSitemapEditor').modal("hide");
                    //$("#staticSitemapEditor .modal-body>.row>.col").html(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    let message = ((jqXHR.responseJSON!==undefined && jqXHR.responseJSON.message!== undefined) ? jqXHR.responseJSON.message : errorThrown);
                    alert("{!! __('ContentPanel::sitemap.sitemap_didnt_updated') !!} " + "\n"  + "\n"+ errorThrown + "\n" + message);
                }
            });
        });

        $('#staticSitemapEditor div.modal-footer button#saveStaticSitemap').show();
    });
</script>
