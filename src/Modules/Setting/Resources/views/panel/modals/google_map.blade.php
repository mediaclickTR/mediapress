<div class="modal fade bd-example-modal-lg" id="googleMap" tabindex="-1" role="dialog"
     aria-labelledby="googleMapLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageQualityLabel">{!! __('SettingPanel::google_map.modal_title') !!}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('Settings.googleMap') }}" method="post" id="googleMap-form" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <label for="api_key">{!! __('SettingPanel::google_map.api_key') !!}</label>
                                <input type="text" name="api_key" class="form-control" id="api_key" value="{!! settingSun('google_map.api_key') !!}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! __('SettingPanel::general.close') !!}</button>
                <button type="button" class="btn btn-primary" onclick="saveGoogleMap()">{!! __('SettingPanel::general.save') !!}</button>
            </div>
        </div>
    </div>
</div>
