@extends('HeraldistPanel::inc.module_main')
@section('content')

    @push('specific.styles')
        <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
    @endpush

    @include('MPCorePanel::inc.breadcrumb')
    <div class="topPage p-0">
        <div class="float-left">
            <div class="title">{!! trans("HeraldistPanel::messages.form_messages",['form'=>$form->name]) !!}</div>
        </div>
        <div class="float-right">
            <a class="btn btn-primary btn-sm" id="remove-buton" href="{{ route('Heraldist.message.export', ['form_id' => $form->id]) }}">
                <i class="fa fa-upload"></i>{!! trans("HeraldistPanel::messages.export_excel") !!}
            </a>
        </div>
        <div class="float-right">
            <a class="btn btn-primary btn-sm" id="remove-buton" href="{{ route('panel.logout') }}"
               onclick="event.preventDefault(); document.getElementById('form').submit();">
                <i class="fa fa-trash"></i> {!! trans("MPCorePanel::general.selected-delete") !!}
            </a>
        </div>
    </div>
    @if(count($messages) > 0)
        <div class="clearfix"></div>
        <article>
            <div class="message-box">
                <form id="form" action="{!! route('Heraldist.message.selectedRemove') !!}" method="POST">
                    @csrf
                    <div class="item">
                        <ul class="hierarchical_slide hierarchical_slide_inView">
                            <a class="btn btn-primary btn-sm mb-3" id="select-all-button">
                                <i class="fa fa-trash"></i> {!! trans("HeraldistPanel::messages.select_all") !!}
                            </a>
                            @foreach($messages as $message)
                                <li id="mesaj{!! $message->id !!}" data-id="{!! $message->id !!}"
                                    class="{{ $message->read==0?"unread":"read" }}">
                                    <div class="row">
                                        <div class="col-1">
                                            <div class="checkbox">
                                                <label for="male">
                                                    <input type="checkbox" class="deleteCheck" name="checkbox[]"
                                                           value="{!! $message->id !!}">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="head">
                                                <span>{!! strtoupper(substr($message->email,0,2)) !!}</span>
                                                {!! $message->email !!}
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="head">{!! \Str::limit($message->first,70) !!}</div>
                                        </div>
                                        <div class="col-4">
                                            <div class="float-right">
                                                <select class="float-right" onchange="locationOnChange(this.value);">
                                                    <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                                    <option value="{!! route('Heraldist.message.markUnread',$message->id) !!}">{!! trans("HeraldistPanel::messages.check-unread") !!}</option>
                                                </select>
                                                <i class="float-left">{!! translateDate("d/m/y H:i",$message->created_at) !!}</i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail-content">
                                        <table>

                                            <tr>
                                                <td>{!! trans("MPCorePanel::general.reads") !!}</td>
                                                <td>@if($message->read==1) {!! trans("MPCorePanel::general.read") !!} @else {!! trans("MPCorePanel::general.unread") !!} @endif</td>
                                            </tr>
                                            @if(isset($message->data))

                                                @foreach($message->data as $key=>$data)
                                                    @if($data['value'] == $message->email)
                                                        <tr>
                                                            <td>{!! trans("MPCorePanel::general.email") !!}</td>
                                                            <td>
                                                                <a href="mailto:{!! $message->email !!}">{{$message->email}}
                                                                    </a></td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{ strip_tags($data['label']) }}</td>
                                                            <td>
                                                                @if(isset($data['attr_type']) && $data['attr_type'] == "file")
                                                                    @if(strpos($data['value'],"href"))
                                                                        <a href="{!! route('Heraldist.message.downloadFile', encrypt(parseHtmlHref($data['value']))) !!}">{!! trans("HeraldistPanel::messages.download_file") !!}</a>
                                                                    @else
                                                                        <a href="{!! route('Heraldist.message.downloadFile', encrypt($data['value'])) !!}">{!! trans("HeraldistPanel::messages.download_file") !!}</a>
                                                                    @endif
                                                                @else
                                                                    {{$data['value']}}
                                                                    @if(is_url($data['value']))
                                                                        <br/><a href="{{$data['value']}}" class="btn"
                                                                                target="_blank">{!! trans("HeraldistPanel::messages.open.new.page") !!}</a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </table>
                                        <table>
                                            <tbody><tr>
                                                <td><img src="{!! asset('vendor/mediapress/images/date.png') !!}" alt="">{!! trans("HeraldistPanel::messages.form_detail.date") !!}</td>
                                                <td>{!! \Carbon\Carbon::parse($message->created_at)->formatLocalized('%d %B %Y') !!}</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{!! asset('vendor/mediapress/images/clock.png') !!}" alt="">{!! trans("HeraldistPanel::messages.form_detail.time") !!}</td>
                                                <td>{!! \Carbon\Carbon::parse($message->created_at)->format('H.i') !!}</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{!! asset('vendor/mediapress/images/ip.png') !!}" alt="">IP</td>
                                                <td>{!! $message->ip !!}</td>
                                            </tr>
                                            @if($message->source)
                                                <tr>
                                                    <td><img src="{!! asset('vendor/mediapress/images/chrome.png') !!}" alt="">{!! trans("HeraldistPanel::messages.form_detail.referrer") !!}</td>
                                                    <td><a href="{!! $message->source !!}" target="_blank">{!! $message->source !!}</a></td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td><img src="{!! asset('vendor/mediapress/images/chrome.png') !!}" alt="">OS / Browser</td>
                                                <td>{!! $message->agent['platform'] !!} / {!! $message->agent['browser'] !!} / {!! $message->agent['version'] !!}</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{!! asset('vendor/mediapress/images/desc.png') !!}" alt="">{!! trans("HeraldistPanel::messages.form_detail.visited_pages") !!}</td>
                                                <td>
                                                    {!! arrayToLi($message->lastVisit) !!}
                                                </td>
                                            </tr>
                                            @if($message->utm)
                                                <tr>
                                                    <td>{!! trans("HeraldistPanel::messages.form_detail.utm") !!}</td>
                                                    <td>
                                                        {!! arrayToLi($message->utm,' = ') !!}
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($message->id)
                                                <tr>
                                                    <td>{!! trans("HeraldistPanel::messages.form_detail.form_recording") !!}</td>
                                                    <td>
                                                        <a class="btn-btn-sm" target="_blank" href="{!! route('admin.flows.show',['id'=>$message->id]) !!}">{!! trans("HeraldistPanel::messages.form_detail.watch_recording") !!}</a>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody></table>
                                        <br/><br/>
                                        <div class="reply">
                                            <a href="{!! route('Heraldist.message.markUnread',$message->id) !!}">{!! trans("HeraldistPanel::messages.check-unread") !!}</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </form>
            </div>
        </article>
        {!! $messages->links('HeraldistPanel::message.paginate') !!}
    @else
        <div class="alert alert-warning">{!! trans("HeraldistPanel::messages.empty_message_box") !!}</div>
    @endif
@endsection

@push("styles")
    <style>
        .nice-select span:after {
            margin-top: -1px !important;
        }
    </style>
@endpush

@push("scripts")
    <script>
        $(window).scroll(function () {
            if (($(window).scrollTop()) > $(".message-box .list .item:nth-child(3)").offset()['top'] - 500) {
                $('.message-box .list .item:nth-child(3)> ul').addClass('hierarchical_slide_inView');
            }
        });
        // Okundu olarak işaretle
        $(document).ready(function () {
            $("[id*=mesaj].unread").click(function () {
                $(this).addClass("read");
                $(this).removeClass("unread");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('Heraldist.message.changeReadYes') }}",
                    type: "post",
                    data: id = {'id': $(this).attr("data-id")},
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            });
            if (window.location.hash != "") {
                $(window.location.hash).trigger("click");
                $(window.location.hash + " .head").trigger("click");
            }
        });

        $('#select-all-button').on('click', function() {
            $.each($('input.deleteCheck'), function(k, v) {
                $(v).iCheck('toggle');
            });
        })
    </script>
@endpush
