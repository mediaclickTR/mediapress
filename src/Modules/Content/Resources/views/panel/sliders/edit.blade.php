@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">{!! __('ContentPanel::slider.edit.title') !!}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.slider.scenes.index', ['slider_id' => $slider->id]) }}"
                   class="btn btn-light">
                    {!! __('ContentPanel::slider.edit.go_scenes') !!}
                    <i class="fa fa-chevron-right"></i>
                </a>
            </div>
            <div class="float-right mr-2">
                <a href="{{ route('Content.slider.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form center">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('Content.slider.update', $slider->id) !!}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="website">{!! __('ContentPanel::slider.create.select_website') !!}</label>
                        <select name="website_id" id="website">
                            <option value="">{!! __('ContentPanel::general.select') !!}</option>
                            @foreach($websites as $key => $website)
                                <option value="{{$key}}"
                                    {{ $key == old('website_id', $slider->website_id) ? 'selected' : '' }}>
                                    {{ $website }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="name">{!! __('ContentPanel::slider.create.name') !!}</label>
                        <input name="name" type="text" value="{!! old('name', $slider->name) !!}">
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('ContentPanel::slider.create.type.label') !!}</label>
                        <div class="checkbox">
                            <label for="image-radio">
                                <input name="type" id="image-radio" type="radio"
                                       value="image" {!! 'image' == old('type',$slider->type) ? 'checked' : '' !!}>
                                {!! __('ContentPanel::slider.create.type.image') !!}
                            </label>
                            <label for="video-radio">
                                <input name="type" id="video-radio" type="radio"
                                       value="video" {!! 'video' == old('type',$slider->type) ? 'checked' : '' !!}>
                                {!! __('ContentPanel::slider.create.type.video') !!}
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image-radio">{!! __('ContentPanel::slider.create.screen') !!}</label>
                        <select name="screen" id="screen">
                            <option value="">{!! __('ContentPanel::general.select') !!}</option>
                            @php($screens = [1 => 'Fullscreen', 2 => 'Background', 3 => 'Boxed'])
                            @foreach($screens as $key => $screen)
                                <option value="{{$key}}"
                                    {{ $key == old('screen', $slider->screen) ? 'selected' : '' }}>
                                    {{ $screen }}
                                </option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group s-width">
                        <label for="width">{!! __('ContentPanel::slider.create.width') !!}</label>
                        <input name="width" type="text" value="{!! old('width', $slider->width) !!}" style="width: 30%">
                    </div>
                    <div class="form-group s-height">
                        <label for="height">{!! __('ContentPanel::slider.create.height') !!}</label>
                        <input name="height" type="text" value="{!! old('height', $slider->height) !!}"
                               style="width: 30%">
                    </div>
                    <div class="form-group">
                        <label for="text">{!! __('ContentPanel::slider.create.text') !!}</label>
                        <select name="text" id="text">
                            @for($i = 0; $i <= 5; $i++)
                                <option value="{{ $i }}"
                                    {{ $i == old('text', $slider->text) ? 'selected' : '' }}>
                                    {{ $i }}
                                </option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group {{ $slider->type == 'image' ? '' : 'd-none' }}">
                        <label for="button">{!! __('ContentPanel::slider.create.button') !!}</label>
                        <select name="button" id="button">
                            @for($i = 0; $i < 3; $i++)
                                <option value="{{ $i }}"
                                    {{ $i == old('button', $slider->button) ? 'selected' : '' }}>
                                    {{ $i }}
                                </option>
                            @endfor
                        </select>
                    </div>
                    <div class="submit">
                        <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
    <script>
        updateCells();
        $('select[name="screen"]').change(function () {
            updateCells();
        });

        function updateCells() {
            let val = $('select[name="screen"] option:selected').val();
            if (val == 2) {
                $('.s-width').hide();
                $('.s-height').show();
            } else if (val == 3) {
                $('.s-width').show();
                $('.s-height').show();
            } else {
                $('.s-width').hide();
                $('.s-height').hide();
            }
        }
    </script>
@endpush
