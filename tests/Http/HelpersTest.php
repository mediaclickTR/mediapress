<?php


namespace Mediapress\Tests\Foundation;


use Illuminate\Support\Facades\Config;
use Orchestra\Testbench\TestCase;

class HelpersTest extends TestCase
{

    /**
     * @covers ::base_url()
     */

    public function testBaseUrl()
    {
        $baseUrl = base_url();

        $this->assertEquals($baseUrl, 'http://localhost');
    }

    /**
     * @covers ::mp_asset()
     */
    public function testMpAsset()
    {
        $this->setUpTheTestEnvironment();

        $asset = mp_asset('test');
        $this->assertEquals($asset, 'http://localhost/test');
        putenv('MEDIAPRESS_CDN=https://www.mediapress.com.tr/');
        $asset = mp_asset('test');
        $this->assertEquals($asset, 'https://www.mediapress.com.tr/test');
    }

    /**
     * @covers ::panel()
     */
    public function testPanel()
    {
        Config::set('app.panel_url_slug', 'mp-admin');
        $panelPrefix = panel();
        $this->assertEquals($panelPrefix, 'mp-admin');
        $panelPrefix = panel('login');
        $this->assertEquals($panelPrefix, 'mp-admin/login');

    }

    /**
     * @covers ::is_url()
     */
    public function testIsUrl()
    {
        $test = is_url('http://localhost');
        $this->assertFalse($test);
        $test = is_url('https://www.mediapress.com.tr');
        $this->assertNotFalse($test);
        $test = is_url('http://twitter.com');
        $this->assertNotFalse($test);
        $test = is_url('http://öçşiğü.online');
        $this->assertNotFalse($test);
        $test = is_url('http://ist.istanbul');
        $this->assertNotFalse($test);
        $test = is_url('http://ist.accountant');
        $this->assertFalse($test);
    }

    /**
     * @covers ::cache_key()
     */
    public function testCacheKey()
    {
        session(['HTTP_HOST' => 'mediapress']);
        $test = cache_key('test');
        $this->assertEquals($test, '3005abbc1d0d32c6324884b519bd9050-key:test');
    }

    /**
     * @covers ::get_gravatar()
     */
    public function testGetGravatar()
    {
        $test = get_gravatar('xyz@abc.com');
        $this->assertEquals($test, "https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=80&d=mm&r=g");
        $test = get_gravatar('xyz@abc.com', 25);
        $this->assertEquals($test, "https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=25&d=mm&r=g");
        $test = get_gravatar('xyz@abc.com', 25, 'monsterid');
        $this->assertEquals($test, "https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=25&d=monsterid&r=g");
        $test = get_gravatar('xyz@abc.com', 25, 'monsterid', 'r');
        $this->assertEquals($test, "https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=25&d=monsterid&r=r");
        $test = get_gravatar('xyz@abc.com', 25, 'monsterid', 'r', true);
        $this->assertEquals($test, "<img src=\"https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=25&d=monsterid&r=r\" />");
        $test = get_gravatar('xyz@abc.com', 25, 'monsterid', 'r', true, ['width' => 25, 'height' => 25, 'class' => 'test-class']);
        $this->assertEquals($test, "<img src=\"https://www.gravatar.com/avatar/e24778c9815b9a079e520b2867df444d?s=25&d=monsterid&r=r\" width=\"25\" height=\"25\" class=\"test-class\" />");
    }

    /**
     * @covers ::mediapress_path()
     */
    public function testMediapressPath()
    {
        $dir = mediapress_path('test');

        $pos = strpos($dir,'mediapress/src/Http/../test') !== false;

        $this->assertTrue($pos);
    }


    /**
     * @covers ::array_to_xml()
     */
    public function testArrayToXml(){
        $array = [
            'foo'=>'bar',
            'bar'=>'foo'
        ];

        $result = trim(array_to_xml($array));

        $pos = strpos($result,'<data><foo>bar</foo><bar>foo</bar></data>') !== false;

        $this->assertTrue($pos);
    }




}