<?php

return [
    'title' => 'Kullanıcı Aksiyonları',
    'website' => 'Website',
    'user' => 'Kullanıcı',
    'module' => 'Modül Adı',
    'content' => 'Aksiyon Detayı',
    'list_size' => 'Liste Sayısı',
    'clear_filters' => 'Filtreleri Temizle',
];
