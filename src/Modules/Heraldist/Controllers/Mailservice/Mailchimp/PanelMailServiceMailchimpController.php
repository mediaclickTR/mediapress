<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;

class PanelMailServiceMailchimpController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.mailchimp.processing');
    }
}