<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use App;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Traits\HasMFiles;
use Mediapress\Modules\Content\Traits\HasDetails;

class Sitemap extends BaseModel
{

    use SoftDeletes;
    use HasMFiles;
    use HasDetails;

    protected $table = "sitemaps";

    public $timestamps = true;

    protected $dates = ["deleted_at"];

    public $fillable = [
        "sitemap_type_id",
        "urlStatus",
        "detailPage",
        "reservedUrl",
        "filesystem",
        "category",
        "criteria",
        "property",
        "searchable",
        "website_id",
        "show_in_panel_menu",
        "featured_in_panel_menu",
        "admin_id",
        "status",
        "custom",
        "feature_tag",
        "sitemap_id",
        'ctex_1', 'ctex_2', 'cvar_1', 'cvar_2', 'cvar_3', 'cvar_4', 'cvar_5', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cdat_1', 'cdat_2', 'cdec_1', 'cdec_2', 'cdec_3', 'cdec_4',
    ];

    public function websites()
    {
        return $this->belongsToMany(Website::class);
    }

    public function getWebsiteAttribute()
    {
        return $this->websites()->first();
    }

    public function defaultDetail()
    {
        return $this->hasOne(SitemapDetail::class)->where('language_id', $this->defaultLanguage->id);
    }

    public function sitemap(){
        return $this->hasOne(Sitemap::class, "id", "id");
    }

    public function details()
    {
        return $this->hasMany(SitemapDetail::class)->join('languages','languages.id','sitemap_details.language_id')/*->orderBy('languages.sort')*/->select('sitemap_details.*');
    }

    public function pages()
    {
        if ($this->panel || App::runningInConsole()) {
            return $this->hasMany(Page::class);
        } else {
            return $this->hasMany(Page::class)->where(function($q){
                return $q->where('status', 1)->
                orWhere(function ($q) {
                    return $q->where('status', 5)
                        ->whereRaw('published_at < NOW()');
                });
            });
        }



    }

    public function pagesOrdered()
    {
        return $this->hasMany(Page::class)->status('active')->orderBy('order');
    }

    public function urls()
    {
        return $this->hasManyThrough(Url::class, SitemapDetail::class, null, 'model_id')
            ->where('model_type', SitemapDetail::class);
    }

    public function rootCategory()
    {
        return $this->hasOne(Category::class)->where("deleted_at", "==", null)->orderBy("lft");
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function sliders()
    {
        return $this->hasMany(Slider::class);
    }

    public function criterias()
    {
        return $this->hasMany(Criteria::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function sitemapType()
    {
        return $this->belongsTo(SitemapType::class, "sitemap_type_id");
    }


    public function extras()
    {
        return $this->hasMany(SitemapExtra::class);
    }

    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value)) {
            return $extra->value;
        }

        return null;
    }

    public function countryGroup()
    {
        return $this->morphOne(CountryGroup::class, 'owner');
    }

    public function countryGroups()
    {
        return $this->morphMany(CountryGroup::class, 'owner');
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function getWebsiteIdAttribute(){
        //return SitemapWebsite
    }

    // WIDGET
    public function parentSitemap(){
        return $this->belongsTo(Sitemap::class, "sitemap_id", "id");
    }

    public function children(){
        return $this->hasMany(Sitemap::class, "sitemap_id", "id");
    }


}
