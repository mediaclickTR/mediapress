@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }

        .post-photo{
            width: 200px;
            height: auto;
        }

        .post{
            margin-top: 20px;
        }

        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title" style="">
            <div class="float-left">
                Albüm Oluştur
            </div>
{{--                <div class="float-right" >--}}
{{--                    <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>--}}
{{--                </div>--}}

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.index') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Albümler</a>
            </div>
        </div>

        <div id="alert" class="alert alert-danger" role="alert" style="display: none;margin-top: 100px;">
            <p>

            </p>
        </div>

        <form action="" id="album-form">
            <div class="table-field">
                <div style="margin-top: 70px !important; border-top: 1px solid #111;">
                    <div class="container" style="margin-top: 20px !important;">
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h4><strong>Albüm Bilgileri</strong></h4>
                                    <hr>
                                </div>

                                <div class="formgroup" style="margin-top: 10px">
                                    <label for="name">Albüm Adı</label>
                                    <input type="text" class="form-control" name="album_name" id="name">
                                </div>
                                <div class="formgroup" style="margin-top: 10px">
                                    <label for="slug">Albüm Slug</label>
                                    <input type="text" class="form-control" name="album_slug" id="slug">
                                </div>
                                <div class="formgroup " style="margin-top: 10px">
                                    <label for="status">Yayınlama Durumu</label>
                                    <select class="form-control" name="album_status" id="status">
                                        <option value="">Seçiniz</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Pasif</option>
                                    </select>
                                </div>
                                <div class="text-center" style="margin-top: 20px">
                                    <h4><strong>Albüm Medyaları</strong></h4>
                                    <hr>
                                </div>
                                <div class="formgroup text-center" style="margin-top: 20px; display: none!important;">
                                    <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-primary btn-lg refresh" data-option="posts">Kendin Belirle</button>
                                        <button type="button" class="btn btn-primary btn-lg refresh" data-option="refresh">Sürekli Yenile</button>
                                    </div>
                                </div>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    Sürekli Yenile
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="padding-bottom: 30px;">
                                            <div class="card-body">
                                                <div class="col-12">
                                                    <div class="formgroup">
                                                        <label for="limit">Kullanıcı Adı Veya Etiket Giriniz</label>
                                                        <input type="text" class="form-control" name="album_data" placeholder="kullaniciadi, #etiket" value="mediaclicktr">
                                                    </div>
                                                    <div class="formgroup" style="margin-top: 10px">
                                                        <label for="limit">Limit Belirleyin</label>
                                                        <input type="number" class="form-control" name="album_limit" id="limit">
                                                    </div>
                                                    <div class="formgroup"  style="margin-top: 10px">
                                                        <label for="refresh">Yenileme Aralığı</label>
                                                        <select class="form-control" name="album_refresh" id="refresh">
                                                            <option value="">Seçiniz</option>
                                                            <option value="86400">Günlük</option>
                                                            <option value="3600">Saatlik</option>
                                                            <option value="60">Her Dakika</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" >
                                                    Medya Seç
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="padding-bottom: 30px;">
                                            <div class="card-body" >
                                                <div class="row inline justify-content-md-center">
                                                    <div class="col">
                                                        <label for="formGroupExampleInput">Verileri Kullanıcı Adı Üzerinden Senkronize Et</label>
                                                        <input id="input-user" type="text" class="form-control" placeholder="instagram" value="goturkey">
                                                    </div>

                                                    <div class="col">
                                                        <a role="button" id="sync-user" href="javascript:void(0);" class="btn btn-primary form-control" style="margin-top: 30px;">
                                                            <i class="fas fa-sync"></i>Eşitle
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="row inline justify-content-md-center mt-5">
                                                    <div class="col">
                                                        <label for="formGroupExampleInput">Verileri Konu Etiketi Üzerinden Senkronize Et</label>
                                                        <input id="input-hashtag" type="text" class="form-control" placeholder="#instagram" value="goturkey">
                                                    </div>

                                                    <div class="col">
                                                        <a id="sync-hashtag" role="button" class="btn btn-primary form-control" style="margin-top: 30px;">
                                                            <i class="fas fa-sync"></i>Eşitle
                                                        </a>
                                                    </div>
                                                </div>

                                                <div id="postsDiv">

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <a id="save" role="button" class="btn btn-primary form-control" style="margin-top: 30px; float: left; width: 100px">
                                Kaydet
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        var posts = "";
        $(".btn").click(function (e) {
            e.preventDefault();
        });
        $("#save").click(function () {
            $( "input[type=checkbox]:checked" ).each(function () {
                var val = $(this).data("id");
                posts = posts +","+ val;
            });
            var $form = $("#album-form");
            var data = getFormData($form);

            $.post( "{!! route("Content.instagram.saveAlbum") !!}", {
                "posts": posts,
                "data": data,
                "_token": "{{ csrf_token() }}",
            }).fail(function(data) {
                $("#alert").show();
                var errors = "<ul>";
                $.each( data.responseJSON.errors, function( key, value ) {
                    errors = errors + "<li><p>"+value+"</p></li>";
                });
                errors = errors + "</ul>";
                $("#alert p").html(errors);
                window.location.hash = '#alert';
            }).done(function( data ) {
                window.location.href = "{!! route('Content.instagram.index')."?album=true" !!}";
            });
            //console.log(posts);
        });

        $(".refresh").click(function (e) {
            e.preventDefault();
            var option = $(this).data("option");
            if (option == "refresh"){
                $("#schedule").show();
                $("#posts").hide();
            }
            else if(option == "posts"){
                $("#posts").show();
                $("#schedule").hide();
            }
        });

        function getFormData($form){
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};

            $.map(unindexed_array, function(n, i){
                indexed_array[n['name']] = n['value'];
            });

            return indexed_array;
        }

        $("#sync-hashtag").click(function (e) {
            e.preventDefault();
            $("#sync-hashtag i").addClass("turn");
            var hashtag = $("#input-hashtag").val();
            $.post( "{!! route("Content.instagram.albumSyncHashtag") !!}", {
                "hashtag": hashtag,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-hashtag i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });

        $("#sync-user").click(function (e) {
            e.preventDefault();
            $("#sync-user i").addClass("turn");
            var user = $("#input-user").val();
            $.post( "{!! route("Content.instagram.albumSyncUser") !!}", {
                "user": user,
                "_token": "{{csrf_token()}}",
            }).done(function( data ) {
                $("#sync-user i").removeClass("turn");
                $("#postsDiv").append(data);
            });
        });


    </script>
@endpush
