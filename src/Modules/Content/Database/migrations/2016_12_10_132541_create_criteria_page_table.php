<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaPageTable extends Migration
{

    public const CRITERIA_PAGE = 'criteria_page';

    public function up()
    {
        if (!Schema::hasTable(self::CRITERIA_PAGE)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CRITERIA_PAGE, function (Blueprint $table) {
                $table->integer('criteria_id')->unsigned()->index();
                $table->integer('page_id')->unsigned()->index();
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CRITERIA_PAGE);
    }
}
