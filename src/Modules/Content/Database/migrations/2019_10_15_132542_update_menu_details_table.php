<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMenuDetailsTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable('menu_details')) {
            $schema->table('menu_details', function (Blueprint $table) {
                $table->integer('country_group_id')->default(1)->after('language_id');
            });
        }

    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if ($schema->hasTable('menu_details')) {
            $schema->table('menu_details', function (Blueprint $table) {
                $table->dropColumn("country_group_id");
            });
        }
    }
}
