@if(isset($field))

    @if(in_array($field['tag'],['h1','h2','h3','h4','h5','h6']))
        @include('HeraldistWeb::forms.components.h',['field'=>$field])
    @else
        @include('HeraldistWeb::forms.components.'.$field['tag'],['field'=>$field])
    @endif

@endif