<?php


return [
    'dashboard' => 'Giriş',
    'content' => 'Güncelle',
    'forms' => 'Formlar',
    'settings' => 'Ayarlar',

    'general_settings' => 'Genel Ayarlar',
    'tools' => 'Geliştirici Araçları',
    'translate_center' => 'Çeviri Merkezi',
    'user_actions' => 'Kullanım Geçmişi',
    'file_manager' => 'Dosya Yöneticisi',
    'panel_language_parts' => 'Panel Dil Değişkenleri',

    'survey' => 'Anket',
];
