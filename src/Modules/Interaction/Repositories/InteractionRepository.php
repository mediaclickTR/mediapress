<?php

namespace Mediapress\Modules\Interaction\Repositories;

use Carbon\Carbon;
use Mediapress\Modules\Interaction\Models\Browser;
use Mediapress\Modules\Interaction\Models\Cookie;
use Mediapress\Modules\Interaction\Models\Device;
use Mediapress\Modules\Interaction\Models\OperatingSystem;
use Mediapress\Modules\Interaction\Models\Session;
use Mediapress\Modules\Interaction\Models\Visit;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Support\Collection;

class InteractionRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @return array
     */
    public function stats(): array
    {
        $cookies = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count, created_at FROM cookies
                WHERE created_at > :start AND created_at < :finish
                GROUP BY created_at
                ", ['start' => $this->start, 'finish' => $this->finish]);

        $sessions = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count, created_at FROM sessions
                WHERE created_at > :start AND created_at < :finish
                GROUP BY created_at
                ", ['start' => $this->start, 'finish' => $this->finish]);

        $visits = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count, created_at FROM visits
                WHERE created_at > :start AND created_at < :finish
                GROUP BY created_at
                ", ['start' => $this->start, 'finish' => $this->finish]);


        [$totalCookies, $graphCookies] = $this->countGroupedByDate($cookies);
        [$totalSessions, $graphSessions] = $this->countGroupedByDate($sessions);
        [$totalVisits, $graphVisits] = $this->countGroupedByDate($visits);


        return
            [
                'counts' => [
                    'cookies' => $totalCookies,
                    'sessions' => $totalSessions,
                    'visits' => $totalVisits
                ],
                'tabs' => [
                    'cookies' => [
                        'categories' => array_keys($graphCookies),
                        'values' => array_values($graphCookies)
                    ],
                    'sessions' => [
                        'categories' => array_keys($graphSessions),
                        'values' => array_values($graphSessions)
                    ],
                    'visits' => [
                        'categories' => array_keys($graphVisits),
                        'values' => array_values($graphVisits)
                    ],
                ],
            ];
    }

    /**
     * @return array
     */
    public function online(): array
    {
        $fiveMins = Carbon::now()->subMinutes(5);

        $sessions = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count,* FROM sessions
                WHERE created_at > :start AND created_at < :finish
                ", ['start' => $this->start, 'finish' => $this->finish]);

        $visits = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count FROM visits
                WHERE created_at > :start AND created_at < :finish
                ", ['start' => $this->start, 'finish' => $this->finish]);

        $uniqueSession = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count FROM (SELECT id, COUNT(id) as c_id FROM visits
                WHERE created_at > :start AND created_at < :finish
                GROUP BY session_id) AS total_visit
                WHERE c_id = 1
                ", ['start' => $this->start, 'finish' => $this->finish]);

        $online = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count FROM (SELECT id FROM visits
                WHERE visits.created_at > :start
                GROUP BY visits.session_id) AS total_visit
                ", ['start' => $fiveMins]);


        $sessionCount = isset($sessions[0]) ? ($sessions[0]->count == 0 ? 1 : $sessions[0]->count) : 1;
        $uniqueSessionCount = isset($uniqueSession[0]) ? $uniqueSession[0]->count : 0;
        $visitCount = isset($visits[0]) ? $visits[0]->count : 0;
        $onlineCount = isset($online[0]) ? $online[0]->count : 0;

        $bounces = number_format($uniqueSessionCount / $sessionCount * 100, 2);

        $avgVisit = number_format($visitCount / $sessionCount, 2);

        return [
            'online' => $onlineCount,
            'avgVisit' => $avgVisit,
            'bounces' => $bounces,
        ];
    }

    /**
     * @return array
     */
    public function platforms(): array
    {
        $platforms = \DB::connection('mediapress_interaction')
            ->select("
                SELECT de.name, COUNT(cookies.id) as count FROM cookies
                LEFT JOIN devices as de on cookies.device_id = de.id
                WHERE cookies.created_at < ? AND cookies.created_at > ?
                GROUP BY de.name
                ORDER BY count desc
                ", [$this->finish, $this->start]);
        $hold = [];
        foreach ($platforms as $platform) {

            if (is_null($platform->name)) {
                $name = __("InteractionPanel::dashboard.other");
            } else {
                $name = $platform->name;
            }
            $hold[$name] = $platform->count;
        }

        $hours = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count, hour, week FROM visits
                WHERE visits.created_at < :finish AND visits.created_at > :start
                Group By hour, week
                ", ['finish' => $this->finish, 'start' => $this->start]);


        $hours = $this->replaceTimes($hours);

        $referers = $this->getReferers();

        return [
            'hourly' => $hours,
            'platform' => $this->pieChart($hold, 'N/A'),
            'referers' => $this->pieChart($referers, 'Direct'),
        ];
    }

    /**
     * @return array
     */
    public function browsers(): array
    {
        $operatingSystems = $this->getOperatingSystems();


        $browsers = \DB::connection('mediapress_interaction')
            ->select("
                SELECT br.name, COUNT(cookies.id) as count FROM cookies
                LEFT JOIN browsers as br on cookies.browser_id = br.id
                WHERE cookies.created_at < :finish AND cookies.created_at > :start
                Group By br.name
                ORDER BY count desc
                ", ["finish" => $this->finish, "start" => $this->start]);


        $hold = [];

        foreach ($browsers as $browser) {

            if (is_null($browser->name)) {
                $name = __("InteractionPanel::dashboard.other");
            } else {
                $name = $browser->name;
            }
            $hold[$name] = $browser->count;
        }

        return [
            'os' => $this->pieChart(array_splice($operatingSystems, 0, 3), 'N/A'),
            'browser' => $this->pieChart($hold, 'N/A'),
        ];
    }

    /**
     * @return array|array[]
     */
    public function urls(): array
    {
        return [
            'urls' => $this->getUrls()
        ];
    }

    /**
     * @param Collection $items
     * @return array
     */
    private function countGroupedByDate(array $items): array
    {
        $hold = [];
        $total = 0;
        foreach ($items as $item) {
            $date = Carbon::parse($item->created_at)->format('Y-m-d');
            $total +=   $count =  1 * $item->count;
            if(isset($hold[$date])){
                $hold[$date] += $count;
            }else{
                $hold[$date] = $count;
            }

        }
        return [$total, $hold];
    }


    /**
     * @return array
     */
    private function getReferers(): array
    {
        $referers = \DB::connection('mediapress_interaction')
            ->select("
                SELECT COUNT(id) as count, referer FROM visits
                WHERE visits.created_at < :finish AND visits.created_at > :start AND visits.referer IS NOT NULL AND visits.referer <> '' AND visits.referer <> :url
                GROUP BY visits.referer
                ORDER BY count desc
                limit 10
                ", ['finish' => $this->finish, 'start' => $this->start, 'url' => session("panel.website.slug")]);


        $refererData = [];

        $self = parse_url(url('/'), PHP_URL_HOST);
        foreach ($referers as $referer) {
            $host = $referer->referer;
            if (!$host) {
                $host = 'Direct';
            }
            $host = str_replace('www.', '', $host);
            if (isset($refererData[$host])) {
                $refererData[$host] += $referer->count;
            } else {
                $refererData[$host] = $referer->count;
            }
        }

        unset($refererData[$self]);

        return $refererData;
    }


    /**
     * @return array
     */
    private function getOperatingSystems(): array
    {
        $operationSystems = \DB::connection('mediapress_interaction')
            ->select("
                SELECT os.name, COUNT(cookies.id) as count, (SELECT COUNT(id) from cookies) AS total FROM cookies
                LEFT JOIN operating_systems as os on cookies.operating_system_id = os.id
                WHERE cookies.created_at < ? AND cookies.created_at > ?
                GROUP BY os.name
                ORDER BY count desc
                ", [$this->finish, $this->start]);

        $hold = [];

        foreach ($operationSystems as $os) {

            if (is_null($os->name) || $os->name == "0") {
                $name = __("InteractionPanel::dashboard.other");
            } else {
                $name = $os->name;
            }
            $hold[$name] = $os->count;
        }

        return $hold;
    }

    /**
     * @return array
     */
    private function getUrls(): array
    {
        $urls = \DB::connection('mediapress_interaction')
            ->select("
                SELECT visits.url_id, COUNT(id) as count FROM visits
                WHERE visits.created_at < ? AND visits.created_at > ?
                GROUP BY visits.url_id
                ORDER BY count desc
                limit 10
                ", [$this->finish, $this->start]);

        $urlData = [];
        foreach ($urls as $url) {

            $tempUrl = Url::select('model_id', 'model_type', 'url')->find($url->url_id);

            $urlName = $this->getOriginalUrlModelName($tempUrl);

            $urlData[] = [
                'name' => $urlName,
                'url' => $tempUrl ? $tempUrl->url : '/' ,
                'count' => $url->count
            ];
        }

        return $urlData;
    }

    private function getOriginalUrlModelName($url)
    {
        if(is_null($url)) {
            return "N/A";
        }
        if ($url->type == 'redirect') {
            $this->getOriginalUrlModelName($url->model);
        }
        return optional($url->model)->name;
    }


    /**
     * @return bool
     */
    public function countries(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function ages(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function alert(): bool
    {
        return true;
    }

}
