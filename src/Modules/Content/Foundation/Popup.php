<?php


namespace Mediapress\Modules\Content\Foundation;

use Mediapress\Foundation\Browser;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Modules\Content\Models\Popup as Model;
use Carbon\Carbon;
use Mediapress\Modules\Content\Models\PopupCustomUrl;
use Mediapress\Modules\Content\Models\SitemapDetail;

class Popup
{
    public $popups;
    private $urlModel;
    private $langModel;
    private $activeCountryGroupModel;
     public function __construct($mediapress)
    {
        $this->urlModel = $mediapress->url;
        $this->langModel = $mediapress->activeLanguage();
        $this->activeCountryGroupModel = $mediapress->activeCountryGroup();

        $userAgent = new UserAgent();

        $this->popups = Model::where('status', 1)
            ->where('website_id', $mediapress->website->id)
            ->whereHas('country_groups', function ($query) {
                $query->where('country_group_id', $this->activeCountryGroupModel->id);
            })
            ->whereHas('languages', function ($query) {
                $query->where('id', $this->langModel->id);
            })
            ->where('devices', 'like', '%'. $userAgent->getDevice() . '%')
            ->where(function ($query) {
                $query->where(function ($q) {
                    $date = Carbon::now()->format("Y-m-d");
                    $q->where('start_date', '<=', $date)
                        ->where('finish_date', '>=', $date)
                        ->where('display_date_type', 1);
                })->orWhere('display_date_type', 0);
            })
            ->where(function ($query) {
                $query->where(function($q) {
                    $q->where('show_page', 0)
                        ->orwhere(function ($query) {
                            $query->where('show_page', 1)
                                ->whereHas('urls', function ($query) {
                                    $query->where('id', $this->urlModel->id);
                                });
                        })
                        ->orwhere(function ($query) {
                            $customUrls = PopupCustomUrl::all();
                            $ids = [];
                            $currentUrl = request()->getRequestUri();

                            foreach ($customUrls as $customUrl) {
                                if(strpos($currentUrl, $customUrl->url) !== false) {
                                    $ids[] = $customUrl->popup_id;
                                }
                            }

                            $query->where('show_page', 3)->whereIn('id', $ids);
                        })
                        ->orWhere(function ($query) {
                            $homePage = 0;
                            if ($this->urlModel && $model = $this->urlModel->model) {
                                if ($model && $model instanceof SitemapDetail && $model->sitemap->feature_tag == "homepage") {
                                    $homePage = 1;
                                }
                            }
                            $query->where('show_page', 2)->where(\DB::raw("1"), $homePage);
                        });
                });

            })
            ->with('urls', 'languages')
            ->orderBy('order')
            ->get();
    }

    public function render()
    {
        $popups = $this->popups;
        return view('web.popups.main',compact('popups'));
    }
}
