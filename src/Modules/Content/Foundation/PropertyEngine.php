<?php

namespace Mediapress\Modules\Content\Foundation;

use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\ClusterArray;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;

class PropertyEngine
{


    public const PROPERTY_ID = "property_id";
    public const PROPERTIES = "Properties";
    public const CONTENT = "Content";
    public const MEDIAPRESS = "Mediapress";
    public const SITEMAP = "Sitemap_";
    public const SITEMAPS = "Sitemaps";
    public const DETAIL = 'detail';

    public function ajaxCreate(Sitemap $sitemap = null, Property $parent = null, Property $edit = null)
    {

        if ($edit == null) {
            $preCreate = Property::preCreate(["sitemap_id" => $sitemap->id, self::PROPERTY_ID => $parent ? $parent->id : null]);
            foreach ($sitemap->details as $detail) {
                $preCreate->details()->create(['language_id' => $detail->language_id, 'country_group_id' => $detail->country_group_id]);
            }
            $title = trans("ContentPanel::categories.create_property");
        } else {
            Content::createUnexistentDetailsOfParent($edit);
            $title = $edit->detail->name ?? "";
            $title = ($title ? $title ." - ": ""). trans("ContentPanel::categories.create_property");
            $preCreate = $edit;
        }



        $renderable_class = Content::getPropertyRenderableClassPath($preCreate);

        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class(["property" => $preCreate]);

        if (!is_a($renderable, BuilderRenderable::class)) {
            return "<strong><code>$renderable_class</code></strong> <br/>bir \n<br/><strong><code>" . BuilderRenderable::class . "</code></strong>\n<br/> türevi olmalıydı ama değil.";
        }
        return view("ContentPanel::categories.property.ajax", compact("renderable", "title"));
    }

    public function ajaxStore(Request $request, Property $property)
    {

        $renderable_class = Content::getPropertyRenderableClassPath($property);

        $renderable = new $renderable_class(["property" => $property]);

        $storer_class = Content::getPropertyStorerClassPath($property);

        $storer = new $storer_class($renderable, $request);
        $storer->store();
        $errors = $storer->getErrors();
        $warnings = $storer->getWarnings();

        $redirect = redirect(route('Content.categories.property.create',$property->sitemap_id));

        if(count($warnings)){
            $redirect->with("warning",$warnings);
        }

        if(count($errors)){
            $redirect->withErrors($errors);
        }

        return $redirect;
    }

    public function propertyOrderSave(Request $request)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = self::PROPERTY_ID;

        $array = $this->buildTree(json_decode($request->nestable_output), $cluster->table_id);
        $id = $array[0]['id'];

        foreach ($cluster->cluster($array) as $property) {
            Property::where("id", $property['id'])->update($property);
        }

        return true;
    }

    private function buildTree($array, $parent_column = "property_id")
    {
        $ls = null;

        foreach ($array as $parent) {
            if ($parent->id == 0) {
                continue;
            }

            if (isset($parent->children)) {
                $ls[] = [
                    "id" => $parent->id, $parent_column => 0, "children" => $this->children($parent->children, $parent_column, $parent->id)
                ];
            } else {
                $ls[] = ["id" => $parent->id, $parent_column => 0];
            }
        }

        return $ls;
    }

    private function nestableGetList($sitemap_id, $model, $parent_id)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = $parent_id;

        // $this->website->defaultLanguage()->id
        $default_language = 760;
        $list = $model::whereSitemapId($sitemap_id)->where('status', '<>', Property::PREDRAFT)->orderBy("lft")->with([
            self::DETAIL => function ($query) use ($parent_id, $default_language) {
                $query->where("language_id", $default_language)->select([$parent_id, "name"]);
            }
        ])->get(["id", $parent_id]);

        $list = $list->toArray();

        foreach ($list as $key => $value) {
            if (isset($value[self::DETAIL]['name'])) {
                $list[$key][self::DETAIL]['name'] = str_replace(["'", '"'], ["\'", '\"'], $value[self::DETAIL]['name']);
            }
        }

        return str_replace("\\\\", "\\", json_encode($cluster->buildFromDB($list)));
    }

    private function children($array, $parent_column, $parent)
    {
        $childList = null;

        foreach ($array as $child) {
            if (isset($child->children)) {
                $childList[] = [
                    "id" => $child->id, $parent_column => $parent, "children" => $this->children($child->children, $parent_column, $child->id)
                ];
            } else {
                $childList[] = ["id" => $child->id, $parent_column => $parent];
            }
        }

        return $childList;
    }
}
