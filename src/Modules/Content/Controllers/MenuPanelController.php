<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\PanelMenu;
use Mediapress\Modules\Content\Models\PanelMenuDetail;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Foundation\ClusterArray;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\Content\Facades\Auth;

class MenuPanelController extends Controller
{
    #region Panel Menu CRUD
    public const TOKEN = '_token';
    public const CONTENT_PANELMENUS_INDEX = "Content.panelmenus.index";
    public const MESSAGE = 'message';
    public const SITEMAP_ID = 'sitemap_id';
    public const PARENT = 'parent';
    public const SITEMAP_DETAILS_LANGUAGE_ID = 'sitemap_details.language_id';
    public const CONTENT_PANEL_PANELMENU_SITEMAP = "ContentPanel::panelmenu.sitemap";
    public const SITEMAPS_ID = 'sitemaps.id';
    public const SITEMAP_DETAILS_SITEMAP_ID = 'sitemap_details.sitemap_id';
    public const SITEMAP_DETAILS = 'sitemap_details';
    public const SITEMAP_DETAILS_NAME = "sitemap_details.name";
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        if(! activeUserCan([
            "mpcore.panel_menus.index",
        ])){
            return rejectResponse();
        }
        $panelmenus = PanelMenu::where('id','<>',0);
        $queries = FilterEngine::filter($panelmenus,false)['queries'];
        $panelmenus = FilterEngine::filter($panelmenus,false)['model'];

        return view('ContentPanel::panelmenus.index', compact('panelmenus','queries'));
    }

    public function create()
    {

        if(! activeUserCan([
            "mpcore.panel_menus.create",
        ])){
            return rejectResponse();
        }

        return view('ContentPanel::panelmenus.create');
    }

    public function edit($id)
    {
        $panelmenu = PanelMenu::findOrFail($id);

        return view('ContentPanel::panelmenus.edit', compact('panelmenu'));
    }

    public function store(Request $request)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.create",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN);

        $insert = PanelMenu::firstOrCreate($data);
        // Log
        if ($insert){
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
            return redirect(route(self::CONTENT_PANELMENUS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }
        return redirect()->back();
    }

    public function update(Request $request)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.update",
        ])){
            return rejectResponse();
        }

        $id = $request->id;
        $data = request()->except(self::TOKEN);
        $update = PanelMenu::updateOrCreate(['id'=>$id],$data);

        // Log
        if ($update){
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::CONTENT_PANELMENUS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.delete",
        ])){
            return rejectResponse();
        }

        $data = PanelMenu::findOrFail($id);

        if ($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect(route(self::CONTENT_PANELMENUS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
    #endregion

    #region DETAILS CRUD
    public function details($id)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.index",
        ])){
            return rejectResponse();
        }

        $main_menu = PanelMenu::findOrFail($id);
        $menu = $this->nestableGetList($id, self::PARENT);

        $languageID = session('panel.active_language.id');
        $sitemaps = Sitemap::
            join(self::SITEMAP_DETAILS, self::SITEMAP_DETAILS_SITEMAP_ID, '=', self::SITEMAPS_ID)
            ->where(self::SITEMAP_DETAILS_LANGUAGE_ID, $languageID)
            ->pluck(self::SITEMAP_DETAILS_NAME, self::SITEMAPS_ID);

        return view('ContentPanel::panelmenus.details', compact(  'id', 'menu','main_menu','sitemaps'));
    }

    public function detailsPost(Request $request)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.create",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN);

        /*
         * Validation
         */

        $fields = [
            self::SITEMAP_ID => trans(self::CONTENT_PANEL_PANELMENU_SITEMAP),
        ];
        $rules = [
            self::SITEMAP_ID => 'required',
        ];
        $messages = [
            'sitemap_id.required' => trans("MPCorePanel::validation.filled", ['filled',trans(self::CONTENT_PANEL_PANELMENU_SITEMAP)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $insert = PanelMenuDetail::firstOrCreate($data);
        // Log
        if ($insert){
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }
        return redirect()->back();
    }

    public function detailsEdit($id)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.update",
        ])){
            return rejectResponse();
        }

        $panel_menu = PanelMenuDetail::findOrFail($id);
        $sitemaps = Sitemap::
        join(self::SITEMAP_DETAILS, self::SITEMAP_DETAILS_SITEMAP_ID, '=', self::SITEMAPS_ID)
            ->where(self::SITEMAP_DETAILS_LANGUAGE_ID, 760)
            ->pluck(self::SITEMAP_DETAILS_NAME, self::SITEMAPS_ID);

        return view('ContentPanel::panelmenus.edit-detail', compact('sitemaps','panel_menu',  'id'));
    }

    public function detailsEditPost(Request $request,$id)
    {

        if(! activeUserCan([
            "mpcore.panel_menus.update",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN);

        /*
         * Validation
         */

        $fields = [
            self::SITEMAP_ID => trans(self::CONTENT_PANEL_PANELMENU_SITEMAP),
        ];
        $rules = [
            self::SITEMAP_ID => 'required',
        ];
        $messages = [
            'sitemap_id.required' => trans("MPCorePanel::validation.filled", ['filled',trans(self::CONTENT_PANEL_PANELMENU_SITEMAP)]),
        ];

        $this->validate($request, $rules, $messages, $fields);


        $update = PanelMenuDetail::updateOrCreate(['id'=>$id],$data);
        // Log
        if ($update){
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
            return redirect(route("Content.panelmenus.details", $update->menu_id))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }
        return redirect()->back();
    }

    public function detailsUpdate(Request $request)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.update",
        ])){
            return rejectResponse();
        }

        $data = request()->except(self::TOKEN);
        $update = PanelMenuDetail::where("id",$data['id'])->updateOrCreate($data);

        if ($update) {
            return redirect()->back()->with(self::MESSAGE,trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with("error",trans("MPCorePanel::general.danger_message"));
    }

    public function detailsDelete($id)
    {
        if(! activeUserCan([
            "mpcore.panel_menus.delete",
        ])){
            return rejectResponse();
        }

        if (!userAction('message.panelmenus.delete', true, false)) {
            return redirect()->to(url(route('accessdenied')));
        }

        $data = PanelMenuDetail::findOrFail($id);

        if ($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
            return redirect()->back()->with(self::MESSAGE,trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

    }

    public function updateList(Request $request)
    {
        $cluster = new ClusterArray();
        $array = $this->buildTree(json_decode($request->menu_json), self::PARENT);

        foreach ($cluster->cluster($array) as $menupart)
        {
            $menupart['draft'] = 0;
            PanelMenuDetail::where("id", $menupart["id"])->update($menupart);
        }

        if ($cluster->buildTree($array, self::PARENT)) {
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with('error', trans('MPCorePanel::general.danger_message'));
    }
    #endregion

    #region Cluster staff
    private function nestableGetList($id, $parent_id)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = $parent_id;
        $list = PanelMenuDetail::where('menu_id', $id)
            ->join(self::SITEMAP_DETAILS, self::SITEMAP_DETAILS_SITEMAP_ID,"=","panel_menu_details.sitemap_id")
            ->where(self::SITEMAP_DETAILS_LANGUAGE_ID,760)
            ->orderBy("panel_menu_details.lft")->get(["panel_menu_details.id", $parent_id, 'panel_menu_details.sitemap_id', self::SITEMAP_DETAILS_NAME]);
        $list = $list->toArray();
        foreach ($list as $key=>$value){
            if(isset($value['name'])){
                $list[$key]['name'] = str_replace(["'",'"'],["\'",'\"'],$value['name']);
            }
        }

        return str_replace("\\\\","\\",json_encode($cluster->buildFromDB($list)));
    }

    private function buildTree($array, $parent_column = "parent")
    {
        $ls = null;
        foreach ($array as $parent) {

            if ($parent->id == 0) {
                continue;
            }
            if (isset($parent->children)) {
                $ls[] = [
                    "id" => $parent->id, $parent_column => 0, "children" => $this->children($parent->children, $parent_column, $parent->id)
                ];
            } else {
                $ls[] = ["id" => $parent->id, $parent_column => 0];
            }
        }

        return $ls;
    }

    private function children($array, $parent_column, $parent)
    {
        $childList = null;

        foreach ($array as $child) {
            if (isset($child->children)) {
                $childList[] = [
                    "id" => $child->id, $parent_column => $parent, "children" => $this->children($child->children, $parent_column, $child->id)
                ];
            } else {
                $childList[] = ["id" => $child->id, $parent_column => $parent];
            }
        }

        return $childList;
    }

    private function deleteListRecursive($id, $bool = false)
    {
        $finded = PanelMenuDetail::where(self::PARENT, $id)->get();
        if ($finded) {
            foreach ($finded as $find) {
                PanelMenuDetail::findOrFail($find->id)->delete();
                $this->deleteListRecursive($find->id, true);
            }
        }
        if (!$bool) {
            PanelMenuDetail::findOrFail($id)->delete();

            return true;
        }
    }

    #endregion
}
