<?php

namespace Mediapress\Support\Database;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\Auth;

trait CacheQueryBuilder
{
    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {




        if (!isPanel() && !app()->runningInConsole() ) {

            $conn = $this->getConnection();

            $grammar = $conn->getQueryGrammar();

            return new Builder($conn, $grammar, $conn->getPostProcessor());
        } else {
            $connection = $this->getConnection();
            return new QueryBuilder(
                $connection, $connection->getQueryGrammar(), $connection->getPostProcessor()
            );

        }

    }
}
