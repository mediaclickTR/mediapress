@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("HeraldistPanel::ebulletin.ebulletins") !!}</div>
            </div>
            <div class="float-right">
                <select class="nice" id="multiple-processings" onchange="locationOnChange(this.value);">
                    <option value="">{!! trans('HeraldistPanel::general.transactions') !!}</option>
                    <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!} value="?list=all">{!! trans('HeraldistPanel::general.all_website_records') !!}</option>
                </select>
                @if(request()->get('list') && request()->get('list')=="all")
                    <a href="{!! route("Heraldist.ebulletin.index") !!}" class="filter-block">
                        <i class="fa fa-remove"></i> {!! trans('HeraldistPanel::general.clear_filters') !!}
                    </a>
                @endif
                @if(userAction('ebulletin.export',true,false))
                    <a class="btn btn-primary btn-sm" href="{!! route('Heraldist.ebulletin.excel.all') !!}"> <i class="fa fa-file-excel"></i> {!! trans("HeraldistPanel::ebulletin.excelall") !!}</a>
                @endif
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="table-field">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script>
        $(document).ready(function(){
            $('#multiple-processings').change(function(){
                $("#selectedRemove").submit();
            });
        });
    </script>
@endpush
