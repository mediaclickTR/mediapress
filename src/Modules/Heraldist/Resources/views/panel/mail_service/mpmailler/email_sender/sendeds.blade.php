@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title"> {!! trans("HeraldistPanel::mail_service.sendedemails") !!}</h1>
            @if(count($emailsenders)<=0)
                <div class="alert alert-warning"> {!! trans("MPCorePanel::general.nodata") !!}</div>
                    <div class="float-right">
                        <a href="{!! route('Heraldist.mpmailler.email_senders.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i>  {!! trans("HeraldistPanel::mail_service.newsend_email") !!}</a>
                    </div>
            @else
                <div class="float-left">
                    <select id="multiple-processings">
                        <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                        <option value="remove">{!! trans('MPCorePanel::general.delete_selected') !!}</option>
                    </select>
                </div>
                <div class="float-right">
                    <a href="{!! route('Heraldist.mpmailler.email_senders.create') !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans("HeraldistPanel::mail_service.newsend_email") !!}</a>
                </div>
            <form action="{!! route("Heraldist.mpmailler.email_senders.selectedRemove") !!}" id="selectedRemove" method="POST">
                @csrf
            <table>
                <thead>
                <tr>
                    <th>{!! trans("MPCorePanel::general.check") !!}</th>
                    <th>{!! trans("MPCorePanel::general.id") !!}</th>
                    <th>{!! trans("HeraldistPanel::mail_service.maillist_name") !!}</th>
                    <th>{!! trans("HeraldistPanel::mail_service.template_name") !!}</th>
                    <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                    <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                </tr>
                </thead>
                @foreach($emailsenders as $sender)
                    <tr>
                        <td>
                            <div class="checkbox">
                                <label for="male">
                                    <input type="checkbox" name="checked[]" value="{!! $sender->id !!}" id="male">
                                </label>
                            </div>
                        </td>
                        <td>{!! $sender->id !!}</td>
                        <td>{!! (isset($sender->maillist)) ? $sender->maillist->name : 'E-Posta listesi bulunamadı.'!!}</td>
                        <td>{!! (isset($sender->email_template)) ? $sender->email_template->name : 'E-Posta teması bulunamadı.'!!}</td>
                        <td>{!! $sender->created_at !!}</td>
                        <td>
                            <select class="nice" onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.email_senders.show',$sender->id) !!}">{!! trans("MPCorePanel::general.detail") !!}</option>
                                <option value="{!! route('Heraldist.mpmailler.email_senders.delete',$sender->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="float-left">
                <div class="pagination-wrapper"> {!! $emailsenders->render() !!} </div>
            </div>
            <div class="float-right">
                <select class="nice">
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
            </form>
        </div>
        @endif
@endsection
@push("scripts")
    <script>
        $(document).ready(function(){
            $('#multiple-processings').change(function(){
                $("#selectedRemove").submit();
            });
        });
    </script>
@endpush
