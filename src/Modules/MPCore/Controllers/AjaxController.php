<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Support\Facades\DB;
use Mediapress\Facades\Slots\MessagingSlot;
use Mediapress\Facades\URLEngine;
use Mediapress\Foundation\BackupData;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\Backup;
use Mediapress\Modules\MPCore\Models\Country;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\CountryGroupCountry;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Facades\MetaEngine;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\Content\Facades\PropertyEngine;
use Mediapress\Modules\Content\Facades\CriteriaEngine;
use Mediapress\Modules\Content\Facades\CategoryEngine;
use Mediapress\Http\Controllers\PanelController as Controller;

class AjaxController extends Controller
{
    public const SITEMAP_ID = "sitemap_id";
    public const WEBSITE_ID = 'website_id';
    public const COUNTRY_CODE = 'country_code';
    public const LANGUAGE_ID = 'language_id';
    public const PARENT_ID = 'parent_id';
    public const EDIT_ID = 'edit_id';
    public $data = [];

    public function __construct()
    {
        $request = Request::capture()->all();
        if(isset($request['data'])){
           $this->data =  $request["data"];
        }
    }

    public function popup($process)
    {
        switch ($process) {

            case 'cgCreateOrUpdate':

                $country_group = CountryGroup::find($this->data['id']);
                $website_id = $this->data[self::WEBSITE_ID];

                // Global grubu dışındaki grupların ülkelerini al
                $groups_ids = CountryGroup::where("owner_id",$website_id)->select("id")->get()->pluck("id")->toArray();
                $grouped_countries = CountryGroupCountry::whereIn('country_group_id',$groups_ids)->distinct()->select(self::COUNTRY_CODE)->pluck(self::COUNTRY_CODE)->toArray();

                $selected_countries = [];
                $selected_languages = collect();
                if (isset($country_group)){

                    $selected_countries = $country_group->countries()->get()->pluck("code","id")->toArray();
                    $selected_languages = $country_group->languages;
                    // düzenleme anındaki ülke grubunun ülkelerini gruplanmış ülkelerden ayır
                    $active_cg_countries = $country_group->countries()->get()->pluck("code", "id")->toArray();
                    $grouped_countries = array_diff($grouped_countries,$active_cg_countries);
                }

                $countries = Country::whereNotIn("code",$grouped_countries)->get()->pluck("tr","code");
                $languages = Language::get()->pluck("name","id");

                $countryNames = '';
                $otherCountries = Country::whereHas('countryGroups', function ($q) {
                    $q->where('code', '<>', 'gl');
                })->get(['id', 'tr']);

                foreach ($otherCountries as $otherCount) {
                    $countryNames .= $otherCount->tr . ', ';
                }

                return view("ContentPanel::websites.ajax.country_group.create_edit",compact("languages", "countries", self::WEBSITE_ID, 'selected_countries', 'countryNames', 'selected_languages', 'country_group'));
                break;

            case 'getPattern':

                $website = isset($this->data[self::WEBSITE_ID]) ? Website::find($this->data[self::WEBSITE_ID]) : null;
                $language = isset($this->data[self::LANGUAGE_ID]) ? Language::find($this->data[self::LANGUAGE_ID]) : null;
                $country = isset($this->data[self::COUNTRY_CODE]) ? $this->data[self::COUNTRY_CODE] : null;
                $sitemap_ids = isset($this->data['sitemap_ids']) ? $this->data['sitemap_ids'] : null;
                $category_ids = isset($this->data['category_ids']) ? $this->data['category_ids'] : null;
                return URLEngine::getPattern($website, $language, $country, $sitemap_ids, $category_ids);

                break;

            case 'checkPattern':

                $website = isset($this->data[self::WEBSITE_ID]) ? Website::find($this->data[self::WEBSITE_ID] * 1) : null;
                $pattern = isset($this->data['pattern']) ? $this->data['pattern'] : null;
                $slug = isset($this->data['slug']) ? $this->data['slug'] : null;

                return URLEngine::checkPattern($website, $pattern, $slug);
                break;

            case 'getExcelImport':

                return view("ContentPanel::seo.ajax.excel_import");

            break;

            case 'categoryCreate':

                $smid = isset($this->data[self::SITEMAP_ID]) ? Sitemap::find($this->data[self::SITEMAP_ID]) : null;
                $pid = isset($this->data[self::PARENT_ID]) ? Category::find($this->data[self::PARENT_ID]) : null;
                $eid = isset($this->data[self::EDIT_ID]) ? Category::find($this->data[self::EDIT_ID]) : null;

                $websiteId = session("panel.website.id");

                $action = "create";
                $realSmID=isset($this->data[self::SITEMAP_ID]) ? $this->data[self::SITEMAP_ID] : 0;
                if($eid){
                    $realSmID=$eid->sitemap_id;
                    $action = "update";
                }

                $permissions = [
                    "content.websites.website$websiteId.sitemaps.sitemap".$realSmID.".categories.$action",
                    "content.websites.website$websiteId.sitemaps.categories.$action",
                    "content.websites.sitemaps.categories.$action"
                ];


                if(! activeUserCan($permissions)){
                    return rejectResponse(null,null,"modal-body-content",null,200);
                }
                return CategoryEngine::ajaxCreate($smid, $pid, $eid);
                break;

            case 'createProperty':

                $smid = isset($this->data[self::SITEMAP_ID]) ? Sitemap::find($this->data[self::SITEMAP_ID]) : null;
                $pid = isset($this->data[self::PARENT_ID]) ? Property::find($this->data[self::PARENT_ID]) : null;
                $eid = isset($this->data[self::EDIT_ID]) ? Property::find($this->data[self::EDIT_ID]) : null;


                $action = "create";
                $realSmID=isset($this->data[self::SITEMAP_ID]) ? $this->data[self::SITEMAP_ID] : 0;
                if($eid){
                    $realSmID=$eid->sitemap_id;
                    $action = "update";
                }

                $websiteId = session("panel.website.id");

                if(! activeUserCan([
                    "content.websites.website$websiteId.sitemaps.sitemap".$realSmID.".properties.$action",
                    "content.websites.website$websiteId.sitemaps.properties.$action",
                    "content.websites.sitemaps.properties.$action"
                ])
                ){
                    return rejectResponse(null,null,"modal-body-content",null,200);
                }

                return PropertyEngine::ajaxCreate($smid, $pid, $eid);
                break;

            case 'createCriteria':

                $smid = isset($this->data[self::SITEMAP_ID]) ? Sitemap::find($this->data[self::SITEMAP_ID]) : null;
                $pid = isset($this->data[self::PARENT_ID]) ? Criteria::find($this->data[self::PARENT_ID]) : null;
                $eid = isset($this->data[self::EDIT_ID]) ? Criteria::find($this->data[self::EDIT_ID]) : null;

                $websiteId = session("panel.website.id");


                $action = "create";
                $realSmID=isset($this->data[self::SITEMAP_ID]) ? $this->data[self::SITEMAP_ID] : 0;
                if($eid){
                    $realSmID=$eid->sitemap_id;
                    $action = "update";
                }

                if(! activeUserCan([
                    "content.websites.website$websiteId.sitemaps.sitemap".$realSmID.".criterias.$action",
                    "content.websites.website$websiteId.sitemaps.criterias.$action",
                    "content.websites.sitemaps.criterias.$action"
                ])
                ){
                    return rejectResponse(null,null,"modal-body-content",null,200);
                }
                return CriteriaEngine::ajaxCreate($smid, $pid, $eid);

                break;

            case 'urlMetasList':

                $url = Url::find($this->data);
                $urlMetas = MetaEngine::getUrlMetas($this->data)[1];
                $metaTypes = MetaEngine::getMetaTypes();
                $this->data = [
                    'metas' => $urlMetas,
                    'metaTypes' =>
                        $metaTypes
                ];

                $data = $this->data;

                return view("ContentPanel::seo.ajax.url_metas", compact('data', 'url'));
                break;

            case 'categoryCriteriasEdit':

                $category = Category::with("detail")->where("id",$this->data[self::PARENT_ID])->first();


                $websiteId = session("panel.website.id");
                $smId = $category->sitemap_id;

                $permissions = [
                    "content.websites.website$websiteId.sitemaps.sitemap".$smId.".categories.update",
                    "content.websites.website$websiteId.sitemaps.categories.update",
                    "content.websites.sitemaps.categories.update"
                ];

                if(! activeUserCan($permissions)){
                    return rejectResponse(null,null,"modal-body-content",null,200);
                }

                $criterias = Criteria::where(function ($query) {
                        $query->where(self::SITEMAP_ID, $this->data[self::SITEMAP_ID]);
                    })
                    ->where("depth",0)
                    ->with([
                        "details" => function ($query) {
                            $query->where(self::LANGUAGE_ID, session("panel.website")->defaultLanguage()->id)->select("criteria_id", "name");
                        },
                        "categories" => function ($query) {
                            $query->where("id", $this->data[self::PARENT_ID])->select("id");
                        }
                    ])
                    ->orderBy("lft","ASC")->get(["id"]);

                $data = $this->data;

                return view("ContentPanel::ajax.categories.category.criterias", compact('data', 'criterias', 'category'));
                break;

            case 'categoryPropertiesEdit':

                $category = Category::with("detail")->where("id",$this->data[self::PARENT_ID])->first();


                $websiteId = session("panel.website.id");
                $smId = $category->sitemap_id;

                $permissions = [
                    "content.websites.website$websiteId.sitemaps.sitemap".$smId.".categories.update",
                    "content.websites.website$websiteId.sitemaps.categories.update",
                    "content.websites.sitemaps.categories.update"
                ];

                if(! activeUserCan($permissions)){
                    return rejectResponse(null,null,"modal-body-content",null,200);
                }

                $properties = Property::where(function ($query) {
                    $query->where(self::SITEMAP_ID, $this->data[self::SITEMAP_ID]);
                })->with([
                    "details" => function ($query) {
                        $query->where(self::LANGUAGE_ID, session("panel.website")->defaultLanguage()->id)->select("property_id", "name");
                    },
                    "categories" => function ($query){
                        $query->where("id", $this->data[self::PARENT_ID])->select("id");
                    }
                ])->get(["id"]);

                $data = $this->data;

                return view("ContentPanel::ajax.categories.category.properties", compact('data', 'properties', 'category'));
                break;

            case 'contact_management_modal':
                $templates = MessagingSlot::getEmailTemplates()->pluck("name","id");
                $data = ["templates"=>$templates, "selected"=>serialize($this->data)];
                return view("HeraldistPanel::mail_service.mpmailler.contact_management.ajax.modal", compact('data'));
                break;

            case 'cm_excel_import':
                return view("HeraldistPanel::mail_service.mpmailler.contact_management.ajax.excel_import", compact('data'));
                break;

            default:
                return view("MPCorePanel::ajax.not-found");
                break;
        }
    }


    public function backup(Request $request){

        $backup = new BackupData();

        $backups = $backup->get($request);
        return  view("MPCorePanel::ajax.backups",compact('backups'));

    }
}
