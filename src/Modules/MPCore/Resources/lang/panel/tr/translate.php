<?php

return [
    'tab' => [
        'langPart' => "Dil Değişkenleri",
        'exportContent' => "İçerik Dışarı Aktarıcı",
        'importContent' => "İçerik İçeri Aktarıcı",
    ],

    'langPart_title' => 'Dil Değişkenleri',
    'save_info' => 'Dikkat kaydetme işlemi otomatik yapılır. İlgili alanları doldurmanız yeterlidir.',
    'search' => 'Arama...',
    'variable'=>'Değişken',
    'default_lang_value'=>'Varsayılan Dil Değeri',
    'detail' => "Detay",
    'view'=>'Görüntüle',
    'languages'=>'Diller',
    'no_data'=>':website  adlı siteye ait dil değişkeni bulunamadı.',
];
