@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! trans("ContentPanel::panelmenu.create_menu") !!}
                </div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.panelmenus.index') }}" class="btn btn-light mr-3">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
       <div class="p-4 float-left w-100">
           <div class="col-md-12">
           @include("ContentPanel::panelmenus.form")
           </div>
       </div>
    </div>
@endsection
