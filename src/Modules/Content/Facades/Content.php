<?php

namespace Mediapress\Modules\Content\Facades;

use Illuminate\Support\Facades\Facade;

class Content extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\Content\Content::class;
    }

}