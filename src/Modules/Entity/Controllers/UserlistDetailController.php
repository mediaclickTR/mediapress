<?php

namespace Mediapress\Modules\Entity\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mediapress\Modules\Entity\Models\User;
use Mediapress\Modules\Entity\Models\Entities;
use Mediapress\Modules\Entity\Models\UserlistPivot;
use Mediapress\Modules\Entity\Models\Userlist;



class UserlistDetailController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const USERS = 'users';
    public const LIST_ID = 'list_id';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index($list_id)
    {
        if(!userAction('message.forms.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $users = UserlistPivot::where("userlist_id",$list_id)->paginate(25);
        $userlist = Userlist::find($list_id);
        return view('EntityPanel::userlists.listdetail.index', compact(self::USERS,'userlist', self::LIST_ID));
    }

    public function create($list_id)
    {
        if(!userAction('message.users.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $users = User::all();
        $entities = Entities::all();
        return view("EntityPanel::userlists.listdetail.create",compact(self::USERS,'entities', self::LIST_ID));
    }

    public function edit($list_id,$id)
    {
        $user = UserlistPivot::find($id);
        $modulusers = $user->userable_type::get();
        return view('EntityPanel::userlists.listdetail.edit', compact("modulusers","user", self::LIST_ID));
    }

    public function store(Request $request,$list_id)
    {
        if(!userAction('message.users.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $fields = [
            self::USERS => 'Üyeler',
        ];
        $rules = [
            self::USERS => 'required',
        ];
        $messages = [
            'users.required' => 'Üyeler boş bırakılmamalıdır.',
        ];

        $this->validate($request, $rules, $messages, $fields);

        foreach ($request->users as $user)
        {
            // Gelen değerdeki id'yi string'den kurtar
            $userid = substr_replace($user,'', -5);

            if(strstr($user,"user")){ // user model
                $user = User::find($userid);
            }
            elseif(strstr($user,"enti")){ //entity model
                $user = Entities::find($userid);
            }

            $list = new UserlistPivot;
            $save = $user->userable()->save($list);
            // Log
            if ($save) {
                UserlistPivot::where("id",$save->id)->update(["userlist_id"=>$list_id]); // kayda ait list_id'yi güncelle
                UserActionLog::create(__CLASS__."@".__FUNCTION__,$save);
            }

        }
        return redirect(route('admin.userlists.listdetail.index',$list_id))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request,$list_id)
    {
        $update=UserlistPivot::updateOrCreate(['id'=>$request->id],['userable_id'=>$request->userable_id]);
        if ($update) {
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route('admin.userlists.listdetail.index',$list_id))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function detail($list_id,$id)
    {
        $user = UserlistPivot::find($id);
        $moduluser = $user->userable_type::find($user->userable_id);
        return view('EntityPanel::userlists.listdetail.detail', compact("moduluser","user", self::LIST_ID));
    }

    public function delete($list_id,$id)
    {
        if(!userAction('message.users.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $data=UserlistPivot::find($id);
        if($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function selectedRemove(Request $request)
    {
        if($request->checked) {
            foreach ($request->checked as $check){
                UserlistPivot::destroy($check);
            }
        }
        else{
            return redirect()->back()->withErrors([trans('MPCorePanel::general.danger_message')]);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}