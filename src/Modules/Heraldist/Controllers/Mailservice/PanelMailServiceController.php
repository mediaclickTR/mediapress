<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice;

use Mediapress\Http\Controllers\PanelController as Controller;

class PanelMailServiceController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.processing');
    }
}