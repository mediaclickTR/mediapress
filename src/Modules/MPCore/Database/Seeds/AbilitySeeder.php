<?php

namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abilities')->insert(array(
            array('name' => '*', 'title' => 'All Abilities', 'entity_type' => '*')
        ));
    }
}
