<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use mysql_xdevapi\Schema;

class UpdateAdminsAddApiKeyTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->table('admins', function (Blueprint $table) {
            $table->string('api_token')->nullable()->after('remember_token');
        });
    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if( $schema->hasTable("admins")) {
            $schema->table("admins", function (Blueprint $table) {
                $table->dropColumn('api_token');

            });
        }
    }
}
