@extends('ContentPanel::inc.module_main')

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
@endpush

@php
    $domain = getWebsiteRootUrl(session('panel.website'));
@endphp

@push("styles")
    <style>
        table#sitemaps_table td:not(.include-column) > .auto{
            text-align: center;
        }

        table#sitemaps_table td.include-column > .customized{
            margin-bottom: -4px;
        }

        .input-group input#filename{

            height: 2.25rem !important;
            margin-top: 1px;
        }

        input#filename.locked{
            background-color: #2196f3;
            color: white;
        }

    </style>
@endpush

@section('content')
    <div class="container-fluid">
        @include('MPCorePanel::inc.breadcrumb')
        <div class="page-content p-0">
            @php
                $action = $model->exists ? "update" : "create";
                $default_for_homepage = $block_templates["homepage"] ?? null;
                $default_for_others = $block_templates["other"] ?? null;
            @endphp
            <div class="topPage">
                <div class="float-left">
                    <div class="title m-0">{!! trans("ContentPanel::sitemap_xmls.".$action."_sitemap_xml") !!}
                        <span style="color:#bbb">({{ $model->type=="default" ? "Genel Dosya" : "Özel Dosya" }})</span>
                    </div>
                </div>
                <div class="float-right">
                    <a class="btn-primary btn-sm" href="{{url(route("Content.seo.sitemap_xmls.index"))}}" class="btn btn-link float-right"><i
                            class="fa fa-arrow-left"></i>  {!! __('ContentPanel::general.go_back_list') !!}</a>
                </div>
            </div>

            @include("MPCorePanel::inc.errors")
            <div class="p-30 mt-4">
                <form action="{{ url(route("Content.seo.sitemap_xmls.store")) }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $model->id }}">
                    <input type="hidden" name="website_id" value="{{session("panel.website.id")}}">
                    <input type="hidden" name="type" value="{{ $model->type }}">

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group focus">
                                    <label> {!! __('ContentPanel::seo.xml.title') !!} (*)</label>
                                    <input type="text" name="title" value="{{ old("title",$model->title) }}"
                                           maxlength="191">
                                    <small id="helpId" class="form-text text-muted">
                                        {!! __('ContentPanel::seo.xml.shown_name') !!}
                                    </small>
                                </div>
                                <div class="clearfix"></div>
                                <br>

                                <div class="row">
                                    <div class="col-11">
                                        <div class="input-addon">
                                            <label> {!! __('ContentPanel::seo.xml.file_name') !!} (*)</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="filename-label">{{ $domain }}/</span>
                                                </div>
                                                <input type="text" class="form-control {{ $model->exists ? "locked" : "" }}" name="filename" id="filename"
                                                       value="{{ old("filename",$model->filename) }}" maxlength="30"
                                                       {!! $model->exists ? 'readonly="readonly"' : '' !!}
                                                       aria-label="filename" aria-describedby="filename-label">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">.xml</span>
                                                </div>
                                            </div>
                                            <small id="helpId" class="form-text text-muted">
                                                {!! __('ContentPanel::seo.xml.access_name') !!} <kbd>a-z</kbd> <kbd>0-9</kbd>
                                            </small>
                                        </div>

                                    </div>
                                    <div class="col-1">
                                        <button id="filename-lock-toggler" type="button" class="btn btn-default" style="margin-top:28px;"><i class="fa fa-lock{!! $model->exists ? '-open' : '' !!}"></i></button>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-6">

                                <div class="form-group focus text-right">
                                    <input type="hidden" name="renew_freq" value="0">
                                    <div class="checkbox d-inline-block">
                                        <label>
                                            @php
                                                $renew_freq_options = [
                                                    "daily"     => __('ContentPanel::seo.xml.daily'), //"Günlük",
                                                    "3-days"    => __('ContentPanel::seo.xml.three_days'),  //"3 Günlük",
                                                    "weekly"    => __('ContentPanel::seo.xml.weekly'), //"Haftalık",
                                                    "monthly"   => __('ContentPanel::seo.xml.monthly'), //"Aylık",
                                                    "yearly"    => __('ContentPanel::seo.xml.yearly'),  //"Yıllık"
                                                ];
                                            @endphp
                                            {!! __('ContentPanel::seo.xml.renew_freq_1') !!}

                                            <select class="p-1" style="margin-top:-5px;" name="renew_freq">
                                                @foreach($renew_freq_options as $rfok => $rfov)
                                                    <option
                                                        value="{!! $rfok !!}" {!! old('renew_freq', $model->renew_freq) == $rfok ? 'selected' : '' !!}>
                                                        {{ $rfov }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            {!! __('ContentPanel::seo.xml.renew_freq_2') !!}
                                        </label>

                                    </div>

                                </div>

                            </div>
                        </div>

                        @php
                            $include_fields = [
                                "include_detail",
                                "include_categories",
                                "include_pages",
                            ];
                            $priority_fields = [
                                "detail_priority",
                                "categories_priority",
                                "pages_priority",
                            ];
                            $cf_fields = [
                                "detail_change_frequency",
                                "categories_change_frequency",
                                "pages_change_frequency",
                            ];

                            $fields = array_merge($include_fields, $priority_fields, $cf_fields);

                            $has_include_customization = false;
                            $has_priority_customization = false;
                            $has_cf_customization = false;

                        @endphp
                        @foreach($sitemaps as $sitemap)
                            @php
                                if($has_include_customization && $has_priority_customization && $has_cf_customization) {break;}

                                $dataset = count($blocks) ? ($blocks->where("sitemap_id",$sitemap->id)->first() ?? null) : null;
                                $default_settings = $sitemap->feature_tag == "homepage" ?  $default_for_homepage : $default_for_others;

                                foreach($fields as $field){
                                    try{
                                        if(! is_null($dataset) && ! is_null($dataset->$field) && $dataset->$field != $default_settings[$field]){
                                            if(in_array($field, $include_fields)){
                                                $has_include_customization = true;
                                            }elseif(in_array($field, $priority_fields)){
                                                $has_priority_customization = true;
                                            }elseif(in_array($field, $cf_fields)){
                                                $has_cf_customization = true;
                                            }
                                            break;
                                        }
                                    }catch(Exception $e){
                                        //nothing to do :)
                                    }
                                }
                            @endphp
                        @endforeach
                        <div class="row">{{--
                        <div class="title mt-5">Özelleştir</div>--}}
                            <table id="sitemaps_table" class="table">
                                <thead>
                                <tr>
                                    <th colspan="2" style="text-align: right;">{!! __('ContentPanel::seo.xml.customize_values') !!}</th>
                                    <th style="text-align: center;">
                                        <div class="checkbox d-inline-block">
                                            <label>
                                                <input type="checkbox"
                                                       id="customize_including"
                                                       class="column_content_toggler"
                                                       data-column="include-column"
                                                       name="customize_including"
                                                       {!! old('customize_including', ($has_include_customization ? 1 : 0) ) == 1 ? 'checked' : '' !!}
                                                       value="1">
                                            </label>
                                        </div>
                                    </th>
                                    <th style="text-align: center;">
                                        <div class="checkbox d-inline-block">
                                            <label>
                                                <input type="checkbox"
                                                       id="customize_priority"
                                                       class="column_content_toggler"
                                                       data-column="priority-column"
                                                       name="customize_priority"
                                                       {!! old('customize_priority', ($has_priority_customization ? 1 : 0) ) == 1 ? 'checked' : '' !!}
                                                       value="1">
                                            </label>
                                        </div>
                                    </th>
                                    <th style="text-align: center;">
                                        <div class="checkbox d-inline-block">
                                            <label>
                                                <input type="checkbox"
                                                       id="customize_change_freq"
                                                       class="column_content_toggler"
                                                       data-column="change_frequency_column"
                                                       name="customize_change_freq"
                                                       {!! old('customize_change_freq', ($has_cf_customization ? 1 : 0) ) == 1 ? 'checked' : '' !!}
                                                       value="1">
                                            </label>
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width:20%;">{!! __('ContentPanel::seo.xml.page_structure') !!}</th>
                                    <th style="width:42%;">{!! __('ContentPanel::seo.xml.section') !!}</th>
                                    <th style="width:9%;">{!! __('ContentPanel::seo.xml.include') !!}</th>
                                    <th style="width:9%;" class="priority-column">{!! __('ContentPanel::seo.xml.priority') !!}</th>
                                    <th style="width:20%;" class="change-frequency-column">Change Freq.</th>
                                    {{--<th>Eylemler</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $change_f_options = [
                                        "hourly"    => __('ContentPanel::seo.xml.hourly'),
                                        "daily"     => __('ContentPanel::seo.xml.daily'),
                                        "weekly"    => __('ContentPanel::seo.xml.weekly'),
                                        "monthly"   => __('ContentPanel::seo.xml.monthly'),
                                        "yearly"    => __('ContentPanel::seo.xml.yearly'),
                                        "never"     => __('ContentPanel::seo.xml.newer'),
                                        // customer should use this opt. only when the url is archived
                                        //"always"=>"Her zaman (always)", //commented out for the reason there wouldn't be such pages in our works
                                    ];


                                @endphp
                                @forelse($sitemaps as $sitemap)
                                    {{-- Ana sayfa için şimdilik özelleştirme yapılmasına gerek olmadığı için atlanıyor --}}
                                    {{-- Ana sayfa da özelleştirilebilir yapılmış ve bu kapsamda kod yazılmıştı sonradan kapatıldı --}}
                                    {{--@if($sitemap->feature_tag == "homepage")
                                        @continue
                                    @endif--}}
                                    @php
                                        $dataset = count($blocks) ? ($blocks->where("sitemap_id",$sitemap->id)->first() ?? null) : null;

                                        $default_settings = $sitemap->feature_tag == "homepage" ?  $default_for_homepage : $default_for_others;

                                        if(is_null($dataset)){

                                            $dataset = $default_settings;
                                        }else{
                                            $dataset = $dataset->toArray();
                                        }


                                        $categories = $sitemap->category ? 1 : 0;
                                        $pages = $sitemap->sitemapType->sitemap_type_type=="dynamic" ? 1 : 0;
                                        $static = $sitemap->sitemapType->sitemap_type_type=="static" && $sitemap->feature_tag!="homepage" ? 1 : 0;

                                        $page_point = $pages /*|| $static*/ ? 1 :0;
                                        $rows_count = 1+$page_point + $categories;
                                    @endphp
                                    <tr class="">
                                        <td rowspan="{{$rows_count}}">
                                            <strong
                                                class="text-default">
                                                {{  $sitemap->detail->name ?? trans('ContentPanel::seo.xml.page_structure') .' '.$sitemap->id  }}
                                            </strong>
                                        </td>
                                        <td>
                                            <strong>{!! __('ContentPanel::seo.xml.root_directory') !!}</strong>
                                            <span
                                                class="d-block text-muted mt-1">{{ $domain .$sitemap->detail->url }}</span>
                                        </td>
                                        <td class="include-column">
                                            <div class="customized">
                                                <input type="hidden" name="include_detail[{{$sitemap->id}}]" value="0">
                                                <div class="checkbox d-inline-block">
                                                    <label>
                                                        @php
                                                            $input_value = is_null($dataset["include_detail"]) ? $default_settings['include_detail'] : $dataset["include_detail"];
                                                        @endphp
                                                        <input type="checkbox" name="include_detail[{{$sitemap->id}}]"
                                                               {!! old('include_detail.'.$sitemap->id, $input_value ) == 1 ? 'checked' : '' !!}
                                                               value="1">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="auto">
                                                @if($default_settings["include_detail"])
                                                    <span class="text-success"><i
                                                            class="fa fa-2x fa-check-square"></i></span>
                                                @else
                                                    <span class="text-danger"><i class="fa fa-2x fa-times"></i></span>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="priority-column">
                                            <div class="customized">

                                                <input type="number" name="detail_priority[{{$sitemap->id}}]"
                                                       class="ml-1 mr-1 form-control"
                                                       step="0.01" max="1.00" min="0.10"
                                                       value="{{ old("detail_priority.".$sitemap->id,($dataset["detail_priority"] ?? ($default_settings["detail_priority"] ?? 0.8))) }}">
                                            </div>
                                            <div class="auto">
                                                <strong>{{ $default_settings["detail_priority"] }}</strong>
                                            </div>
                                        </td>
                                        <td class="change_frequency_column">
                                            <div class="customized">
                                                <select name="detail_change_frequency[{{$sitemap->id}}]">
                                                    @foreach($change_f_options as $cfok => $cfov)
                                                        <option
                                                            value="{!! $cfok !!}" {!! old('detail_change_frequency.'.$sitemap->id, $dataset["detail_change_frequency"] ) == $cfok ? 'selected' : '' !!}>
                                                            {{ $cfov }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="auto">
                                                <strong class="d-block w-100">{{ $change_f_options[$default_settings["detail_change_frequency"]] }}</strong>
                                            </div>
                                        </td>
                                    </tr>
                                    @if($categories)
                                        <tr>
                                            <td>
                                                <strong>{!! __('ContentPanel::seo.xml.categories') !!}</strong>
                                                <span class="d-block text-muted mt-1">{{ getSitemapCategoryUrlBase($sitemap, $sitemap->countryGroup, $sitemap->langauge)}}<i
                                                        class="fa fa-asterisk"></i> </span>
                                            </td>
                                            <td class="include-column">
                                                <div class="customized">
                                                    <input type="hidden" name="include_categories[{{$sitemap->id}}]"
                                                           value="0">
                                                    <div class="checkbox d-inline-block">
                                                        <label>
                                                            @php
                                                                $input_value = is_null($dataset["include_categories"]) ? $default_settings['include_categories'] : $dataset["include_categories"];
                                                            @endphp
                                                            <input type="checkbox"
                                                                   name="include_categories[{{$sitemap->id}}]"
                                                                   {!! old('include_categories.'.$sitemap->id, $input_value ) == 1 ? 'checked' : '' !!} value="1">
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="auto">
                                                    @if($default_settings["include_categories"])
                                                        <span class="text-success"><i class="fa fa-2x fa-check-square"></i></span>
                                                    @else
                                                        <span class="text-danger"><i class="fa fa-2x fa-times"></i></span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="priority-column">
                                                <div class="customized">
                                                    <input type="number"
                                                           name="categories_priority[{{$sitemap->id}}]"
                                                           class="ml-1 mr-1 form-control"
                                                           step="0.01" max="1.00" min="0.10"
                                                           value="{{ old("categories_priority.".$sitemap->id,($dataset["categories_priority"] ?? ($default_settings["categories_priority"] ?? 0.6))) }}">
                                                </div>
                                                <div class="auto">
                                                    <strong>{{ $default_settings["categories_priority"] }}</strong>
                                                </div>
                                            </td>
                                            <td class="change_frequency_column">
                                                <div class="customized">
                                                    <select name="categories_change_frequency[{{$sitemap->id}}]">
                                                        @foreach($change_f_options as $cfok => $cfov)
                                                            <option
                                                                value="{!! $cfok !!}" {!! old('categories_change_frequency.'.$sitemap->id, $dataset["categories_change_frequency"] ) == $cfok ? 'selected' : '' !!}>
                                                                {{ $cfov }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="auto">
                                                    <strong>{{ $change_f_options[$default_settings["categories_change_frequency"]] }}</strong>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($pages)
                                        <tr>
                                            <td><strong>{!! __('ContentPanel::seo.xml.pages') !!}</strong>
                                                <span class="d-block text-muted mt-1">{{ $domain .$sitemap->detail->slug }}/<i
                                                        class="fa fa-asterisk"></i> </span>
                                            </td>
                                            <td class="include-column">
                                                <div class="customized">
                                                    <input type="hidden" name="include_pages[{{$sitemap->id}}]" value="0">
                                                    <div class="checkbox d-inline-block">
                                                        <label>
                                                            @php
                                                                $input_value = is_null($dataset["include_pages"]) ? $default_settings['include_pages'] : $dataset["include_pages"];
                                                            @endphp
                                                            <input type="checkbox" name="include_pages[{{$sitemap->id}}]"
                                                                   {!! old('include_pages.'.$sitemap->id, $input_value ) == 1 ? 'checked' : '' !!}
                                                                   value="1">
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="auto">
                                                    @if($default_settings["include_pages"])
                                                        <span class="text-success"><i class="fa fa-2x fa-check-square"></i></span>
                                                    @else
                                                        <span class="text-danger"><i class="fa fa-2x fa-times"></i></span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="priority-column">
                                                <div class="customized">
                                                    <input type="number"
                                                           name="pages_priority[{{$sitemap->id}}]"
                                                           class="ml-1 mr-1 form-control"
                                                           step="0.01" max="1.00" min="0.10"
                                                           value="{{ old("pages_priority.".$sitemap->id,($dataset["pages_priority"] ?? ($default_settings["pages_priority"] ?? 0.4))) }}">
                                                </div>
                                                <div class="auto">
                                                    <strong>{{ $default_settings["pages_priority"] }}</strong>
                                                </div>
                                            </td>
                                            <td class="change_frequency_column">
                                                <div class="customized">
                                                    <select name="pages_change_frequency[{{$sitemap->id}}]">
                                                        @foreach($change_f_options as $cfok => $cfov)
                                                            <option
                                                                value="{!! $cfok !!}" {!! old('pages_change_frequency.'.$sitemap->id, $dataset["pages_change_frequency"] ) == $cfok ? 'selected' : '' !!}>
                                                                {{ $cfov }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="auto">
                                                    <strong>{{ $change_f_options[$default_settings["pages_change_frequency"]] }}</strong>
                                                </div>
                                            </td>
                                        </tr>

                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="5">
                                            {!! __('ContentPanel::seo.xml.no_page_structure') !!}
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn float-right">{!! __('ContentPanel::seo.xml.save') !!}</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        @endsection

        @push("specific.scripts")
            <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
        @endpush

        @push("scripts")
            <script>
                $(document).ready(function () {

                    $("input.column_content_toggler").each(function(ind,el){
                        toggleColumnContents($(el));
                    });

                    $("input.column_content_toggler").on("ifToggled", function(){
                        // alert("change");
                        toggleColumnContents($(this));
                    }).trigger("ifToggled");

                    $('button#filename-lock-toggler').click(function(){
                        let input = $("input#filename");
                        if(input.hasClass("locked")){
                            input.removeClass("locked").removeAttr("readonly").focus();
                            $(".fa",$(this)).removeClass("fa-lock-open").addClass("fa-lock")
                        }else{
                            input.addClass("locked").attr("readonly","true");
                            $(".fa",$(this)).removeClass("fa-lock").addClass("fa-lock-open")
                        }
                    });
                    /*$("input#auto_calculate_priority").on("ifChecked", function(){
                        $("th.priority-column, td.priority-column").hide();
                    })
                    $("input#auto_calculate_priority").on("ifUnchecked", function(){
                        $("th.priority-column, td.priority-column").show();
                    })
                    $("input#auto_calculate_priority").trigger("ifUnchecked");

                    $("input#disable_change_frequency").on("ifChecked", function(){
                        $("th.change-frequency-column, td.change-frequency-column").hide();
                    })
                    $("input#disable_change_frequency").on("ifUnchecked", function(){
                        $("th.change-frequency-column, td.change-frequency-column").show();
                    })
                    $("input#disable_change_frequency").trigger("ifUnchecked");*/
                });

                function toggleColumnContents(toggler_el){
                    let column_class=$(toggler_el).data("column");
                    let checked = $(toggler_el).prop("checked");
                    let hide = checked ? "auto" : "customized";
                    let show = checked ? "customized" : "auto";
                    $("table#sitemaps_table td." + column_class + " div."+hide).slideUp(200);
                    $("table#sitemaps_table td." + column_class + " div."+show).slideDown(200);

                    $("table#sitemaps_table td." + column_class + " div."+hide+" input,table#sitemaps_table td." + column_class + " div."+hide+" select").attr("disabled","disabled");
                    $("table#sitemaps_table td." + column_class + " div."+show+" input,table#sitemaps_table td." + column_class + " div."+show+" select").removeAttr("disabled");


                }

            </script>
    @endpush
