<?php

namespace Mediapress\Modules\Locale;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class LocaleServiceProvider extends ServiceProvider
{
    public const LOCALE = "Locale";
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = self::LOCALE;
    protected $namespace = 'Mediapress\Modules\Locale';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name . 'Panel');
        $this->publishActions(__DIR__);
        
        $files = $this->app['files']->files(__DIR__ . '/Config');
        //return dd($files);
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $this->app['config']->set("database.connections.mediapress_locale", ['driver' => 'sqlite', 'database' => __DIR__ . '/Database/Locale.db', 'prefix' => '']);

        $loader = AliasLoader::getInstance();
        $loader->alias(self::LOCALE, Locale::class);
        app()->bind(self::LOCALE, function () {
            return new \Mediapress\Modules\Locale\Locale;
        });
    }
    
    
    protected function mergeConfig($path, $key)
    {
        
        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }
                
            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }
}
