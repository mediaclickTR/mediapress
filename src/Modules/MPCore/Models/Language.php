<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class Language extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;

    protected $table = "languages";
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public $fillable = ["id", "code", "name", "alphabet", "native", "regional", "flag", "fontColor", "hexColor", "direction", "active", "default", "sort"];


}
