@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.create_maillist") !!}</h1>
        <div class="row">
            <div class="col-md-12">
                <form action="{!! route('Heraldist.mailchimp.maillists.store') !!}" novalidate method="POST">
                    @include("HeraldistPanel::mail_service.mailchimp.maillists.form")
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="application/javascript">
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
        $('#pre-selected-options').multiSelect({
            selectableHeader: "<div class='custom-header'>Seçilebilir</div>",
            selectionHeader: "<div class='custom-header'>Seçilen</div>",
        });
    </script>
@endpush
