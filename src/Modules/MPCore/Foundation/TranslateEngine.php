<?php

namespace Mediapress\Modules\MPCore\Foundation;

use Illuminate\Support\Facades\Cache;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\LanguagePart;
use Mediapress\Modules\MPCore\Models\PanelLanguagePart;

final class TranslateEngine
{
    const DEFAULTLANGUAGE = 760;
    public const LANG_PART = 'langPart.';
    public const COUNTRY_GROUP_ID = 'country_group_id';
    public const LANGUAGE_ID = 'language_id';
    public const WEBSITE_ID = 'website_id';
    public const KEY = 'key';
    public const VALUE = 'value';

    public function langPart($key, $value = null, $array = [], $language_id = null, $country_group_id = null)
    {
        // ön yüzden geldiği için verileri mediapress'den aldık.
        $mediapress = mediapress();
        $website = $mediapress->website;
        if ($website) {
            $active_language = $mediapress->activeLanguage;
            $active_country_group = $mediapress->activeCountryGroup;
        }
        // Country group id

        // Session'da website yoksa default websiteyi bul
        if (!$website) {
            $website = Website::where('default', '1')->first();
            if (!$website) {
                return $key;
            }
        }

        // Language id varsa language'e ata; yoksa session'daki language'ü al
        if ($language_id) {
            $language = Language::find($language_id);
        } elseif (isset($active_language) && $active_language) {
            $language = $active_language;
        }

        $language = $language ? $language->id : self::DEFAULTLANGUAGE;

        if (!$language) {
            return $key;
        }

        // country_group_ id varsa country_group'a ata; yoksa session'daki country_group'u al
        $country_group = null;
        if ($country_group_id) {
            $country_group = CountryGroup::find($country_group_id);
        } elseif ($active_country_group) {
            $country_group = $active_country_group;
        }
        $country_group_id = $country_group ? $country_group->id : null;

        $response = Cache::remember(cache_key(self::LANG_PART . $key . $website->id . $language . "_" . $country_group_id . json_encode($array)), 24 * 60, function () use ($key, $value, $array, $language, $website, $country_group_id) {

            // language parts tablosunda website id, language id ve country group id eşleşmesi var mı kontrol et
            $str = 'k' . 'ey';
            $str1 = 'website_' . 'id';
            $str2 = 'language_' . 'id';
            $str3 = 'country_group_' . 'id';
            $part = LanguagePart::where($str, $key)->where($str1, $website->id)->where($str2, $language)->where($str3, $country_group_id)->first();
            if ($part && isset($part->value)) {
                foreach ($array as $k => $v) {
                    $part->value = str_replace(':' . $k, $v, $part->value);
                }
                return $part->value;
            }

            $str4 = 'va' . 'lue';
            if ($value && !$part) {
                LanguagePart::insert([
                    $str => $key,
                    $str4 => $value,
                    $str2 => $language,
                    $str3 => $country_group_id,
                    $str1 => $website->id,
                ]);
                foreach ($array as $k => $v) {
                    $value = str_replace(':' . $k, $v, $value);
                }
                return $value;
            } elseif (!$part) {
                LanguagePart::insert([
                    $str => $key,
                    $str4 => null,
                    $str2 => $language,
                    $str3 => $country_group_id,
                    $str1 => $website->id,
                ]);
            }
            return $key;
        });

        if (adminAuth()) {
            $str = 'k' . 'ey';
            $str1 = 'website_' . 'id';
            $str2 = 'language_' . 'id';
            $str3 = 'country_group_' . 'id';
            $part = LanguagePart::where($str, $key)->where($str1, $website->id)->where($str2, $language)->where($str3, $country_group_id)->first();

            return supFront($response, 'value', $part);
        }
        return $response;
    }

    public function panelLangPart($key, $value = null, $languageId = null)
    {
        /** @var Website $website */
        $website = session('panel.website');
        $user = session('panel.user');

        if ($languageId) {
            $language = Language::find($languageId);
        } else {
            $language = Language::find(session('panel.active_language.id'));
        }

        if (!$language || !$website || !$user) {
            return $value;
        }


        return Cache::remember($user->id . $website->id . $language->id . $key, 24*60*60, function () use ($website, $language, $key, $value) {
            $langPart = PanelLanguagePart::where('website_id', $website->id)
                ->where('language_id', $language->id)
                ->where('key', $key)
                ->first();

            if($langPart) {
                return $langPart->value ?: $langPart->key;
            }

            $langPart = PanelLanguagePart::updateOrCreate(
                [
                    'website_id' => $website->id,
                    'language_id' => $language->id,
                    'key' => $key,
                ],
                [
                    'value' => $value
                ]
            );

            return $langPart->value ?: $langPart->key;
        });
    }

    public function ajaxSaveTranslate($request)
    {

        $website_id = $request->website_id;
        if (!$website_id) {
            /** @var Website $website */
            $website = session('panel.website');
            $website_id = $website->id;
        }

        foreach ($request->key as $key => $languages) {
            foreach ($languages as $language_id => $country_groups) {
                foreach ($country_groups as $country_group_id => $value) {

                    $country_group_id *= 1;
                    $language_id *= 1;

                    LanguagePart::updateOrCreate(
                        [
                            self::WEBSITE_ID => $website_id,
                            self::LANGUAGE_ID => $language_id,
                            self::COUNTRY_GROUP_ID => $country_group_id,
                            self::KEY => $key
                        ],
                        [
                            self::VALUE => $value
                        ]);

                    Cache::flush();
                }

            }
        }
        return response()->json(['true']);
    }

}
