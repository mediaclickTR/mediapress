<?php


namespace Mediapress\Foundation;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Log
{

    public $url;
    public $admin;
    public $modelData;
    public $find;
    public $data;
    public $request;

    public function insert($modelData, $data = null, $find = null)
    {

        $logging = config("mediapress.log.enable");
        if (!$logging) {
            return null;
        }

        $this->url = config("mediapress.log.url");

        if (Auth::guard('admin')->check()) {
            $this->admin = Auth::guard('admin')->user()->id;
        }
        $this->modelData = $modelData;
        $this->find = $find;
        $this->data = $data;
        $this->hash = Hash::make(uniqid());
        $this->request = Request::capture();
        return $this->makeLog();
    }

    public function makeLog()
    {
        $data = [];

        if (!$this->find) {
            return null;
        }
        if (isset($this->find[0]) && $this->find[0] == 'id') {
            $data['model_id'] = $this->find[2];
            foreach ($this->modelData as $key => $value) {
                $data['model_type'] = $key;
                break;
            }

            $data['hash'] = $this->hash;
            $data['admin_id'] = $this->admin;
            $data['type'] = 'active';
        }
        if (isset($this->data['data']['_token'])) {
            unset($this->data['data']['_token']);
        }
        $post = [
            'model_data' => $this->modelData,
            'find' => $this->find,
            'data' => $this->data,
            'hash' => $this->hash,
            'host' => $this->request->server('HTTP_HOST')
        ];

        $return = $this->post('insert', $post);

        if($return == 'true'){
            \Mediapress\Models\Log::create($data);
        }
        return $return;

    }

    public function retrive($hash){

        $logging = config("mediapress.log.enable");
        if (!$logging) {
            return null;
        }

        $this->url = config("mediapress.log.url");

        if (!$hash){
            return  response()->json(['false']);
        }
        return $this->post('retrive', ['hash'=>$hash]);


    }

    public function post($method, $data = [])
    {
        $data_string = json_encode($data);
        $ch = curl_init($this->url . $method);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        return curl_exec($ch);
    }
}
