@if ($paginator->hasPages())
    <div class="btn-group">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a href="javascript:void(0);" class="btn disabled">
                <i class="fa fa-angle-left"></i>
            </a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="btn">
                <i class="fa fa-angle-left"></i>
            </a>
        @endif

        <ul class="pagination" role="navigation">
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li><a class="btn" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </ul>


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="btn">
                <i class="fa fa-angle-right"></i>
            </a>
        @else
            <a href="javascript:void(0);" class="btn disabled">
                <i class="fa fa-angle-right"></i>
            </a>
        @endif
    </div>
@endif
