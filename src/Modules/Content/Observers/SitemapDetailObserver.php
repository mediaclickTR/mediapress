<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 21.03.2019
     * Time: 17:45
     */

    namespace Mediapress\Modules\Content\Observers;

    use Illuminate\Support\Str;
    use Mediapress\Modules\Content\Controllers\ContentController;
    use Mediapress\Modules\Content\Models\CategoryDetail;
    use Mediapress\Modules\Content\Models\SitemapDetail;
    use Mediapress\Modules\MPCore\Facades\URLEngine;
    use Mediapress\Modules\MPCore\Models\SettingSun;
    use Mediapress\Modules\MPCore\Models\Url;
    use Illuminate\Support\Facades\Cache;
    use Mediapress\Modules\Content\Models\PageDetail;
    use DB;

    class SitemapDetailObserver
    {

        public function created(SitemapDetail $model)
        {
            if($model->sitemap->urlStatus == 1) {
                $slug = $model->slug;
                $urltype = "original";
                $check_url = URLEngine::getPatternForSitemapDetail($model, $model->countryGroup, $model->language, $slug);
                $new_url_prefix = $check_url[0];
                $new_url_slug = $check_url[1] ? "/" . $check_url[1] : "";
                $new_url_str = $new_url_prefix . $new_url_slug;
                $do = URLEngine::create($model->parent->sitemap->websites()->first(), $new_url_str, $model, $urltype);
            }

            if ($model->parent->category ) {
                $catslug = ($model->category_slug == '/' || !$model->category_slug) ? Str::random(16) : $model->category_slug;
                $caturltype = ($model->category_slug == '/' || !$model->category_slug) ? "sm_cat_reserved" : "category";
                $check_url = URLEngine::getPatternForSitemapDetail($model, $model->countryGroup, $model->language, $catslug);
                $new_url_prefix = $check_url[0];
                $new_url_slug = "/" . $check_url[1];
                $new_url_str = $new_url_prefix . $new_url_slug;
                $do = URLEngine::create($model->parent->sitemap->websites()->first(), $new_url_str, $model, $caturltype);
            }
            Cache::flush();
            return true;
        }


        public function updating(SitemapDetail $model){
            // below code is concerned with a slug change only://in_array("slug", array_keys($pd->getDirty()))
            if(in_array("slug", array_keys($model->getDirty()))){
                // get a valid url string parts using the new data (slug) for detail object
                return $this->renewSlug($model, true);
            }
            if ($model->parent->category ) {
                if (in_array("category_slug", array_keys($model->getDirty()))) {
                    // get a valid url string parts using the new data (slug) for detail object
                    return $this->renewCategorySlug($model, true);
                }
            }

            Cache::flush();
            return true;
        }

        public function deleted(SitemapDetail $object){

            if($object->url){
                $object->url()->delete();
            }

            if($object->extras->isNotEmpty()) {
                $object->extras()->delete();
            }


            $page_ids = DB::table("pages")->select("id")->where('sitemap_id', $object->sitemap_id)->get()->pluck("id")->toArray();
            if(count($page_ids)){
                $page_details = PageDetail::whereIn('page_id', $page_ids)->where('country_group_id', $object->country_group_id)->where('language_id',$object->language_id)->get();
                foreach($page_details as $page_detail){
                    $page_detail->delete();
                }
            }

            Cache::flush();
        }


        // region UNDER CONSTRUCTION

        public function restoring(SitemapDetail $object){

            Cache::flush();

            if( ! $this->tryRestoringOldUrl($object)){
                if(Str::slug($object->slug)==""){
                    $slug = Str::slug($object->name);
                    if($slug==""){
                        $slug = Str::random(16);
                    }
                    $object->slug = $slug;
                }
                return $this->renewSlug($object, false);
            }

            if ($object->parent->category ) {
                if (!$this->tryRestoringOldCatUrl($object)) {
                    if (Str::slug($object->category_slug) == "") {
                        $slug = Str::slug($object->name);
                        if ($slug == "") {
                            $slug = Str::random(16);
                        }
                        $object->category_slug = $slug;
                    }
                    return $this->renewCategorySlug($object, false);
                }
            }
            return true;
        }

        private function tryRestoringOldUrl(SitemapDetail $object){
            $url = $object->url()->withTrashed()->first();
            if($url){
                $url->restore();
                return true;
            }
            return false;
        }

        private function tryRestoringOldCatUrl(SitemapDetail $object){
            $url = $object->categoryUrl()->withTrashed()->first();
            if($url){
                $url->restore();
                return true;
            }
            return false;
        }

        private function renewSlug(SitemapDetail $object, $force_clean_slug = true){

            $sd = $object;

            $check_url = URLEngine::getPatternForSitemapDetail($sd, $sd->countryGroup, $sd->language, $sd->slug);

            // produce the new whole url string
            $new_url_prefix = $check_url[0];
            $new_url_slug = "/" . $check_url[1];
            $new_url_str = $new_url_prefix . $new_url_slug;

            // throw an error if slug cleanising is not pre-performed.
            // User should send a valid slug string which already been sluggified.
            if ($force_clean_slug && $new_url_slug != $sd->slug) {
                throw new \Exception("Slug, temiz bir slug metni değil. Uyuşmayanlar: gönderilen hali \"$sd->slug\" ve filtrelenen hali \"$new_url_slug\"");
            }

            // retrieve the existing url model if there is one:
            // if there is not, there have been something exceptional. Should be analised.
            $existing_url_model = $sd->urls()->whereIn("type",["original", "reserved"])->first();
            // if we have a url model for this detail object
            if($existing_url_model){
                if($existing_url_model->type == "original" && $existing_url_model->url == $new_url_str){
                    // if new string for the url model is already same with the current one, there is no need to update.
                    return true;
                }
                // if url models type is "original" AND new url string is different
                // OR IF the url models type is "reserved"
                // url model should be updated as "original" with the new url string
                $do = URLEngine::update($sd->parent->sitemap->websites()->first(), $existing_url_model->url, $new_url_str, $sd, "original");
                if(!$do[0]){
                    throw new \Exception($do[1]);
                    return false;
                }
            }else{
                // there should already been an url model existing for any created detail object
                // if flow drops here that means there is something exceptional. Should be analised later.
                // Still need to make sure to have an url model with original type in this context:
                $do = URLEngine::create($sd->parent->sitemap->websites()->first(), $new_url_str, $sd," original");
                if(!$do[0]){
                    throw new \Exception($do[1]);
                    return false;
                }
            }

            return true;

        }


        private function renewCategorySlug(SitemapDetail $model, $force_clean_slug = true){
            // get a valid url string parts using the new data (slug) for detail object
            $check_url = URLEngine::getPatternForSitemapDetail($model, $model->countryGroup, $model->language, $model->category_slug);

            // produce the new whole url string
            $new_url_prefix = $check_url[0];
            $new_url_slug = "/" . $check_url[1];
            $new_url_str = $new_url_prefix . $new_url_slug;

            // throw an error if slug cleanising is not pre-performed.
            // User should send a valid slug string which already been sluggified.
            if ($force_clean_slug && $new_url_slug != $model->category_slug) {
                throw new \Exception("Category slug, temiz bir slug metni değil. Uyuşmayanlar: gönderilen hali \"$model->category_slug\" ve filtrelenen hali \"$new_url_slug\"");
            }

            // retrieve the existing url model if there is one:
            // if there is not one, there have been something exceptional. Should be analised later.
            $existing_url_model = $model->urls()->whereIn("type",["category", "sm_cat_reserved"])->first();

            // if we have a url model for this detail object
            if($existing_url_model){
                if($existing_url_model->type == "category" && $existing_url_model->url == $new_url_str){
                    // if new string for the url model is already same with the current one, there is no need to update.
                    return true;
                }
                // if url models type is "original" AND new url string is different
                // OR IF the url models type is "reserved"
                // url model should be updated as "original" with the new url string
                $do = URLEngine::update($model->parent->sitemap->websites()->first(), $existing_url_model->url, $new_url_str, $model, "category");
                if(!$do[0]){
                    throw new \Exception($do[1]);
                    return false;
                }

            }else{
                // there should already been an url model existing for any created detail object
                // if flow drops here that means there is something exceptional. Should be analised later.
                // Still need to make sure to have an url model with original type in this context:
                $do = URLEngine::create($model->parent->sitemap->websites()->first(), $new_url_str, $model,"category");
                if(!$do[0]){
                    throw new \Exception($do[1]);
                    return false;
                }
            }

            return true;
        }

        // endregion
    }
