@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="name">Çalıştırmak istediğiniz testi seçin</h1>
        <button id="test">Test</button>
        </div>
    </div>

@endsection
@push("scripts")
    <script>
        $(document).ready(function(e){
        $('#test').on('click',function () {

            var token = $("meta[name=csrf-token]").attr("content");
            $.ajax({
                url: '{{ route('MPCore.country_province.get_values') }}',
                data: {
                    "_token": token,
                    "country_ids":[1]
                },
                success:function(data, textStatus, jqXHR){

                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Sayfa yapısı ekleme/düzenleme için açılamadı: " + "\n"  + "\n"+ errorThrown + "\n" + (jqXHR.responseJSON.message !== undefined ? jqXHR.responseJSON.message : errorThrown));
                }
            });
        });
        });
    </script>
@endpush
