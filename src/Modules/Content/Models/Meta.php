<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Foundation\KeyValueCollection;
use Mediapress\Modules\MPCore\Foundation\MetaKeyValueCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Collection;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Support\Database\CacheQueryBuilder;

class Meta extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;

    public const FUNCTION1 = "function";
    public const OBJECT = "object";
    public const ARGS = "args";
    protected $table = 'metas';
    public $timestamps = true;
    protected $fillable = ["url_type", "url_id", 'key', 'desktop_value', 'desktop_enabled', 'order', 'type', 'template'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function url()
    {
        return $this->morphTo();
    }

    public function urls()
    {
        return $this->belongsTo(Url::class, 'url_id', 'id');
    }

    public function websiteUrls($website_id)
    {
        return $this->hasMany(Url::class)->where('website_id', $website_id);
    }

    public function getKeyValueForDevice($device){
        if( ! $this->desktop_enabled){
            return [];
        }

        $value = $this->desktop_value;

        return [$this->key=>$value];
    }

    public function render($device=null)
    {

        if($device == null){
            $device = (new UserAgent())->getDevice();
        }

        if( ! $this->desktop_enabled){
            return null;
        }

        $value = $this->desktop_value;

        if (is_string($this->template) && $value != null) {
            return preg_replace(['/\$0/m', '/\$1/m'], [$this->key, $value], $this->template);
        } else {
            return "";
        }
    }

    //For Extras

    public static function boot()
    {
        parent::boot();

        self::saving(function ($model) {
            if (!$model->order){
                $model->order = 0;
            }
            if (!$model->type){
                $model->type = "description";
            }

            if(\Str::startsWith($model->key, 'og:')) {
                $model->template = '<meta property="$0" content="$1">';
            } elseif($model->key == 'title') {
                $model->template = '<$0>$1</$0>';
            } else {
                $model->template = '<meta name="$0" content="$1">';
            }
        });
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($get){
        if($get=="model"){
            return $this;
        }else{
            return parent::__get($get);
        }
    }

    public function newCollection(array $models = [])
    {
        $debug = debug_backtrace(true);
        if ($debug[1][self::FUNCTION1] == "initRelation") {
            $model = $debug[1][self::ARGS][0][0];
            $method = $debug[1][self::ARGS][1];
            $object = $model->$method(); //OK
        } else if ($debug[1][self::FUNCTION1] == "getRelationValue") {
            $model = $debug[5][self::ARGS][0][0];
            $method = $debug[5][self::ARGS][1];
            $object = $model->$method(); //OK
        } else if ($debug[1][self::FUNCTION1] == "get") {
            $object = $debug[2][self::OBJECT] ?? null; //OK
        } else if ($debug[1][self::FUNCTION1] == "hydrate") {
            $object = $debug[4][self::OBJECT]; //OK
        } else if ($debug[1][self::FUNCTION1] == "getResults") {
            $object = $debug[1][self::OBJECT]; //OK
        } else {
            $object = null;
        }


        return $object === null ? new KeyValueCollection($models) : new MetaKeyValueCollection($models, $object);
    }

}
