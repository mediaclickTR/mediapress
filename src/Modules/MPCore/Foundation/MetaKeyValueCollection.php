<?php

namespace Mediapress\Modules\MPCore\Foundation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Modules\MPCore\Facades\TranslateEngine;
use Mediapress\Modules\Content\Models\Meta;
use Illuminate\Contracts\Pagination\Paginator;
use Mediapress\Foundation\KeyValueCollection;
use Arr;

class MetaKeyValueCollection extends KeyValueCollection
{
    public const OG_TITLE = "og:title";
    public const TEMPLATE = "template";
    const DESCRIPTION = "description";
    const TITLE = "title";

    public function render()
    {

        $device = (new UserAgent())->getDevice();
        $values = $this->reduce(function ($basket, $item) use ($device) {
            return ($basket .= $item->render($device) . "\n    ");
        });

        $noindex = $this->noIndexCache();

        $values .= $this->selfCalculate();

        return $values . (isset($noindex["robots"]) ? '<meta name="robots" content="noindex,nofollow, nosnippet, noarchive" />' : '');
    }

    public function getListForDevice($device = null, $format = "array", $block = [], $dontfix = [])
    {

        $device = (new UserAgent())->getDevice();

        $values = [];
        $this->each(function ($item, $key) use ($device, &$values) {
            /** @var Meta $item */
            $values = array_merge($values, $item->getKeyValueForDevice($device));
        });

        $values = array_merge($values, $this->noIndexCache());


        if (!isset($values[self::TITLE]) && ($dontfix[self::TITLE] ?? true)) {
            try {
                $values[self::TITLE] = $this->parent->url->model->name;
            } catch (\Exception $e) {
                $e = ${"e"};
            }
        }
        if (!isset($values[self::DESCRIPTION]) && ($dontfix[self::DESCRIPTION] ?? true)) {
            try {
                $values[self::DESCRIPTION] =  \Str::limit(strip_tags($this->parent->url->model->detail), 100);
            } catch (\Exception $e) {
                $e = ${"e"};
            }
        }

        if (!isset($values["canonical"]) && ($dontfix["canonical"] ?? true)) {
            try {
                $values["canonical"] =  url($this->parent->url->url);
            } catch (\Exception $e) {
                $e = ${"e"};
            }
        }


        $values = \Arr::except($values, $block);

        switch ($format) {
            case 'json':
                return json_decode($values);
            case 'array':
            default:
                return $values;
                break;
        }

    }


    public function setTemp($name, $value, $attributes = [])
    {
        //multi array
        $meta = $this->where('key', $name)->first();
        if ($meta) {
            $meta->desktop_value = $value;
        } else {
            $template = "<meta name=\"$0\" content=\"$1\">";
            if ($name == self::TITLE) {
                $template = "<$0>$1</$0>";
            }
            $attr = $attributes + ["key" => $name, "desktop_value" => $value, "type" => self::DESCRIPTION, self::TEMPLATE => $template, "indexed" => "index,follow"];
            $this->add(new Meta($attr));
        }
    }

    public function paginate($paginator)
    {
        if (!is_a($paginator, Paginator::class)) {
            throw new \Exception("Paginator not valid extended");
        }

        if ($paginator->previousPageUrl()) {
            $this->setTemp("prev", preg_replace('/\?page=1$/', '', $paginator->previousPageUrl()), ["type" => "paginator", self::TEMPLATE => "<link rel=\"$0\" href=\"$1\">"]);
        }

        if ($paginator->nextPageUrl()) {
            $this->setTemp("next", $paginator->nextPageUrl(), ["type" => "paginator", self::TEMPLATE => "<link rel=\"$0\" href=\"$1\">"]);
        }

        if ($this->title) {
            $this->setTemp(self::TITLE, $this->title . " - " . TranslateEngine::langPart("page", "Sayfa") . " " . $paginator->currentPage() . "/" . $paginator->lastPage());
        }

        if ($this->{self::OG_TITLE}) {
            $this->setTemp(self::OG_TITLE, $this->{self::OG_TITLE} . " - " . TranslateEngine::langPart("page", "Sayfa") . " " . $paginator->currentPage() . "/" . $paginator->lastPage());
        }
    }

    /**
     * get stored Key Value pair inside of eloquent model
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $model = $this->where('key', $name)->first();
        $template = "<meta name=\"$0\" content=\"$1\">";
        if ($name == self::TITLE) {
            $template = "<$0>$1</$0>";
        }
        return $model ? $model :
            $model = (clone($this->getResultsObject))->firstOrCreate(["key" => $name, self::TEMPLATE => $template]);
    }

    /**
     * set stored Key Value pair inside of eloquent model
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $template = "<meta name=\"$0\" content=\"$1\">";
        if ($name == self::TITLE) {
            $template = "<$0>$1</$0>";
        }
        $model = $this->where('key', $name)->first() ?? (clone($this->getResultsObject))->firstOrCreate(["key" => $name, self::TEMPLATE => $template]);
        if ($model->exists) {
            if (is_array($value)) {
                foreach ($value as $vk => $vv) {
                    $model->$vk = $vv;
                }
            } else {
                $model->desktop_value = $value;
            }
            $model->save();

        } else {
            dd("model yok");
        }


        /*if ($data = $this->where('key', $name)->first()) {
            if ($data->value != $value) {
                $data->value = $value;
                $data->update();
            }
            return;
        }
        $model = (clone($this->getResultsObject))->firstOrCreate(["key" => $name], ["value" => $value]);
        if ($model->exists && $model->value != $value) {
            $model->value = $value;
            $model->update();
        }*/
    }

    public function __toString()
    {
        return $this->render();
    }

    private function selfCalculate()
    {

        $items = collect($this->items);

        $values = '';
        if (!count($items->where('key', self::TITLE)->where('desktop_value', '!=', ''))) {


            try {
                $values .= "<title>" . $this->parent->url->model->name . "</title>\n";

            } catch (\Exception $e) {

            }
        }
        if (!count($items->where('key', self::DESCRIPTION)->where('desktop_value', '!=', ''))) {


            try {
                $values .= "<meta name=\"description\" content=\"" . \Str::limit(strip_tags($this->parent->url->model->detail), 100) . "\" />\n";

            } catch (\Exception $e) {

            }
        }

        try {
            $values .= "<link rel=\"canonical\" href=\"" . url($this->parent->url->url) . "\" />\n";

        } catch (\Exception $e) {

        }

        return $values;

    }

    public function noIndexCache($request = null, $server = null){
        $request = $request ?: $request = Request::capture();
        $server = $server ?: $request->server('HTTP_HOST');
        $return =  Cache::rememberForever('server.noindex.' . $server, function () use ($server) {
            $list = [
                'test.*',
                '*.mclck.com',
                '*.mediaclick.work',
                '*.test',
                '*.local',
                '*.dev'
            ];
            foreach ($list as $disable) {
                preg_match_all('/' . str_replace('*', '.*', $disable) . '/m', $server, $matches, PREG_SET_ORDER, 0);

                if (count($matches) > 0) {
                    return ["robots" =>"noindex,nofollow, nosnippet, noarchive"];
                }
            }
            return [];
        });

        return is_array($return) ? $return : [];

    }


}
