@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.emailsender") !!}</h1>
        @include("MPCorePanel::inc.errors")
       <div class="row">
           <div class="col-md-12">
               <form action="{!! route('Heraldist.mailchimp.emailsender.store') !!}" novalidate method="POST">
                   @include("HeraldistPanel::mail_service.mailchimp.emailsender.form")
               </form>
           </div>
       </div>
@endsection
