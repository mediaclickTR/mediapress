<?php

return [
    'status' => 'Durum',
    'upload' => 'Yükle',
    'save' => 'Kaydet',
    'new_add' => 'Yeni Ekle',
    'add' => 'Ekle',
    'edit' => 'Düzelt',
    'show' => 'Görüntüle',
    'change' => 'Değiştir',
    'remove' => 'Kaldır',
    'delete' => 'Sil',
    'preview' => 'Ön izle',
    'widget' => 'Eklentiler',
    'back' => 'Geri',
    'yes' => 'Evet',
    'no' => 'Hayır',
    'cancel' => 'Vazgeç',
    'transactions' => 'İşlemler',
    'actions' => 'İşlemler',
    'all_website_records' => 'Tüm Site Kayıtları',
    'clear_filters' => 'Filteleri Temizle',
    'selection' => 'Seçim Yapınız',
    'list_selection' => 'Listeden Seç',
    'general' => 'Genel',
    'create-title' => ':name Ekle',
    'edit-title' => ':name Düzenle',
    'selected-delete' => 'Seçilenleri Sil',

    'next' => 'İleri',
    'previous' => 'Geri',

    'id' => 'ID',
    'name' => 'Ad',
    'title' => 'Başlık',
    'type' => 'Tür',
    'image' => 'Resim',
    'name_entry' => 'Ad giriniz',

    'order' => 'Sıra',
    'order_entry' => 'Sıra giriniz',

    'users' => 'Üyeler',

    'slug' => 'Slug',
    'slug_entry' => 'Slug giriniz',

    'detail' => 'Detay',
    'detail_entry' => 'Detay giriniz',

    'note' => 'Not',
    'note_entry' => 'Not giriniz',

    'key' => 'Anahtar',
    'key_entry' => 'Anahtar giriniz',


    'created_at' => 'Kayıt Tarihi',
    'updated_at' => 'Güncelleme Tarihi',
    'deleted_at' => 'Silinme Tarihi',

    'reads' => 'Okunma',
    'read' => 'Okundu',
    'unread' => 'Okunmadı',
    'subject' => 'Konu',
    'email' => 'E-Posta',

    'nodata'=> 'Kayıtlı veri bulunamadı.',
    'select_service'=> 'Servis Seçiniz',
    'select_processing'=> 'İşlem Seçiniz',

    'mpmailler' => 'MPMailler',
    'mailchimp' => 'Mailchimp',
    'eligible' => 'Seçilebilir',
    'selected' => 'Seçildi',
];
