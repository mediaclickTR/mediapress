<?php

namespace Mediapress\Modules\Heraldist\Controllers;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\MPCore\Models\Extra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mediapress\Modules\Heraldist\Mail\EbulletinSender;
use Illuminate\Validation\Rule;

class WebEbulletinController extends Controller
{

    public const EMAIL = 'email';
    public const WEBSITE_ID = 'website_id';
    public const FILLED = 'filled';
    public const HERALDIST_PANEL_EBULLETIN_EMAIL = "HeraldistPanel::ebulletin.email";
    public const PHONE = 'phone';

    public function ebulletinSender(Request $request)
    {
        $this->validate($request,
        [
            'name' => 'min:3',
            self::EMAIL => Rule::unique('e_bulletins')->whereNull('deleted_at')
        ],
        [
            'name.min' => trans("MPCorePanel::validation.min.numeric", [self::FILLED,trans("HeraldistPanel::ebulletin.name")]),
            'email.required' => trans("MPCorePanel::validation.filled", [self::FILLED,trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL)]),
            'email.unique' => trans("MPCorePanel::validation.unique", [self::FILLED,trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL)]),
        ],
        [
            'name' => trans('HeraldistPanel::ebulletin.name'),
            self::EMAIL => trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL),
            self::PHONE => trans('HeraldistPanel::ebulletin.phone')
        ]);

        $ebulletin = new Ebulletin();
        $ebulletin->name = (ucfirst($request->{'name'})) ? ucfirst($request->{'name'}) : null;
        $ebulletin->email = ($request->{self::EMAIL}) ? $request->{self::EMAIL} : null;
        $ebulletin->phone = ($request->{self::PHONE}) ? $request->{self::PHONE} : null;
        $ebulletin->website_id = ($request->{self::WEBSITE_ID}) ? $request->{self::WEBSITE_ID} : 1;

        if ($ebulletin->save())
        {
            if($request->extras){
                $extraList = [];
                foreach ($request->extras as $key=>$extras){
                    foreach ($extras as $extra){
                        $extraList[] = Extra::insert(['key'=>$key,'value'=>$extra]);
                    }
                }
                $ebulletin->extras()->saveMany($extraList);
            }

            if(config('mediapress.mail.sender_function') && function_exists('MAIL_SENDER_FUNCTION')){
                MAIL_SENDER_FUNCTION('e-bulletin',$ebulletin);
            }else{
                Mail::to($ebulletin->email)->send(new EbulletinSender($ebulletin));
            }

            return response()->json(['response' => 'e-Bültene kayıt oldunuz. Lütfen e-posta kutunuzu kontrol ediniz.']);
        }
        else {
            return response()->json(['response' => 'e-Bültene kayıt sırasında hata oluştu. Lütfen daha sonra tekrar deneyiniz.'])->setStatusCode(422);
        }
    }
    public function eBulletinActivation($id){
        $ebulletin = Ebulletin::find(decrypt($id));

        if($ebulletin->activated == 1){
            throw new Exception("Hata oluştu", \GuzzleHttp\json_encode($ebulletin));
        }

        $ebulletin->activated = 1;
        $ebulletin->save();

        return view('vendor.mail.ebulletin_signed',compact('ebulletin'));
    }
}
