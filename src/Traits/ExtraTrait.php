<?php

namespace Mediapress\Traits;

use Mediapress\Modules\Content\Models\SitemapExtra;
use Illuminate\Database\Eloquent\Collection;

trait ExtraTrait
{
    public function extras()
    {
        return $this->morphMany(SitemapExtra::class, 'model');
    }
    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value))
        {
            return $extra->value;
        }

        return null;
    }

    public function __get($key)
    {
        $attribute = $this->getAttribute($key);

        if ($attribute) {
            return $attribute;
        }

        if (isset($this->extras) && $this->extras instanceof Collection) {
            $extra = $this->extras->where('key', $key)->first();
        } else if (method_exists($this, 'extras')) {
            $extra = $this->extras()->where('key', $key)->first();
        }

        if (isset($extra->value)) {
            return $extra->value;
        }
    }


}