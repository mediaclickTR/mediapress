@extends("HeraldistPanel::inc.module_main")
@section('content')
    <div class="page-content">
        <div class="title">
            {!! trans("MPCorePanel::general.select_processing") !!}
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler/Maillists') !!}"><button class="btn btn-primary">{!! trans("HeraldistPanel::mail_service.list_manage") !!}</button></a>
            </div>
            <div class="col-md-4">
                <a href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler/Mailtemplates') !!}"><button class="btn btn-primary">{!! trans("HeraldistPanel::mail_service.template_manage") !!}</button></a>
            </div>
            <div class="col-md-4">
                <a href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler/EmailSender') !!}"><button class="btn btn-primary">{!! trans("HeraldistPanel::mail_service.emailsender") !!}</button></a>
            </div>
        </div>
    </div>
@endsection
