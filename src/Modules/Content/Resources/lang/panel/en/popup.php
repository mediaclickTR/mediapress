<?php

return [
    "list_title" => "Popup List",
    "add_new_popup" => "Add Popup",
    "type" => "Type",
    "languages" => "Languages",
    "copy" => "Copy",

    'order' => 'Order',
    'name' => ":name Name",

    'type_direction' => [
        'center' => 'Center',
        'right_left' => 'Right/Left',
        'bottom_up' => 'Bottom/Upper Bar',
        'fullscreen' => 'Fullscreen',
    ],

    'place' => ':name Place',

    'settings' => 'Settings',

    'screening_place' => 'Screening Place',

    'device' => [
        'title' => 'Device',
        "desktop" => "Desktop",
        "mobile" => "Mobile",
        "tablet" => "Tablet",
    ],

    'screening_page' => [
        'title' => 'Screening Page',
        'all' => 'All Page',
        "specific_page" => "Specific Pages",
        "specific_page_title" => "Screening Pages",
        "homepage" => "Home page",
        "custom_url" => "Custom Url",
        "custom_url_title" => "Page Urls",
    ],

    'limits' => [
        'title' => 'Limits',
        'how_much' => 'How many times will the same person see?',
        'continuous' => ' Continuous',
        'same_device' => 'Same Device',
        'same_session ' => ' Same Session ',
        'count_1' => 'It will be show',
        'count_2' => 'times in same session.'
    ],

    'times' => [
        'title' => 'Set Duration',
        'when' => 'When?',
        'when_continuous' => 'Continuous',
        'specific_dates' => 'On certain dates',
        'start_date' => 'Start Date',
        'finish_date' => 'End Date',
        'how_many_seconds' => 'How many seconds should you appear?',
        'second_continuous' => 'Continuous Display',
        'specific_seconds' => 'Specific Duration (seconds)',
        'trigger_button' => 'Until You Trigger the Button',
    ],

    'trigger' => [
        'title' => 'Triggers',
        'which_condition' => 'Under what conditions?',
        'time' => 'Duration (seconds)',
        'pixel' => 'Scroll (pixel)',
    ],

    'content' => [
        'title' => 'Content',
        'name_display' => 'Does the :name name appear?',
        'content_only_image' => 'Is Tooltip Content Visual?',
        'content' => ':name Content',
        'button_type' => 'Button Type',
        'button_type_1' => 'Close Button',
        'button_type_2' => 'Confirm Button',
        'button_text' => 'Button Text',
    ],


    'design' => [
        'title' => 'Design',
        'font' => 'Font',
        'border_size' => 'Border Width',
        'border_color' => 'Border Color',
        'padding_size' => 'Padding Value',
        'button_bg ' => 'Button Background Color',
        'button_color' => 'Button Color',
        'full_bg' => 'Background Fade Status',
        'background' => 'Background Color of Box',
        'color' => 'Color of Text',
        'a_color' => 'Link Color',
        'title_size' => 'Title Font Size',
        'title_color' => 'Title Font Color',
        'title_animation' => 'Title Animation',
        'title_space' => 'Title Space',
        'content_font_size' => 'Title Font Spacing',
        'content_animation' => 'Content Animation',
        'btn_font_size' => 'Move Button Font Size',
        'btn_position' => 'Move Button Position',
        'counter_font_size' => 'Counter Font Size ',
        'border_radius' => 'Corner Sharpness Size',
        'custom_css' => 'Custom Style',
    ],

    'animations' => [
        'fadeIn' => 'Come From Center',
        'fadeInLeft' => 'Come From Left',
        'fadeInRight' => 'Come Right',
        'fadeInDown' => 'Come From Below',
        'fadeInUp' => 'Come From the Top',
    ],

    'positions' => [
        'bottom' => 'Bottom',
        'up' => 'Top',
        'left' => 'Left',
        'right' => 'Right',
        'left_bottom' => 'Bottom Left',
        'right_bottom' => 'Bottom Right',
    ],
];
