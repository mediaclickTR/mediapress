<?php

namespace Mediapress\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;

class Install extends Command
{

    protected $signature = 'mp:install';

    protected $description = 'Command description';

    private $ds = DIRECTORY_SEPARATOR;

    private $mainRouteFile = "<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the \"web\" middleware group. Now create something great!
|
*/

";
    private $appBladeFile = '@extends(\'mediapress::main\')
@push(\'styles\')
<link rel="shortcut icon" href="{!! settingSun(\'logo.favicon\') ? image(settingSun(\'logo.favicon\')) : asset(\'vendor/mediapress/images/favicon.ico\') !!}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
@endpush
@include("web.inc.header")
@include("web.inc.footer")
@prepend(\'scripts\')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
@endprepend';

    private $headerBladeFile = '@section(\'header\')
    @include("ContentWeb::default.layouts.header")
@endsection';

    private $footerBladeFile = '@section(\'footer\')
    @include("ContentWeb::default.layouts.footer")
@endsection';

    private $searchBladeFile = '@extends(\'web.inc.app\')
    @section(\'content\')
    @include("ContentWeb::default.search")
@endsection';

    public function handle()
    {
        $this->makeModulesFolder();
        $this->removeMainRoute();
        $this->createMainAppBlade();
        $this->updateFileSystem();
        $this->gitIgnore();
        $this->ErrorBlade();
        $this->blackList();

        Artisan::call('mp:debugbar');
        $this->info("Debugbar Fixed !!");

        Artisan::call('mp:search');
        $this->info("Search Table Setted !!");
    }

    private function blackList()
    {
        $file = base_path('config'.DIRECTORY_SEPARATOR.'app.php');
        $config = file_get_contents($file);

        $blacklist = '

    \'debug_blacklist\' => [
        \'_ENV\' => array_keys( $_ENV ),
        \'_SERVER\' => array_keys( $_SERVER ),
        \'_POST\' => array_keys( $_POST ),
        \'_GET\' => array_keys( $_GET ),
        \'_COOKIE\' => array_keys( $_COOKIE ),
    ],

];';

        if( !Str::contains($config,"'debug_blacklist'") ) {
            $config = str_replace('];', $blacklist, $config);
            file_put_contents($file,$config);
        }
        $this->info("Blacklist is adjusted.");
    }

    private function makeModulesFolder()
    {

        $folder = app_path('Modules' . $this->ds . 'Content');
        if(!file_exists($folder)){
            $oldmask = umask(0);
            mkdir($folder, 0777,true);
            umask($oldmask);
            $this->info("Modules folder has been created");
        }else{
            $this->info("Modules folder is already exists!");
        }

    }

    private function removeMainRoute()
    {
        $file = base_path('routes'.$this->ds.'web.php');

        $content = file_get_contents($file);

        if(strpos($content,'view(\'welcome\');') !== false){
            file_put_contents($file,$this->mainRouteFile);
            $this->info("Main route file has been replaced");
        }else{
            $this->info("Main route file was changed already");
        }

    }

    private function createMainAppBlade()
    {
        $views = 'views';
        $folder = resource_path($views.$this->ds.'web'.$this->ds.'inc');
        $appFile = resource_path($views.$this->ds.'web'.$this->ds.'inc'.$this->ds.'app.blade.php');
        $headerFile = resource_path($views.$this->ds.'web'.$this->ds.'inc'.$this->ds.'header.blade.php');
        $footerFile = resource_path($views.$this->ds.'web'.$this->ds.'inc'.$this->ds.'footer.blade.php');

        $searchFolder = resource_path($views.$this->ds.'search');
        $searchFile = resource_path($views.$this->ds.'search'.$this->ds.'results.blade.php');
        if(!file_exists($appFile)){
            if(!file_exists($folder)){
                $oldmask = umask(0);
                mkdir($folder, 0755,true);
                umask($oldmask);
            }
            if(!file_exists($searchFolder)){
                $oldmask = umask(0);
                mkdir($searchFolder, 0755,true);
                umask($oldmask);
            }
            file_put_contents($appFile,$this->appBladeFile);
            file_put_contents($headerFile,$this->headerBladeFile);
            file_put_contents($footerFile,$this->footerBladeFile);
            file_put_contents($searchFile,$this->searchBladeFile);
            @unlink(resource_path($views.$this->ds.'welcome.blade.php'));
            $this->info("Main app blade has beed created");
        }else{
            $this->info("Main app blade is already exists");
        }

    }

    private function updateFileSystem()
    {
        $file = base_path('config'.DIRECTORY_SEPARATOR.'filesystems.php');
        $config = file_get_contents($file);

        $config = str_replace('storage_path(\'app\')','public_path(\'uploads\')',$config);
        file_put_contents($file,$config);
        $this->info("File systems changed");
    }

    private function gitIgnore()
    {
        $gitignore = base_path('.gitignore');
        $content = file_get_contents($gitignore);

        if(strpos($content,'composer.lock') !==false){
            $this->info("GitIgnore is not changed");
        }else{

             $content .= "
/public/uploads
/.idea
composer.lock
/public/vendor
package-lock.json";
            file_put_contents($gitignore,$content);
            $this->info("GitIgnore is changed");

        }

    }

    private function ErrorBlade()
    {
        $view = resource_path('views/errors/404.blade.php');
        if(file_exists($view)){
            $content = file_get_contents($view);
            if(strpos($content, 'errors::minimal') !== false) {

                $content = '
@php
if(!isset($mediapress)){
$mediapress = mediapress();
}

@endphp
@extends(\'web.inc.app\')

@section(\'content\')

    <section>
        <div class="page-content p_b_0">
            <article>
                <div class="container-fluid">
                    <div class="a404 text-center">
                        <h2>Sayfa Bulunamadı</h2>
                        <span>Ulaşmaya çalıştığınız sayfa bulunmadı.</span>
                        <p>Lütfen ulaşmak istediğiniz sayfanın adresini doğru yazdığınızı kontrol edin.</p>
                        <p>Eğer doğru adresi yazdığınıza eminseniz, ulaşmak istediğiniz sayfa silinmiş olabilir.</p>
                        <a href="{!! $mediapress->homePageUrl !!}" class="btns">Ana Sayfaya Dön</a>
                    </div>
                </div>
            </article>
        </div>
    </section>
@endsection';
                file_put_contents($view, $content);
                $this->info("404 blade is configured");

            } else {
                $this->info("404 blade is not changed");
            }
        }else{
            $this->error(" 404 blade is not found make php artisan vendor:publish ");
        }

    }
}
