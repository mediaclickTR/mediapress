<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mediapress\Modules\Entity\Facades\Entity;
use Mediapress\Modules\MPCore\Facades\MPCore;

class PanelMembersController extends Controller
{
    public const MAILLISTID = 'maillistid';
    public const HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX = 'Heraldist.mailchimp.maillists.members.index';
    public const HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX1 = self::HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX;
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const ACCESSDENIED = 'accessdenied';
    public const LISTS = "lists/";
    public const EMAIL_ADDRESS = 'email_address';
    public const MEMBERS = "/members/";
    public const MESSAGE = 'message';
    protected $apiKey;
    protected  $apiUrl;


    public function __construct()
    {
        $mailChimpApiKey = settingSun('heraldist.mailchimp.apikey');
        // API KEY KONTROL
        if($mailChimpApiKey!=''){
            $this->apiKey = $mailChimpApiKey;
        }
        else{
           // dd("Mailchimp api key bulunamadı.5");
            return;
        }

        // APi URL
        $dataCenter = substr($this->apiKey,strpos($this->apiKey,'-')+1);
        $this->apiUrl = "https://".$dataCenter.".api.mailchimp.com/3.0/";
    }

    public function index($id)
    {
        if(!userAction('message.forms.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $maillistid = $id;
        // Maillist name için
        $maillist = curlGenerator($this->apiUrl. self::LISTS .$id,$this->apiKey,"GET");
        // Curl list
        $members = curlGenerator($this->apiUrl. self::LISTS .$maillistid."/members",$this->apiKey,"GET");
        $total = $members->total_items;
        $members = $members->members;


        return view('HeraldistPanel::mail_service.mailchimp.maillists.members.index', compact('members', self::MAILLISTID,'maillist','total'));
    }

    public function create($id)
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = Entity::getMaillist();

        $maillistid = $id;

        return view("HeraldistPanel::mail_service.mailchimp.maillists.members.create", compact("mailtemplates","data", self::MAILLISTID));
    }

    public function store($id,Request $request)
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $maillistid = $id;

        $fields = [
            'emails' => 'E-Posta Adresi'
        ];
        $rules = [
            'emails' => 'required',

        ];
        $messages = [
            'emails.required' => 'E-Posta boş bırakılmamalıdır.',
        ];

        $this->validate($request, $rules, $messages, $fields);

        if($request->emails) {
            foreach ($request->emails as $email)
            {
                $json = json_encode([
                    self::EMAIL_ADDRESS => $email,
                    'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
                ]);

                $memberId = md5(strtolower($email));
                // curl store
                curlGenerator($this->apiUrl. self::LISTS .$id. self::MEMBERS .$memberId,$this->apiKey,"PUT",$json);
            }
        }
        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX1, compact(self::MAILLISTID)))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function edit($id,$memberid)
    {
        $maillistid = $id;
        $member = curlGenerator($this->apiUrl. self::LISTS .$id. self::MEMBERS .$memberid,$this->apiKey,"GET");
        return view("HeraldistPanel::mail_service.mailchimp.maillists.members.edit", compact("member", self::MAILLISTID));
    }

    public function update($id,$memberid,Request $request)
    {
        $maillistid = $id;
        $fields = [
            self::EMAIL_ADDRESS => 'E-Posta Adresi'
        ];
        $rules = [
            self::EMAIL_ADDRESS => 'required',

        ];
        $messages = [
            'emails.required' => 'E-Posta boş bırakılmamalıdır.',
        ];


        $this->validate($request, $rules, $messages, $fields);

        $json = json_encode([
            self::EMAIL_ADDRESS => $request->email_address,
            'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
        ]);

        curlGenerator($this->apiUrl. self::LISTS .$id. self::MEMBERS .$memberid,$this->apiKey,"PATCH",$json);

        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX1,compact(self::MAILLISTID)))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id,$memberid)
    {
        if(!userAction('message.maillists.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $maillistid = $id;
        //Curl delete
        curlGenerator($this->apiUrl. self::LISTS .$id. self::MEMBERS .$memberid,$this->apiKey,"DELETE");

        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_MEMBERS_INDEX1, compact(self::MAILLISTID)))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

}
