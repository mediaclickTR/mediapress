<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 20.03.2019
 * Time: 16:18
 */

namespace Mediapress\Traits;

use Mediapress\FileManager\Models\MFile;

trait HasMFiles
{

    public function mfiles($file_key = null)
    {
        $files = $this->morphToMany(MFile::class, "model", "mfile_general", "model_id", "mfile_id");

        if ($file_key) {
            $files = $files->where('file_key', $file_key);
        }
        return $files->orderBy('ordernum')->withPivot('id', 'file_key', 'details');
    }

    public function setSyncMFilesAttribute($value)
    {
        $files = $value;

        $mfiles = $this->mfiles()->get();

        $hold = [];
        foreach($mfiles as $mfile) {
            $pivot = $mfile->pivot;
            if($pivot) {
                $hold[$mfile->id . $pivot->file_key] = $pivot->details;
            }
        }

        $this->mfiles()->sync([]);

        $order = 0;
        foreach ($files as $file_key => $ids_concatted_str) {
            $file_ids = json_decode($ids_concatted_str);
            foreach ($file_ids as $file_id) {
                if(isset($hold[$file_id.$file_key])) {
                    $this->mfiles()->attach($file_id, ["file_key" => $file_key, "ordernum" => $order, "details" => $hold[$file_id.$file_key]]);
                } else {
                    $this->mfiles()->attach($file_id, ["file_key" => $file_key, "ordernum" => $order]);
                }
                $order++;
            }
        }
    }


    public function getFiles($file_key = null)
    {

        return $this->mfiles($file_key)->get();
    }

}
