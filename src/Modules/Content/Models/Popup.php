<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Support\Database\CacheQueryBuilder;

class Popup extends Model
{
    use SoftDeletes,CacheQueryBuilder;

    protected $table = 'popups';

    public $timestamps = true;

    protected $guarded = [
        'id'
    ];

    protected $dates = [
        'start_date',
        'finish_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = ['devices' => 'array'];


    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function website(){
        return $this->belongsTo(Website::class);
    }

    public function urls()
    {
        return $this->belongsToMany(Url::class, 'popup_urls');
    }

    public function customUrls()
    {
        return $this->hasMany(PopupCustomUrl::class);
    }

    public function languages($country_group_id = null)
    {
        if(! is_null($country_group_id)) {
            return $this->belongsToMany(Language::class, 'popup_languages')->where('country_group_id', $country_group_id)->get();
        }
        return $this->belongsToMany(Language::class, 'popup_languages');
    }
    public function country_groups()
    {
        return $this->belongsToMany(CountryGroup::class, 'popup_languages');
    }
}
