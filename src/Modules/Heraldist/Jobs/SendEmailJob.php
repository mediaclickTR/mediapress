<?php

namespace Mediapress\Modules\Heraldist\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emails;
    protected $data;
    protected $subject;
    protected $title;

    public function __construct($emails,$data,$subject,$emailSender,$title)
    {
        $this->emails = $emails;
        $this->data = $data;
        $this->subject = $subject;
        $this->emailSender = $emailSender;
        $this->title = $title;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('HeraldistPanel::mail_service.mpmailler.mail_templates.template', $this->data, function($message)
        {
            $message->to($this->emails)->subject($this->subject);
            $message->from($this->emailSender,$this->title);
        });
    }
}