<?php

return [
    "contact.management.title"=>"İletişim Yönetimi",
    "record"=>[
        "datasource"=> "Veri kaynağı",
        "total"=>"Toplam Kayıt Sayısı"
    ],
    "available"=>[
        "email"=>"Uygun E-Posta",
        "phone"=>"Uygun Telefon",
    ],
    "send"=>"Toplu E-Posta & SMS Gönder",
    "select"=>[
        "template"=>"Seçim",
        "content"=>"İçerik",
        "type"=>"Tür",
        "email"=>"E-Posta",
        "sms"=>"SMS",
    ],
    "email.subject"=>"Başlık & Konu",
    "email.detail"=>"İçerik",
    "sending"=>"E-Posta & SMS gönderim işlemi başlatıldı! Lütfen bekleyiniz..",
    "success.import"=>"Veriler etkileşim hedefleri tablosuna başarıyla aktarıldı. "
];