@extends('ContentPanel::inc.module_main')
@push('styles')
    <style>
        @media only screen and (max-width:1250px){
            .detail-meta-templates-editor .add-variable:before{
                display: none;
            }
            .add-variable span{
                padding: 0;
                float:none;
            }
        }
        html{
            font-size:14px;
        }
        .form-control:focus {
            box-shadow: none;
        }
        input.sitemap_name{
            height: 33px !important;
            padding: .375rem .75rem !important;
            font-size: 1.25rem !important;
            color: #007bff !important;
        }
        table#sitemap_slugs_table td{
            vertical-align: middle;
        }
        .detail-meta-templates-editor .rich-meta-input {
            display: block;
            width: 100%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            overflow: auto;
            height: auto;
            max-width: 1000px;
            width: 100%;
            min-height: 38px;
        }

        .detail-meta-templates-editor .rich-meta-input p,
        .detail-meta-templates-editor .rich-meta-input span,
        .detail-meta-templates-editor .rich-meta-input div,
        .detail-meta-templates-editor .rich-meta-input b,
        .detail-meta-templates-editor .rich-meta-input i,
        .detail-meta-templates-editor .rich-meta-input em {
            display: inline !important;
            margin: 0;
        }
        .detail-meta-templates-editor .rich-meta-input br {
            display: none;
        }
        .detail-meta-templates-editor .rich-meta-input span {
            position: relative;
            border-radius: 500px;
            background-color: #28A9D9;
            text-align: center;
            margin: 2px;
            color: #fff;
            font-size: 12px;
            padding: 5px 20px;
            padding-right: 30px;
            pointer-events: none;
            display: inline-block !important;
        }
        .detail-meta-templates-editor .rich-meta-input span:before {
            content: '';
            width: 1px;
            height: 100%;
            background-color: rgba(255, 255, 255, 0.34);
            top: 0;
            bottom: 0;
            right: 20px;
            pointer-events: none;
            position: absolute;
        }
        .detail-meta-templates-editor .rich-meta-input span i {
            pointer-events: auto;
            cursor: pointer;
            position: absolute;
            right: 8px;
            top: 11px;
            line-height: 5px;
            font-size: 12px;
            font-style: unset;
            color: #fff;
        }

        .detail-meta-templates-editor .add-variable {
            color: #393939;
            font-size: 14px;
            border: 1px solid #CECECE;
            border-radius: 5px;
            background: #ECECEC;
            text-align: center;
            height: 38px;
            line-height: 36px;
            cursor: pointer;
            position: relative;
            padding: 0 0 0 35px;
        }
        .detail-meta-templates-editor .add-variable:before {
            content: "+";
            position: absolute;
            top: 0;
            left: 0;
            border-right: 1px solid #CECECE;
            height: 36px;
            line-height: 33px;
            font-size: 22px;
            font-weight: bold;
            width: 35px;
        }

        .meta-variables-modal {
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(214, 214, 214, 0.6);
            z-index: 1000; }
        .meta-variables-modal .centered {
            background-color: #fff;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
            border: 1px solid #C2C2C2;
            position: absolute;
            top: 50%;
            transform: translateY(-50%) translateX(-50%);
            width: 370px;
            left: 50%;
            padding: 20px;
            z-index: 1; }
        .meta-variables-modal .centered .clse {
            cursor: pointer;
            float: right;
            font-size: 27px;
            line-height: 22px;
            color: #CECECE;
            position: relative;
            top: -13px;
            right: -11px; }
        .meta-variables-modal ul {
            list-style: none;
            padding: 0;
            background-color: #fff;
            max-height: 400px;
            min-height: 400px;
            overflow: hidden;
            overflow-y: auto;
            margin: 10px 0px;
            /* width */
            /* Track */
            /* Handle */
            /* Handle on hover */ }
        .meta-variables-modal ul li {
            padding: 5px 10px;
            border-bottom: 1px solid #f2f2f2;
            cursor: pointer; }
        .meta-variables-modal ul li.active {
            background-color: #D6D6D6; }
        .meta-variables-modal ul li:last-child {
            border-bottom: 0; }
        .meta-variables-modal ul::-webkit-scrollbar {
            width: 7px; }
        .meta-variables-modal ul::-webkit-scrollbar-track {
            background: #f1f1f1; }
        .meta-variables-modal ul::-webkit-scrollbar-thumb {
            background: #fff;
            border: 1px solid #D6D6D6; }
        .meta-variables-modal ul::-webkit-scrollbar-thumb:hover {
            background: #555; }
        .meta-variables-modal input {
            border-radius: 5px;
            padding: 10px 15px;
            border: 1px solid #CECECE;
            width: 100%;
            outline: none; }
        .meta-variables-modal button {
            width: 100%;
            border-radius: 5px;
            background-color: #ECECEC;
            border: 1px solid #CECECE;
            color: #393939;
            padding: 10px;
            cursor: pointer; }
        .meta-variables-modal:after {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0; }


    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! $sitemap->detail->name !!}
                </div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.meta_templates.index') }}" class="btn btn-light"><i class="fa fa-chevron-left"></i>{!! __('ContentPanel::general.go_back_list') !!}</a>
            </div>
        </div>
        <div class="p-30">
            <div class="col-md-12 p-0">
                @include("ContentPanel::meta_templates.form")
            </div>
        </div>
    </div>
@endsection
