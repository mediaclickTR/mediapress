<?php

namespace Mediapress\Modules\Interaction\Facades;

use Illuminate\Support\Facades\Facade;

class Interaction extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\Interaction\Interaction::class;
    }
}