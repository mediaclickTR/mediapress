<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Mediapress\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Cache;

class AssetController extends BaseController
{

    protected function css($names)
    {
        if(config('mediapress.asset_cache')) {
            return $this->getCss($names,true);
        } else {
            $data = $this->getCss($names);
        }

        $lastModified = date("D, d M Y H:i:s GMT", $data['lastModified']);

        return response($data['css'], 200)->header('Content-Type', 'text/css')
            ->header("pragma", "public")
            ->header("Cache-Control", "public, max-age=31536000")
            ->header("Last-Modified", $lastModified)
            ->header("Etag", $data['etag']);
    }


    protected function js($names)
    {
        if(config('mediapress.asset_cache')) {

                return $this->getJs($names,true);

        } else {
            $data = $this->getJs($names);
        }

        $lastModified = date("D, d M Y H:i:s GMT", $data['lastModified']);

        return response($data['js'], 200)->header('Content-Type', 'application/javascript')
            ->header("pragma", "public")
            ->header("Cache-Control", "public, max-age=31536000")
            ->header("Last-Modified", $lastModified)
            ->header("Etag", $data['etag']);
    }


    protected function mediapressCSS($names)
    {
        $cache = Cache::rememberForever(md5($names . 'css'), function () use ($names) {
            $nameArr = explode('|', $names);

            $hold = "";
            $fileNames = "";
            foreach ($nameArr as $name) {
                $minFile = public_path('vendor/mediapress/css/' . $name . '.min.css');
                $file = public_path('vendor/mediapress/css/' . $name . '.css');

                $lastModified = 0;
                $etag = "";
                if (file_exists($minFile)) {
                    if (filemtime($minFile) > $lastModified) {
                        $lastModified = filemtime($minFile);
                        $etag = md5_file($minFile);
                    }

                    $fileNames .= " * " . $name . ".min.css is exist. \n";
                    $hold .= file_get_contents($minFile);
                } elseif (file_exists($file)) {
                    if (filemtime($file) > $lastModified) {
                        $lastModified = filemtime($file);
                        $etag = md5_file($file);
                    }

                    $fileNames .= " * " . $name . ".css is exist. \n";
                    $hold .= file_get_contents($file);
                } else {
                    $fileNames .= " * " . $name . ".min.css | .css is not exist. \n";
                }
            }

            $css = "/* MediapressPack | © " . date('Y') . " \n * \n";
            $css .= $fileNames . " * \n */ \n\n" . $hold;

            return ['etag' => $etag, 'lastModified' => $lastModified, 'css' => $css];
        });
        if(!file_exists(public_path('mediapressAssets/css/'))){
            mkdir(public_path('mediapressAssets/css/'),0755,true);
        }
        file_put_contents('mediapressAssets/css/'.$names.'.css',$cache['css']);
        $lastModified = date("D, d M Y H:i:s GMT", $cache['lastModified']);

        return response($cache['css'], 200)->header('Content-Type', 'text/css')
            ->header("pragma", "public")
            ->header("Cache-Control", "public, max-age=31536000")
            ->header("Last-Modified", $lastModified)
            ->header("Etag", $cache['etag']);
    }


    protected function mediapressJS($names)
    {

        $cache = \Cache::rememberForever(md5($names . 'js'), function () use ($names) {

            $nameArr = explode('|', $names);

            $hold = "";
            $fileNames = "";
            $lastModified = 0;
            $etag = "";

            foreach ($nameArr as $name) {
                $minFile = public_path('vendor/mediapress/js/' . $name . '.min.js');
                $file = public_path('vendor/mediapress/js/' . $name . '.js');

                if (file_exists($minFile)) {
                    if (filemtime($minFile) > $lastModified) {
                        $lastModified = filemtime($minFile);
                        $etag = md5_file($minFile);
                    }

                    $fileNames .= " * " . $name . ".min.js is exist. \n";
                    $file_detail = file_get_contents($minFile);
                    if (!in_array($file_detail[0] ?? "!", ['!', '+'])) {
                        if ($file_detail[0] . $file_detail[1] . $file_detail[2] . $file_detail[3] == 'func') {
                            $hold .= "!";
                        }
                    }
                    $hold .= $file_detail;
                } elseif (file_exists($file)) {
                    if (filemtime($file) > $lastModified) {
                        $lastModified = filemtime($file);
                        $etag = md5_file($file);
                    }

                    $fileNames .= " * " . $name . ".js is exist. \n";
                    $fileContent = file_get_contents($file);
                    if (!in_array($fileContent[0] ?? "!", ['!', '+'])) {
                        if ($fileContent[0] . $fileContent[1] . $fileContent[2] . $fileContent[3] == 'func') {
                            $hold .= "!";
                        }
                    }
                    $hold .= $fileContent;
                } else {
                    $fileNames .= " * " . $name . ".min.js | .js is not exist. \n";
                }
            }

            $js = "/* MediapressPack | © " . date('Y') . " \n * \n";
            $js .= $fileNames . " * \n */ \n\n" . $hold;

            return ['etag' => $etag, 'lastModified' => $lastModified, 'js' => $js];
        });

        $lastModified = date("D, d M Y H:i:s GMT", $cache['lastModified']);
        if(!file_exists(public_path('mediapressAssets/js/'))){
            mkdir(public_path('mediapressAssets/js/'),0755,true);
        }
        file_put_contents('mediapressAssets/js/'.$names.'.css',$cache['js']);
        $lastModified = date("D, d M Y H:i:s GMT", $cache['lastModified']);
        return response($cache['js'], 200)->header('Content-Type', 'application/javascript')
            ->header("pragma", "public")
            ->header("Cache-Control", "public, max-age=31536000")
            ->header("Last-Modified", $lastModified)
            ->header("Etag", $cache['etag']);
    }


    private function getCss($names,$create=false) {

        preg_match_all('/(.*?)[|_.]/i', $names.'.', $types_arr, PREG_SET_ORDER, 0);

        $nameArr =[];
        foreach ($types_arr as $type){
            if(isset($type[0])){
                $nameArr[] = rtrim($type[0],'_.|');
            }
        }
        $hold = "";
        $fileNames = "";
        foreach ($nameArr as $name) {
            $minFile = resource_path('assets/css/' . $name . '.min.css');
            $file = resource_path('assets/css/' . $name . '.css');

            $lastModified = 0;
            $etag = "";
            if (file_exists($minFile)) {
                if (filemtime($minFile) > $lastModified) {
                    $lastModified = filemtime($minFile);
                    $etag = md5_file($minFile);
                }

                $fileNames .= " * " . $name . ".min.css is exist. \n";
                $hold .= file_get_contents($minFile);
            } elseif (file_exists($file)) {
                if (filemtime($file) > $lastModified) {
                    $lastModified = filemtime($file);
                    $etag = md5_file($file);
                }

                $fileNames .= " * " . $name . ".css is exist. \n";
                $hold .= file_get_contents($file);
            } else {
                $fileNames .= " * " . $name . ".min.css | .css is not exist. \n";
            }
        }
        if(config('app.env') != 'local'){
            $css = $hold;
        }else{
            $css = "/* MediapressPack | © " . date('Y') . " \n * \n";
            $css .= $fileNames . " * \n */ \n\n" . $hold;
        }

        if($create){
            if(!file_exists(public_path('assets/css/'))){
                mkdir(public_path('assets/css/'),0755,true);
                file_put_contents('assets/css/.gitignore',"*\n!.gitignore");
            }
            file_put_contents('assets/css/'.$names.'.css',$css);
            return response($css)->header('Content-Type', 'text/css')
                ->header("pragma", "public")
                ->header("Cache-Control", "public, max-age=31536000")
                ->header("Last-Modified", $lastModified)
                ->header("Etag",$etag);
        }
        return ['etag' => $etag, 'lastModified' => $lastModified, 'css' => $css];
    }

    private function getJs($names,$create=false)
    {
        preg_match_all('/(.*?)[|_.]/i', $names.'.', $types_arr, PREG_SET_ORDER, 0);

        $nameArr =[];
        foreach ($types_arr as $type){
            if(isset($type[0])){
                $nameArr[] = rtrim($type[0],'_.|');
            }
        }
        $hold = "";
        $fileNames = "";
        $lastModified = 0;
        $etag = "";
        foreach ($nameArr as $name) {
            $minFile = resource_path('assets/js/' . $name . '.min.js');
            $file = resource_path('assets/js/' . $name . '.js');


            if (file_exists($minFile)) {
                if (filemtime($minFile) > $lastModified) {
                    $lastModified = filemtime($minFile);
                    $etag = md5_file($minFile);
                }

                $fileNames .= " * " . $name . ".min.js is exist. \n";
                $fileContent = file_get_contents($minFile);
                if (!in_array($fileContent[0] ?? "!", ['!', '+'])) {
                    if ($fileContent[0] . $fileContent[1] . $fileContent[2] . $fileContent[3] == 'func') {
                        $hold .= "!";
                    }
                }
                $hold .= $fileContent;
            } elseif (file_exists($file)) {
                if (filemtime($file) > $lastModified) {
                    $lastModified = filemtime($file);
                    $etag = md5_file($file);
                }

                $fileNames .= " * " . $name . ".js is exist. \n";
                $fileContent = file_get_contents($file);
                if (!in_array($fileContent[0] ?? "!", ['!', '+'])) {
                    if ($fileContent[0] . $fileContent[1] . $fileContent[2] . $fileContent[3] == 'func') {
                        $hold .= "!";
                    }
                }
                $hold .= $fileContent;
            } else {
                $fileNames .= " * " . $name . ".min.js | .js is not exist. \n";
            }
        }
        if(config('app.env') != 'local'){
            $js = $hold;
        }else{
            $js = "/* MediapressPack | © " . date('Y') . " \n * \n";
            $js .= $fileNames . " * \n */ \n\n" . $hold;
        }
        if($create){
            if(!file_exists(public_path('assets/js/'))){
                mkdir(public_path('assets/js/'),0755,true);
                file_put_contents('assets/js/.gitignore',"*\n!.gitignore");
            }

            file_put_contents('assets/js/'.$names.'.js',$js);
            return response($js)->header('Content-Type', 'application/javascript')
                ->header("pragma", "public")
                ->header("Cache-Control", "public, max-age=31536000")
                ->header("Last-Modified", $lastModified)
                ->header("Etag",$etag);
        }

        return ['etag' => $etag, 'lastModified' => $lastModified, 'js' => $js];
    }
}
