<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Traits\HasMFiles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Traits\IsDetail;

class SitemapDetail extends BaseModel
{
    use HasMFiles;
    use SoftDeletes;
    use IsDetail;

    public const META_TEMPLATES = 'meta_templates';
    public const CATEGORY_SLUG = 'category_slug';
    public const MODEL = 'model';
    protected $table = 'sitemap_details';

    protected $dates = ['deleted_at'];
    public $timestamps = false;

    protected $casts = [
        self::META_TEMPLATES => 'array',
    ];

    protected $fillable = [
        'sitemap_id',
        'website_id',
        'language_id',
        'country_group_id',
        'manual_meta',
        'name',
        'slug',
        'active',
        self::CATEGORY_SLUG,
        self::META_TEMPLATES,
        'status'
    ];

    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }

    public function parent()
    {
        return $this->belongsTo(Sitemap::class, 'sitemap_id', 'id');
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class, "country_group_id", "id");
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'model')->withTrashed()->withDefault(['url' => '/'])->whereIn('type', ['original', 'reserved'])->orderBy('type', 'asc');
    }

    public function categoryUrl()
    {
        return $this->morphOne(Url::class, self::MODEL)->where('type', 'category');
    }

    public function urls()
    {
        return $this->morphMany(Url::class, self::MODEL);
    }

    public function extras()
    {
        return $this->hasMany(SitemapDetailExtra::class);
    }

    public function extra($key)
    {
        $extra = $this->extras()->where('key', $key)->first();
        if (isset($this->extras) && isset($extra->value)) {
            return $extra->value;
        }

        return null;
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function setSlugAttribute($value)
    {
        if (mb_substr($value, 0, 1) != '/') {
            $this->attributes['slug'] = '/' . $value;
        } else {
            $this->attributes['slug'] = $value;
        }
    }

    public function setCategorySlugAttribute($value)
    {

        if (mb_substr($value, 0, 1) != '/') {
            $this->attributes[self::CATEGORY_SLUG] = '/' . $value;
        } else {
            $this->attributes[self::CATEGORY_SLUG] = $value;
        }

    }

    public function getParentIdAttribute()
    {
        return $this->sitemap_id;
    }

    public function getMetaTemplatesAttribute($value)
    {
        //return [null];
        $cast = $this->castAttribute(self::META_TEMPLATES, $value);
        //$correct = is_array($cast) ? $cast : [];
        return is_array($cast) ? $cast : [];
    }

    public function countryGroups()
    {
        return $this->morphMany(CountryGroup::class, 'owner');
    }

    public function getDetailAttribute($value)
    {
        return supFront($value, 'detail', $this);
    }

    public function getNameAttribute($value)
    {
        return supFront($value, 'name', $this);
    }
}
