@extends('ContentPanel::inc.module_main')
@push("styles")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/selector.css")}}" />
@endpush


@section('content')
    {{--    <div class="container-fluid">--}}
    {{--        <div class="page">--}}
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! __("ContentPanel::sitemap.edit_page") !!}</div>
                {{--<script>alert("this is the running one");</script>--}}
            </div>
            @if($object->sitemapType->sitemap_type_type == 'dynamic')
                <div class="float-right ml-2">
                    <a href="{!! route('Content.pages.index', ['sitemap_id' => $object->id]) !!}"
                       target="_blank"
                       class="btn btn-sm btn-warning">{{ trans('ContentPanel::page.pages') }}</a>
                </div>
            @endif
            <div class="float-right">
                @include('MPCorePanel::inc.backup',['object'=>$object])
            </div>
            @if(config('app.env') == 'local')
                @php
                    $temp = str_replace(["App\\", "\\"], ["", "/"], $renderable->class) . '.php';
                @endphp
                <div class="float-right">
                    <a href="{!! route('admin.editors.edit',encrypt(app_path($temp)) ) !!}" target="_blank" class="btn btn-sm btn-secondary">Renderable</a>
                </div>
            @endif
        </div>
        <div class="p-30">
            @php /** @var \Mediapress\AllBuilder\Foundation\BuilderRenderable $renderable */ @endphp
            @if( isset($renderable) && is_a($renderable, \Mediapress\AllBuilder\Foundation\BuilderRenderable::class))
                @php $renderable->render(); @endphp
                @php
                    $push = $renderable->getStacks();
                @endphp
                @foreach($push as $stack => $p)
                    @push($stack) {!! implode("\n",$p) !!} @endpush
                @endforeach
            @endif
        </div>
    </div>

    {{--        </div>--}}
    {{--    </div>--}}
@endsection

@php
    if($renderable->getParam('sitemap')) {
        $languages = array_map('intval',$renderable->getParam('sitemap')->details->pluck('language_id')->toArray());
        $languages = implode(',', $languages);
        $model_id = $renderable->getParam('sitemap')->id;
        $model_type = urlencode(get_class($renderable->getParam('sitemap')));
    }
@endphp
@push('scripts')

    <script src=" {{asset("/vendor/mediapress/js/jquery.bootstrap.wizard.min.js")}}"></script>
    <script src="{{asset("/vendor/mediapress/js/fm.selectator.jquery.js")}}"></script>
    <script src="{{asset("/vendor/mediapress/js/icheck.min.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>
    <script>
        CKEDITOR.disableAutoInline = true;
        CKEDITOR.replaceClass = 'ckeditor';
    </script>
    <script src="{!! asset('vendor/mediapress/ContentModule/js/DetailsEngine.js') !!}"></script>

    <script>
        $(document).ready(function() {
            <!-- Select 2 -->
            $('.select2').select2({
                tags: true,
                maximumSelectionLength: 10,
                tokenSeparators: [',', ' '],
                placeholder: "En çok kullanılan etiketlerden seç ya da yeni bir etiket gir",
                width: '100%'
            });
            $('li.next > a[id^="anchor-"]').html('{{ trans('MPCorePanel::general.next') }} <i class="fa-angle-double-right fa"> </i>');
            $('li.previous > a[id^="anchor-"]').html('<i class="fa-angle-left fa"></i> {{ trans('MPCorePanel::general.previous') }}');

            $('.rootwizard').bootstrapWizard({
                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('.rootwizard').find('.progress-bar').css({width: $percent + '%'});
                }
            });

            $('textarea.ckeditor').each(function (key, item) {
                CKEDITOR.replace($(this).attr('id'),
                    {
                        language: 'tr',
                        //extraPlugins: 'bol,kopyala',allow_diskkeys%5B0%5D=azure&allow_diskkeys%5B1%5D=local&
                        filebrowserImageBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Görsel',
                        filebrowserBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Dosya',
                        allowedContent: {
                            script: true,
                            div: true,
                            $1: {
                                // This will set the default set of elements
                                elements: CKEDITOR.dtd,
                                attributes: true,
                                styles: true,
                                classes: true
                            }
                        },
                        entities: false,
                        entities_latin: false,
                        removeDialogTabs: null,//'image:advanced;link:advanced',
                        allowedContent: false,
                        pasteFromWordPromptCleanup: true,
                        pasteFromWordRemoveFontStyles: true,
                        forcePasteAsPlainText: true,
                        ignoreEmptyParagraph: true,
                        toolbar: [
                            {
                                name: 'clipboard',
                                groups: ['clipboard', 'undo'],
                                items: ['PasteFromWord', '-', 'Undo', 'Redo']
                            },
                            {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace']},
                            //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                            {
                                name: 'paragraph',
                                groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                            },
                            {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                            {name: 'links', items: ['Link', 'Unlink']},
                            '/',
                            {
                                name: 'basicstyles',
                                groups: ['basicstyles', 'cleanup'],
                                items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                            },
                            {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                            {name: 'colors', items: ['TextColor', 'BGColor']},
                            {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                            {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                        ]
                    });


                CKEDITOR.config.extraPlugins = 'justify,iframe,indentblock,indent,textindent';
            });
            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });


            var details_engine = new DetailsEngine({});
            details_engine.init();

            document.addEventListener('detailNameChange', function (elem) {
                console.log(elem);
            }, false);
            /*$.ajax({

            });*/
        });
        function pageCategoriesSelectionChanged(page_id,category_ids_arr){
            leaveCategoriesCriteriasOnly(category_ids_arr);
        }

        function pageCountryandProvinceSelectionChanged(country_id) {
            getProvinces(country_id);
        }

        function getProvinces(country_id){
            var token = $("meta[name='csrf-token']").attr("content");
            var value = parseInt($('#select-province').children("option:selected").val());

            if($('#select-province').length){
                $.ajax({
                    url: "{{ url(route("MPCore.country_province.get_values")) }}",
                    method: 'GET',
                    data: {country_id: country_id, _token: token},
                    dataType: "json",
                    success: function (data) {
                        $('#select-province').html("");
                        $.each(data, function (ind, el) {
                            if (value === el.id) {
                                $('#select-province').append('<option  value="' + el.id + '" selected>' + el.name + '</option>');
                            } else {
                                $('#select-province').append('<option  value="' + el.id + '">' + el.name + '</option>');
                            }
                        });
                        $('#select-province').selectpicker('refresh');

                    },
                    error: function (xhr, status, errorThrown) {
                        //alert(false);
                    }
                });
            }
        }

        function tabForward(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#'+id+'"]').parent();
            active.next().find('a').trigger('click');
        }

        function tabBack(el) {
            let id = el.parents('.tab-pane').attr('id');
            let nav = el.parents('.rootwizard');

            let active = nav.find('.navbar .nav-pills a[href="#'+id+'"]').parent();
            active.prev().find('a').trigger('click');
        }
    </script>


    <!-- Save Button Start -->
    <script>
        $(document).ready(function() {
            var button = $("button.btn-step-tabs-submit[type='submit']");

            button.html("{{ __('ContentPanel::general.save_as.active') }}");
            button.addClass('btn-success');
        });
    </script>
    <!-- Save Button Finish -->

    <!-- PreviewBox Start -->
    <script>
        $(document).ready(function() {
            @foreach($renderable->getParam('sitemap')->details as $detail)
            @php
                if($renderable->getParam('sitemap')->category == 1) {
                    $url = $detail->categoryUrl;
                    if( !$url ){
                        $url = $detail->url;
                    }
                } else {
                    $url = $detail->url;
                }
            @endphp
            $('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] .previewBox').html('' +
                '<div class="boxes prev{{ $detail->language_id . '_' . $detail->country_group_id }}">' +
                '    <h2>\n' +
                '        <a href="javascript:void(0);">' +
                '            <i class="previewName"> {{ $url->metas->title->desktop_value }} </i>\n' +
                '            <span class="previewUrl"> {{ url($url->url) }} </span>\n' +
                '        </a>\n' +
                '    </h2>\n' +
                '    <p class="previewContent"> {{ $url->metas->description->desktop_value }} </p>\n' +
                '</div>' +
                '');


            $(document).delegate('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="title"] div.divInput', 'keyup' , function() {

                var cloneEl = $(this).clone();
                cloneEl.find('em').remove();
                var spans = cloneEl.find('span');
                if(spans.length > 0) {
                    $.each(spans, function (k, v) {
                        $(v).replaceWith($(v).attr('data-meta-value'));
                    })
                }

                $('.boxes.prev{{ $detail->language_id . '_' . $detail->country_group_id }}').find('.previewName').html(cloneEl.html());
            });

            $(document).delegate('div[data-language-id="{{$detail->language_id}}"][data-country-group-id="{{$detail->country_group_id}}"] div.select-item > div[data-meta-device="desktop"][data-meta-name="description"] div.divInput', 'keyup' , function() {

                var cloneEl = $(this).clone();
                cloneEl.find('em').remove();
                var spans = cloneEl.find('span');
                if(spans.length > 0) {
                    $.each(spans, function (k, v) {
                        $(v).replaceWith($(v).attr('data-meta-value'));
                    })
                }

                $('.boxes.prev{{ $detail->language_id . '_' . $detail->country_group_id }}').find('.previewContent').html(cloneEl.html());
            });
            @endforeach
        })
    </script>
    <!-- PreviewBox Finish -->
@endpush

@if(function_exists('filemanager'))
    @php
        if($renderable->getParam('sitemap')) {
            $languages = array_map('intval',$renderable->getParam('sitemap')->details->pluck('language_id')->toArray());
            $model_id = $renderable->getParam('sitemap')->id;
            $model_type = urlencode(get_class($renderable->getParam('sitemap')));
        }
    @endphp

    @push('scripts')
        {!! filemanager($model_id, $model_type, $languages) !!}
    @endpush
@endif
