<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 21.03.2019
     * Time: 17:45
     */

    namespace Mediapress\Modules\Content\Observers;
    use Mediapress\Modules\Content\Models\Sitemap;
    use Illuminate\Support\Facades\Cache;
    use Mediapress\Modules\Content\Models\SitemapWebsite;

    class SitemapObserver
    {
        public function updated(Sitemap $sm)
        {
            Cache::flush();
        }
        public function deleting(Sitemap $object)
        {
            SitemapWebsite::where('sitemap_id', $object->id)->delete();

            if($object->categories->isNotEmpty()) {
                $object->categories->delete();
            }

            Cache::flush();
        }
        public function deleted(Sitemap $sm)
        {
            Cache::flush();
        }
    }
