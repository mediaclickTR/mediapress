<?php

namespace Mediapress\Modules\Interaction\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Analytics;
use Illuminate\Support\Collection;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Spatie\Analytics\Period;
use Mediapress\Modules\MPCore\Traits\GoogleAnalyticsTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public const TOTALS_FOR_ALL_RESULTS = 'totalsForAllResults';
    public const GA_PAGEVIEWS = 'ga:pageviews';
    public const GA_USERS = 'ga:users';
    protected $days;
    public $account_id;
    public $access_token;

    use GoogleAnalyticsTrait;

    public function __construct()
    {
        $this->analyticsInit();

        if(request('days') || request('days')=='0'){
            $this->days = (int)str_replace(array(' ', ','), '', request('days'));
        }
        else{
            $this->days = 7;
        }
    }

    public function login()
    {
        return redirect()->to("http://googleauth.mediapress.com.tr/login/google?host=".getHttpSchemaWithHost());
    }

    public function oAuth()
    {
        $expire_date_check = SettingSun::where("key","google.analytics.expire.date")
                                            ->where("key","google.analytics.account.id")
                                            ->where("key","google.analytics.refresh.token")
                                            ->where("key","google.analytics.access.token")
                                            ->where("website_id",session("panel.website")->id)->first();

        if (!$expire_date_check || time() > $expire_date_check->value)
        {
            if ($this->refreshOrRegisterAuth(request("refresh_token"))){
                return response("<script>var data = {'status':true};window.close();</script>");
            }
            else{
                return response("<script>var data = {'status':false, 'message' : '".trans("MPCorePanel::analytics.analytical.data.notfound.with.website",['website'=>getHttpSchemaWithHost()])."'};window.close();</script>");
            }
        }
        else{
            return response("<script>window.close();</script>");
        }
    }

    public function index()
    {
        $conn = 1;
        $max=0;

        $data = [
            "visitorAndPageViews" => [],
            "totalViews" => null,
            "users" => null,
            "uniquePageviews" => null,
            "organicSearch" => null,
        ];

        try {
            $data = [
                'visitorAndPageViews' => $this->adapterFetchVisitorsAndPageViews($this->days),
                'totalViews' => $this->getTotalViews($this->days)[self::TOTALS_FOR_ALL_RESULTS][self::GA_PAGEVIEWS],
                'users' => $this->getUsers($this->days)[self::TOTALS_FOR_ALL_RESULTS][self::GA_USERS],
                'uniquePageviews' => $this->getUniquePageViews($this->days)[self::TOTALS_FOR_ALL_RESULTS]['ga:uniquePageviews'],
                'organicSearch' => $this->getOrganicSearch($this->days)[self::TOTALS_FOR_ALL_RESULTS]['ga:organicSearches'],
            ];

            $max = $data['visitorAndPageViews']["max"];

        } catch (\Exception $e) {
            $conn = 0;
        }

        return view("MPCorePanel::analytics.google.index", compact('data','max','conn'));
    }

    public function detailedAnalytics()
    {

        $conn = 1;
        try {
        $pageAnalytics = $this->adapterGetPageAnalytics($this->days);
        $trafficSources = $this->getTrafficSources($this->days);
        $countries = $this->getSessionCountries($this->days);
        $browsersAndSystems = $this->getBrowserAndSystems($this->days);
        $searchEngineKeywords = $this->getSearchEngineKeywords($this->days);
        }
        catch (\Exception $e) {
            $conn = 0;
        }

        return view("MPCorePanel::analytics.google.pageAnalytics",
            compact('pageAnalytics',
                            'trafficSources',
                            'countries',
                            'browsersAndSystems',
                            'searchEngineKeywords',
                            'conn'
            ));
    }

    public function googleAnalyticsDisconnect()
    {
        SettingSun::where("key", 'like', "google.analytics.%")
            ->where('group', 'Analytics')
            ->where("website_id",session("panel.website.id"))->delete();

    }

}
