<?php

namespace Mediapress\Modules\Entity\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\GeneralPivot;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class EntityList extends Model
{

    protected $table = 'entitylist';
    public $timestamps = true;

    protected $fillable = [
        "list_type",
        "name",
        'description',
        'status'
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function records()
    {
        return $this->hasMany(EntitylistPivot::class, "entitylist_id");
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function websites()
    {
        return $this->belongsToMany(Website::class);
    }
}
