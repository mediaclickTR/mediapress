<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGroupLanguageTable extends Migration
{
    public const COUNTRY_GROUP_LANGUAGE = 'country_group_language';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::COUNTRY_GROUP_LANGUAGE)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });

                $schema->create(self::COUNTRY_GROUP_LANGUAGE, function (Blueprint $table) {
                    $table->unsignedInteger('language_id');
                    $table->unsignedInteger('country_group_id');
                    $table->unsignedInteger('default')->default(false);
                });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::COUNTRY_GROUP_LANGUAGE);
    }
}
