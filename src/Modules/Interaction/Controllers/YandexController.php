<?php
namespace Mediapress\Modules\Interaction\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Spatie\Analytics\Period;
use Cache;
use Request;
use Mediapress\Modules\MPCore\Traits\YandexTrait;
use Mediapress\Modules\MPCore\Facades\MPCore;

class YandexController extends Controller
{
    protected $days;

    use YandexTrait;

    protected $url = 'https://api-metrika.yandex.com.tr/';
    protected $counter_id;
    protected $token;
    protected $cache;
    protected $data;
    protected $controlConn=true;

    // constructor
    public function __construct()
    {

        $this->cache = MPCore::settingSunFindValue('mpcore.analytics.yandex.cache');
        $this->counter_id = MPCore::settingSunFindValue('mpcore.analytics.yandex.counter_id');
        $this->token = MPCore::settingSunFindValue('mpcore.analytics.yandex.token');

        if(!$this->cache || !$this->counter_id || !$this->token){
            $this->controlFolder = false;
        }

        if(request('days') || request('days')=='0'){
            $this->days = (int)request('days');
        }
        else{
            $this->days = 7;
        }

        $data = $this->getGeneralAnalytics($this->days);

        if(!$data) {
            $this->controlConn = false;
        }
    }

    public function index()
    {
        $data = $this->getGeneralAnalytics($this->days);
        // Organic Data include : organic visits, users
        $organicData = $this->getOrganicData($this->days);
        //Duration
        $durationData = $this->getDurationData($this->days);
        $totalDuration = floor($durationData['totals']['avgVisitDurationSeconds']);
        $conn = $this->controlConn;

        return view("MPCorePanel::analytics.yandex.index", compact('data','organicData','totalDuration','conn'));
    }

    public function detailedAnalytics()
    {
        $status = $this->controlConn;

        $pageAnalytics = $this->getPageAnalytics($this->days);
        $trafficSources = $this->getDurationData($this->days)['data'];
        $countries = $this->getCountries($this->days);
        $browsersAndSystems = $this->getBrowserAndSystems($this->days);
        return view("MPCorePanel::analytics.yandex.pageAnalytics",
            compact('pageAnalytics',
                'trafficSources',
                'countries',
                'browsersAndSystems',
                'status'
            ));
    }

    public function getControlFolder()
    {
        return $this->controlFolder;
    }

    public function getControlConn()
    {
        return $this->controlConn;
    }
}
