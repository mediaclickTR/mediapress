@extends('ContentPanel::inc.module_main')

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
@endpush

@section('content')
    <div class="container-fluid">
        @include('MPCorePanel::inc.breadcrumb')
        <div class="page-content">
                <div class="title">{!! trans("ContentPanel::website.edit_website") !!}</div>
                 @include("MPCorePanel::inc.errors")
            <div class="rootwizard">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <ul>
                                    <li><a href="#website" data-toggle="tab">1. {!! trans("ContentPanel::website.tab_websites") !!}</a></li>
                                    <li><a href="#language" id="tab-language" data-toggle="tab">2. {!! trans("ContentPanel::website.tab_languages") !!}</a></li>
                                    <li><a href="#details" id="tab-details" data-toggle="tab">3. {!! trans("ContentPanel::website.tab_details") !!}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="bar" class="progress progress-striped active">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="tab-content">
                        @include("ContentPanel::websites.form")
                    </div>
                </div>
            </div>
        </div>
@endsection

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function() {
            $('.rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    $('.rootwizard').find('.progress-bar').css({width:$percent+'%'});
                }});
            $('.baseLanguages').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
        });
    </script>
@endpush
