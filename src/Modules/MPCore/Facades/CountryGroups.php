<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 14.11.2018
     * Time: 11:07
     */

    namespace Mediapress\Modules\MPCore\Facades;

    use Illuminate\Support\Facades\Facade;

    class CountryGroups extends Facade
    {

        protected static function getFacadeAccessor()
        {
            return \Mediapress\Modules\MPCore\Foundation\CountryGroupsEngine::class;
        }

    }