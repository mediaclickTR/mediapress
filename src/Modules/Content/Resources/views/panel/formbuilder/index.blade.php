{{--
<div class="content">
    {!! Form::open(['url'=>route('admin.formbuilder.store'),'class' => 'form-horizontal', 'files' => true,'method'=>'POST']) !!}
    @php /*TabWriter::create($form)*/ @endphp
    <div class="form-group">
        <div class="col-md-offset-2 col-md-4">
            <button type="submit" class="btn btn-success kaydet">Oluştur</button>
        </div>
    </div>
    {!! Form::close() !!}

    <div>
        @php dd($form) @endphp
    </div>
</div>--}}
<div class="col-md-12 pa">
    <div class="step">
        <div class="rootwizard">
            <div class="navbar">
                <div class="navbar-inner">
                    <ul>
                        <li><a href="#tab1" data-toggle="tab"><i>1.</i>Genel</a></li>
                        <li><a href="#tab2" data-toggle="tab"><i>2.</i>Fotoğraflar</a></li>
                        <li><a href="#tab3" data-toggle="tab"><i>3.</i>Benzer Ürünler</a></li>
                        <li><a href="#tab4" data-toggle="tab"><i>4.</i>Kriter & Özellik</a></li>
                        <li><a href="#tab5" data-toggle="tab"><i>5.</i>SEO</a></li>
                        <li><a href="#tab6" data-toggle="tab"><i>6.</i>Yayınla</a></li>
                    </ul>
                </div>
            </div>
            <div id="bar" class="progress progress-striped active">
                <div class="progress-bar"></div>
            </div>
            <div class="form">
                <form action="#">
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <div class="contents">
                                <div class="category">
                                    <div class="title">
                                        İlgili Kategoriler
                                    </div>
                                    <select class="multiple" name="select2" multiple>
                                        <option value="1" selected>Test Ürünü</option>
                                        <option value="2">Test Ürünü 2</option>
                                        <option value="3">Test Ürünü 3</option>
                                        <option value="4">Test Ürünü 4</option>
                                        <option value="5">Test Ürünü 5</option>
                                        <option value="6">Test Ürünü 6</option>
                                    </select>
                                </div>
                                <div class="tab-list">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#tr">TR</a></li>
                                        <li><a data-toggle="tab" href="#en">EN</a></li>
                                        <li><a data-toggle="tab" href="#de">DE</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tr" class="tab-pane fade in active">
                                            <div class="form-group">
                                                <label>Başlık</label>
                                                <input type="text">
                                            </div>
                                            <div class="form-group">
                                                <textarea name="editor1" id="editor1" rows="10" cols="80"> This is my textarea to be replaced with CKEditor. </textarea>
                                            </div>
                                        </div>
                                        <div id="en" class="tab-pane fade in ">
                                            <div class="form-group">
                                                <label>Başlık</label>
                                                <input type="text">
                                            </div>
                                        </div>
                                        <div id="de" class="tab-pane fade in">
                                            <div class="form-group upload">
                                                <label>Yeni Görsel Yükle</label>
                                                <div class="mc-md-3 pl">
                                                    <div class="all">
                                                        <div class="images">
                                                            <a href="javascript:void(0);">
                                                                <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mc-md-9 pr">
                                                    <div class="right-bx">
                                                        <div class="upload-btn-wrapper">
                                                            <div class="form-group">
                                                                <button class="btn">Bilgisayarımdan Yükle</button>
                                                                <input type="file" name="myfile"/>
                                                            </div>
                                                            <div class="form-group rel">
                                                                <button class="btn">Galeriden Seç</button>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label for="male">Tüm dillerde aynı görseli kulan
                                                                <input type="checkbox" name="checkbox" id="male"
                                                                       checked>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-list">
                                                <label>Yüklenen Görseller</label>
                                                <ul class="sortable">
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="contents">
                                <div class="boxes-right on-off">
                                    <div class="url">
                                        <ul>
                                            <li>
                                                <span>Tüm dillerde aynı görseli kulan</span>
                                            </li>
                                            <li>
                                                <label for="mycheckbox" class="switch-toggle" data-on="On"
                                                       data-off="Off">Varsayılan Url</label>
                                                <input type="checkbox" id="mycheckbox"/>
                                            </li>
                                            <li>
                                                Her dil için ayrı görsel kullan
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-list">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#trf">TR</a></li>
                                        <li><a data-toggle="tab" href="#enf">EN</a></li>
                                        <li><a data-toggle="tab" href="#def">DE</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="trf" class="tab-pane fade in active">
                                            <div class="form-group upload">
                                                <div id="myModal2" class="modal fade imgoption" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Menü Düzenle</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="post">
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Dil</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <select class="multiple" name="select2"
                                                                                    multiple>
                                                                                <option value="1">English</option>
                                                                                <option value="2">Turkish</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Resim Adı (TR)</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Resim Adı">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Resim Adı (EN)</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Resim Adı">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Yükseklik</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Yükseklik">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Genişlik</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Genişlik">
                                                                        </div>
                                                                    </div>
                                                                    <div class="submit">
                                                                        <button> Güncelle</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label>Yeni Görsel Yükle</label>
                                                <div class="mc-md-3 pl">
                                                    <div class="all">
                                                        <div class="images">
                                                            <a href="javascript:void(0);">
                                                                <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal2">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mc-md-9 pr">
                                                    <div class="right-bx">
                                                        <div class="upload-btn-wrapper">
                                                            <div class="form-group">
                                                                <button class="btn">Bilgisayarımdan Yükle</button>
                                                                <input type="file" name="myfile"/>
                                                            </div>
                                                            <div class="form-group rel">
                                                                <button class="btn">Galeriden Seç</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-list">
                                                <label>Yüklenen Görseller</label>
                                                <div id="myModal" class="modal fade imgoption" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Menü Düzenle</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="post">
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Dil</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Dil">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Resim Adı</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Resim Adı">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Yükseklik</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Yükseklik">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="mc-md-2 pa">
                                                                            <label>Genişlik</label>
                                                                        </div>
                                                                        <div class="mc-md-10 pa">
                                                                            <input type="text" placeholder="Genişlik">
                                                                        </div>
                                                                    </div>
                                                                    <div class="submit">
                                                                        <button> Güncelle</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="sortable">
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#myModal">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="enf" class="tab-pane fade in ">
                                            <div class="form-group upload">
                                                <label>Yeni Görsel Yükle</label>
                                                <div class="mc-md-3 pl">
                                                    <div class="all">
                                                        <div class="images">
                                                            <a href="javascript:void(0);">
                                                                <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mc-md-9 pr">
                                                    <div class="right-bx">
                                                        <div class="upload-btn-wrapper">
                                                            <div class="form-group">
                                                                <button class="btn">Bilgisayarımdan Yükle</button>
                                                                <input type="file" name="myfile"/>
                                                            </div>
                                                            <div class="form-group rel">
                                                                <button class="btn">Galeriden Seç</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-list">
                                                <label>Yüklenen Görseller</label>
                                                <ul class="sortable">
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="def" class="tab-pane fade in">
                                            <div class="form-group upload">
                                                <label>Yeni Görsel Yükle</label>
                                                <div class="mc-md-3 pl">
                                                    <div class="all">
                                                        <div class="images">
                                                            <a href="javascript:void(0);">
                                                                <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mc-md-9 pr">
                                                    <div class="right-bx">
                                                        <div class="upload-btn-wrapper">
                                                            <div class="form-group">
                                                                <button class="btn">Bilgisayarımdan Yükle</button>
                                                                <input type="file" name="myfile"/>
                                                            </div>
                                                            <div class="form-group rel">
                                                                <button class="btn">Galeriden Seç</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-list">
                                                <label>Yüklenen Görseller</label>
                                                <ul class="sortable">
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="ui-state-default">
                                                        <div class="images">
                                                            <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                        </div>
                                                        <div class="extras">
                                                            <div class="extras-inner">
                                                                <a href="javascript:void(0);"
                                                                   onclick="javascript:resim_sil()" class="">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="">
                                                                    <i class="fa fa-cog"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="no-tab">
                                    <div class="form-group upload">
                                        <div id="myModal0" class="modal fade imgoption" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                        <h4 class="modal-title">Menü Düzenle</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="post">
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Resim Adı</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Resim Adı">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Yükseklik</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Yükseklik">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Genişlik</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Genişlik">
                                                                </div>
                                                            </div>
                                                            <div class="submit">
                                                                <button> Güncelle</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Yeni Görsel Yükle</label>
                                        <div class="mc-md-3 pl">
                                            <div class="all">
                                                <div class="images">
                                                    <a href="javascript:void(0);">
                                                        <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                    </a>
                                                </div>
                                                <div class="extras">
                                                    <div class="extras-inner">
                                                        <a href="javascript:void(0);" onclick="javascript:resim_sil()"
                                                           class="">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#myModal0">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mc-md-9 pr">
                                            <div class="right-bx">
                                                <div class="upload-btn-wrapper">
                                                    <div class="form-group">
                                                        <button class="btn">Bilgisayarımdan Yükle</button>
                                                        <input type="file" name="myfile"/>
                                                    </div>
                                                    <div class="form-group rel">
                                                        <button class="btn">Galeriden Seç</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="upload-list">
                                        <label>Yüklenen Görseller</label>
                                        <div id="myModalx" class="modal fade imgoption" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                        <h4 class="modal-title">Menü Düzenle</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="post">
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Resim Adı</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Resim Adı">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Yükseklik</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Yükseklik">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="mc-md-2 pa">
                                                                    <label>Genişlik</label>
                                                                </div>
                                                                <div class="mc-md-10 pa">
                                                                    <input type="text" placeholder="Genişlik">
                                                                </div>
                                                            </div>
                                                            <div class="submit">
                                                                <button> Güncelle</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="sortable">
                                            <li class="ui-state-default">
                                                <div class="images">
                                                    <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                </div>
                                                <div class="extras">
                                                    <div class="extras-inner">
                                                        <a href="javascript:void(0);" onclick="javascript:resim_sil()"
                                                           class="">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#myModalx">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="ui-state-default">
                                                <div class="images">
                                                    <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                </div>
                                                <div class="extras">
                                                    <div class="extras-inner">
                                                        <a href="javascript:void(0);" onclick="javascript:resim_sil()"
                                                           class="">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="ui-state-default">
                                                <div class="images">
                                                    <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                </div>
                                                <div class="extras">
                                                    <div class="extras-inner">
                                                        <a href="javascript:void(0);" onclick="javascript:resim_sil()"
                                                           class="">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="ui-state-default">
                                                <div class="images">
                                                    <img src="images/gorsel1.jpg" class="resimatt" alt="">
                                                </div>
                                                <div class="extras">
                                                    <div class="extras-inner">
                                                        <a href="javascript:void(0);" onclick="javascript:resim_sil()"
                                                           class="">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="contents">
                                <div class="boxes-right on-off">
                                    <div class="url">
                                        <ul>
                                            <li>
                                                <span>Aynı Kategorideki Ürünleri Getir</span>
                                            </li>
                                            <li>
                                                <label for="mycheckbox" class="switch-toggle" data-on="On"
                                                       data-off="Off">Varsayılan Url</label>
                                                <input type="checkbox" id="mycheckbox"/>
                                            </li>
                                            <li>
                                                Ürün Seç
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="benzer">
                                    <div class="item add">
                                        <div class="select">
                                            <select class="nice">
                                                <option>Seçiniz</option>
                                                <option>XXXXXXX XXXXXX Benzer Ürünü</option>
                                                <option>XXXXXXX XXXXXX Benzer Ürünü</option>
                                                <option>XXXXXXX XXXXXX Benzer Ürünü</option>
                                            </select>
                                        </div>
                                        <div class="add-button"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            <div class="contents">
                                <div class="tab-list">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#kriter">Kriterler</a></li>
                                        <li><a data-toggle="tab" href="#ozellikler">Özellikler</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="kriter" class="tab-pane fade in active">
                                            <div class="kriterbox">
                                                <div class="mc-md-3 pl">
                                                    <div class="tab">
                                                        <a href="javascript:void(0);" class="tablinks active"
                                                           onclick="openCity(event, 'Renk')">Renk</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Kagit')">Kağıt Boyutu (2)</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Baski')">Baskı Hızı</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Buyukluk')">Büyüklük</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Renk2')">Renk</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'KBoyut')">Kağıt Boyutu (4)</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'BHizi')">Baskı Hızı</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Buyukluk2')">Büyüklük</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Renk3')">Renk</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'KBK')">Kağıt Besleme Kapasitesi</a>
                                                    </div>
                                                </div>

                                                <div class="mc-md-9 pr">
                                                    <div class="txt">
                                                        <div id="Renk" class="tabcontent active">
                                                            <p>Mavi</p>
                                                        </div>
                                                        <div id="Kagit" class="tabcontent">
                                                            <div class="checkbox">
                                                                <label for="a3">
                                                                    a3
                                                                    <input type="checkbox" name="checkbox" id="a3">
                                                                </label>
                                                                <label for="a4">
                                                                    a4
                                                                    <input type="checkbox" name="checkbox" id="a4">
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div id="Baski" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="Buyukluk" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="Renk2" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="KBoyut" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="BHizi" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="Buyukluk2" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="Renk3" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                        <div id="KBK" class="tabcontent">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                                Adipisci aperiam architecto, atque aut autem dicta
                                                                dolorem enim fugit impedit maxime nam, nesciunt
                                                                provident quibusdam rerum sed similique voluptate
                                                                voluptatibus. Laudantium.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="ozellikler" class="tab-pane fade">
                                            <div class="kriterbox">
                                                <div class="mc-md-3 pl">
                                                    <div class="tab">
                                                        <a href="javascript:void(0);" class="tablinks active"
                                                           onclick="openCity(event, 'Ozellik1')">Özellik1</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Ozellik2')">Özellik2</a>
                                                        <a href="javascript:void(0);" class="tablinks"
                                                           onclick="openCity(event, 'Ozellik3')">Özellik3</a>
                                                    </div>
                                                </div>

                                                <div class="mc-md-9 pr">
                                                    <div class="txt">
                                                        <div id="Ozellik1" class="tabcontent active">
                                                            <input type="text" placeholder="Özellik1">
                                                        </div>
                                                        <div id="Ozellik2" class="tabcontent ">
                                                            <input type="text" placeholder="Özellik2">
                                                        </div>
                                                        <div id="Ozellik3" class="tabcontent ">
                                                            <input type="text" placeholder="Özellik3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab5">
                            <div class="contents">
                                <div class="tab-list">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#tr1">TR</a></li>
                                        <li><a data-toggle="tab" href="#en1">EN</a></li>
                                        <li><a data-toggle="tab" href="#de1">DE</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tr" class="tab-pane fade in active">
                                            <div class="boxesall">
                                                <div class="title">
                                                    Arama Motoru Görünümü
                                                </div>
                                                <div class="boxes mc-md-8 pl">
                                                    <h2>
                                                        <a href="#"><i>Ödüllü İstanbul Web Tasarım Ajansı MediaClick
                                                                ®</i>
                                                            <span>www.mediaclick.com.tr</span>
                                                        </a>
                                                    </h2>
                                                    <p>Kurumsal <b>web</b> tasarım ajansı <b>MediaClick</b> İstanbul,
                                                        SEO ve mobil uyumlu profesyonel web site tasarımları ile web
                                                        sitesi tasarımı yarışmalarında ... Kendimizi kısaca 360 derece
                                                        bütünleşik dijital iletişim hizmeti veren bir ajans olarak
                                                        tanımlıyoruz.</p>
                                                </div>
                                                <div class="boxes-right mc-md-4 pr">
                                                    <div class="url">
                                                        <ul>
                                                            <li>
                                                                <span>Otomatik SEO</span>
                                                            </li>
                                                            <li>
                                                                <label for="mycheckbox" class="switch-toggle"
                                                                       data-on="On" data-off="Off">Varsayılan
                                                                    Url</label>
                                                                <input type="checkbox" id="mycheckbox"/>
                                                            </li>
                                                            <li>
                                                                Manuel
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="url-detail">
                                                <div class="accordion">
                                                    <h3>Temel SEO Ayarları</h3>
                                                    <div class="f-list">
                                                        <div class="form-group url focus">
                                                            <label>Url</label>
                                                            <input type="text" placeholder="www.mediaclick.com.tr">
                                                        </div>
                                                        <div class="form-group title focus">
                                                            <label>Title</label>
                                                            <input type="text"
                                                                   placeholder="Ödüllü İstanbul Web Tasarım Ajansı MediaClick ®">
                                                        </div>
                                                        <div class="form-group description focus">
                                                            <label>Description</label>
                                                            <input type="text"
                                                                   placeholder="Kurumsal web tasarım ajansı MediaClick İstanbul, SEO ve mobil uyumlu profesyonel web site tasarımları ile web sitesi tasarımı yarışmalarında ...">
                                                        </div>
                                                        <div class="form-group ">
                                                            <label>Keywords</label>
                                                            <input type="text">
                                                        </div>
                                                    </div>
                                                    <h3>Özel SEO Ayarları</h3>
                                                    <div class="f-list">
                                                        <div class="form-group">
                                                            <label>301 Url - <i>Bu sayfaya yönlenmesini istediğiniz
                                                                    sayfa url'sini yazınız.</i></label>
                                                            <input type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Canonical Url - <i>Bu sayfanın içeriği bir başka
                                                                    sayfanın içeriği ile aynı ise, kaynak sayfa url'sini
                                                                    yazınız.</i></label>
                                                            <input type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab6">
                            <div class="contents">
                                <div class="mc-md-6 pl">
                                    <div class="pass-box left">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    Parola Korumalı Sayfa
                                                    <input type="checkbox">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <select class="nice">
                                                <option>Tek şifreli giriş</option>
                                                <option>Üyelere özel giriş</option>
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <select class="nice">
                                                <option>Üye Grubu Seç</option>
                                                <option>Test</option>
                                                <option>Deneme</option>
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <input type="Şifre Giriniz">
                                        </div>
                                    </div>
                                </div>
                                <div class="mc-md-6 pr">
                                    <div class="pass-box right">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    Hemen yayınla
                                                    <input type="radio" name="radio">
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    Yayınla ancak liste ve menülerde gizle (Sadece url ile girilir)
                                                    <input type="radio" name="radio">
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    Taslak olarak sakla / Pasif yap
                                                    <input type="radio" name="radio">
                                                </label>
                                            </div>
                                            <div class="checkbox" id="dates">
                                                <label>
                                                    İleri tarihte yayınla
                                                    <input type="radio" name="radio">
                                                </label>
                                                <div class="datepicks">
                                                    <input type="text" class="datepick" placeholder="Tarih Seçiniz">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous"><a href="javascript:;"><i class="fa fa-chevron-left"></i>Geri Dön</a>
                            </li>
                            <li class="next"><a href="javascript:;">Kaydet ve İlerle <i
                                        class="fa fa-angle-double-right"></i></a></li>
                            <li class="oke"><a href="javascript:;">Kaydet ve Çık</a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

