<?php

namespace Mediapress\Modules\Entity\Models;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class UserlistPivot extends Model
{

    protected $table = 'userlist_pivot';
    public $timestamps = false;

    protected $fillable = [
        "userable_type",
        "userable_id",
        "userlist_id"
    ];

    public function userable()
    {
        return $this->morphTo();
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

}
