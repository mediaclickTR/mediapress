<?php

namespace Mediapress\Modules\MPCore\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Mediapress\Modules\MPCore\Models\LanguagePart;

class LanguagePartExport implements FromArray
{
    public function array(): array
    {
        $hold = [];
        $website = session('panel.website');

        $headingRow = [null];
        foreach ($website->countryGroups as $countryGroup) {
            foreach ($countryGroup->languages as $language) {
                $headingRow[] = $countryGroup->code . '-' . $language->code;
            }
        }
        $hold[] = $headingRow;

        $languageParts = LanguagePart::with('language', 'countryGroup')
            ->select('id', 'language_id', 'country_group_id', 'website_id', 'key', 'value')
            ->where('website_id', session('panel.website.id'))
            ->get()
            ->groupBy('key');

        foreach ($languageParts as $key => $parts) {
            $temp = [];
            $temp[] = $key;

            foreach ($headingRow as $hRow) {
                if(!isset($temp[$hRow]) && $hRow != null) {
                    $temp[$hRow] = null;
                }
            }

            foreach ($parts as $part) {

                if(is_null($part->countryGroup) || is_null($part->language)) {
                    continue;
                }

                $temp[$part->countryGroup->code . '-' . $part->language->code] = $part->value;
            }

            $hold[] = $temp;
        }

        return $hold;
    }
}
