<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriteriaDetailExtrasTable extends Migration
{

    public const CRITERIA_DETAIL_EXTRAS = 'criteria_detail_extras';

    public function up()
    {
        if (!Schema::hasTable(self::CRITERIA_DETAIL_EXTRAS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::CRITERIA_DETAIL_EXTRAS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('criteria_detail_id')->unsigned()->nullable()->index();
                $table->string('key');
                $table->text('value');
            });
        }
    }

    public function down()
    {
        Schema::drop(self::CRITERIA_DETAIL_EXTRAS);
    }
}
