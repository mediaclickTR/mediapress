@extends('MPCorePanel::inc.app')
@section('content')
    <div class="container-fluid">
        <div class="page">
            <div class="title float-left">{!! trans("MPCorePanel::analytics.detailedAnalytics") !!}</div>
            @if($conn == 1)
            <div class="float-right">
                <select onchange="locationOnChange(this.value);">
                    @php($request = request())
                    <option @if($request->days)  @if($request->days=='0') selected @endif @endif value="{!! url()->current() !!}?days='0'">{!! trans("MPCorePanel::analytics.today") !!}</option>
                    <option @if($request->days) @if($request->days==1) selected @endif @endif value="{!! url()->current() !!}?days=1">{!! trans("MPCorePanel::analytics.yesterday") !!}</option>
                    <option @if($request->days==7 || !$request->days) selected @endif value="{!! url()->current() !!}?days=7">{!! trans("MPCorePanel::analytics.week") !!}</option>
                    <option @if($request->days) @if($request->days==30) selected @endif @endif value="{!! url()->current() !!}?days=30">{!! trans("MPCorePanel::analytics.month") !!}</option>
                    <option @if($request->days) @if($request->days==90) selected @endif @endif value="{!! url()->current() !!}?days=90">{!! trans("MPCorePanel::analytics.three-month") !!}</option>
                    <option @if($request->days) @if($request->days==365) selected @endif @endif value="{!! url()->current() !!}?days=365">{!! trans("MPCorePanel::analytics.year") !!}</option>
                </select>
            </div>
            @endif
        </div>
        <div class="page-content">
            @if($conn == 1)
            <div class="accordion row">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#pageAnalytics" aria-expanded="true" aria-controls="pageAnalytics">
                                {!! trans("MPCorePanel::analytics.pageAnalytics") !!}
                            </button>
                        </h5>
                    </div>
                    <div id="pageAnalytics" class="collapse" data-parent=".accordion">
                        <div id="pageAnalyticsGraph"></div>
                        <table>
                            <thead>
                            <tr>
                                <th>{!! trans("MPCorePanel::analytics.pageTitle") !!}</th>
                                <th>{!! trans("MPCorePanel::analytics.singularPageViews") !!}</th>
                            </tr>
                            </thead>
                            @foreach($pageAnalytics['page_titles_unique_views'] as $key=>$value)
                            @if($value<5) @continue @endif
                             <tr>
                                <td>{!! $key !!}</td>
                                <td>{!! $value !!}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#trafficSources" aria-expanded="false" aria-controls="traffic-sources">
                                {!! trans("MPCorePanel::analytics.traffic-sources") !!}
                            </button>
                        </h5>
                    </div>
                    <div id="trafficSources" class="collapse" data-parent=".accordion">
                        <div class="card-body">
                            <div id="trafficSourcesGraph"></div>
                            <table>
                                <tr>
                                    <th>{!! trans("MPCorePanel::analytics.source") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.medium") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.pageviews") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.duration") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.exits") !!}</th>
                                </tr>
                                <tr>
                                </tr>
                                @foreach($trafficSources['rows'] as $source)
                                    @if($source['ga:sessions']<2) @continue @endif
                                    <tr>
                                        <td>{!! $source['ga:source'] !!}</td>
                                        <td>{!! $source['ga:medium'] !!}</td>
                                        <td>{!! $source['ga:sessions'] !!}</td>
                                        <td>{!! $source['ga:pageviews'] !!}</td>
                                        <td>{!! $source['ga:sessionDuration'] !!}</td>
                                        <td>{!! $source['ga:exits'] !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">

                    <div class="card-header">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#countries" aria-expanded="true" aria-controls="collapseOne">
                                {!! trans("MPCorePanel::analytics.countries") !!}
                            </button>
                        </h5>
                    </div>

                    <div id="countries" class="collapse" data-parent=".accordion">
                        <div class="card-body">
                            <div id="countriesGraph"></div>
                            <table>
                                <tr>
                                    <th>{!! trans("MPCorePanel::analytics.country") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                </tr>
                                <tr>
                                </tr>
                                @foreach($countries['rows'] as $country)
                                    <tr>
                                        <td>{!! $country['ga:country'] !!}</td>
                                        <td>{!! $country['ga:sessions'] !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#search-engine-keywords" aria-expanded="true" aria-controls="search-engine-keywords">
                                {!! trans("MPCorePanel::analytics.search-engine-keywords") !!}
                            </button>
                        </h5>
                    </div>
                    <div id="search-engine-keywords" class="collapse" data-parent=".accordion">
                        <div class="card-body">
                            <div id="search-engine-keywords"></div>
                            <table>
                                <tr>
                                    <th>{!! trans("MPCorePanel::analytics.source") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.medium") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.keyword") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.pageviews") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.duration") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.exits") !!}</th>
                                </tr>
                                @foreach($trafficSources['rows'] as $source)
                                    <tr>
                                        <td>{!! $source['ga:source'] !!}</td>
                                        <td>{!! $source['ga:medium'] !!}</td>
                                        <td>{!! $source['ga:keyword'] !!}</td>
                                        <td>{!! $source['ga:sessions'] !!}</td>
                                        <td>{!! $source['ga:pageviews'] !!}</td>
                                        <td>{!! $source['ga:sessionDuration'] !!}</td>
                                        <td>{!! $source['ga:exits'] !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#browsers-and-systems" aria-expanded="true" aria-controls="browsers-and-systems">
                                {!! trans("MPCorePanel::analytics.browsers-and-systems") !!}
                            </button>
                        </h5>
                    </div>
                    <div id="browsers-and-systems" class="collapse" data-parent=".accordion">
                        <div class="card-body">
                            <div id="browsersGraph"></div>
                            <table>
                                <tr>
                                    <th>{!! trans("MPCorePanel::analytics.operatingSystem") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.operatingSystemVersion") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.browser") !!}</th>
                                    <th>{!! trans("MPCorePanel::analytics.sessions") !!}</th>
                                </tr>
                                <tr>
                                </tr>
                                @foreach($browsersAndSystems['rows'] as $browser)
                                    @if($browser['ga:sessions']<5) @continue @endif
                                    <tr>
                                        <td>{!! $browser['ga:operatingSystem'] !!}</td>
                                        <td>{!! $browser['ga:operatingSystemVersion'] !!}</td>
                                        <td>{!! $browser['ga:browser'] !!}</td>
                                        <td>{!! $browser['ga:sessions'] !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @else
                <div class="alert alert-light" style="border:#ccc 1px solid">
                    {!! trans("MPCorePanel::analytics.connection_warning") !!}
                    <a id="google_connect" style="color:#6c757d" class="float-right" href="#">
                        {!! trans("MPCorePanel::analytics.google_connect") !!} <i class="fa fa-key"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{!! asset('vendor/mediapress/js/zingchart.min.js') !!}"></script>
    @if($conn == 1)
    <script>
        var pageAnalytics = {
            "type": "line",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($pageAnalytics['pageviews'] as $date=>$page)
                        "{!! translateDate("j F Y",$date) !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($pageAnalytics['pageviews'] as $page)
                        {!! $page !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.pluralPageViews") !!}"
                }
            ]
        };

        zingchart.render({
            id: "pageAnalyticsGraph",
            data: pageAnalytics,
            height: "300",
            width: "100%"
        });

        var trafficSources = {
            "type": "bar",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($trafficSources['rows'] as $source)
                        @if($source['ga:sessions']<2) @continue @endif
                        "{!! $source['ga:source'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($trafficSources['rows'] as $source)
                        @if($source['ga:sessions']<2) @continue @endif
                            {!! $source['ga:pageviews'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.pageviews") !!}"
                },
                {
                    "values": [
                        @foreach($trafficSources['rows'] as $source)
                            {!! $source['ga:exits'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.exits") !!}"
                }
            ]
        };

        zingchart.render({
            id: "trafficSourcesGraph",
            data: trafficSources,
            height: "300",
            width: "100%"
        });

        var countries = {
            "type": "area",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($countries['rows'] as $country)
                        "{!! $country['ga:country'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($countries['rows'] as $country)
                            {!! $country['ga:sessions'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.click") !!}"
                }
            ]
        };

        zingchart.render({
            id: "countries",
            data: countries,
            height: "300",
            width: "100%"
        });

        var keywords = {
            "type": "line",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($searchEngineKeywords['rows'] as $keyword)
                        "{!! $keyword['ga:keyword'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($searchEngineKeywords['rows'] as $keyword)
                        {!! $keyword['ga:sessions'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.sessions") !!}"
                }
            ]
        };

        zingchart.render({
            id: "keywordsGraph",
            data: keywords,
            height: "300",
            width: "100%"
        });

        var browsers = {
            "type": "area",
            "plot": {
                "value-box": {
                    "text": "%v"
                },
                "tooltip": {
                    "text": "%v"
                }
            },
            "legend": {
                "toggle-action": "hide",
                "header": {
                    "text": "{!! trans("MPCorePanel::analytics.columns") !!}"
                },
                "item": {
                    "cursor": "pointer"
                },
                "draggable": true,
                "drag-handler": "icon"
            },
            "scale-x": {
                "values": [
                    @foreach($browsersAndSystems['rows'] as $browser)
                        @if($browser['ga:sessions']<5) @continue @endif
                        "{!! $browser['ga:browser'] !!}",
                    @endforeach
                ]
            },
            "series": [
                {
                    "values": [
                        @foreach($browsersAndSystems['rows'] as $browser)
                        @if($browser['ga:sessions']<5) @continue @endif
                        {!! $browser['ga:sessions'] !!},
                        @endforeach
                    ],
                    "text": "{!! trans("MPCorePanel::analytics.sessions") !!}"
                }
            ]
        };

        zingchart.render({
            id: "browsersGraph",
            data: browsers,
            height: "300",
            width: "100%"
        });

    </script>
    @endif
@endpush
