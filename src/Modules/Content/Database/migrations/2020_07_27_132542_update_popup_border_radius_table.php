<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePopupBorderRadiusTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('popups', function (Blueprint $table) {
            $table->string('border_radius')->nullable()->after('button_text');
        });

    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('popups', function (Blueprint $table) {
            $table->dropColumn('border_radius');
        });
    }
}
