<?php
namespace Mediapress\Modules\MPCore\Traits;

use Carbon\Carbon;
use Cache;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Log;

trait BingAnalyticsTrait
{

    // return AvgClickPosition, Clicks, Date, Impressions (gösterimler)
    public function getAvgClickAndViews()
    {
        $methodUrl = $this->apiUrl."GetQueryStats?siteUrl=".$this->siteUrl."&apikey=".$this->apikey;
        return curlGenerator($methodUrl,null,"GET");
    }


    public function getTrafficStats()
    {
        $methodUrl = $this->apiUrl."GetQueryTrafficStats?siteUrl=".$this->siteUrl."&query=%22query1%22&apikey=".$this->apikey;
        return curlGenerator($methodUrl,null,"GET");
    }

    public function getUrlTrafficeInfo()
    {
        $methodUrl = $this->apiUrl."GetUrlTrafficInfo?siteUrl=".$this->siteUrl."&url=%22".$this->siteUrl."%22&&query=%22query1%22&apikey=".$this->apikey;
        return curlGenerator($methodUrl,null,"GET");
    }

    public function getPageStats()
    {
        $methodUrl = $this->apiUrl."GetPageStats?apikey=".$this->apikey."&siteUrl=".$this->siteUrl;
        return curlGenerator($methodUrl,null,"GET");
    }

    public function getRankAndTrafficStats()
    {
        $methodUrl = $this->apiUrl."GetQueryTrafficStats?siteUrl=".$this->siteUrl."&query=query1&apikey=".$this->apikey;
        return curlGenerator($methodUrl,null,"GET");
    }

    public function getKeywordStats()
    {
        $methodUrl = $this->apiUrl."GetKeywordStats?siteUrl=".$this->siteUrl."&apikey=".$this->apikey;
        return curlGenerator($methodUrl,null,"GET");
    }

    public function getUserWebSites()
    {
        $methodUrl = $this->apiUrl."GetUserSites?apikey=".$this->apikey;
        $cacheName = md5(serialize('user-sites'.rand(1,2000)));
        return curlGenerator($methodUrl,null,"GET");
    }

    public function request($url, $cacheName)
    {
        return Cache::remember($cacheName, $this->cache, function() use($url){
            try
            {
                $client = new GuzzleClient();
                $response = $client->request('GET', $url);

                $result = json_decode($response->getBody(), true);

            } catch (ClientException $e)
            {
                Log::error('Bing: '.$e->getMessage());

                $result = null;
            }
            return $result;
        });
    }
}