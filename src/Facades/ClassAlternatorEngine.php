<?php

namespace Mediapress\Facades;

use Mediapress\Foundation\ClassAlternator;
use Illuminate\Support\Facades\Facade;

class ClassAlternatorEngine extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ClassAlternator::class;
    }
}
