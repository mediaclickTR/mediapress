
<!DOCTYPE html>

<html><head>
    <title></title>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/MutationObserver.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/mutation-summary.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/tree-mirror.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/svg.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/mediapress/flow/recording.js') !!}"></script>
    <script type="text/javascript">
        window.recordingFrame = new HsrRecordingIframe("http:\/\/3renk.test\/tr\/projeler");
        if (!window.recordingFrame.isSupportedBrowser()) {
            var notSupportedMessage = "This browser is not supported, please use a later version or try a different browser.";
            $('html').append('<' + 'body' + '><div style="color:red; margin: 20px; font-size: 20px;font-weight:bold;">' + notSupportedMessage + '</div><' + '/' + 'body' + '>');
        } else {
            window.recordingFrame.initialMutation(JSON.parse( {!! json_encode($hsr->hsr_ev[0]['te']) !!}));
        }
    </script>
</head></html>

