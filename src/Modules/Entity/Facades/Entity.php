<?php

namespace Mediapress\Modules\Entity\Facades;

use Illuminate\Support\Facades\Facade;

class Entity extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\Entity\Entity::class;
    }
}