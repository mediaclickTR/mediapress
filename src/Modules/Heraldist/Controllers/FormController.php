<?php

namespace Mediapress\Modules\Heraldist\Controllers;

use App;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Heraldist\Models\FormDetail;
use Illuminate\Support\Facades\DB;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Facades\FilterEngine;

class FormController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    /**
     * @return mixed
     */
    public function index()
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.index","heraldist.forms.index", ])){return rejectResponse();}

        $forms = Form::where('id','<>',0);
        $queries = FilterEngine::filter($forms)['queries'];
        $forms = FilterEngine::filter($forms)['model'];

        return view('HeraldistPanel::forms.index', compact('forms','queries'));
    }
    public function create()
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.create","heraldist.forms.create", ])){return rejectResponse();}

        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('HeraldistPanel::forms.create', compact('websites'));
    }

    public function edit($id)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.update","heraldist.forms.update", ])){return rejectResponse();}

        $form = Form::find($id);
        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('HeraldistPanel::forms.edit', compact('form', 'id','websites'));
    }

    public function store(Request $request)
    {

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.create","heraldist.forms.create", ])){return rejectResponse();}

        $data = request()->except('_token', 'website_id');

        $insert = Form::firstOrCreate($data);

        // Log
        if ($insert){
            // Pivot tabloya kaydet
            foreach ($request->website_id as $website)
            {
                $insert->websites()->attach([$website]);
            }
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route("Heraldist.forms.index"))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.update","heraldist.forms.update", ])){return rejectResponse();}

        $id = $request->id;
        $data = request()->except('_token', 'website_id');
        $update = Form::updateOrCreate(['id'=>$id],$data);

        // Log
        if ($update){
            // Pivottan sil
            $update->websites()->detach();
            foreach($request->website_id as $website){
                // Pivota ekle
                $update->websites()->attach([$website]);
            }
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route('Heraldist.forms.index'))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.delete","heraldist.forms.delete", ])){return rejectResponse();}

        $form = Form::find($id);

        if ($form)
        {
            // Pivottan sil
            $form->websites()->detach();
            $form->delete();
            $form_details = FormDetail::where("form_id",$id)->get();
            $ids = [];
            foreach ($form_details as $form_detail) {
                $ids[] = $form_detail->id;
            }
            // Form details delete
            FormDetail::whereIn('id',$ids)->delete();
            // Log
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$form);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    /*
     * Form Details
     */

    public function details($form_id)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.manage_components","heraldist.forms.manage_components", ])){return rejectResponse();}

        $form = Form::where("id", $form_id)->first();
        return view('HeraldistPanel::forms.details.details', compact('form_id', 'form'));
    }

    public function detailsCreateOrUpdate(Request $request, $form_id)
    {
        $website_id = session("panel.website.id");
        // Permissions Checkpoint
        if(! activeUserCan(["heraldist.website$website_id.forms.manage_components","heraldist.forms.manage_components", ])){return rejectResponse();}


        $update = Form::where("id",$form_id)->update(['formbuilder_json' => $request->form_data]);
        if($update) {
            // log
            $model = Form::find($form_id);
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$model);
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }else{
            return redirect()->back()->with('error', trans('MPCorePanel::general.danger_message'));
        }

        /*
        DB::beginTransaction();
        try {

            $oldinputs = FormDetail::where("form_id", $form_id)->get();

            DB::commit();

                if ($oldinputs) {
                    //Eski form elemanlarını sil.
                    foreach ($oldinputs as $input)
                    {
                        FormDetail::destroy($input->id);
                    }
                }

                $datas = json_decode(json_decode($request->datas));
                foreach ($datas as $data) {
                    $insert = [
                        'form_id' => $form_id,
                        'type' => $data->type,
                        'label' => strip_tags($data->label),
                        'name' => $data->name
                    ];

                    if (isset($data->rules)){
                        $insert['rules'] = addslashes(addslashes($data->rules));
                    }
                    if (isset($data->divAttrs)){
                        $insert['div_attr'] = addslashes(addslashes($data->divAttrs));
                    }
                    if (isset($data->parameters)){
                        $insert['parameters'] = addslashes(addslashes($data->parameters));
                    }

                    if ($data->type == "select" || $data->type == "checkbox-group" || $data->type == "radio-group"){
                        $insert['values'] = json_encode($data->values,JSON_FORCE_OBJECT); // Array to JSON
                    }

                    if (isset($data->dataSource)){
                        $insert['datasource'] = $data->dataSource;
                    }

                    FormDetail::create($insert);
                }
                // Form json update
                $update = Form::where("id",$form_id)->update(['formbuilder_json' => json_decode($request->datas)]);

                if($update)
                {
                    // log
                    $model = Form::find($form_id);
                    UserActionLog::update(__CLASS__."@".__FUNCTION__,$model);
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
        catch (\Exception $e) {
            DB::rollback();
        }
        */

    }
}
