<?php

namespace Mediapress\Modules\Heraldist\Models;

use Mediapress\Facades\DataSourceEngine;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Heraldist\Foundation\FormRenderer;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class Form extends Model
{
    public const LABEL = "label";
    public const DATA_SOURCE = "dataSource";
    public const PLACEHOLDER = "placeholder";
    public const DESCRIPTION = "description";

    use SoftDeletes,CacheQueryBuilder;
    protected $table = 'forms';

    public $timestamps = true;

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'send_user',
        'name',
        'slug',
        'sender',
        'password',
        'formbuilder_json',
        'smtp',
        'port',
        'security',
        'receiver',
        'success',
        'error',
        'button',
        'captcha',
        'captcha_site_key',
        'captcha_secret'
    ];


    public function getSourceAttribute()
    {
        $lngPartKeyPrefix = "form." . $this->slug . ".";
        $data = json_decode($this->formbuilder_json, true);


        array_walk($data, function (&$i) use ($lngPartKeyPrefix) {


            if (isset($i[DATA_SOURCE])) {
                $i["values"] = DataSourceEngine::getDataSource("MPCore", $i[DATA_SOURCE], $i)->getData();
                unset($i[DATA_SOURCE]);
            }
            if (isset($i[LABEL])) {
                $i[LABEL] = langPart($lngPartKeyPrefix . $i["name"] . ".label", $i[LABEL]);
            }
            if (isset($i[PLACEHOLDER])) {
                $i[PLACEHOLDER] = langPart($lngPartKeyPrefix . $i["name"] . ".placeholder", $i[PLACEHOLDER]);
            }
            if (isset($i[DESCRIPTION])) {
                $i[DESCRIPTION] = langPart($lngPartKeyPrefix . $i["name"] . ".description", $i[DESCRIPTION]);
            }
            //placeholder


        });
        return $data;
    }

    public function extras()
    {
        return $this->morphMany(Extra::class, 'model');
    }

    public function details()
    {
        return $this->hasMany(FormDetail::class);
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function websites()
    {
        return $this->belongsToMany(Website::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, "form_id", "id");
    }

    public function renderForm()
    {
        $render = new FormRenderer($this);
        $render->render();

        $this->renderable = $render;
    }

    public function getRenderAttribute()
    {
        if ($this->renderable === null) {
            $this->renderForm();
        }
        return $this->renderable;
    }

    public function view($extra_fields=[])
    {

        /*
         * Sample Using Extra Field
        "extras"=>
            [
                [
                    'field_key'=>'extra_fields[]',
                    'field_value'=>[
                        'key'=>'example',
                        'label'=>'label.name',
                        'value'=>'value'
                    ]
                ]
            ]
        */

        $sl = DIRECTORY_SEPARATOR;
        $formBlade = 'vendor' . $sl . 'forms' . $sl . $this->attributes['slug'];
        $path = resource_path('views' . $sl . $formBlade . '.blade.php');
        $form = $this;
        if (file_exists($path)) {
            return view($formBlade, compact('form'));
        }

        if (!empty($extra_fields)) {
            $mediapress = mediapress();
            $mediapress->extra_form_fields = $extra_fields;
        }

        return view('HeraldistWeb::forms.render', compact('form'));
    }


    public function email($sender,$data)
    {
        $sended_message = $data['message'];
        $form = $this;

        if (!file_exists(resource_path("views/vendor/mail/form.blade.php"))) {
            return $sender
                ->subject($data['subject'])
                ->from($data['sender_email'], $data['sender_name'])
                ->view("HeraldistWeb::forms.email",compact('data', 'sended_message', 'form'));
        } else {
            return $sender
                ->subject($data['subject'])
                ->from($data['sender_email'], $data['sender_name'])
                ->view($data['view'], compact('data', 'sended_message', 'form'));
        }
    }
}
