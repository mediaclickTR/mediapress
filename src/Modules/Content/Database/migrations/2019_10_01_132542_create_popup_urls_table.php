<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePopupUrlsTable extends Migration {

	public function up()
	{
        if (Schema::hasTable('popup_urls'))
            return;

		Schema::create('popup_urls', function(Blueprint $table) {
			$table->integer('popup_id')->unsigned()->index();
            $table->integer('url_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::dropIfExists('popup_urls');
	}
}
