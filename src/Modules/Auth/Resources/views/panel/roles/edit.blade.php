@extends('AuthPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("AuthPanel::roles.edit") !!}</div>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="content">
                @include("MPCorePanel::inc.errors")
                @include("AuthPanel::roles.form")
            </div>
        </div>
    </div>
@endsection
