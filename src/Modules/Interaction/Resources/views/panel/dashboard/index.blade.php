@extends('MPCorePanel::inc.app')
@php
    $public_url = "vendor/mediapress/";
@endphp
@push("styles")
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/home.css') !!}"/>
    <style>
        .card{
            margin: 0;
        }
        .showFirst{
            margin-top: 25px;
            margin-bottom: 25px;
        }
       .show-online,.showPlatformAll,.showBroserAll{
            margin-bottom: 25px;
        }
        .bd-example, .bd-example.white{
            float:left;
            width: 100%;
        }
        .showFirst,.show-stats,.show-online,.showCountriesAll,.showPlatformAll,.showAgesAll,.showBroserAll,.showUrlAll{
            float: left;
            width: 100%;
            position: relative;
        }
        .resetButtonFixed{
            position: absolute;
            right: 20px;
            top:21px;
            z-index: 9;
        }
        .show-alert .alert{
            margin: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="titles" style="display: flex;align-items: center;padding: 8px 0;">
                        <h4 style="color: #656A88;font-size: 18px;margin: 0;">
                            {!! __("InteractionPanel::dashboard.title") !!}
                        </h4>
                    </div>
                    <!--<div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="javascript:void(0);" class="text-decoration-none">{!! __("InteractionPanel::dashboard.title") !!}</a>
                            </li>
                        </ul>
                    </div>!-->
                </div>
                <div class="col-md-6">
                    <div class="float-right">

                        <div class="float-right">
                            <select onchange="locationOnChange(this.value);">
                                <option @if($time==0) selected @endif
                                    value="{!! url()->current() !!}?days=0">
                                    {!! trans("InteractionPanel::dashboard.dates.today") !!}
                                </option>
                                <option @if($time==1) selected @endif
                                    value="{!! url()->current() !!}?days=1">
                                    {!! trans("InteractionPanel::dashboard.dates.yesterday") !!}
                                </option>
                                <option @if($time==7 ) selected @endif
                                    value="{!! url()->current() !!}?days=7">
                                    {!! trans("InteractionPanel::dashboard.dates.week") !!}
                                </option>
                                <option @if($time==30) selected @endif
                                    value="{!! url()->current() !!}?days=30">
                                    {!! trans("InteractionPanel::dashboard.dates.month") !!}
                                </option>
                                <option @if($time==90) selected @endif
                                    value="{!! url()->current() !!}?days=90">
                                    {!! trans("InteractionPanel::dashboard.dates.three-month") !!}
                                </option>
                                <option @if($time==180) selected @endif
                                    value="{!! url()->current() !!}?days=180">
                                    {!! trans("InteractionPanel::dashboard.dates.six-month") !!}
                                </option>
                                <option @if($time==365) selected @endif
                                    value="{!! url()->current() !!}?days=365">
                                    {!! trans("InteractionPanel::dashboard.dates.year") !!}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="show-alert" style="margin-top: 10px;"></div>
            <div class="showFirst">
                <div class="resetButtonFixed">
                    <button class="btn btn-sm btn-outline-secondary" onclick="statsAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                </div>
                <div class="show-stats">
                    <div class="bd-example white p-4 dashboardPage float-left w-100">
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="yellow"><a data-toggle="tab" href="#visit-tab" class="active">&nbsp;</a></li>
                                <li class="blue"><a data-toggle="tab" href="#session-tab">&nbsp;</a></li>
                                <li class="purple"><a data-toggle="tab" href="#cookie-tab">&nbsp;</a></li>
                            </ul>
                        </div>
                        <div class="tab-content pt-3">
                            <div id="visit-tab" class="tab-pane fade in active show">
                                <div id="visit-graph" class="dash text-center">

                                    <img
                                        src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                        height="300px" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="show-online">
                <div class="bd-example d-inline-block w-100 ">
                    <div class="dashboardBoxes w-100">
                        <div class="row">
                            <div class="col">
                                <div class="item text-center">
                                    <img
                                        src="https://cdn.dribbble.com/users/225707/screenshots/2958729/attachments/648705/straight-loader.gif"
                                        height="100px" alt="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="item text-center">
                                    <img
                                        src="https://cdn.dribbble.com/users/225707/screenshots/2958729/attachments/648705/straight-loader.gif"
                                        height="100px" alt="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="item text-center">
                                    <img
                                        src="https://cdn.dribbble.com/users/225707/screenshots/2958729/attachments/648705/straight-loader.gif"
                                        height="100px" alt="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="item text-center">
                                    <img
                                        src="https://cdn.dribbble.com/users/225707/screenshots/2958729/attachments/648705/straight-loader.gif"
                                        height="100px" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <div class="showCountriesAll">
               @if(settingSun('google.analytics.access.token') && settingSun('google.analytics.account.id'))
                   <div class="resetButtonFixed">
                       <button class="btn btn-sm btn-outline-secondary" onclick="countriesAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                   </div>
               @endif
               <div class="show-countries">
                   <div class="bd-example p-4 white mb-4">
                       <div class="dashboardMaps text-center">
                           <img
                               src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                               height="300px" alt="">
                       </div>
                   </div>
               </div>
           </div>

            <div class="showPlatformAll">
                <div class="resetButtonFixed">
                    <button class="btn btn-sm btn-outline-secondary" onclick="platformsAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                </div>
                <div class="show-platforms">
                    <div class="bd-example miniBoxes">
                        <div class="row">
                            <div class="col">
                                <div class="card p-4">
                                    <div class="card-title color m-0"></div>
                                    <div class="card-body p-0">
                                        <div id="platform-pie" class="pieChartAll">
                                            <img
                                                src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                                width="100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card p-4">
                                    <div class="card-title color m-0"></div>
                                    <div class="card-body p-0">
                                        <div id="pieTimes" class="pieChartAll">
                                            <img
                                                src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                                width="100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card p-4">
                                    <div class="card-title color m-0"></div>
                                    <div class="card-body p-0">
                                        <div id="referer-pie" class="pieChartAll">
                                            <img
                                                src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                                width="100%" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <div class="showAgesAll">
              @if(settingSun('google.analytics.access.token') && settingSun('google.analytics.account.id'))
                  <div class="resetButtonFixed">
                      <button class="btn btn-sm btn-outline-secondary" onclick="agesAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                  </div>
              @endif
              <div class="show-ages">
                  <div class="bd-example p-4 white">
                      <div class="dashboardMaps text-center">
                          <img
                              src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                              height="300px" alt="">
                      </div>
                  </div>
              </div>
          </div>
            <div class="showBroserAll">
                <div class="resetButtonFixed">
                    <button class="btn btn-sm btn-outline-secondary" onclick="browsersAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                </div>
                <div class="show-browsers">
                    <div class="bd-example p-4 white">
                        <div class="dashboardMaps text-center">
                            <img
                                src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                height="300px" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="showUrlAll">
                <div class="resetButtonFixed">
                    <button class="btn btn-sm btn-outline-secondary" onclick="urlsAjax()">{!! __("InteractionPanel::dashboard.stats.refresh") !!}</button>
                </div>
                <div class="show-urls">
                    <div class="bd-example p-4 white">
                        <div class="dashboardMaps text-center">
                            <img
                                src="https://cdn.dribbble.com/users/4053286/screenshots/7089546/media/d093f8df85feff0718f10e3adc4c3510.gif"
                                height="300px" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.highcharts.com/maps/highmaps.js"></script>
    <script src="https://code.highcharts.com/maps/modules/data.js"></script>
    <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>
    <script src="https://code.highcharts.com/modules/sunburst.src.js"></script>
    <script>
        window.graphData = {};

        $(document).ready(function () {
            onlineAjax()
            setTimeout(function () {
                ajaxList();
            }, 750)
        })

        function ajaxList() {
            let list = ['alert', 'stats', 'countries', 'platforms', 'ages', 'browsers', 'urls'];

            $.each(list, function (i, func) {
                $.ajax('{!! route('dashboard.ajax') !!}?func=' + func + '&time={!! $time !!}', {
                    success: function (data) {
                        $('.show-' + func).html(data);
                    }
                })
            })
        }

        function statsAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=stats&time={!! $time !!}', {
                success: function (data) {
                    $('.show-stats').html(data);
                }
            })
        }
        function countriesAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=countries&time={!! $time !!}', {
                success: function (data) {
                    $('.show-countries').html(data);
                }
            })
        }
        function platformsAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=platforms&time={!! $time !!}', {
                success: function (data) {
                    $('.show-platforms').html(data);
                }
            })
        }
        function agesAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=ages&time={!! $time !!}', {
                success: function (data) {
                    $('.show-ages').html(data);
                }
            })
        }
        function browsersAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=browsers&time={!! $time !!}', {
                success: function (data) {
                    $('.show-browsers').html(data);
                }
            })
        }
        function urlsAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=urls&time={!! $time !!}', {
                success: function (data) {
                    $('.show-urls').html(data);
                }
            })
        }

        //setTimeout(onlineAjax, 2000)

        function onlineAjax() {
            $.ajax('{!! route('dashboard.ajax') !!}?func=online&time={!! $time !!}', {
                success: function (data) {
                    $('.show-online').html(data);
                    setTimeout(onlineAjax, 3000)
                }
            })
        }
    </script>
@endpush
