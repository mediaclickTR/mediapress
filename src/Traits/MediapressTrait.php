<?php

namespace Mediapress\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;
use Mediapress\Modules\Content\Foundation\UrlMetaWorker;
use Mediapress\Modules\Interaction\Facades\Interaction;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Http\Request;

trait MediapressTrait
{
    public function __construct()
    {
        //mediapress objesini tüm view'lara ekliyoruz
        $this->metaworker = new UrlMetaWorker();
        view()->share("mediapress", $this);
        $this->data = [];

        /*View::composer('*', function ($view) {
            View::startPush("metas", mediapress('metas')->render());
            //TODO: head body ve footer içerikleri dolurulucak
            View::startPush("prepend_head", "");
            View::startPush("append_head", "");
            View::startPush("prepend_body", "");
            View::startPush("append_body", "");
            View::startPush("append_footer", "");
            View::startPush("prepend_footer", "");
        });*/


        if(request()->segment(1) !== 'api' && !config('mediapress.unit_test') && !request()->ajax()) {
            $this->visit = Interaction::checkCookie();
        }
    }

    public function __debugInfo()
    {
        return \Arr::except((array)$this, get_class_methods(MediapressTrait::class));
    }

    public function __wakeup()
    {
        $this->__construct();
    }

    public function __destruct()
    {
        //TODO: Cache the mediapress for next request
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        if (method_exists(self::class, $name)) {
            $this->$name = $this->$name();
            if (is_a($this->$name, Relation::class)) {
                $relation = $this->$name;
                $this->$name = $this->$name->getResults();
                $relation->getParent()->setRelation($name, $this->$name);
            }
        }
        if (isset($this->$name)) {
            return $this->$name;
        }
        if ($name == 'url') {
            $request = Request::capture();
            $path = '/' . ltrim('/', $request->path());

            return new Url(['url' => $path]);
        }
        return null;

    }

    public function fillSelf()
    {

    }

    public function init()
    {
        app()->call("Mediapress\Modules\MPCore\Controllers\WebUrlController@fillMediapress", ["parsedData" => array_merge(["path" => "/"], parse_url(request()->fullUrl()))]);

    }

    public function initWithUrl($path)
    {
        app()->call("Mediapress\Modules\MPCore\Controllers\WebUrlController@fillMediapress", ["parsedData" => array_merge(parse_url(request()->fullUrl()), ["path" => $path])]);
    }

    public function initAll()
    {
        $diffMethods = array_merge(get_class_methods(MediapressTrait::class), ["menu", "form", "search", "loginForUrl"]);
        foreach (array_diff(get_class_methods(self::class), $diffMethods) as $method_name) {
            $this->$method_name;
        }
    }
}
