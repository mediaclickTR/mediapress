<?php

namespace Mediapress\Modules\MPCore\Facades;

use Illuminate\Support\Facades\Facade;

class MetaEngine extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\MPCore\Foundation\MetaEngine::class;
    }

}