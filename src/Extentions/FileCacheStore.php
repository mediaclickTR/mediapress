<?php

namespace Mediapress\Extentions;

use Illuminate\Cache\FileStore;
use Illuminate\Support\Facades\Auth;

class FileCacheStore extends FileStore
{


    public function __construct()
    {
        parent::__construct(app('files'), config('cache.stores.file.path'), config('cache.stores.file.permission'));
    }


    public function get($key)
    {
        return parent::get($this->getPrefix() . $key); // TODO: Change the autogenerated stub
    }

    public function put($key, $value, $seconds)
    {
        return parent::put($this->getPrefix() . $key, $value, $seconds); // TODO: Change the autogenerated stub
    }

    public function forever($key, $value)
    {
        return parent::forever($this->getPrefix() . $key, $value); // TODO: Change the autogenerated stub
    }

    public function getPrefix()
    {
        $guard = Auth::guard('admin')->getSession()->has('admin');

        return md5(request()->getSchemeAndHttpHost()) . '.' . ($guard ? 1 : 0) . '.' . webp() . '.';
    }


}
