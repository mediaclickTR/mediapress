<?php

namespace Mediapress\Modules\Comment\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Modules\Comment\Models\CommentStructure;

class CommentStructureController extends Controller
{

    private $rules = [
        'name' => 'required',
        'key' => 'required',
        'status' => 'required',
        'show_time' => 'required',
        'condition' => 'required',
    ];


    public function index()
    {
        if(!activeUserCan(["comment.index"])){return rejectResponse();}

        $commentStructures = CommentStructure::all();

        return view('CommentView::comment_structure.index', compact('commentStructures'));
    }


    public function create()
    {
        if(!activeUserCan(["comment.update"])){return rejectResponse();}

        return view('CommentView::comment_structure.create');
    }


    public function store(Request $request) {

        $data = $request->except('_token');

        $labels = __('CommentPanel::create.labels');

        $this->validate($request, $this->rules, [], $labels);

        $data['admin_id'] = auth('admin')->id();
        $data['website_id'] = session('panel.website.id');

        CommentStructure::firstOrCreate($data);

        return redirect()->route('CommentStructure.index');
    }


    public function edit($id)
    {
        if(!activeUserCan(["comment.update"])){return rejectResponse();}

        $commentStructure = CommentStructure::find($id);

        return view('CommentView::comment_structure.edit', compact('commentStructure'));
    }


    public function update($id, Request $request)
    {

        $commentStructure = CommentStructure::find($id);

        if(is_null($commentStructure)) {
            return redirect()->back()->withError(['message' => "Yorum yapısı bulunamadı!"]);
        }
        $data = $request->except('_token');

        $labels = __('CommentPanel::create.labels');


        $this->validate($request, $this->rules, [], $labels);

        $commentStructure->update($data);

        return redirect()->route('CommentStructure.edit', ['id' => $commentStructure->id]);
    }


    public function delete($id)
    {
        if(!activeUserCan(["comment.update"])){return rejectResponse();}

        $commentStructure = CommentStructure::find($id);

        if($commentStructure) {
            $commentStructure->delete();

            return redirect()->route('CommentStructure.index');
        } else {
            return redirect()->back()->withError(['message' => "Yorum yapısı bulunamadı!"]);
        }
    }
}
