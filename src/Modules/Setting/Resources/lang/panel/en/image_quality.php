<?php

return [
    'modal_title' => "Image Settings",
    'quality' => "Quality Ratio =",
    "original_size" => "Original Size =",
    "new_size" => "New Size =",
];
