<?php

namespace Mediapress\Models;

use Closure;
use Illuminate\Support\Facades\File;
use Mediapress\Contracts\IModule;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Support\Database\CacheQueryBuilder;

Class MPModule implements IModule
{
    use CacheQueryBuilder;

    public function getMPModules($only_actives=false)
    {
        return ModulesEngine::getMPModules($only_actives);
    }

    public function getMPSlots()
    {
        return ModulesEngine::getMPSlots();
    }

    public function getModuleClasses()
    {
        return ModulesEngine::getModuleClasses();
    }

    public function getMPModuleNames()
    {
        return ModulesEngine::getMPModuleNames();
    }

    public function getRenderableObjectTypes()
    {
        // TODO: Implement getRenderableObject() method.
    }

    public function serveClasses()
    {
        // TODO: Implement serveClasses() method.
    }

    public function getPanelMenus()
    {
        // TODO: Implement getPanelMenus() method.
    }

    /*protected function getActions($section=null)
    {
        dump(strtolower($this->name)."_module_actions");
        $actions = config(strtolower($this->name)."_module_actions");
        $actions = \getActions($this->name, $section,true,true);
        return $actions;
        if($section){
            $actions = data_get($actions,$section,[]);
        }
        //return compileActionsRecursive(null,null,null,true,true);
        //return $actions;
    }*/

    public function getActions($section = null, $include_subs = false, $include_variations = false)
    {
        $module_key = strtolower($this->name);
        $key = $module_key . "_module_actions" . ($section ? ".$section" : "");

        //return dd($key);

        // tek bir section için istenmişse
        if ($section) {
            $sections = [$section => config($key)];
            return $this->compileActionsRecursive($sections[$section], $section, $section, $include_subs, $include_variations);
        } // tüm sectionlar istenmişse:
        else {
            $sections = config($key) ?? [];
            $result = [];

            foreach ($sections as $seckey => $section) {
                $result = array_merge($result, $this->compileActionsRecursive($section, $seckey, $seckey, $include_subs, $include_variations));
            }
            return $result;
        }


    }

    protected function compileActionsRecursive($section, $section_key, $starting_key, $include_subs = false, $include_variations = false)
    {
        $key = $section_key;
        $result = [
            $key => [
                "name" => $section["name"],
                "node_type" => $section["node_type"] ?? "accordion-card",
                NAME_AFFIX => (isset($section[NAME_AFFIX]) && $section[NAME_AFFIX] ? $section[NAME_AFFIX] : ""),
                ACTIONS_STR => [],
                'subs' => [],
                VARIATIONS => [],
                'variations_title' => (isset($section["variations_title"]) ? $section["variations_title"] : ""),
                'data' => (isset($section["data"]) ? $section["data"] : []),
                'settings'=>$section["settings"] ?? []
            ]
        ];
        if (isset($section["actions"]) && count($section["actions"])) {

            foreach ($section["actions"] as $modactkey => $modaction) {
                $modaction["value"] = null;
                $result[$key]["actions"][$starting_key . "." . $modactkey] = $modaction;
            }
            //$result=$actions;
        }
        if ($include_subs) {
            if (isset($section['subs']) && count($section['subs'])) {
                foreach ($section['subs'] as $sectsubkey => $sectsub) {
                    $result[$key]["subs"] = array_merge($result[$key]["subs"], $this->compileActionsRecursive($sectsub, $sectsubkey, $starting_key . "." . $sectsubkey, $include_subs, $include_variations));
                }
            }
        }
        if ($include_variations) {
            if (isset($section["variations"])) {
                if (is_object($section["variations"]) && ($section["variations"] instanceof Closure)) {
                    $section["variations"] = $section["variations"]($section);
                } elseif($section["variations"] == "website_variations") {
                    $section["variations"] = getVariations($section);
                } elseif($section["variations"] == "heraldist_variations") {
                    $section["variations"] = getHeraldistVariations($section);
                }elseif($section["variations"] == "sitemap_variations") {

                    if($section["item_id"]=="*"){
                        return [];
                    }

                    $sitemaps = \Mediapress\Modules\Content\Models\Sitemap::with('detail')->get();
                    $sets = [];

                    foreach ($sitemaps as $sm) {
                        if (!$sm->detail) {
                            continue;
                        }
                        $set = [];
                        $set["name"] = 'ContentPanel::auth.sections.sitemap';
                        $set["name_affix"] = $sm->detail->name;
                        $set['item_model'] = \Mediapress\Modules\Content\Models\Sitemap::class;
                        $set['item_id'] = $sm->id;
                        $set['actions'] = [
                            'update' => ['name' => "MPCorePanel::auth.actions.update",],
                            'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                        ];
                        $set['settings'] = [
                            "auth_view_collapse" => "out"
                        ];
                        $set['data'] = [
                            "is_root" => false,
                            "is_variation" => true,
                            "is_sub" => false,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ];
                        $set['subs'] = $section['subs'];
                        data_set($set['subs'], '*.data.descendant_of_variation', true);
                        $sets['sitemap' . $sm->id] = $set;
                    }
                    $section['variations'] = $sets;
                }
                foreach ($section["variations"] as $sectvarkey => $sectvar) {
                    $result[$key]["variations"] = array_merge($result[$key]["variations"], $this->compileActionsRecursive($sectvar, $sectvarkey, $starting_key . "." .$sectvarkey, $include_subs, $include_variations));
                }
            }
        }


        return $result;

    }

    public function fillActionsContainer($container)
    {
        $container["subs"] = self::getActions(null, true, true) ?? [];
        return $container;
    }

    public function getBreadcrumb($crumbs = [])
    {

        $breadcrumb = array_merge($this->getBreadcrumbBase(), $crumbs);
        /*        dump("Sent crumbs are:", $crumbs);
                dump($base = $this->getBreadcrumbBase());
                dump(array_merge($this->getBreadcrumbBase(), $crumbs));*/
        return array_merge($this->getBreadcrumbBase(), $this->getModuleCrumb(), $crumbs);
    }

    protected function getModuleCrumb(){
        return [];
    }

    private function getBreadcrumbBase()
    {
        return [
            [
                "key" => "dashboard",
                "icon" => "home",
                "text" => trans("MPCorePanel::menu_titles.dashboard"),
                "href" => route('panel.dashboard')
            ],
        ];
    }
}
