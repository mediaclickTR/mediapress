@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">

        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h3 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">{{ $commentStructure->name }}</h3>
            </div>
        </div>

        <div class="content mt-3">
            <div class="block block-rounded">
                <div class="block-content">
                    <div id="accordion" class="mb-3" role="tablist" aria-multiselectable="true">
                        @foreach($comments as $comment)
                            <div class="block block-rounded mb-1">
                                <div class="block-header block-header-default {{ $comment->read == 2 ? 'bg-black-10' : '' }}" role="tab" id="accordion_{{ $comment->id }}">
                                    <a class="font-w600 w-100 accordionBtn"
                                       data-toggle="collapse"
                                       data-parent="#accordion"
                                       href="#accordion_q{{ $comment->id }}"
                                       aria-expanded="true"
                                       aria-controls="accordion_q{{ $comment->id }}"
                                       data-id="{{ $comment->id }}"
                                       data-read="{{ $comment->read }}">
                                        <div class="row">
                                            <div class="col-md-3">{{ $comment->model->detail->name }}</div>
                                            <div class="col-md-6">{{ \Str::limit(strip_tags($comment->comment), 50) }}</div>
                                            <div class="col-md-2">{{ $comment->created_at->formatLocalized('%d %B %Y') }}</div>
                                            <div class="col-md-1"><i class="fa fa-chevron-down float-right"></i></div>
                                        </div>
                                    </a>
                                </div>
                                <div id="accordion_q{{ $comment->id }}" class="collapse" role="tabpanel" aria-labelledby="accordion_{{ $comment->id }}" data-parent="#accordion">

                                    <div class="block-content">
                                        <table class="table table-vcenter">
                                            <tbody>
                                            <tr>
                                                <th class="font-w600">Sayfa Adı</th>
                                                <td>
                                                    {{ $comment->model->detail->name }}
                                                </td>
                                            </tr>
                                            @if($comment->parent)
                                                <tr>
                                                    <th class="font-w600">Cevap Verilen Yorum</th>
                                                    <td>
                                                        {{ $comment->parent->comment }}
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <th class="font-w600">Yorum</th>
                                                <td>
                                                    {{ $comment->comment }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="font-w600">Kullanıcı Adı</th>
                                                <td>
                                                    {{ $comment->fullname }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="font-w600">Kullancıcı E-posta</th>
                                                <td>
                                                    {{ $comment->email }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="font-w600">IP</th>
                                                <td>
                                                    {{ $comment->ip }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="font-w600">Gönderilme Tarihi</th>
                                                <td>
                                                    {{ $comment->created_at->formatLocalized('%d %B %Y, %H:%M:%S') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="font-w600"></th>
                                                <td>
                                                    @if($comment->status == 1)
                                                    <a href="{!! route('CommentStructure.comments.update', ['commentStructureId' => $commentStructure->id, 'id' => $comment->id, 'status' => 2]) !!}" class="mr-2 btn btn-sm btn-success text-white" title="Pasif Yap">
                                                        <i class="fa fa-check"></i> Aktif
                                                    </a>
                                                    @else
                                                        <a href="{!! route('CommentStructure.comments.update', ['commentStructureId' => $commentStructure->id, 'id' => $comment->id, 'status' => 1]) !!}" class="mr-2 btn btn-sm btn-danger text-white" title="Aktif Yap">
                                                            <i class="fa fa-times"></i> Pasif
                                                        </a>
                                                    @endif
                                                    <a href="{!! route('CommentStructure.comments.delete', ['commentStructureId' => $commentStructure->id, 'id' => $comment->id]) !!}" class="deleteBtn btn btn-sm btn-warning text-white" title="{!! trans("MPCorePanel::general.delete") !!}">
                                                        <i class="fa fa-trash-alt"></i> Sil
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .bg-black-10 {
            background-color: rgba(0,0,0,.1)!important;
        }
        .block-header-default {
            background-color: #f7f7f7;
        }

        .block-header {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -ms-flex-align: center;
            align-items: center;
            padding: .75rem 1.25rem;
            transition: opacity .25s ease-out;
        }
        .block-content {
            transition: opacity .25s ease-out;
            width: 100%;
            margin: 0 auto;
            padding: 1.25rem 1.25rem 1px;
            overflow-x: visible;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('.accordionBtn').click(function () {
            var self = $(this);
            var id = self.attr('data-id');
            var readStatus = self.attr('data-read') == 2;

            if(readStatus) {
                $.ajax({
                    'url': '{{ route('CommentStructure.comments.updateReadStatus', ['commentStructureId' => $commentStructure->id]) }}',
                    'method': 'GET',
                    'data': {id: id},
                    success: function () {
                        self.closest('div.block-header').removeClass('bg-black-10');
                        self.attr('data-read', 1);
                    }
                })
            }
        });
    </script>
@endpush
