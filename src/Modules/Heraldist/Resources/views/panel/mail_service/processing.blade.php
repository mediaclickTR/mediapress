@extends("HeraldistPanel::inc.module_main")
@section('content')
    <div class="page-content">
        <div class="title">
            {!! trans("MPCorePanel::general.select_service") !!}
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <a class="float-right" href="{!! url('mp-admin/Heraldist/Mailservice/MPMailler') !!}"><button class="btn btn-primary">{!! trans("MPCorePanel::general.mpmailler") !!}</button></a>
                </div>
                <div class="col-md-6">
                    <a href="{!! url('mp-admin/Heraldist/Mailservice/Mailchimp') !!}"><button class="btn btn-primary">{!! trans("MPCorePanel::general.mailchimp") !!}</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection
