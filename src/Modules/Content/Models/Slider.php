<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Foundation\SliderEngine;
use Mediapress\Modules\MPCore\Models\BaseModel;

class Slider extends BaseModel
{
    use SoftDeletes;

    protected $table = "sliders";

    public $timestamps = true;

    protected $dates = ["deleted_at"];

    public $fillable = [
        "website_id",
        "name",
        "type",
        "text",
        "button",
        "screen",
        "width",
        "height",
        "admin_id"
    ];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function website(){
        return $this->belongsTo(Website::class);
    }

    public function scenes(){
        return $this->hasMany(Scene::class);
    }

    public function render(){

        $sliderEngine = new SliderEngine($this);
        return $sliderEngine->render();
    }

    public function build()
    {
        $sliderEngine = new SliderEngine($this);
        return $sliderEngine->build();
    }
}
