<?php

return [
    'sitemap_types'=>' Sayfa Yapısı Türleri',
    'create'=>' Tür Ekle',
    'edit'=>' Tür Düzenle',
    'name'=>' Sayfa Yapısı Tür Adı',
    'status'=>'Tür Durumu',
];
