<?php

namespace Mediapress\Modules\Locale\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Modules\MPCore\Models\Country;

class Province extends Model
{
    protected $connection = 'mediapress_locale';
    protected $table = 'provinces';
    protected $fillable = ["country_id", "name"];

    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function counties()
    {
        return $this->hasMany(County::class, 'province_id');
    }

    public function getPlateAttribute()
    {
        return $this->id;
    }
}
