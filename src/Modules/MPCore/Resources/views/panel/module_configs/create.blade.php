@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">Modül Özelliği Oluştur</h1>
        <div class="col-md-12">
            <form action="{!! route('MPCore.module_configs.store') !!}" novalidate method="POST">
                @csrf
                <div class="form-group rel focus">
                    <div class="tit">Modül Seçimi</div>
                    <select class="nice" name="module_id" required>
                        <option value="">Modül Seçiniz</option>
                        @foreach($modules as $module)
                            <option value="{!! $module->id !!}">{!! $module->name !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group rel focus">
                    <div class="tit">Özellik Adı</div>
                    <input type="text" name="config_name">
                </div>
                <div class="form-group rel focus">
                    <div class="tit">Özellik Key</div>
                    <input type="text" name="key">
                </div>
                <div class="form-group rel focus">
                    <div class="tit">Form Türü</div>
                    <select name="type" id="type" required>
                        <option value="">Seçim Yapınız</option>
                        <option value="text">Text</option>
                        <option value="select">Select Box</option>
                    </select>
                </div>
                <div class="form-group rel focus" id="ektralar" style="display:none;">
                    <div class="tit">Ektralar (JSON)</div>
                    <textarea style="min-height:200px;" name="details" placeholder="Radio, checkbox veya select seçimlerde JSON tipinde seçimler girilmelidir."></textarea>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            $("#type").change(function(){
                var value = $(this).val();
                if(value == "checkbox" || value == "radio" || value == "select"){
                    $("#ektralar").show();
                }
                else{
                    $("#ektralar").hide();
                }
            });
        });
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
    </script>
@endpush
@push("styles")
    <style>
        select{
            width:100% !important;
        }
    </style>
@endpush