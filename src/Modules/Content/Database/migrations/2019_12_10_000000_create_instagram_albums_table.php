<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramAlbumsTable extends Migration {

	public function up()
	{
	    // neden mediapress blueprint kullnılıyor öğren
        Schema::create('instagram_albums', function(Blueprint $table) {

            $table->bigIncrements("id");
            $table->text("name");
            $table->text("slug");
            $table->integer("status");
            $table->integer("sync_type")->nullable();
            $table->text("sync_data")->nullable();
            $table->integer("sync_minutes")->nullable();
            $table->time("sync_last")->nullable();
            $table->integer("sync_limit")->nullable();
            $table->text("sync_priority")->nullable();
            $table->json("last_posts_datas")->nullable();
            $table->timestamps();
            $table->softDeletes();
		});

	}

	public function down()
	{
		Schema::drop('instagram_albums');
	}
}
