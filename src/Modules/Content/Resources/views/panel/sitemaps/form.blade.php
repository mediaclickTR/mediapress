{!! Form::open(['route' => ['Content.sitemaps.store',"isNew"=>request()->isNew=="yes" ? "yes":'no'],'method' => 'POST'])  !!}
{!! Form::hidden("id",$sitemap->id) !!}

<div class="form-group">
    {{--{!! Form::label("select_websites", trans("ContentPanel::sitemap.select_websites")) !!}
    {!! Form::select("websites[]", $websites,old("websites[]"), ['class' => 'nice', 'required']) !!}--}}
    <input type="hidden" name="websites[]" value="{{session("panel.website")->id}}">
    <div class="clearfix"></div>
</div>


@if($sub_active)
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! trans("ContentPanel::sitemap.type_home") !!}</div>
                {!! Form::select("type", /*$types->pluck("name","id")*/["static"=>"Statik (Tek sayfa)","dynamic"=>"Dinamik (Çok sayfalı)"],old("type"), ['class' => 'nice','required',"id"=>"sub_key_selector"]) !!}
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! trans("ContentPanel::sitemap.parent_sitemap") !!}</div>
                {!! Form::select("parent_sitemap_id", /*$types->pluck("name","id")*/$sitemaps,old("parent_sitemap_id"), ['class' => 'nice',"id"=>"parent_key_selector"]) !!}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! trans("ContentPanel::sitemap.sub_key") !!}</div>
                {!! Form::text("sub_key", old("sub_key"), ['placeholder'=>trans("ContentPanel::sitemap.sub_key"),'class' =>'required','required']) !!}
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-4 offset-2">
            <div class="checkbox mt-5">
                <label title="{!! trans("ContentPanel::sitemap.url_status.input.tooltip") !!}">
                    {!! Form::checkbox('url_status', 'on', old('url_status'),["class"=>"form-check-input"]) !!}
                    {!! trans("ContentPanel::sitemap.url_status") !!}
                </label>
            </div>
        </div>
    </div>
@else

    <div class="col-12 p-0">
        <div class="form-group">
            <div class="tit">{!! trans("ContentPanel::sitemap.type_home") !!} :
                @if(old("type") == "static")
                    Statik (Tek sayfa)
                @else
                    Dinamik (Çok sayfalı)
                @endif
            </div>
        </div>
    </div>
    <div class="col-12 p-0">
        <div class="form-group">
            <div class="tit">{!! trans("ContentPanel::sitemap.parent_sitemap") !!}</div>
            {!! Form::select("parent_sitemap_id", /*$types->pluck("name","id")*/$sitemaps,old("parent_sitemap_id",$sitemap->sitemap_id), ['class' => 'nice',"id"=>"parent_key_selector"]) !!}
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! trans("ContentPanel::sitemap.sub_key") !!} : {{ old("sub_key") }}</div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-4 offset-2">
            <div class="checkbox mt-5">
                <label title="{!! trans("ContentPanel::sitemap.url_status.input.tooltip") !!}">
                    {!! Form::checkbox('url_status', 'on', old('url_status'),["class"=>"form-check-input"]) !!}
                    {!! trans("ContentPanel::sitemap.url_status") !!}
                </label>
            </div>
        </div>
    </div>
@endif

<div class="form-group hidden">
    <div class="tit"
         title="{!! trans("ContentPanel::sitemap.filesystem.description") !!}">{!! trans("ContentPanel::sitemap.filesystem.pls_select") !!}</div>
    {!! Form::text("filesystem", old("filesystem"), ['placeholder'=>trans("ContentPanel::sitemap.filesystem"),'class' =>'required','required']) !!}
    <div class="clearfix"></div>
</div>

<div class="form-group hidden">
    <div class="tit"
         title="{!! trans("ContentPanel::sitemap.sfilesystem.description") !!}">{!! trans("ContentPanel::sitemap.sfilesystem.pls_select") !!}</div>
    {!! Form::text("sfilesystem", old("sfilesystem"), ['placeholder'=>trans("ContentPanel::sitemap.sfilesystem"),'class' =>'required','required']) !!}
    <div class="clearfix"></div>
</div>

<div class="form-group">
    <div class="tit">{!! trans("ContentPanel::sitemap.structural_features") !!}</div>
    <div class="row">
        @foreach(["searchable", "has_detail", "auto_order", "sub_page_order", "has_reserved_url", "has_category", "has_criteria", "has_property"] as $key)
            <div class="col-md-3">
                <div class="checkbox">
                    <label title="{!! trans("ContentPanel::sitemap.".$key.".input.tooltip") !!}">
                        {!! Form::checkbox($key, 'on', old($key),["class"=>"form-check-input"]) !!}
                        {!! trans("ContentPanel::sitemap.".$key) !!}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
    <div class="clearfix"></div>
</div>

<div class="form-group rel">
    <div class="tit">{!! trans("ContentPanel::sitemap.menu_settings") !!}</div>
    <div class="row">
        @foreach(["show_in_panel_menu","featured_in_panel_menu"] as $key)
            <div class="col-md-3 @if($loop->first) col-md-offset-1 @endif">
                <div class="checkbox">
                    <label title="{!! trans("ContentPanel::sitemap.".$key.".input.tooltip") !!}">
                        {!! Form::checkbox($key, 'on', old($key),["class"=>"form-check-input"]) !!}
                        {!! trans("ContentPanel::sitemap.".$key) !!}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<div class="title mb-3 mt-5">{{trans("ContentPanel::sitemap.variations_title")}}</div>

@php

    $website_variation_data = $variation_templates[session("panel.website.id")];

@endphp

@foreach($website_variation_data["country_groups"] as $cgid => $cgvd)
    <div>
        @if(count($website_variation_data["country_groups"])>1)
            <h6 class="text-black-50 mb-0" style="font-size:21px;">Hedef Bölge: {{ $cgvd["title"] }} ({{ $cgvd["code"] }}) </h6>
        @endif
        <table class="table table-hover" id="sitemap_slugs_table">
            <thead>
            <tr>
                <th width="7%">{!! __('ContentPanel::sitemap.on_off') !!}</th>
                <th width="13%">{!! __('ContentPanel::sitemap.language') !!}</th>
                <th width="25%">{!! __('ContentPanel::seo.title') !!}</th>
                <th width="55%">{!! __('ContentPanel::sitemap.slugs') !!}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cgvd["languages"] as $lngid => $lng)
                @php
                    $detail = $sitemap->details()->where("country_group_id",$cgid)->where("language_id",$lngid)->first();

                @endphp
                @php  $prefix = getWebsiteDetailUrl(session("panel.website.id"), $cgid, $lngid)[0]; @endphp
                <tr data-cgid="{{$cgid}}" data-lngid="{{$lngid}}">
                    <td class="">
                        <div class="checkbox">
                            <label title="">
                                <input class="form-check-input chk_sitemap_detail_active" @if($detail && $detail->exists) checked="checked" @endif
                                       name="sitemap_detail_active[{{ $cgid }}][{{ $lngid }}]" type="checkbox"
                                       value="on"/>
                            </label>
                        </div>
                        {{--<button class="btn btn-danger btn-sm deactivate-detail @if( ! $detail) d-none @endif" type="button" title="Bu ülke grubu/dil çiftini kapat ve ona ait içeriği sil"><i class="fa fa-times"></i></button>
                        <button class="btn btn-success btn-sm activate-detail @if($detail) d-none @endif" type="button" title="Bu ülke grubu/dil çiftini etkinleştir"><i class="fa fa-check"></i></button>
                        <input type="hidden" name="sitemap_detail_active[{{ $cgid }}][{{ $lngid }}]" id="sitemap_detail_active_{{ $cgid }}_{{ $lngid }}">--}}
                    </td>
                    <td class="">
                        <img src="{{ mp_asset('vendor/mediapress/images/flags/'. strtoupper($lng["code"]).'.png') }}"
                             class="rounded"
                             alt=""> {{ $lng["name"] }} ({{ $lng["code"] }}) {{--<strong>*</strong>--}}
                    </td>
                    <td class="">
                        <div class="form-group">
                            <label for="sitemap_name[{{ $lng["code"] }}]"></label>
                            <input type="text" class="form-control sitemap_name auto_sync_slug"
                                   name="sitemap_name[{{ $cgid }}][{{ $lngid }}]"
                                   id="sitemap_name_{{ $cgid }}_{{ $lngid }}" value="{{ $detail->name ?? "" }}"
                                   placeholder="">
                        </div>
                    </td>
                    <td class="">
                        <div class="row mb-1 sitemapSlugDiv {{ $sitemap && $sitemap->urlStatus == 1 ? '' : 'd-none' }}">
                            <div class="col-md-3 col-lg-3 align-middle">
                                <span class="">{!! __('ContentPanel::page.page_structure_slug') !!}</span>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" title="Başlıktan otomatik güncelle">
                                            <label for="check" class="m-0">
                                                <i class="fa fa-link mr-1"></i>
                                                <input type="checkbox" class="align-middle ml-1 chk_auto_sync_slug"
                                                       id="auto_sync_slug_{{ $cgid }}_{{ $lngid }}" {{ $detail ? "" : "checked" }}>
                                            </label>
                                        </span>
                                        <span class="input-group-text">
                                    {{ getWebsiteRootUrl(session("panel.website.id"))}}@if($prefix){!! '<strong class="text-primary">/'.$prefix.'</strong>' !!}@endif
                                </span>
                                    </div>
                                    <input class="form-control  slug-input" type="text"
                                           value="{{$detail->slug ?? ""}}"
                                           name="sitemap_slug[{{ $cgid }}][{{ $lngid }}]"
                                           id="sitemap_slug_{{ $cgid }}_{{ $lngid }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row category-field">
                            <div class="col-md-3 col-lg-3">
                                <span class="">Kök Kat. Slug</span>
                            </div>
                            <div class="col-md-9 col-lg-9">
                                <input type="hidden" name="sitemap_pattern[{{ $cgid }}][{{ $lngid }}]">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            {{ getWebsiteRootUrl(session("panel.website.id"))}}@if($prefix){!! '<strong class="text-primary">/'.$prefix.'</strong>' !!}@endif
                                        </span>
                                    </div>
                                    <input class="form-control  category-slug-input"
                                           type="text"
                                           value="{{$detail->category_slug ?? "/"}}"
                                           name="sitemap_catslug[{{ $cgid }}][{{ $lngid }}]"
                                           id="sitemap_catslug_{{ $cgid }}_{{ $lngid }}"/>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



@endforeach

<hr>
<div class="sitemap_group" style="clear: both;"></div>

<div class="submit mt-5">{!! Form::button(trans("MPCorePanel::general.save"),["type"=>"submit", "class"=>"btn btn-primary float-right"]) !!}</div>

{!! Form::close() !!}

@push("scripts")
    <script src="{!! url('vendor/mediapress/js/icheck.min.js') !!}"></script>
    <script>
        var xhr_requests = [];
        var sitemapUrlStatus = "{!! $sitemap && $sitemap->urlStatus == 1 ? 'true' : 'false' !!}";

        $(document).ready(function () {

            @if($sitemap && $sitemap->feature_tag !== 'homepage')
                    $('html').delegate('input.sitemap_name.auto_sync_slug', 'keyup', function () {

                        if(sitemapUrlStatus == false) {
                            return;
                        }
                        var id = $(this).attr("id");
                        var value = $(this).val();
                        var slug_input = $(this).closest("tr").find("input.slug-input");
                        var token = $("meta[name='csrf-token']").attr('content');
                        var tr = $(this).closest("tr");

                        var cgid = tr.data("cgid");
                        var lngid = tr.data("lngid");

                        if (xhr_requests[id] !== undefined) {
                            xhr_requests[id].abort();
                            delete (xhr_requests[id]);
                        }

                        xhr_requests[id] = $.ajax({
                            method: "POST",
                            url: "{{route("Content.checkUrl")}}",
                            data: {
                                _token: token,
                                detail_type: 'Mediapress\\Modules\\Content\\Models\\SitemapDetail',
                                detail_id: 0,
                                parent_id: $('[name=id]').val(),
                                language_id: lngid,
                                country_group_id: cgid === 0 ? null : cgid,
                                sluggable_text: value,
                            },
                            success: function (data) {
                                $('[name=\"sitemap_pattern\[' + cgid + '\]\[' + lngid + '\]\"]').val(data.pattern);
                                $('[name=\"sitemap_slug\[' + cgid + '\]\[' + lngid + '\]\"]').val("/" + data.slug);
                            }
                        });
                    });
            @endif
{{--
            $('html').delegate('input.chk_auto_sync_slug', 'change', function () {
                var tr = $(this).closest("tr");
                if ($(this).prop("checked")) {
                    $(tr).find("input.sitemap_name").addClass("auto_sync_slug").trigger("keyup");
                } else {
                    $(tr).find("input.sitemap_name").removeClass("auto_sync_slug");
                }
            });
--}}

            $(document).delegate('input.chk_sitemap_detail_active', 'ifChanged', function () {
                var tr = $(this).closest("tr");
                if (this.checked) {
                    $(tr).css("opacity", 1);
                    $("input[type=text]", tr).prop("readonly", false);
                    $("input.chk_auto_sync_slug", tr).prop("disabled", false);
                } else {
                    $(tr).css("opacity", 0.4);
                    $("input[type=text]", tr).prop("readonly", true);
                    $("input.chk_auto_sync_slug", tr).prop("disabled", true);
                }
            });

            $(document).delegate("[name=has_category]", 'ifChanged', function () {
                if (this.checked) {
                    $('.category-field').show(100).children("input").removeAttr("disabled");
                } else {
                    $('.category-field').hide(100).children("input").attr("disabled", "disabled");
                }
            });

            $("div.checkbox input[type=checkbox]").trigger('ifChanged');
        });
    </script>

    <script>

        $(document).ready(function() {
            var _type = "{{old("type")}}" != "" ? "{{old("type")}}" : $('[name="type"]').val();
            setFeatures(_type);

            $('[name="type"]').on('change', function(e) {
                setFeatures(e.target.value);
            });

            $('[name="url_status"]').on('ifUnchecked', function(e) {
                sitemapUrlStatus = false;
                $('input.slug-input').val('/');
                $('.sitemapSlugDiv').addClass('d-none');
            });
            $('[name="url_status"]').on('ifChecked', function(e) {
                sitemapUrlStatus = true;
                $('input.sitemap_name').trigger('keyup');
                $('.sitemapSlugDiv').removeClass('d-none');
            });
        });

        function setFeatures(_type) {
            if(_type == 'static') {
                $('[name="auto_order"]').iCheck('uncheck')
                $('[name="auto_order"]').iCheck('disable')
                $('[name="auto_order"]').closest('label').css('color', "#cecece")

                $('[name="sub_page_order"]').iCheck('uncheck')
                $('[name="sub_page_order"]').iCheck('disable')
                $('[name="sub_page_order"]').closest('label').css('color', "#cecece")

                $('[name="has_reserved_url"]').iCheck('uncheck')
                $('[name="has_reserved_url"]').iCheck('disable')
                $('[name="has_reserved_url"]').closest('label').css('color', "#cecece")

                $('[name="has_category"]').iCheck('uncheck')
                $('[name="has_category"]').iCheck('disable')
                $('[name="has_category"]').closest('label').css('color', "#cecece")

                $('[name="has_criteria"]').iCheck('uncheck')
                $('[name="has_criteria"]').iCheck('disable')
                $('[name="has_criteria"]').closest('label').css('color', "#cecece")

                $('[name="has_property"]').iCheck('uncheck')
                $('[name="has_property"]').iCheck('disable')
                $('[name="has_property"]').closest('label').css('color', "#cecece")
            } else if(_type == 'dynamic') {
                $('[name="auto_order"]').iCheck('enable')
                $('[name="auto_order"]').closest('label').css('color', "#646464")

                $('[name="sub_page_order"]').iCheck('enable')
                $('[name="sub_page_order"]').closest('label').css('color', "#646464")

                $('[name="has_reserved_url"]').iCheck('enable')
                $('[name="has_reserved_url"]').closest('label').css('color', "#646464")

                $('[name="has_category"]').iCheck('enable')
                $('[name="has_category"]').closest('label').css('color', "#646464")

                $('[name="has_criteria"]').iCheck('enable')
                $('[name="has_criteria"]').closest('label').css('color', "#646464")

                $('[name="has_property"]').iCheck('enable')
                $('[name="has_property"]').closest('label').css('color', "#646464")
            }
        }
    </script>
@endpush
