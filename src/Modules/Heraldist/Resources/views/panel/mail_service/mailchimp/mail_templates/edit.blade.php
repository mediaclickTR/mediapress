@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <h1 class="title">{!! trans("HeraldistPanel::mail_service.edit_template") !!}</h1>
        <div class="col-md-12">
            <form action="{!! route('Heraldist.mailchimp.mail_templates.update') !!}" method="POST">
                @include("HeraldistPanel::mail_service.mailchimp.mail_templates.form")
                <div class="float-right">
                    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="application/javascript">
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
        $(document).ready(function () {
            CKEDITOR.replace( 'editor2' );
        });
    </script>
@endpush
