<?php

namespace Mediapress\Foundation;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mediapress\AllBuilder\Foundation\FormStorer;
use Mediapress\Modules\MPCore\Models\Backup;

class BackupData
{

    public function store(FormStorer $formStorer){

        $object = $formStorer->getMainObjectFromBucket();

        if(is_null($object)) {
            return;
        }

        $backup = new Backup([
            'admin_id' => Auth::guard('admin')->user()->id,
            'ip'=>request()->ip(),
            'content' => $formStorer->getFormData(),
            'model_id' => $object->id,
            'model_type'=> get_class($object),
        ]);

        $backup->save();

    }

    public function get(Request $request){
        return Backup::where('model_type',$request->type)->where('model_id',$request->id)->orderBy('created_at','desc')->get();
    }

    public function frontStore($parent, $content)
    {

        $backup = new Backup([
            'admin_id' => Auth::guard('admin')->user()->id,
            'ip'=>request()->ip(),
            'content' => $content,
            'model_id' => $parent->id,
            'model_type'=> get_class($parent),
        ]);

        $backup->save();
    }

}
