@include('HeraldistWeb::inc.alerts')

<form action="{!! $form->render->url !!}" method="post" enctype="multipart/form-data">
    {!! $form->render->hidden !!}
    <div class="row">

        @include('HeraldistWeb::forms.main',['fields'=>$form->render->fields])

    </div>


    <div class="row">
        <div class="form-group col-md-6">
            @if(strlen($form->captcha_site_key)>5)
                <div class="g-recaptcha" data-sitekey="{{$form->captcha_site_key}}"></div>
            @endif
        </div>

        <div class="form-group text-right col-md-6">
            <button type="submit" class="btn btn-primary">{!! langPart('form.button.'.$form->button,$form->button) !!}</button>
        </div>
    </div>
</form>


@push('styles')
    {!! $form->render->style !!}
@endpush

@pushonce('scripts:form')
    {!! $form->render->script !!}
    <script src="https://www.google.com/recaptcha/api.js?hl={{ mediapress()->activeLanguage->code }}" async defer></script>
    <script>

        $('form[action="{{route('form.store')}}"]').on('submit',function(){
            movePlus();
        });

    </script>
@endpushonce
