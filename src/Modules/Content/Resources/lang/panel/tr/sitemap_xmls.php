<?php


return [
    "sitemap_xml_files"=>"Sitemap XML Dosyaları",
    "selected_sitemap_count"=>"Seçili Sayfa Yapısı Sayısı",
    "create_sitemap_xml"=>"Yeni Sitemap XML Dosyası Oluştur",
    "update_sitemap_xml"=>"Sitemap XML Dosyasını Düzenle"
];
