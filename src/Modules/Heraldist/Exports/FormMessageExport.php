<?php

namespace Mediapress\Modules\Heraldist\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Mediapress\Modules\MPCore\Models\LanguagePart;

class FormMessageExport implements FromArray
{

    private $form;

    /**
     * FormMessageExport constructor.
     */
    public function __construct($form)
    {
        $this->form = $form;
    }

    public function array(): array
    {
        $hold = [];
        $headingRow = [];

        $messages = $this->form->messages()->select('ip', 'data', 'created_at')->get();

        foreach ($this->form->getRenderAttribute()->source['fields'] as $value) {
            $headingRow[] = langPartAttr(strip_tags($value['config']['label']));
        }

        foreach ($messages as $message) {
            $temp = [];

            foreach ($message->data as $data) {

                $index = array_search(strip_tags($data['label']), $headingRow, 1);

                if($index !== false) {
                    if($data['type'] == 'file') {
                        $html = $data['value'];
                        preg_match('/href=["\']?([^"\'>]+)["\']?/', $html, $match);
                        $temp[$index] = $match[1];
                    } else {
                        $temp[$index] = $data['value'];
                    }
                }

            }
            $temp[] = $message->ip;
            $temp[] = $message->created_at;

            ksort($temp);
            $hold[] = $temp;
        }

        $headingRow[] = 'IP';
        $headingRow[] = 'Kayıt Tarihi';

        array_unshift($hold, $headingRow);

        return $hold;
    }
}
