<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\MPMailler;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Modules\Heraldist\Models\MailTemplate;

class PanelMailTemplateController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const WEBSITES = 'websites';
    public const MESSAGE_MAIL_TEMPLATES_CREATE = 'message.mail_templates.create';
    public const HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_NAME = "HeraldistPanel::mail_service.template_name";
    public const MAIL_TITLE = 'mail_title';
    public const MAIL_SUBJECT = 'mail_subject';
    public const NAME = 'name';
    public const MAIL_DETAIL = 'mail_detail';
    public const BODY = 'body';
    public const WEBSITE_ID = 'website_id';
    public const HERALDIST_MPMAILLER_MAIL_TEMPLATES_INDEX = 'Heraldist.mpmailler.mail_templates.index';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_TITLE = "HeraldistPanel::mail_service.template_title";
    public const HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_SUBJECT = "HeraldistPanel::mail_service.template_subject";
    public const HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_DETAIL = "HeraldistPanel::mail_service.template_detail";
    public const HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_CONTENT = "HeraldistPanel::mail_service.template_content";
    public const HERALDIST_PANEL_MAIL_SERVICE_WEBSITE = "HeraldistPanel::mail_service.website";
    public const MESSAGE = 'message';
    public const REQUIRED = 'required';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';

    public function index()
    {
        if(!userAction('message.mail_templates.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $website_id = session("panel.website")->id;
        $mailtemplates = MailTemplate::where('id','<>',0);

        if (request()->get('list') && request()->get('list')=="all") {
            $mailtemplates = $mailtemplates->paginate(25);
        }
        else {
            $mailtemplates=$mailtemplates->whereHas(self::WEBSITES, function($query) use ($website_id) {
                $query->where('id', $website_id);
            })->paginate(25);
        }

        return view('HeraldistPanel::mail_service.mpmailler.mail_templates.index', compact('mailtemplates'));
    }

    public function show($id)
    {
        $form = MailTemplate::find($id);
        return view('HeraldistPanel::mail_service.mpmailler.mail_templates.show', compact('form'));
    }

    public function create()
    {
        if(!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck(self::NAME, "id");
        return view('HeraldistPanel::mail_service.mpmailler.mail_templates.create', compact(self::WEBSITES));
    }

    public function edit($id)
    {
        if(!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $mailtemplate = MailTemplate::find($id);

        $domain = request()->getHost();
        $websites = Website::where("target", "internal")->where("status",1)->orderByRaw('FIELD(slug,"' . $domain . '") DESC')->get()->pluck(self::NAME, "id");

        return view('HeraldistPanel::mail_service.mpmailler.mail_templates.edit', compact('mailtemplate', self::WEBSITES));
    }

    public function store(Request $request)
    {
        if(!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE,true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $fields = [
            self::NAME => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_NAME),
            self::MAIL_TITLE => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_TITLE),
            self::MAIL_SUBJECT => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_SUBJECT),
            self::MAIL_DETAIL => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_DETAIL),
            self::BODY => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_CONTENT),
            self::WEBSITE_ID => trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE),
        ];

        $rules = [
            self::NAME => 'required|max:100|regex:/^[A-Za-z\şŞıİçÇöÖüÜĞğ\s]+$/',
            self::MAIL_TITLE => self::REQUIRED,
            self::MAIL_SUBJECT => self::REQUIRED,
            self::MAIL_DETAIL => self::REQUIRED,
            self::BODY => self::REQUIRED,
            self::WEBSITE_ID => self::REQUIRED,
        ];

        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_NAME)]),
            'mail_title.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_TITLE)]),
            'mail_subject.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_SUBJECT)]),
            'mail_detail.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_DETAIL)]),
            'body.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_CONTENT)]),
            'website_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $data = request()->except(['_token', self::WEBSITE_ID]);
        $insert = MailTemplate::firstOrCreate($data);

        // Log
        if ($insert){
            foreach ($request->website_id as $website) {
                $insert->websites()->attach([$website]);
            }
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        $fields = [
            self::NAME => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_NAME),
            self::MAIL_TITLE => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_TITLE),
            self::MAIL_SUBJECT => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_SUBJECT),
            self::MAIL_DETAIL => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_DETAIL),
            self::BODY => trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_CONTENT),
            self::WEBSITE_ID => trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE),
        ];

        $rules = [
            self::NAME => 'required|max:100|regex:/^[A-Za-z\şŞıİçÇöÖüÜĞğ\s]+$/',
            self::MAIL_TITLE => self::REQUIRED,
            self::MAIL_SUBJECT => self::REQUIRED,
            self::MAIL_DETAIL => self::REQUIRED,
            self::BODY => self::REQUIRED,
            self::WEBSITE_ID => self::REQUIRED,
        ];

        $messages = [
            'name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_NAME)]),
            'mail_title.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_TITLE)]),
            'mail_subject.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_SUBJECT)]),
            'mail_detail.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_DETAIL)]),
            'body.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_TEMPLATE_CONTENT)]),
            'website_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::HERALDIST_PANEL_MAIL_SERVICE_WEBSITE)]),
        ];

        $this->validate($request, $rules, $messages, $fields);

        $data = request()->except(['_token', self::WEBSITE_ID]);
        $update=MailTemplate::updateOrCreate(['id'=>$request->id],$data);

        // Log
        if ($update){
            // Pivottan sil
            $update->websites()->detach();
            foreach($request->website_id as $website){
                // Pivota ekle
                $update->websites()->attach([$website]);
            }
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(!userAction('message.mail_templates.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $template = MailTemplate::find($id);

        if ($template)
        {
            // Log
            $template->delete();
            // Pivottan sil
            $template->websites()->detach();

            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$template);
        }

        return redirect(route(self::HERALDIST_MPMAILLER_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}