<div class="modal-header">
    <h4 class="modal-title">
      {!! trans("MPCorePanel::module.slot.add") !!}
    </h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button>
</div>
<div class="modal-body">

    {{ Form::open(['route'=>'MPCore.technical.modules.slot.store']) }}

    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-md">
                <span class="tit">{!! trans("MPCorePanel::module.slot.name") !!}</span>
                {!!Form::text('slot', null, ["placeholder"=>trans("MPCorePanel::module.slot.name"), "class"=>"validate[required]"])!!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group col-md">
                <span class="tit">{!! trans("MPCorePanel::module.slot.description") !!}</span>
                {!!Form::text('description', null, ["placeholder"=>trans("MPCorePanel::module.slot.description"), "class"=>"validate[required]"])!!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="float-right">
                <button type="submit" class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
            </div>
        </div>
    </div>

    {{ Form::close() }}
</div>