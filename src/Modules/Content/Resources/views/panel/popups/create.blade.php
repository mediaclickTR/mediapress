@extends('ContentPanel::inc.module_main')

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content pop p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">{!! __("ContentPanel::popup.add_new_popup") !!}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.popup.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form center" style="width: 75% !important;">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('Content.popup.store') !!}" method="post">
                    {!! csrf_field() !!}
                    <div class="item itemBar">

                        <div class="form-group">
                            <div class="name">{!! __("ContentPanel::popup.order") !!}</div>
                            <input type="number" name="order" value="{!! old('order') !!}" style="width: 300px">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label class="active">
                                    {!! __("ContentPanel::general.status_title.active") !!}
                                    <input type="radio" name="status"
                                           value="1" {!! old('status', 1) == 1 ? 'checked' : '' !!}>
                                </label>
                                <label class="passive">
                                    {!! __("ContentPanel::general.status_title.passive") !!}
                                    <input type="radio" name="status"
                                           value="0" {!! old('status') === '0' ? 'checked' : '' !!}>
                                </label>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="name popupName">{!! __("ContentPanel::popup.name") !!}</div>
                            <input type="text" name="name" value="{!! old('name') !!}">
                        </div>


                        <div class="form-group">
                            <div class="name">{!! __("ContentPanel::popup.type") !!}</div>
                            <div class="checkbox radio">
                                <label>
                                    Lightbox ({!! __("ContentPanel::popup.type_direction.center") !!})
                                    <input type="radio" name="type"
                                           value="1" {!! old('type', 1) == 1 ? 'checked' : '' !!}>
                                    <img src="{!! asset("vendor/mediapress/images/lightbox.png") !!}" alt="">
                                </label>
                                <label>
                                    Tooltip ({!! __("ContentPanel::popup.type_direction.right_left") !!})
                                    <input type="radio" name="type"
                                           value="2" {!! old('type') == 2 ? 'checked' : '' !!}>
                                    <img src="{!! asset("vendor/mediapress/images/tooltip.png") !!}" alt="">
                                </label>
                                <label>
                                    Bar ({!! __("ContentPanel::popup.type_direction.bottom_up") !!})
                                    <input type="radio" name="type"
                                           value="3" {!! old('type') == 3 ? 'checked' : '' !!}>
                                    <img src="{!! asset("vendor/mediapress/images/bar.png") !!}" alt="">

                                </label>
                                <label>
                                    Screen ({!! __("ContentPanel::popup.type_direction.fullscreen") !!})
                                    <input type="radio" name="type"
                                           value="4" {!! old('type') == 4 ? 'checked' : '' !!}>
                                    <img src="{!! asset("vendor/mediapress/images/perde.png") !!}" alt="">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="placeBox d-none itemBar">
                        <div class="form-group">
                            <div class="name popupPlace">{{ __('ContentPanel::popup.place') }}</div>
                            <div class="checkbox radio placeBoxContent">
                            </div>
                        </div>
                    </div>

                    <div class="name">{{ __('ContentPanel::popup.settings') }}</div>
                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.screening_place') }}</h3>
                        <div class="content">

                            <div class="form-group">
                                <div class="name">{!! __("ContentPanel::popup.device.title") !!}</div>
                                <div class="checkbox">
                                    <label class="p-0">
                                        {!! __("ContentPanel::popup.device.desktop") !!}
                                        <input type="checkbox" name="devices[]"
                                               value="desktop" {!! in_array('desktop', old('devices', ['desktop'])) ? 'checked' : '' !!}>
                                    </label>
                                    <label class="p-0">
                                        {!! __("ContentPanel::popup.device.mobile") !!}
                                        <input type="checkbox" name="devices[]"
                                               value="mobile" {!! in_array('mobile', old('devices', [])) ? 'checked' : '' !!}>
                                    </label>
                                    <label class="p-0">
                                        {!! __("ContentPanel::popup.device.tablet") !!}
                                        <input type="checkbox" name="devices[]"
                                               value="tablet" {!! in_array('tablet', old('devices', [])) ? 'checked' : '' !!}>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group{!! old('show_page') == 3 ? 'input-hidden' : '' !!}">
                                <label for="website">{!! __("ContentPanel::popup.languages") !!}</label>
                                <select class="multiple langs" multiple name="languages[]">
                                    @foreach($countryGroups as $countryGroup)
                                        @foreach($countryGroup->languages as $language)
                                            <option value="{{ $countryGroup->id.'-'.$language->id }}"
                                                {!! in_array($countryGroup->id.'-'.$language->id, old('languages', [])) ? 'selected' : '' !!}>
                                                {!! $language->name . '(' . $countryGroup->title . ')' !!}
                                            </option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">

                                <div class="name">{{ __('ContentPanel::popup.screening_page.title') }}</div>
                                <div class="checkbox radio">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.screening_page.all') }}
                                        <input type="radio" name="show_page"
                                               {!! old('show_page', 0) == '0' ? 'checked' : '' !!} value="0">
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.screening_page.specific_page') }}
                                        <input type="radio" name="show_page"
                                               {!! old('show_page') == 1 ? 'checked' : '' !!} value="1">
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.screening_page.homepage') }}
                                        <input type="radio" name="show_page"
                                               {!! old('show_page') == 2 ? 'checked' : '' !!} value="2">
                                    </label>
                                    <label>
                                        {{ __('ContentPanel::popup.screening_page.custom_url') }}
                                        <input type="radio" name="show_page"
                                               {!! old('show_page') == 3 ? 'checked' : '' !!} value="3">
                                    </label>
                                </div>
                            </div>

                            <div class="form-group url-ids {!! old('show_page') == 1 ? '' : 'input-hidden' !!}">
                                <div class="name">{{ __('ContentPanel::popup.screening_page.specific_page_title') }}</div>
                                <select class="multiple urls" multiple name="url_ids[]">
                                </select>
                            </div>

                            <div class="form-group  custom-url-ids {!! old('show_page') == 3 ? '' : 'input-hidden' !!}">
                                <div class="name">{{ __('ContentPanel::popup.screening_page.custom_url_title') }}</div>
                                <select class="multiple custom-urls" multiple name="custom_url_ids[]" style="width: 100%">
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.limits.title') }}</h3>
                        <div class="content">
                            <div class="form-group">
                                <div class="name">{{ __('ContentPanel::popup.limits.how_much') }}</div>
                                <div class="checkbox radio">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.limits.continuous') }}
                                        <input type="radio" name="impressions" value="0"
                                               {!! old('impressions', 0) == '0' ? 'checked' : '' !!} checked>
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.limits.same_device') }}
                                        <input type="radio" name="impressions"
                                               value="1" {!! old('impressions') == 1 ? 'checked' : '' !!}>
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.limits.same_session') }}
                                        <input type="radio" name="impressions"
                                               value="2" {!! old('impressions') == 2 ? 'checked' : '' !!}>
                                    </label>
                                </div>
                                <div class="form-group mt-3 {!! old('impressions') != 0 ? '' : 'input-hidden' !!}">
                                    <label class="p-0">{{ __('ContentPanel::popup.limits.count_1') }} </label>
                                    <input type="text" name="impression_count" value="{!! old('impression_count', 1) !!}">
                                    <label class="p-0">{{ __('ContentPanel::popup.limits.count_2') }} </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.times.title') }}</h3>
                        <div class="content">
                            <div class="form-group">
                                <div class="name">{{ __('ContentPanel::popup.times.when') }}</div>
                                <div class="checkbox date">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.times.when_continuous') }}
                                        <input type="radio" name="display_date_type"
                                               value="0" {!! old('display_date_type', 0) == '0' ? 'checked' : '' !!}>
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.times.specific_dates') }}
                                        <input type="radio" name="display_date_type"
                                               value="1" {!! old('display_date_type') == 1 ? 'checked' : '' !!}>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group dates {!! old('display_date_type') == 1 ? '' : 'input-hidden' !!}">
                                <div class="datepicks col-6">
                                    <input type="text" class="datepick" name="start_date"
                                           value="{!! old('start_date') !!}"
                                           placeholder="{{ __('ContentPanel::popup.times.start_date') }}">
                                </div>
                                <div class="datepicks col-6">
                                    <input type="text" class="datepick" name="finish_date"
                                           value="{!! old('finish_date') !!}"
                                           placeholder="{{ __('ContentPanel::popup.times.finish_date') }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="name">{{ __('ContentPanel::popup.times.how_many_seconds') }}</div>
                                <div class="checkbox time">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.times.second_continuous') }}
                                        <input type="radio" name="display_time_type"
                                               value="0" {!! old('display_time_type', 0) == '0' ? 'checked' : '' !!}>
                                    </label>
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.times.specific_seconds') }}
                                        <input type="radio" name="display_time_type"
                                               value="1" {!! old('display_time_type') == 1 ? 'checked' : '' !!}>
                                    </label>
                                    <input type="text" placeholder="Saniye"
                                           class="{!! old('display_time_type', 0) == '0' ? 'input-hidden' : '' !!}"
                                           name="display_time"
                                           value="{!! old('display_time', 1) !!}">


                                    <label class="p-0 d-none" id="displayTimeTypeButton">
                                        {{ __('ContentPanel::popup.times.trigger_button') }}
                                        <input type="radio" name="display_time_type"
                                               value="3" {!! old('display_time_type') == '3' ? 'checked' : '' !!}>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.trigger.title') }}</h3>
                        <div class="content">
                            <div class="form-group">
                                <div class="name">{{ __('ContentPanel::popup.trigger.which_condition') }}</div>
                                <div class="checkbox">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.trigger.time') }}
                                        <input type="radio" name="conditions"
                                               value="0" {!! old('conditions', 0) == '0' ? 'checked' : '' !!}>
                                    </label>
                                </div>
                                <input type="text" placeholder="Saniye" name="condition_time"
                                       class="{!! old('conditions') == 1 ? 'input-hidden' : '' !!}"
                                       value="{!! old('condition_time', 1) !!}">

                                <div class="checkbox">
                                    <label class="p-0">
                                        {{ __('ContentPanel::popup.trigger.pixel') }}
                                        <input type="radio" name="conditions"
                                               value="1" {!! old('conditions') == 1 ? 'checked' : '' !!}>
                                    </label>
                                </div>
                                <input type="text" placeholder="Pixel" name="condition_pixel"
                                       class="{!! old('conditions', 0) == '0' ? 'input-hidden' : '' !!}"
                                       value="{!! old('condition_pixel', 100) !!}">
                            </div>
                        </div>
                    </div>
                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.content.title') }}</h3>
                        <div class="content">

                            <div class="form-group">
                                <div class="name popupNameStatus">{{ __('ContentPanel::popup.content.name_display') }}</div>
                                <div class="checkbox">
                                    <label class="active">
                                        {{ __('ContentPanel::general.yes') }}
                                        <input type="radio" name="name_display"
                                               value="1" {!! old('name_display', 1) == 1 ? 'checked' : '' !!}>
                                    </label>
                                    <label class="passive">
                                        {{ __('ContentPanel::general.no') }}
                                        <input type="radio" name="name_display"
                                               value="0" {!! old('name_display') == '0' ? 'checked' : '' !!}>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group d-none">
                                <div class="name popupContentType">{{ __('ContentPanel::popup.content.content_only_image') }}</div>
                                <div class="checkbox">
                                    <label class="active">
                                        {{ __('ContentPanel::general.yes') }}
                                        <input type="radio" name="content_only_image"
                                               value="1" {!! old('content_only_image') == 1 ? 'checked' : '' !!}>
                                    </label>
                                    <label class="passive">
                                        {{ __('ContentPanel::general.no') }}
                                        <input type="radio" name="content_only_image"
                                               value="0" {!! old('content_only_image', 0) == '0' ? 'checked' : '' !!}>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="name popupContent">{{ __('ContentPanel::popup.content.content') }}</div>
                                <textarea name="content" id="content">{!! old('content') !!}</textarea>
                            </div>

                            <div class="buttonBoxes {{ in_array(old('type'), [3,4]) ? '' : 'd-none' }}">
                                <div class="form-group btnType {{ old('type') == 3 ? '' : 'd-none' }}">
                                    <div class="name">{{ __('ContentPanel::popup.content.button_type') }}</div>
                                    <div class="checkbox">
                                        <label>
                                            {{ __('ContentPanel::popup.content.button_type_1') }}
                                            <input type="radio" name="button_type"
                                                   value="1" {!! old('button_type', 1) == 1 ? 'checked' : '' !!}>
                                        </label>
                                        <label>
                                            {{ __('ContentPanel::popup.content.button_type_2') }}
                                            <input type="radio" name="button_type"
                                                   value="2" {!! old('button_type') == 2 ? 'checked' : '' !!}>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group buttonTextBox {{ old('button_type') == 2 || old('type') == 4 ? '' : 'd-none' }}">
                                    <div class="name">{{ __('ContentPanel::popup.content.button_text') }}</div>
                                    <input type="text" name="button_text" value="{!! old('button_text') !!}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordionItem item itemBar">
                        <h3>{{ __('ContentPanel::popup.design.title') }}</h3>
                        <div class="content">

                            <div class="row">
                                @if(file_exists(public_path('assets/fonts')) && $fonts)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="name">{{ __('ContentPanel::popup.design.font') }}</div>
                                            <select name="font">
                                                @foreach($fonts as $font)
                                                    <option {{ old('font') == str_replace('.ttf', '', $font) ? 'selected' : '' }}
                                                            value="{{ str_replace('.ttf', '', $font) }}">
                                                        <div>{{ $font }}</div>
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-4 designBox type1 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.border_size') }}</div>
                                        <input type="number" name="border_size" min="0" value="{{ old('border_size', 5) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.border_color') }}</div>
                                        <input type="color" name="border_color" value="{{ old('border_color', '#000000') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.padding_size') }}</div>
                                        <input type="number" name="padding_size" min="0" value="{{ old('padding_size', 15) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 type2 type3 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.button_bg') }}</div>
                                        <input type="color" name="button_bg" value="{{ old('button_bg', '#000000') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 type2 type3 type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.button_color') }}</div>
                                        <input type="color" name="button_color" value="{{ old('button_color', '#000000') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type2 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.full_bg') }}</div>
                                        <select name="full_bg" class="w-100">
                                            <option {{ old('full_bg', 'active') == 'active' ? 'selected' : '' }} value="active">{{ __('ContentPanel::general.status_title.active') }}</option>
                                            <option {{ old('full_bg') == 'none' ? 'selected' : '' }} value="none">{{ __('ContentPanel::general.status_title.passive') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type2 type3 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.background') }}</div>
                                        <input type="color" name="background" value="{{ old('background', '#ffffff') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type2 type3 type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.color') }}</div>
                                        <input type="color" name="color" value="{{ old('color', "#000000") }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 type2 type3 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.a_color') }}</div>
                                        <input type="color" name="a_color" value="{{ old('a_color', "#007bff") }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.title_size') }}</div>
                                        <input type="number" name="title_size" min="0" value="{{ old('title_size', 20) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.title_color') }}</div>
                                        <input type="color" name="title_color" value="{{ old('title_color', '#ffffff') }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.title_animation') }}</div>
                                        <select name="title_animation" class="w-100">
                                            <option {{ old('title_animation', 'fadeIn') == 'fadeIn' ? 'selected' : '' }} value="fadeIn">{{ __('ContentPanel::popup.animations.fadeIn') }}</option>
                                            <option {{ old('title_animation') == 'fadeInLeft' ? 'selected' : '' }} value="fadeInLeft">{{ __('ContentPanel::popup.animations.fadeInLeft') }}</option>
                                            <option {{ old('title_animation') == 'fadeInRight' ? 'selected' : '' }} value="fadeInRight">{{ __('ContentPanel::popup.animations.fadeInRight') }}</option>
                                            <option {{ old('title_animation') == 'fadeInDown' ? 'selected' : '' }} value="fadeInDown">{{ __('ContentPanel::popup.animations.fadeInDown') }}</option>
                                            <option {{ old('title_animation') == 'fadeInUp' ? 'selected' : '' }} value="fadeInUp">{{ __('ContentPanel::popup.animations.fadeInUp') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.title_space') }}</div>
                                        <input type="number" name="title_space" min="0" value="{{ old('title_space', 10) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.content_font_size') }}</div>
                                        <input type="number" name="content_font_size" min="0" value="{{ old('content_font_size', 20) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.content_animation') }}</div>
                                        <select name="content_animation" class="w-100">
                                            <option {{ old('content_animation', 'fadeIn') == 'fadeIn' ? 'selected' : '' }} value="fadeIn">{{ __('ContentPanel::popup.animations.fadeIn') }}</option>
                                            <option {{ old('content_animation') == 'fadeInLeft' ? 'selected' : '' }} value="fadeInLeft">{{ __('ContentPanel::popup.animations.fadeInLeft') }}</option>
                                            <option {{ old('content_animation') == 'fadeInRight' ? 'selected' : '' }} value="fadeInRight">{{ __('ContentPanel::popup.animations.fadeInRight') }}</option>
                                            <option {{ old('content_animation') == 'fadeInDown' ? 'selected' : '' }} value="fadeInDown">{{ __('ContentPanel::popup.animations.fadeInDown') }}</option>
                                            <option {{ old('content_animation') == 'fadeInUp' ? 'selected' : '' }} value="fadeInUp">{{ __('ContentPanel::popup.animations.fadeInUp') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.btn_font_size') }}</div>
                                        <input type="number" name="btn_font_size" min="0" value="{{ old('btn_font_size', 12) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.btn_position') }}</div>
                                        <select name="btn_position" class="w-100">
                                            <option {{ old('btn_position', 'right') == 'right' ? 'selected' : '' }} value="right">{{ __('ContentPanel::popup.positions.right') }}</option>
                                            <option {{ old('btn_position') == 'left' ? 'selected' : '' }} value="left">{{ __('ContentPanel::popup.positions.left') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type4 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.counter_font_size') }}</div>
                                        <input type="number" name="counter_font_size" min="0" value="{{ old('counter_font_size', 12) }}">
                                    </div>
                                </div>
                                <div class="col-md-4 designBox type1 type2 d-none">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.border_radius') }}</div>
                                        <input type="number" name="border_radius" min="0" value="{{ old('border_radius', 0) }}">
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="name">{{ __('ContentPanel::popup.design.custom_css') }}</div>
                                        <textarea name="custom_css" style="height: 300px !important; overflow: auto !important;">{!! old('custom_css') !!}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="submit">
                        <button class="btn btn-primary">{{ __('ContentPanel::general.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endpush

@push("scripts")
    <style>
        .input-hidden {
            display: none !important;
        }
    </style>

    <script>
        showPlaceBox("{{ old('type', 1) }}");
        showDesignBoxes("{{ old('type', 1) }}");
        changeLabels("{{ old('type', 1) }}");

        $('[name="title_size"]').closest('.designBox').removeClass('d-none');

        $('[name="type"]').on('ifChecked', function () {
            var value = $(this).val();

            if(value == 3 || value == 4) {
                if(value == 4) {
                    $('.buttonBoxes > .btnType').addClass('d-none')
                    $('.buttonBoxes > .buttonTextBox').removeClass('d-none')
                } else {
                    $('.buttonBoxes > .btnType').removeClass('d-none')
                    $('.buttonBoxes > .buttonTextBox').addClass('d-none')
                }
                $('.buttonBoxes').removeClass('d-none');
                $('#displayTimeTypeButton').removeClass('d-none');
            } else {
                $('.buttonBoxes').addClass('d-none')
                $('#displayTimeTypeButton').addClass('d-none');
            }

            if(value == 2) {
                $('.popupContentType').closest('.form-group').removeClass('d-none');
            } else {
                $('.popupContentType').closest('.form-group').addClass('d-none');
            }

            changeLabels(value);
            showPlaceBox(value);
            showDesignBoxes(value);
        })

        $('[name="button_type"]').on('ifChecked', function () {
            if($(this).val() == 2) {
                $('.buttonTextBox').removeClass('d-none');
            } else {
                $('.buttonTextBox input').html('');
                $('.buttonTextBox').addClass('d-none');
            }
        })

        $('[name="name_display"]').on('ifChecked', function () {
            var value = $(this).val();

            if(value == 1) {
                $('[name="title_size"]').closest('.designBox').removeClass('d-none');
            } else {
                $('[name="title_size"]').closest('.designBox').addClass('d-none');
            }
        })

        function changeLabels(value) {
            if(value == 1) {
                $('.popupName').html("{{ __('ContentPanel::popup.name', ['name' => 'LightBox']) }}");
                $('.popupNameStatus').html("{{ __('ContentPanel::popup.content.name_display', ['name' => 'LightBox']) }}")
                $('.popupContent').html("{{ __('ContentPanel::popup.content.content', ['name' => 'LightBox']) }}")
            } else if(value == 2) {
                $('.popupName').html("{{ __('ContentPanel::popup.name', ['name' => 'Tooltip']) }}");
                $('.popupNameStatus').html("{{ __('ContentPanel::popup.content.name_display', ['name' => 'Tooltip']) }}")
                $('.popupContent').html("{{ __('ContentPanel::popup.content.content', ['name' => 'Tooltip']) }}")
                $('.popupPlace').html("{{ __('ContentPanel::popup.place', ['name' => 'Tooltip']) }}")
            } else if(value == 3) {
                $('.popupName').html("{{ __('ContentPanel::popup.name', ['name' => 'Bar']) }}");
                $('.popupNameStatus').html("{{ __('ContentPanel::popup.content.name_display', ['name' => 'Bar']) }}")
                $('.popupContent').html("{{ __('ContentPanel::popup.content.content', ['name' => 'Bar']) }}")
                $('.popupPlace').html("{{ __('ContentPanel::popup.place', ['name' => 'Bar']) }}")
            } else if(value == 4) {
                $('.popupName').html("{{ __('ContentPanel::popup.name', ['name' => 'Perde']) }}");
                $('.popupNameStatus').html("{{ __('ContentPanel::popup.content.name_display', ['name' => 'Perde']) }}")
                $('.popupContent').html("{{ __('ContentPanel::popup.content.content', ['name' => 'Perde']) }}")
                $('.popupPlace').html("{{ __('ContentPanel::popup.place', ['name' => 'Perde']) }}")
            }
        }

        function showPlaceBox(value) {
            $('.placeBox').addClass('d-none');

            if(value == 3) {
                var content = '' +
                    '<label>\n' +
                    '    {{ __('ContentPanel::popup.positions.bottom') }}\n' +
                    '    <input type="radio" name="place"\n' +
                    '           value="bottom" {!! old("place") == "bottom" ? "checked" : "" !!}>\n' +
                    '</label>\n' +
                    '<label>\n' +
                    '    {{ __('ContentPanel::popup.positions.up') }}\n' +
                    '    <input type="radio" name="place"\n' +
                    '           value="top" {!! old("place", 'top') == "top" ? "checked" : "" !!}>\n' +
                    '</label>';

                $('.placeBox .placeBoxContent').html(content);
                $('.placeBox').removeClass('d-none');

                $('.placeBox .placeBoxContent input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
            } else if(value == 2) {

                var content = '' +
                    '<label>\n' +
                    '    {{ __('ContentPanel::popup.positions.left_bottom') }}\n' +
                    '    <input type="radio" name="place"\n' +
                    '           value="left" {!! old("place", 'left') == "left" ? "checked" : "" !!}>\n' +
                    '</label>\n' +
                    '<label>\n' +
                    '    {{ __('ContentPanel::popup.positions.right_bottom') }}\n' +
                    '    <input type="radio" name="place"\n' +
                    '           value="right" {!! old("place") == "right" ? "checked" : "" !!}>\n' +
                    '</label>';

                $('.placeBox .placeBoxContent').html(content);
                $('.placeBox').removeClass('d-none');

                $('.placeBox .placeBoxContent input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
            }
        }

        function showDesignBoxes(val) {
            $('.designBox').addClass('d-none');

            $('.designBox.type' + val).removeClass('d-none');
        }
    </script>

    <script>
        $('[name="conditions"]').on('ifChecked', function () {
            if ($(this).val() == 1) {
                $('input[name="condition_pixel"]').removeClass('input-hidden');
                $('input[name="condition_time"]').addClass('input-hidden');
            } else {
                $('input[name="condition_pixel"]').addClass('input-hidden');
                $('input[name="condition_time"]').removeClass('input-hidden');
            }
        });

        $('[name="display_date_type"]').on('ifChecked', function () {
            if ($(this).val() == 1) {
                $('.dates').removeClass('input-hidden');
            } else {
                $('.dates').addClass('input-hidden');
            }
        });

        $('[name="display_time_type"]').on('ifChecked', function () {
            if ($(this).val() == 1) {
                $('input[name="display_time"]').removeClass('input-hidden');
            } else {
                $('input[name="display_time"]').addClass('input-hidden');
            }
        });

        $('[name="impressions"]').on('ifChecked', function () {
            if ($(this).val() == 2 || $(this).val() == 1) {
                $('input[name="impression_count"]').parent().removeClass('input-hidden');
            } else {
                $('input[name="impression_count"]').parent().addClass('input-hidden');
            }
        });

        $('[name="show_page"]').on('ifChecked', function () {
            if ($(this).val() == 1) {
                $('.url-ids').removeClass('input-hidden');
            } else {
                $('.url-ids').addClass('input-hidden');
            }

            if($(this).val() == 3) {
                $('.custom-url-ids').removeClass('input-hidden');
                $('.langs').closest('.form-group').addClass('input-hidden');
            } else {
                $('.custom-url-ids').addClass('input-hidden');
                $('.langs').closest('.form-group').removeClass('input-hidden');
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            var langs = [];
            $('.urls').selectator({
                searchFields: 'value text subtitle right',
                minSearchLength: 0,
                showAllOptionsOnFocus: true,
                load: function (search, callback) {
                    $.ajax({
                        url: '{{ route('Content.popup.search_url') }}',
                        data: {'search': search, langs: langs},
                        type: 'get',
                        dataType: 'json',
                        success: function (data) {
                            callback(data);
                        },
                        error: function () {
                            callback();
                        }
                    });
                },
            });

            $(".custom-urls").select2({
                tags: true,
            });
            $('.langs').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right',
            });

            $('.langs').on('change', function () {
                langs = $(this).val();
                $('.urls').selectator('refresh');
                $('.url-ids input.selectator_input').trigger('keyup');
            })


        });

        CKEDITOR.replace('content',
            {
                language: 'tr',
                //extraPlugins: 'bol,kopyala',allow_diskkeys%5B0%5D=azure&allow_diskkeys%5B1%5D=local&
                filebrowserImageBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Görsel',
                filebrowserBrowseUrl: '/mp-admin/FileManager?key=ckeditor&file_type=image&allow_actions%5B%5D=select&allow_actions%5B%5D=upload&allow_diskkeys%5B%5D=azure&allow_diskkeys%5B%5D=local&extensions=JPG%2CJPeG%2CPNG%2CGIF%2CSVG&min_width=&max_width=&min_height=&max_height=&min_filesize=&max_filesize=2048&max_file_count=1&additional_rules=&element_id=mf9114cd33c72&process_id=1589982859347&title=Dosya',
                allowedContent: {
                    script: true,
                    div: true,
                    $1: {
                        // This will set the default set of elements
                        elements: CKEDITOR.dtd,
                        attributes: true,
                        styles: true,
                        classes: true
                    }
                },
                entities: false,
                entities_latin: false,
                removeDialogTabs: null,//'image:advanced;link:advanced',
                pasteFromWordPromptCleanup: true,
                pasteFromWordRemoveFontStyles: true,
                forcePasteAsPlainText: true,
                ignoreEmptyParagraph: true,
                toolbar: [
                    {
                        name: 'clipboard',
                        groups: ['clipboard', 'undo'],
                        items: ['PasteFromWord', '-', 'Undo', 'Redo']
                    },
                    {
                        name: 'editing',
                        groups: ['find', 'selection', 'spellchecker'],
                        items: ['Find', 'Replace']
                    },
                    //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                    {
                        name: 'paragraph',
                        groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                    },
                    {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {
                        name: 'basicstyles',
                        groups: ['basicstyles', 'cleanup'],
                        items: ['Bold', 'Italic', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                    },
                    {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                    {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                ]
            });
        CKEDITOR.config.extraPlugins = 'justify,iframe,indentblock,indent,textindent';
    </script>
@endpush
