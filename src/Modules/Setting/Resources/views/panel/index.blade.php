@extends('SettingPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#login">
                            <div class="card-body">
                                <h5 class="card-title">{!! __('SettingPanel::login.modal_title') !!}</h5>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#imageQuality">
                            <div class="card-body">
                                <h5 class="card-title">{!! __('SettingPanel::image_quality.modal_title') !!}</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#breadcrumb">
                            <div class="card-body">
                                <h5 class="card-title">{!! __('SettingPanel::breadcrumb.modal_title') !!}</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#logo">
                            <div class="card-body">
                                <h5 class="card-title">{!! __('SettingPanel::logo.modal_title') !!}</h5>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#googleMap">
                            <div class="card-body">
                                <h5 class="card-title">{!! __('SettingPanel::google_map.modal_title') !!}</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("SettingPanel::modals.login")
    @include("SettingPanel::modals.breadcrumb", ['settings' => $breadcrumb_settings])
    @include("SettingPanel::modals.image_quality")
    @include("SettingPanel::modals.logo")
    @include("SettingPanel::modals.google_map")
@endsection

<style>
    .modal-body {
        position: relative;
    }

    .card {
        margin-right: 5% !important;
    }
</style>

@push('styles')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <!-- Image Quality Start -->
    <script>
        var quality = "{{ config('mediapress.image_quality') }}";
        var webp_status = "{{ config('mediapress.support_webp') }}";

        $(document).ready(function () {
            updateImage(quality);
        });

        function updateImage(val) {
            quality = val;

            $.ajax({
                url: "{{ route('Settings.imageAjax') }}",
                data: {'quality': val, '_token': "{{csrf_token()}}"},
                method: "post",
                cache: false,
                success: function (result) {
                    $('#image-test').attr('src', result.image);
                    $('#image-size').html(result.size);
                }
            })
        }

        function changeEnv() {

            var imageData = {
                '_token': "{{csrf_token()}}",
                'quality': quality,
                'webp_status': webp_status,
            };
            $.ajax({
                url: "{{ route('Settings.envAjax') }}",
                data: imageData,
                method: "post",
                cache: false,
                success: function () {
                    $('#imageQuality').modal('hide')
                }
            })
        }

        $('input[name="webp_status"]').change(function(){
            var $self = $(this);
            webp_status = $self.val();
            if ($self.prop('checked')) {
                $('input[name="webp_status"]').parent().removeClass('btn-primary');
                $self.parent().addClass('btn-primary');
            } else {
                $self.parent().removeClass('btn-default');
            }
        });
    </script>
    <!-- Image Quality Finish -->

    <script>
        function updateBreadcrumb() {

            var data = {
                'add_homepage': $('input[name="add_homepage"]:checked').val(),
                'add_domain': $('input[name="add_domain"]:checked').val(),
                'menu_slug': $('select[name="menu_slug"]').val(),
                'category_depth': $('input[name="category_depth"]').val(),
                'string_type': $('select[name="string_type"]').val(),
                '_token': "{{csrf_token()}}"
            };

            $.ajax({
                url: "{{ route('Settings.breadcrumb') }}",
                data: data,
                method: "post",
                cache: false,
                success: function (result) {
                    $('#breadcrumb').modal('hide')
                }
            })
        }
    </script>

    <script>
        function addLogo(el) {
            var id = $(el).attr('class');
            $('#' + id).click();

            $('#' + id).on('change', function() {
                if( $('#' + id)[0].files.length > 0 ){
                    $('#' + id).parent().find('i').show();
                } else {
                    $('#' + id).parent().find('i').hide();
                }
            });

        }

        function saveLogo() {
            $('#logo-form').submit();
        }
    </script>

    <script>
        function saveGoogleMap() {
            $('#googleMap-form').submit();
        }
    </script>

    <script>
        $(document).ready(function() {
            $('#whiteList').select2({
                tags: true,
                tokenSeparators: [',', ' '],
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term,
                        newTag: true // add additional parameters
                    }
                }
            });
        });
    </script>
@endpush
