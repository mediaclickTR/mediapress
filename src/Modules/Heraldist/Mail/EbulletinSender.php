<?php

namespace Mediapress\Modules\Heraldist\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mediapress\Modules\Heraldist\Models\Ebulletin;

class EbulletinSender extends Mailable
{
    use Queueable, SerializesModels;

    protected $ebulletin;
    protected $sender_email;
    protected $sender_name;
    public $subject;
    public $view;
    public $message;
    public $custom_url = 'e-bulletin';

    public function __construct(Ebulletin $ebulletin, $custom_url = null)
    {
        $this->ebulletin = $ebulletin;
        $this->subject = config('mediapress.ebulletin.mail_subject');
        $this->sender_email = config('mediapress.ebulletin.sender_email');
        $this->sender_name = config('mediapress.ebulletin.sender_name');
        $this->view = config('mediapress.ebulletin.view_page');
        $this->message = config('mediapress.ebulletin.mail_message');

        if (!is_null($custom_url)) {
            $this->custom_url = $custom_url;
        }
    }

    public function build()
    {
        if (config('mediapress.vue_url')) {
            $detailLink = config('mediapress.vue_url') . '/' . $this->custom_url . '/' . encrypt($this->ebulletin->id);
        } else {
            $detailLink = url($this->custom_url . '/' . encrypt($this->ebulletin->id));
        }

        $data = [
            'subject' => $this->subject,
            'sender_email' => $this->sender_email,
            'sender_name' => $this->sender_name,
            'message' => $this->message,
        ];

        return $this
            ->subject($this->subject)
            ->from($this->sender_email, $this->sender_name)
            ->view($this->view,
                compact('detailLink', 'data'));
    }
}
