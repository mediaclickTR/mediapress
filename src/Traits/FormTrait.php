<?php

namespace Mediapress\Traits;

use Mediapress\Exceptions\NotFoundException;
use Mediapress\Foundation\Mediapress;
use Mail;
use Mediapress\Models\Message;
use Auth;
trait FormTrait
{

    public function sendMail(Mediapress $mediapress,$fields,$formClass,$extras=[], $ignoredfields=[]){
        $list = [];
        $dataArray = $mediapress->request->all();
        foreach ($extras as  $key=>$value){
            $dataArray[$key] =$value;
        }
        $dataArray = array_diff_key($dataArray, array_flip($ignoredfields));
        unset($dataArray['g-recaptcha-response'],$dataArray['_token']);
        foreach ($dataArray as $key => $data) {
            $orjinalKey = $key;
            if(isset($fields[$key])){
                $key = $fields[$key];
            }
            if(is_array($data)){
                $list[$key] = implode(', ',$data);
            }elseif(is_file($data)){
                $file =$mediapress->request->file($orjinalKey);

              $file_name = uniqid().'.'.$file->extension();
                $file->storeAs('',$file_name,'uploads');
                $store = '';

                $list[$key] =   '<a href="'.url('uploads/'.$file_name).'">'.$key.'</a>';
            }else{
                $list[$key] = $data;
            }

        }


        if(session('utm')){
            $list = array_merge($list,session('utm'));
        }
        if(session('referer')){
            $list = array_merge($list,session('referer'));
        }
        if ($mediapress->request->server('HTTP_CF_CONNECTING_IP')) {
            $list['IP'] = $mediapress->request->server('HTTP_CF_CONNECTING_IP');
        }else{
            $list['IP'] = $mediapress->request->server('REMOTE_ADDR');
        }
        /*
        if(Auth::check()){
            $auth = Auth::user();
            $user = [
                'user_id'=> $auth->id,
                'user_name'=> $auth->name,
                'user_email'=> $auth->email,
            ];
            $list = array_merge($list,$user);
        }
        */
        unset($list['form_type']);
        unset($list['type']);
        $this->saveForm($mediapress,$list);
        Mail::to($mediapress->mail)->send(new $formClass($list,$mediapress->subject));
        if( count(Mail::failures()) ){
            return [false,implode(', ', Mail::failures())];
        }else{
            return [true, null];
        }
    }

    public function saveForm(Mediapress $mediapress,$list){
        $ip = $mediapress->request->ip();
        $mail = '';
        foreach ($list as $data) {
            if (!filter_var($data, FILTER_VALIDATE_EMAIL) === false) {
                $mail = $data;
            }
        }
        $message = new Message();
        $message->subject = $mediapress->subject;
        $message->mail = $mail;
        $message->ip = $ip;
        $message->data = $list;
        return $message->save();
    }

}