@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("HeraldistPanel::forms.forms") !!}</div>
            </div>
            <div class="float-right">
                @if(count($forms)>0)
                    <select id="multiple-processings" onchange="locationOnChange(this.value);">
                        <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                        <option  {!! requestHasAndEqual("list","all") ? "selected" : '' !!} value="{!! route("Heraldist.forms.index", array_merge($queries, ["list"=>"all"])) !!}">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
                    </select>
                    @if(!empty($queries))
                        <a href="{!! route("Heraldist.forms.index") !!}" class="filter-block">
                            <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                        </a>
                    @endif
                @endif
                    <a class="btn btn-primary btn-sm" href="{!! route('Heraldist.forms.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
        </div>
            <div class="p-30">
                <div class="table-field">
                    @if(count($forms) == 0)
                        <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                    @else
                        <table>
                            <thead>
                            <tr>
                                <th>{!! trans("MPCorePanel::general.name") !!}</th>
                                <th>{!! trans("MPCorePanel::general.slug") !!}</th>
                                <th>{!! trans("MPCorePanel::general.website") !!}</th>
                                <th>{!! trans("MPCorePanel::general.created_at") !!}</th>
                                <th>{!! trans("HeraldistPanel::forms.inbox") !!}</th>
                                <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                            </tr>
                            </thead>
                            @foreach($forms as $form)
                                <tr>
                                    <td>{!! $form->name !!}</td>
                                    <td>{!! $form->slug !!}</td>
                                    <td>
                                        @if($form->websites())
                                            @foreach($form->websites as $site)
                                                {!! $site->slug !!}
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{!! $form->created_at !!}</td>
                                    <td>
                                        <a href="{!! route("Heraldist.message.show", $form->id) !!}" class="showMessages">
                                            <i class="fa fa-envelope" ></i> {!! trans("HeraldistPanel::forms.inbox") !!} ({!! $form->messages->count() !!})
                                        </a>
                                    </td>
                                    <td>
                                        <select class="nice" onchange="locationOnChange(this.value);">
                                            <option value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                            <option value="{{ route("Heraldist.message.show", $form->id) }}">{!! trans("HeraldistPanel::forms.inbox") !!} ({!! $form->messages->count() !!})</option>
                                            <option value="{!! route('Heraldist.forms.edit',$form->id) !!}">{!! trans("MPCorePanel::general.edit") !!}</option>
                                            <option value="{{ route('Heraldist.forms.details',$form->id ) }}">{!! trans("HeraldistPanel::forms.form.inputs") !!}</option>
                                            <option value="{!! route('Heraldist.forms.delete',$form->id ) !!}">{!! trans("MPCorePanel::general.delete") !!}</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="float-left">
                            {!! $forms->links() !!}
                        </div>
                        <div class="float-right">
                            <select onchange="locationOnChange(this.value);">
                                <option value="#">{!! trans('MPCorePanel::general.select.list.size') !!}</option>
                                <option  {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("Heraldist.forms.index", array_merge($queries, ["list_size"=>"15"])) !!}">15</option>
                                <option  {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("Heraldist.forms.index", array_merge($queries, ["list_size"=>"25"])) !!}">25</option>
                                <option  {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("Heraldist.forms.index", array_merge($queries, ["list_size"=>"50"])) !!}">50</option>
                                <option  {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("Heraldist.forms.index", array_merge($queries, ["list_size"=>"100"])) !!}">100</option>
                            </select>
                        </div>
                    @endif
                </div>
            </div>
        </div>
@endsection

@push("styles")
    <style>
        td .fa {
            display:inline !important;
            text-align: left;
            margin: 0px;
        }
        .showMessages{
            color:#2196f3 !important;
        }
    </style>
@endpush
