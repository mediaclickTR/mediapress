<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    public const LANGUAGES = 'languages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(self::LANGUAGES)){
            return;
        }

        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create(self::LANGUAGES, function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code', 15);
            $table->string('name', 190);
            $table->string('alphabet', 190)->nullable();
            $table->string('native', 190);
            $table->string('regional', 190)->nullable();
            $table->string('flag', 190)->nullable();
            $table->string('fontColor', 190)->nullable();
            $table->string('hexColor', 190)->nullable();
            $table->enum('direction', ['ltr','rtl']);
            $table->boolean('active');
            $table->boolean('default');
            $table->unsignedInteger('sort');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::LANGUAGES);
    }
}
