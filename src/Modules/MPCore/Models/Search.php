<?php

namespace Mediapress\Modules\MPCore\Models;

use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\Meta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class Search extends Model
{
    use CacheQueryBuilder;


    protected $table = 'search_content';

    public function url()
    {
        return $this->hasOne(Url::class, 'id', 'url_id');
    }
    public function model()
    {
        return $this->morphTo();
    }


}
