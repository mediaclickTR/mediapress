@php echo '<'.'?x'.'ml version="1.0" encoding="UTF-8"?>'; @endphp
<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84">
    <!-- created with Mediapress Sitemap Generator www.mediaclick.com.tr -->
    @foreach($files as $file)
        <sitemap>
            <loc>{!! url(str_replace('&','&amp;',$file)) !!}</loc>
        </sitemap>
    @endforeach
</sitemapindex>
