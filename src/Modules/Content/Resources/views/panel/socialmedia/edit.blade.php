@extends('ContentPanel::inc.module_main')

@php
    $icon="[" .  $socialmedia->icon . "]";
@endphp

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
@endpush

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{{ __('ContentPanel::social_media.add_social_media') }}</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.socialmedia.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>

        <div class="p-30 mt-4">
            <div class="form center">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('Content.socialmedia.update',$socialmedia->id) !!}" method="post">
                    {!! csrf_field() !!}

                    <div class="form-group ">
                        <label for="website">{{ __('ContentPanel::social_media.select_group') }}</label>
                        <select name="group_id">
                            @for($i=1;$i<=20;$i++)
                                <option @if($socialmedia->group_id==$i) selected @endif value="{!! $i  !!}">
                                    {{ __('ContentPanel::social_media.group') }} {!! $i !!}
                                </option>
                            @endfor
                        </select>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="website">{{ __('ContentPanel::social_media.name') }}</label>
                        <select name="name" id="website">
                            <option @if($socialmedia->name=="facebook") selected @endif value="facebook">Facebook</option>
                            <option @if($socialmedia->name=="twitter") selected @endif value="twitter">Twitter</option>
                            <option @if($socialmedia->name=="instagram") selected @endif value="instagram">Instagram</option>
                            <option @if($socialmedia->name=="youtube") selected @endif value="youtube">YouTube</option>
                            <option @if($socialmedia->name=="whatsapp") selected @endif value="whatsapp">Whatsapp</option>
                            <option @if($socialmedia->name=="linkedin") selected @endif value="linkedin">Linkedin</option>
                            <option @if($socialmedia->name=="tripadvisor") selected @endif value="tripadvisor">Tripadvisor</option>
                            <option @if($socialmedia->name=="blogger") selected @endif value="blogger">Blogspot</option>
                            <option @if($socialmedia->name=="flickr") selected @endif value="flickr">Flickr</option>
                            <option @if($socialmedia->name=="tiktok") selected @endif value="tiktok">Tiktok</option>
                            <option @if($socialmedia->name=="pinterest") selected @endif value="pinterest">Pinterest</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">{{ __('ContentPanel::general.order') }}</label>
                        <input name="order" type="number" style="width: 100px;"
                               value="{!! old('order',$socialmedia->order) !!}">
                    </div>
                    <div class="form-group mt-3">
                        <label for="image-radio">{{ __('ContentPanel::general.status') }}</label>
                        <div class="checkbox">
                            <label for="image-radio">
                                <input name="status" id="image-radio" type="radio"
                                       value="1" {!! 1 == $socialmedia->status ? 'checked' : '' !!}>{{ __('ContentPanel::general.status_title.active') }}
                            </label>
                            <label for="video-radio">
                                <input name="status" id="video-radio" type="radio"
                                       value="0" {!! 0 == $socialmedia->status ? 'checked' : '' !!}>{{ __('ContentPanel::general.status_title.passive') }}
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="name">{{ __('ContentPanel::social_media.icon') }}</label>
                        <div class="col-4 mt-5">
                            <?php
                            $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}', 1);
                            $options['key'] = 'social_media';
                            ?>
                            <div class="file-box file-manager"
                                 id="social_media"
                                 data-options='{!! json_encode($options) !!}'>
                                <input name="icon"
                                       id="file-manager-input" type="hidden"
                                       value="[{!! trim($icon, '[]') !!}]">

                                <a href="javascript:void(0);"
                                   onclick="javascript:addImage($(this))">

                                    <span class="file-icon"></span>
                                    <i class="file-manager-rule">{{ __('ContentPanel::general.max_file_size', ['size' => 5]) }}</i>
                                </a>
                            </div>
                        </div>
                    </div>

                    @foreach($country_groups as $country_group)

                        @if(count($country_groups)>1)

                            <h3 class="title mb-0" style="font-size:21px;">{{ __('ContentPanel::social_media.target_area') }}: {{ $country_group["title"] }}
                                ({{ $country_group["code"] }}) </h3>
                        @endif

                        <ul class="nav nav-tabs col-12">
                            @foreach($languages as $key=>$language)

                                <li><a {!! $key == 0 ? 'class="show active"' :'' !!} data-toggle="tab"
                                       href="#{!! $country_group->code.'_'.$language->code !!}"><img
                                                src="{!! asset('vendor/mediapress/images/flags/'.$language->flag) !!}"
                                                height="13" alt=""> {!! strtoupper($language->code) !!}</a></li>
                            @endforeach

                        </ul>
                        <div class="tab-content">
                            @foreach($languages as $key=>$language)
                                <div id="{!! $country_group->code.'_'.$language->code !!}"
                                     class="tab-pane fade in  {!! $key == 0 ? 'show active' :'' !!}">

                                    <div class="form-group">
                                        <label for="name">{{ __('ContentPanel::social_media.link') }}</label>
                                        <input name="{!! $country_group->id !!}[{!! $language->id !!}][link]" type="text"
                                               value="{!! $socialmedia->link[$country_group['id']][$language->id]['link'] ?? '' !!}">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach

                    <div class="submit">
                        <button class="btn btn-primary">{{ __('ContentPanel::general.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dropify.css') !!}">
    <style>
        .slidepage .images img {
            max-height: 183px;
            border-radius: 4px;
        }

        .post-file.has-file {
            border: none;
        }

        hr {
            margin-top: -30px;
            margin-bottom: 30px;
            clear: left;

        }

        .right-area {
            float: right;
            position: absolute;
            right: -120px;
            top: -60px;
        }

        .slidepage .right-area input {
            width: 70px !important;
            float: left !important;
        }

        .slidepage .right-area label {
            float: left !important;
            margin: 6px 3px 6px 20px !important;
        }

        .slidepage .option {

            float: left;
            width: 100%;
            position: relative;
            padding: 0 13px;
            min-height: 25px;

        }
    </style>
    <style>
        .file-manager {
            border: 2px solid #E5E5E5;
            height: 181px;
            width: 230px;
        }

        .file-manager-image {
            height: 100%;
        }
        .file-manager-image img {
            height: 100%;
        }
        i.file-manager-rule {
            position: absolute;
            left: 0;
            bottom: 48px;
            text-align: center;
            display: block;
            width: 100%;
            font: 12px "Poppins", sans-serif;
            color: #393939;
        }

        ul.file-manager-options {
            position: initial !important;
        }
        ul.file-manager-options li {
            width: 100% !important;
            padding: 0 10px;
            float: none;
        }
        ul.file-manager-options li u {
            text-align: left !important;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $('.item .cont a').click(function () {
            var copy = $(this).parent().parent().parent().find('.col-4:nth-child(1) textarea,input').val();
            $(this).parent().find('textarea,input').val(copy);
        });
    </script>

    @if(function_exists('customFilemanager'))
        {!! customFilemanager() !!}
    @endif
@endpush
