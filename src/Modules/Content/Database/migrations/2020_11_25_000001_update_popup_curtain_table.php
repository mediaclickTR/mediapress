<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePopupCurtainTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('popups', function (Blueprint $table) {
            $table->string('title_color')->nullable()->after('border_radius');
            $table->string('title_animation')->nullable()->after('title_color');
            $table->string('title_space')->nullable()->after('title_animation');
            $table->string('content_font_size')->nullable()->after('title_space');
            $table->string('content_animation')->nullable()->after('content_font_size');
            $table->string('btn_font_size')->nullable()->after('content_animation');
            $table->string('btn_position')->nullable()->after('btn_font_size');
            $table->string('counter_font_size')->nullable()->after('btn_position');
        });

    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->table('popups', function (Blueprint $table) {
            $table->dropColumn('title_color');
            $table->dropColumn('title_animation');
            $table->dropColumn('title_space');
            $table->dropColumn('content_font_size');
            $table->dropColumn('content_animation');
            $table->dropColumn('btn_font_size');
            $table->dropColumn('btn_position');
            $table->dropColumn('counter_font_size');
        });
    }
}
