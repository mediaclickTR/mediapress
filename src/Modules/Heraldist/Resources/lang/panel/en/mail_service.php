<?php

return [
    /*
     * MPMailler
     */
    'website' => 'Website',
    'using_queue' => 'Use Queuing? (Drying is useful for sending large amounts of email)',
    'quick_create' => 'Quick Create',
    'list_manage' => 'Manage E-Mail List',
    'template_manage' => 'Manage E-Mail Template',
    'emailsender' => 'E-Mail Sender',
    /*
     * Maillists
     */
    'maillists' => 'E-Mail Lists',
    'create_maillist' => 'Create E-Mail List',
    'edit_maillist' => 'Edit E-Mail List',
    'maillist_name' => 'E-Mail List Name',
    'email_select' => 'Select E-Mail',
    'sendedemails' => 'Sent E-mails',
    'mailsend' => 'Send E-Mail',
    'messages' => 'Messages',
    'email_sender' => 'Messages',
    'e_bulletin' => 'E-Bulletin',
    'emailsender_name' => 'E-Mail Sender Name',
    /*
     * Emailtemplates
     */
    'email_template_select' => 'Select E-Mail Template',
    'emailtemplates' => 'E-Mail Templates',
    'create_template' => 'Create E-Mail Template',
    'edit_template' => 'Edit E-Mail Template',
    'template_name' => 'Template Name',
    'template_title' => 'Template Title',
    'template_subject' => 'Template Subject',
    'template_detail' => 'Template Detail',
    'template_content' => 'Template Content',
    'template_create_info' => 'Username (% username%), Title (% mail_title%), Subject (% mail_subject%) and Detail (% mail_detail%), you can send personalized e-mails by writing the variables in the brackets in the appropriate places',
    /*
     * EmailSender
     */
    'newsend_email' => 'Send New E-Mail',
    'select_maillist' => 'Select E-Mail List',
    'select_template' => 'Select Template',
    'create_quicklist' => 'Create Quick Email List',
    'create_quicktemplate' => 'Create Quick Template',
    'email_detail' => 'E-Mail Detail',
    'sended_emails' => 'Addresses Sent',

    ## Mailchimp ##

    /*
    * Maillists
    */
    'mailchimp_maillists' => 'Mailchimp Email Lists',
    'mailchimp_listid' => 'List ID',
    'sender_email' => 'Sender E-Mail',
    'sender_name' => 'Sender Name',

    /*
     * Maillist > Members
     */
    'email_selection' => 'Select E-Mail',
    'create_user' => 'Create Member',
    'edit_user' => 'Edit Member',
    'lists_user' => 'List of Members',
    'back_maillist' => 'Back to the e-mail list',
    /*
     * Mailtemplates
     */
    'mailchimp_emailtemplates' => 'Mailchimp Email Templates',
    'template_image' => 'Template Image',
    'previewin_mailchimp' => 'View on Mailchimp Site',
    'mailchimp_template_info' => 'Mailchimp API does not support editing email templates. So you have to delete and save again to make changes to the template.',
    /*
     * Campaigns
     */
    'campaigns' => 'Campaigns',
    'create_campaign' => 'Create Campaign',
    'edit_campaign' => 'Edit Campaign',
    'campaign_name' => 'Campaign Name',
    'multiple_emailsend' => 'Send Status',
    'template_id' => 'Template ID',
    'sended_email' => 'Sent',
    'email_maillist' => 'E-Mail List',
    'email_template' => 'E-Mail Template',

    /*
     * Errors
     */

    'please_select_email' => 'Please Select E-mail',
    'not_found_email_template' => "No such email template was found.",
    'not_found_email_list' => "No such e-mail mailing list was found.",
    'email_template_empty_fields' => "There are blank spaces in your template selections. Please fill in and try again",
];
