@extends('EntityPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("EntityPanel::entitylist.edit-entitylist") !!}</div>
            </div>
        </div>
        <div class="p-30 mt-4">
            @include("EntityPanel::entitylist.form")
        </div>
    </div>
@endsection
