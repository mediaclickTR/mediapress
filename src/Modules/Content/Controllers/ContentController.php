<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Foundation\BackupData;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\CriteriaPage;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\SitemapDetailExtra;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\AllBuilder\Foundation\FormStorer;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\Content\Facades\Auth;
use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;

use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\Modules\MPCore\Models\Url;
use mysql_xdevapi\Exception;
use Yajra\DataTables\Html\Builder;
use Html;
use DataTables;


class ContentController //extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const SITEMAP = "sitemap";
    public const LANGUAGE_ID = 'language_id';
    public const COUNTRY_GROUP_ID = 'country_group_id';
    public const SITEMAP_ID = "sitemap_id";
    public const PAGE_ID = 'page_id';
    public const WEBSITE_ID = "website_id";
    public const WEBSITE = "website";

    /**
     * @return mixed
     */
    public function index($sitemap_id, Builder $builder)
    {
        if (!userAction('message.pages.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        return "under construction";
    }


    public function create($sitemap_id)
    {
        if (!userAction('message.pages.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        DB::beginTransaction();
        try {
            $preCreate = Page::preCreate([self::SITEMAP_ID => $sitemap_id]);
            $sitemap_details = Sitemap::find($sitemap_id)->details;

            foreach ($sitemap_details as $detail) {
                $detail_data = [
                    self::PAGE_ID => $preCreate->id,
                    self::LANGUAGE_ID => $detail->language_id,
                    self::COUNTRY_GROUP_ID => $detail->country_group_id,
                ];
                PageDetail::insert($detail_data);
            }

            DB::commit();

            return redirect(route("Content.pages.edit", [self::SITEMAP_ID => $sitemap_id, 'id' => $preCreate->id]));

        } catch (\Exception $e) {
            DB::rollback();
            $trace = $e->getTrace();
            foreach ($trace as $trace_) {
                if (isset($trace_["file"]) && $trace_["file"] == __FILE__) {
                    dump("Hata " . __FILE__ . " dosyasında " . __LINE__ . ". satırda tetiklendi.");
                    break;
                }
            }
            dump("Page | PageDetail oluşturulurken hata oluştu. Rollback yapıldı.");
            dump("Exception " . __FILE__ . " dosyasında " . (__LINE__ + 1) . ". satırda fırlatıldı.");
            throw $e;
        }

    }


    public function edit($sitemap_id, $id, Request $request)
    {
        if (!userAction('message.pages.edit', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $page = Page::whereId($id)->whereSitemapId($sitemap_id)->first();

        if (!$page) {
            throw new \Exception("Page kaydı bulunamadı:$id");
        }

        $params = $request->all();

        $renderable_class = Content::getPageRenderableClassPath($page);

        $renderable = new $renderable_class(["page" => &$page, "request_params" => $params]);

        return view('ContentPanel::pages.edit', compact('page', 'id', self::SITEMAP_ID, 'renderable'));

    }


    public function update($sitemap_id, $id)
    {

        $page = Page::with([self::SITEMAP, "sitemap.sitemapType"])->whereId($id)->first();
        if (!$page) {
            throw new \InvalidArgumentException("Page kaydı bulunamadı:$id");
        }
        $request = Request::capture();


        $renderable_class = Content::getPageRenderableClassPath($page);
        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class(["page" => &$page]);


        $storer_class = Content::getPageStorerClassPath($page);
        /** @var FormStorer $storer */
        $storer = new $storer_class($renderable, $request);


        $store = $storer->store();

        if (!$store) {
            return redirect()->back()->withErrors($storer->getErrors());
        }

        return redirect()->back()->with('message', trans('MPCorePanel::general.success_message'));


    }


    public function delete($sitemap_id, $id)
    {
        if (!userAction('message.pages.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $page = Page::find($id);
        if ($page) {
            $page->delete();
            UserActionLog::delete(__CLASS__ . "@" . __FUNCTION__, $page);
        }
        return redirect(route('Content.pages.index', $sitemap_id))->with('message', trans('MPCorePanel::general.success_message'));
    }

    public function getPageCriteriaValues(Request $request)
    {
        $page_ids = $request->get("page_ids", []);
        $data = [];
        foreach ($page_ids as $page_id) {
            $data[$page_id] = CriteriaPage::where(self::PAGE_ID, $page_id)->get()->pluck('criteria_id')->toArray();
        }
        return $data;
    }

    public function getPagePropertyValues(Request $request)
    {
        $page_ids = $request->get("page_ids", []);
        $data = [];
        foreach ($page_ids as $page_id) {
            $data[$page_id] = \DB::table('page_property')->where(self::PAGE_ID, $page_id)->get()->pluck('value', 'property_id')->toArray();
        }
        return $data;
    }


    public function variationStatus(Page $page)
    {
        return response()->json(VariationTemplates::variationStatus($page));
    }

    public function checkPageDetailUrl($page_id, Request $request)
    {

        $result = [
            "request_params" => $request->all(),
            "status" => false,
            "info" => [
                "url_head" => null,
                "applicable_slug" => null,
                "possible_url" => null,
                "current_url_model" => null,
            ],
            "applicable_slug" => null, //again
            "messages" => [],
            "warnings" => [],
            "errors" => []
        ];

        try {
            // pick parameters from request
            $cg_id = $request->get(self::COUNTRY_GROUP_ID) * 1;
            $lng_id = $request->get(self::LANGUAGE_ID) * 1;
            $text2slug = $request->get("text2slug") ?? null;
            $slug2slug = $request->get("slug2slug") ?? null;

            // get page object with details
            $page = Page::whereId($page_id * 1)->with(["details", "sitemap.details"])->first();
            $page_detail = $page->details->where(self::COUNTRY_GROUP_ID, $cg_id)->where(self::LANGUAGE_ID, $lng_id)->first();
            // set info
            $result["info"]["current_url_model"] = $page_detail->url;


            if (is_null($page)) {
                throw new \InvalidArgumentException(trans("ContentPanel::page.page_not_found_by_id", ["id" => $page_id]));
            }

            //we'll use its url:
            $sitemap_detail = $page->sitemap->details->where(self::COUNTRY_GROUP_ID, $cg_id)->where(self::LANGUAGE_ID, "$lng_id")->first();

            if (is_null($sitemap_detail)) {
                throw new \InvalidArgumentException(trans("ContentPanel::sitemap.detail_not_found_by_id", ["id" => $page_id]));
            }

            //url which we'll take url string to build one for page detail
            $url = $sitemap_detail->url;
            //set info
            $result["info"]["url_head"] = $url->url;

            //nothing sent as a slug? really? this is a page folks, not detail of homepage sitemap.
            if (is_null($text2slug) && is_null($slug2slug)) {
                throw new \InvalidArgumentException(trans("MPCorePanel::general.status_messages.at_least_one_should_be_provided", ["mass" => "text2slug, slug2slug"]));
            }

            //text2slug has higher priority.
            //sluggified state of this will be page detail's slug:
            $slugify_this = $text2slug ?? $slug2slug;

            $slug = \Str::slug(Str::ascii($slugify_this));

            $url_full = rtrim($url->url, "/") . "/" . $slug;

            //check if one already exists
            // so on...
            // TODO: Complete code here.


        } catch (\Exception $e) {
            $result["status"] = false;
            $result["errors"][] = $e->getMessage() . " (file:\"" . $e->getFile() . "\" line:" . $e->getLine() . ")";
        } finally {
            return response()->json($result);
        }

    }

    public function getMetaVariablesFor(Request $request)
    {
        $model_type = $request->model_type;
        $model_id = $request->model_id;

        $values_to_override = $request->has("valuesToOverride") ? $request->values_to_override : [];

        return response()->json(Content::getMetaVariablesFor([$model_type, $model_id], $values_to_override));

    }


    public function deleteDetail(Request $request)
    {

        $model = $request->detail_type::find($request->detail_id);

        $delete = $model->delete();
        if (!$delete) {
            return [false, $model];
        }
        return [true, $model];
    }

    public function restoreDetail(Request $request)
    {

        $model = $request->detail_type::withTrashed()->find($request->detail_id);
        $do = $model->restore();
        if (!$do) {
            return [false, null];
        }
        return [true, $request->detail_type::find($request->detail_id)->with("url")];
    }


    #region Helpers

    private function getValidUrlString($url_string, $num = null)
    {
        if (!is_null($num)) {
            //$num = ($num+0)++;
        }
        $check = Url::where("url", $url_string)->first();
    }

    public function checkUrl(Request $request)
    {
        $website_id = ($request->get(self::WEBSITE_ID) ?? session("panel.website.id")) * 1;
        $detail_type = $request->get("detail_type");

        $cls_rel_data = getSimpleRelationData($detail_type, "Detail");
        $parent_type = $cls_rel_data["parent_class"];

        $detail_id = $request->get("detail_id") * 1;
        $parent_id = $request->get("parent_id") * 1;

        $language_id = $request->get(self::LANGUAGE_ID) * 1;
        $country_group_id = $request->has(self::COUNTRY_GROUP_ID) ? $request->get(self::COUNTRY_GROUP_ID) : null;

        $slug = $request->get("sluggable_text");


        $country_group_model = CountryGroup::find($country_group_id);
        $detail_model = $detail_type::where($cls_rel_data["childs_foreign_key"], $parent_id)->where(self::LANGUAGE_ID, $language_id)->where(self::COUNTRY_GROUP_ID, $country_group_model ? $country_group_model->id : null)->first();
        $parent_model = $parent_type::find($parent_id);
        if ($detail_model == null) {
            if (is_a($parent_model, Sitemap::class)) {
                $detail_data = [
                    "type" => $detail_type,
                    "page" => $parent_model,
                    self::SITEMAP => $parent_model,
                    self::WEBSITE => $parent_model->websites->first()
                ];
                if ($website_id == 0) {
                    $website_id =
                        isset($detail_data[self::WEBSITE])
                            ? $detail_data[self::WEBSITE]->id
                            : $parent_model
                            ->websites()
                            ->first()
                            ->id;
                }
            } else {
                $detail_data = [
                    "type" => $detail_type,
                    "page" => $parent_model,
                    self::SITEMAP => $parent_model->sitemap,
                    self::WEBSITE => $parent_model->sitemap->websites->first()
                ];
                if ($website_id == 0) {
                    $website_id = isset($detail_data[self::WEBSITE]) ? $detail_data[self::WEBSITE]->id : $parent_model->sitemap->websites()->first()->id;
                }
            }
        }

        //return dd(($detail_model ?? $detail_data), $country_group_model, $language_id, $slug);
        //throw new \Exception("yannıışşşşş agaaaa");

        $pattern = URLEngine::generateUrlString(($detail_model ?? $detail_data), $country_group_model, $language_id, $slug);
        $checked = URLEngine::checkPattern($website_id, $pattern["pattern"], $pattern["slug"], $detail_model);

        return response()->json($checked);
    }


    private function createUrl($websiteIds = [], $language, $slug, $model, $country = null)
    {
        $url = "/$language";
        if ($country) {
            $url .= "/$country";
        }
        $url .= "/$slug";
        foreach ($websiteIds as $wid) {
            $url = $this->checkUrlRecursive($wid, $url);
            Url::create([self::WEBSITE_ID => $wid, "url" => $url, "model_type" => get_class($model), "model_id" => $model->id]);
        }
    }

    private function checkUrlRecursive($websiteId, $language = null, $country = null, $url = null, $i = 1)
    {
        $pattern = '';
        $website = Website::find($websiteId);
        $website_default_lang = $website->defaultLanguage();

        if ($website->use_deflng_code == 0) {
            if ($website_default_lang->code == "$language" && $country == null) {
                $pattern = '';
            } else {
                if ($country == null) {
                    $pattern = $language;
                } else {
                    $pattern = "/" . $language . "_" . $country;
                }
            }
        } else {
            if ($country == null) {
                $pattern = $language;
            } else {
                $pattern = "/" . $language . "_" . $country;
            }
        }

        if ($url == "null") {
            return $pattern;
        }
        $query = $i > 1 ? $pattern . $url . "-$i" : $pattern . $url;

        $urlModel = Url::where(['url' => $query, self::WEBSITE_ID => $websiteId])->first();

        if ($urlModel != null) {
            return $this->checkUrlRecursive($websiteId, $language, $country, $url, ++$i);
        } else {
            if ($i > 1) {
                return $pattern . $url . "-$i";
            } else {
                return $pattern . $url;
            }
        }
    }

    #endregion
}
