@extends('MPCorePanel::inc.app')
@section('content')
@include('MPCorePanel::inc.breadcrumb')
<div class="page-content">
    @include("MPCorePanel::inc.errors")
    <div class="col-md-12 pa">
        <div class="float-left">
            <div class="title">{!! trans("MPCorePanel::analytics.google_settings") !!}</div>
        </div>
        <div class="float-right">
            <a href="{!! route("Interaction.analytics.google.googleAnalyticsDoc") !!}" class="btn btn-primary" target="_blank">
                {!! trans("MPCorePanel::analytics.google_analytics_tutorial") !!}
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="form add-slide">
            <div class="add-remove">
                <div class="asd">
                    <form action="{!! route('Interaction.analytics.google.googleAnalyticsSettingPost') !!}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group rel focus">
                            <label for="">analytics_view_id:</label>
                            <input type="text"  name="analytics_view_id" value="@if(isset($data)){!! $data['analytics_view_id'] !!}@endif">
                            <code>Örn : 179348244</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">type:</label>
                            <input type="text"  name="type" value="@if(isset($data)){!! $data['type'] !!}@endif">
                            <code>Örn : service_account</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">project_id:</label>
                            <input type="text"  name="project_id" value="@if(isset($data)){!! $data['project_id'] !!}@endif">
                            <code>Örn : analytics-test-212013</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">private_key_id:</label>
                            <input type="text"  name="private_key_id" value="@if(isset($data)){!! $data['private_key_id'] !!}@endif">
                            <code>Örn : 3ff297f6a237b6b029b8c43b1931a03a5d2c7008</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">private_key:</label>
                            <textarea name="private_key">@if(isset($data)){!! $data['private_key'] !!}@endif</textarea>
                            <code>Örn : -----BEGIN PRIVATE KEY----- MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCqrjppyxfZAcOf...</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">client_email:</label>
                            <input type="text"  name="client_email" value="@if(isset($data)){!! $data['client_email'] !!}@endif">
                            <code>Örn : test-229@analytics-test-212013.iam.gserviceaccount.com</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">client_id:</label>
                            <input type="text"  name="client_id" value="@if(isset($data)){!! $data['client_id'] !!}@endif">
                            <code>Örn : 102656248746873466383</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">auth_uri:</label>
                            <input type="text" name="auth_uri" value="@if(isset($data)){!! $data['auth_uri'] !!}@endif">
                            <code>Örn : https://accounts.google.com/o/oauth2/auth</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">token_uri:</label>
                            <input type="text"  name="token_uri" value="@if(isset($data)){!! $data['token_uri'] !!} @endif">
                            <code>Örn : https://oauth2.googleapis.com/token </code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">auth_provider_x509_cert_url:</label>
                            <input type="text"  name="auth_provider_x509_cert_url" value="@if(isset($data)){!! $data['auth_provider_x509_cert_url'] !!}@endif">
                            <code>Örn : https://www.googleapis.com/oauth2/v1/certs</code>
                        </div>
                        <div class="form-group rel focus">
                            <label for="">client_x509_cert_url:</label>
                            <input type="text"  name="client_x509_cert_url" value="@if(isset($data)){!! $data['client_x509_cert_url'] !!}@endif">
                            <code>Örn : https://www.googleapis.com/robot/v1/metadata/x509/test-229%40analytics-test-212013.iam.gserviceaccount.com</code>
                        </div>
                        <div class="form-group rel focus">
                            <div class="pull-right"><br/>
                                <button class="btn btn-success">{!! trans("MPCorePanel::general.save") !!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 </div>
@endsection

@push("styles")
    <style>
        code {
            font-size:12px !important;
            color:#d45e5e!important;
        }
    </style>
@endpush
