@stack("styles")
<link rel="stylesheet" href="{{asset("/vendor/mediapress/css/cropper.min.css")}}"/>
<div class="modal-header">
    <h4 class="modal-title">{!! $title ?? null !!}</h4>
    @include('MPCorePanel::inc.backup', ['object' => $renderable->getParam('category')])

    @if(config('app.env') == 'local')
        @php
            $temp = str_replace(["App\\", "\\"], ["", "/"], $renderable->class) . '.php';
        @endphp
        <div class="float-right">
            <a href="{!! route('admin.editors.edit',encrypt(app_path($temp)) ) !!}" target="_blank" class="btn btn-secondary btn-sm">Renderable</a>
        </div>
    @endif
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button>
</div>
<div class="modal-body">
    {!! $renderable->render() !!}
</div>
@stack("scripts")


<!-- Save Button Start -->
<script>

    $(document).ready(function() {

        updateSaveButton($('input[name$="status"]:checked'));
        $('li.next > a[id^="anchor-"]').html('{{ trans('MPCorePanel::general.next') }} <i class="fa-angle-double-right fa"> </i>');
        $('li.previous > a[id^="anchor-"]').html('<i class="fa-angle-left fa"></i> {{ trans('MPCorePanel::general.previous') }}');

        $(document).delegate('input[name$="status"]', 'ifChecked', function () {
            updateSaveButton($(this));
        })
    })

    function updateSaveButton(el) {
        var value = el.val();
        var button = el.closest('div.form').find("button.btn-step-tabs-submit");

        button.removeClass("btn-warning btn-success btn-danger btn-secondary");
        if(value == 2) { //Draft
            button.html("{{ __('ContentPanel::general.save_as.draft') }}");
            button.addClass('btn-warning')
        } else if(value == 1) { //Active
            button.html("{{ __('ContentPanel::general.save_as.active') }}");
            button.addClass('btn-success')
        } else if(value == 0) { //Passive
            button.html("{{ __('ContentPanel::general.save_as.passive') }}");
            button.addClass('btn-danger')
        } else if(value == 5) { //Postdate
            button.html("{{ __('ContentPanel::general.save_as.postdate') }}");
            button.addClass('btn-secondary')
        }
    }

</script>
<!-- Save Button Finish -->

@if(function_exists('filemanager'))
    @php
        if($renderable->getParam('category')) {
            $languages = array_map('intval', $renderable->getParam('category')->details->pluck('language_id')->toArray());
            $model_id = $renderable->getParam('category')->id;
            $model_type = urlencode(get_class($renderable->getParam('category')));
        }
    @endphp

    {!! filemanager($model_id, $model_type, $languages) !!}
@endif
