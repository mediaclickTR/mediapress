<?php

namespace Mediapress\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Mediapress\Models\Module;
use Mediapress\Modules\MPCore\Models\Country;

class Update extends Command
{

    protected $signature = 'mp:update
                            {--m|module : Seed new modules}
                            {--c|country : Update Country Names}
                            {--d|debugbar : Fixed Debugbar}
                            {--file=* : Update words in the given file type [controller, renderable]}';


    public function handle()
    {
        Artisan::call('migrate', ['--force' => true]);

        $this->info(Artisan::output());

        if(is_dir(public_path('vendor'))) {
            $this->delTree(public_path('vendor'));
            $this->info(public_path('vendor') . " Deleted !!" . PHP_EOL);
        }

        if(file_exists(base_path('config/mediapress.php'))) {
            @unlink(base_path('config/mediapress.php'));
            $this->info(base_path('config/mediapress.php') . " Deleted !!" . PHP_EOL);
        }

        $this->info('**It may take longer to publish, wait a few seconds.' . PHP_EOL);
        Artisan::call('vendor:publish', ['--all' => true]);
        $this->info(Artisan::output());


        if($this->option('file')) {
            Artisan::call('mp:replace', ['--file' => $this->option('file')]);
            $this->info(Artisan::output());
        }


        if($this->option('module')) {
            $this->moduleSeeder();
            $this->info("Module seeder completed!!" . PHP_EOL);
        }


        if($this->option('country')) {
            $this->updateCountries();
            $this->info("Country Table Updated !!". PHP_EOL);
        }

        if($this->option('debugbar')) {
            Artisan::call('mp:debugbar');
            $this->info("Debugbar Fixed !!". PHP_EOL);
        }

        Artisan::call('mp:search');
        $this->info("Search Table Setted !!". PHP_EOL);

        Artisan::call('view:clear');
        $this->info("View Cleared !!". PHP_EOL);

        Artisan::call('cache:clear');
        $this->info("Cache Cleared !!". PHP_EOL);

        $this->info("------------------" . PHP_EOL);
        $this->info(" Update Completed " . PHP_EOL);
        $this->info("------------------" . PHP_EOL);
    }

    private function updateCountries() {
        Country::find(45)->update(['tr' => "Cook Adaları"]);
        Country::find(108)->update(['tr' => "İran", 'en' => "Iran"]);
        Country::find(191)->update(['tr' => "Rusya", 'en' => 'Russia', 'native' => 'Russia']);
        Country::find(216)->update(['tr' => "Tayland"]);
        Country::find(2)->update(['tr' => "Birleşik Arap Emirlikleri"]);
        Country::find(238)->update(['tr' => "Virjinya Adaları", 'en' => "Virgin Islands", 'native' => 'Virgin Islands']);
        Country::find(29)->update(['tr' => "Brunei"]);
        Country::find(124)->update(['tr' => "Cayman Adaları"]);
        Country::find(80)->update(['tr' => "Fransız Guyanası"]);
        Country::find(60)->update(['tr' => "Dominika"]);
        Country::find(156)->update(['tr' => "Malavi", 'en' => "Malawi", 'native' => "Malawi"]);
        Country::find(146)->update(['tr' => "Myanmar"]);
        Country::find(241)->update(['tr' => "Vallis ve Futuna"]);
        Country::updateOrcreate([
            'tr' => "Kosova",
            'en' => "Kosovo",
            'native' => 'Kosovo',
            'code' => 'XK'
        ]);
    }

    public function moduleSeeder() {
        DB::table('modules')->truncate();

        $modules = [];
        foreach (File::directories(base_path('vendor/mediapress/mediapress/src/Modules')) as $dic) {
            $dic = basename($dic);
            $modules[] = ["name" => $dic, "namespace" => "Mediapress\\Modules\\" . $dic, "status" => 1, "slot_id" => null];
        }

        if(file_exists(base_path('app/WebModules'))) {
            foreach (File::directories(base_path('app/WebModules')) as $dic) {
                $dic = basename($dic);
                $modules[] = ["name" => $dic, "namespace" => "App\\WebModules\\" . $dic, "status" => 1, "slot_id" => null];
            }
        }

        DB::table('modules')->insert($modules);


        Module::updateOrCreate([
            'name' => "FileManager",
            'namespace' => "Mediapress\\FileManager",
            'status' => 1,
        ]);
        Module::updateOrCreate([
            'name' => "Tools",
            'namespace' => "Mediapress\\Tools",
            'status' => 1,
        ]);

        if(class_exists('Mediapress\\ECommerce\\ECommerce')){
            Module::updateOrCreate([
                'name' => "ECommerce",
                'namespace' => "Mediapress\\ECommerce",
                'status' => 1,
            ]);
        }

        if(Route::has('Survey.index')) {
            Module::updateOrCreate([
                'name' => "Survey",
                'namespace' => "Mediapress\\Survey",
                'status' => 1,
            ]);
        }

    }

    public function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            if($file == 'storage') continue;
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
    }
}
