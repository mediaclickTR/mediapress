<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteDetailExtra extends Model {

    protected $table = 'website_detail_extras';

    public $timestamps=false;

    protected $fillable = ["website_detail_id","key", "value"];

    public function websiteDetail()
    {
        return $this->belongsTo(WebsiteDetail::class);
    }
}
