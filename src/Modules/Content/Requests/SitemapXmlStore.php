<?php

    namespace Mediapress\Modules\Content\Requests;

    use Illuminate\Contracts\Validation\Validator;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\ValidationException;

    class SitemapXmlStore extends FormRequest
    {
        public const BOOLEAN = "boolean";
        public const ARRAY1 = "array";
        public const CHANGE_FREQ_OPTIONS = "in:hourly,daily,weekly,monthly,yearly,never";

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                "title" => "required|max:191|min:4",
                "filename" => "required|unique:sitemap_xmls,filename,".$this->request->get("id")."|max:30|regex:/^[a-z0-9-]*$/",
                "renew_freq" => "in:daily,weekly,monthly,yearly",

                "include_detail" => self::ARRAY1,
                "include_categories" => self::ARRAY1,
                "include_pages" => self::ARRAY1,

                "include_detail.*" => self::BOOLEAN,
                "include_categories.*" => self::BOOLEAN,
                "include_pages.*" => self::BOOLEAN,

                "detail_priority" => self::ARRAY1,
                "categories_priority" => self::ARRAY1,
                "pages_priority" => self::ARRAY1,
                
                "detail_change_frequency" => self::ARRAY1,
                "category_change_frequency" => self::ARRAY1,
                "pages_change_frequency" => self::ARRAY1,
                
                "detail_change_frequency.*" => self::CHANGE_FREQ_OPTIONS,
                "category_change_frequency.*" => self::CHANGE_FREQ_OPTIONS,
                "pages_change_frequency.*" => self::CHANGE_FREQ_OPTIONS,

                "detail_priority.*" => "numeric|min:0.1|max:1.0|required_if:include_detail,1",
                "categories_priority.*" => "numeric|min:0.1|max:1.0|required_if:include_categories,1",
                "pages_priority.*" => "numeric|min:0.1|max:1.0|required_if:include_pages,1"
            ];
        }

        public function attributes()
        {
            return [
                "title" => "Başlık",
                "filename" => "Dosya Adı",
                "renew_freq" => "Güncelleme Sıklığı",

                "include_detail" => "Kök Dizin / Sayfa - Dahil Et",
                "include_categories" => "Kategoriler - Dahil Et",
                "include_pages" => "Sayfalar - Dahil Et",

                "include_detail.*" => "Kök Dizin / Sayfa - Dahil Et",
                "include_categories.*" => "Kategoriler - Dahil Et",
                "include_pages.*" => "Sayfalar - Dahil Et",

                "detail_priority" => "Kök Dizin / Sayfa - Öncelik",
                "categories_priority" => "Kategoriler - Öncelik",
                "pages_priority" => "Sayfalar - Öncelik",

                "detail_priority.*" => "Kök Dizin / Sayfa - Öncelik",
                "categories_priority.*" => "Kategoriler - Öncelik",
                "pages_priority.*" => "Sayfalar - Öncelik",
    
                "detail_change_frequency" => "Kök Dizin / Sayfa - Değişim Sıklığı",
                "category_change_frequency" => "Kategoriler - Değişim Sıklığı",
                "pages_change_frequency" => "Sayfalar - Değişim Sıklığı",
                
                "detail_change_frequency.*" => "Kök Dizin / Sayfa - Değişim Sıklığı",
                "category_change_frequency.*" => "Kategoriler - Değişim Sıklığı",
                "pages_change_frequency.*" => "Sayfalar - Değişim Sıklığı",
            ];
        }


    }
