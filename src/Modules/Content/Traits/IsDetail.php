<?php

namespace Mediapress\Modules\Content\Traits;

trait IsDetail{
    
    
    public function scopeOrderedByLanguage($query, $direction="ASC")
    {
        return $query->orderBy("languages.sort",$direction);
    }
}
