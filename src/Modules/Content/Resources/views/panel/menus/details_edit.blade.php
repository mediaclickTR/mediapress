@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! __("ContentPanel::menu.edit_menu") !!}</div>
            </div>
        </div>
        <div class="p-30 mt-4 ">
            <div class="row">
                <div class="col-md-12 hasFileUploader pt-5">
                    {{ Form::model($menu, ['route' => ['Content.menus.update.details',$menu->id], 'method' => 'POST']) }}
                    @csrf
                    {!!Form::hidden('menu_id', $menu_id)!!}
                    {!!Form::hidden('language_id', $menu->language_id)!!}
                    {!!Form::hidden('country_group_id', $menu->country_group_id)!!}

                    <div class="row">
                        <div class="col-4">
                            <?php
                            $options = json_decode('{"key":"","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,WEBP","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"2048","max_file_count":"1","additional_rules":""}', 1);
                            $options['key'] = $menu->language_id . "_menu";
                            ?>
                            <div class="file-box file-manager" id="{!! $menu->language_id !!}_menu"
                                 data-options='{!! json_encode($options) !!}'>
                                <input name="file_id" type="hidden"
                                       value="[{!! $menu->file_id !!}]">
                                <a href="javascript:void(0);"
                                   onclick="javascript:addImage($(this))">
                                    <span class="file-icon"></span>
                                    <i class="mx">Max dosya boyutu 2 MB</i>
                                </a>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group focus">
                                        <div class="tit">{!! __("ContentPanel::menu.name") !!}</div>
                                        {!!Form::text('name', null, ["class"=>"validate[required]"])!!}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel" id="types">
                                        <div class="tit">{!! __("ContentPanel::menu.link_types") !!}</div>
                                        {!!Form::select('type', $form_data['types'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => 'nice'])!!}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel" id="inlink" style="display:none;">
                                        <div class="tit">{!! __("ContentPanel::menu.inlink") !!}</div>
                                        <select name="url_id" class="selectpicker"
                                                data-width="100%"
                                                data-size="8"
                                                data-live-search="true">
                                            <option value="">{{ __("ContentPanel::general.selection") }}</option>
                                            @foreach($urls as $url)
                                                <option value="{{ $url->id }}" @if($menu->url) {!! $url->url == $menu->url->url ? "selected" : "" !!} @endif >{!! $url->url !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel" id="outlink" style="display:none;">
                                        <div class="tit">{!! __("ContentPanel::menu.outlink") !!}</div>
                                        {!!Form::text('out_link', null, ["class"=>"validate[required]"])!!}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel" id="">
                                        <div class="tit">{!! __("ContentPanel::menu.view") !!}</div>
                                        {!!Form::select('type_view', $form_data['view'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => 'nice'])!!}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel" id="targets">
                                        <div class="tit">{!! __("ContentPanel::menu.targets") !!}</div>
                                        {!!Form::select('target', $form_data['targets'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => 'nice'])!!}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group rel">
                                        <div class="tit">{!! __("ContentPanel::menu.status") !!}</div>
                                        {!!Form::select('status', $form_data['status'],null, ['placeholder' => __("ContentPanel::general.selection"),'class' => 'nice'])!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="float-right">
                        <button class="btn btn-primary">{!! __("ContentPanel::general.save") !!}</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push("styles")
    <link rel="stylesheet" href="{!! asset('/vendor/mediapress/css/bootstrap-select.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dropify.css') !!}">
    <style>
        select {
            width: 100% !important;
        }

        .bootstrap-select .btn {
            -webkit-appearance: none;
            background: url("/vendor/mediapress/images/path.png") right 10px center #fff no-repeat;
            color: #3c4858 !important;
            border: 1px solid #d2d2d2;
            margin: 0;
            line-height: inherit;border-radius: 4px;height: auto;font-size:14px;
        }

        .bootstrap-select .btn:after {
            content: none;
        }

        .bootstrap-select .dropdown-menu {
            width: 100% !important;

        }

        .bootstrap-select .bs-searchbox > input {
            height: 30px;
            padding: 12px 12px;
            margin: 8px 0;
            display: inline-block;
        }
    </style>

    <style>
        .hasFileUploader .images img {
            max-height: 183px;
            border-radius: 4px;
        }

        .post-file.has-file {
            border: none;
        }

        hr {
            margin-top: -30px;
            margin-bottom: 30px;
            clear: left;

        }
        .right-area{
            float:right;
            position: absolute;
            right: -120px;
            top: -60px;
        }

        .hasFileUploader .right-area input{
            width: 70px !important;
            float: left !important;
        }
        .hasFileUploader .right-area label{
            float: left !important;
            margin: 6px 3px 6px 20px !important;
        }
    </style>

    <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
    <style>
        .file-manager {
            border: 2px solid #E5E5E5;
            height: 181px;
            width: 230px;
        }

        .file-manager-image {
            height: 100%;
        }
        .file-manager-image img {
            height: 100%;
            max-height: 100% !important;
        }
        i.file-manager-rule {
            position: absolute;
            left: 0;
            bottom: 48px;
            text-align: center;
            display: block;
            width: 100%;
            font: 12px "Poppins", sans-serif;
            color: #393939;
        }

        ul.file-manager-options {
            position: initial !important;
        }
        ul.file-manager-options li {
            width: 100% !important;
            padding: 0 10px;
            float: none;
        }
        ul.file-manager-options li u {
            text-align: left !important;
        }
    </style>
@endpush

@push('scripts')
    <script src="{!! asset("/vendor/mediapress/js/bootstrap-select.min.js") !!}"></script>
    <script>
        $(document).ready(function (){

        $("select[name='type']").bind('change',function(){

                if($(this).val()==0) {
                    $("#inlink").show();
                    $("#outlink").hide();
                }
                else if($(this).val()==1){
                    $("#outlink").show();
                    $("#inlink").hide();
                }
                else if($(this).val()==2) {
                    $("#outlink").hide();
                    $("#inlink").hide();
                    $("#targets").hide();
                }
                else {
                    $("#outlink").hide();
                    $("#inlink").hide();
                }
            });
            $("select[name='type']").trigger('change');

            $.each($('.post-file'), function (i, div) {
                handleFileManagerSelection($(div).attr('id'),JSON.parse($(div).find('input[type="hidden"]').val()));
            });
        });
    </script>

@endpush

@if(function_exists('customFilemanager'))
    @push('scripts')
        {!! customFilemanager() !!}
    @endpush
@endif
