<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionsTable extends Migration {

	public function up()
	{
        if (Schema::connection('mediapress_interaction')->hasTable('sessions'))
            return;

		Schema::connection('mediapress_interaction')->create('sessions', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cookie_id')->index('cookie_id');
            $table->text('hash');
            $table->timestamp('created_at')->index('created_at')->nullable();
		});
	}

	public function down()
	{
        Schema::connection('mediapress_interaction')->dropIfExists('sessions');
	}
}
