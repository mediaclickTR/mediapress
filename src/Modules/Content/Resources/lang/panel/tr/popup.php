<?php

return [
    "list_title" => "Popup Listesi",
    "add_new_popup" => "Popup Ekle",
    "type" => "Tipi",
    "languages" => "Diller",
    "copy" => "Kopyala",

    'order' => 'Sıralama',
    'name' => ":name Adı",

    'type_direction' => [
        'center' => 'Merkez',
        'right_left' => 'Sağ/Sol',
        'bottom_up' => 'Alt/Üst Uzun Bar',
        'fullscreen' => 'Tam Ekran',
    ],

    'place' => ':name Yeri',

    'settings' => 'Ayarlar',

    'screening_place' => 'Gösterim Yeri',

    'device' => [
        'title' => 'Cihaz',
        "desktop" => "Masaüstü",
        "mobile" => "Mobil",
        "tablet" => "Tablet",
    ],

    'screening_page' => [
        'title' => 'Gösterilecek Sayfa',
        'all' => 'Tüm Sayfalar',
        "specific_page" => "Belirli Sayfalar",
        "specific_page_title" => "Gösterilecek Sayfalar",
        "homepage" => "Ana Sayfa",
        "custom_url" => "Özel Url",
        "custom_url_title" => "Sayfa Url'leri",
    ],

    'limits' => [
        'title' => 'Limitler',
        'how_much' => 'Aynı kişi kaç kez görecek?',
        'continuous' => 'Sürekli',
        'same_device' => 'Aynı Cihaz',
        'same_session' => 'Aynı Oturum',
        'count_1' => 'Aynı oturumda',
        'count_2' => 'kez gösterilecek.'
    ],

    'times' => [
        'title' => 'Süre Ayarla',
        'when' => 'Ne Zaman?',
        'when_continuous' => 'Sürekli',
        'specific_dates' => 'Belli tarihlerde',
        'start_date' => 'Başlangıç Tarihi',
        'finish_date' => 'Bitiş Tarihi',
        'how_many_seconds' => 'Kaç saniye görünsün?',
        'second_continuous' => 'Sürekli Görünsün',
        'specific_seconds' => 'Belirli Süre Görünsün (saniye)',
        'trigger_button' => 'Butonu Tetikleyene Kadar',
    ],

    'trigger' => [
        'title' => 'Tetikleyiciler',
        'which_condition' => 'Hangi şartlarda gösterilecek?',
        'time' => 'Süre (saniye)',
        'pixel' => 'Scroll (pixel)',
    ],

    'content' => [
        'title' => 'İçerik',
        'name_display' => ':name Adı Gözüksün mü?',
        'content_only_image' => 'Tooltip İçerik Görsel mi?',
        'content' => ':name İçerik',
        'button_type' => 'Buton Tipi',
        'button_type_1' => 'Kapatma Butonu',
        'button_type_2' => 'Onay Butonu',
        'button_text' => 'Buton Yazısı',
    ],


    'design' => [
        'title' => 'Tasarım',
        'font' => 'Font',
        'border_size' => 'Border Genişliği',
        'border_color' => 'Border Rengi',
        'padding_size' => 'Padding Değeri',
        'button_bg' => 'Buton Arka Plan Rengi',
        'button_color' => 'Buton Rengi',
        'full_bg' => 'Arka Plan Karartı Durumu',
        'background' => 'Kutunun Arkaplan Rengi',
        'color' => 'Yazının Rengi',
        'a_color' => 'Link Rengi',
        'title_size' => 'Başlık Font Büyüklüğü',
        'title_color' => 'Başlık Yazı Rengi',
        'title_animation' => 'Başlık Animasyonu',
        'title_space' => 'Başlık Boşluğu',
        'content_font_size' => 'Başlık Yazı Boşluğu',
        'content_animation' => 'İçerik Animasyonu',
        'btn_font_size' => 'İlerle Butonu Yazı Boyutu',
        'btn_position' => 'İlerle Butonu Pozisyonu',
        'counter_font_size' => 'Sayaç Font Boyutu',
        'border_radius' => 'Köşe Keskinliği Ölçüsü',
        'custom_css' => 'Özel Stiller',
    ],

    'animations' => [
        'fadeIn' => 'Ortadan Gelsin',
        'fadeInLeft' => 'Soldan Gelsin',
        'fadeInRight' => 'Sağdan Gelsin',
        'fadeInDown' => 'Aşağıdan Gelsin',
        'fadeInUp' => 'Üstten Gelsin',
    ],

    'positions' => [
        'bottom' => 'Alt',
        'up' => 'Üst',
        'left' => 'Sol',
        'right' => 'Sağ',
        'left_bottom' => 'Sol Alt',
        'right_bottom' => 'Sağ Alt',
    ],
];
