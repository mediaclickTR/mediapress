<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelMenusTable extends Migration {

    public const PANEL_MENUS = 'panel_menus';

    public function up()
    {

        if (!Schema::hasTable(self::PANEL_MENUS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::PANEL_MENUS, function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('website_id')->nullable()->unsigned();
                $table->string('name')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::PANEL_MENUS);
        Schema::enableForeignKeyConstraints();
    }
}
