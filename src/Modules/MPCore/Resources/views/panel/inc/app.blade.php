@php
    $public_url = "vendor/mediapress/";
@endphp
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="robots" content="noindex, nofollow, noodp, noydir"/>
    <title>{!! ((isset($page->title)) ? $page->title :'MediaPress v4.0') !!}</title>
    <meta name="description" content="TR" />
    <meta name="keywords" content="TR" />
    <link rel="canonical" href="TR" />
    <link rel="alternate" href="TR" hreflang="TR">
    <meta name="author" content="TR">
    <meta name="robots" content="noindex,nofollow, nosnippet, noarchive" />

    <link rel="shortcut icon" href="{!! asset($public_url.'images/favicon.png') !!}"/>

    <link rel="stylesheet" href="{!! asset($public_url.'css/custom.css?v=1') !!}" />
    <link rel="stylesheet" href="{!! asset($public_url.'css/mediapress.css?v=4') !!}" />
    <link rel="stylesheet" href="{!! asset($public_url.'css/jquery-ui.css') !!}" />
    <link rel="stylesheet" href="{!! asset($public_url.'css/animate.css') !!}" />
    <link rel="stylesheet" href="{!! asset($public_url.'libs/fontawesome/css/all.min.css') !!}">
    <link rel="stylesheet" href="{!! asset($public_url.'css/bootstrap.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset($public_url.'css/global.css') !!}" />

    <link rel="stylesheet" href="{!! asset($public_url.'css/filemanager.css?v=2') !!}" />

    <link rel="stylesheet" href="{!! asset($public_url.'css/sweetalert2.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset($public_url.'css/dataTables.min.css') !!}"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />

    <link rel="stylesheet" href="{!! asset($public_url.'css/jquery.lwMultiSelect.css') !!}"/>
    <link rel="stylesheet" href="{!! asset($public_url.'css/multi-select.css') !!}"/>
    <link rel="stylesheet" href="{!! asset($public_url.'css/bootstrap-select.min.css') !!}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900&display=swap" rel="stylesheet">
    <style>

        td.status > i {
            content: "";
            float: left;
            display: block;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            margin: 5px 8px;
        }
        header{ background-color: #ffffff !important; }
        header i.bg{ background-color: #ffffff !important; }
        /*.website-local header{ background-color: #b6e5ff !important; }
        .website-local header i.bg{ background-color: #b6e5ff !important; }
        .website-mclck header{ background-color: #fff17c !important; }
        .website-mclck header i.bg{ background-color: #fff17c !important; }*/
        .login *{font: 15px poppins,sans-serif !important;}
    </style>
    <!--Page Specific resources-->
    @stack("specific.styles")
    <!--/Page Specific resources-->

    <!--Page specific styles-->
    @stack("styles")
    <!--/Page specific styles-->

    <!--Inject body before elements-->
    @stack("body.before")
    <!--/Inject body before elements-->
    @php
        $env = 'live';
        if(auth()->check()){
            $user_email = auth()->user()->email;
            if(strpos($user_email, "@mediaclick.com.tr") !== false || strpos($user_email, "@veron.com") !== false)  {
                $server = explode('.',request()->server('HTTP_HOST'));
                if(last($server)=='local'){
                    $env = 'local';
                }elseif($server[1] == 'mclck'){
                    $env = 'mclck';
                }
            }
        }
    @endphp
</head>

<body class="animated fadeIn website-{{ $env }}">

<div class="modal main-modal fade" id="general-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal</h4>

                <button type="button" class="close" id="closePopup" data-dismiss="modal" aria-hidden="true"
                        onclick="javascript:close_popup(this);">
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

@stack('modals')

@if(strpos(Request::path(), 'login') == false)
    @include('MPCorePanel::inc.header')
    <div class="container-fluid">
        <div class="page">
            @yield('content')
        </div>
    </div>
@else
    @yield('content')
@endif


@if(config('mediapress.google2fa') && !isMCAdmin() && session('panel.user') && is_null(auth('admin')->user()->google2fa))
    @include('MPCorePanel::inc.google2fa')
@endif

@include('MPCorePanel::inc.footer')


<script src="{!! asset($public_url.'js/jquery.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/popper.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/bootstrap.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/jquery-ui.js') !!}"></script>
<script src="{!! asset($public_url.'js/icheck.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/jquery.nestable.js') !!}"></script>
<script src="{!! asset($public_url.'js/dropify.js') !!}"></script>
<script src="{!! asset($public_url.'js/chosen.jquery.js') !!}"></script>
<script src="{!! asset($public_url.'js/bootstrap-select.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/mediapress.js') !!}"></script>
<script src="{!! asset($public_url.'js/mpbackup.js') !!}"></script>


<script src="{!! asset($public_url.'js/mpgeneral.js') !!}"></script>
<script src="{!! asset($public_url.'js/sweetalert2.js') !!}"></script>

<!-- DataTable-->
<script src="{!! asset($public_url.'js/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset($public_url.'js/jquery.multi-select.js') !!}"></script>
<script src="{!! asset($public_url.'js/jquery.lwMultiSelect.min.js') !!}"></script>
{!! (isset($dataTable)) ? $dataTable->scripts() : '' !!}
<!--/DataTable-->

<!--Page Specific resources-->
@stack("specific.scripts")
<!--/Page Specific resources-->

<!--Page Specific scripts-->
@stack("scripts")
<!--/Page Specific scripts-->

@if(auth()->check() && key_exists('Mediapress\VeronLogin\Providers\VeronLoginServiceProvider', app()->getLoadedProviders()))
    @include('VeronPanel::rocket')
@endif
@isset($renderables_script)
    <script type="text/javascript">
        {!! $renderables_script !!}
    </script>
@endisset
<script>
    $(document).ready(function(){

        $('.subTop').click(function () {
            $(this).find('ul').fadeToggle();
        });

        $(".datepicker").datepicker({
            language: 'tr',
            format: "yy-mm-dd",
            dateFormat: "yy-mm-dd",
            monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            dayNamesMin: ["Pa", "Pt", "Sl", "Çrş", "Pe", "Cu", "Ct"],
            firstDay: 1
        });



        $("html").delegate("a.needs-confirmation","click",function(e){

            e.preventDefault();

            var _href = $(this).attr("href");
            var _title = $(this).data("dialog-title") || '{{ trans("MPCorePanel::general.are_you_sure") }}';
            var _text = $(this).data("dialog-text") || '';
            var _type = $(this).data("dialog-type") || 'info';
            var _cancellable = !!$(this).data("dialog-cancellable");
            var _confirm_text = $(this).data("dialog-confirm-text") || '{{ trans("MPCorePanel::general.yes") }}';
            var _cancel_text = $(this).data("dialog-cancel-text") || '{{ trans("MPCorePanel::general.cancel") }}';

            swal({
                title: _title,
                text: _text,
                type: _type,
                showCancelButton: _cancellable,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: _confirm_text,
                cancelButtonText: _cancel_text,
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (confirmed) {
                if (confirmed.value) {
                    location = _href;
                }
            });

        });

        $("html").delegate('form', 'submit', function(){
            $('.remove-on-submit', $(this)).remove();
        });
    });
</script>
<script>
    function locationOnChange($new) {
        if (/delete/g.test($new)) {
            swal({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ trans("MPCorePanel::general.yes") }}',
                cancelButtonText: '{{ trans("MPCorePanel::general.cancel") }}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    location = $new;
                }
            });
        } else {
            location = $new;
        }
       // $(this).removeAttr('selected').find('option:first').attr('selected', 'selected');
    }
    @isset( $dataTablePathConsole )
        {!! $dataTablePathConsole !!}
    @endisset
</script>
<style>
    .select2-container .select2-search--inline {
        float: none;
        padding-left: 10px;
    }
    .select2-container .select2-search--inline > input{
        width:100% !important;
    }
</style>
<script>
    function openFileManagerFromMenu() {
        window.open("{{ url(route("FileManager.index")) }}?basic=1" , "filemanager", "width=1210,height=900,left=400,top=100");
    }
</script>

@if(request()->debugbar==1)

    @php
        $renderer = Debugbar::getJavascriptRenderer();
    @endphp
    {!! $renderer->renderHead() !!}
    {!! $renderer->render() !!}
@endif
</body>
</html>
