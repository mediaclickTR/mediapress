<?php

namespace Mediapress\Modules\Content\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MetaExport implements FromCollection,WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            "id",
            "url",
            "language",
            "title",
            "title_mobile",
            "description",
            "description_mobile",
            "keywords",
            "keywords_mobile",
        ];
    }
}
