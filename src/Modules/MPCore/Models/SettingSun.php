<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class SettingSun extends Model
{
    use CacheQueryBuilder;
    protected $table = 'setting_sun';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['website_id','group', 'title', 'key','value','vtype','params'];
}
