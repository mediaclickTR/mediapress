<?php

namespace Mediapress\Modules\Heraldist\Exports;

use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EbulletinExport implements FromCollection,WithHeadings
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'id',
            trans("HeraldistPanel::ebulletin.name"),
            trans("HeraldistPanel::ebulletin.email"),
            trans("HeraldistPanel::ebulletin.subscription.date"),
        ];
    }
}
