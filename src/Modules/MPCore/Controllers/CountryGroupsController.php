<?php

    namespace Mediapress\Modules\MPCore\Controllers;

    use Mediapress\Http\Controllers\PanelController as Controller;
    use Illuminate\Http\Request;
    use Mediapress\Modules\MPCore\Facades\CountryGroups as CGF;
    use Mediapress\Modules\MPCore\Models\CountryGroup;
    use View;

    class CountryGroupsController extends Controller
    {
        public const OWNER_TYPE = "owner_type";
        public const OWNER_ID = "owner_id";
        public const GROUP_ID = "group_id";
        public const STATUS = "status";
        public const FAIL_MESSAGE = "fail_message";

        /**
         * Main function
         * @param Request $request
         * @return mixed
         */
        public function manage(Request $request)
        {

            //return dd(CGF::getUngroupedCountriesOfOwner(["Mediapress\\Modules\\Content\\Models\\Website",2]));

            //http://pilot.local/mp-admin/MPCore/CountryGroups/manage?changesOwner=no&selectedOwnerId=1&selectedOwnerType=Mediapress%5CModules%5CContent%5CWebsite
            $params = $request->all();
            $renderable = "Mediapress\\Modules\\MPCore\\AllBuilder\\Renderables\\ManageCountryGroups";
            $renderable_object = new $renderable(["request_params" => $params]);

            return view("MPCorePanel::country_groups.manage", compact("renderable_object"));
        }


        /**
         * Return "country groups" of owner item
         * @param Request $request
         * @return string
         */
        public function getOwnersCountryGroups(Request $request)
        {

            // Capture request
            $params = $request->all();
            // Default value to return
            $result = [];

            // Stop if required parameters not sent
            if (!isset($params[self::OWNER_TYPE]) || !isset($params[self::OWNER_ID])) {
                return json_encode($result);
            }

            // Reassign param values (not to use array values directly)
            $owner = $params[self::OWNER_TYPE];
            $owner_id = $params[self::OWNER_ID];

            // CGF::getGroupsOf() may return false so wrap it in if
            if ($result = CGF::getGroupsOf([$owner, $owner_id])) {
                $result = $result->toArray();
            }

            // Return result
            return json_encode($result);
        }


        /**
         * Return country codes array of CountryGroup like ["TR","GB",...]
         * @param Request $request
         * @return string
         */
        public function getGroupsCountries(Request $request)
        {

            // Capture request
            $params = $request->all();
            // Default value to return
            $result = json_encode([]);

            // Stop if required parameters not sent
            if (!isset($params[self::GROUP_ID]) || !$params[self::GROUP_ID]) {
                return $result;
            }

            // Reassign param values (not to use array values directly)
            $group_id = $params[self::GROUP_ID];

            // CGF::getCountriesOfGroup() may return false so "if" it first
            if ($result = CGF::getCountriesOfGroup($group_id)) {
                $result = $result->pluck("code")->toJson();
            }

            return $result;
        }


        /**
         *  Create / Update CountryGroup and sync countries
         * @param Request $request
         * @return string
         */
        public function saveCountryGroup(Request $request)
        {

            // A numeric value for an existing group or "new" keyword for the one which is gonna be created
            $cg_id = $request->cg_id;
            // Possible new | renewal group name
            $group_name = $request->group_name;
            // Country codes array like ["TR","GB",...]
            // CGF::setCountriesOfGroup() requires the countries param to be an array only
            $countries = $request->countries ?? [];

            // Detect | Prepare CountryGroup model at first
            $cg = $cg_id == "new" ? (new CountryGroup()) : CountryGroup::find($cg_id);

            // Owner of country group
            $cg->owner_type = $request->cg_owner;
            $cg->owner_id = $request->cg_owner_id;

            // Assign new name
            $cg->title = $request->group_name;

            // Default return data
            $result = [
                self::STATUS => "fail",
                "model" => null
            ];

            // Save group, sync countries and reassign values of default return data
            if ($cg->save()) {
                CGF::setCountriesOfGroup($cg, $countries);
                $result[self::STATUS] = "success";
                $result["model"] = $cg->toArray();
            }

            return json_encode($result);

        }

        public function removeCountryGroup(Request $request){

            // A numeric value for an existing group which is gonna be deleted
            $cg_id = $request->group_id;

            $cg = CountryGroup::find($cg_id);

            $result = [
                self::STATUS => "fail",
                self::FAIL_MESSAGE =>""
            ];

            if( null == $cg ){
                $result[self::FAIL_MESSAGE]="Grup kayıtlı değil.";
            }elseif ($cg->delete()){
                $result=[self::STATUS =>"success"];
            }else{
                $result[self::FAIL_MESSAGE]="Grup bulundu ancak silinemedi.";
            }

            //return dd($result);

            return response()->json($result);

        }

        public function getUngroupedCountries(Request $request){

            // Capture request
            $params = $request->all();
            // Default value to return
            $result = [];

            // Stop if required parameters not sent
            if (!isset($params[self::OWNER_TYPE]) || !isset($params[self::OWNER_ID])) {
                return json_encode($result);
            }

            // Reassign param values (not to use array values directly)
            $owner = $params[self::OWNER_TYPE];
            $owner_id = $params[self::OWNER_ID];

            // CGF::getUngroupedCountriesOfOwner() may return false so wrap it in if
            if ($result = CGF::getUngroupedCountriesOfOwner([$owner, $owner_id])) {
                $result = $result->pluck("en","code")->toArray();
            }

            // Return result
            return /*addcslashes(*/json_encode($result)/*,"'")*/;
        }

    }
