<?php

namespace Mediapress\Contracts;

interface ICoreModule extends IModule
{

    public function getMPModules();

    public function getMPModuleNames();

    public function getMPSlots();
    
}