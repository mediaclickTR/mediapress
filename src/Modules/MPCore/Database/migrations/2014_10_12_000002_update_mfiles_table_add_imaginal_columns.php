<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMfilesTableAddImaginalColumns extends Migration
{
    public const MFILES = 'mfiles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable(self::MFILES)) {
            Schema::table(self::MFILES, function (Blueprint $table) {
                $table->string('mimeclass',100)->nullable()->default(null);
                $table->integer('width')->nullable()->default(null);
                $table->integer('height')->nullable()->default(null);
                $table->integer('size')->default(0);
                $table->integer('admin_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::MFILES)) {
            Schema::table(self::MFILES, function (Blueprint $table) {
                $table->dropColumn('mimeclass');
                $table->dropColumn('width');
                $table->dropColumn('height');
                $table->dropColumn('size');
                $table->dropColumn('admin_id');
            });
        }
    }
}
