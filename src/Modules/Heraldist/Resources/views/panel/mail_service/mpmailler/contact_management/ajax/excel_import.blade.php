<div class="modal-header">
    <h3><i class="fa fa-upload"></i> {!! trans("ContentPanel::seo.excel.import") !!}</h3>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button>
</div>
<div class="modal-body">
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    <div class="asd">
                        {{ Form::open(['route' => 'Heraldist.mpmailler.contact_management.import', 'enctype'=>'multipart/form-data']) }}
                            <div class="upload-btn-wrapper">
                                <div class="form-group">
                                    <label>{!! trans("ContentPanel::seo.excel.select") !!}</label><br/><br/>
                                    {!!Form::file('excel', null, ["placeholder"=>trans("ContentPanel::seo.excelImport"), "id"=>"input-file-now", "class"=>"dropify"])!!}
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
