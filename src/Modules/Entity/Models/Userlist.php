<?php

namespace Mediapress\Modules\Entity\Models;

use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Database\Eloquent\Model;

class Userlist extends Model
{

    protected $table = 'userlists';
    public $timestamps = true;

    protected $fillable = [
        "name",
        "description"
    ];

    public function userable()
    {
        return $this->morphTo();
    }
    public function userlistPivots()
    {
        return $this->hasMany(UserlistPivot::class, "userlist_id")->count();
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

}