<?php

namespace Mediapress\Modules\Entity\Models;

use Mediapress\Foundation\ResetPassword;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        "data" => "array"
    ];

    public function model()
    {
        return $this->morphMany(EntitylistPivot::class, 'model');
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function website()
    {
        return $this->belongsTo(Website::class);
    }
    public function websites(){
        return $this->belongsToMany(Website::class);
    }

    public function extras(){
        return $this->hasMany(UserExtra::class);
    }
    /*

    public function getShortNameAttribute()
    {
        preg_match('/^[a-zA-Z0-9]+(.*) [a-zA-Z0-9]/si', $this->name, $matches);
        if (isset($matches[0]) && $matches[0]) {
            return $matches[0].'.';
        }
        return $this->name;
    }

    public function address(){
        return $this->hasMany(Address::class)->with("extras");
    }

    public function extras(){
        return $this->hasMany(UserExtra::class);
    }

    public function orders(){
        return $this->hasMany(Order::class)->orderBy('created_at','desc');
    }

    public function __get($key)
    {
        $attribute = $this->getAttribute($key);

        if ($attribute)
        {
            return $attribute;
        }

        if (isset($this->extras) && $this->extras instanceof Collection)
        {
            $extra = $this->extras->where('key', $key)->first();
        }
        else if (method_exists($this, 'extras'))
        {
            $extra = $this->extras()->where('key', $key)->first();
        }

        if (isset($extra->value))
        {
            return $extra->value;
        }
    }
    */
    public function sendPasswordResetNotification($token)
    {

        $this->notify(new ResetPassword($token));
    }

}
