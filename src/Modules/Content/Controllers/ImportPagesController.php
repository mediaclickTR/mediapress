<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Imports\PagesImport;
use Mediapress\Modules\Content\Models\Sitemap;
use PhpOffice\PhpWord\Shared\Html;
use Maatwebsite\Excel\Facades\Excel;

class ImportPagesController extends Controller
{
    public function selectLanguage()
    {
        $website = session('panel.website');
        $countryGroups = $website->countryGroups;

        return view('ContentPanel::import_pages.select_language', compact('countryGroups'));
    }

    public function index(Request $request)
    {
        $lang = $request->get('language');

        if ($lang) {
            $lang = explode(',', $lang);

            $sitemaps = Sitemap::whereHas('sitemapType', function ($q) {
                $q->where('sitemap_type_type', 'dynamic');
            })
                ->whereHas('detail', function ($q) use ($lang) {
                    $q->where('country_group_id', $lang[0])
                        ->where('language_id', $lang[1]);
                })
                ->whereHas('websites', function ($q) use ($lang) {
                    $q->where('id', session('panel.website.id'));
                })
                ->get();
        } else {
            return $this->selectLanguage();
        }

        return view('ContentPanel::import_pages.index', compact('sitemaps'));
    }

    public function import(Request $request) {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $tempLanguage = explode(',', $request->get('language'));
        $file = $request->file('file');

        if (count($tempLanguage) != 2) {
            return redirect()->route('Content.import_pages.selectLanguage');
        }

        if (is_null($file)) {
            return redirect()->route('Content.import_pages.index', ['language' => $request->get('language')]);
        }

        $countryGroupId = $tempLanguage[0] * 1;
        $languageId = $tempLanguage[1] * 1;


        $fileName = $file->store(null, 'public');
        $fileName = storage_path('app/public/'. $fileName);


        Excel::import(new PagesImport($countryGroupId, $languageId), $fileName);

        @unlink($fileName);

        return redirect()->route('Content.import_pages.index', ['language' => $request->get('language')])->with(['message' => "İçeri Aktarım Başarılı !"]);
    }
}
