<div class="modal-header">
    <h3>{!! trans("HeraldistPanel::contact_management.send") !!}</h3>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">×
    </button>
</div>
<div id="sending_emails">{!! trans("HeraldistPanel::contact_management.sending") !!}</div>
<div class="modal-body" id="email_sms_modal">
    <div class="content">
        <div class="col-md-12 pa">
            <div class="rootwizard">
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <ul>
                                <li><a href="#template" data-toggle="tab">1. {!! trans("HeraldistPanel::contact_management.select.template") !!}</a></li>
                                <li><a href="#content" id="tab-language" data-toggle="tab">2. {!! trans("HeraldistPanel::contact_management.select.content") !!}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="bar" class="progress progress-striped active">
                    <div class="progress-bar"></div>
                </div>
                <div class="tab-content">
                    <form action="" method="POST">
                        @csrf
                        <div id="template" class="tab-pane fade in">
                            <input type="hidden" value='{!! $data['selected'] !!}' name="selected">
                            <div class="form-group" style="display:none;">
                                <span class="tit">{!! trans("HeraldistPanel::contact_management.select.type") !!}</span>
                                <div class="checkbox">
                                    <label>
                                        {!! trans("HeraldistPanel::contact_management.select.email") !!}
                                        <input type="radio" checked name="type" id="type" value="email">
                                    </label>
                                    <label>
                                        {!! trans("HeraldistPanel::contact_management.select.sms") !!}
                                        <input type="radio" name="type" id="type" value="sms">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group  col-md">
                                <span class="tit">{!! trans("HeraldistPanel::contact_management.select.template") !!}</span><br/><br/>
                                {!!Form::select('template', $data['templates'],null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => '', 'id'=>'templates'])!!}
                            </div>
                            <ul class="pager wizard ">
                                <li class="next float-right"><a href="javascript:void();" class="btn btn-primary">{!! trans("MPCorePanel::general.next") !!} <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
                        <div id="content" class="tab-pane fade in">

                            <div class="form-group  col-md">
                                <span class="tit">{!! trans("HeraldistPanel::contact_management.email.subject") !!}</span><br/><br/>
                                {!!Form::text('subject',null, ['placeholder' => trans("MPCorePanel::general.selection")])!!}
                            </div>

                            <div class="form-group  col-md">
                                <span class="tit">{!! trans("HeraldistPanel::contact_management.email.detail") !!}</span><br/><br/>
                                {!!Form::textarea('detail',null,[])!!}
                            </div>

                            <div class="float-right">
                                <button id="sendMultipleEmail" class="btn btn-primary" type="submit">{!! trans("MPCorePanel::general.save") !!}</button>
                            </div>

                            <div class="clearfix"></div>
                            <ul class="pager wizard">
                                <li class="previous float-right"><a style="color:#f3dd24 !important;font-weight:bold !important;" href="javascript:;"><i class="fa fa-angle-double-left"></i>{!! trans("MPCorePanel::general.back") !!}</a></li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
<script>
    $(document).ready(function() {
        $('.rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('.rootwizard').find('.progress-bar').css({width:$percent+'%'});
        }});

        $('#sendMultipleEmail').click(function(e){
            e.preventDefault();

            $("#email_sms_modal").css("pointer-events", "none").css("opacity", "0.4");
            $("#sending_emails").css("display", "block");
            var type = $("input[name='type']").val();
            var template = $("select[name='template']").val();
            var subject = $("input[name='subject']").val();
            var detail = $("textarea[name='detail']").val();
            var selected = $("input[name='selected']").val();
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ route('Heraldist.mpmailler.contact_management.send') }}",
                method: 'POST',
                data: {
                    type:type,
                    template:template,
                    subject:subject,
                    detail:detail,
                    selected:selected,
                    _token:token
                },
                success: function (data) {
                    $("#sending_emails").css("display", "none");
                    $("#email_sms_modal").css("pointer-events", "auto").css("opacity", "1");
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    });
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        });

    });

</script>
