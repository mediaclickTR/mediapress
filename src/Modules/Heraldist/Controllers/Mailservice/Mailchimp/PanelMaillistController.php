<?php

namespace Mediapress\Modules\Heraldist\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Facades\MPCore;

class PanelMaillistController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const LISTS = "lists";
    public const HERALDIST_MAILCHIMP_MAILLISTS_INDEX = 'Heraldist.mailchimp.maillists.index';
    public const MESSAGE = 'message';
    public const LISTS1 = "lists/";
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const DENEME = 'deneme';
    protected $apiKey;
    protected  $apiUrl;

    public function __construct()
    {
        $mailChimpApiKey = settingSun('heraldist.mailchimp.apikey');
        // API KEY KONTROL
        if($mailChimpApiKey!=''){
            $this->apiKey = $mailChimpApiKey;
        }
        else {
            //dd("Mailchimp api key bulunamadı.3");
            return;
        }

        // APi URL
        $dataCenter = substr($this->apiKey,strpos($this->apiKey,'-')+1);
        $this->apiUrl = "https://".$dataCenter.".api.mailchimp.com/3.0/";
    }

    public function index()
    {
        if(!userAction('message.forms.index',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        // Curl list
        $maillists = curlGenerator($this->apiUrl. self::LISTS,$this->apiKey,"GET");
        $total = $maillists->total_items;
        $maillists = $maillists->lists;
        return view('HeraldistPanel::mail_service.mailchimp.maillists.index', compact('maillists','total'));
    }

    public function create()
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        return view("HeraldistPanel::mail_service.mailchimp.maillists.create", compact("mailtemplates","data",'table_names'));
    }

    public function edit($id)
    {
        //curl edit
        $maillist = curlGenerator($this->apiUrl.     self::LISTS1 .$id,$this->apiKey,"GET");

        return view('HeraldistPanel::mail_service.mailchimp.maillists.edit', compact("maillist"));
    }

    public function store(Request $request)
    {
        if(!userAction('message.maillists.create',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $json = json_encode([
            'name' => $request->name,
            'contact'  => [
                'company' => 'MailChimp',
                'address1' => self::DENEME,
                'address2' => self::DENEME,
                'city' => self::DENEME,
                'state' => self::DENEME,
                'zip' => self::DENEME,
                'country' => self::DENEME,
                'phone' => self::DENEME
            ],
            'campaign_defaults'  => [
                'from_name' => $request->from_name,
                'from_email' => $request->from_email,
                'subject' => $request->subject,
                'language' => 'tr',
            ],
            'permission_reminder'=> 'Youre receiving this email because you signed up for updates about Freddies newest hats.',
            'email_type_option' => true
        ]);

        // curl store
        curlGenerator($this->apiUrl. self::LISTS,$this->apiKey,null,$json);

        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {

        $json = json_encode([
            'name' => $request->name,
            'contact'  => [
                'company' => 'MailChimp',
                'address1' => self::DENEME,
                'address2' => self::DENEME,
                'city' => self::DENEME,
                'state' => self::DENEME,
                'zip' => self::DENEME,
                'country' => self::DENEME,
                'phone' => self::DENEME
            ],
            'campaign_defaults'  => [
                'from_name' => $request->from_name,
                'from_email' => $request->from_email,
                'subject' => $request->subject,
                'language' => 'tr',
            ],
            'permission_reminder'=> 'Youre receiving this email because you signed up for updates about Freddies newest hats.',
            'email_type_option' => true
        ]);

        curlGenerator($this->apiUrl. self::LISTS1 .$request->id,$this->apiKey,"PATCH",$json);

        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(!userAction('message.maillists.delete',true,false)){
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        //Curl delete
        curlGenerator($this->apiUrl. self::LISTS1 .$id,$this->apiKey,"DELETE");

        return redirect(route(self::HERALDIST_MAILCHIMP_MAILLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
