<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class CreateWebsiteDetailsTable extends Migration
    {

        public const WEBSITE_DETAILS = 'website_details';

        public function up()
        {
            if (!Schema::hasTable(self::WEBSITE_DETAILS)) {
                Schema::create(self::WEBSITE_DETAILS, function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('website_id')->unsigned()->index();
                    $table->unsignedInteger('language_id')->unsigned()->index();
                    $table->unsignedInteger('country_group_id')->nullable()->index();
                    $table->string('name');
                    $table->string('slug')->nullable();
                    $table->timestamps();
                    $table->softDeletes();
                });
        }
        }

        public function down()
        {
            if (Schema::hasTable(self::WEBSITE_DETAILS)) {
                Schema::drop(self::WEBSITE_DETAILS);
            }
        }
    }
