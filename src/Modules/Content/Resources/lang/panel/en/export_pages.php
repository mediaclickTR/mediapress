<?php

return [
    'select_language' => "Choose Language",
    'next' => "Next",
    "export" => "Export",
    "select_pages" => "Select Pages",
];
