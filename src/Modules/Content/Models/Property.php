<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Traits\HasMFiles;
use Mediapress\Modules\Content\Traits\HasDetails;

class Property extends BaseModel
{

    use SoftDeletes;
    use HasMFiles;
    use HasDetails;

    protected $table = 'properties';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $fillable = ['sitemap_id', 'property_id', 'type', 'status', 'admin_id', 'lft', 'rgt', 'depth', 'ctex_1', 'ctex_2', 'cvar_1', 'cvar_2', 'cvar_3', 'cvar_4', 'cvar_5', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cdat_1', 'cdat_2', 'cdec_1', 'cdec_2', 'cdec_3', 'cdec_4'];

    public function details()
    {
        return $this->hasMany(PropertyDetail::class);
    }

    public function children()
    {
        return $this->hasMany(Property::class);
    }

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
