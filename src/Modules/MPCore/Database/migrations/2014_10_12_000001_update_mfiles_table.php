<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMfilesTable extends Migration
{
    public const MFILES = 'mfiles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( Schema::hasTable(self::MFILES)) {
            Schema::table(self::MFILES, function (Blueprint $table) {

                $table->dropColumn('name');
                $table->string('uploadname',1000);
                $table->string('fullname',1000);
                $table->string('filename',1000);
                $table->string('extension',1000);
                $table->string('process_id',200);
                $table->string('mimetype',100)->nullable()->default(null);

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::MFILES)) {
            Schema::table(self::MFILES, function (Blueprint $table) {
                $table->string('name',500);
                $table->dropColumn('uploadname');
                $table->dropColumn('fullname');
                $table->dropColumn('filename');
                $table->dropColumn('extension');
                $table->dropColumn('process_id');
                $table->dropColumn('mimetype');

            });
        }
    }
}
