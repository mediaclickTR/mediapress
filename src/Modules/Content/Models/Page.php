<?php

namespace Mediapress\Modules\Content\Models;

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Comment\Models\Comment;
use Mediapress\Modules\MPCore\Models\BaseModel;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\FileManager\Models\MFile;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Traits\HasMFiles;
use Mediapress\Modules\Content\Traits\HasDetails;
use Mediapress\ECommerce\Models\Product;
use App;

class Page extends BaseModel
{
    use SoftDeletes;
    use HasMFiles;
    use HasDetails;


    public const SITEMAP_ID = "sitemap_id";
    protected $table = 'pages';

    public $timestamps = true;

    protected $fillable = [
        self::SITEMAP_ID,
        "page_id",
        "admin_id",
        "fb_template",
        "order",
        "date",
        "status",
        "published_at",
        'ctex_1',
        'ctex_2',
        'cvar_1',
        'cvar_2',
        'cvar_3',
        'cvar_4',
        'cvar_5',
        'cint_1',
        'cint_2',
        'cint_3',
        'cint_4',
        'cint_5',
        'cdat_1',
        'cdat_2',
        'cdec_1',
        'cdec_2',
        'cdec_3',
        'cdec_4',
    ];
    protected $dates = ['published_at', 'deleted_at'];

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function category()
    {
        return $this->categories()->first();
    }

    public function parent()
    {
        return $this->belongsTo(Page::class, "page_id");
    }

    public function children()
    {
        return $this->hasMany(Page::class);
    }

    public function criterias()
    {
        return $this->belongsToMany(Criteria::class);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class)->withPivot('value')->withPivot('language_id');
    }

    public function secondCriterias()
    {
        return $this->belongsToMany(Criteria::class);
    }

    public function thirdCriterias()
    {
        return $this->belongsToMany(Criteria::class);
    }

    public function extras()
    {
        return $this->hasMany(PageExtra::class);
    }

    public function extra()
    {
        return $this->hasOne(PageExtra::class);
    }

    public function ecommerce()
    {
        return $this->hasOne(Product::class)->join('country_groups', 'country_groups.id', 'products.country_group_id')/*->orderBy('languages.sort')*/->select('products.*');
    }

    public function details()
    {
        return $this->hasMany(PageDetail::class)->join('languages', 'languages.id', 'page_details.language_id')/*->orderBy('languages.sort')*/->select('page_details.*');
    }

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
    }

    public function urls()
    {
        return $this->morphToMany(Url::class, PageDetail::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'model', 'model_class');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Sitemap null = all
     * Sitemap same = this
     * Sitemap id = sitemap_id
     *
     * @param int $limit
     * @param null $sitemap
     */
    public function similars($limit = 5, $sitemap = null)
    {
        $tags = $this->tags->pluck('id')->toArray();
        $list = [];

        if ($tags) {
            foreach ($tags as $tag) {
                $tag = Tag::with('pages')->find($tag);
                foreach ($tag->pages as $page) {
                    if ($page->id != $this->id) {
                        $list[$page->id] = ((isset($list[$page->id])) ? $list[$page->id] + 1 : 1);
                    }
                }
            }

            arsort($list);
            $list = array_keys($list);
            //$list = array_slice($list,0,$limit);

            if (!$sitemap) {
                $pages = Page::whereIn('id', $list);
            } elseif ($sitemap == 'same') {
                $pages = Page::whereIn('id', $list)->where(self::SITEMAP_ID, $this->sitemap_id);
            } else {
                $pages = Page::whereIn('id', $list)->where(self::SITEMAP_ID, $sitemap);
            }

            if ($list) {
                $pages = $pages->orderByRaw("FIELD(`id`," . @implode(',', $list) . ")");
            }

            $pages = $pages->limit($limit)->get();

            if ($pages) {
                return $pages;
            }
        }
        return collect();


    }
}
