<?php

return [
    'messages' => 'Mesajlar',
    'forms' => 'Formlar',
    'new_total' => 'Yeni/Toplam',
    'gomessages' => 'Mesajlara Git',
    'open-new-page' => 'Yeni sayfada aç',
    'check-unread' => 'Okunmadı olarak işaretle',
    'form_messages' => ':form adlı Forma Ait Mesajlar',
    'empty_message_box' => 'Mesaj kutunuz boş.',
    'open.new.page' => 'Yeni sayfada aç',
    'select_all' => 'Hepsini Seç',
    'export_excel' => 'Excel\'e Aktar',
    'download_file' => 'Dosyayı İndir',
    'form_detail' => [
        'date' => 'Tarih',
        'time' => 'Saat',
        'referrer' => 'Nereden Geldi',
        'visited_pages' => 'Gezdiği Sayfalar',
        'utm' => 'Utm Bilgileri',
        'form_recording' => 'Form Doldurma',
        'watch_recording' => 'Kaydı İzle',
    ],
];
