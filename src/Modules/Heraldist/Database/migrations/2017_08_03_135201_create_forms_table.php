<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    public const FORMS = 'forms';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable(self::FORMS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::FORMS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('sender')->nullable();
                $table->string('password')->nullable();
                $table->longText('formbuilder_json')->nullable();
                $table->string('smtp')->nullable();
                $table->string('port')->nullable();
                $table->string('security')->nullable();
                $table->string('receiver')->nullable();
                $table->string('success')->nullable();
                $table->string('error')->nullable();
                $table->string('button')->nullable();
                $table->string('captcha')->nullable();
                $table->string('captcha_site_key')->nullable();
                $table->string('captcha_secret')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        // Pivot With websites
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('form_website', function (Blueprint $table) {
            $table->integer('form_id');
            $table->integer('website_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop(self::FORMS);
        Schema::drop('form_website');
        Schema::enableForeignKeyConstraints();
    }
}
