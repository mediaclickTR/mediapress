@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! __("ContentPanel::sitemap.edit_sitemap") !!}</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    {{ Form::model($sitemap, ['route' => ['Content.sitemaps.mainUpdate'], 'method' => 'POST']) }}
                    main sitemap edit inputs
                    <div class="submit">
                        <button type="submit">{!! __("ContentPanel::sitemap.save") !!}</button>
                    </div>
                    {{ Form::close() }}
                </div>
             </div>
        </div>
    </div>
@endsection
