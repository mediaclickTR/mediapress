<?php

namespace Mediapress\Modules\Content\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;

class PopupLanguage extends Model
{

    protected $table = 'popup_languages';

    public $timestamps = false;

    protected $fillable = [
        "popup_id",
        "country_group_id",
        "language_id"
    ];


    public function popup()
    {
        return $this->belongsTo(Popup::class);
    }
    public function country_group()
    {
        return $this->belongsTo(CountryGroup::class);
    }

}
