@extends("HeraldistPanel::inc.module_main")
@section('content')
@include('MPCorePanel::inc.breadcrumb')
<div class="content">
    <div class="widget">

        <div class="widget-header">&nbsp;
        </div>
        <div class="widget-body">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-title">
                        <a href="{!! route('Heraldist.message.show',['subject'=>$message->subject]) !!}" class="btn"
                           rel="tooltip" title="" data-original-title="Geri" style="float:left;"><i
                                    class="fa fa-arrow-left"></i></a>
                        <h3><i class="fa fa-envelope"></i>Mesaj : #{!! $message->id !!} - {!! $message->subject !!}</h3>
                    </div>
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                        <div class="box-content nopadding" style="overflow: hidden; width: auto;">
                            <ul class="messages">
                                <li class="left">
                                    <div class="image">
                                        <img src="//0.gravatar.com/avatar/{!! md5($message->mail) !!}?d=mm&amp;s="
                                             alt="">
                                    </div>
                                    <div class="message" style="background:white;">
                                            <span class="name"> <a
                                                        href="mailto:{!! $message->mail !!}">&lt;{{$message->mail}}&gt;</a></span>
                                        <span class="time"
                                              style="float:right;">{!! date('d.m.Y H:i',strtotime($message->created_at)) !!}</span>
                                        <br>
                                        <br>
                                        <div class="body" style="border: 1px solid #990033;">
                                            <div class="head"
                                                 style="background: #990033; padding: 5px 0; text-align:center;">
                                                <h3 style="color:#fff;">
                                                    {!! $message->subject !!}
                                                </h3>

                                            </div>
                                            <table class="table">
                                                @foreach($message->data as $key=>$data)
                                                    <tr>
                                                        <td>{{ $key }}</td>
                                                        <td>
                                                            {{$data}}
                                                            @if(is_url($data))
                                                                <br/><a href="{{$data}}" class="btn" target="_blank">Yeni
                                                                    Sayfada Aç</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td>IP</td>
                                                    <td>{{ $message->ip }}</td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </li>
                            </ul>
                            <div class="form-actions">
                                @if(userAction('messages.mark_all_unread',true,false))
                                    <a href="{!! route('Heraldist.message.notRead',['id'=>$message->id]) !!}"
                                       class="btn btn-primary">Okunmadı Olarak İşaretle</a>
                                @endif
                                @if($message->deleted)
                                    <a href="{!! route('Heraldist.message.inbox',['id'=>$message->id]) !!}"
                                       class="btn btn-info">Gelen Kutusuna Taşı</a>
                                @else
                                    @if(userAction('messages.delete',true,false))
                                        <a href="{!! route('Heraldist.message.delete',['id'=>$message->id]) !!}"
                                           class="btn btn-danger">Sil</a>
                                    @endif
                                @endif

                            </div>
                        </div>
                        <div class="slimScrollBar ui-draggable"
                             style="background: rgb(102, 102, 102); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 464px;"></div>
                        <div class="slimScrollRail"
                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                    </div>
                </div>
            </div>
            <table>

            </table>
        </div>
    </div>
</div>
@endsection
