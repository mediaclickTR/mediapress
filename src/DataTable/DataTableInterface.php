<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.03.2017
 * Time: 17:29
 */

namespace Mediapress\DataTable;

use Yajra\Datatables\Html\Builder;

interface DataTableInterface
{
    public function columns(Builder $builder);
}