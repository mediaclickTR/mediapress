<?php

return [
    'entitylist' => 'Member Lists',
    'standard' => 'Standard',
    'core' => 'Core',

    'list-members' => 'List Members',
    'list-members-create' => 'Add Members to List',
    'edit-entitylist' => 'Update List',

    'name-entitylist' => 'List Name',
    'description-entitylist' => 'List Description',
    'create-entitylist' => 'Create List',
    "select_websites" => "Select Website (s) to Link List to",
];
