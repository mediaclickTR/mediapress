<?php

namespace Mediapress\Modules\Entity\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Entity\Models\User;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Http\Request;
use Mediapress\Modules\Entity\Models\Userlist;

class UserlistController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const DESCRIPTION = 'description';
    public const NAME = 'name';
    public const ADMIN_USERLISTS_INDEX = 'admin.userlists.index';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        if(! activeUserCan(["entity.users.user_lists.index",])){return rejectResponse();}

        $userlists = Userlist::paginate(25);
        return view('EntityPanel::userlists.index', compact('userlists'));
    }

    public function create()
    {

        if(! activeUserCan(["entity.users.user_lists.create",])){return rejectResponse();}

        return view("EntityPanel::userlists.create");
    }

    public function edit($id)
    {
        if(! activeUserCan(["entity.users.user_lists.update",])){return rejectResponse();}

        $userlist = Userlist::find($id);
        return view('EntityPanel::userlists.edit', compact("userlist"));
    }

    public function store(Request $request)
    {
        if(! activeUserCan(["entity.users.user_lists.create",])){return rejectResponse();}

        $fields = [
            self::NAME => 'Liste Adı',
            self::DESCRIPTION => 'Açıklama',
        ];
        $rules = [
            self::NAME => 'required|max:255',
        ];
        $messages = [
            'name.required' => 'Liste adı boş bırakılmamalıdır.',
        ];
        $this->validate($request, $rules, $messages, $fields);
        $data = request()->except("_token");
        $insert=Userlist::firstOrCreate($data);

        if ($insert){
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);
        }

        return redirect(route(self::ADMIN_USERLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {
        if(! activeUserCan(["entity.users.user_lists.update",])){return rejectResponse();}

        $fields = [
            self::NAME => 'Liste Adı',
            self::DESCRIPTION => 'Açıklama',
        ];
        $rules = [
            self::NAME => 'required|max:255',
        ];
        $messages = [
            'name.required' => 'Liste adı boş bırakılmamalıdır.',
        ];
        $datas = [
            self::NAME => $request->name,
            self::DESCRIPTION => $request->description,
        ];
        $this->validate($request, $rules, $messages, $fields);

        $update=Userlist::updateOrCreate(['id'=>$request->id],$datas);
        if ($update){
            UserActionLog::update(__CLASS__."@".__FUNCTION__,$update);
        }

        return redirect(route(self::ADMIN_USERLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if(! activeUserCan(["entity.users.user_lists.delete",])){return rejectResponse();}

        $data = Userlist::find($id);

        if($data) {
            $data->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__,$data);
        }

        return redirect(route(self::ADMIN_USERLISTS_INDEX))->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }
}
