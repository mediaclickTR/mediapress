<?php

namespace Mediapress\DataTable\Tables;

use Html;
use Mediapress\DataTable\DataTableInterface;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Auth\Models\Role;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class PanelAdminController implements DataTableInterface
{
    const ROLE_ID = 'role_id';
    const TITLE = 'title';
    const FOOTER = 'footer';

    public function columns(Builder $builder)
    {
        return $builder->columns([

            [
                'data' => self::ROLE_ID, 'name' => self::ROLE_ID, self::TITLE => trans("MPCorePanel::general.role"), self::FOOTER => trans("MPCorePanel::general.role")
            ],
            [
                'data' => 'username', 'name' => 'username', self::TITLE => trans("MPCorePanel::general.name"), self::FOOTER => trans("MPCorePanel::general.name")
            ], [
                'data' => 'email', 'name' => 'email', self::TITLE => trans("MPCorePanel::general.email"), self::FOOTER => trans("MPCorePanel::general.email")
            ],
            /*[
                'data' => 'phone', 'name' => 'phone', 'title' => 'Telefon', 'footer' => trans("MPCorePanel::general.phone")
            ],
            */
            [
                'data' => 'created_at', 'name' => 'created_at', self::TITLE => trans("MPCorePanel::general.created_at"), self::FOOTER => trans("MPCorePanel::general.created_at")
            ], [
                'data' => 'updated_at', 'name' => 'updated_at', self::TITLE => trans("MPCorePanel::general.updated_at"), self::FOOTER => trans("MPCorePanel::general.updated_at")
            ]
        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);

    }
    public function ajax($website_id=null)
    {
        /*$superMCAdminRole = Role::where("name","SuperMCAdmin")->select("id")->first();
        $superMCAdminRoleId = $superMCAdminRole ? $superMCAdminRole->id : 0;*/

        $roles = Role::where("name","<>", "SuperMCAdmin")->select("name")->get()->pluck("name")->toArray();
        $admins = Admin::query();

        $isFirst = true;
        while(count($roles)){
            $role = array_shift($roles);
            if($isFirst){
                $admins = $admins->whereIs($role);
                $isFirst = false;
            }else{
                $admins =  $admins->orWhere(function($query)use($role){
                    $query->whereIs($role);
                });
            }
        }


        return Datatables::eloquent($admins)
            ->addColumn(self::ROLE_ID, function($admin)
            {
                //return get_class($admin->getRoles());
                return implode(", ", $admin->getRoles()->toArray(9));
            })
            ->addColumn('action', function($admin)
            {
                return
                    '<a href="'.route('Auth.admins.edit',  $admin->id).'" class="m-1" title="' . trans("MPCorePanel::general.edit") . '"><span class="fa fa-pen"></span></a>'.
                    '<a href="'.route('Auth.admins.delete',$admin->id).'" class="m-1 needs-confirmation" title="' . trans("MPCorePanel::general.delete") . '" data-dialog-text="'.trans("MPCorePanel::general.action_cannot_undone").'" data-dialog-cancellable="1" data-dialog-type="warning" ><span class="fa fa-trash"></span></a>';

            })
            ->setRowId('tbl-{{$id}}')->make(true);
    }
}
