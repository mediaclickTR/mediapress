<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMetasTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('metas', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs("url");
            $table->string("key");
            $table->string("value")->nullable();
            $table->integer("order");
            $table->string("type");
            $table->string("template");
            $table->string("indexed")->default("index,follow");
            $table->timestamps();
            $table->softDeletes();

//            $table->index(["url_type", "url_model"], "url_index");
            $table->index("key", "key_index");
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
       // Schema::dropIndex("url_index");
       // Schema::dropIndex("key_index");
        Schema::dropIfExists('metas');
        Schema::enableForeignKeyConstraints();
    }
}
