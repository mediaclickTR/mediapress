<?php

namespace Mediapress\Modules\Content\Controllers;

use Mediapress\Facades\DataSourceEngine;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\Content\Facades\CategoryEngine;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryCriteria;
use Mediapress\Modules\Content\Models\CategoryProperty;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public const CATEGORY_ID = "category_id";
    public const SITEMAP_TYPE = "sitemapType";
    public const WEBSITES = 'websites';
    public const ERROR = "error";
    public const CATEGORY = "category";
    public const MP_CORE_PANEL_GENERAL_DANGER_MESSAGE = 'MPCorePanel::general.danger_message';
    public const MESSAGE = "message";
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {

        $website = session("panel.website");
        $website_id = $website->id;

        if(!activeUserCan([
                "content.websites.website$website_id.sitemaps.categories.index",
                "content.websites.sitemaps.categories.index"
            ]) &&
            activeUserCan([
                "content.websites.website$website_id.sitemaps.criterias.index",
                "content.websites.sitemaps.criterias.index"
            ]) &&
            activeUserCan([
                "content.websites.website$website_id.sitemaps.properties.index",
                "content.websites.sitemaps.properties.index"
            ])
        ){
            return rejectResponse();
        }


        $data["countCategories"] = Sitemap::whereHas(self::WEBSITES, function ($query) use ($website_id) {
            $query->where('id', $website_id);
        })->with(self::SITEMAP_TYPE)->where(self::CATEGORY, "1")->count();

        $data["countCriterias"] = Sitemap::whereHas(self::WEBSITES, function ($query) use ($website_id) {
            $query->where('id', $website_id);
        })->where("criteria", "1")->count();

        $data["countProperties"] = Sitemap::whereHas(self::WEBSITES, function ($query) use ($website_id) {
            $query->where('id', $website_id);
        })->where("property", "1")->count();

        $crumb = [
            "key"=>"taxonomy_index",
            "text"=>trans("ContentPanel::categories.classification"),
            "icon"=>"stream",
            "href"=>"javascript:void(0);"
        ];
        $breadcrumb = Content::getBreadcrumb([$crumb]);

        return view('ContentPanel::categories.index', compact( 'data', 'breadcrumb'));
    }

    /**
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getList($type = "category")
    {
        $website_id = session("panel.website.id");

        if (!userAction('sitemap.category.index', true, false)) {
            return redirect()->to(url(route('accessdenied')));
        }

        $plural = Str::plural($type);

        if ($type == self::CATEGORY  || $type == "criteria" || $type == "property") {


            if(! activeUserCan([
                "content.websites.website$website_id.sitemaps.$plural.index",
                "content.websites.sitemaps.$plural.index",
            ])){
                return rejectResponse();
            }

            $title = trans("ContentPanel::categories." . $plural);
            // current website sitemap categories list
            $website_id = session("panel.website")->id;
            $sitemaps = Sitemap::whereHas(self::WEBSITES, function ($query) use ($website_id) {
                $query->where('id', $website_id);
            })->with(self::SITEMAP_TYPE)->where($type, "1")->where($type, 1)->get();
        } else {
            dd("Lütfen sadece kategori, kriter veya özellik seçiniz.");
        }
        $icons = ["category"=>"stream", "criteria"=>"tasks", "property"=>"list"];

        $crumbs = [
            [
                "key"=>"taxonomy_index",
                "text"=>trans("ContentPanel::categories.classification"),
                "icon"=>"stream",
                "href"=>route("Content.categories.index")
            ],
            [
                "key"=> $plural . "_index",
                "text"=>trans("ContentPanel::categories.".$plural),
                "icon"=>$icons[$type],
                "href"=>"javascript:void(0);"
            ]
        ];
        $breadcrumb = Content::getBreadcrumb($crumbs);

        return view("ContentPanel::categories.list", compact('sitemaps', 'title', 'type','breadcrumb'));
    }

    public function create($sitemap_id, $id = null)
    {
        $websiteId = session("panel.website.id");

        if(! activeUserCan([
            "content.websites.website$websiteId.sitemaps.categories.index",
            "content.websites.sitemaps.categories.index"
        ])
        ){
            return rejectResponse();
        }

        $sitemap = Sitemap::with(self::SITEMAP_TYPE)->find($sitemap_id);

        if (!$sitemap) {
            dd(trans("ContentPanel::categories.not_found_sitemap"));
        }

        $data = new \stdClass();
        $data->nestable = CategoryEngine::nestableGetList($sitemap_id, Category::class, self::CATEGORY_ID);
        //dd($data->nestable);
        $type = trans("ContentPanel::categories.category");
        $metaset =json_encode([]);


        $crumbs = [
            [
                "key"=>"taxonomy_index",
                "text"=>trans("ContentPanel::categories.classification"),
                "icon"=>"stream",
                "href"=>route("Content.categories.index")
            ],
            [
                "key"=>"categories_index",
                "text"=>trans("ContentPanel::categories.categories"),
                "icon"=>"stream",
                "href"=>route("Content.categories.getList", "category")
            ],
            [
                "key"=>"categories_of_sitemap",
                "text"=>$sitemap->detail->name. " - ". trans("ContentPanel::categories.categories"),
                "icon"=>"sitemap",
                "href"=>route("Content.categories.category.create", $sitemap_id)
            ],
            /*[
                "key"=>"page_editor",
                "text"=>$page->detail->name ?: "<yeni sayfa>",
                "icon"=>"file",
                "href"=>"javascript:void(0);"
            ],*/
        ];
        $breadcrumb = Content::getBreadcrumb($crumbs);
        return view("ContentPanel::categories.category.create", compact('sitemap', 'sitemap_id',  'data', 'type','metaset', 'breadcrumb'));
    }

    public function store(Request $request, Category $category)
    {
        return CategoryEngine::ajaxStore($request, $category);
    }

    public function delete(Request $request)
    {
        $categoryId = $request->get('id');
        $sub = $request->get('sub');

        $category = Category::with("details")->find($categoryId);

        if($category) {
            if($sub == 1) {
                $category->children()->delete();
            } else {
                $category->children()->update(
                    [
                        'category_id' => $category->category_id,
                        'lft' => null,
                        'rgt' => null,
                        'depth' => $category->depth,
                    ]
                );
            }

            if ($category->delete()) {
                foreach($category->details as $detail){
                    $detail->delete();
                }
                return 1;
            }
        }
        return 0;
    }

    public function orderSave($sitemap_id,Request $request)
    {

        $websiteId = session("panel.website.id");

        if(! activeUserCan([
            "content.websites.website$websiteId.sitemaps.sitemap$sitemap_id.categories.reorder",
            "content.websites.website$websiteId.sitemaps.categories.reorder",
            "content.websites.sitemaps.categories.reorder"
        ])
        ){
            return rejectResponse();
        }

        if (CategoryEngine::orderSave($request)) {
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
        }
    }


    public function criteriaSave($sitemap_id,Request $request)
    {
        $websiteId = session("panel.website.id");

        if(! activeUserCan([
            "content.websites.website$websiteId.sitemaps.sitemap$sitemap_id.criterias.reorder",
            "content.websites.website$websiteId.sitemaps.criterias.reorder",
            "content.websites.sitemaps.criterias.reorder"
        ])
        ){
            return rejectResponse();
        }

        if (CategoryEngine::criteriaSave($request)) {
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
        }
    }


    public function propertySave($sitemap_id,Request $request)
    {
        $websiteId = session("panel.website.id");

        if(! activeUserCan([
            "content.websites.website$websiteId.sitemaps.sitemap$sitemap_id.properties.reorder",
            "content.websites.website$websiteId.sitemaps.properties.reorder",
            "content.websites.sitemaps.properties.reorder"
        ])
        ){
            return rejectResponse();
        }

        if (CategoryEngine::propertySave($request)) {
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_DANGER_MESSAGE));
        }
    }


    public function getRelatedCriteriasOfCategories(Request $request)
    {
        $req = $request->all();

        $category_ids = $req["category_ids"] ?? [];

        /*if (!is_array($category_ids) || !count($category_ids)) {
            return response()->json([]);
        }

        $ids = CategoryCriteria::whereIn(self::CATEGORY_ID,$category_ids)->select("criteria_id")->get()->pluck("criteria_id")->toArray();*/

        $ids = DataSourceEngine::getDataSource("Content","CriteriaIdsOfCategories",["category_ids"=>$category_ids])->getData();

        return response()->json($ids);
    }

    public function getRelatedCriteriasOfCategoriesFull(Request $request)
    {
        $req = $request->all();

        $category_ids = $req["category_ids"] ?? [];

        /*if (!is_array($category_ids) || !count($category_ids)) {
            return response()->json([]);
        }

        $ids = CategoryCriteria::whereIn(self::CATEGORY_ID,$category_ids)->select("criteria_id")->get()->pluck("criteria_id")->toArray();*/

        $ids = DataSourceEngine::getDataSource("Content","CriteriasOfCategories",["category_ids"=>$category_ids])->getData();


        return response()->json($ids);
    }


    public function getRelatedPropertiesOfCategories(Request $request)
    {
        $req = $request->all();

        $category_ids = $req["category_ids"] ?? null;

        if (!is_array($category_ids) || !count($category_ids)) {
            return response()->json([]);
        }

        $property_ids = CategoryProperty::whereIn(self::CATEGORY_ID, $category_ids)->select("property_id")->get()->pluck("property_id")->toArray();

        return response()->json($property_ids);

    }

}
