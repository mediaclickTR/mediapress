<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMdisksTable extends Migration
{
    public const MDISKS = 'mdisks';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable(self::MDISKS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            $schema->create(self::MDISKS, function (Blueprint $table) {
                $table->increments('id');
                $table->string('diskkey')->unique();
                $table->string('name');
                $table->string('driver');
                $table->string('root');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if( Schema::hasTable(self::MDISKS)) {
            Schema::dropIfExists(self::MDISKS);
        }
    }
}
