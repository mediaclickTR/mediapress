@extends('MPCorePanel::inc.app')
@section('content')

    @push("styles")
        <!-- page specific resources-->
        <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/dashboard.css') !!}"/>
        <!-- page specific resources-->
    @endpush

    <div class="container-fluid">
        <div class="page">
            <div class="title float-left">
                {!! isset($title) && $title ?'<i class="fa fa-exclamation-triangle"></i> '.$title : "" !!}
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col">
                        <p class="text-danger">
                            {!! isset($message) && $message ?'<i class="fa fa-exclamation-triangle"></i> '.$message : "" !!}
                        </p>
                        @include("MPCorePanel::inc.errors")
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@push("scripts")
    <script>

    </script>
@endpush
