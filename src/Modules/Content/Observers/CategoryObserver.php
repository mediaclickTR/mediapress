<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 21.03.2019
     * Time: 17:45
     */

    namespace Mediapress\Modules\Content\Observers;

    use Mediapress\Modules\Content\Models\Category;
    use Mediapress\Modules\Content\Models\CategoryCriteria;
    use Mediapress\Modules\Content\Models\CategoryPage;
    use Mediapress\Modules\Content\Models\CategoryProperty;
    use Mediapress\Modules\Content\Models\CriteriaPage;
    use Mediapress\Modules\Content\Models\Page;
    use Illuminate\Support\Facades\Cache;
    use DB;

    class CategoryObserver
    {
        public function updated(Category $object)
        {
            Cache::flush();
        }
        public function deleting(Category $object) {
            if ($object->isForceDeleting()) {
                $object->details()->forceDelete();
            }


            $object->details()->delete();

            if($object->extras->isNotEmpty()) {
                $object->extras()->delete();
            }

            CategoryPage::where('category_id', $object->id)->delete();
            CategoryCriteria::where('category_id', $object->id)->delete();
            CategoryProperty::where('category_id', $object->id)->delete();

            Cache::flush();
        }


    }
