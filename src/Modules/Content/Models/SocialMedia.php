<?php


namespace Mediapress\Modules\Content\Models;

use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class SocialMedia extends Model
{
    use CacheQueryBuilder;
    protected $table = 'social_media';

    public $timestamps = true;

    protected $dates = ["deleted_at"];

    protected $fillable = ["publish", "order","admin_id", "name", "link", "icon","group_id", "status","other_platform","website_id","lang_id"];

    public $casts = [
        'link' => 'array',
    ];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function website(){

        return $this->belongsTo(Website::class);
    }

}
