@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">{!! trans("ContentPanel::panelmenu.panelmenus") !!}</div>
                @if(count($panelmenus)>0)
                    @if(!empty($queries))
                        <a href="{!! route("Content.panelmenus.index") !!}" class="filter-block">
                            <i class="fa fa-trash"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                        </a>
                    @endif
                @endif
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{!! route('Content.panelmenus.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
        </div>
        <div class="p-4 float-left w-100">
            <div class="clearfix"></div>
            @if(count($panelmenus)<=0)
                <div class="alert alert-warning">{!! trans("MPCorePanel::general.nodata") !!}</div>
                @else
                <table>
                    <thead>
                    <tr>
                        <th>{!! trans("MPCorePanel::general.id") !!}</th>
                        <th>{!! trans("ContentPanel::panelmenu.name") !!}</th>
                        <th>{!! trans("MPCorePanel::general.actions") !!}</th>
                    </tr>
                    </thead>
                    <tr>
                    </tr>
                    @foreach($panelmenus as $key=>$panelmenu)
                        <tr>
                            <td>{!! $panelmenu->id !!}</td>
                            <td>{!! $panelmenu->name !!}</td>
                            <td>
                                <a href="{!! route('Content.panelmenus.edit', $panelmenu->id) !!}" class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{!! route("Content.panelmenus.details", $panelmenu->id) !!}" class="mr-2" title="{!! trans("MPCorePanel::general.detail") !!}">
                                    <i class="fa fa-bars"></i>
                                </a>
                                <a href="{!! route('Content.panelmenus.delete', $panelmenu->id) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                                    <i class="fa fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @if(count($panelmenus)>0)
                <div class="float-left">
                    {!! $panelmenus->links() !!}
                </div>
                <div class="float-right">
                    <select onchange="locationOnChange(this.value);">
                        <option value="#">{!! trans('MPCorePanel::general.select.list.size') !!}</option>
                        <option  {!! requestHasAndEqual("list_size",15) ? "selected" : '' !!} value="{!! route("Content.panelmenus.index", array_merge($queries, ["list_size"=>"15"])) !!}">15</option>
                        <option  {!! requestHasAndEqual("list_size",25) ? "selected" : '' !!} value="{!! route("Content.panelmenus.index", array_merge($queries, ["list_size"=>"25"])) !!}">25</option>
                        <option  {!! requestHasAndEqual("list_size",50) ? "selected" : '' !!} value="{!! route("Content.panelmenus.index", array_merge($queries, ["list_size"=>"50"])) !!}">50</option>
                        <option  {!! requestHasAndEqual("list_size",100) ? "selected" : '' !!} value="{!! route("Content.panelmenus.index", array_merge($queries, ["list_size"=>"100"])) !!}">100</option>
                    </select>
                </div>
                @endif
            @endif

        </div>
    </div>
@endsection
