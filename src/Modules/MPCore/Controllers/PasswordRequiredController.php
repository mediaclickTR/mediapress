<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Http\Request;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\BaseController;

class PasswordRequiredController extends BaseController
{
    public function main()
    {
        return redirect()->to("/urlAuth?next=" . mediapress("next").'&lg='.mediapress("activeLanguage")->code);
    }

    public function index(Request $request)
    {

        app()->setLocale($request->lg);
        $next = $request->next;
        $code = $request->code;
        if(!$next){
            abort(404);
        }
        if (file_exists(resource_path('views/web/inc/urlAuth.blade.php'))) {
            return response()->view("web.inc.urlAuth", compact('next','code'),401);
        }
        return response()->view("ContentPanel::inc.urlAuth", compact('next','code'),401);


    }

    public function check(Request $request,Mediapress $mediapress)
    {
        $mediapress->next = request("next", null);
        if (!$mediapress->next) {
            throw new \Exception("Next page required");
        }
        app()->setLocale($request->lg);
        if ($mediapress->loginForUrl($mediapress->next, request("password"))) {
            return redirect()->to($mediapress->next);
        } else {
            return redirect()->to("/urlAuth?next=".mediapress("next").'&lg='.mediapress("activeLanguage")->code)->withErrors(trans("ContentPanel::page.password_invalid"));
        }
    }
}
