<?php

namespace Mediapress\Modules\Entity;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Mediapress\Modules\Entity\Facades\Entity;

class EntityServiceProvider extends ServiceProvider
{
    public const ENTITY = "Entity";
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $module_name = self::ENTITY;
    protected $namespace= 'Mediapress\Modules\Entity';

    public function boot()
    {
        Parent::boot();
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel', $this->module_name.'Panel');
        $this->publishActions(__DIR__);

        $files = $this->app['files']->files(__DIR__ . '/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias(self::ENTITY, Entity::class);
        app()->bind(self::ENTITY, function() {
            return new \Mediapress\Modules\Entity\Entity;
        });
    }
}
