<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("website_id")->unsigned();
            $table->string("name");
            $table->text("devices")->nullable();
            $table->boolean("tablet_auto")->default(true);
            $table->boolean("mobile_auto")->default(true);
            $table->tinyInteger("status")->default(1);
            $table->enum("type",['image','video'])->default('image');
            $table->text("desktop_options")->nullable();
            $table->text("tablet_options")->nullable();
            $table->text("mobile_options")->nullable();
            $table->boolean("language_support")->default(true);
            $table->text("revolution_options")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        $schema->create('scenes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("slider_id")->unsigned();
            $table->integer("time")->nullable();
            $table->integer("order")->default(1);
            $table->tinyInteger("status")->default(1);
            $table->tinyInteger("admin_id")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        $schema->create('scene_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("scene_id")->unsigned();
            $table->integer("language_id")->nullable();
            $table->text("titles")->nullable();
            $table->text("buttons")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
        Schema::dropIfExists('scenes');
        Schema::dropIfExists('scene_details');
    }
}
