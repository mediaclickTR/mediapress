<?php

namespace Mediapress\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*
        'Mediapress\Events\ActivityLogged' => [
            'Mediapress\Listeners\ActivityLoggedListener',
        ],
        */

        'Illuminate\Cache\Events\CacheHit' => [
            'Mediapress\Listeners\LogCacheHit',
        ],

        'Illuminate\Cache\Events\CacheMissed' => [
            'Mediapress\Listeners\LogCacheMissed',
        ],

        'Illuminate\Cache\Events\KeyForgotten' => [
            'Mediapress\Listeners\LogKeyForgotten',
        ],

        'Illuminate\Cache\Events\KeyWritten' => [
            'Mediapress\Listeners\LogKeyWritten',
        ],
    ];
}
