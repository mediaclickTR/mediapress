<?php
return [

    "interaction" => [
        'name' => 'InteractionPanel::auth.sections.interaction_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [

            "analytics" => [
                'name' => 'InteractionPanel::auth.sections.analytics',
                'name_affix' => "",
                'item_model' => "",
                'item_id' => "*",
                "actions" => [
                    'auth' => [
                        'name' => "MPCorePanel::auth.actions.auth",
                    ],
                    'view' => [
                        'name' => "MPCorePanel::auth.actions.view",
                    ],
                ],
                'settings' => [
                    "auth_view_collapse" => "in"
                ],
            ],
        ]
    ]
];
