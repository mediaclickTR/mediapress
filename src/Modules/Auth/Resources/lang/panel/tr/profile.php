<?php

return [
    "page_title" => "Profil Sayfası",
    "information" => "Bilgilerim",
    "security" => "Güvenlik",
    "google_authenticator" => "Google Authenticator (2FA)",
    "active" => "Aktif",
    "pasive" => "Pasif",
    "activate" => "Aktif Et",
    "deactivate" => "İptal Et",
    "google2fa_warning" => "Google 2FA Uyarısı Göster"
];
