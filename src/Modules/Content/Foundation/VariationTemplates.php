<?php

namespace Mediapress\Modules\Content\Foundation;

use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Website;

class VariationTemplates
{
    public const COUNTRY_GROUPS = "country_groups";
    public const LANGUAGES = "languages";

    public function variations($object)
    {
        $data = [];
        if (is_a($object, Website::class)) {
            $object->load(["countryGroups.languages", "countryGroups.countries"]);
            $data = [$object->id => ["id" => $object->id, "name" => $object->name]];
            foreach ($object->countryGroups as $cg) {
                foreach ($cg->languages as $lw) {
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["id"] = $cg->id;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["code"] = $cg->code;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["list_title"] = $cg->list_title;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["title"] = $cg->title;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id][self::LANGUAGES][$lw->id] = [
                        "id" => $lw->id, "code" => $lw->code, "name" => $lw->name, "flag" => $lw->flag
                    ];
                    foreach ($cg->countries as $country) {
                        $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["countries"][$country->id] = [
                            "id" => $country->id, "code" => $country->code, "name" => $country->native
                        ];
                    }

                    if ($cg->code == "gl") {
                        $data[$object->id]["default_country_group_id"] = $cg->id;
                        $data[$object->id][self::LANGUAGES][$lw->id] = [
                            "id" => $lw->id,
                            "code" => $lw->code,
                            "name" => $lw->name,
                            "flag" => $lw->flag
                        ];
                        if ($lw->default) {
                            $data[$object->id]["default_language"] = $lw->id;
                        }
                    }
                }
            }
        } else if (is_a($object, Sitemap::class)) {
            foreach ($object->websites as $website) {
                $data[$website->id] = $this->variations($website)[$website->id];
            }
        }
        return $data;
    }

    public function variationStatus($object)
    {
        $data = [];
        if (is_a($object, Website::class)) {
            $object->load(["countryGroups.languages", "countryGroups.countries"]);
            $data = [$object->id => ["id" => $object->id, "name" => $object->name]];
            foreach ($object->countryGroups as $cg) {
                foreach ($cg->languages as $lw) {
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["id"] = $cg->id;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["code"] = $cg->code;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["title"] = $cg->title;
                    $data[$object->id][self::COUNTRY_GROUPS][$cg->id][self::LANGUAGES][$lw->id] = [
                        "id" => $lw->id, "code" => $lw->code, "name" => $lw->name, "flag" => $lw->flag
                    ];
                    foreach ($cg->countries as $country) {
                        $data[$object->id][self::COUNTRY_GROUPS][$cg->id]["countries"][$country->id] = [
                            "id" => $country->id, "code" => $country->code, "name" => $country->native
                        ];
                    }

                    if ($cg->code == "gl") {
                        $data[$object->id]["default_country_group_id"] = $cg->id;
                        $data[$object->id][self::LANGUAGES][$lw->id] = [
                            "id" => $lw->id,
                            "code" => $lw->code,
                            "name" => $lw->name,
                            "flag" => $lw->flag
                        ];
                        if ($lw->default) {
                            $data[$object->id]["default_language"] = $lw->id;
                        }
                    }
                }
            }
        } else if (is_a($object, Sitemap::class)) {
            foreach ($object->websites as $website) {
                $data[$website->id] = $this->variations($website)[$website->id];
            }
        } else if (is_a($object, Page::class)) {
            $data = \Arr::first($this->variations($object->sitemap));
            $f = $object->details->pluck("language_id", "language_id")->toArray();
            $diff = array_diff_key($data[self::LANGUAGES], $f);
            $data["creatable"] = $diff;
            $data["existing"] = array_keys($f);
            return $data;
        }
        return $data;

    }
}
