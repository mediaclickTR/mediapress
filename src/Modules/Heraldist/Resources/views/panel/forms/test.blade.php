@extends('HeraldistPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>{!! trans("HeraldistPanel::forms.forms") !!}</h1>
        @foreach($forms as $field)
            @if(array_key_exists("group_key",$field["options"]))
                <div class="form-group col-xs-12 col-sm-12 col-xs-12">
                    <p>{!! langPart($field["options"]["group_key"]) !!}</p>
                </div>
            @endif
            <div {!! $field["div_attr"] !!}>
                @if($field["type"] == "text")
                    <a @if(array_key_exists("href",$field['options'])) href="{!! $field['options']['href'] !!}"
                       @endif @if(array_key_exists("onclick",$field['options'])) onclick="{!! $field['options']['onclick'] !!}" @endif>{!! langPart($field['values']) !!}</a>
                @else
                    <label>{!! langPart($field['label']) !!}</label>
                    @if($field["type"] == "radio")
                        @foreach($field["values"][0] as $key=>$value)
                            <div class="checkbox">
                                <label for="{!! $field['name'].'.'.$key !!}">{!! langPart($value) !!}
                                    <input id="{!! $field['name'].'.'.$key !!}" type="radio"
                                           name="{!! $field['name'] !!}"
                                           id="{!! $key !!}">
                                </label>
                            </div>
                        @endforeach
                    @elseif($field["type"] == "textarea")
                        <textarea name="{!! $field['name'] !!}"
                                  class="{!! str_replace('form-control','',$field['options']['class']) !!}"
                                  cols="30" rows="3">{!! old($field['name']) !!}</textarea>
                    @else
                        <input type="text" name="{!! $field['name'] !!}" value="{!! old($field['name']) !!}"
                               class="{!! str_replace('form-control','',$field['options']['class']) !!}"/>
                    @endif

                    @if(array_key_exists("i",$field["options"]))
                        <i>{!! langPart($field["options"]["i"]) !!}</i>
                    @endif
                @endif
            </div>
        @endforeach
    </div>
@endsection
