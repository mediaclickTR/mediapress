<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    class UpdateUrlsTableRemovePasswordColumn extends Migration
    {

        public function up()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('urls')) {
                $schema->table('urls', function (Blueprint $table) {
                    $table->dropColumn("password");
                });
            }
        }

        public function down()
        {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
            if ($schema->hasTable('urls')) {
                $schema->table('urls', function (Blueprint $table) {
                    $table->string("password")->nullable();
                });
            }
        }
    }
