<?php

namespace Mediapress\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Support\Database\CacheQueryBuilder;

class Slot extends Model
{
    use CacheQueryBuilder;
    protected $table = 'slots';

    protected $primaryKey = 'id';
    protected $fillable = ['slot', 'module', 'namespace'];

    public function module()
    {
        return $this->hasOne(Module::class, "slot_id")->where("status",1);
    }
}
