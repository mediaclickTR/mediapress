@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("ContentPanel::seo.robots.page_title") !!}</div>
        <div class="clearfix"></div>
        <br/>

        <form action="" method="post">
            @csrf
            <div id="first-section" class="form-row mb-3">
                <div class="col">
                    <div class="btn-group-toggle float-right ml-1" data-toggle="buttons">
                        <label class="sitemap btn btn-outline-secondary"
                               title="{!! trans("ContentPanel::seo.sitemap.title") !!}">
                            <input type="checkbox" checked
                                autocomplete="off">{!! trans("ContentPanel::seo.sitemap") !!}</label>
                    </div>
                    <div class="btn-group-toggle float-right ml-1" data-toggle="buttons">
                        <label class="website btn btn-outline-secondary"
                               title="{!! trans("ContentPanel::seo.sitemap.title") !!}">
                            <input type="checkbox" checked
                                autocomplete="off">{!! trans("ContentPanel::seo.robots.index_website") !!}</label>
                    </div>
                </div>
            </div>

            <div class="form-row mt-3">
                <div class="col">
                    <textarea class="form-control" name="robot" rows="10">{!! settingSun('content.robot') !!}</textarea>
                </div>
            </div>

            <div class="form-row mt-2">
                <div class="col">
                    <button type="submit"
                            class="btn btn-primary float-right">{!! trans("MPCorePanel::general.save") !!}</button>
                </div>
            </div>
        </form>

    </div>
@endsection

@push('styles')
    <style>
        .btn-outline-success {
            border-color: #28a745 !important;
        }

        .btn-outline-danger {
            border-color: #dc3545 !important;
        }

        .btn-outline-secondary {
            border-color: #6c757d !important;
        }

        .active [data-add] {
            display: none;
        }

        .passive [data-add] {
            border-radius: 0 .25rem .25rem 0 !important;

        }

        .passive [data-remove] {
            display: none;
        }
    </style>
@endpush

@push('scripts')
    <script>

        $(document).ready(function () {
            var robot = $('textarea').val();
            var sml = `{{ url('/sitemap.xml') }}`;

            var defaultAllowText = `User-Agent: * \nAllow: /`
            var defaultDisallowText = `User-Agent: * \nDisallow: /`


            checkWebsite();
            checkSitemap();

            $('textarea').on('keyup', function () {
                checkWebsite();
                checkSitemap();
            })

            $('.website').on('click', function () {
                if ($(this).hasClass('active')) {
                    robot = defaultDisallowText;
                } else {
                    robot = defaultAllowText;
                }

                if ($('.sitemap').hasClass('active')) {
                    robot += "\n\n\nSitemap: " + sml;
                }

                $('textarea').val(robot);
            });

            $('.sitemap').on('click', function () {
                if ($(this).hasClass('active')) {
                    robot = robot.replace("\n\n\nSitemap: " + sml, "");
                } else {
                    robot += "\n\n\nSitemap: " + sml;
                }
                $('textarea').val(robot);
            })
        })

        function checkWebsite() {
            var robot = $('textarea').val();
            var websiteDisallow = getMatches(robot, /^[D|d]isallow: \/$/gm, 1);
            var websiteAllow = getMatches(robot, /^[A|a]llow: \/$/gm, 1);
            if (websiteAllow.length !== 0) {
                $('.website').addClass('active');
            } else if(websiteDisallow.length !== 0) {
                $('.website').removeClass('active');
            }
        }

        function checkSitemap() {
            var robot = $('textarea').val();
            var $sitemapUrl = getMatches(robot, /^[S|s]itemap:\s.*?(.*?)$/gm, 1);
            if ($sitemapUrl.length === 0) {
                $('.sitemap').removeClass('active');
            } else {
                $('.sitemap').addClass('active');
            }
        }

        function getMatches(string, regex, index) {
            var matches = [];
            var match;
            while (match = regex.exec(string)) {
                matches.push(match[index]);
            }
            return matches;
        }
    </script>
@endpush
