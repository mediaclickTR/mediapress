<?php
return [
    "mpcore" => [
        'name' => 'MPCorePanel::auth.sections.mpcore_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [
            "userlogs" => [
                'name' => 'MPCorePanel::auth.sections.userlogs',
                'name_affix' => "",
                'item_model' => \Mediapress\Modules\MPCore\Models\UserActionLogs::class,
                'item_id' => "*",
                "actions" => [
                    "view" => [
                        "name" => "MPCorePanel::auth.actions.view"
                    ]
                ],
                'settings' => [
                    "auth_view_collapse" => "out"
                ],
            ],
            "translations" => [
                'name' => 'MPCorePanel::auth.sections.translations',
                'name_affix' => "",
                'item_model' => "",
                'item_id' => "*",
                "actions" => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                ]
            ],
            "panel_menus" => [
                'name' => 'ContentPanel::auth.sections.panel_menus',
                'item_model' => \Mediapress\Modules\Content\Models\PanelMenu::class,
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                        'variables' => []
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                        'variables' => []
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update"
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'subs' => [
                ],
            ],
            /*"modules" => [
                'name' => 'MPCorePanel::auth.sections.modules',
                'name_affix' => "",
                'item_model' => "",
                'item_id' => "*",
                "actions" => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'update_level1_settings' => [
                        'name' => "MPCorePanel::auth.actions.update_level1_settings",
                    ],
                    'update_level2_settings' => [
                        'name' => "MPCorePanel::auth.actions.update_level2_settings",
                    ],
                    'update_level3_settings' => [
                        'name' => "MPCorePanel::auth.actions.update_level3_settings",
                    ],
                ],
                "subs"=>[],
                "variations" => function ($root_item) {
                    $modules = \Mediapress\Models\Module::all();
                    $sets = [];
                    foreach ($modules as $module) {
                        $set = [];
                        $set["name"] = 'MPCorePanel::auth.sections.module';
                        $set["name_affix"] = $module->name;
                        $set['item_model'] = \Mediapress\Models\Module::class;
                        $set['item_id'] = $module->id;
                        $set['actions'] = [
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'update_level1_settings' => [
                                'name' => "MPCorePanel::auth.actions.update_level1_settings",
                            ],
                            'update_level2_settings' => [
                                'name' => "MPCorePanel::auth.actions.update_level2_settings",
                            ],
                            'update_level3_settings' => [
                                'name' => "MPCorePanel::auth.actions.update_level3_settings",
                            ],
                        ];
                        $set['settings'] = [
                            "auth_view_collapse" => "out"
                        ];
                        $set['data'] = [
                            "is_root" => false,
                            "is_variation" => true,
                            "is_sub" => false,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ];
                        $set['subs'] = $root_item['subs'];
                        data_set($set['subs'], '*.data.descendant_of_variation', true);

                        $sets['module' . $module->id] = $set;
                    }

                    return $sets;
                }
            ]*/
        ],
        'variations_title' => "",
        'variations' => []
    ]
];
