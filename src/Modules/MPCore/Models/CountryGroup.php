<?php

namespace Mediapress\Modules\MPCore\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\MPCore\Models\Country;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Support\Database\CacheQueryBuilder;

class CountryGroup extends Model
{
    use CacheQueryBuilder;
    use SoftDeletes;

    protected $table = 'country_groups';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "code",
        "title",
        "list_title",
        "owner_type",
        "owner_id",
    ];

    public function owner(){
        return $this->morphTo();
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class,'country_group_country','country_group_id','country_code','id','code');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

    public function defaultLanguage() {
        return $this->belongsToMany(Language::class)->wherePivot('default', 1)->first();
    }

}
