<?php
namespace Mediapress\Modules\Content\Foundation;
use Mediapress\AllBuilder\Facades\AllBuilder as AllBuilderEngine;
use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\BackupData;
use Mediapress\Foundation\ClusterArray;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;

class CategoryEngine
{
    #region Ajax

    public const CATEGORY_ID = "category_id";
    public const CATEGORIES = "Categories";
    public const CONTENT = "Content";
    public const MEDIAPRESS = "Mediapress";
    public const APP = "App";
    public const SITEMAP = "Sitemap_";
    public const SITEMAPS = "Sitemaps";
    public const DETAIL = 'detail';
    public const CHILDREN = 'children';
    public const PAGE_ID = "page_id";

    public function ajaxCreate(Sitemap $sitemap = null, Category $parent = null, Category $edit = null)
    {

        if ($edit == null) {
            $preCreate = Category::preCreate(["sitemap_id" => $sitemap->id, self::CATEGORY_ID => $parent ? $parent->id : null]);
            foreach ($sitemap->details as $detail) {
                $preCreate->details()->create(['language_id' => $detail->language_id, 'country_group_id' => $detail->country_group_id]);
            }
            $title = $parent ? trans("ContentPanel::categories.create_category") : trans("ContentPanel::categories.add_main_category");
        } else {
            Content::createUnexistentDetailsOfParent($edit);
            $title = $edit->detail->name ?? "";
            $title = ($title ? $title ." - ": ""). trans("ContentPanel::categories.edit_category");
            $preCreate = $edit;
        }

        $renderable_class = Content::getCategoryRenderableClassPath($preCreate,true);
        //return dd($renderable_class);
        //$renderable_class = AllBuilderEngine::getBuilderClassPath(self::CATEGORIES, self::CONTENT, [self::APP, self::MEDIAPRESS], [self::SITEMAP . $preCreate->sitemap_id, self::SITEMAPS]);

        /** @var BuilderRenderable $renderable */
        $renderable = new $renderable_class(["category" => $preCreate]);

        if (!is_a($renderable, BuilderRenderable::class)) {
            return "<strong><code>$renderable_class</code></strong> <br/>bir \n<br/><strong><code>" . BuilderRenderable::class . "</code></strong>\n<br/> türevi olmalıydı ama değil.";
        }


        return view("ContentPanel::categories.category.ajax", compact("renderable", "title"));
    }

    public function ajaxStore(Request $request, Category $category)
    {



        $renderable_class = Content::getCategoryRenderableClassPath($category,true);

        $renderable = new $renderable_class(["category" => $category]);

        $storer_class = Content::getCategoryStorerClassPath($category);

        $storer = new $storer_class($renderable, $request);
        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $storer->setMainObjectData($category);

        $storer->store();

        //----------------- SÜRÜMLENDİRME İÇİN EKLENEN ----------------
        $object_to_log = $storer->getMainObjectFromBucket();
        $serialized = serialize($object_to_log);

        $backup = new BackupData();
        $backup->store($storer);


        /* Order Save */
        $data = collect();
        $data->nestable_output = $this->nestableGetList($category->sitemap_id, Category::class, 'category_id');
        $this->orderSave($data);

        $errors = $storer->getErrors();
        $warnings = $storer->getWarnings();

        $redirect = redirect(route('Content.categories.category.create',$category->sitemap_id));

        if(count($warnings)){
            $redirect->with("warning",$warnings);
        }

        if(count($errors)){
            $redirect->withErrors($errors);
        }

        return $redirect;
    }

    #endregion

    public function orderSave($data)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = self::CATEGORY_ID;
        $nestable = $data->nestable_output;

        $array = $this->buildTree(json_decode($nestable), $cluster->table_id);

        try {
            foreach ($cluster->cluster($array) as $category) {
                Category::where("id", $category['id'])->update($category);
            }
            return true;
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function criteriaSave($data)
    {
        $save = Category::find($data->category_id)->criterias()->sync($data->criterias);
        if ($save){
            return true;
        }
        return false;
    }

    public function propertySave($data)
    {
        $save = Category::find($data->category_id)->properties()->sync($data->properties);
        if ($save){
            return true;
        }
        return false;
    }

    #region Helper Methods
    public function buildTree($array, $parent_column = "category_id")
    {
        $ls = null;
        foreach ($array as $parent) {
            if ($parent->id == 0) {
                continue;
            }
            if (isset($parent->children)) {
                $ls[] = [
                    "id" => $parent->id, $parent_column => 0, self::CHILDREN => $this->children($parent->children, $parent_column, $parent->id)
                ];
            } else {
                $ls[] = ["id" => $parent->id, $parent_column => 0];
            }
        }
        return $ls;
    }

    public function nestableGetList($sitemap_id, $model, $parent_id)
    {
        $cluster = new ClusterArray();
        $cluster->table_id = $parent_id;

        // $this->website->defaultLanguage()->id
        $default_language = session("panel.active_language.id");
        $zone_admin = false;
        if( auth()->user()->country_group != NULL ){
            $zone_admin = auth()->user()->country_group;
        }

        $list = $model::whereSitemapId($sitemap_id)->where('status','<>',Category::PREDRAFT)->orderBy("lft")
            ->when($zone_admin, function($query)use($zone_admin, $default_language){
                return $query->whereHas('details', function($q)use($zone_admin,$default_language){
                    return $q->where('country_group_id', $zone_admin)->where('language_id', $default_language)->where('name', '<>', '');
                });
            })
            ->with([
                'detail' => function($query)use($zone_admin,$default_language){
                    if($zone_admin){
                        return $query->where('language_id', $default_language)
                            ->where('country_group_id', $zone_admin)
                            ->where('name', '<>', '');
                    }
                    if(!$zone_admin && $default_language == 616){
                        return $query->where('language_id', 616)
                            ->where('name', '<>', '');
                    }
                }
            ])
            ->get(["id", $parent_id, 'status']);

        $list = $list->toArray();

        foreach ($list as $key => $value) {
            if (isset($value[self::DETAIL]['name'])) {
                $list[$key][self::DETAIL]['name'] = str_replace(['"'], ['\"'], $value[self::DETAIL]['name']);
            }
        }

        return str_replace("\\\\", "\\", json_encode($cluster->buildFromDB($list), JSON_UNESCAPED_UNICODE ));
    }

    public function children($array, $parent_column, $parent)
    {
        $childList = null;

        foreach ($array as  $child) {
            if (isset($child->children)) {
                $childList[] = [
                    "id" => $child->id, $parent_column => $parent, self::CHILDREN => $this->children($child->children, $parent_column, $child->id)
                ];
            } else {
                $childList[] = ["id" => $child->id, $parent_column => $parent];
            }
        }

        return $childList;
    }

    private function removeCategoryRecursive($category_id, $sub)
    {
        $id = $category_id;
        $category = Category::whereHas("sitemap", function ($query) {
            $query->where("website_id", session("panel.website")->id);
        })->with(self::CHILDREN)->find($id);

        if ($category) //Session üzerinden yetki kontrolü sağlanıyor
        {
            if (count($category->children)) {
                foreach ($category->children as $child_cat) {
                    $this->removeCategoryRecursive($child_cat->id, $sub);
                }
            }
            if ($sub) {
                $page_ids = CategoryPage::select(self::PAGE_ID)->where(self::CATEGORY_ID, $id)->get()->pluck(self::PAGE_ID);

                foreach ($page_ids as $page) {

                    $category_ids = CategoryPage::select(self::PAGE_ID)->where(self::CATEGORY_ID, $id)->get()->pluck(self::PAGE_ID);
                    if (count($category_ids) > 1) {
                        CategoryPage::where(self::CATEGORY_ID, $id)->where(self::PAGE_ID, $page)->delete();
                    } else {
                        $collect = collect(['id' => $page]);
                        $this->delete($collect);
                    }

                }
            }

            $category->delete();
            objectDeleted($category);

            return [true, trans("MPCorePanel::general.success_message")];

        }
        return [true, trans("MPCorePanel::general.danger_message")];
    }
    #endregion
}
