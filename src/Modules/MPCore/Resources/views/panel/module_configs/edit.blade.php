@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">Modül Özelliği Oluştur</h1>
        <div class="col-md-12">
            <form action="{!! route('MPCore.module_configs.update') !!}" novalidate method="POST">
                @csrf
                <input type="hidden" value="{!! $module_config->id !!}" name="id">
                <div class="form-group rel focus">
                    <div class="tit" for="">Modül Seçimi</div>
                    <select name="module_id" required>
                        <option value="">Modül Seçiniz</option>
                    @foreach($modules as $module)
                        <option @if($module->id == $module_config->module_id) selected @endif value="{!! $module->id !!}">{!! $module->name !!}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group rel focus">
                    <div class="tit">Özellik Adı</div>
                    <input type="text" value="{!! $module_config->config_name !!}" name="config_name">
                </div>
                <div class="form-group rel focus">
                    <div class="tit">Özellik Key</div>
                    <input type="text" value="{!! $module_config->key !!}" name="key">
                </div>
                <div class="form-group rel focus">
                    <div class="tit" for="">Form Türü</div>
                    <select class="nice" name="type" id="type">
                        <option value="">Seçim Yapınız</option>
                        <option @if($module_config->type == "text")  selected @endif value="text">Text</option>
                        <option @if($module_config->type == "select")  selected @endif value="select">Select Box</option>
                    </select>
                </div>
                <div class="form-group rel focus" id="ektralar" @if(!$module_config->details) style="display:none;" @endif>
                    <div class="tit">Ektralar (JSON)</div>
                    <textarea style="min-height:200px;" name="details" placeholder="Radio, checkbox veya select seçimlerde JSON tipinde seçimler girilmelidir.">{!! $module_config->details !!}</textarea>
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            $("#type").change(function(){
                var value = $(this).val();
                if(value == "checkbox" || value == "radio" || value == "select"){
                    $("#ektralar").show();
                }
                else{
                    $("#ektralar").hide();
                }
            });
        });
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
    </script>
@endpush

@push("styles")
    <style>
        select{
            width:100% !important;
        }
    </style>
@endpush