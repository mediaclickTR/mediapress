<?php


Route::group(['prefix' => 'CommentStructure', 'middleware' => 'panel.auth', 'as' => 'CommentStructure.'], function () {
    Route::get('/', 'CommentStructureController@index')->name('index');
    Route::get('/create', 'CommentStructureController@create')->name('create');
    Route::post('/store', 'CommentStructureController@store')->name('store');
    Route::get('/edit/{id}', 'CommentStructureController@edit')->name('edit');
    Route::post('/update/{id}', 'CommentStructureController@update')->name('update');
    Route::get('/delete/{id}', 'CommentStructureController@delete')->name('delete');


    Route::group(['prefix' => '{commentStructureId}/Comments', 'middleware' => 'panel.auth', 'as' => 'comments.'], function () {
        Route::get('/', 'CommentController@index')->name('index');
        Route::get('/updateReadStatus', 'CommentController@updateReadStatus')->name('updateReadStatus');
        Route::get('/update/{id}', 'CommentController@update')->name('update');
        Route::get('/delete/{id}', 'CommentController@delete')->name('delete');
    });
});

