<div class="bd-example white p-4 dashboardPage float-left w-100">
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="yellow">
                <a data-toggle="tab" href="#visit-tab" class="active">
                    {{ __("InteractionPanel::dashboard.stats.visits") }}
                    <i class="float-none">{{ number_format($data['counts']['visits'],0,',','.') }}</i>
                </a>
            </li>
            <li class="blue">
                <a data-toggle="tab" href="#session-tab">
                    {{ __("InteractionPanel::dashboard.stats.session") }}
                    <i class="float-none">{{ number_format($data['counts']['sessions'],0,',','.') }}</i>
                </a>
            </li>
            <li class="purple">
                <a data-toggle="tab" href="#cookie-tab">
                    {{ __("InteractionPanel::dashboard.stats.cookie") }}
                    <i class="float-none">{{ number_format($data['counts']['cookies'],0,',','.') }}</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content pt-3">
        <div id="visit-tab" class="tab-pane fade in active show">
            <div id="visit-graph" class="dash"></div>
        </div>
        <div id="session-tab" class="tab-pane fade">
            <div id="session-graph" class="dash"></div>
        </div>
        <div id="cookie-tab" class="tab-pane fade">
            <div id="cookie-graph" class="dash"></div>
        </div>
    </div>
</div>
<script>
  window.graphData.tabs = @json($data['tabs'])
</script>
<script src="{!! asset('vendor/mediapress/js/dashboardTabs.js') !!}"></script>
