<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePopupTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        if (!$schema->hasColumn('popups', 'font')) {
            $schema->table('popups', function (Blueprint $table) {
                $table->string('font')->nullable()->after('condition_pixel');
                $table->string('border_size')->nullable()->after('font');
                $table->string('border_color')->nullable()->after('border_size');
                $table->string('padding_size')->nullable()->after('border_color');
                $table->string('button_bg')->nullable()->after('padding_size');
                $table->string('button_color')->nullable()->after('button_bg');
                $table->string('full_bg')->nullable()->after('button_color');
                $table->string('background')->nullable()->after('full_bg');
                $table->string('color')->nullable()->after('background');
                $table->string('title_size')->nullable()->after('color');
                $table->string('a_color')->nullable()->after('title_size');
                $table->string('place')->nullable()->after('a_color');
                $table->string('button_type')->nullable()->after('place');
                $table->string('button_text')->nullable()->after('button_type');
            });
        }

    }

    public function down()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        if ($schema->hasColumn('popups', 'font')) {
            $schema->table('popups', function (Blueprint $table) {
                $table->dropColumn('font');
                $table->dropColumn('border_size');
                $table->dropColumn('border_color');
                $table->dropColumn('padding_size');
                $table->dropColumn('button_bg');
                $table->dropColumn('button_color');
                $table->dropColumn('full_bg');
                $table->dropColumn('background');
                $table->dropColumn('color');
                $table->dropColumn('title_size');
                $table->dropColumn('a_color');
                $table->dropColumn('place');
                $table->dropColumn('button_type');
                $table->dropColumn('button_text');
            });
        }
    }
}
