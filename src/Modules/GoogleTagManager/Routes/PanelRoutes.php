<?php


Route::group(['middleware' => 'panel.auth'], function () {
    Route::group(['prefix' => 'GoogleTagManager', 'as' => 'admin.GoogleTagManager.'], function () {

        /**
         * Base Modules
         * Include ebulletin, forms, messages
         */


        Route::get('/', ['as' => 'index', 'uses' => 'GoogleTagManagerController@index']);
        Route::get('/connect', ['as' => 'connect', 'uses' => 'GoogleTagManagerController@connect']);
        Route::get('/unconnect', ['as' => 'unconnect', 'uses' => 'GoogleTagManagerController@unconnect']);

        Route::group(['prefix' => 'Google', 'as' => 'google.'], function () {

            Route::get('/OAuth', ['as' => 'oAuth', 'uses' => 'GoogleController@oAuth']);
            Route::get('/login', ['as' => 'login', 'uses' => 'GoogleController@login']);

        });


    });
});
