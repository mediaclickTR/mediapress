<?php

return [
    'yes' => "Evet",
    'no' => "Hayır",
    'select' => "Seçiniz",
    'close' => "Kapat",
    'save' => "Kaydet",
    'active' => "Aktif",
    'passive' => "Pasif",
];
