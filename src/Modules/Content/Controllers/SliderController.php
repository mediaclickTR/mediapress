<?php

namespace Mediapress\Modules\Content\Controllers;

use Illuminate\Support\Facades\Auth;
use Mediapress\Http\Controllers\PanelController as Controller;

use Mediapress\Modules\Content\Facades\CategoryEngine;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryCriteria;
use Mediapress\Modules\Content\Models\CategoryProperty;
use Mediapress\Modules\Content\Models\Property;
use Mediapress\Modules\Content\Models\Sitemap;
use Illuminate\Http\Request;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\MPCore\Facades\MPCore;

class SliderController extends Controller
{

    public const REQUIRED = 'required';
    public const ACCESSDENIED = 'accessdenied';
    public const CONTENT_SLIDER_INDEX = 'Content.slider.index';
    private $rules = [
        'website_id' => self::REQUIRED,
        'name' => self::REQUIRED,
        'type' => self::REQUIRED,
        'text' => self::REQUIRED,
        'button' => self::REQUIRED,
        'screen' => 'required|numeric',
        'width' => 'required_if:screen,3|nullable|numeric',
        'height' => 'required_if:screen,2|required_if:screen,3|nullable|numeric',

    ];

    private $labels = [
        'website_id' => 'Web Sitesi',
        'name' => 'Slider adı',
        'type' => 'Slider tipi',
        'text' => 'Metin Sayısı',
        'screen' => 'Slider Ekranı',
        'width' => 'Genişlik',
        'height' => 'Yükseklik',
        'button' => 'Buton Sayısı',
    ];

    public function index()
    {

        $websiteId = session("panel.website.id");
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.index",
            "content.websites.sliders.index",
        ])){
            return rejectResponse();
        }

        // TODO: Aktif website'a göre listelenmeli

        $sliders = Slider::where('website_id', $websiteId)->get();

        return view('ContentPanel::sliders.index', compact('sliders'));
    }


    public function create()
    {

        $websiteId = session("panel.website.id");
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.create",
            "content.websites.sliders.create",
        ])){
            return rejectResponse();
        }

        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('ContentPanel::sliders.create', compact('websites'));
    }

    public function store(Request $request)
    {

        $websiteId = session("panel.website.id");
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.create",
            "content.websites.sliders.create",
        ])){
            return rejectResponse();
        }

        $this->validate($request, $this->rules, [], $this->labels);

        $data = $request->all();
        $data['admin_id'] = Auth::guard('admin')->user()->id;

        Slider::create($data);

        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_INDEX));
    }

    public function edit($id)
    {

        $slider = Slider::find($id);
        $websiteId = $slider->website_id;
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.update",
            "content.websites.sliders.update",
        ])){
            return rejectResponse();
        }

        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('ContentPanel::sliders.edit', compact('websites', 'slider'));
    }

    public function update($id, Request $request)
    {

        $this->validate($request, $this->rules, [], $this->labels);
        $data = $request->all();
        $slider = Slider::find($id);

        $websiteId = $slider->website_id;
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.update",
            "content.websites.sliders.update",
        ])){
            return rejectResponse();
        }
        foreach ($data as $key => $value) {
            if (mb_substr($key, 0, 1) != '_') {
                $slider->{$key} = $value;
            }
        }

        $slider->save();

        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_INDEX));
    }

    public function delete($id)
    {

        $slider = Slider::find($id);

        $websiteId = $slider->website_id;
        if(!activeUserCan([
            "content.websites.website$websiteId.sliders.delete",
            "content.websites.sliders.delete",
        ])){
            return rejectResponse();
        }

        foreach ($slider->scenes as $scene) {
            $scene->delete();
        }

        $slider->delete();

        \Cache::flush();

        return redirect(route(self::CONTENT_SLIDER_INDEX));
    }
}
