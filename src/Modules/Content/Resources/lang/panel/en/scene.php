<?php

return [
    'title' => ' - Scenes',
    'settings' => 'Settings',
    'text_area' => 'Text Area',
    'video_link' => 'Video Link',
    'video_desc' => 'You need to enter the Video Embed link.',
    'add_scene' => 'Add Scene',
    'edit_scene' => 'Edit Scene',
    'time' => 'Time',
    'desktop' => 'Desktop',
    'tablet' => 'Tablet',
    'mobile' => 'Mobile',
    'image' => 'Image',
    'images' => 'Images',
    'blank_fields_text' => 'A desktop image is added to the fields and languages left blank.',
    'text_field' => 'Text Field',
    'copy_desktop_text' => 'Copy desktop Text',
    'button_text' => 'Button Text',
    'if_you_dont_enter_button_text_p1' => 'If you don\' add button text,',
    'if_you_dont_enter_button_text_p2' => 'button won\'t show up.',
    'link' => 'Link',
    'external_link' => 'External Link',
    'same_page' => 'Same Page',
    'link_scope' => 'Link Scope',



];
