@php
    $request = \Illuminate\Http\Request::capture();

@endphp

@if($request->debugbar==1)

    @php
        $renderer = Debugbar::getJavascriptRenderer();
    @endphp
    {!! $renderer->renderHead() !!}
    {!! $renderer->render() !!}
@endif