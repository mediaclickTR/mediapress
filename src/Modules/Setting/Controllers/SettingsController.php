<?php

namespace Mediapress\Modules\Setting\Controllers;

use Mediapress\Http\Controllers\PanelController as Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mediapress\Facades\Modules\Content;
use Intervention\Image\ImageManagerStatic;
use Mediapress\Modules\MPCore\Facades\FilesEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Modules\MPCore\Models\SettingSun;

class SettingsController extends Controller
{

    public function index()
    {

        if(!activeUserCan(["setting.index"])){return rejectResponse();}


        $breadcrumb_settings = \Mediapress\Modules\MPCore\Models\SettingSun::where('group', 'Breadcrumb')
            ->where('website_id', $this->website->id)
            ->pluck('value', 'key');

        $loginSettings = \Mediapress\Modules\MPCore\Models\SettingSun::where('group', 'Login Limiter')
            ->where('website_id', $this->website->id)
            ->pluck('value', 'key')->toArray();

        return view('SettingPanel::index', compact('breadcrumb_settings', 'loginSettings'));
    }

    public function breadcrumb()
    {

        if(!activeUserCan(["setting.update"])){return rejectResponse();}
        $data = request()->except('_token');

        foreach ($data as $key => $val) {

            SettingSun::updateOrCreate(
                [
                    'website_id' => $this->website->id,
                    'group' => "Breadcrumb",
                    'key' => 'breadcrumb.'.$key,
                ],
                [
                    'value' => $val,
                ]
            );
        }
    }

    public function imageAjax()
    {
        $quality = request()->get('quality');

        $image = ImageManagerStatic::make(public_path("vendor/mediapress//images/image-quality.jpg"))->encode('jpg', $quality);
        $base64 = 'data:image/jpg;base64,' . base64_encode($image);
        $base64size = (int)(strlen(rtrim(('data:image/jpg;base64,' . base64_encode($base64)), '=')) * 3 / 4);
        return ['image' => $base64, 'size' => formatBytes($base64size * 190513 / 254058)];
    }

    public function envAjax()
    {
        if(!activeUserCan(["setting.update"])){return rejectResponse();}
        $quality = request()->get('quality');
        $webp_support = request()->get('webp_status');

        $env = file_get_contents(base_path('.env'));
        if (strpos($env, "MP_QUALITY") === false) {
            $temp = $env . "\nMP_QUALITY=" . $quality . "\n";
        } else {
            $temp = preg_replace('/\nMP_QUALITY=(.*?)\n/', "\nMP_QUALITY=" . $quality . "\n", $env);
        }

        if (strpos($env, "MEDIAPRESS_SUPPORT_WEBP") === false) {
            $temp = $temp . "\nMEDIAPRESS_SUPPORT_WEBP=" . $webp_support . "\n";
        } else {
            $temp = preg_replace('/\nMEDIAPRESS_SUPPORT_WEBP=(.*?)\n/', "\nMEDIAPRESS_SUPPORT_WEBP=" . $webp_support . "\n", $temp);
        }

        file_put_contents(base_path('.env'), $temp);
    }

    public function logo() {

        if(!activeUserCan(["setting.update"])){return rejectResponse();}
        $images = request()->except('_token');

        foreach ($images as $key => $image) {
            $process_id = uniqid();
            $folder_path = date('Y/m');
            $file = $image;
            $diskkey = $request->diskkey ?? 1;
            $save = FilesEngine::easySaveUploadedFile($diskkey, $file, $folder_path, null, $process_id);

            if($save['status'] && isset($save['data']['MFile'])) {
                UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $save['data']['MFile']);

                SettingSun::updateOrCreate(
                    [
                        'website_id' => $this->website->id,
                        'group' => "Logo",
                        'key' => 'logo.'.$key,
                    ],
                    [
                        'value' => $save['data']['MFile']->id,
                    ]
                );
            }
        }

        return redirect()->back();
    }

    public function googleMap() {

        if(!activeUserCan(["setting.update"])){return rejectResponse();}
        $apiKey = request()->get('api_key');

        if($apiKey) {
            SettingSun::updateOrCreate(
                [
                    'website_id' => $this->website->id,
                    'group' => "Google Map",
                    'key' => 'google_map.api_key',
                ],
                [
                    'value' => $apiKey,
                ]
            );
        }

        return redirect()->back();
    }

    public function login() {

        if(!activeUserCan(["setting.update"])){return rejectResponse();}
        $google2fa = request()->get('google2fa');
        $loginLimiter = request()->get('login_limiter');

        $this->setGoogle2faSettings($google2fa);


        $loginLimiter['white_list'] = isset($loginLimiter['white_list']) ? json_encode($loginLimiter['white_list']) : json_encode([]);
        foreach ($loginLimiter as $key => $limiter) {
            SettingSun::updateOrCreate(
                [
                    'website_id' => $this->website->id,
                    'group' => "Login Limiter",
                    'key' => 'login_limiter.' . $key,
                ],
                [
                    'value' => $limiter,
                ]
            );
        }

        return redirect()->back();
    }

    private function setGoogle2faSettings($google2fa)
    {
        $status = $google2fa['status'] == 1 ? 'true' : 'false';
        $required = $google2fa['required'] == 1 ? 'true' : 'false';

        $env = file_get_contents(base_path('.env'));
        if (strpos($env, "MEDIAPRESS_GOOGLE2FA") === false) {
            $temp = $env . "\nMEDIAPRESS_GOOGLE2FA=" . $status . "\n";
        } else {
            $temp = preg_replace('/\nMEDIAPRESS_GOOGLE2FA=(.*?)\n/', "\nMEDIAPRESS_GOOGLE2FA=" . $status . "\n", $env);
        }

        if (strpos($temp, "MEDIAPRESS_GOOGLE2FA_REQUIRED") === false) {
            $temp .= "MEDIAPRESS_GOOGLE2FA_REQUIRED=" . $required . "\n";
        } else {
            $temp = preg_replace('/\nMEDIAPRESS_GOOGLE2FA_REQUIRED=(.*?)\n/', "\nMEDIAPRESS_GOOGLE2FA_REQUIRED=" . $required . "\n", $temp);
        }

        file_put_contents(base_path('.env'), $temp);
    }
}
