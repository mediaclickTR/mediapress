@if(!isset($menu))
    {{ Form::open(['route' => 'Content.menus.store']) }}
@else
    {{ Form::model($menu, ['route' => ['Content.menus.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$menu->id) !!}
@endif
@csrf

<div class="container-fluid">
    <div class="form-group rel">
        {!!Form::hidden('website_id',$current_website->id, [])!!}
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! __("ContentPanel::menu.name") !!}</div>
                {!!Form::text('name', null, ["placeholder"=>__("ContentPanel::menu.name"), "class"=>"validate[required]", "id"=>"name"])!!}
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! __("ContentPanel::menu.slug") !!}</div>
                {!!Form::text('slug', null, ["placeholder"=>__("ContentPanel::menu.slug"), "class"=>"validate[required]", "id"=>"slug"])!!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group tabSelectBox">
                <div class="tit">{!! __("ContentPanel::menu.type") !!}</div>
                {!!Form::select('type', $menu_types, null, ['class' => '', 'required','placeholder'=> __("ContentPanel::general.selection")])!!}
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="tit">{!! __("ContentPanel::menu.sections") !!}</div>
                {!!Form::text('sections', null, ["placeholder"=>__("ContentPanel::menu.sections"), "class"=>"validate[required]"])!!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="tit">{!! __("ContentPanel::menu.status") !!}</div>
                <div class="checkbox">
                    <label>
                        {!! __("ContentPanel::general.status_title.active") !!}
                        <input type="radio" {{ isset($menu) && $menu->status == 1 ? 'checked' : '' }} name="status"
                               value="1">
                    </label>
                    <label>
                        {!! __("ContentPanel::general.status_title.passive") !!}
                        <input type="radio" {{ isset($menu) && $menu->status != 1 ? 'checked' : '' }}  name="status"
                               value="0">
                    </label>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="float-right">
    <button class="btn btn-primary">{!! __("ContentPanel::general.save") !!}</button>
</div>

{{ Form::close() }}

@push('scripts')
    <script>
        var slug = function (str) {
            var trimmed = $.trim(str);
            $slug = trimmed.replace("ı", "i").replace("ö", "o").replace("ü", "u").replace("ş", "s").replace("ğ", "g").replace("ç", "c").replace("Ü", "U").replace("İ", "I").replace("Ö", "O").replace("Ü", "U").replace("Ş", "S").replace("Ğ", "G").replace("Ç", "C").replace("ü", "u").replace(/-+/g, '-').replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, '-').replace(/^-|-$/g, '');
            return $slug.toLowerCase();
        }
        $('#name').keyup(function () {
            var takedata = $('#name').val();
            $('#slug').val(slug(takedata));
        });
    </script>
@endpush
