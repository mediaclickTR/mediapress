<?php

return [
    "modules"=>[
        "mpcore"=>"Mediapress",
        "content"=>"İçerik",
        "heraldist"=>"İletişim",
        "entity"=>"Varlık",
        "auth"=>"Üyelik",
    ],
    "sub_modules"=>[
        "menus"=>"Menüler",
        "sitemaps"=>"Site Yapıları",
        "websites"=>"Web Siteler",
        "categories"=>"Kategoriler",
        "panelmenus"=>"Panel Menüleri",
        "ebulletins"=>"E-Bülten",
        "forms"=>"Formlar",
        "users"=>"Üyeler",
        "entitylist"=>"Üye Listeleri",
        "translate"=>"Dil Değişkenleri",
        "useractionlogs"=>"Kullanıcı Aksiyonları",
        "category"=>"Kategori",
        "criteria"=>"Kriterler",
        "property"=>"Özellikler",
        "admins"=>"Yöneticiler",
        "messages"=>"Mesajlar"
    ],
    "actions"=>[
        "create"=>"Yeni Ekle",
        "store"=>"Kaydet",
        "edit"=>"Düzenle",
        "delete"=>"Sil",
        "update"=>"Güncelle",
        "details"=>"Detay",
        "show"=>"Listele",
        "edit-detail"=>"Detay Düzenle",
        "listdetail"=>"Detay",
    ],
    "additions"=>[
        "mp-admin"=>"Dashboard",
        "list"=>"Liste",
        "seo"=>"SEO",
        "robot"=>"Robot",
        "metas"=>"Metalar",
        "categorycreate"=>"Kategori Yönet"
    ]
];