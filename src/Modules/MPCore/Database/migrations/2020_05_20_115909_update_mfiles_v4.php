<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;;

class UpdateMfilesV4 extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        setlocale(LC_CTYPE, 'en_US.UTF8');
        if ($schema->hasTable('mfiles')) {

            $schema->table('mfiles', function ($table) {
                $table->renameColumn('admin_id', 'admin_id_old');
                $table->renameColumn('created_at', 'created_at_old');
                $table->renameColumn('updated_at', 'updated_at_old');
            });

            //Add a new column with the regular name:
            $schema->table('mfiles', function (Blueprint $table) {
                $table->integer('admin_id')->after('id');
                $table->timestamps();
            });

            //Copy the data across to the new column:
            DB::table('mfiles')->update([
                'admin_id' => DB::raw('admin_id_old'),
                'created_at' => DB::raw('created_at_old'),
                'updated_at' => DB::raw('updated_at_old'),
            ]);

            //Remove the old column:
            $schema->table('mfiles', function (Blueprint $table) {
                $table->dropColumn('admin_id_old');
                $table->dropColumn('created_at_old');
                $table->dropColumn('updated_at_old');
            });




            if(! $schema->hasColumn('mfiles', 'deleted_at')) {
                $schema->table('mfiles', function (Blueprint $table) {
                    $table->softDeletes();
                });
            } else {
                $schema->table('mfiles', function ($table) {
                    $table->renameColumn('deleted_at', 'deleted_at_old');
                });

                DB::table('mfiles')->update([
                    'deleted_at' => DB::raw('deleted_at_old'),
                ]);
                $schema->table('mfiles', function (Blueprint $table) {
                    $table->dropColumn('deleted_at_old');
                });
            }
        }
    }

    public function down()
    {
        //
    }
}
