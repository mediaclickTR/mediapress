<?php

namespace Mediapress\Modules\MPCore\Foundation;

use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Models\Module;
use Mediapress\Modules\MPCore\Models\UserActionLogs;

final class UserActionLog
{
    public const WEBSITE_ID = 'website_id';
    public const USER_ID = 'user_id';
    public const CONTROLLER = 'controller';
    public const ACTION = 'action';
    public const CONTEXT_TYPE = 'context_type';
    public const CONTEXT_ICON = 'context_icon';
    public const CONTENT = 'content';
    public const PRIORITY = 'priority';
    public const PANEL_USER = 'panel.user';
    public const PANEL_WEBSITE = "panel.website";
    public const MODULE_ID = 'module_id';

    /**
     * @param string $controller_and_action
     * @param array $model
     * @param integer $priority|null
     * @return void
     */

    public function create($controller_and_action,$model,$priority=null)
    {
        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $user = Admin::find(session()->get(self::PANEL_USER)->id);
        $website =session(self::PANEL_WEBSITE);
        $table = $model->getTable();
        $content = "'$website->slug' sitesinde '$user->username' adlı kullanıcı, '$table' tablosuna '$model->id' ID'li içeriği ekledi.";
        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $user->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::CONTEXT_TYPE =>"success",
            self::CONTEXT_ICON =>"fa-check",
            self::CONTENT =>$content,
            self::PRIORITY =>$priority,
        ];

        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }

        if (request()->server('HTTP_CF_CONNECTING_IP')) {
            $ip = request()->server('HTTP_CF_CONNECTING_IP');
        } else {
            $ip = request()->server('REMOTE_ADDR');
        }
        if($ip) {
            $data += ['ip' => $ip];
        }


        $model->log()->create($data);
    }

    /**
     * @param string $controller_and_action
     * @param array $model
     * @param integer $priority|null
     * @return void
     */

    public function update($controller_and_action,$model,$priority=null)
    {
        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $user = Admin::find(session()->get(self::PANEL_USER)->id);
        $website =session(self::PANEL_WEBSITE);
        $table = $model->getTable();
        $content = "'$website->slug' sitesinde '$user->username' adlı kullanıcı '$table' tablosunda, '$model->id' ID'li içeriği güncelledi.";
        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $user->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::CONTEXT_TYPE =>"warning",
            self::CONTEXT_ICON =>"fa-edit",
            self::CONTENT =>$content,
            self::PRIORITY =>$priority,
        ];

        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }

        if (request()->server('HTTP_CF_CONNECTING_IP')) {
            $ip = request()->server('HTTP_CF_CONNECTING_IP');
        } else {
            $ip = request()->server('REMOTE_ADDR');
        }
        if($ip) {
            $data += ['ip' => $ip];
        }

        $model->log()->create($data);
    }

    /**
     * @param string $controller_and_action
     * @param array $model
     * @param integer $priority|null
     * @return void
     */

    public function delete($controller_and_action,$model,$priority=null)
    {
        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $user = Admin::find(session()->get(self::PANEL_USER)->id);
        $website =session(self::PANEL_WEBSITE);
        $table = $model->getTable();
        $content = "'$website->slug' sitesinde '$user->username' adlı kullanıcı '$table' tablosundan, '$model->id' ID'li içeriği sildi.";

        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $user->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::CONTEXT_TYPE =>"danger",
            self::CONTEXT_ICON =>"fa-remove",
            self::CONTENT => $content,
            self::PRIORITY =>$priority,
        ];

        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }

        if (request()->server('HTTP_CF_CONNECTING_IP')) {
            $ip = request()->server('HTTP_CF_CONNECTING_IP');
        } else {
            $ip = request()->server('REMOTE_ADDR');
        }
        if($ip) {
            $data += ['ip' => $ip];
        }

        $model->log()->create($data);
    }

    /**
     * @param string $controller_and_action
     * @param array $model
     * @param integer $priority|null
     * @return void
     */

    public function emailSended($controller_and_action,$model,$priority=null)
    {
        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $user = Admin::find(session()->get(self::PANEL_USER)->id);
        $website =session(self::PANEL_WEBSITE);
        $table = $model->getTable();
        $content = "'$website->slug' sitesinde '$user->username' adlı kullanıcı '$table' tablosundan '$model->id' ID'li e-postayı gönderdi.";

        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $user->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::MODULE_ID => null,
            self::CONTEXT_TYPE =>"success",
            self::CONTEXT_ICON =>"fa-envelope-o",
            self::CONTENT =>$content,
            self::PRIORITY =>$priority,
        ];

        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }

        $model->log()->create($data);
    }

    /**
     * @param string $controller_and_action
     * @param array $model
     * @param integer $priority|null
     * @return void
     */

    public function login($controller_and_action,$model,$priority=null)
    {

        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $website =session(self::PANEL_WEBSITE);
        $content = "'$website->slug' sitesinde '$model->username' adlı kullanıcı giriş yaptı.";

        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $model->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::CONTEXT_TYPE =>"info",
            self::CONTEXT_ICON =>"fa-sign-in",
            self::CONTENT =>$content,
            self::PRIORITY =>$priority,
        ];

        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }
        if (request()->server('HTTP_CF_CONNECTING_IP')) {
            $ip = request()->server('HTTP_CF_CONNECTING_IP');
        } else {
            $ip = request()->server('REMOTE_ADDR');
        }
        if($ip) {
            $data += ['ip' => $ip];
        }

        /*return dd(*/$model->log()->create($data)/*)*/;
    }

    /**
     * @param string $controller_and_action
     * @param array $model|null
     * @param integer $priority|null
     * @return void
     */

    public function logout($controller_and_action,$model=null,$priority=null)
    {
        $parse_controller_url = explode('\\', $controller_and_action);
        $module_name = $parse_controller_url[2];
        $module_id = Module::where("name", $module_name)->first();

        $website =session(self::PANEL_WEBSITE);
        $content = "'$website->slug' sitesinde '$model->username' adlı kullanıcı çıkış yaptı.";

        $data = [
            self::WEBSITE_ID => $website->id,
            self::USER_ID => $model->id,
            self::CONTROLLER => explode('@', $controller_and_action)[0],
            self::ACTION => explode('@', $controller_and_action)[1],
            self::CONTEXT_TYPE =>"info",
            self::CONTEXT_ICON =>"fa-sign-out",
            self::CONTENT =>$content,
            self::PRIORITY =>$priority,
        ];


        if($module_id){
            $data += [self::MODULE_ID =>$module_id->id];
        }
        else{
            $data += [self::MODULE_ID =>null];
        }

        $model->log()->create($data);
    }

}
