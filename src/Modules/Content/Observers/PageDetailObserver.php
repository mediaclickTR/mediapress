<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 21.03.2019
 * Time: 17:45
 */

namespace Mediapress\Modules\Content\Observers;

use Illuminate\Support\Str;
use Mediapress\Modules\Content\Controllers\ContentController;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use Mediapress\Modules\MPCore\Models\SettingSun;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Support\Facades\Cache;

class PageDetailObserver
{

    public function created(PageDetail $pd)
    {
        if ($pd->slug == '/' || !$pd->slug) {
            if($pd->page->sitemap->reservedUrl != 1) {
                return true;
            }
            $url_hash = Str::random(16);
            $check_url = URLEngine::getPatternForPageDetail($pd, $pd->countryGroup, $pd->language, $url_hash);
            $new_url_prefix = $check_url[0];
            $new_url_slug = "/" . $check_url[1];
            $new_url_str = $new_url_prefix . $new_url_slug;
            $do = URLEngine::create($pd->parent->sitemap->websites()->first(), $new_url_str, $pd, "reserved");
        }else{
            $check_url = URLEngine::getPatternForPageDetail($pd, $pd->countryGroup, $pd->language, $pd->slug);
            $new_url_prefix = $check_url[0];
            $new_url_slug = "/" . $check_url[1];
            $new_url_str = $new_url_prefix . $new_url_slug;
            $do = URLEngine::create($pd->parent->sitemap->websites()->first(), $new_url_str, $pd, "original");
        }
        return true;
    }

    public function updating(PageDetail $pd)
    {
        // below code is concerned with a slug change only://in_array("slug", array_keys($pd->getDirty()))
        if (in_array("slug", array_keys($pd->getDirty()))) {
            return $this->renewSlug($pd, true);
        }

        Cache::flush();
        return true;
    }

    public function deleting(PageDetail $object) {
        if ($object->isForceDeleting()) {
            $object->urls()->forceDelete();
        }

        if($object->url) {
            $object->url()->delete();
        }

        if($object->extras->isNotEmpty()) {
            $object->extras()->delete();
        }

        Cache::flush();
    }

    public function deleted(PageDetail $object)
    {
        if (is_countable($object->urls) && count($object->urls)) {
            $object->urls()->delete();
        }
        Cache::flush();
    }

    public function restoring(PageDetail $object)
    {
        if (!$this->tryRestoringOldUrl($object)) {
            if (Str::slug($object->slug) == "") {
                $slug = Str::slug($object->name);
                if ($slug == "") {
                    $slug = Str::random(16);
                }
                $object->slug = $slug;
            }
            return $this->renewSlug($object, false);
        }
        return true;
    }

    private function tryRestoringOldUrl(PageDetail $object)
    {
        $object->deleted_at = null;
        $object->save();
        $object->load(["url"]);
        $url = $object->url()->withTrashed()->first();
        if (!$url) {
            $url = Url::where('model_type', get_class($object))->where('model_id', $object->id)->first();
        }
        if ($url) {
            $url->restore();
            return true;
        }
        return false;
    }

    private function renewSlug(PageDetail $object, $force_clean_slug = true)
    {
        $pd = $object;
        $check_url = URLEngine::getPatternForPageDetail($pd, $pd->countryGroup, $pd->language, $pd->slug);


        // produce the new whole url string
        $new_url_prefix = $check_url[0];
        $new_url_slug = "/" . $check_url[1];
        $new_url_str = $new_url_prefix . $new_url_slug;

        // throw an error if slug cleanising is not pre-performed.
        // User should send a valid slug string which already been sluggified.
        if ($force_clean_slug && $new_url_slug != $pd->slug) {
            //dump("force clean slug and not eqwual, will throw error");
            throw new \Exception("Slug, temiz bir slug metni değil. Uyuşmayanlar: gönderilen hali \"$pd->slug\" ve filtrelenen hali \"$new_url_slug\"");
            return false;
        }

        // retrieve the existing url model if there is one:
        // if there is not, there have been something exceptional. Should be analised.
        $existing_url_model = $pd->urls()->whereIn("type", ["original", "reserved"])->first();
        //dump("Checking existing url model", $existing_url_model);
        // if we have a url model for this detail object
        if ($existing_url_model) {
            if ($existing_url_model->type == "original" && $existing_url_model->url == $new_url_str) {
                // if new string for the url model is already same with the current one, there is no need to update.
                return true;
            }
            // if url models type is "original" AND new url string is different
            // OR IF the url models type is "reserved"
            // url model should be updated as "original" with the new url string
            $do = URLEngine::update($pd->parent->sitemap->websites()->first(), $existing_url_model, $new_url_str, $pd, "original");
            if (!$do[0]) {
                throw new \Exception($do[1]);
                return false;
            }
        } else {
            // there should already been an url model existing for any created detail object
            // if flow drops here that means there is something exceptional. Should be analised later.
            // Still need to make sure to have an url model with original type in this context:
            $do = URLEngine::create($pd->parent->sitemap->websites()->first(), $new_url_str, $pd, "original");
            if (!$do[0]) {
                throw new \Exception($do[1]);
                return false;
            }
        }
        Cache::flush();

        return true;
    }
}
