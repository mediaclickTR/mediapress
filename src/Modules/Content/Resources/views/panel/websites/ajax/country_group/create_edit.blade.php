<div class="modal-header">
    <h4 class="modal-title">
        @if(isset($country_group))
            {!! trans("ContentPanel::website.country_group_edit") !!}
            @else
            {!! trans("ContentPanel::website.country_group_create") !!}
        @endif
    </h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
            onclick="javascript:close_popup(this);">
    </button>
</div>
<div class="modal-body">

        @if(!isset($country_group))
            {{ Form::open(["id"=>"cg_form"]) }}
        @else
            {{ Form::model($country_group, ['method' => 'POST','id'=>'cg_form']) }}
        @endif

        <div class="row">
            <input type="hidden" name="cg_owner_id" value={!! $website_id !!}>
            <input type="hidden" name="cg_owner_type" value="Mediapress\Modules\Content\Models\Website">
            @if(!isset($country_group))
                <input type="hidden" name="cg_id" value="new">
                @else
                <input type="hidden" name="cg_id" value="{!! $country_group->id !!}">
            @endif

            @if(!isset($country_group) || (isset($country_group) && $country_group->code != 'gl'))
                <div class="col-md-12">
                    <div class="form-group col-md">
                        <span class="tit">{!! trans("ContentPanel::website.country_group_list_name") !!}</span>
                        {!!Form::text('list_title', null, ["placeholder"=>trans("ContentPanel::website.country_group_list_name"), "class"=>"validate[required]"])!!}
                    </div>
                </div>
            @endif

            <div class="col-md-6">
                <div class="form-group col-md">
                    <span class="tit">{!! trans("ContentPanel::website.country_group_name") !!}</span>
                    {!!Form::text('title', null, ["placeholder"=>trans("ContentPanel::website.country_group_name"), "class"=>"validate[required]"])!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group col-md">
                    <span class="tit">{!! trans("ContentPanel::website.country_group_code") !!}</span>
                    {!!Form::text('code', null, ["placeholder"=>trans("ContentPanel::website.country_group_code"), "class"=>"validate[required]", "id"=>"cg_code"])!!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group rel">
                    <span class="tit">{!! trans("ContentPanel::website.languages") !!}</span><br/>
                    {!!Form::select('languages', $languages, isset($country_group) ? $selected_languages : null, ['class' => 'multiple','multiple', 'required','id'=>'ajaxLanguages'])!!}
                </div>
            </div>
            <div class="col-4">
                <div class="form-group rel">
                    <span class="tit">{!! trans("ContentPanel::website.default_language") !!}</span><br/>
                    {!!Form::select('default_language', isset($country_group) ? $selected_languages->pluck('name', 'id') : [null], isset($country_group) ? ($country_group->defaultLanguage() ? $country_group->defaultLanguage()->id : null) : null, ['placeholder'=>trans("MPCorePanel::general.selection"), 'class' => 'default_language'])!!}
                </div>
            </div>

            @if((isset($country_group) && $country_group->code != 'gl') || !isset($country_group))
                <div class="col-md-12">
                    <div class="form-group rel">
                        <span class="tit">{!! trans("ContentPanel::website.countries") !!}</span><br/><br/>
                        {!!Form::select('countries', $countries, isset($country_group) ? $selected_countries : null, ['class' => 'multiple','multiple', 'required','id'=>'ajaxCountries'])!!}
                    </div>
                </div>
            @else
                <div class="col-md-12">
                    <p>{!! rtrim($countryNames, ', ') !!} dışındaki ülkeleri içerir.</p>
                </div>
            @endif

            <div class="col-md-12">
                <div class="float-right">
                    <button class="btn btn-primary" id="cg_save" type="submit">{!! trans("MPCorePanel::general.save") !!}</button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
<script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>

<script>
    $(document).ready(function () {
        $('#cg_save').click(function(e){
            e.preventDefault();
            var cg_id = $("input[name='cg_id']").val();
            var cg_owner_type = $("input[name='cg_owner_type']").val();
            var cg_owner_id = $("input[name='cg_owner_id']").val();
            var title = $("input[name='title']").val();
            var list_title = $("input[name='list_title']").val();
            var code = $("input[name='code']").val();
            var languages = $("select[name='languages']").val();
            var default_language = $("select[name='default_language']").val();
            var countries = $("select[name='countries']").val();
            //var form = $("#cg_form").serializeArray();
            var token = $("meta[name='csrf-token']").attr("content");;
            $.ajax({
                url: "{{ route('Content.websites.country_groups.save', $website_id) }}",
                method: 'POST',
                data: {
                    cg_id:cg_id,
                    cg_owner_id:cg_owner_id,
                    cg_owner_type:cg_owner_type,
                    title:title,
                    list_title:list_title,
                    code:code,
                    languages:languages,
                    default_language:default_language,
                    countries:countries,
                    _token:token
                },
                success: function (data) {
                    swal({
                        title: '{{ trans("MPCorePanel::general.success_title") }}',
                        text: '{{ trans("MPCorePanel::general.success_message") }}',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ trans("MPCorePanel::general.ok") }}',
                    }).then((result) => {
                        if (result) {
                            $('#general-modal').modal('hide')
                        }
                    });
                    countryGroupsEvent();
                },
                error: function (xhr, status, errorThrown) {
                    alert(false);
                }
            });
        });

        $('#cg_code').unbind('keyup change input paste').bind('keyup change input paste',function(e){
            var $this = $(this);
            $this.val($this.val().toUpperCase());
            var val = $this.val();
            var valLength = val.length;
            var maxCount = 2;
            if(valLength>maxCount){
                $this.val($this.val().substring(0,maxCount));
            }
        });

        $('#ajaxLanguages').selectator({
            showAllOptionsOnFocus: true,
            searchFields: 'value text subtitle right'
        });
        $('#ajaxCountries').selectator({
            showAllOptionsOnFocus: true,
            searchFields: 'value text subtitle right'
        });
    });


    var tempOption = `<option value="{value}">{name}</option>`;

    $(document).delegate("#ajaxLanguages", "change", function (e) {
        $("select.default_language").html("");
        var content = "";

        $('[selected]', e.currentTarget).each(function (el, item) {

            var lang = item.innerText;
            var id = item.value;
            var tempOp = tempOption.replace(/{value}/gm, id).replace(/{name}/gm, lang);
            content += tempOp;
        });
        $("select.default_language").html(content);
    });


    $(document).delegate("select.default_language", "change", function (e) {

        $('select.default_language > option[value="'+$(this).val()+'"]').attr('selected', "");

    });

</script>
