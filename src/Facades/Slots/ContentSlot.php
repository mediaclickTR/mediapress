<?php

namespace Mediapress\Facades\Slots;

use Illuminate\Support\Facades\Facade;
use Mediapress\Models\Slot;

class ContentSlot extends Facade
{
    protected static function getFacadeAccessor()
    {
        $slot = Slot::where("slot", substr(strrchr(__CLASS__, "\\"), 1))->first();
        return str_replace("/","\\",$slot->module->namespace).'\\'.$slot->module->name;
    }
}