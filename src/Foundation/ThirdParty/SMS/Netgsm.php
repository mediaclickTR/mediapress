<?php

namespace Mediapress\Foundation\ThirdParty\SMS;

use Exception;
use SoapClient;

class Netgsm
{

    private $xmlURL = 'https://api.netgsm.com.tr/sms/send/xml';
    private $soapURL = 'http://soap.netgsm.com.tr:8080/Sms_webservis/SMS?wsdl';
    private $gsm = [];
    private $msg = '';


    public function setGsm($numbers)
    {
        if (is_array($numbers)) {
            foreach ($numbers as $number) {
                $this->gsm[] = $this->clear($number);
            }
        } elseif (is_string($numbers) || is_integer($numbers) || is_numeric($numbers)) {
            $this->gsm[] = $this->clear($numbers);
        }
    }

    public function setMessage($msg)
    {
        $this->msg = $msg;
    }


    public function send($method = 'XML')
    {

        if ($method == 'XML') {
            $xml = '<?xml version="1.0" encoding="UTF-8"?>
             <mainbody>
             <header>
             <company dil="TR">Netgsm</company>
             <usercode>' . config('netgsm.username') . '</usercode>
             <password>' . config('netgsm.password') . '</password>
             <type>1:n</type>
             <msgheader>' . config('netgsm.header') . '</msgheader>
             </header>
             <body>
             <msg>
             <![CDATA[' . $this->msg . ']]>
             </msg>'."\n";
            foreach ($this->gsm as $gsm) {
                $xml .= "<no>". $gsm . "</no>\n";
            }
            $xml .= '</body>
             </mainbody>';
            return $this->xml($xml);
        } elseif ($method == 'SOAP') {
            return $this->soap();
        }

    }

    private function clear($number)
    {
        //If not a Turkey number, add 00 to the beginning
        if( substr($number, 0, 3) != "+90" ) {
            $number = "00" . $number;
        }

        preg_match_all('/\d+/', $number, $matches);
        return implode('',$matches[0]);
    }

    private function soap()
    {
        try {
            $client = new SoapClient($this->soapURL);
            $result = $client->smsGonder1NV2(
                [
                    'username' => config('netgsm.username'),
                    'password' => config('netgsm.password'),
                    'header' => config('netgsm.header', config('netgsm.username')),
                    'msg' => $this->msg,
                    'gsm' => $this->gsm,
                    'filter' => '',
                    'startdate' => '',
                    'stopdate' => '',
                    'encoding' => ''
                ]
            );


            $response = ['success' => true, 'response' => $result];
        } catch (Exception $exc) {
            $response = ['success' => false, 'response' => $exc->getMessage()];

        }
        return $response;
    }

    private function xml(string $xml)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->xmlURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($ch);
       return ['success' => true, 'response' => $result];
    }

}
