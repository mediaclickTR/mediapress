<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'panel.auth'], function () {


    /*
     * Ajax
     */

    Route::get('RedirectEdit','RedirectController@redirectEdit');

    Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
        Route::post('/', ['as' => 'request', 'uses' => 'AjaxController@request']);
        Route::post('/popup/{process}', ['as' => 'popup', 'uses' => 'AjaxController@popup']);
        Route::get('backup',['as' => 'backup', 'uses' => 'AjaxController@backup']);
    });

    Route::group(['prefix' => 'MPCore', "as"=>"MPCore."], function () {



        /*Route::group(['as' => 'megexplorer.', 'prefix' => 'Megexplorer'], function ()
        {
            Route::get('/', ['as' => 'root', 'uses' => function () {
                $params = request()->all();
                return view('MPCorePanel::dashboard.index', compact('params'));
            }]);
            Route::any('/index/{diskkey?}/{folder_id?}', ['as' => 'index', 'uses' => 'FileManagerController@index']);
            Route::any('/download/{diskkey}/{folder_id}/{file_id}', ['as' => 'download', 'uses' => 'FileManagerController@download']);
            Route::any('/info/{diskkey}/{folder_id?}/{file_id?}', ['as' => 'info', 'uses' => 'FileManagerController@info']);
            Route::any('/select/{diskkey}/{folder_id?}/{file_id?}', ['as' => 'select', 'uses' => 'FileManagerController@select']);
            //Route::post('/upload/{diskkey}/{folder_id?}}',['as'=>'upload','uses'=>'FileManagerController@upload']);
            Route::post('/upload2', ['as' => 'upload', 'uses' => 'FileManagerController@upload2']);
            Route::any('/remove/{diskkey}/{folder_id?}/{file_id?}', ['as' => 'remove', 'uses' => 'FileManagerController@remove']);
            Route::any('/crop/{diskkey}/{folder_id}/{file_id}', ['as' => 'crop', 'uses' => 'FileManagerController@crop']);
            Route::any('/create-folder/{diskkey}/{folder_id?}', ['as' => 'create_folder', 'uses' => 'FileManagerController@createFolder']);
            //Route::any('/create/{diskkey}/{folder_id?}',['as'=>'create', 'uses'=>'FileManagerController@createFile']);
        });*/

        Route::group(['prefix' => 'Session', 'as' => 'session.'], function () {
            Route::get('/setWebsiteSession/{website_id}', ['as' => 'setWebsiteSession', 'uses' => 'SessionController@setWebsiteSession']);
            Route::get('/setLanguageSession/{language_id}', ['as' => 'setLanguageSession', 'uses' => 'SessionController@setLanguageSession']);
        });

        /*
         * Datasource
         */

        Route::group(['prefix' => 'Datasource', 'as' => 'datasource'], function () {
            Route::get('/get/{module_name}/{ds_name}', ['as' => 'get', 'uses' => 'DataSourceController@get']);
            Route::get('/get/{module_name}/{ds_name}/{params}', ['as' => 'get', 'uses' => 'DataSourceController@get']);
            Route::post('/post', ['as' => 'post', 'uses' => 'DataSourceController@post']);
        });


        Route::group(['prefix' => 'Playground', 'as' => 'playground.'], function () {
            Route::get('/', 'PlaygroundController@index')->name('index');
            Route::get('/{actionName}/{params?}', 'PlaygroundController@runAction')->where('params', '(.*)')->name("runner");
        });


        Route::group(['prefix' => 'CountryGroups', 'as' => 'country_groups.'], function () {
            Route::get('/manage', ['as' => 'manage', 'uses' => 'CountryGroupsController@manage']);
            Route::get('/ownersGroups', ['as' => 'ownersGroups', 'uses' => 'CountryGroupsController@getOwnersCountryGroups']);
            Route::get('/groupsCountries', ['as' => 'groupsCountries', 'uses' => 'CountryGroupsController@getGroupsCountries']);
            Route::get('/ungroupedCountries', ['as' => 'ungroupedCountries', 'uses' => 'CountryGroupsController@getUngroupedCountries']);
            Route::post('/save', ['as' => 'save', 'uses' => 'CountryGroupsController@saveCountryGroup']);
            Route::post('/remove', ['as' => 'remove', 'uses' => 'CountryGroupsController@removeCountryGroup']);
        });

        Route::group(['prefix' => 'CountryProvince', 'as' => 'country_province.'], function () {
            Route::get('/get_values', ['as' => 'get_values', 'uses' => 'PlaygroundController@test_country_province']);

        });

        Route::group(['prefix' => 'Translations', 'as' => 'translations.'], function () {
            Route::get('/list', ['as' => 'list', 'uses' => 'TranslationsController@list']);
            Route::get('/get/{key}', ['as' => 'get', 'uses' => 'TranslationsController@get']);
            Route::post('/update/{key}', ['as' => 'update', 'uses' => 'TranslationsController@update']);
            Route::post('/remove/{key}', ['as' => 'remove', 'uses' => 'TranslationsController@remove']);
        });

        /*
         * Logs
         */

        Route::group(['prefix' => 'UserActionLogs', 'as' => 'logs.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'UserActionLogController@index']);
        });

        /*
       * Translate
       */

        Route::group(['prefix' => 'Translate', 'as' => 'translate.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'TranslateController@index']);
            Route::post('/save', ['as' => 'save', 'uses' => 'TranslateController@save']);
            Route::post('/search', ['as' => 'search', 'uses' => 'TranslateController@search']);
            Route::get('/export', ['as' => 'export', 'uses' => 'TranslateController@export']);
            Route::post('/import', ['as' => 'import', 'uses' => 'TranslateController@import']);
            Route::get('/delete', ['as' => 'delete', 'uses' => 'TranslateController@delete']);
        });

        Route::group(['prefix' => 'PanelTranslate', 'as' => 'panel.translate.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PanelTranslateController@index']);
            Route::post('/save', ['as' => 'save', 'uses' => 'PanelTranslateController@save']);
            Route::post('/search', ['as' => 'search', 'uses' => 'PanelTranslateController@search']);
        });



        /*
         * Technical modules
         * Include SettingSun
         */

        Route::group(['prefix' => 'Technical', 'as' => 'technical.'], function () {

            Route::group(['prefix' => 'Modules', 'as' => 'modules.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'ModuleController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'ModuleController@create']);
                Route::post('/store', ['as' => 'store', 'uses' => 'ModuleController@store']);
                Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'ModuleController@edit']);
                Route::post('/update', ['as' => 'update', 'uses' => 'ModuleController@update']);
                Route::get('/{id}/show', ['as' => 'details', 'uses' => 'ModuleController@show']);
                Route::get('/{id}/manage', ['as' => 'manage', 'uses' => 'ModuleController@manage']);
                Route::post('/managestore', ['as' => 'managestore', 'uses' => 'ModuleController@manageStore']);
                Route::post('/slotStore', ['as' => 'slot.store', 'uses' => 'ModuleController@slotStore']);
                Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'ModuleController@delete']);
                Route::group(['prefix' => 'Config', 'as' => 'config.'], function () {
                    Route::get('/', ['as' => 'index', 'uses' => 'ModuleConfigController@index']);
                    Route::get('/create', ['as' => 'create', 'uses' => 'ModuleConfigController@create']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'ModuleConfigController@store']);
                    Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'ModuleConfigController@edit']);
                    Route::post('/update', ['as' => 'update', 'uses' => 'ModuleConfigController@update']);
                    Route::get('/{id}/show', ['as' => 'details', 'uses' => 'ModuleConfigController@show']);
                    Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'ModuleConfigController@delete']);
                });
            });

            Route::group(['prefix' => 'Routes', 'as' => 'routes.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'Technical\RouteController@index']);
            });

            Route::group(['prefix' => '/SettingSun', 'as' => 'setting_sun.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'Technical\SettingSunController@index']);
                Route::get('/create', ['as' => 'create', 'uses' => 'Technical\SettingSunController@create']);
                Route::post('/store', ['as' => 'store', 'uses' => 'Technical\SettingSunController@store']);
                Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'Technical\SettingSunController@edit']);
                Route::post('/update', ['as' => 'update', 'uses' => 'Technical\SettingSunController@update']);
                Route::get('/{id}/show', ['as' => 'details', 'uses' => 'Technical\SettingSunController@show']);
                Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'Technical\SettingSunController@delete']);
            });

        });


        Route::group(['prefix' => 'SitemapTypes', 'as' => 'sitemap_types.'], function () {

            Route::get('/', ['as' => 'index', 'uses' => 'SitemapTypeController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'SitemapTypeController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'SitemapTypeController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'SitemapTypeController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => 'SitemapTypeController@update']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'SitemapTypeController@delete']);

        });

    });
});
