<?php
return [

    "content" => [
        'name' => 'ContentPanel::auth.sections.content_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [
            "websites" => [
                'name' => 'ContentPanel::auth.sections.websites',
                'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Content\Models\Website::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => true,
                    "is_variation" => false,
                    "is_variation" => false,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                        'description'=>"",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'subs' => [
                    "sitemaps" => [
                        'name' => 'ContentPanel::auth.sections.sitemaps',
                        'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                        'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,
                        'item_id' => "*",
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ],
                            'activate_variation' => [
                                'name' => 'Varyasyonu Etkinleştir'
                            ],
                            'deactivate_variation' => [
                                'name' => 'Varyasyonu Kull. Dışı Bırak'
                            ]
                        ],
                        'subs' => [
                            "pages" => [
                                'name' => 'ContentPanel::auth.sections.pages',
                                'item_model' => \Mediapress\Modules\Content\Models\Page::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => false,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                    ],
                                    'create' => [
                                        'name' => "MPCorePanel::auth.actions.create",
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ],
                                ],
                                'subs' => [

                                ]
                            ],
                            "categories" => [
                                'name' => 'ContentPanel::auth.sections.categories',
                                'item_model' => \Mediapress\Modules\Content\Models\Category::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                    ],
                                    'create' => [
                                        'name' => "MPCorePanel::auth.actions.create",
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'reorder' => [
                                        'name' => "MPCorePanel::auth.actions.reorder",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ]
                                ],
                                'subs' => [

                                ],
                            ],
                            "criterias" => [
                                'name' => 'ContentPanel::auth.sections.criterias',
                                'item_model' => \Mediapress\Modules\Content\Models\Criteria::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                    ],
                                    'create' => [
                                        'name' => "MPCorePanel::auth.actions.create",
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'reorder' => [
                                        'name' => "MPCorePanel::auth.actions.reorder",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ]
                                ],
                                'subs' => [
                                ],
                            ],
                            "properties" => [
                                'name' => 'ContentPanel::auth.sections.properties',
                                'item_model' => \Mediapress\Modules\Content\Models\Property::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                    ],
                                    'create' => [
                                        'name' => "MPCorePanel::auth.actions.create",
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'reorder' => [
                                        'name' => "MPCorePanel::auth.actions.reorder",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ]
                                ],
                                'subs' => [
                                ],
                            ],

                        ],
                        'variations' => []

                    ],
                    "menus" => [
                        'name' => 'ContentPanel::auth.sections.menus',
                        'item_model' => \Mediapress\Modules\Content\Models\Menu::class,
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => []
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                                'variables' => []
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update"
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                        ],
                    ],

                    "seo" => [
                        'name' => 'ContentPanel::auth.sections.seo',
                        'item_model' => \Mediapress\Modules\MPCore\Models\Meta::class,
                        'actions' => [],
                        'subs' => [
                            'meta_management' => [
                                'name' => 'ContentPanel::auth.sections.meta_management',
                                'item_model' => \Mediapress\Modules\Content\Models\Meta::class,
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                        'variables' => []
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ],
                                    'export' => [
                                        'name' => "MPCorePanel::auth.actions.export",
                                    ],
                                    'import' => [
                                        'name' => "MPCorePanel::auth.actions.import",
                                    ],
                                ],
                                'subs' => [],
                                'variations' => []
                            ],
                            'meta_templates' => [
                                'name' => 'ContentPanel::auth.sections.meta_templates',
                                'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                        'variables' => []
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                    'delete' => [
                                        'name' => "MPCorePanel::auth.actions.delete",
                                    ],
                                ],
                                'subs' => [],
                                'variations' => []
                            ],
                            'robots' => [
                                'name' => 'ContentPanel::auth.sections.robots',
                                'item_model' => stdClass::class,
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.view",
                                        'variables' => []
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                ],
                                'subs' => [],
                                'variations' => []
                            ],
                            'sitemap_xmls' => [
                                'name' => 'ContentPanel::auth.sections.sitemap_xmls',
                                'item_model' => \Mediapress\Modules\Content\Models\SitemapXml::class,
                                'actions' => [
                                    'index' => [
                                        'name' => "MPCorePanel::auth.actions.list",
                                        'variables' => []
                                    ],
                                    'create' => [
                                        'name' => "MPCorePanel::auth.actions.create",
                                        'variables' => []
                                    ],
                                    'update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                    ],
                                ],
                                'subs' => [],
                                'variations' => []
                            ]
                        ]
                    ],

                    "social_media" => [
                        'name' => 'ContentPanel::auth.sections.social_media',
                        'item_model' => \Mediapress\Modules\Content\Models\SocialMedia::class,
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => []
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                                'variables' => []
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update"
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                        ],
                    ],
                    "sliders" => [
                        'name' => 'ContentPanel::auth.sections.sliders',
                        'item_model' => \Mediapress\Modules\Content\Models\Slider::class,
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => []
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                                'variables' => []
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update"
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                        ],
                    ],
                    "popups" => [
                        'name' => 'ContentPanel::auth.sections.popups',
                        'item_model' => \Mediapress\Modules\Content\Models\Popup::class,
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => []
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                                'variables' => []
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update"
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                        ],
                    ],

                ],
                'variations_title' => 'ContentPanel::auth.sections.recorded_websites',
                'variations' => 'website_variations',
                'settings' => [
                    "auth_view_collapse" => "in"
                ],
            ],
        ]
    ]
        /*"settings" => [
        'name' => 'Ayarlar',
        'item_model' => 'Mediapress\Models\Setting',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        ],
        'subs' => [
        'langs' => [
        'name' => 'Diller',
        'item_model' => \Mediapress\Modules\MPCore\Models\Language::class,
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        ]

        ],
        'lang_parts' => [
        'name' => 'Dil Değişkenleri',
        'item_model' => \Mediapress\Modules\MPCore\Models\LanguagePart::class,
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ]
        ]
        ],
        'urls' => [
        'name' => 'URL İşlemleri',
        'item_model' => \Mediapress\Modules\MPCore\Models\Url::class,
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'view_url_list' => [
        'name' => "Url Listesi"
        ],
        ]
        ],
        'upfiles' => [
        'name' => 'Yüklenen Dosyalar',
        'item_model' => null,
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'view' => [
        'name' => 'Görüntüle'
        ],
        'delete_not_on_disk' => [
        'name' => 'Diskte Bulunmayanları Sil'
        ],
        'delete_not_on_db' => [
        'name' => 'Veritabanında Bulunmayanları Sil'
        ],
        'delete_inconsistent' => [
        'name' => 'Tüm Bulunamayanları Sil'
        ]
        ],
        ]
        ]
        ],*/
        /*"script_manager" => [
        'name' => 'Script Yöneticisi',
        'item_model' => 'Mediapress\Models\Script',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ],
        ],
        "slider" => [
        'name' => 'Slider',
        'item_model' => 'Mediapress\Models\Slider',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ],
        'subs' => [
        'part' => [
        'name' => 'Slide',
        'item_model' => 'Mediapress\Models\SliderPart',
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'subs' => [
        'detail' => [
        'name' => 'Slide Detay',
        'item_model' => 'Mediapress\Models\SliderPartDetail',
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => true,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ],
        ]
        ]
        ]
        ]
        ],
        "popup" => [
        'name' => 'Pop-up',
        'item_model' => 'Mediapress\Models\Popup',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ]
        ],
        "sidebar" => [
        'name' => 'Sidebar',
        'item_model' => 'Mediapress\Models\Sidebar',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ]
        ],*/
        /*
        "warehouse" => [
        'name' =>'Depo'
        ],
        */
        /*"page_subscription" => [
        'name' => 'Sayfa Aboneliği',
        'item_model' => 'Mediapress\Models\PageSubscription',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ],
        'export' => [
        'name' => "Excele Aktar",
        'variables' => ['file_type']
        ]
        ],
        ],*/
        /*"photogallery" => [
        'name' => 'Foto Galeri',
        'item_model' => 'Mediapress\Models\PageGallery',
        'subs' => [
        'detail' => [
        'name' => 'Galeri Detay',
        'item_model' => 'Mediapress\Models\PageGalleryDetail',
        'data' => [
        "is_root" => false,
        "is_variation" => false,
        "is_sub" => true,
        "descendant_of_sub" => false,
        "descendant_of_variation" => false
        ],
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ],
        'edit_update' => [
        'name' => "MPCorePanel::auth.actions.update",
        'variables' => ['languages']
        ]
        ],
        'subs' => [
        'detail' => [
        'name' => 'Galeri Detay Görseller',
        'item_model' => 'Mediapress\Models\PageGalleryImage',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        ]
        ]
        ]
        ],
        ]
        ],*/
        /*"user_process" => [
        'name' => 'Üye İşlemleri',
        'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,

        'subs' => [
        "users" => [
        'name' => 'Üyeler',
        'item_model' => \Mediapress\Modules\Entity\Models\User::class,
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ]
        ],
        "dues" => [
        'name' => 'Aidatlar',
        'item_model' => 'Mediapress\Models\Due',
        'actions' => [
        'index' => [
        'name' => "MPCorePanel::auth.actions.list",
        'variables' => []
        ],
        'create' => [
        'name' => "MPCorePanel::auth.actions.create",
        'variables' => []
        ],
        'update' => [
        'name' => "MPCorePanel::auth.actions.update"
        ],
        'debit' => [
        'name' => "Borçlandır"
        ],
        'delete' => [
        'name' => "MPCorePanel::auth.actions.delete",
        ]
        ]
        ],

        ],
        ],*/
        /*
        "members" => [
        'name' =>'Üyeler'
        ],
        "tech" => [
        'name' =>'Teknik Yönetim'
        ],
        "dues" => [
        'name' =>'Aidatlar'
        ],
        */
    ];
