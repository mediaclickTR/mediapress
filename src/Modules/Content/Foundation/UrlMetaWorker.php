<?php


namespace Mediapress\Modules\Content\Foundation;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Arr;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\MPCore\Foundation\MetaKeyValueCollection;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Url;

class UrlMetaWorker
{

    // region $ Properties
    protected $_url_base = null;
    protected $_url_model = null;
    protected $_meta_collection = [];
    protected $_meta_variables = [];
    protected $_flat_meta_variables = [];
    protected $_meta_templates = [];
    protected $_device = "";
    protected $_final_list = [];

    // endregion

    // region Getters & Setters


    /**
     * @return null|string
     */
    public function getUrlBase()
    {
        return $this->_url_base;
    }

    /**
     * @param null $url_base
     * @return UrlMetaWorker
     */
    public function setUrlBase($url_base=null):UrlMetaWorker
    {
        $this->_url_base = $url_base;
        $this->_final_list = $this->_replaceVariables($this->_getMergedList());
        return $this;
    }

    /**
     * @return null
     */
    public function getMetaTemplates()
    {
        return $this->_meta_templates;
    }

    /**
     * @param null $meta_templates
     * @return UrlMetaWorker
     */
    public function setMetaTemplates($meta_templates):UrlMetaWorker
    {
        $this->_meta_templates = $meta_templates;
        return $this;
    }

    /**
     * @return string
     */
    public function getDevice():string
    {
        return $this->_device;
    }

    /**
     * @param null $device
     * @return UrlMetaWorker
     */
    public function setDevice($device):UrlMetaWorker
    {
        $this->_device = $device;
        return $this;
    }

    /**
     * @return array
     */
    public function getMetaVariables():array
    {
        return $this->_meta_variables;
    }

    /**
     * @param null $meta_variables
     * @return UrlMetaWorker
     */
    public function setMetaVariables($meta_variables):UrlMetaWorker
    {
        $this->_meta_variables = $meta_variables;
        $flat_meta_variables = [];
        foreach($meta_variables as $mvs){
            foreach($mvs["variables"] as $mk => $mv){
                $flat_meta_variables[$mk]=$mv["value"];
            }
        }
        $this->_flat_meta_variables = $flat_meta_variables;

        return $this;
    }

    /**
     * @return null
     */
    public function getUrlModel()
    {
        return $this->_url_model;
    }

    /**
     * @return array
     */
    public function getFinalList()
    {
        return $this->_final_list;
    }



    // reinitialization necessary for a URL change
    /**
     * @param null $url_model
     * @return UrlMetaWorker
     */
    /*public function setUrlModel($url_model):UrlMetaWorker
    {
        $this->_url_model = $url_model;
        return $this;
    }*/

    /**
     * @return null|MetaKeyValueCollection|array
     */
    public function getMetaCollection()
    {
        return $this->_meta_collection;
    }

    /**
     * @param MetaKeyValueCollection $meta_collection
     * @return UrlMetaWorker
     */
    public function setMetaCollection(MetaKeyValueCollection $meta_collection)
    {
        $this->_meta_collection = $meta_collection;
        return $this;
    }

    // endregion

    // region Magic Methods
    public function __construct(Url $url=null, MetaKeyValueCollection $metas = null, $meta_variables = [], $meta_templates = [], $device = null)
    {
        return $this->init($url, $metas, $meta_variables, $device);
    }

    public function __toString()
    {
        return $this->toHtml();
    }

    public function __get($name):?string
    {
        if(array_key_exists($name, $this->_final_list)){
            return $this->_final_list[$name]["value"] ?? null;
        }
        return null;
    }

    public function __set($name, $value):void
    {
        if(array_key_exists($name, $this->_final_list)){
            $this->_final_list[$name]["value"]=$value;
        }
    }
    // endregion


    // region Main Methods

    /**
     * @param Url $url_model
     * @param MetaKeyValueCollection|null $metas
     * @param array $meta_variables
     * @param array $meta_templates
     * @param string $device
     * @return $this
     * @throws \Exception
     */
    public function init(Url $url_model=null, MetaKeyValueCollection $metas = null, $meta_variables = [], $meta_templates = [], string $device = ""):UrlMetaWorker
    {

        /*if(!$url_model->exists){
            throw new \Exception("Url model does not exist");
        }*/

        $url_model_valid = is_a($url_model,Url::class) && $url_model->exists;

        if(!$metas){
            $metas = $url_model_valid ? $url_model->metas()->get() : new MetaKeyValueCollection();
        }

        if(!$device){
            $device = 'desktop';
        }

        if(!$meta_variables){
            $meta_variables = $url_model_valid ? Content::getMetaVariablesFor($url_model->model) : [];
        }

        if(!$meta_templates){
            $meta_templates = $url_model_valid ? Content::getMetaTemplatesOf($url_model->model, $url_model->model->parent->sitemap->detail) : [];
        }
        $this->_url_model = $url_model;

        $this->setMetaCollection($metas)
            ->setMetaTemplates($meta_templates)
            ->setMetaVariables($meta_variables)
            ->setDevice($device);

        //$this->processMetas();

        $this->_final_list = $this->_replaceVariables($this->_getMergedList());

        return $this;
    }

    public function render(array $except_keys=[])
    {
        echo $this->toHtml($except_keys);
    }

    public function toHtml(array $except_keys=[]):string
    {
        $str = "";

        foreach(Arr::except($this->_final_list, $except_keys) as $meta_data){
            $str.= "\r\n".preg_replace(['/\$0/m', '/\$1/m'], [$meta_data["key"], $meta_data["value"]], $meta_data["print_template"]);
        }

        return trim($str);
    }

    public function toVueClass(array $except_keys=[])
    {

        $vue_bucket = new \stdClass();

        if(!in_array("title", $except_keys)){
            $vue_bucket->title=$this->_final_list["title"]["value"];
        }

        $metas = Arr::except($this->_final_list, ["title"]+$except_keys);

        $flattener = function($meta_data){
            return ["name" =>$meta_data["key"], "content"=>$meta_data["value"]];
        };

        $vue_bucket->meta = array_values(array_map($flattener, $metas));
        unset($metas);

        return $vue_bucket;
    }

    public function toVueResponse(array $except_keys=[])
    {
        return response()->json($this->toVueClass($except_keys));
    }



    // endregion


    // region Sub Methods

    protected function _getMergedList ()
    {
        $printable_list = [];
        $keys_met = [];
        $value_property = "desktop_value";

        foreach($this->_meta_collection as $meta){
            $keys_met[] = $meta->key;
            $enabled = true;
            if($enabled){
                $value = $meta->$value_property."";
                $has_template_value = (isset($this->_meta_templates[$meta->key][$value_property]) && $this->_meta_templates[$meta->key][$value_property]);
                if(($value=="" || is_null($value)) && $has_template_value){
                    $value = $this->_meta_templates[$meta->key][$value_property]."";
                }

                if($meta->key == 'og:title') {
                    $value = $value ?: $this->_url_model->metas->title->desktop_value;
                }

                if($meta->key == 'og:description') {
                    $value = $value ?: $this->_url_model->metas->description->desktop_value;
                }

                if($value == "" || is_null($value)) {
                    continue;
                }

                $printable_list[$meta->key] = [
                    "key"=>$meta->key,
                    "print_template"=>$meta->template,
                    "value"=>str_replace('"', '”', $value).""
                ];
            }
        }

        $unmet_keys = array_diff(array_keys($this->_meta_templates), $keys_met);

        if(count($unmet_keys)){
            foreach($unmet_keys as $key){
                $value = isset($this->_meta_templates[$key][$value_property]) ? $this->_meta_templates[$key][$value_property]."":"";
                $printable_list[$key] = [
                    "key"=>$key,
                    "print_template"=>  $this->getTemplate($key),
                    "value"=>"".$value
                ];
            }
        }

        $printable_list["author"]=[
            "key"=>"author",
            "print_template"=>"<meta name=\"$0\" content=\"$1\">",
            "value"=>"This site managed by MediaClickCMS"
        ];

        if(!array_key_exists("title",$printable_list) || trim($printable_list["title"]["value"])=="" ){
            $printable_list["title"] = $this->_createMissingBasicMeta("title");
        }
        if(!array_key_exists("description",$printable_list) || trim($printable_list["description"]["value"])==""){
            $printable_list["description"] = $this->_createMissingBasicMeta("description");
        }
        if(!array_key_exists("robots",$printable_list)){
            $check = $this->_noIndexCache();
            if($check){
                $printable_list["robots"] = [
                    "key"=>"robots",
                    "print_template"=>"<meta name=\"$0\" content=\"$1\">",
                    "value"=>"noindex,nofollow, nosnippet, noarchive"
                ];
            }
        }

        if(!array_key_exists("canonical",$printable_list)){
            $url_str = $this->_url_model->url ?? "";

            $printable_list["canonical"] = [
                "key"=>"canonical",
                "print_template"=>"<link rel=\"$0\" href=\"$1\">",
                "value"=>url($url_str)
            ];
        }

        $printable_list = $printable_list + $this->_createAlternateLinks();

        return $printable_list;
    }

    protected function _replaceVariables(array $merged_list=[])
    {
        if(! count($merged_list)){return $merged_list;}
        foreach($merged_list as $metakey => &$metavalue){
            foreach($this->_flat_meta_variables as $variable => $value){
                $metavalue = preg_replace('/%('.$variable.')%/', $value, $metavalue);
            }
        }
        if(isset($merged_list["canonical"]) && $merged_list["canonical"]){

            $url_str = $this->_url_model ? url($this->_url_model->url) :"";

            if($this->getUrlBase()){
                $current_base_url = url('');
                $url_str = str_replace($current_base_url, $this->getUrlBase(), $url_str);
                $merged_list["canonical"]["value"]=$url_str;
            }
        }

        return $merged_list;
    }

    protected function _createMissingBasicMeta(string $metakey="")
    {

        $inject=[
            "key"=>$metakey,
            "print_template"=> $this->getTemplate($metakey),
            "value"=>null
        ];


        $guide = [
            "Mediapress\\Modules\\Content\\Models\\SitemapDetail"=>[
                "title"=>"%sitemap_title%",
                "description"=>"%sitemap_detail%"
            ],
            "Mediapress\\Modules\\Content\\Models\\PageDetail"=>[
                "title"=>"%page_title%",
                "description"=>"%page_detail%"
            ],
            "Mediapress\\Modules\\Content\\Models\\CategoryDetail"=>[
                "title"=>"%category_title%",
                "description"=>"%category_detail%"
            ],
        ];

        if(
            !in_array($metakey, ["title","description"]) ||
            !($this->_url_model->model_type ?? false) ||
            !array_key_exists($this->_url_model->model_type, $guide)
        ){
            return $inject;
        }


        $inject["value"] = $guide[$this->_url_model->model_type][$metakey];


        return $inject;
    }

    protected function _createAlternateLinks()
    {
        // No url model, no alternates
        if(!$this->_url_model){
            return [];
        }

        $mp = mediapress();
        $siblings = $mp->otherLanguagesforMeta(false);
        unset($mp->otherLanguagesforMeta);
        // No sibling details, no alternates
        if(!$siblings || count($siblings)<=1){
            return [];
        }

        $results = [];


        foreach($siblings as $ind => $sibling){
            if($sibling["country_group_code"] == "en"){
                $sibling["country_group_code"] = "gb";
            }

            if($sibling["country_group_code"] == "rg"){
                $sibling["country_group_code"] = "ru";
            }

            $inject=[
                "key"=>"alternate_cg".$sibling["country_group_code"]."_lng".$sibling["language_code"],
                "print_template"=>"<link rel=\"alternate\" hreflang=\"".$sibling["language_code"].($sibling["country_group_code"]=="gl" ? "" : "-".$sibling["country_group_code"])."\" href=\"$1\" />",
                "value"=>url($sibling["url"]->url ?? "")
            ];
            $results[$inject["key"]]=$inject;
        }

        return $results;

    }

    protected function _noIndexCache($request = null, $server = null)
    {
        $request = $request ?: $request = Request::capture();
        $server = $server ?: $request->server('HTTP_HOST');
        $return =  Cache::rememberForever('server.noindex.' . $server, function () use ($server) {
            $list = [
                'test.*',
                '*.mclck.com',
                '*.mediaclick.work',
                '*.test',
                '*.local',
                '*.dev'
            ];
            foreach ($list as $disable) {
                preg_match_all('/' . str_replace('*', '.*', $disable) . '/m', $server, $matches, PREG_SET_ORDER, 0);

                if (count($matches) > 0) {
                    return true;
                }
            }
            return false;
        });

        if(!$return && \Route::current()) {
            $return = \Route::current()->getName() == 'web.url.auth';
        }

        if(!$return) {
            $passwordRequired = $this->_url_model ? $this->_url_model->model->parent->password : null;
            $return = (!is_null($passwordRequired) && $passwordRequired != '');
        }

        return is_bool($return) ? $return : false;
    }

    protected function getTemplate($key) {
        switch ($key) {
            case 'title':
                return "<$0>$1</$0>";
            case 'og:title':
                return '<meta property="$0" content="$1">';
            case 'og:description':
                return '<meta property="$0" content="$1">';
            case 'og:image':
                return '<meta property="$0" content="$1">';
            case 'og:video':
                return '<meta property="$0" content="$1">';
            default:
                return '<meta name="$0" content="$1">';
        }
    }
    // endregion



    // region Deprecated Methods
    public function dep_processMetas()
    {
        foreach($this->_meta_collection as &$meta){
            foreach($this->_flat_meta_variables as $variable => $value){
                $meta->desktop_value = preg_replace('/%('.$variable.')%/', $value, $meta->desktop_value);
            }
        }
    }

    public function dep_render($device=null)
    {
        $device = $device ?:$this->getDevice();
        echo $this->getMetaCollection()->render($device);
    }

    public function dep_toJson()
    {
        return $this->getMetaCollection()->toJson();
    }
    // endregion

}
