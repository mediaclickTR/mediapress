<?php

namespace Mediapress\Modules\MPCore\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Support\Database\CacheQueryBuilder;

class LanguagePart extends BaseModel
{
    use CacheQueryBuilder;
    use SoftDeletes;
    protected $table = 'language_parts';

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "language_id",
        "country_group_id",
        "website_id",
        "key",
        "value"
    ];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroup::class);
    }
}
