<?php

return [
    'entitylist'=>'Üye Listeleri',
    'standard'=>'Standart',
    'core'=>'Çekirdek',
    'list-members'=>'Listeye Ait Üyeler',
    'list-members-create'=>'Listeye Üye Ekle',
    'edit-entitylist'=>'Listeyi Güncelle',
    'name-entitylist'=>'Liste Adı',
    'description-entitylist'=>'Liste Açıklaması',
    'create-entitylist'=>'Liste Oluştur',
    "select_websites" => "Liste'nin Bağlanacağı Website(leri) Seç",
];