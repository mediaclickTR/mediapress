<?php
namespace Mediapress\Modules\Content\DataSources;

use Mediapress\Facades\DataSourceEngine;
use Mediapress\Foundation\DataSource;
use Mediapress\Modules\Content\Models\CategoryCriteria;
use Illuminate\Support\Arr;

class CriteriaIdsOfCategories extends DataSource
{
    public function getData()
    {
        $category_ids = $this->params["category_ids"] ?? [];

        if(count($category_ids)){
            $parent_cats = DataSourceEngine::getDataSource("Content","CategoriesWithParentTree", ["category_ids"=>$category_ids])->getData();
            $cat_ids = array_merge(Arr::collapse($parent_cats),$category_ids);
            return CategoryCriteria::whereIn("category_id",$cat_ids)->select("criteria_id")->get()->pluck("criteria_id")->toArray();
        }

        return [];
    }
}
