@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="content">
        @include("MPCorePanel::inc.errors")
        <h1>Modülü Yönet</h1>
        <div class="col-md-12 pa">
            <div class="form add-slide">
                <div class="add-remove">
                    <div class="asd">
                        <form action="{!! route('MPCore.technical.modules.managestore') !!}" novalidate method="POST">
                            @csrf
                            @foreach($module_configs as $config)
                            <div class="form-group rel focus">
                                    <label>{!! $config->config_name !!}</label>
                                    @if($config->type=="text")
                                        <input type="text" name="{!! $config->key !!}" value="{!! $config->value !!}">
                                    @elseif($config->type=="select" && $config->details)
                                        <select name="{!! $config->key !!}">
                                            <option value="">Seçim Yap</option>
                                           @foreach(json_decode($config->details) as $key=>$value)
                                                <option @if($key==$config->value) selected @endif value="{!! $key !!}">{!! $value !!}</option>
                                           @endforeach
                                        </select>
                                    @endif
                            </div>
                            @endforeach
                            <div class="submit">
                                <button type="submit">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="application/javascript">
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
    </script>
@endpush