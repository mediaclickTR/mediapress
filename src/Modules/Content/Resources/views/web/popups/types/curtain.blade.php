<link rel="stylesheet" href="{!! mp_asset('mediapressAssets/css/biggenerModal.css?v=2') !!}"/>
<div class="biggenerModal animated fadeIn" style="display: none">
    <div class="biggenerTimer" data-font-size="{{ $popup->counter_font_size }}px">
        <span id="biggenerTimer" data-timer="{{ $popup->display_time_type == 1 ? $popup->display_time : 10 }}"></span>
        <span>{!! langPartAttr('popup.curtain_counter', "saniye sonra kapanacak.") !!}</span>
    </div>
    <div class="container-fluid">
        <div class="biggenerAll">

            @if($popup->name_display == 1)
                <div class="animated biggenerText"
                     data-font-size="{{ $popup->title_size }}px"
                     data-color="{{ $popup->title_color }}"
                     data-margin="0 0 {{ $popup->title_space }}px 0"
                     data-animated-position="{{ $popup->title_animation }}">
                    {{$popup->name}}
                </div>
            @endif
            <div class="animated biggenerContent"
                 data-font-size="{{ $popup->content_font_size }}px"
                 data-color="{{ $popup->color }}"
                 data-animated-position="{{ $popup->content_animation }}">
                {!! $popup->content !!}

                <div class="biggnerContentClose"
                     data-color="{{ $popup->button_color }}"
                     data-font-size="{{ $popup->btn_font_size }}px"
                     data-position="{{ $popup->btn_position }}">
                    <span>{{ $popup->button_text }}</span>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    $(window).on('load', function () {
        setTimeout(function () {
            $('.biggenerModal').show();

            var dataTimer = $('#biggenerTimer').attr('data-timer');
            var Minutes = dataTimer * 1,
                display = document.querySelector('#biggenerTimer');
            startTimer(Minutes, display);
            $('#biggenerTimer').text(dataTimer);
        }, {{ $popup->condition_time > 0 ? $popup->condition_time * 1000 : 100 }});
    });

    $('.biggenerModal').each(function () {

        var $titleMargin = $(this).find('.biggenerText').attr("data-margin");
        $(this).find('.biggenerText,.biggenerText *').css({
            'margin': '' + $titleMargin + ''
        });

        var $titleFontSize = $(this).find('.biggenerText').attr("data-font-size");
        $(this).find('.biggenerText,.biggenerText *').css({
            'fontSize': '' + $titleFontSize + ''
        });

        var $animatedTitle = $(this).find('.biggenerText').attr("data-animated-position");
        $('.biggenerText').addClass('' + $animatedTitle + '');

        var $titleColor = $(this).find('.biggenerText').attr("data-color");
        $(this).find('.biggenerText,.biggenerText *').css({
            'color': '' + $titleColor + ''
        });

        var $contentFontSize = $(this).find('.biggenerContent').attr("data-font-size");
        $(this).find('.biggenerContent,.biggenerContent *').css({
            'fontSize': '' + $contentFontSize + ''
        });

        var $contentColor = $(this).find('.biggenerContent').attr("data-color");
        $(this).find('.biggenerContent,.biggenerContent *').css({
            'color': '' + $contentColor + ''
        });

        var $animatedContent = $(this).find('.biggenerContent').attr("data-animated-position");
        $('.biggenerContent').addClass('' + $animatedContent + '');

        var $closeColor = $(this).find('.biggnerContentClose').attr("data-color");
        $(this).find('.biggnerContentClose,.biggnerContentClose *').css({
            'color': '' + $closeColor + ''
        });

        var $closeFontSize = $(this).find('.biggnerContentClose').attr("data-font-size");
        $(this).find('.biggnerContentClose,.biggnerContentClose *').css({
            'fontSize': '' + $closeFontSize + ''
        });

        var $closePosition = $(this).find('.biggnerContentClose').attr("data-position");
        $(this).find('.biggnerContentClose,.biggnerContentClose *').css({
            'float': '' + $closePosition + ''
        });

        var $timerFontSize = $(this).find('.biggenerTimer').attr("data-font-size");
        $(this).find('.biggenerTimer').css({
            'fontSize': '' + $timerFontSize + ''
        });
    });

</script>

<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            minutes = minutes < 10 ? minutes : minutes;
            seconds = seconds < 10 ? seconds : seconds;
            display.textContent = seconds;
            if (--timer < 0) {
                $('.biggenerModal').removeClass('fadeIn');
                $('.biggenerModal').addClass('fadeOut');
                setTimeout(function () {
                    $('.biggenerModal').remove();
                    $('#biggenerTimer').remove();
                }, 500);
                timer = duration;
                /*$('#time').remove();*/
            }
        }, 900);
    }

    $('.biggnerContentClose span').click(function () {
        $('.biggenerModal').removeClass('fadeIn');
        $('.biggenerModal').addClass('fadeOut');
        setTimeout(function () {
            $('.biggenerModal').remove();
            $('#biggenerTimer').remove();
        }, 500);
    });
</script>
