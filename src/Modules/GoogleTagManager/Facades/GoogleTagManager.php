<?php

namespace Mediapress\Modules\GoogleTagManager\Facades;

use Illuminate\Support\Facades\Facade;

class GoogleTagManager extends Facade
{

    protected static function getFacadeAccessor()
    {
        return \Mediapress\Modules\GoogleTagManager\GoogleTagManager::class;
    }
}