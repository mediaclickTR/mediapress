<?php

use Illuminate\Database\Migrations\Migration;
use Mediapress\Modules\MPCore\Foundation\Blueprint;

class CreateWebsitesTable extends Migration
{

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        $schema->create('websites', function (Blueprint $table) {
            $table->increments('id');
            $table->enum("type", ["domain", "folder", "alias"]);
            $table->enum("target", ["internal", "external"]);
            $table->unsignedInteger("sort")->nullable();
            $table->string("name")->nullable();
            $table->string("slug")->nullable();

            $table->boolean("ssl")->default(false);
            $table->boolean("default")->default(false);
            $table->integer("website_id")->nullable();
            $table->tinyInteger("use_deflng_code")->default(false);
            $table->tinyInteger("regions")->default(false);

            $table->string("variation_template")->default("{lng}/{cg}");

            $table->defaultFields();
            $table->orderable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        //Schema::dropUnique("domain_unique");
        Schema::drop('websites');
        Schema::enableForeignKeyConstraints();
    }
}
