<?php

namespace Mediapress\Modules\Content\Foundation;
use Mediapress\Modules\Content\Models\SocialMedia;

class SocialMediaEngine
{
    public $array_social= array();

    public function __construct(SocialMedia $social)
    {
        $this->social = $social;
        $this->mediapress = mediapress();
        $this->languageID = $this->mediapress->activeLanguage->id;
    }

    public function socialMedia($group_id)
    {
        $website = $this->mediapress->website;
        $socialmedia = $this->social->where(['website_id' => $website->id,'group_id' => $group_id,'status'=>"1"])->orderBy('order')->get();
        $country_group_id =$this->mediapress->activeCountryGroup->id;
        $array_social=[];


        foreach ($socialmedia as $social){
            if(isset($social->link[$country_group_id][$this->languageID])) {
                $array_social[] = [
                    'id'=>$social->id,
                    'link'=>$social->link[$country_group_id][$this->languageID],
                    'name'=>$social->name,
                    'icon'=>$social->icon
                ];
            }
        }

        return collect($array_social);
    }
}
