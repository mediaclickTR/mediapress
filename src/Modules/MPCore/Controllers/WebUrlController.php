<?php

namespace Mediapress\Modules\MPCore\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Foundation\UrlMetaWorker;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\DynamicUrl;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\RedirectUrl;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class WebUrlController extends Controller
{
    public const SITEMAP = "sitemap";
    public const SCHEME = 'scheme';
    public const PARENT = "parent";
    protected $parsedUrl;
    public $request;

    /***
     * Parse edilen url'i birleştiriyor
     * @param string $parsed_url
     * @return string
     * @see http://php.net/manual/tr/function.parse-url.php#106731
     */
    private function unparse_url($parsed_url): string
    {
        $scheme = isset($parsed_url[self::SCHEME]) ? $parsed_url[self::SCHEME] . '://' : '';
        $host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass = isset($parsed_url['pass']) ? ':' . $parsed_url['pass'] : '';
        $pass = ($user || $pass) ? "$pass@" : '';
        $path = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    public function fillMediapress($parsedData = null)
    {

        $redirectRequired = $this->redirectUrl($parsedData);
        $request = request();

        $domain = $parsedData["host"] = str_replace("www.", "", $parsedData["host"]);
        $domainHold = [$domain, "www.".$domain];
        /** @var Website $website */
        $website = Website::whereIn('slug', $domainHold)->first();
        $parent = null;

        if ($website && $website->website && $website->type == 'alias') {
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold) {
                $q->whereIn("slug", $domainHold);
            })->with("dynamicUrl")->orderBy("created_at")->first();

        }else{
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold) {
                $q->whereIn("slug", $domainHold);
            })->with("dynamicUrl")->orderBy("created_at")->first();
        }

        if (!$url) {
            $dynamic = DynamicUrl::whereRaw('? REGEXP `regex`', [$parsedData["path"]])->first();
            if($dynamic){
                $url = $dynamic->url;
            }
        }

        if (!$url) {
            $folder = $request->segment(1);
            $parsedData['path'] = mb_substr($parsedData['path'],(strlen($folder)+1));
            $url = Url::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domainHold,$folder) {
                $q->where("slug", $folder)->whereHas('website',function ($query) use($domainHold){
                    $query->whereIn('slug',$domainHold);
                });
            })->with("dynamicUrl")->orderBy("created_at")->first();
        }


        $redirectCategoryUrl = $this->redirectCategoryUrl($parsedData, $url);
        if($redirectCategoryUrl != false) {
            return $redirectCategoryUrl;
        }

        if (!$url || !$url->model) {

            if ($redirectRequired !== false && isset($parsedData['original']) && $parsedData['original'] != $parsedData['path']) {

                return $redirectRequired;
            }

            /** @var Mediapress $mediapress */
            $requestPath = $request->path();
            if ($requestPath == '/') {

                $mediapress = mediapress();
                if ($mediapress->website){
                    $cg = $mediapress->website->predictCountryGroup();
                }else{
                    $website = Website::first();
                    $website = getWebsiteRootUrl($website->id);
                    return redirect($website);
                }

                if ($cg) {
                    if ($cg->code == 'gl') {
                        $language = $this->getLocationLanguage($cg);
                        $sitemap = SitemapDetail::where("language_id", $language->id)
                            ->where("country_group_id", $cg->id)
                            ->whereHas(self::SITEMAP, function ($q) {
                                $q->where("feature_tag", "homepage");
                            })->first();

                        if($sitemap && $sitemap->url){
                            return new RedirectController($sitemap->url->url,301);
                        }
                    }
                    $mediapress->activeLanguage =  $lg = $mediapress->website->predictLanguage($cg);
                    $mediapress->activeCountryGroup = $cg;
                    $sitemap = SitemapDetail::where("country_group_id", $cg->id)
                        ->where("language_id",$lg->id)
                        ->whereHas(self::SITEMAP, function ($q) {
                        $q->where("feature_tag", "homepage");
                    })->first();
                    if($sitemap && $sitemap->url){
                        return new RedirectController($sitemap->url->url,301);
                    }
                }
            }
            $mediapress = mediapress();

            $mediapress->activeLanguage;
            $mediapress->homePageUrl();
            $mediapress->type = 404;

            abort(404);
        } else {
            $mediapress = mediapress();

            if ($redirectRequired !== false && isset($parsedData['original']) && $parsedData['original'] != $parsedData['path']) {

                return $redirectRequired;
            }
        }


        mediapress("website", $url->website);
        mediapress("url", $url);
        mediapress("metaworker", (new UrlMetaWorker($url)));

        $relation = mediapress("relation",$url->model);
        if(is_a($relation, PageDetail::class) || is_a($relation, SitemapDetail::class)){
            if (($relation->parent->password != null || $relation->parent->password != "") && !session()->exists("password_for_" . $url->id)) {
                mediapress("next", $url->url);
                return new PasswordRequiredController();
            }
        }


        if($relation) {
            $model = $relation->parent;

            $status = null;
            $invert = null;
            if(isset($model->status)){
                $status = $model->status;
                $invert = Carbon::parse($model->published_at)->diff()->invert;
            }
            if($model && isset($model->status) && ($status != 1 || ($status != 5 &&  $invert != 0))) {

                if($request->get('preview') != 1 && (($status == 5 && $invert != 0) || $status != 1 && $status != 5)) {
                    abort(404);
                }
            }
        }

        /** Force zone redirect */
        if(config('mediapress.force_redirect_to_zone') && !auth()->guard('admin')->check()){
            $isDiff = $website->isCountryGroupDifferent($relation);
            if($isDiff){
                $cg = $isDiff['cg'];
                $lg = $isDiff['lg'];
                $parent = $relation->parent;
                $findDetail = $parent->details()->where('country_group_id',$cg->id)
                    ->where('language_id',$lg->id)->first();
                if($findDetail){
                    return new RedirectController($findDetail->url->url,301);
                }
                $sitemap = SitemapDetail::where("country_group_id", $cg->id)
                    ->where("language_id",$lg->id)
                    ->whereHas(self::SITEMAP, function ($q) {
                        $q->where("feature_tag", "homepage");
                    })->first();
                if($sitemap && $sitemap->url){
                    return new RedirectController($sitemap->url->url,301);
                }else{
                    abort(404);
                }

            }
        }
        if($relation && $relation->country_group_id){
            mediapress("activeLanguage",Language::find($relation->language_id));
            mediapress("activeCountryGroup",CountryGroup::find($relation->country_group_id));
        }




        $siteMapType = null;

        //TODO:Düşük Öncelikli -> Aşağıdaki classların, relation'ın bağlı olduğu modulden otomatik işlenmesi ve aşağıdaki kodun class ismi belirtmede jeneric çalışması
        if (is_a($relation, CategoryDetail::class)) {
            /** @var CategoryDetail $relation */
            mediapress(self::PARENT, $relation->category);
            mediapress(self::SITEMAP, mediapress(self::PARENT)->sitemap);
            $siteMapType = &mediapress(self::SITEMAP)->sitemapType;
        } else if (is_a($relation, PageDetail::class)) {
            /** @var PageDetail $relation */
            mediapress(self::PARENT, $relation->page);
            mediapress(self::SITEMAP, mediapress(self::PARENT)->sitemap);
            $siteMapType = mediapress(self::SITEMAP)->sitemapType;
            if (config('mediapress.allow_postdate_content') && $relation->page->status == Page::POSTDATE && $relation->page->published_at->diff()->invert == 0) {
                $relation->page->status = Page::ACTIVE;
                $relation->page->published_at = null;
                $relation->page->save();
            }
        } else if (is_a($relation, SitemapDetail::class)) {
            /** @var SitemapDetail $relation */
            mediapress(self::PARENT, $relation->sitemap);
            mediapress(self::SITEMAP, mediapress(self::PARENT));
            $siteMapType = mediapress(self::SITEMAP)->sitemapType;
        } else {
            throw new \Exception(__CLASS__ . " New Type Founded : " . get_class($relation), __LINE__);
        }

        $classNamespace = "App\Modules\Content\\Website". $mediapress->website->id ."\Http\Controllers\Web\\" . ucfirst($siteMapType->name) . "Controller";

        if($mediapress->website->type == 'folder'){
            $classNamespace = "App\Modules\Content\\Website". $mediapress->website->id ."\Http\Controllers\Web" . "\\" . ucfirst($siteMapType->name) . "Controller";
        }

        if (!class_exists($classNamespace)) {
            throw new \Exception(__CLASS__ . " Controller Not Found: " . $classNamespace, __LINE__);
        }
        $class = new $classNamespace;
        if (!is_a($class, "Mediapress\Http\Controllers\BaseController")) {
            throw new \Exception(__CLASS__ . " BaseController Not Found: Mediapress\Http\Controllers\BaseController", __LINE__);
        }
        return $class;
    }

    public function index(Request $request)
    {

        if(config('mediapress.vue_url')) {
            return redirect()->to(config('mediapress.vue_url') . (parse_url($request->fullUrl())['path'] ?? ''));
        }
        start_measure('mediapress','Mediapress');
        $response = $this->fillMediapress(array_merge(["path" => "/"], parse_url($request->fullUrl())));

        if ($response instanceof \Illuminate\Http\RedirectResponse) {

            return $response;
        }
        $controller = app()->call([$response, "main"]);
        stop_measure('mediapress','Mediapress');

        return $controller;
    }

    /**
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function redirectUrl(&$parsedData)
    {
        //parsed_url den host çekilip domain olarak websites içinde sorgulanıyor
        $domain = $parsedData["host"];
        $redirect = RedirectUrl::where("url", $parsedData["path"])->whereHas("website", function ($q) use ($domain) {
            return $q->where("slug", $domain);
        })->orderBy("created_at")->first();

        //redirect bulunduysa şemayı oluştur
        if ($redirect != null) {
            //websitesi ssl'e sahip ise scheme yı https olarak düzenle
            $parsedData[self::SCHEME] .= $redirect->website->has_ssl ? 's' : '';
            //redirect yapılacak url'in path'i ayarlaniyor
            $parsedData["original"] = $parsedData["path"];
            $parsedData["path"] = $redirect->model->url;
            //301 ile url unparse edilip redirect ediliyor
            return redirect($this->unparse_url($parsedData), 301);
        }
        return false;
    }

    protected function redirectCategoryUrl($parsedData = [], $url) {

        if($url && $url->model instanceof SitemapDetail && $url->type != 'category') {
            $redirect = $url->model->categoryUrl;
            if($redirect) {
                $parsedData[self::SCHEME] .= $redirect->website->has_ssl ? 's' : '';
                $parsedData["original"] = $parsedData['path'];
                $parsedData["path"] = $redirect;
                return redirect($this->unparse_url($parsedData), 301);
            }
        }
        return false;
    }

    private function getLocationLanguage($countryGroup)
    {
        $language = mediapress()->website->defaultLanguage();
        $browserLanguage = [];


        if(config('mediapress.redirect_browser_language') && count($browserLanguage)) {
            $language = Language::where('code', $browserLanguage)->first();
        }

        return $language;
    }
}
