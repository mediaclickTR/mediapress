@extends('MPCorePanel::inc.app')
@push("styles")
    <style>
        [class^="col-"] {
            float: left;
        }
        .turn{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }
    </style>
@endpush
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title">
            <div class="float-left">
                Instagram
            </div>
{{--            <div class="float-right" >--}}
{{--                <a role="button" href="{!! route('Content.instagram.sync') !!}"  class="btn btn-primary"><i class="fas fa-sync"></i>Verileri Eşitle</a>--}}
{{--            </div>--}}

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.album') !!}" class="btn btn-primary"><i class="fas fa-plus"></i>Albüm Oluştur</a>
            </div>

            <div class="float-right" style="margin-right: 10px;">
                <a role="button" href="{!! route('Content.instagram.index') !!}" class="btn btn-primary"><i class="fas fa-images"></i>Albümler</a>
            </div>

        </div>
        @php
            if( isset($_GET["album"]) && $_GET["album"] == true){
        @endphp
            <div class="alert alert-success" role="alert" style="margin-top: 80px; margin-bottom: 0;">
                Albüm Başarılı Bir Şekilde Kayıt Edildi
            </div>
        @php
         }
        @endphp
        @if(isset($name) && isset($_GET["album"]) && $_GET["album"] == true)
            <div class="alert alert-success" role="alert" style="margin-top: 80px; margin-bottom: 0;">
                <strong>{!! $name !!}</strong> Albümü Verileri Güncellendi
            </div>
        @endif
        @if(isset($name) && isset($_GET["delete"]) && $_GET["delete"] == true)
            <div class="alert alert-success" role="alert" style="margin-top: 80px; margin-bottom: 0;">
                <strong>{!! $name !!}</strong> Albümü Başarılı Bir Şekilde Silindi
            </div>
        @endif

        <table class="table table-hover" style="margin-top: 50px">
            <thead>
            <tr>
                <th scope="col"><h4>Albümler</h4></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            <tr>
                <th scope="col">Albüm Adı</th>
                <th scope="col">Slug</th>
                <th scope="col">Durum</th>
                <th scope="col">Senkron Verisi</th>
                <th scope="col">Yenilenme Süresi</th>
                <th scope="col">İşlemler</th>
            </tr>
            </thead>
            <tbody>
                        @foreach($albums as $album)
                            <input type="hidden" name="time-{!! $album->id !!}" data-date="">
                            <tr>
                                <th scope="row">{!! $album->name !!}</th>
                                <td>
                                    {!! $album->slug !!}
                                </td>
                                <td>
                                    <i class="status_circle mr-1 {!! $album->status == 1 ? "active": "passive" !!}" title="{!! $album->status == 1 ? "Aktif": "Pasif" !!}"></i>
                                </td>
                                <td>
                                    {!! $album->sync_data !!}
                                </td>
                                <td>
                                    <strong id="coutdown-{!! $album->id !!}" style="margin-top: 2px">
                                        0h 0m 0s
                                    </strong>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" title="Şimdi Güncelle" data-toggle="tooltip" data-placement="top" class=""><span
                                                class="refresh-now fa fa-sync" data-id="{!! $album->id !!}"></span></a>
                                    <a onclick="locationOnChange('')"
                                       href="{!! route('Content.instagram.edit-album',$album->id) !!}" title="{!! trans("MPCorePanel::general.edit") !!}"><span
                                                class="fa fa-pen"></span></a>
                                    <a href="javascript:void(0)" title="{!! trans("MPCorePanel::general.delete") !!}" ><span
                                                class="fa fa-trash delete-album" data-id="{!! $album->id !!}"></span></a>

                                </td>
                            </tr>
                        @endforeach


                    </tbody>
        </table>
    </div>

@endsection
@push('scripts')
    <script>
        var countDownDate = new Date("Jan 21 2020 00:00:00");
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time

            var now = new Date().getTime();
            // Find the distance between now and the count down date
            var distance = countDownDate-now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("coutdown-17").innerHTML = hours + "h "+ minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "Güncellendi";
            }
        }, 1000);

        $(".refresh-now").click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            $(this).addClass("turn");
            console.log(id);
            $.post( "{!! route("Content.instagram.deleteCache") !!}", {
                "id": id,
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {
                $(this).removeClass("turn");
               var url = window.location.href+"?sync=true&id="+id;
                window.location.href = url;
            });
        });

        $(".delete-album").click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            console.log(id);
            $.post( "{!! route("Content.instagram.deleteAlbum") !!}", {
                "id": id,
                "_token": "{{ csrf_token() }}",
            }).done(function( data ) {
                alert(data);
                if (data == "true"){
                    var url = window.location.href+"?delete=true&id="+id;
                    window.location.href = url;
                }
            });
        });
    </script>
@endpush
