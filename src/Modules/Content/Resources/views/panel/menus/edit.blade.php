@extends('ContentPanel::inc.module_main')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        @include("MPCorePanel::inc.errors")
        <div class="topPage">
            <div class="float-left">
                <div class="title m-0">
                    {!! __("ContentPanel::menu.edit_menu") !!}
                </div>
            </div>
            <div class="float-right">
                <a href="{{ route('Content.menus.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30">
            @include("ContentPanel::menus.form")
        </div>
@endsection
