<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'panel.auth'], function () {
    /*
     * Analytics Modules
     * Include Google, Yandex, Bing
     */

    Route::get('/', ['as' => 'panel.dashboard', 'uses' => 'DashboardController@index']);
    Route::get('/notPermitted', 'DashboardController@notPermitted')->name("not_permitted");
    Route::get('/error', 'DashboardController@error')->name("error");
    Route::get('/dashboardAjax', [ 'uses' => 'DashboardController@ajax'])->name('dashboard.ajax');

    Route::group(['prefix' => 'Interaction', "as" =>"Interaction."], function () {

        Route::group(['prefix' => 'Analytics', 'as' => 'analytics.'], function () {

            Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);

            Route::group(['prefix' => '/Google', 'as' => 'google.'], function () {

                Route::get('/OAuth', ['as' => 'oAuth', 'uses' => 'GoogleController@oAuth']);
                Route::get('/login', ['as' => 'login', 'uses' => 'GoogleController@login']);

                Route::get('/', ['as' => 'index', 'uses' => 'GoogleController@index']);
                Route::get('/detailedAnalytics', ['as' => 'detailedAnalytics', 'uses' => 'GoogleController@detailedAnalytics']);
                Route::get('googleAnalyticsSetting', ['as' => 'googleAnalyticsSetting', 'uses' => 'GoogleController@googleAnalyticsSetting']);
                Route::get('googleAnalyticsDoc', ['as' => 'googleAnalyticsDoc', 'uses' => 'GoogleController@googleAnalyticsSettingDoc']);
                Route::post('googleAnalyticsSettingPost', ['as' => 'googleAnalyticsSettingPost', 'uses' => 'GoogleController@googleAnalyticsSettingPost']);
                Route::get('googleAnalyticsDisconnect', ['as' => 'googleAnalyticsDisconnect', 'uses' => 'GoogleController@googleAnalyticsDisconnect']);
            });

            Route::group(['prefix' => '/Yandex', 'as' => 'yandex.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'YandexController@index']);
                Route::get('/detailedAnalytics', ['as' => 'detailedAnalytics', 'uses' => 'YandexController@detailedAnalytics']);
            });

            Route::group(['prefix' => '/Bing', 'as' => 'bing.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'BingController@index']);
                Route::get('/detailedAnalytics', ['as' => 'detailedAnalytics', 'uses' => 'BingController@detailedAnalytics']);
            });

        });
    });
});
