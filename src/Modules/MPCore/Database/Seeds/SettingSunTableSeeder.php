<?php
namespace Mediapress\Modules\MPCore\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSunTableSeeder extends Seeder
{
    public const WEBSITE_ID = 'website_id';
    public const GROUP = 'group';
    public const TITLE = 'title';
    public const ANALYTICS = 'Analytics';
    public const VTYPE = 'vtype';
    public const VALUE = 'value';
    public const STRING = 'string';
    public const PARAMS = 'params';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete users table records
        DB::table('setting_sun')->delete();
        //insert some dummy records
        DB::table('setting_sun')->insert([
            [self::WEBSITE_ID =>null, self::GROUP =>'E-Posta Yönetimi', self::TITLE =>'Mailchimp API Key','key'=>'heraldist.mailchimp.apikey', self::VALUE =>'a7376c4ec8bf6dd27d24266bd01808df-us18', self::VTYPE =>     self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP =>'E-Posta Yönetimi', self::TITLE =>'Gönderici E-Posta','key'=>'heraldist.mpmailler.fromemailaddress', self::VALUE =>'default@default.com', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP =>'Varsayılan Gönderici Adı', self::TITLE =>'Gönderici E-Posta','key'=>'heraldist.mpmailler.fromname', self::VALUE =>'test', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP => self::ANALYTICS, self::TITLE =>'Yandex Metrica Cache','key'=>'mpcore.analytics.yandex.cache', self::VALUE =>'600', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP => self::ANALYTICS, self::TITLE =>'Yandex Metrica Counter ID (Sayaç ID)','key'=>'mpcore.analytics.yandex.counter_id', self::VALUE =>'49873495', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP => self::ANALYTICS, self::TITLE =>'Yandex Metrica Token','key'=>'mpcore.analytics.yandex.token', self::VALUE =>'AQAAAAAo4lphAAUjimZh6CfDoEkevy2m8jNLwt0', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP => self::ANALYTICS, self::TITLE =>'Bing API Key','key'=>'mpcore.analytics.bing.apikey', self::VALUE =>'5d8fb140135c40f28a09042f7fe81bff', self::VTYPE => self::STRING, self::PARAMS =>null],
            [self::WEBSITE_ID =>null, self::GROUP => self::ANALYTICS, self::TITLE =>'Bing API website URL','key'=>'mpcore.analytics.bing.api_website_url', self::VALUE =>'http://test.com', self::VTYPE => self::STRING, self::PARAMS =>null],
        ]);
    }
}
