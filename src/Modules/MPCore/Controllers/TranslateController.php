<?php

namespace Mediapress\Modules\MPCore\Controllers;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Modules\MPCore\Exports\LanguagePartExport;
use Mediapress\Modules\MPCore\Imports\LanguagePartImport;
use Mediapress\Modules\Content\Facades\VariationTemplates;
use Mediapress\Modules\MPCore\Facades\TranslateEngine;
use Mediapress\Modules\MPCore\Models\LanguagePart;
use Maatwebsite\Excel\Facades\Excel;

class TranslateController extends Controller
{
    public const PANEL_LANGUAGES = "panel.languages";

    public function index()
    {

        if (!activeUserCan(["mpcore.translations.index",])) {
            return rejectResponse();
        }

        // Ön yüzden panelLanguages config dosyası güncellenirse panel.languages session'ını yenile
        $config_lang = config("panelLanguages");
        $session_lang = session(self::PANEL_LANGUAGES);
        $diff_session_config_lang = strcmp(serialize($config_lang), serialize($session_lang));
        if ($diff_session_config_lang) {
            session()->forget(self::PANEL_LANGUAGES);
            session()->put(self::PANEL_LANGUAGES, $config_lang);
        }

        $website = session('panel.website');
        $default_language = $website->defaultLanguage()->id;

        $data['website_id'] = $website->id;
        $parts = LanguagePart::with('language')->where('website_id', $website->id)->get([
            'key', 'value', 'language_id', 'country_group_id'
        ]);
        $variations = VariationTemplates::variations($website);

        $panel_languages = session("panel.languages.languages");
        $website_languages = array_keys($variations[$website->id]['languages']);

        $data['parts'] = [];
        // TODO country group'a göre düzenleme gerekiyor

        foreach ($parts as $part) {
            $language_id = ($part->language) ? $part->language->id : 0;
            $country_group_id = ($part->country_group_id) ? intval($part->country_group_id) : 0;
            $data['parts'][$part->key][$language_id][$country_group_id] = $part->value;
        }
        return view("MPCorePanel::translate.index",
            compact('data', 'website', 'variations', 'default_language', 'website_languages', 'panel_languages',
                'diff_session_config_lang'));
    }

    public function search()
    {

        $searchKey = request()->get('search_key');
        $website = session('panel.website');
        $default_language = $website->defaultLanguage()->id;

        $data['website_id'] = $website->id;
        $parts = LanguagePart::with('language')
            ->where('website_id', $website->id)
            ->where(function ($q) use ($searchKey) {
                $q->where('key', 'like', '%'.$searchKey.'%')
                    ->orWhere('value', 'like', '%'.$searchKey.'%');
                if($q->first()) {
                    return $q->orWhere('key', $q->first()->key);
                }
            })
            ->get(['key', 'value', 'language_id', 'country_group_id']);


        $variations = VariationTemplates::variations($website);

        $panel_languages = session("panel.languages.languages");
        $website_languages = array_keys($variations[$website->id]['languages']);

        $data['parts'] = [];
        // TODO country group'a göre düzenleme gerekiyor

        foreach ($parts as $part) {
            $language_id = ($part->language) ? $part->language->id : 0;
            $country_group_id = ($part->country_group_id) ? intval($part->country_group_id) : 0;
            $data['parts'][$part->key][$language_id][$country_group_id] = $part->value;
        }
        return view("MPCorePanel::translate.ajax",
            compact('data', 'website', 'variations', 'default_language', 'website_languages', 'panel_languages'))->render();

    }

    public function save(Request $request)
    {

        if (!activeUserCan(["mpcore.translations.update",])) {
            return rejectResponse();
        }

        return TranslateEngine::ajaxSaveTranslate($request);
    }

    public function delete(Request $request)
    {
        $key = $request->get('key');

        if($key) {
            LanguagePart::where('key', $key)->delete();
        }
        return redirect()->route('MPCore.translate.index');
    }

    public function export() {
        return Excel::download(new LanguagePartExport, 'language_parts.xlsx');
    }

    public function import(Request $request) {

        $file = $request->file('excelFile');

        if($file) {
            $fileName = $file->store(null, 'public');
            $fileName = storage_path('app/public/'. $fileName);


            Excel::import(new LanguagePartImport, $fileName);

            @unlink($fileName);

            return redirect()->route('MPCore.translate.index');
        }
    }
}
