<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitemapDetailsTable extends Migration
{

    public const SITEMAP_DETAILS = 'sitemap_details';

    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        if (!$schema->hasTable(self::SITEMAP_DETAILS)) {
            $schema->create(self::SITEMAP_DETAILS, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('sitemap_id')->unsigned()->index();
                $table->integer('language_id')->unsigned()->index();
                $table->unsignedInteger('country_group_id')->nullable()->index();
                $table->unsignedInteger('website_id');
                $table->string('name');
                $table->string('slug')->nullable();
                $table->string('category_slug')->nullable();
                $table->boolean('active');

                $table->integer("status")->default(3);
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable(self::SITEMAP_DETAILS)) {
            Schema::drop(self::SITEMAP_DETAILS);
        }
    }
}
