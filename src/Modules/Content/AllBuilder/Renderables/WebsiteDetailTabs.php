<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 24.09.2018
     * Time: 16:07
     */

    namespace Mediapress\Modules\Content\AllBuilder\Renderables;

    use Mediapress\AllBuilder\Foundation\BuilderRenderable;
    use Mediapress\Foundation\HtmlElement;


    class WebsiteDetailTabs extends BuilderRenderable
    {

        public const OPTIONS = "options";
        public const CONTENTS = "contents";
        public const ATTRIBUTES = "attributes";
        public const VALUE = "value";
        public const DETAIL = "detail";
        public const PARAMS = "params";
        public $contents = [
            [
                "type" => "tab",
                self::OPTIONS => [
                ],
                self::CONTENTS => [
                    [
                        "type" => "div",
                        self::OPTIONS => [
                            "html" => [
                                self::ATTRIBUTES => [
                                    "class" => self::CONTENTS
                                ]
                            ]
                        ],
                        self::CONTENTS => [
                            [
                                "type" => "formgroup",
                                self::CONTENTS => [
                                    [
                                        "type" => "inputwithlabel",
                                        self::OPTIONS => [
                                            "title" => "Website Başlığı (title tag)",
                                            "html" => [
                                                self::ATTRIBUTES => [
                                                    "name" => "detail->name",
                                                    self::VALUE => "<print>detail->name</print>"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ]
        ];

        public $collectable_as = ["tabs", "detailtabs"];

        private $rename_search = self::DETAIL;
        private $rename_replace = self::DETAIL;


        // Bu

        public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
        {

            parent::__construct($params, $contents, $options, $data);

            $contents = $this->contents;

            $keyname = $this->params["keyname"] ?? 'key';
            $itemname = $this->params["itemname"] ?? self::DETAIL;
            $this->rename_search = $itemname;
            $array = $this->params['array'] ?? $this->params['details'];

            $tabs = [];
            $modeltab = array_shift($this->contents);
            if (isset($this->options["capsulate"]) && $this->options["capsulate"]) {
                $current_class = data_get($modeltab, "options.html.attributes.class", "");
                $renewed_class = implode(" ", array_filter([$current_class, "detail-capsule"]));
                data_set($modeltab, "options.html.attributes.class", $renewed_class);
            }

            foreach ($array as ${$keyname} => ${$itemname}) {
                $tab = $modeltab;
                $detail_class = get_class(${$itemname});
                $parent_class = preg_replace('/Detail$/', '', $detail_class);
                data_set($tab, "options.html.attributes.data-detail-type", $detail_class);
                data_set($tab, "options.html.attributes.data-detail-id", ${$itemname}->id);
                data_set($tab, "options.html.attributes.data-language-id", ${$itemname}->language_id);
                data_set($tab, "options.html.attributes.data-country-group-id", ${$itemname}->country_group_id);
                data_set($tab, "options.html.attributes.data-parent-type", $parent_class);
                data_set($tab, "options.html.attributes.data-parent-id", ${$itemname}->parent_id);
                $find_alias = array_search($detail_class, $this->options["object_types"]);
                if ($find_alias !== false) {
                    $this->rename_replace = $find_alias . "->" . ${$itemname}->id;
                }
                $tab = $this->map_raw_contents([$this, "renameFormField"], $tab);

                if (!isset($tab[self::PARAMS])) {
                    $tab[self::PARAMS] = [];
                }

                data_set($tab, 'options.title', get_variation_local_title(${$itemname}), false);


                $tab[self::PARAMS][$itemname] = ${$itemname};
                $tabs[] = $tab;

            }

            $this->contents += $this->parseAnnotations($tabs);

        }

        protected function renameFormField($field)
        {
            $name = $field[self::OPTIONS]["html"][self::ATTRIBUTES]["name"] ?? null;
            if (!is_null($name) && \Str::startsWith($name, $this->rename_search)) {
                    $field[self::OPTIONS]["html"][self::ATTRIBUTES]["name"] = preg_replace('/^' . preg_quote($this->rename_search, '/') . '/', $this->rename_replace, $name);
            }
            return $field;
        }


        public function getHtmlElement()
        {
            $el = $this->getSelfHtmlElement();
            $contents_var = $this->getContentHtmlElements();
            $nav = (new HTMLElement("ul", false))->add_class("nav nav-tabs");
            $counter = 0;
            foreach ($contents_var as $content) {
                if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tab-pane") && $content->get_tag()) {
                    $counter++;
                    if ($counter == 1) {
                        $content->add_class("active show");
                    }
                    $content->add_class("fade");
                    $button = (new HtmlElement('a', false))
                        ->add_attr("href", '#' . $content->get_id())
                        ->data('toggle', 'tab')
                        ->add_class($counter == 1 ? "active show" : "")
                        ->add_content($content->data('tab-title'));
                    $nav->add_content((new HTMLElement("li", false))->add_content($button));
                }
            }

            $tabcontent = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;");

            $el->add_content($nav)
                ->add_content(
                    $tabcontent->add_content($contents_var)
                        ->add_content("<div class='clearfix'></div>")
                );
            return $el;
        }
    }
