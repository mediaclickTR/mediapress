<?php

return [
    'social_media' => 'Sosyal Medya',
    'link' => 'Sosyal Medya Linki',
    'add_social_media' => 'Sosyal Medya Ekle',
    'select_group' => 'Grup Seçiniz',
    'group' => 'Grup',
    'name' => 'Social Medya Adı',
    'icon' => 'Sosyal Medya İkonu',
    'target_area' => 'Hedef Bölge'
];
