@if(isset($field))
    <{!! $field['tag'] !!} {!! $field->attributes() !!} id="{!! $field['id'] !!}" >
    {!! $field['content'] !!}
    @include('HeraldistWeb::forms.email_main',['fields'=>$field->children])
    </{!! $field['tag'] !!}>
@endif