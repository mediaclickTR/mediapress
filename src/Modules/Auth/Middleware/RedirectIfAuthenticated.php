<?php

namespace Mediapress\Modules\Auth\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if(isPanel()){
            $guard = 'admin';
        }else{
            $guard = null;
        }
        if ($guard == 'admin' && Auth::guard($guard)->check() && session("panel.website") && session("panel.user")) {
            return redirect('/mp-admin');
        }elseif ($guard == null && Auth::guard($guard)->check()){
            return redirect('/');
        }

        return $next($request);
    }
}
