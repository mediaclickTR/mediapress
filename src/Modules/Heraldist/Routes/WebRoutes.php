<?php

Route::post('/ebulletinSender', ['as' => 'ebulletin.sender', 'uses' => 'WebEbulletinController@ebulletinSender']);
Route::get('/e-bulletin/{id}', ['as' => 'ebulletin.activation', 'uses' => 'WebEbulletinController@eBulletinActivation']);
Route::post('/form/store',['as' => 'form.store', 'uses' => 'WebFormController@store']);

Route::post('/change-country',['as' => 'form.changeCountry', 'uses' => 'WebFormController@changeCountry']);
Route::post('/change-province',['as' => 'form.changeProvince', 'uses' => 'WebFormController@changeProvince']);

Route::get("uploaded-file/{filename}", "WebFormController@uploadedFile")->name("download.uploadedFile");
