<?php

namespace Mediapress\Http\Controllers;

use App\Http\Controllers\Controller;
use Mediapress\Traits\DefaultSitemapKitTrait;

class BaseController extends Controller
{
    use DefaultSitemapKitTrait;

    public function main()
    {
        if (!method_exists($this, $method_name = \Str::camel(substr(strrchr(mediapress("url")->model_type, "\\"), 1)))) {
            throw new  \Exception(get_class($this) . " Method Not Found : " . $method_name, __LINE__);
        }
        return app()->call([$this, $method_name]);
    }

}
