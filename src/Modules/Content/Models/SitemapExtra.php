<?php

namespace Mediapress\Modules\Content\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Support\Database\CacheQueryBuilder;

class SitemapExtra extends Model {

    use CacheQueryBuilder;
	protected $table = 'sitemap_extras';

    protected $fillable = ["sitemap_id","key", "value"];

    public $timestamps=false;

    public function sitemap()
    {
        return $this->belongsTo(Sitemap::class);
	}

    public function parent()
    {
        return $this->belongsTo(Sitemap::class);
	}
}
