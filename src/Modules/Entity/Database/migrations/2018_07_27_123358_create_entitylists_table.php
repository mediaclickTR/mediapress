<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('entitylist', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('list_type', ['standard','core'])->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('status');
            $table->timestamps();
        });

        // Pivot With websites
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('entity_list_website', function (Blueprint $table) {
            $table->integer('entity_list_id');
            $table->integer('website_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entitylist');
        Schema::drop('entity_list_website');
    }
}
